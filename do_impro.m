function [Cell,edgCloSit,resu,bund1] = do_impro(Cell,edgCloSit,resu,thisDem,...
    Sim,studyA,SINR2tput,clu,ter,dem,bund1,dsm,SlLigDat)
%% [Title]
% [Description]
%
% * v0.1 KK 06Feb17 Created

% If there is unserved demand in this year, then we need to perform improvements. The below
% algorithm runs in two modes essentially:
%   a) during the first mode, the network is densified randomly, in order to be able to serve all
%      the unserved demand
%   b) When the first step has been completed, then the algorithm will get into the second mode,
%      which would try to make the changes in a more efficient manner.
% There is also an element of time though, i.e. we do not allow any improvements if the elapsed
% time for the algorithm in this year is more than 1h (=3600 secs). This limit though can be
% changed.

%% Code example to rename hash files
% for m = 1 : length(m_)
%     % Update the year
%     Sim.yea.thisYea = m_(m);
%     
%     Hash1 = DataHash(struct('Sim_yea_thisYea',Sim.yea.thisYea));
%     Hash2 = DataHash(struct('Sim_yea_thisYea',Sim.yea.thisYea,...
%         'Sim_shari_net',Sim.shari.net));
%     
%     status = movefile([Hash1 '.mat'],[Hash2 '.mat']);
%     assert(status)
% end
% Sim.yea = rmfield(Sim.yea,'thisYea');
% clear m Hash1 Hash2 m_

%%
%HO: Create Hash file for this year
Hash = DataHash(struct('simYeaThisYea',Sim.yea.thisYea,...
    'simShariIs',Sim.shari.is));
% beep
% disp('Deleting Hash')
% delete(['OutpAux\' Hash '.mat'])

%HO: check if this year has been simulated. If this is the case then get the related mat file
if exist(['OutpAux\' Hash '.mat'],'file')
    
    load(['OutpAux\' Hash '.mat'],'Cell','edgCloSit','bund1') %HO: KK: it is a good practise to specify what we would like to load.. e.g. Cell, edgCloSit
    
    S = load(['OutpAux\' Hash '.mat'],'resu');
    resu.demFoc = S.resu.demFoc; %@HO: it looks like all the results needed are in Cell, edgCloSit resu.demFoc, resu.demBuf, resu.demUnservPerc? -> KK: this 
    resu.demBuf = S.resu.demBuf;
    resu.servPerc = S.resu.servPerc;
    resu.demUnservPerc = S.resu.demUnservPerc;
    resu.BWperGovernora = S.resu.BWperGovernora;
    
    switch Sim.SA.name
      case 'West Bank'
        resu.BWperOslo = S.resu.BWperOslo;
      case 'Gaza'
      otherwise
        error('Undefined')
    end
else %HO: otherwise, then we need to simulate this year..
    % Store cell prior to application of rule, in case it is needed for
    % rule reconsideration
    Cell_save = Cell;
    % Busy hour size
    BHcou = size(Sim.dem.hourDistri, 1);
    
    interfIequi = shiftdim(repmat(min(round((...
        Sim.linBud.PRButilisCovera+1./Sim.utilisN./2) ./ ...
        (1./Sim.utilisN)),Sim.utilisN),[1,BHcou,length(Cell)]),1);
    
    % Find the cells that need be updated/refreshed
    if Sim.forcNetwCranIn == Sim.yea.thisYea
        % If in this year the sites chains switch from DRAN to CRAN then apply this switch.
        n_ = find(Sim.yea.thisYea == [Cell.expiYea] | ...
            ismember({Cell.pwrAmpClas},Sim.forcNetwCranTyp));
    else
        % Sites are refreshed even when not needed.
        n_ = find(Sim.yea.thisYea == [Cell.expiYea]);  %HO: check if the expiry year of the Cells is due or not ->  %HO: find the indices of these expired Cells.
    end
    
    % There are improvements necessary or to be made
    if ~isempty(n_) && ~Sim.optimis.decomm %@HO: not sure what is Sim.optimis.decomm?? This is defined in main5N (line 119). this means whether to throw away of site, at the end we don't allow decomissioning at all in 5G NORMA. so ~Sim.optimis.decomm is true all the time for 5G NORMA. @HO: TODO: get rid of ~Sim.optimis.decomm in the condition here.. As this is always true..
%         fprintf(1,['Refreshing ' num2str(length(n_)) ' sites']);
        
        % Initiliase the improvement list
        impro = struct('operato',[],'rulID',[],'I',[],'J',[],'CellIND',[]);
        
        isDecomm = find([Sim.rul.isDecomm]);  %@HO: not sure what Sim.rul.isDecomm is? Probably this is filled in Cell.m. @HO: talk to KK. -> these are the indices of the "rules" to be decomm.. (not indices of Cell but to rules..). Note that isDecomm in this case means Cells that can be refreshed. TODO: change the name of this variable..
        for n = 1 : length(n_) %HO: n_ are the indices of expired cells
            impro(n).operato = Cell(n_(n)).Operator;
            curre = struct('sitChainConfig',Cell(n_(n)).sitChaiConfig,...
                'sectoCou',Cell(n_(n)).sectoCou,...
                'AeCou',Cell(n_(n)).AeCou,...
                'Band',Cell(n_(n)).band.capa);  %HO: Getting the features of this current Cell. This parameter is just used in few lines later to get the rulID for this improvement
            
            if Sim.forcNetwCranIn == Sim.yea.thisYea
                impro(n).rulID = find([Sim.rul.ValidFrom] <= Sim.yea.thisYea & ...
                    Sim.yea.thisYea <= [Sim.rul.ValidTo] & ...
                    strcmp(DataHash(curre),{Sim.rul.curreHash}) & ...
                    ~[Sim.rul.isDecomm] & ...
                    arrayfun(@(x) x.upg.sitChainConfig==2,Sim.rul.') & ...
                    arrayfun(@(x) all(x.curre.Band==x.upg.Band),Sim.rul.'));
            else
                impro(n).rulID = isDecomm(...
                    [Sim.rul(isDecomm).ValidFrom] <= Sim.yea.thisYea & ... %HO: Take ValidFrom rules for isDecomm that are less than this year (e.g. 2017). KK: Note that isDecomm in this case means refresh.. Note that for certain years (e.g. 2030) rulID can be multidimensional and more complicated..
                    Sim.yea.thisYea <= [Sim.rul(isDecomm).ValidTo] & ... %HO: and Take ValidTo rules (isDecomm) that are bigger (or equal) to this year
                    strcmp(DataHash(curre),{Sim.rul(isDecomm).curreHash}) & ...
                    strcmp(DataHash(curre),{Sim.rul(isDecomm).upgHash})); %HO: and Take the rules that have the same DataHash of this Cell (curre)
            end
            assert(length(impro(n).rulID)==1)
            impro(n).I = Cell(n_(n)).I;
            impro(n).J = Cell(n_(n)).J;
            impro(n).CellIND = n_(n);
        end

        % Apply improvement
        [Cell,edgCloSit,interfIequi,bund1] = apply_impro(Cell,edgCloSit,...
            interfIequi,impro,Sim,studyA,clu,ter,[],bund1);

        clear n_ n curre impro isDecomm
        
%         fprintf(1,'DONE\n');
    end
    
    % Serve demand with site chains
    fprintf(1,'Serving the demand...');
    isCell0 = true(size(Cell));
    isXtunab = true(size(Cell)).';
    triPoi = 1;
    [Cell,thisDem,interfIequi] = serv_dem(Cell,thisDem,Sim,studyA,...
        SINR2tput,clu,false,isCell0,interfIequi,isXtunab,triPoi,false,bund1);
    Cell = Cell.c_userDyn(Sim);
    edgCloSit = edgCloSit.upd_coreCou(Cell,Sim);
    fprintf(1,'DONE\n');

    % Calculate the cost
    for n = 1 : length(Cell)
        Cell(n) = Cell(n).c_cost(Sim);
    end
    for n = 1 : length(edgCloSit)
        edgCloSit(n) = edgCloSit(n).c_cost(Cell,Sim);
    end

    % Calculate SINR statistics
    if ~isempty(thisDem)
        SINR_DL = [thisDem(studyA.INfoc([thisDem.IND])).SINR_DL];
        SINR_DL = reshape(SINR_DL(:),BHcou,size(Sim.demTranslati,1),[]);
        M = [{'SINR  (dB)'};repmat({[]},4,1);num2cell((-10:80).')];  % cell matrix to be written in csv
        for n = 1 : size(Sim.demTranslati,1)
            counts = hist(squeeze(SINR_DL(:,n,:)).',-10:80);
            M = [M [repmat(Sim.demTranslati(n,1),1,BHcou)
                repmat(Sim.demTranslati(n,4),1,BHcou)
                repmat(Sim.demTranslati(n,6),1,BHcou)
                Sim.dem.hourDistri(:,1).'
                Sim.dem.hourDistri(:,2).'
                num2cell(bsxfun(@rdivide,counts,sum(counts,1)))]];
        end
        cell2csv(fullfile('Outp',['SINR bef refre ' ...
            num2str(Sim.yea.thisYea) '.csv']),M)
    end
    
    % Define which set of InP tries to serve which SeP
    if Sim.yea.thisYea >= Sim.shari.from
        if all(Sim.shari.is(:))
            % Any InP can accommodate any service
            InP_ = {1:length(Sim.dem.operators)};
            tenan_ = InP_;
            proc = true;
        elseif all(all(Sim.shari.is==eye(size(Sim.shari.is),'logical'),2),1)
            % Each InP is associated with one service
            InP_ = num2cell(1:length(Sim.dem.operators));
            tenan_ = InP_;
            proc = true;
        elseif all(Sim.shari.is(1,:)==1,2) && ...
                all(all(Sim.shari.is(2:end,:)==0,2),1)
            % The first InP can accommodate any service
            InP_ = {1};
            tenan_ = {1:length(Sim.dem.operators)};
            proc = true;
        else
            proc = false;
        end
        if ~proc
            beep
            disp('Manually enter')
            keyboard
            InP_ = {[1 2]};
            tenan_ = {[1 2]};
        end
    else
        InP_ = num2cell(1:length(Sim.dem.operators));
        tenan_ = InP_;
    end
    
    % []
    % The unserved demand in this year
%     unserv = c_unserv(Sim,thisDem,studyA,'unserv','per pixel');
%     filepart2 = ['bef refre impro ' num2str(Sim.yea.thisYea)];
%     plot_map_unserv(Sim,Cell,studyA,thisDem,unserv,'foc+buf',...
%         'Before refresh improvements',filepart2)

    %% Other improvements as needed
    % The unserved demand in this year
    unserv = c_unserv(Sim,thisDem,studyA,'unserv','per pixel');

    % Remove 67% of the demand, as we assume that this is served by the
    % macrocell layer
    [B,I] = sort(unserv);
    k = find(cumsum(B)./sum(B)>2./3,1);
    for dn = I(1:k).'
        thisDem(dn).satis(:) = true;
    end
    clear B I k dn
    
%     sz1 = size(Sim.dem.hourDistri,1);     % hours in a day
%     sz4 = length(Sim.dem.famUseCase_ );   % families of use cases (e.g. MBB, etc..)
%     p_dem_heatm(sz1,sz4,Sim,studyA,thisDem)
%     clear sz1 sz4
    
    % The unserved demand in this year
    unserv = c_unserv(Sim,thisDem,studyA,'unserv','per pixel');
    
    filepart2 = ['bef non refre impro ' num2str(Sim.yea.thisYea)];
    plot_map_unserv(Sim,Cell,studyA,thisDem,unserv,'foc+buf',...
        'Before non-refresh improvements',filepart2)
    Cell.p_map_utilis(studyA,Sim,filepart2)
    
    [Cell,edgCloSit,thisDem,interfIequi,unserv2,bund1] = find_impro(Cell,...
        edgCloSit,thisDem,interfIequi,unserv,Sim,studyA,clu,ter,...
        SINR2tput,Cell_save,InP_,tenan_,bund1,dsm,SlLigDat);

    % Demand not served (decimal fraction)
    resu = resu.c_demUnservPerc(Sim,unserv2);

    % [Title]
    % More PROBES (boxplots)
    filepart2 = ['aft non refre impro (pix sho unserv start of yea) ' ...
        num2str(Sim.yea.thisYea)];
    plot_map_unserv(Sim,Cell,studyA,thisDem,unserv,'foc+buf',...
        'After non-refresh improvements (pixels show unserved start of year)',filepart2)
    Cell.p_map_utilis(studyA,Sim,filepart2)
    Cell.p_map_interfIequi(studyA,Sim,interfIequi,filepart2)
    Cell.p_box_utilis_Python(interfIequi,Sim,filepart2)
    Cell.p_serv_dem_per_sit_Python(studyA,thisDem,Sim,filepart2)
    filepart2 = ['aft non refre impro (pix sho unserv end of yea) ' ...
        num2str(Sim.yea.thisYea)];
    % Plot unserved pixels
    plot_map_unserv(Sim,Cell,studyA,thisDem,...
        c_unserv(Sim,thisDem,studyA,'unserv','per pixel'),'foc+buf',...
        'After non-refresh improvements (pixels show unserved end of year)',filepart2)
    % Plot served pixels
    filepart2 = ['aft non refre impro (pix sho serv end of yea) ' ...
        num2str(Sim.yea.thisYea)];
    plot_map_serv_pix(Sim,Cell,studyA,thisDem,...
        c_unserv(Sim,thisDem,studyA,'unserv','per pixel'),'foc+buf',...
        'After non-refresh improvements (pixels show unserved end of year)',filepart2)
%    Cell.p_box_utilis(interfIequi,Sim,filepart2)
%    Cell.p_serv_dem_per_sit(studyA,thisDem,Sim,filepart2)
%     Cell.c_ra(studyA,Sim,thisDem,true,1:length(Cell))
    
%     fprintf(1,'Plotting service maps...');
%     Cell.p_map_serv_GE(studyA,Sim,thisDem,true,1:length(Cell))
%     fprintf(1,'DONE\n');

%     plot_map_edgCloSit(Sim,Cell,edgCloSit,studyA)
%     filename = fullfile('OutpM',...
%         ['plot_map_edgCloSit ' num2str(Sim.yea.thisYea) '.jpg']);
%     export_fig(filename,'-jpg','-q100','-r300')
% %     print(gcf,fullfile('OutpM',...
% %         ['plot_map_edgCloSit ' num2str(Sim.yea.thisYea)]),'-djpeg','-r300')
%     delete(gcf)
    
    % Report % of population with band and throughput
    if ~isempty(thisDem)
        resu = resu.c_servPerc(Sim,thisDem,BHcou,dem);
    end

    % Report BW use per governorate and area
    resu = resu.c_BWperGovernora(studyA,Cell,Sim);
    switch Sim.SA.name
      case 'West Bank'
        resu = resu.c_BWperOslo(studyA,Cell,Sim); %HO: error when number of sites is high
      case 'Gaza'
      otherwise
        error('Undefined')
    end
    
    save(['OutpAux\' Hash '.mat'],'Cell','edgCloSit','resu','bund1')
    
    % Output more PROBES (HO)
    if ~isempty(thisDem)
        msg_file_title = 'After All Improvements';
        c_SINR_stat(Sim,thisDem,studyA,msg_file_title);  % Output SINR statistics
        c_dem_serv_Cel(Sim,thisDem,Cell,msg_file_title);  % Output demand per Cell
        c_Tput_per_Tech(Sim,thisDem,Cell,msg_file_title);  % Output Throughput per technology (Single user, coverage)
        c_Tput_Cap_Tech(Sim, thisDem, Cell, msg_file_title) % Output CAPA Throughput per technology (actual tput, capacity)
    end
    
    demServPerSit = cellfun(@(x) c_unserv(Sim,thisDem,studyA,'serv',...
        'per site',Cell,x),num2cell(1:length(Cell)).');
    if nnz(demServPerSit==0)./length(demServPerSit) > 5
        beep
        disp('There is a considerable number of sites that do not serve demand.')
        disp('Investigate why!')
    end
end
