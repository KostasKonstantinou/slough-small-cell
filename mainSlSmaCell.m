function mainSlSmaCell(configFile)
%% [Title]
% [Description]

%% Create output folders and release file
% Create output folders
if ~exist(fullfile(pwd,'OutpM'),'file')
    mkdir('OutpM')
end
if ~exist(fullfile(pwd,'Outp'),'file')
    mkdir('Outp')
end
if ~exist(fullfile(pwd,'OutpAux'),'file')
    mkdir('OutpAux')
end

%% Simulation Parameters
% Construct SIM object
Sim = SIM();

% Set hardcoded parameters
Sim = Sim.s_parame;

% This is the release number for this code (a module global variable).
Sim = Sim.s_rele('rele_0_1_0');

%% Add paths
addpath(genpath(fullfile(Sim.path.prog,'Library')))
addpath(genpath(fullfile(Sim.path.prog,'dem modu')))
addpath(genpath(fullfile(Sim.path.prog,'propag modu')))
py_addpath(fullfile(Sim.path.prog))
py_addpath(fullfile(Sim.path.prog,'Library'))

% Set parameters related to the config file
fprintf('Reading config file...');
Sim = Sim.read_inp(configFile);
fprintf('DONE\n');

% Set the PL database
Sim = Sim.pathlossDB;

%% Create release file
fileID = fopen(fullfile(fullfile(pwd,'Outp'),'release.txt'),'w');
fprintf(fileID,['Release ' Sim.rele '\n']);
fclose(fileID);

%% Create the studyA object
fprintf('Creating the study area...');
studyA = STUDYA(Sim);
fprintf('DONE\n');

%% Read clutter database
fprintf('Reading the clutter database...');
clu = CLU(Sim,studyA);
fprintf('DONE\n');
% clu.c_stats(studyA)
% clu.p_map(studyA)

%% Read DSM database
fprintf('Reading the clutter database...');
dsm = DSM(Sim,studyA);
% dsm.c_stats(studyA)
% dsm.p_map(studyA)
fprintf('DONE\n');

%% Set ONland studyA field
% studyA = studyA.s_ONland(clu);

%% Read meteorological data
% Sim = Sim.s_meteor(studyA);

%% Read terrain database
% Note that terrain is not used by the Hata model, but path loss models
% that have distirnct branches of LoS/NLos effectively use it.
fprintf('Reading the terrain database...');
switch Sim.propag.mod
    case 'ITU_1812_4'
        % Use the actual terrain data
        ter = TER(Sim,studyA);
    case 'Extended_HATA'
        % Do not use the terrain data
        ter.R = clu.R;
        ter.A = zeros(size(clu.A));
end
fprintf('DONE\n');
% ter.p_map(studyA);

%% Plot clutter map
dsm.p_clutter_map(ter,studyA,Sim);

%% Create demand points in the simulation grid
% Print
fprintf(1,'Creating demand points in the simulation grid...');

% dem is an object 
% A, w, ...
% (@HO: are these the addresses or population?) It looks like these are the
% DELR (residential addresses), DELN (small businesses) and DELL (large
% businesses). @HO: why the add.pop has decimal values? -> this is
% calculate from CEnsus data then we have decimal number (we don't round
% this)

% (@HO: why are we adding pop, DELR, DELN and DELL? are we not just
% considering the demand outdoor on throuphare?) -> for MBB we distribute
% demand at throrou point however for MTC we use addresses from DELR, DELN
% and DELL

% Read address database and populate demand source object 1-4
add = read_addresses(Sim,studyA);
%
IND = ~isnan(add.pop);
[I,J] = studyA.R.worldToDiscrete(add.X(IND),add.Y(IND));
dem(1).A = sparse(I,J,add.pop(IND),...
    studyA.R.RasterSize(1),studyA.R.RasterSize(2));
dem(1).layNam = 'popu';
% 
IND = ~isnan(add.DELR);
[I,J] = studyA.R.worldToDiscrete(add.X(IND),add.Y(IND));
dem(2).A = sparse(I,J,add.DELR(IND),...
    studyA.R.RasterSize(1),studyA.R.RasterSize(2));
dem(2).layNam = 'DELR';
%
IND = ~isnan(add.DELN);
[I,J] = studyA.R.worldToDiscrete(add.X(IND),add.Y(IND));
dem(3).A = sparse(I,J,add.DELN(IND),...
    studyA.R.RasterSize(1),studyA.R.RasterSize(2));
dem(3).layNam = 'DELN';
%
IND = ~isnan(add.DELL);
[I,J] = studyA.R.worldToDiscrete(add.X(IND),add.Y(IND));
dem(4).A = sparse(I,J,add.DELL(IND),...
    studyA.R.RasterSize(1),studyA.R.RasterSize(2));
dem(4).layNam = 'DELL';
%
clear add

% Create thorou (thoroughfare) demand points
% eMBB Hamburg - roads
% V2I infotainment - roads
% V2I driver information - roads
% V2I assisted driving - roads
% intelligent transport systems (ITS)
% tmp = read_road_rail(studyA,Sim,clu);
tmp = read_road_rail(studyA,Sim);

% Store service roads as allowed places for new sites (514X, 515X)
% 511X seem to be motorway
% 512X seem to be roads
% 513X seem to be links
% 514X seem to be tracks
% 515X seem to be pedestrian
% 519X seem to be unknown
isWzero = any(bsxfun(@eq,fix([tmp.code]./10),[514;515]),1);
Sim = s_construct(Sim,tmp,isWzero,studyA);
tmp(isWzero) = [];
clear isWzero

% Append to demand source object layers 5-9.
% tmp2_ is a Mx2 cell matrix, with the first column the tags of
% thoroughfare types, and the second column the code values of
% OpenStreetMap. The association of which thoroughfare type corresponds to
% which code is user-defined, see table above for an index to the code
% values used by OpenStreepMap; note that these may change from
% OpenStreetMap.
tmp2_ = {'Motorway' [5111;5131]
    'A road' [5112;5113;5132;5133;5114]
    'B road' [5115;5134;5135;5141]
    'Minor road' [5121;5122;5123;5124;5142;5143;5144;5145;5146;5147;5151;5153;5154;5155;5199]
    'Railway' [6101;6102;6103]};
for n = 5 : 9
    tmp2 = tmp2_{n-4,2};
    IND = any(bsxfun(@eq,[tmp.code],tmp2),1);
    sz = [size(tmp(1).A,1) size(tmp(1).A,2) nnz(IND)];
    dem(n).A = sparse(sum(reshape(full([tmp(IND).A]),sz),3));
    dem(n).layNam = tmp2_{n-4,1};
end

% Append to demand source object layer 10
dem(10).A = sparse(studyA.R.RasterSize(1),studyA.R.RasterSize(2));
dem(10).layNam = 'Tunnelled railway';

% Add X Y environment
dem(5).X = tmp(1).X;
dem(5).Y = tmp(1).Y;

% Clear variables
clear tmp tmp2 IND sz n

% Update demand source object with weights
% dem = c_w2(dem,Sim,studyA);
for n = 1 : length(dem)
    dem(n).w = double(dem(n).A>0);
end
clear n

% S = shaperead(fullfile(Sim.path.dr,'DB','Germany',...
%     'hamburg-latest-free.shp','gis.osm_traffic_free_1'));
% [C,ia,ic] = unique([S.code].');
% counts = hist([S.code],C);
% [num2cell(C) {S(ia).fclass}.' num2cell(counts.')]
% clear S C ia ic
% 'gis.osm_water_a_free_1'
% 'gis.osm_traffic_free_1'  % for traffic signals
% 'gis.osm_traffic_a_free_1'  % for parking
% 'gis.osm_pofw_free_1'  % Church
% 'gis.osm_natural_a_free_1'  % for beach
% 'gis.osm_natural_free_1'  % for trees

% Create HPA land demand points
% [I,J] = find(studyA.INhpa);
% [X,Y] = studyA.R.intrinsicToWorld(J,I);
% [cluI,cluJ] = clu.R.worldToDiscrete(X,Y);
% isLan = clu.A(sub2ind(size(clu.A),cluI,cluJ)) <= 34;  % is land
% I(~isLan) = [];
% J(~isLan) = [];
% % figure;plot(J,I,'.'); axis equal; set(gca,'ydir','reverse')
% rng(1424)
% IND = round(rand(25,1).*(length(I)+1)-0.5);
% dem(1).A = sparse(I(IND),J(IND),1,...
%     studyA.R.RasterSize(1),studyA.R.RasterSize(2));
% dem(1).layNam = 'Connected traffic lights';

% Print
fprintf(1,'DONE\n');

% Addresses
% waste management
% Smart meters - address

% Area
% Environmental monitors

% HPA polygon - land
% Connected traffic lights (receiving sensor info)
% AR for port management and maintenance
% Wireless CCTV
% Logistics sensors - container
% Automated vehicles
% Automated port machinery

% HPA polygon - sea
% Environmental sensors

% HPA terminal
% High density eMBB hotspot - terminal

%% Find pixels where cells can be commisioned
Sim = Sim.s_construct2(dem,studyA);

%% Prepare locations for Edge Cloud Sites
% The locations of the BT Exchanges become the locations of the Edge Cloud
% Sites
% fprintf(1,'Preparing loacations for Edge Cloud Sites...');
edgCloSit = EDGCLOSIT(Sim,studyA);
% edgCloSit.p_map(studyA,Sim)
% fprintf(1,'DONE\n');

%% Update Sim with the pixels that need propagation estimate
col_Hata_branch = 5;  % 'Hata branch'
Sim = Sim.c_propag_pix(dem,studyA,clu,col_Hata_branch);
clear col_Hata_branch

%% Load existing network
% % Print progress text
% fprintf(1,'Loading the network...');
% 
% % Create antenna sites for the start of the simulation
% Cell = CELL('exi',Sim,studyA,edgCloSit,clu);
% Cell = Cell.set_co_ch;
% Cell = Cell.s_tilt(Sim);
% 
% % Calculate median path loss
% fn_ = any(cell2mat(arrayfun(@(x) x.band.supp,Cell.','UniformOutput',false)),1);
% Cell = run_propa_modu_med(Cell,Sim,true(1,length(Cell)),studyA,clu,ter,fn_);
% clear fn_
% 
% % Create edge cloud sites
% edgCloSit = edgCloSit.upd_connTowaClo(Cell,Sim);
% 
% % Plot map of antenna sites
% Cell.p_map(studyA,Sim)
% 
% % Print progress text
% fprintf(1,'DONE\n');

Cell = CELL.empty;

%% Calculate ISD of existing macrocell network
% IN = inpolygon([BSexi.X],[BSexi.Y],studyA.X,studyA.Y);
% ISD_m = calc_ISD('Ofcom 2.6 and 800 condoc',BSexi(IN));
% max(ISD_m)

%% Read Slough Lighting Data
SlLigDat = SLLIGDAT(Sim,studyA);
SlLigDat.p_SlLigDat(studyA,Sim);

%% Read COMB_SLOUGH
% M = m_shaperead(fullfile(Sim.path.on,'05 RW DB - General',...
%     'Slough','COMB_SLOUGH'));
% combSl.lat = M.ncst{1}(:,2);
% combSl.lon = M.ncst{1}(:,1);
% combSl.X = combSl.lon;
% combSl.Y = combSl.lat;
% IND = ~isnan(combSl.lat);
% [combSl.X(IND),combSl.Y(IND)] = latLon2easNor(combSl.lat(IND),...
%     combSl.lon(IND),[],Sim.mstruct.ostn);
% clear M
% 
% figure
% axis equal
% hold on
% plot(combSl.X,combSl.Y)

%% Build network for each year
optim_per_yea(Sim,dem,studyA,Cell,edgCloSit,clu,ter,dsm,SlLigDat);

% Close the file
fclose(Sim.path.logPropagFileID);
fclose(Sim.path.unservDemFileID);
fclose(Sim.path.servPopuThorouFileID);
fclose(Sim.path.BWperGovernora);
fclose(Sim.path.logDemCapaFileID);
switch Sim.SA.name
  case 'West Bank'
    fclose(Sim.path.BWperOslo);
  case 'Gaza'
  otherwise
    error('Undefined')
end

% Sim.split_cost
