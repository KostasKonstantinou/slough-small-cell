classdef STUDYA
    % STUDYA class for management of the study area
    % The STUDYA class is responsible for setting the properties of the
    % extent of the study area. More specifically it is responsible for
    % delivering the following:
    %
    % *	creates the STUDYA object
    % * more to be added

    properties
        X;
        Y;
        BoundingBox;
        BoundingBoxLatLon;
        SHAPE_Area;
        R;
        buf;
        ONland;
        
%         Builtuparea;
%         Governorates;
%         IsraeliLocalities;
%         Oslo;
%         Settlements;
        
        INfoc;
%         INGovernorates;
%         INBuiltuparea;
%         INIsraeliLocalities;
%         INOslo;
%         INSettlements;
        
%         INGovernoratesPer;
%         INBuiltupareaPer;
%         INIsraeliLocalitiesPer;
%         INOsloPer;
%         INSettlementsPer;

        bordSimplif;  % Border simplified
        IN250;
        IN2000;
        residuErr;  % residual noise rise wrt linear fit, due to non-coordination
    end
    
    methods
%% Contructor of study area object
function studyA = STUDYA(Sim)
%% Create studyA object
% create_studyA is the constructor of the study area object, studyA. This
% function should at some point be part of a class. studyA.X and .Y are the
% coordinates of the study area, which coincide with the land borders.
% 
% Sim is a structure and must contain the following fields:
% path.ProgramData - The path to program data
% 
% clu is a CLU object and is used to determine if the pixel is on land
% 
% The output structure smaAre contains the following fields:
% X - X-coordinates of the polygons
% Y - Y-coordinates of the polygons
% BoundingBox - Bounding box
% BoundingBoxLatLon - Bounding box in geographic coordinates
% SHAPE_Area - Shape area
% R - map.rasterref.MapCellsReference object
% INfoc - Logical matrix (MxN) that is true for pixels within focus
% INhpa - Logical matrix (MxN) that is true for pixels within HPA
% INham - Logical matrix (MxN) that is true for pixels within Hamburg City
% INcruShipTer1 - As above for cruise ship terminal 1
% INcruShipTer2 - As above for cruise ship terminal 2
% INcruShipTer3 - As above for cruise ship terminal 3
% ONland - Logical matrix (MxN) that is true for pixels on land
% Where MxN is hte size of the raster, i.e. [M,N] = studyA.R.RasterSize
% 
% buf is a sub-structure relating to the buffer and contains the following
% fields:
% X - X-coordinates of the polygons
% Y - Y-coordinates of the polygons
% BoundingBox - Bounding box
% BoundingBoxLatLon - Bounding box in geographic coordinates
% SHAPE_Area - Shape area
% 
% HPA is a sub-structure relating to Hamburg Port Authority and contains
% the following fields:
% X - X-coordinates of the polygons
% Y - Y-coordinates of the polygons
% BoundingBox - Bounding box

% cruShipTer is an arreay of sub-structures relating to the terminals and
% each containing the following fields:
% X - X-coordinates of the polygons
% Y - Y-coordinates of the polygons
% BoundingBox - Bounding box
% 
% Ham is a sub-structure relating to Hamburg Port Authority and contains
% the following fields:
% X - X-coordinates of the polygons
% Y - Y-coordinates of the polygons
% BoundingBox - Bounding box

% Load or create the study area object
if exist(fullfile(Sim.path.ProgramData,'studyA.mat'),'file')
    % Load the precalculated study area
    load(fullfile(Sim.path.ProgramData,'studyA.mat'),'studyA')
else
    %% London boroughs
    % % X and Y properties
    % studyA.X = [];
    % studyA.Y = [];
    % S = shaperead( Sim.boundDBpath );
    % for n = 1 : length(Sim.SA.district_borough_unitary_region)
    %     IND = strcmp(Sim.SA.district_borough_unitary_region{n},{S.NAME});
    %     if nnz(IND) == 1
    %         [studyA.X,studyA.Y] = polybool('union',studyA.X,studyA.Y,...
    %             S(IND).X,S(IND).Y);
    %     else
    %         error('Choose only from:')
    %         disp({S.NAME}.')
    %     end
    % end
    % clear S n
    % 
    % % Create grid of study area
    % XLimWorld = [floor(min(studyA.buf.X)./Sim.SA.pixSize) ...
    %     ceil(max(studyA.buf.X)./Sim.SA.pixSize)] .* Sim.SA.pixSize;
    % YLimWorld = [floor(min(studyA.buf.Y)./Sim.SA.pixSize) ...
    %     ceil(max(studyA.buf.Y)./Sim.SA.pixSize)] .* Sim.SA.pixSize;
    % RasterSize = [diff(YLimWorld) diff(XLimWorld)] ./ Sim.SA.pixSize;
    % studyA.R = maprasterref('RasterSize',RasterSize,'YLimWorld',YLimWorld,...
    %     'ColumnsStartFrom','north','XLimWorld',XLimWorld);
    % clear XLimWorld YLimWorld RasterSize
    % 
    % % Define pixels within focus
    % X = studyA.R.XWorldLimits(1) + studyA.R.CellExtentInWorldX./2 : ...
    %     studyA.R.CellExtentInWorldX : ...
    %     studyA.R.XWorldLimits(2) - studyA.R.CellExtentInWorldX./2;
    % Y = studyA.R.YWorldLimits(2) - studyA.R.CellExtentInWorldY./2 : ...
    %     -studyA.R.CellExtentInWorldY : ...
    %     studyA.R.YWorldLimits(1) + studyA.R.CellExtentInWorldY./2;
    % [X,Y] = meshgrid(X,Y);
    % studyA.INfoc = inpolygon(X,Y,studyA.X,studyA.Y);
    % 
    % % figure
    % % plot(studyA.X,studyA.Y)
    % % hold on
    % % plot(studyA.buf.X,studyA.buf.Y)
    % % axis equal
    % 
    % % figure
    % % imagesc(studyA.R.XWorldLimits,studyA.R.YWorldLimits,studyA.INfoc)
    % % set(gca,'ydir','normal')
    % % axis equal
    
    %% Hamburg
%     % Load Hamburg polygon
%     tmp = shaperead(Sim.path.HPApoly);
%     S = struct('X',{tmp.X}.','Y',{tmp.Y}.','tag','HPApoly');
%     tmp = shaperead(Sim.path.cruShipTerPoly);
%     S = [S; struct('X',{tmp.X}.','Y',{tmp.Y}.','tag','cruShipTerPoly')];
%     for n = 1 : length(S)
%         [S(n).lat,S(n).lon] = minvtran(Sim.mstruct,S(n).X,S(n).Y);
%     end
%     clear tmp
% 
%     % Find union of HPA
%     [xHPA,yHPA] = polyunion([S.X],[S.Y],'potentially overlapping');
%     [yHPA,xHPA] = polyjoin(yHPA,xHPA);
% 
%     % Draw a box for Hamburg city
%     % For the selection of the hardcoded coordinates look at:
% %     \09 WP6-Verfication\05 T6-4\04 Study item 2 - demand distribution\Sim area definition
%     lly = 53 + 32./60 + 00./3600;
%     llx = 9 + 57./60 + 00./3600;
%     ury = 53 + 34./60 + 30./3600;
%     urx = 10 + 2./60 + 00./3600;
%     Longitude = [llx.*ones(1,5) llx:30./3600:urx urx.*ones(1,5) urx:-30./3600:llx];
%     Latitude = [lly:30./3600:ury ury.*ones(1,10) ury:-30./3600:lly lly.*ones(1,10)];
%     % figure
%     % plot(Longitude,Latitude)
%     % Longitude = [llx llx urx urx llx];
%     % Latitude = [lly ury ury lly lly];
%     [xHam,yHam] = mfwdtran(Sim.mstruct,Latitude,Longitude);
% 
% %     figure
% %     hold on
% %     plot(xHPA{1},yHPA{1})
% %     plot(xHam,yHam)
% 
%     % Union of HPA and Hamburg city
%     [x,y] = polyunion([xHPA.' nan xHam],[yHPA.' nan yHam],...
%         'potentially overlapping');
%     [y,x] = polyjoin(y,x);
% 
% %     figure
% %     plot(x,y)
% 
%     % Prepare object to be saved as shapefile
%     obj = struct('ID',num2cell(1:2),'X',{x,xBuf},'Y',{y,yBuf},...
%         'Geometry','Polygon');
% 
%     % Set BoundingBox
% %     BoundingBox = [min(xBuf) min(yBuf)
% %         max(xBuf) max(yBuf)];
% %     studyA.BoundingBox = BoundingBox;
% 
%     % Prepare structure to be written - at least these have to be in teh fieldnames
% %     [obj.ID] = deal(ID{:});  % Index of the objects
% %     [obj.X] = deal(X{:});
% %     [obj.Y] = deal(Y{:});
% %     [obj.Geometry] = deal('Polygon');
%     
%     % Create separate shapefiles for focus and buffer
%     for n = 1 : length(obj)
%         % DBF specification from the geographic data structure
%         dbfspec = makedbfspec(obj(n));
% 
%         % Create filename
%         switch n
%             case 1
%                 filepart1 = fullfile(Sim.path.ProgramData,'studyA');
%             case 2
%                 filepart1 = fullfile(Sim.path.ProgramData,'studyAplusBuf');
%         end
% 
%         % Now write the shapefile
%         shapewrite(obj(n),filepart1,'DbfSpec',dbfspec)
%         
%         % Write the projection file
%         prj = ['PROJCS["ETRS89 / UTM zone 32N",' ...
%             'GEOGCS["ETRS89",' ...
%             'DATUM["D_European Terrestrial Reference System 1989",' ...
%             'SPHEROID["GRS_1980",6378137.0,298.257222101]],' ...
%             'PRIMEM["Greenwich",0],' ...
%             'UNIT["Degree",0.017453292519943295]],' ...
%             'PROJECTION["Transverse_Mercator"],' ...
%             'PARAMETER["central_meridian",9.0],' ...
%             'PARAMETER["latitude_of_origin",0.0],' ...
%             'PARAMETER["scale_factor",0.9996],' ...
%             'PARAMETER["false_easting",500000.0],' ...
%             'PARAMETER["false_northing",0.0],UNIT["m",1.0]]'];
%         fid = fopen([filepart1 '.prj'],'wt'); % Open for writing
%         fprintf(fid,'%s',prj);
%         fclose(fid);
%     end
%     clear n
% 
% %     S = shaperead(fullfile(Sim.path.ProgramData,'studyA'));

    %% Palestine
%     % Read the geography
%     studyA.Builtuparea = shaperead(Sim.path.Builtuparea);
%     studyA.Governorates = shaperead(Sim.path.Governorates);
%     studyA.IsraeliLocalities = shaperead(Sim.path.IsraeliLocalities);
%     studyA.Oslo = shaperead(Sim.path.Oslo);
%     studyA.Settlements = shaperead(Sim.path.Settlements);
% %     studyA.Towers = shaperead(Sim.path.Towers);
% 
%     % Remove duplicate entries
%     studyA.Settlements([162 168:174]) = [];
%     
%     % All the geography layers were extracted by QGIS with Palestinian Belt
%     % coordinate system, apart from Builtuparea which was revised by OQ and
%     % sent as a WGS84 shapefile.
%     [x,y] = mfwdtran(Sim.mstruct,...
%         [studyA.Builtuparea.Y],[studyA.Builtuparea.X]);
%     x = mat2cell(x,1,cellfun(@length,{studyA.Builtuparea.X}));
%     y = mat2cell(y,1,cellfun(@length,{studyA.Builtuparea.Y}));
%     [studyA.Builtuparea.X] = deal(x{:});
%     [studyA.Builtuparea.Y] = deal(y{:});
%     clear x y
%     
%     % Union of Governorates
%     switch Sim.SA.name
%       case 'West Bank'
%         k = setdiff(1:16,[2,3,6,7,10]);
%       case 'Gaza'
%         k = [2,3,6,7,10];
%       case 'Settlements'
%       otherwise
%         error('Undefined')
%     end
%     switch Sim.SA.name
%       case {'West Bank','Gaza'}
%         [x,y] = polyunion([studyA.Governorates(k).X],...
%             [studyA.Governorates(k).Y],'potentially overlapping');
%       case 'Settlements'
%         [x,y] = polyunion([studyA.Settlements.X],...
%             [studyA.Settlements.Y],'potentially overlapping');
%       otherwise
%         error('Undefined')
%     end
%     
%     % If West Bank, then clip to Oslo
%     switch Sim.SA.name
%       case 'West Bank'
%         RefPol = struct('x',x,'y',y,...
%             'hole',num2cell(double(~ispolycw(x,y))));
%         [ClipPolX,ClipPolY] = polyunion([studyA.Oslo.X],...
%             [studyA.Oslo.Y],'non overlapping');
%         ClipPol = struct('x',ClipPolX,'y',ClipPolY,...
%             'hole',num2cell(double(~ispolycw(ClipPolX,ClipPolY))));
%         OutPol = PolygonClip(RefPol,ClipPol);
%         A = arrayfun(@(x) polyarea(x.x,x.y),OutPol);
%         OutPol(A<3) = [];
%         [x,y] = PolygonClip2ml(OutPol,'XY');
%       case {'Gaza','Settlements'}
%       otherwise
%         error('Undefined')
%     end
%     
%     [y,x] = polyjoin(y,x);
% %     figure
% %     plot(x,y)
% 
%     % Remove settlement areas
%     switch Sim.SA.name
%       case 'West Bank'
%         [Ycells,Xcells] = polysplit(y,x);
%         RefPol = struct('x',Xcells,'y',Ycells,...
%             'hole',num2cell(double(~ispolycw(Xcells,Ycells))));
%         [Ycells,Xcells] = polysplit([studyA.Settlements.Y],...
%             [studyA.Settlements.X]);
%         ClipPol = struct('x',Xcells,'y',Ycells,...
%             'hole',num2cell(double(~ispolycw(Xcells,Ycells))));
%         OutPol = PolygonClip(RefPol,ClipPol,0);
%         A = arrayfun(@(x) polyarea(x.x,x.y),OutPol);
%         OutPol(A<1) = [];
%         [x,y] = PolygonClip2ml(OutPol,'XY');
%         [y,x] = polyjoin(y,x);
%         [y,x] = closePolygonParts(y,x);
%       case {'Gaza','Settlements'}
%       otherwise
%         error('Undefined')
%     end
% %     figure
% %     plot(x,y)
% %     axis equal

    %% Slough
    x = [497135 497135 498670 498670 497135].';
    y = [179226 180513 180513 179226 179226].';
    
    % Prepare object to be saved as shapefile
    obj = struct('ID',num2cell(1),...
        'X',{studyA.BoundingBox([1 1 2 2 1],1)},...
        'Y',{studyA.BoundingBox([1 2 2 1 1],2)},...
        'Geometry','Polygon');
    
    % Create separate shapefiles for focus and buffer
    for n = 1 : length(obj)
        % DBF specification from the geographic data structure
        dbfspec = makedbfspec(obj(n));

        % Create filename
        switch n
            case 1
                filepart1 = fullfile('OutpM\','studyA_BoundingBox');
            case 2
                filepart1 = fullfile(Sim.path.ProgramData,'studyAplusBuf');
        end

        % Now write the shapefile
        shapewrite(obj(n),filepart1,'DbfSpec',dbfspec)

        % Write the projection file
        prj = ['PROJCS["British_National_Grid",' ...
            'GEOGCS["GCS_OSGB_1936",DATUM["D_OSGB_1936",' ...
            'SPHEROID["Airy_1830",6377563.396,299.3249646]],' ...
            'PRIMEM["Greenwich",0],' ...
            'UNIT["Degree",0.017453292519943295]],' ...
            'PROJECTION["Transverse_Mercator"],' ...
            'PARAMETER["False_Easting",400000],' ...
            'PARAMETER["False_Northing",-100000],' ...
            'PARAMETER["Central_Meridian",-2],' ...
            'PARAMETER["Scale_Factor",0.999601272],' ...
            'PARAMETER["Latitude_Of_Origin",49],UNIT["Meter",1]]'];
        fid = fopen([filepart1 '.prj'],'wt'); % Open for writing
        fprintf(fid,'%s',prj);
        fclose(fid);
    end
    clear n
    
    %% studyA - foc, buf, and other components
    % If modelling settlements, then read the Israeli site database and 
    % ensure that these get included inside the study area raster.
    % Obviously some will be outside the focus area.
    switch Sim.SA.name
      case {'West Bank','Gaza'}
        S.X = [];
        S.Y = [];
      case 'Settlements'
        S = shaperead(Sim.path.Towers);
      otherwise
        error('Undefined')
    end
        
    % Focus or just study area
    studyA.X = x;
    studyA.Y = y;
    studyA.BoundingBox = [min([studyA.X;[S.X].']) min([studyA.Y;[S.Y].'])
        max([studyA.X;[S.X].']) max([studyA.Y;[S.Y].'])];
%     [lat,lon] = minvtran(Sim.mstruct,studyA.BoundingBox([1,1,2,2],1),...
%         studyA.BoundingBox([1,2,2,1],2));
%     studyA.BoundingBoxLatLon = [min(lon) min(lat)
%         max(lon) max(lat)];
    [ycells,xcells] = polysplit(studyA.Y,studyA.X);
    studyA.SHAPE_Area = sum(cellfun(@(x,y) polyarea(x,y),...
        xcells,ycells) .* (ispolycw(xcells,ycells).*2-1));
    
    %% London boroughs
    % % Calculate the buffer
    % ps = dpsimplify([studyA.X;studyA.Y].',Sim.dpsimplify.tol);
    % [xBuf,yBuf] = bufferm2('xy',ps(:,1),ps(:,2),Sim.SA.Buff);
    % clear ps
    
    %% Hamburg
%     % Calculate buffer.
%     % Function dpsimplify is used first to simplify the study area, because
%     % otherwise this step takes too long. The simplification tolerance is
%     % set to roughly the size of a pixel.
%     % Function bufferm2 computes buffer zone around a polygon.
%     ps = dpsimplify([x y],Sim.dpsimplify.tol);
%     [xBuf,yBuf] = bufferm2('xy',ps(:,1),ps(:,2),Sim.SA.Buff);
% 
% %     figure
% %     hold on
% %     plot(x,y,'k')
% %     plot(xBuf,yBuf,'k--')    
    
    %% Palestine
    % Calculate buffer.
%     xBuf = studyA.BoundingBox([1;1;2;2;1],1);
%     yBuf = studyA.BoundingBox([1;2;2;1;1],2);
    
    %% Slough
    % Calculate buffer.
    xBuf = x;
    yBuf = y;

    %% 
    % Buffer
    studyA.buf.X = xBuf;
    studyA.buf.Y = yBuf;
    studyA.buf.BoundingBox = [min(studyA.buf.X) min(studyA.buf.Y)
        max(studyA.buf.X) max(studyA.buf.Y)];
    if isfield(Sim.mstruct,'ostn')
        [lat,lon] = easNor2latLon(studyA.buf.BoundingBox([1,1,2,2],1),...
            studyA.buf.BoundingBox([1,2,2,1],2),Sim.mstruct.ostn);
    elseif isfield(Sim.mstruct,'mapprojection')
        [lat,lon] = minvtran(Sim.mstruct,studyA.buf.BoundingBox([1,1,2,2],1),...
            studyA.buf.BoundingBox([1,2,2,1],2));
    else
        error('Code this!')
    end
    studyA.buf.BoundingBoxLatLon = [min(lon) min(lat)
        max(lon) max(lat)];
    [ycells,xcells] = polysplit(studyA.buf.Y,studyA.buf.X);
    studyA.buf.SHAPE_Area = sum(cellfun(@(x,y) polyarea(x,y),...
        xcells,ycells) .* (ispolycw(xcells,ycells).*2-1));

%     % HPA terminals
%     IND = find(strcmp({S.tag},'cruShipTerPoly'));
%     for n = 1 : length(IND)
%         studyA.cruShipTer(n).X = S(IND(n)).X.';
%         studyA.cruShipTer(n).Y = S(IND(n)).Y.';
%         studyA.cruShipTer(n).BoundingBox = [min(studyA.cruShipTer(n).X) min(studyA.cruShipTer(n).Y)
%             max(studyA.cruShipTer(n).X) max(studyA.cruShipTer(n).Y)];
%         
%         [ycells,xcells] = polysplit(studyA.cruShipTer(n).Y,...
%             studyA.cruShipTer(n).X);
%         studyA.cruShipTer(n).SHAPE_Area = sum(cellfun(@(x,y) polyarea(x,y),...
%             xcells,ycells) .* (ispolycw(xcells,ycells).*2-1));
%         
%         % The name of the terminal is not included in the shapefile, thus
%         % inserted hardcoded here
%         switch n
%             case 1
%                 name1 = 'Altona';
%                 name2 = 'CTALTONA';
%             case 2
%                 name1 = 'Steinwerder';
%                 name2 = 'STEINW1';
%             case 3
%                 name1 = 'HafenCity';
%                 name2 = 'GRASBROW';
%             otherwise
%                 error('Undefined')
%         end
%         studyA.cruShipTer(n).name1 = name1;
%         studyA.cruShipTer(n).name2 = name2;
%     end
%     
%     % HPA AGV
%     AGV = struct('name',{'Altenwerder','Burchardkai',...
%         'Tollerort','Eurogate'},'service','AGV',...
%         'sheet',{'Altenwerder - automated vehicle',...
%         'Burchardkai - automated vehicle',...
%         'Tollerort - automated vehicles','Eurogate - automated vehicles'});
%     for n = 1 : length(AGV)
%         num = xlsread(Sim.path.automLoc,AGV(n).sheet);
%         [AGV(n).X,AGV(n).Y] = mfwdtran(Sim.mstruct,...
%             num(:,1),num(:,2));
%         [AGV(n).X,AGV(n).Y] = poly2ccw(...
%             AGV(n).X,AGV(n).Y);
%         
%         k = kml(fullfile(Sim.path.ProgramData,AGV(n).sheet));
%         k.plot(num(:,2),num(:,1),'altitude',0,...
%             'altitudeMode','clampToGround','lineWidth',10,...
%             'name',AGV(n).name);
%         k.save
%     end
%     studyA.AGV = AGV;
%     clear AGV
%     
%     % Hamburg city
%     [studyA.Ham.X,studyA.Ham.Y] = polybool('subtraction',xHam.',yHam.',...
%         xHPA,yHPA);
%     studyA.Ham.BoundingBox = [min(studyA.Ham.X) min(studyA.Ham.Y)
%         max(studyA.Ham.X) max(studyA.Ham.Y)];
%     [ycells,xcells] = polysplit(studyA.Ham.Y,studyA.Ham.X);
%     studyA.Ham.SHAPE_Area = sum(cellfun(@(x,y) polyarea(x,y),...
%         xcells,ycells) .* (ispolycw(xcells,ycells).*2-1));
%     
%     % HPA polygon including terminals
%     studyA.HPA.X = xHPA;
%     studyA.HPA.Y = yHPA;
%     studyA.HPA.BoundingBox = [min(studyA.HPA.X) min(studyA.HPA.Y)
%         max(studyA.HPA.X) max(studyA.HPA.Y)];
%     [ycells,xcells] = polysplit(studyA.HPA.Y,studyA.HPA.X);
%     studyA.HPA.SHAPE_Area = sum(cellfun(@(x,y) polyarea(x,y),...
%         xcells,ycells) .* (ispolycw(xcells,ycells).*2-1));
%     
%     % Create kml output
%     k = kml('Fixed-telephony exchanges');
%     %
%     ps = dpsimplify([studyA.Ham.X,studyA.Ham.Y],50);
%     [tmpX,tmpY] = bufferm2('xy',ps(:,1),ps(:,2),Sim.SA.Buff);
%     [lat,lon] = minvtran(Sim.mstruct,tmpX,tmpY);
%     BoundingBox = [min(lon) min(lat)
%         max(lon) max(lat)];
%     disp(BoundingBox)
%     %
%     k.plot(BoundingBox([1,1,2,2,1],1),BoundingBox([1,2,2,1,1],2),...
%         'altitudeMode','clampToGround','lineWidth',5,'name','foc+buf');
%     %
%     [lat,lon] = minvtran(Sim.mstruct,studyA.Ham.X,studyA.Ham.Y);
%     BoundingBox = [min(lon) min(lat)
%         max(lon) max(lat)];
%     disp(BoundingBox)
%     %
%     k.plot(BoundingBox([1,1,2,2,1],1),BoundingBox([1,2,2,1,1],2),...
%         'altitudeMode','clampToGround','lineWidth',5,'name','foc');
%     k.run;
%     clear ps tmpX tmpY lat lon
    
    %% studyA - maprasterref
    % Create raster of study area
    XLimWorld = [floor(min(studyA.buf.X)./Sim.SA.pixSize) ...
        ceil(max(studyA.buf.X)./Sim.SA.pixSize)] .* Sim.SA.pixSize;
    YLimWorld = [floor(min(studyA.buf.Y)./Sim.SA.pixSize) ...
        ceil(max(studyA.buf.Y)./Sim.SA.pixSize)] .* Sim.SA.pixSize;
    RasterSize = [diff(YLimWorld) diff(XLimWorld)] ./ Sim.SA.pixSize;
    studyA.R = maprasterref('RasterSize',RasterSize,'YLimWorld',YLimWorld,...
        'ColumnsStartFrom','north','XLimWorld',XLimWorld);
    clear XLimWorld YLimWorld RasterSize

    %% studyA - INfoc, INbuf, and other
    % Define pixels within focus
    % A pixel is in focus if more than 4 ninths of its samples are
    % within focus. This methodology is endorsed from image processing,
    % and is the methodology alos adopted by CORINE.
    % The columns are:
    % 1. Name of studyA field to populate with 
    % 2. String 'Per' if per polygon
    % 3. Polygon name
    tmp_ = {'INfoc' '' ''};
%         'INBuiltuparea' '' 'Builtuparea'
%         'INIsraeliLocalities' '' 'IsraeliLocalities'
%         'INSettlements' '' 'Settlements'
%         'INGovernoratesPer' 'Per' 'Governorates'
%         'INBuiltupareaPer' 'Per' 'Builtuparea'
%         'INIsraeliLocalitiesPer' 'Per' 'IsraeliLocalities'
%         'INOsloPer' 'Per' 'Oslo'
%         'INSettlementsPer' 'Per' 'Settlements'};
    
    % Create a denser raster so that each pixel contains 9 pixels.
    % Similar to: https://uk.mathworks.com/help/images/ref/poly2mask.html
    x = studyA.R.XWorldLimits;
    y = studyA.R.YWorldLimits;
    dx = studyA.R.CellExtentInWorldX;
    dy = studyA.R.CellExtentInWorldY;
    X = x(1)+dx./6 : dx./3 : x(2)-dx./6;
    Y = y(2)-dy./6 : -dy./3 : y(1)+dy./6;
    [X,Y] = meshgrid(X,Y);
    for tn = 1 : size(tmp_,1)
        if isempty(tmp_{tn,2})
            studyA.(tmp_{tn,1}) = false((y(2)-y(1))./dy,(x(2)-x(1))./dx);
        else
            studyA.(tmp_{tn,1}) = false((y(2)-y(1))./dy,...
                (x(2)-x(1))./dx,length(studyA.(tmp_{tn,3})));
        end
    end
    clear x y dx dy
    
    % Rasterise each polygon
    for tn = 1 : size(tmp_,1)
        if isempty(tmp_{tn,2})
            switch tmp_{tn,1}
                case 'INfoc'
                    polyX = studyA.X;
                    polyY = studyA.Y;
                case 'INBuiltuparea'
                    [polyX,polyY] = polyunion([studyA.Builtuparea.X],...
                        [studyA.Builtuparea.Y],'non overlapping');
                    [polyY,polyX] = polyjoin(polyY,polyX);
                    %     figure
                    %     plot(polyX,polyY)
                case 'INIsraeliLocalities'
                    [polyX,polyY] = polyunion([studyA.IsraeliLocalities.X],...
                        [studyA.IsraeliLocalities.Y],'non overlapping');
                    [polyY,polyX] = polyjoin(polyY,polyX);
                    %     figure
                    %     plot(polyX,polyY)
                case 'INSettlements'
                    [polyX,polyY] = polyunion([studyA.Settlements.X],...
                        [studyA.Settlements.Y],'non overlapping');
                    [polyY,polyX] = polyjoin(polyY,polyX);
                    %     figure
                    %     plot(polyX,polyY)
                otherwise
                    error('Undefined')
            end
            polyX = {polyX.'};
            polyY = {polyY.'};
        else
%             sta = strfind(tmp_{tn,2},'(');
%             fin = strfind(tmp_{tn,2},')');
%             if isempty(sta)
%                 polyX = studyA.(tmp_{tn,2}).X;
%                 polyY = studyA.(tmp_{tn,2}).Y;
%             else
%                 tmp = str2num(tmp_{tn,2}(sta+1:fin-1));
%                 polyX = studyA.(tmp_{tn,2}(1:sta-1))(tmp).X.';
%                 polyY = studyA.(tmp_{tn,2}(1:sta-1))(tmp).Y.';
%             end
            polyX = {studyA.(tmp_{tn,3}).X};
            polyY = {studyA.(tmp_{tn,3}).Y};
        end
        
        % []
        for pn = 1 : length(polyX)
            [Xcells,Ycells] = polysplit(polyX{pn}.',polyY{pn}.');
            INfoc = false(size(X));
            for m = 1 : length(Xcells)
                if ispolycw(Xcells{m},Ycells{m})
                    INfoc(inpoly([X(:),Y(:)],[Xcells{m} Ycells{m}])) = true;
                else
                    INfoc(inpoly([X(:),Y(:)],[Xcells{m} Ycells{m}])) = false;
                end
            end

            % If 4 or more of the dense raster pixel centres are within the
            % polygon, then consider this pixel as within the polygon
            m_ = 2 : 3 : size(INfoc,1);
            n_ = 2 : 3 : size(INfoc,2);
            for m = 1 : length(m_)
                for n = 1 : length(n_)
                    studyA.(tmp_{tn,1})(m,n,pn) = sum(sum(INfoc(...
                        m_(m)-1:m_(m)+1,n_(n)-1:n_(n)+1),2),1) > 4;
                end
            end
        end
        
        assert(max(max(sum(studyA.(tmp_{tn,1}),3)))==1,...
            'Duplication exists; Use the commented code below to find which ones are duplicate.')
%         tmp = unique(sum(cell2mat(cellfun(@(m,n) find(...
%             studyA.INSettlementsPer(m,n,:)),num2cell(m),num2cell(n),...
%             'UniformOutput',false).').*[1;1i],1));
%         [real(tmp); imag(tmp)]
        
        % If the considered polygons under rasterisation are parts of the
        % focus polygon and their union should match the focus polygon,
        % then ensure that each focus pixel also belongs to a considered
        % polygon.
        % This paragraph needs further work, as it failed in some
        % polygons, where a keyboard command has been inserted. The
        % INSettlementsPer branch should be the right one.
        switch tmp_{tn,1}
          case {'INfoc','INBuiltuparea','INIsraeliLocalities',...
              'INSettlements'}
          case 'INGovernoratesPer'
            [I,J] = find(~sum(studyA.(tmp_{tn,1}),3)&studyA.INfoc);
          case 'INOsloPer'
            beep
            disp('Check this!')
            keyboard
          case 'INBuiltupareaPer'
            beep
            disp('Check this!')
            keyboard
            [I,J] = find(~sum(studyA.(tmp_{tn,1}),3)&studyA.INfoc);
          case {'INIsraeliLocalitiesPer','INSettlementsPer'}
            [I,J] = find(~sum(studyA.(tmp_{tn,1}),3) & ...
                studyA.(tmp_{tn,4}) & studyA.INfoc);
          otherwise
            error('Undefined!')  
        end
        
        % For each pixel that should belong to the whole polygon mask
        % and that does not belong to the part polygon mask, we find
        % the pixel area that belongs to each polygon part, and assign
        % the pixel to the mask that have the greatest area.
        % This paragraph needs further work, as it failed in some
        % polygons, where a keyboard command has been inserted. The
        % INSettlementsPer branch should be the right one.
        switch tmp_{tn,1}
          case {'INfoc','INBuiltuparea','INIsraeliLocalities',...
              'INSettlements'}
          case 'INGovernoratesPer'
            [X2,Y2] = studyA.R.intrinsicToWorld(J,I);
            IN = cell2mat(cellfun(@(x,y) inpolygon(X2,Y2,x,y),...
                polyX,polyY,'UniformOutput',false));
            assert(all(sum(IN,2)==1),'There are pixels without assignment!')
            for m = 1 : size(IN,1)
                studyA.(tmp_{tn,1})(I(m),J(m),IN(m,:)) = true;
            end
          case {'INOsloPer','INBuiltupareaPer','INIsraeliLocalitiesPer',...
              'INSettlementsPer'}
            [X2,Y2] = studyA.R.intrinsicToWorld(J,I);
            for m = 1 : length(X2)
                P1 = struct(...
                    'x',X2(m) + studyA.R.CellExtentInWorldX ./ 2 .* ...
                    [-1 -1 1 1 -1].',...
                    'y',Y2(m) + studyA.R.CellExtentInWorldY ./ 2 .* ...
                    [-1 1 1 -1 -1].',...
                    'hole',0);
                A = zeros(length(studyA.(tmp_{tn,3})),1);  % Area of polygon intersection
                for k = 1 : length(studyA.(tmp_{tn,3}))
                    [Ycells2,Xcells2] = polysplit(...
                        studyA.(tmp_{tn,3})(k).Y,studyA.(tmp_{tn,3})(k).X);
                    P2 = struct('x',Xcells2,'y',Ycells2,...
                        'hole',num2cell(double(~ispolycw(Xcells2,Ycells2))));
                    OutPol = PolygonClip(P1,P2,1);
                    if ~isempty(OutPol)
                        A(k) = arrayfun(@(x) polyarea(x.x,x.y),OutPol);
                    end
                    
%                     figure
%                     plot(X2(1)+studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1],...
%                         Y2(1)+studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1])
%                     hold on
%                     plot(studyA.(tmp_{tn,3})(k).X,studyA.(tmp_{tn,3})(k).Y)
%                     axis equal
                end
                [~,I2] = max(A);
                studyA.(tmp_{tn,1})(I(m),J(m),I2) = true;
            end
        end
    end
    clear polyX polyY INfoc tmp pn

%     figure
%     imagesc(studyA.INfoc)
%     axis equal

%     figure
%     plot(studyA.X,studyA.Y)
%     hold on
%     plot(studyA.buf.X,studyA.buf.Y)
%     axis equal

%     figure
%     imagesc(studyA.R.XWorldLimits,studyA.R.YWorldLimits,studyA.INham)
%     axis equal

    % Function dpsimplify is used first to simplify the study area, because
    % otherwise this step takes too long. The simplification tolerance is
    % set to roughly the size of a pixel.
    ps = dpsimplify([studyA.X studyA.Y],Sim.dpsimplify.tol);
    switch Sim.SA.name
      case {'West Bank','Settlements'}
      case 'Gaza'
        ps(18:end,:) = [];
      otherwise
        error('Undefined')
    end

    % Load the simplified border onto the object
    studyA.bordSimplif = ps;

%     % Make the border a closed polygon
%     ps = [ps; flipud(ps)];
%     
%     % 250 m from border
%     % Function bufferm2 computes buffer zone around a polygon.
%     [xBord,yBord] = bufferm2('xy',ps(:,1),ps(:,2),250);
%     %
%     [Xcells,Ycells] = polysplit(xBord,yBord);
%     INfoc = false(size(X));
%     for m = 1 : length(Xcells)
%         if ispolycw(Xcells{m},Ycells{m})
%             INfoc(inpoly([X(:),Y(:)],[Xcells{m} Ycells{m}])) = true;
%         else
%             INfoc(inpoly([X(:),Y(:)],[Xcells{m} Ycells{m}])) = false;
%         end
%     end
%     % If 4 or more of the dense raster pixel centres are within the
%     % polygon, then consider this pixel as within the polygon
%     INfoc2 = false((y(2)-y(1))./dy,(x(2)-x(1))./dx);
%     m_ = 2 : 3 : size(INfoc,1);
%     n_ = 2 : 3 : size(INfoc,2);
%     for m = 1 : length(m_)
%         for n = 1 : length(n_)
%             INfoc2(m,n) = sum(sum(INfoc(...
%                 m_(m)-1:m_(m)+1,n_(n)-1:n_(n)+1),2),1) > 4;
%         end
%     end
%     %
%     studyA.IN250 = studyA.INfoc & INfoc2;
%     %
% %     figure
% %     imagesc(studyA.INfoc&INfoc2)
% %     axis equal
%     
%     % 2 km from border
%     [xBord,yBord] = bufferm2('xy',ps(:,1),ps(:,2),2000);
%     %
%     [Xcells,Ycells] = polysplit(xBord,yBord);
%     INfoc = false(size(X));
%     for m = 1 : length(Xcells)
%         if ispolycw(Xcells{m},Ycells{m})
%             INfoc(inpoly([X(:),Y(:)],[Xcells{m} Ycells{m}])) = true;
%         else
%             INfoc(inpoly([X(:),Y(:)],[Xcells{m} Ycells{m}])) = false;
%         end
%     end
%     % If 4 or more of the dense raster pixel centres are within the
%     % polygon, then consider this pixel as within the polygon
%     INfoc2 = false((y(2)-y(1))./dy,(x(2)-x(1))./dx);
%     m_ = 2 : 3 : size(INfoc,1);
%     n_ = 2 : 3 : size(INfoc,2);
%     for m = 1 : length(m_)
%         for n = 1 : length(n_)
%             INfoc2(m,n) = sum(sum(INfoc(...
%                 m_(m)-1:m_(m)+1,n_(n)-1:n_(n)+1),2),1) > 4;
%         end
%     end
%     %
%     studyA.IN2000 = studyA.INfoc & INfoc2;
%     %
% %     figure
% %     imagesc(studyA.INfoc&INfoc2)
% %     axis equal

    % Assign randomly outlier pixels in termns of noise rise that is caused
    % by non-coordination at border
    rng(1324)
    residuErr = Sim.IM_DL2(:,2) - (-0.0008061*Sim.IM_DL2(:,1)+1.109);
    studyA.residuErr = zeros(size(studyA.INfoc));
    X = randi(size(residuErr,1),numel(studyA.residuErr),1);
    studyA.residuErr(:) = residuErr(X);
    
    % Save for fast calls
    save(fullfile(Sim.path.ProgramData,'studyA.mat'),'studyA')
end
end

function studyA = s_ONland(studyA,clu)
% Populate ONland

% Create a denser raster so that each pixel contains 9 pixels
x = studyA.R.XWorldLimits;
y = studyA.R.YWorldLimits;
dx = studyA.R.CellExtentInWorldX;
dy = studyA.R.CellExtentInWorldY;
X = x(1)+dx./6 : dx./3 : x(2)-dx./6;
Y = y(2)-dy./6 : -dy./3 : y(1)+dy./6;
[X,Y] = meshgrid(X,Y);

% If 4 or more of the dense raster pixel centres are on land, then
% consider this pixel as on land
[I,J] = clu.R.worldToDiscrete(X,Y);
ONland = clu.A(sub2ind(size(clu.A),I,J)) < 35;
m_ = 2 : 3 : size(ONland,1);
n_ = 2 : 3 : size(ONland,2);
for m = 1 : length(m_)
    for n = 1 : length(n_)
        studyA.ONland(m,n) = sum(sum(ONland(m_(m)-1:m_(m)+1,...
            n_(n)-1:n_(n)+1),2),1) > 4;
    end
end
% clear ONland m_ n_ m n I J
end
    end
end
