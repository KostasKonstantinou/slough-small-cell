function [Cell,edgCloSit,interfIequi,bund1,impro,isCell0,isXtunab] = ...
    apply_impro(Cell,edgCloSit,interfIequi,impro,Sim,studyA,clu,ter,...
    Cell_save,bund1,dsm)
%% [Title]
% [Description]
%
% * v0.1 KK 06Feb17 Created
% * v0.2 AK 07Sep17 Optimising with Cell2Vec instead of cellmat

for n = 1 : length(impro)
    % This is the rule ID for this improvement(it is used for cost calculation)
    rn = impro(n).rulID;
    
    % The power amplifier class
    pwrAmpClas = Sim.rul(rn).pwrAmpClas;
    
    % The operator
    operator = impro(n).operato;
    
    % If this rule is an upgrade or decommissioned one, then we need to change the respective
    % parameters according to the upgrading rules, else we need to construct the new cell
    if ~Sim.rul(rn).isUpg && ~Sim.rul(rn).isDecomm
        % This is a new site
        % Its height
        if isfield(impro(n),'heigm')
            assert(isnan(Sim.propag.BS{strcmp(Sim.propag.BS(:,1),...
                pwrAmpClas),2}),['SIM defines the site height, but ' ...
                'that is given by the lamppost database'])
            h_agl = impro(n).heigm;
        else
            h_agl = Sim.propag.BS{strcmp(Sim.propag.BS(:,1),pwrAmpClas),2};
            assert(~isnan(h_agl),'SIM cannot define the site height as NaN')
        end
        
        % Set the Site ID as the current date
        siteID = [datestr(now,'ddmmyyHHMMSSFFF') 'KK'];
        
        % Parameters based on the this rule
        sitChaiConfig = Sim.rul(rn).upg.sitChainConfig;
        sectoCou = Sim.rul(rn).upg.sectoCou;
        AeCou = Sim.rul(rn).upg.AeCou;
        BW = Sim.rul(rn).upg.Band;

        if Sim.rul(rn).isUpg
            disp('Consider sharing')
            sdfdsf
        end

        % Setting the replacement time
        replEver = Sim.propag.BS{strcmp(pwrAmpClas,Sim.propag.BS(:,1)),8};

        % Setting the FBMC property
        FBMC = false(length(Sim.linBud.freq),1);
%         if Sim.yea.thisYea < 2020
%             FBMC = false(length(Sim.linBud.freq),1);
%         elseif Sim.yea.thisYea < 2027
%             FBMC = [true false false false].';
%         else
%             FBMC = [true true false false].';
%         end
        
        % The location indices
        I = impro(n).I;
        J = impro(n).J;

        % Default parameters for the new sites
        situ = 'outd';
        if Sim.yea.isF && Sim.isShufNsitEstablishedDatOnStartYea
            Opt.Format = 'double';
            rng(sum(DataHash({I,J,pwrAmpClas,operator},Opt)))
            established = Sim.yea.thisYea - randi(replEver) + 1;
        else
            established = Sim.yea.thisYea;
        end

        switch impro(n).XYorIJ
          case 'IJ'
            sty = 'new at row column';
            IorX = I;
            JorY = J;
          case 'XY'
            sty = 'new at Easting Northing';
            IorX = impro(n).X;
            JorY = impro(n).Y;
          otherwise
            error('Undefined')
        end
        
        Cell(end+1) = CELL(sty,Sim,studyA,edgCloSit,clu,IorX,JorY,...
            h_agl,operator,siteID,sitChaiConfig,pwrAmpClas,situ,...
            sectoCou,AeCou,BW,FBMC,established,rn,replEver);

        % Calculate median path loss
        fn_ = Cell(end).band.supp;
        Cell(end) = run_propa_modu_med(Cell,Sim,...
            [false(1,length(Cell)-1) true],studyA,clu,ter,fn_,dsm);

        impro.CellIND = length(Cell);

        interfIequi(:,end+1,:) = repmat(min(round((...
            Sim.linBud.PRButilisCovera + 1./Sim.utilisN./2) ./ ...
            (1./Sim.utilisN)),Sim.utilisN).',size(interfIequi,1),1);
        
        % A flag to signify that we need to update the bund1 information
        flagUpdBund1 = true;
    else
        % This is an existing site
        IND = impro(n).CellIND;

        % Simplification
        curre = Sim.rul(impro(n).rulID).curre.Band;
        upg = Sim.rul(impro(n).rulID).upg.Band;
        isMoreBand = any(curre==0&upg>0,2);

        % Revert the cell to its state without rule, if the rule is being
        % reconsidered
        if Cell(IND).rul > 0
            Cell(IND) = Cell_save(IND);
        end

        % The Cell before the changes
        curCell = Cell( IND );
        % Apply rules to the site
        Cell(IND) = Cell(IND).apply_rul(rn,Sim,edgCloSit);

        % Run propagation if more bands
        if isMoreBand
            % More bands
            % Calculate median path loss
            fn_ = upg>0 & curre==0;
            Cell(IND) = run_propa_modu_med(Cell,Sim,...
                [false(1,IND-1) true false(1,length(Cell)-IND)],...
                studyA,clu,ter,fn_);
        end
        % If the new cell differs at all with the bundle characteristic for the previous cell, then
        % set the flag to true, as we need to update the bund1 information
        flagUpdBund1 = ...
            ~strcmp(Cell(IND).pwrAmpClas,curCell.pwrAmpClas) |...
            ~strcmp(Cell(IND).situ,curCell.situ) |...
            ~all(Cell(IND).AeCou==curCell.AeCou) |...
            ~(Cell(IND).sectoCou==curCell.sectoCou) |...
            ~all(Cell(IND).FBMC==curCell.FBMC);
    end
    
    % Update the bundle1 with the information of the new/updated Cell
    if flagUpdBund1
        bund1 = u_bund1(Sim,Cell(impro(n).CellIND),bund1,impro(n).CellIND);    
    end
end

% Cell.set_co_ch sets the Cell.co_ch property of all sites. Cell.co_ch is
% used by c_sitWithiCov, which is called by serv_dem.
Cell = Cell.set_co_ch;

% Cell.s_tilt sets the Cell.tilt property of all sites. Cell.co_ch is
% used by c_sitWithiCov, which is called by serv_dem.
Cell = Cell.s_tilt(Sim);

% Find the cells that are in the same band bundle
if nargout > 4
    ch = [Cell.ch];
    
    % isCell0 is a logical vector with elements true for the sites we
    % wish to participate in the service of demand. The sites in
    % isCell0 also contribute to interference. We wish to include all
    % the sites that are co-channel with any of the channels of the
    % upgraded site, because if we include a subset of the sites then
    % the loading-equilibrium calculation tends to reduce the
    % interference indices, due to lack of interference.
    isCell0 = false(size(Cell));
    isCell0([impro.CellIND]) = true;
    if ~Sim.servDem.isInterfeExpl.cov
        isCell0 = isCell0 | c_neig(Cell,isCell0);
    end
    
    % isXtunab: are x elements tunable
    if Sim.servDem.isInterfeExpl.cov
        isXtunab = (isCell0 | c_neig(Cell,isCell0)).';
    else
        isXtunab = isCell0.';
    end
    
    if Sim.servDem.isInterfeExpl.cov
        isCell0(any(ch(any(ch(:,isCell0),2),:),1)) = true;
    end
end
