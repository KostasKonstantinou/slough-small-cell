function PVperSit = c_PVperSit(Cell,edgCloSit,ci)
%% Calculate present value of the sum of the antenna and edge site costs
% The costs that CAPisce minimised were calculated as the sum of new
% antenna sites and servers as initial CAPEX, 10 years of OPEX, plus any
% end-of-life CAPEX within a 10 year period, brought to present value with
% 10% discount rate.
% 
% Note that Cell is expected to be a comprehensive array of Cell objects
% within the study area. (It would be better to include only Cell objects
% associated with the edge cloud site affected.) This is because the cost
% of edge cloud sites is calculated from the computational load of all the
% antenna sites they serve.
% 
% c_PVperSit returns the present value per site, PVperSit.
% 
% Input Cell is an array of CELL objects, must contain the following
% fields:
% capexPv, opexPv, connTowaCloID.
% If connTowaCloID is nonzero (the antenna site is in CRAN chain
% configuration), then Cell must also contain the following fields:
% userDyn, userRema, cellProce.
% 
% Input edgCloSit is an array of EDGCLOSIT objects, must contain the
% following fields:
% ID, capexPv, opexPv
% 
% Input ci is a vector of the Cell indices which we wish the PV cost for.
% 
% * v0.1 Kostas Konstantinou Apr 2017

% Calculate the PV of the cost of each antenna site and save it as
% PVperSit, which later will be supplemented with the cost of edge cloud
% sites.
PVperSit = [Cell.capexPv].' + [Cell.opexPv].';

% For each edge cloud site, calculate the PV of the cost of the edge cloud
% sites site. Then aportion the PV cost to the connected antenna sites
% based on their contribution to computational load. Lastly, add to each
% antenna site the PV cost that is associated with the edge.
connTowaCloID = [Cell.connTowaCloID];
for n = 1 : length(edgCloSit)
    IND = connTowaCloID == edgCloSit(n).ID;
    if any(IND)
        coreCou = sum(c_coreCou(Cell(IND)),1).';
        PVperSit(IND) = PVperSit(IND) + (edgCloSit(n).capexPv + ...
            edgCloSit(n).opexPv) .* coreCou ./ sum(coreCou);
    end
end

% Amongst all calcualted antenna site PV costs, keep only these in ci
PVperSit = PVperSit(ci);
