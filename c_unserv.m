function out1 = c_unserv(Sim,thisDem,studyA,sty1,sty2,Cell,CellI,thisDem0)
%% Calculate (un)served demand
% out1 = c_unserv(...) outputs the traffic volume of (un)served demand
% 
% The output matrix out1 has a size that is dependent on the requirement
% set by sty2:
% if sty2 is 'per pixel', then out1 is MxN, where M is the number of demand
% 	points (pixels) and N is the number of tenants
% if sty2 is {'sum over focus','sum over buffer','sum over Hamburg'}, then
% 	out1 is AxN, where A is the number of modelled hours
% if sty2 is 'per site', then out1 is Bx1, where B is the number of sites
% if sty2 is 'per pixel and per site', then out1 is MxB
% 
% The input sty1 identifies if the demand volume in out1 is the served
% part, the newly served, the unserved, or the sum of the two. The newly
% served part is identified as the difference of thisDem minus thisDem0.
% The input sty1 must be one of the following:
% ('unserv','serve','newly serv','all')
% 
% The input sty2 identifies the type of output required. The output out1
% can be provided either per pixel ('per pixel'), per site ('per site'),
% per pixel and per site ('per pixel and per site'), summed over the focus 
% area ('sum over focus'), summed over the focus plus buffer area ('sum
% over buffer'), summed over the area of Hamburg excluding HPA ('sum over
% Hamburg').
% 
% The input Sim is a SIM object and must contain the following fields,
% see class for definition:
% dem.hourDistri
% dem.operators
% dem.consiInMeritPerTena
% dem.trafVol
% demTranslati
% SA.pixSize
% 
% The input Cell is an array of CELL objects and must contain the following
% fields, see class for definition:
% I,J
%
% The input CellI is a logical vector of the same size as Cell, whose
% elements are true for the cells we wish to query within this function.
% 
% The inputs thisDem and thisDem0 are arrays of demand points objects and
% must contain the following fields, see class for definition:
% I,J
% v
% satis
% assoc
% 
% The input studyA is a structure or list of structures with at least the
% following fields:
% INfoc
% INham
% 
% * v0.1 Kostas Konstantinou Dec 2016

BHcou = size(Sim.dem.hourDistri,1);

switch sty2
    case 'per pixel'
        out1 = zeros(length(thisDem),length(Sim.dem.operators));
    case {'sum over focus','sum over buffer','sum over Hamburg'}
        out1 = zeros(BHcou,length(Sim.dem.operators));
    case 'per site'
        out1 = zeros(length(CellI),1);
    case 'per pixel and per site'
        out1 = zeros(length(thisDem),length(CellI));
    case 'per hour'
        out1 = zeros(BHcou,1);
    case 'per hour and mobility class over focus'
        out1 = zeros(BHcou,size(Sim.demTranslati,1));
end

% Calculate unserv
for on = 1 : length(Sim.dem.operators)  % For each tenant
%     switch unit
%         case 'MB/day'
% %         case 'MB/m2 19:00-20:00'
% %             for n = 1 : length(Sim.linBud.UE.pwrAmpClas_)
% %                 if n == 1
% %                     fun = '@(x) ';
% %                 end
% %                 for m = 1 : length(Sim.dem.trafClas_)
% %                     tmp = ['x.MBB.' Sim.dem.trafClas_{m} '.' ...
% %                         Sim.linBud.UE.pwrAmpClas_{n} '.' Sim.linBud.UE.situ_{n} ...
% %                         '.(Sim.dem.operators{' num2str(on) '}).'];
% %                     switch sty1
% %                         case 'unserv'
% %                             fun = [fun 'full(sum(' tmp 'v(20,:) .* (1-' tmp 'satis(20,:)),2)) ./ 8 .* 3600 ./ Sim.SA.pixSize.^2 + '];
% %                         case 'serve'
% %                             fun = [fun 'full(sum(' tmp 'v(20,:) .* (' tmp 'satis(20,:)),2)) ./ 8 .* 3600 ./ Sim.SA.pixSize.^2 + '];
% %                     end
% %                 end
% %             end
%         otherwise
%             error('code this!')
%     end
    
    translati = Sim.demTranslati;
    IND2 = strcmp(translati(:,5),Sim.dem.operators{on});
    switch sty2
        case 'per pixel'
%             fun = eval(fun(1:end-3));
            
%             out1(:,on) = arrayfun(fun,thisDem);
            
            switch sty1
                case 'unserv'
                    v = reshape(full([thisDem.v].*(1-[thisDem.satis])),...
                        [BHcou,length(IND2),length(thisDem)]);
                case 'serve'
                    v = reshape(full([thisDem.v].*[thisDem.satis]),...
                        [BHcou,length(IND2),length(thisDem)]);
                case 'all'
                    v = reshape(full([thisDem.v]),...
                        [BHcou,length(IND2),length(thisDem)]);
                case 'newly serv'
                    v = reshape(full([thisDem.v].*([thisDem.satis] - ...
                        [thisDem0.satis])),...
                        [BHcou,length(IND2),length(thisDem)]);
                otherwise
                    error('Undefined')
            end

            v(:,~IND2,:) = [];
            out1(:,on) = sum(sum(v,2),1) ./ 8 .* 3600;
        case 'sum over focus'
%             fun = eval(fun(1:end-3));
            
%             out1(:,on) = sum(arrayfun(fun,thisDem(studyA.INfoc(sub2ind(size(...
%                 studyA.INfoc),[thisDem.I],[thisDem.J])))));
            
            assert(strcmp(sty1,'all'),'Code this!')
            if all(IND2)
                v = full([thisDem(studyA.INfoc(sub2ind(size(studyA.INfoc),...
                    [thisDem.I],[thisDem.J]))).v]);
            else
                v = cell2mat(arrayfun(@(x) full(sum(x.v(:,IND2),2)),...
                    thisDem(studyA.INfoc(sub2ind(size(studyA.INfoc),...
                    [thisDem.I],[thisDem.J]))),'UniformOutput',false).');
            end
            if isempty(v)
                out1(:,on) = 0;
            else
                out1(:,on) = sum(v,2) ./ 8 .* 3600;
            end
        case 'sum over buffer'
%             fun = eval(fun(1:end-3));
            
%             out1(:,on) = sum(arrayfun(fun,thisDem(~studyA.INfoc(sub2ind(size(...
%                 studyA.INfoc),[thisDem.I],[thisDem.J])))));

            assert(strcmp(sty1,'all'),'Code this!')
            if all(IND2)
                v = full([thisDem(~studyA.INfoc(sub2ind(size(studyA.INfoc),...
                    [thisDem.I],[thisDem.J]))).v]);
            else
                v = cell2mat(arrayfun(@(x) full(sum(x.v(:,IND2),2)),...
                    thisDem(~studyA.INfoc(sub2ind(size(studyA.INfoc),...
                    [thisDem.I],[thisDem.J]))),'UniformOutput',false).');
            end
            if isempty(v)
                out1(:,on) = 0;
            else
                out1(:,on) = sum(v,2) ./ 8 .* 3600;
            end
        case 'sum over Hamburg'
            assert(strcmp(sty1,'all'),'Code this!')
            if all(IND2)
                v = full([thisDem(studyA.INham(sub2ind(size(studyA.INham),...
                    [thisDem.I],[thisDem.J]))).v]);
            else
                beep
                disp('Code this!')
                keyboard
            end
            out1(:,on) = sum(v,2) ./ 8 .* 3600;
        case 'per site'
            % Per site
            % The tenant loop is irrelevant here, and ideally the tenant
            % loop should go within the braches that is relevant. As a
            % quick fix here I terminate the loop with return at the end of
            % the first loop.
            
            thisDem_I = [thisDem.I].';
            thisDem_J = [thisDem.J].';
            
            if length(CellI) == 1
                [X,Y] = studyA.R.intrinsicToWorld(thisDem_J,thisDem_I);
                BoundingBox = Cell(CellI).BoundingBox(~cellfun(@isempty,...
                    Cell(CellI).BoundingBox));
                BoundingBox = reshape(cell2mat(BoundingBox.'),2,2,...
                    length(BoundingBox));
                BoundingBox = [min(BoundingBox(1,1,:)) min(BoundingBox(1,2,:))
                    max(BoundingBox(2,1,:)) max(BoundingBox(2,2,:))];
                isClo = {BoundingBox(1,1)<=X & X<=BoundingBox(2,1) & ...
                    BoundingBox(1,2)<=Y & Y<=BoundingBox(2,2)};
            else
                beep
                disp('code this!')
                keyboard
                
                [~,Locb] = ismember({Cell(CellI).pwrAmpClas},...
                    Sim.propag.BS(:,1));
                isClo = arrayfun(@(x) abs(thisDem_I-x.I)<=Sim.propag.BS{Locb,5}./Sim.SA.pixSize & ...
                    abs(thisDem_J-x.J)<=Sim.propag.BS{Locb,5}./Sim.SA.pixSize,Cell(CellI),...
                    'UniformOutput',false);
            end

            % For each cell in question, calculate the demand that it
            % serves
            for n = 1 : length(CellI)
                if any(isClo{n})
                    consiInMerit = repmat(Sim.dem.consiInMeritPerTena(cell2mat(...
                        Sim.demTranslati(:,7))),nnz(isClo{n}),1).';
                    isTxMultiCopiesDataToAllServ = ...
                        repmat(cellfun(@(x) Sim.dem.trafVol.(x...
                        ).CEtput.isTxMultiCopiesDataToAllServ(1),...
                        Sim.demTranslati(:,1)),nnz(isClo{n}),1).';
                    if any(isTxMultiCopiesDataToAllServ)
                        disp('Check what should happen when there is multiconnectivity!')
                        beep
                        keyboard
                    end
                    assoc = [thisDem(isClo{n}).assoc];
                    switch sty1
                        case 'newly serv'
                            v = bsxfun(@times,full([thisDem(isClo{n}).v] .* ...
                                ([thisDem(isClo{n}).satis] - ...
                                [thisDem0(isClo{n}).satis])),consiInMerit);
                            assoc0 = [thisDem0(isClo{n}).assoc];
                            tmp = reshape(Cell2Vec(assoc),size(assoc,1),...
                                size(assoc,2))==CellI(n) | ...
                                reshape(Cell2Vec(assoc0),size(assoc0,1),...
                                size(assoc0,2)) == CellI(n);
                        case 'serv'
                            v = bsxfun(@times,full([thisDem(isClo{n}).v] .* ...
                                [thisDem(isClo{n}).satis]),consiInMerit);
                            tmp = reshape(Cell2Vec(assoc),size(assoc,1),...
                                size(assoc,2)) == CellI(n);
                        case 'all'
                            v = bsxfun(@times,full([thisDem(isClo{n}).v]),...
                                consiInMerit);
                            tmp = reshape(Cell2Vec(assoc),size(assoc,1),...
                                size(assoc,2)) == CellI(n);
                        otherwise
                            error('Undefined')
                    end
%                     if any(isTxMultiCopiesDataToAllServ)
%                         disp('Check what should happen when there is multiconnectivity!')
%                         beep
%                         keyboard
%                         tmp = false(size(assoc));
%                         tmp(:,isTxMultiCopiesDataToAllServ) = ...
%                             cellfun(@(x) any(x==CellI(n)),...
%                             assoc(:,isTxMultiCopiesDataToAllServ));
%                     else
%                         tmp = reshape(Cell2Vec(assoc),size(assoc,1),...
%                             size(assoc,2)) == CellI(n);
%                     end
                    out1(n) = sum(v(tmp));
                end
            end
        case 'per pixel and per site'
            beep
            disp('check this!')
            keyboard
            
            thisDem_I = [thisDem.I].';
            thisDem_J = [thisDem.J].';
            isClo = arrayfun(@(x) abs(thisDem_I-x.I)<=x.maxDist_m./Sim.SA.pixSize & ...
                abs(thisDem_J-x.J)<=x.maxDist_m./Sim.SA.pixSize,Cell(CellI),...
                'UniformOutput',false);
            
            fun3 = eval([fun3(1:end-3) ',thisDem(isClo))']);
            
            assert(length(Sim.dem.operators)==1)
            assert(length(CellI)==1)
            out1(isClo{1},1) = fun3(CellI,thisDem,Sim,isClo{1});
        case 'per hour'
            switch sty1
                case 'unserv'
                    v = reshape(full([thisDem.v].*(1-[thisDem.satis])),...
                        [BHcou,length(IND2),length(thisDem)]);
                case 'serve'
                    v = reshape(full([thisDem.v].*[thisDem.satis]),...
                        [BHcou,length(IND2),length(thisDem)]);
                case 'all'
                    v = reshape(full([thisDem.v]),...
                        [BHcou,length(IND2),length(thisDem)]);
                otherwise
                    error('Undefined')
            end

            v(:,~IND2,:) = [];
            out1 = out1 + sum(sum(v,3),2)./8.*3600;
        case 'per hour and mobility class over focus'
            INfoc = studyA.INfoc(sub2ind(size(studyA.INfoc),...
                [thisDem.I],[thisDem.J]));
            switch sty1
                case 'unserv'
                    v = reshape(full([thisDem(INfoc).v].*(1-[thisDem(INfoc).satis])),...
                        [BHcou,length(IND2),nnz(INfoc)]);
                case 'serve'
                    v = reshape(full([thisDem(INfoc).v].*[thisDem(INfoc).satis]),...
                        [BHcou,length(IND2),nnz(INfoc)]);
                case 'all'
                    v = reshape(full([thisDem(INfoc).v]),...
                        [BHcou,length(IND2),nnz(INfoc)]);
                otherwise
                    error('Undefined')
            end

            v(:,~IND2,:) = [];
            if ~isempty(v)
                out1(:,IND2) = out1(:,IND2) + sum(v,3)./8.*3600;
            end
    end
end
