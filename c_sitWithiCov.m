function [Cell,thisDem,utilis] = c_sitWithiCov(Cell,visib,thisDem,...
    fn,fs1,trafClI,clu,Sim,params,params2,isDispLinBud,utilisBand,utilisN)
%% [Title]
% [Description]
%
% * v0.1 KK 14Oct16 Created
% * v0.2 HO 11Aug17 Delete code related to handoMargi
% * v0.3 AK Sep17   Added multivariable normal CDF calculation and only DL is considered for
%                   non-eMBB slices

% The visib structure contains information about which site and 'see'
% which pixels, for what frequency and with what SINR.

utilis = params{9};

for servCou = 1 : max(structfun(@(x) x.CEtput.ServCouMax(trafClI),...
        Sim.dem.trafVol))
    if size(visib.SE_DL,2) < servCou
        break
    end
    
    % []
    isDel = isnan(visib.SE_DL(:,servCou));
    if any(isDel)
        names = fieldnames(visib);
        for n = 1 : length(names)
            visib.(names{n})(isDel,:) = [];
        end
    end
    clear isDel n names
    
    [Cell,thisDem,utilis] = c_sitWithiCov_per_connecti(Cell,visib,...
        thisDem,utilis,fn,fs1,trafClI,clu,Sim,isDispLinBud,utilisBand,...
        utilisN,servCou,params,params2);
end
