function SINRthres_DL = c_SINRthres_DL(IND3,visib,isUr,BHcou,utilisN,...
    isBPGstandZero,thisDem_IND,sitI,servCou,dim1Sub)

SINRthres_DL = zeros(BHcou,length(sitI),servCou);
for n = 1 : servCou
    linearInd2 = sub2ind(size(visib.SINRthres_DL),...
        repmat(IND3.',BHcou,1),dim1Sub(:,:,n)+(n-1)*utilisN);
    SINRthres_DL(:,:,n) = visib.SINRthres_DL(linearInd2);
end

%             noisRis = I - visib.noisFl_DL(IND3);
%             I_UL = I_UL0(I_ULic(IND3)) + noisRis;
%             Mu_UL = visib.PRx_across_system_BW_UL(IND3) - I_UL - ...
%                 Sim.linBud.dop(bund2.ia(visib.bund2(IND3)),fn);

% Define standard deviation, Sigma
if isBPGstandZero
%     sigmaW = visib.sigmaLe(IND3,1:servCou);
else
    beep
    disp('Continue vectorisation from here!')
    keyboard
    
    % Define teledensity, teleden, as urban or other
    if isUr(thisDem_IND)
        teleden = 'ur';
    else
        teleden = 'ot';
    end

    beep
    disp('Check this!')
    keyboard
    
    % []
    linBud = Sim.linBud.(pwrAmpClasMS).(situMS);
    if length(BPGstand.(teleden)) == 1
        sigmaW = sqrt(visib.sigmaLe(IND3).^2 + ...
            BPGstand.(teleden).^2 + ...
            linBud.BPGstand.(fs1).(teleden).^2);
    else
        sigmaW = sqrt(visib.sigmaLe(IND3).^2 + ...
            BPGstand.(teleden)(visib_IND3_Celli).'.^2 + ...
            linBud.BPGstand.(fs1).(teleden).^2);
    end
end
