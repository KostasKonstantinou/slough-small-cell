classdef DSM
    % DSM class for management of the clutter
    % The DSM class is responsible for creating the matrix that contains
    % the clutter type and other clutter information. More specifically it is
    % responsible for delivering the following:
    %
    % * creates the dsm object
    % * calculate breakdown statistics
    % * plots a map of dsm object
    %
    % * Kostas Konstantinou 27Jul17
    
    properties
        A;  % Values
        R;  % maprasterref object
%         tran;  % Translation matrix
    end
    
    methods
%% Contructor of the CLASS DSM
function dsm = DSM(Sim,studyA)
% DSM is the class constructor of DSM object, dsm.
% 
% Sim is a structure that contains at least the following fields:
% path - Paths to databases
% 
% path is a sub-structure relating to the location of databases on teh hard
% disk and contains at least the following fields:
% dsm - Path to clutter database

X = floor(studyA.BoundingBox(:,1)./1000).*1000 + 1;
Y = floor(studyA.BoundingBox(:,2)./1000).*1000 + 1;
[X,Y] = meshgrid(X(1):1000:X(2),Y(1):1000:Y(2));

% [I,J] = find(studyA.INfoc);
% [X,Y] = studyA.R.intrinsicToWorld(J,I);
% clear I J

% figure
% plot(X(:),Y(:),'.')
% axis equal
% for n = 1 : numel(X)
%     text(X(n),Y(n),[' ' num2str(n)])
% end

% Header info
nrows = 1000;
ncols = 1000;
cellsize = 1;

% Create raster of DSM
XLimWorld = [floor(min(studyA.X)./cellsize) ...
    ceil(max(studyA.X)./cellsize)] .* cellsize;
YLimWorld = [floor(min(studyA.Y)./cellsize) ...
    ceil(max(studyA.Y)./cellsize)] .* cellsize;
RasterSize = [diff(YLimWorld) diff(XLimWorld)] ./ cellsize;
dsm.R = maprasterref('RasterSize',RasterSize,...
    'YLimWorld',YLimWorld,'ColumnsStartFrom','north',...
    'XLimWorld',XLimWorld);
dsm.A = -9999 .* ones(RasterSize(1),RasterSize(2),'single');
clear XLimWorld YLimWorld RasterSize

% [~,ia] = unique(floor(X./1000)+1i*floor(Y./1000));
for n = 1 : numel(X)
    natGrid = EN2NGR(X(n),Y(n));
    natGrid = natGrid{1};
    
    if str2num(natGrid(4))<5 && str2num(natGrid(9))<5
        tmp = 'sw';
    elseif str2num(natGrid(4))<5 && str2num(natGrid(9))>=5
        tmp = 'nw';
    elseif str2num(natGrid(4))>=5 && str2num(natGrid(9))<5
        tmp = 'se';
    else
        tmp = 'ne';
    end
    f = fullfile(Sim.path.dsm,['LIDAR-DSM-1M-' natGrid([1,2,3,8]) tmp]);
    if ~exist(f,'dir')
        error('Download data from DSM server!')
    end
    f = fullfile(f,...
        [lower(natGrid(1:2)) natGrid([3,4,8,9]) '_DSM_1M.asc']);
    if ~exist(f,'file')
%         beep
%         disp('DSM data missing!')
        continue
    end
    fileID = fopen(f);
    
    % Skip header
    for k = 1 : 2
        fgetl(fileID);
    end
    
    % Read header info
    %             xllcenter = fscanf(fileID,'xllcenter %f\n');
    %             yllcenter = fscanf(fileID,'yllcenter %f\n');
    xllcorner = fscanf(fileID,'xllcorner %f\n');
    yllcorner = fscanf(fileID,'yllcorner %f\n');
    assert(xllcorner==floor(X(n)./1000).*1000)
    assert(yllcorner==floor(Y(n)./1000).*1000)
    
    %             [X,Y] = meshgrid(xllcenter+[0:1:(ncols-1)].*cellsize,...
    %                 yllcenter+[(nrows-1):-1:0].*cellsize);
    assert(eps(xllcorner+cellsize./2+(ncols-1).*cellsize) < ...
        cellsize./2,'change indexing to integers')
    assert(eps(yllcorner+cellsize./2+(nrows-1).*cellsize) < ...
        cellsize./2,'change indexing to integers')
    [X2,Y2] = meshgrid(...
        xllcorner+cellsize./2+[0:1:(ncols-1)].*cellsize,...
        yllcorner+cellsize./2+[(nrows-1):-1:0].*cellsize);
    
    % Skip header
    fgetl(fileID);
    fgetl(fileID);
    
    % Read all file
    tmp = cell2mat(textscan(fileID,repmat('%f ',1,ncols)));
    [I,J] = dsm.R.worldToDiscrete(X2(:),Y2(:));
    TF = ~isnan(I);
    dsm.A(sub2ind(size(dsm.A),I(TF),J(TF))) = tmp(TF);
    
    fclose(fileID);
end

% figure
% imagesc(dsm.R.XWorldLimits,dsm.R.YWorldLimits([2,1]),dsm.A)
% axis equal
% demcmap([-1,200])
% colorbar
% hold on
% plot(studyA.X,studyA.Y,'k')
% set(gca,'ydir','normal')

% The following code is to find the bounding box of the study area with
% 100m resolution, which is the resolution of the clutter database. The
% idea is to read the european clutter database CORINE with QGIS, cut it to
% the extremes of the bounding box and with the UMT32N coordinate system
% with QGIS, save it as a separate file with QGIS, and then load it up with
% ml. The same methodology was followed for the white space calculation
% tool for Ofcom, and results in a memory-light clutter database.
% [fix(min(studyA.buf.X)./100).*100 fix(min(studyA.buf.Y)./100).*100]
% [ceil(max(studyA.buf.X)./100).*100 ceil(max(studyA.buf.Y)./100).*100]

% Read clutter DBs and save info in clu.A and clu.R
% [clu.A,~,bbox] = geotiffread(Sim.path.clu);
% clu.R = maprasterref('RasterSize',size(clu.A),...
%     'YLimWorld',bbox(:,2).','ColumnsStartFrom','north',...
%     'XLimWorld',bbox(:,1).');

% figure
% hold on
% axis equal
% imagesc(bbox(:,1).',fliplr(bbox(:,2).'),clu.A)
% for n = 1 : length(studyA.X)
%     plot(studyA.X{n},studyA.Y{n},'k')
% end

% Read the CSV file which has the clutter table information and save into
% clu.tran
% [~,~,raw] = xlsread('clu v0_1.csv');
% clu.tran.pop = raw;
% clu.tran.veh = raw;
% % clu.tran(3:46,13) = num2cell(logical(cell2mat(clu.tran(3:46,13))));

%% Below the code for Land cover map 2007
% % Define the clutter translation
% % Below is for 4G coverage verification:
% % clu.tran.pop = {'Title' '' '' 'Index' ...
% %     'SD index for population: 1 urban/SU, 2 other' ...
% %     'for i=2 to n-1 in (1c)' 'for i=1 and n in (1c)' 'model (64)' ...
% %     'K_L (66) for veh'
% %     'Dense Urban' '' '' 1 1 20 20 1 5.1
% %     'Urban' '' '' 2 1 15 15 1 5.1
% %     'Industry' '' '' 3 1 10 10 1 4.9
% %     'Suburban' '' '' 4 1 10 10 1 4.9
% %     'Village' '' '' 5 1 10 10 1 4.9
% %     'Parks/Recreation' '' '' 6 2 0 10 2 4.4
% %     'Open' '' '' 7 2 0 10 2 4.4
% %     'Open in Urban' '' '' 8 2 0 10 2 4.4
% %     'Forest' '' '' 9 2 15 15 1 4.4
% %     'Water' '' '' 10 2 0 10 2 4.4};
% clu.tran.pop = {'Aggregate class' 'Broad habitat' 'LCM2007 class' 'LCM2007 class number' 'Hata branch' 'for i=2 to n-1 in (1c)' 'for i=1 and n in (1c)' 'model (64)' 'SD index' 'K_L' 'R' 'G' 'B'
%     'Broadleaf woodland' '''Broadleaved, Mixed and Yew Woodland''' 'Broadleaved woodland' 1 4 15 15  '' '' '' 232 0 0
%     'Coniferous woodland' '''Coniferous Woodland''' '''Coniferous woodland''' 2 4  15 15 '' '' '' 25 103 16
%     'Arable' '''Arable and Horticulture''' '''Arable and horticulture''' 3 4 4 4 '' '' '' 97 53 0
%     'Improved grassland' '''Improved Grassland''' '''Improved grassland''' 4 4 4 4 '' '' '' 71 252 50
%     'Semi-natural grassland' 'Rough Grassland' 'Rough grassland' 5 4 4 4 '' '' '' 247 171 8
%     'Semi-natural grassland' '''Neutral Grassland''' '''Neutral Grassland''' 6 4 4 4 '' '' '' 139 229 133
%     'Semi-natural grassland' '''Calcareous Grassland''' '''Calcareous Grassland''' 7 4 4 4 '' '' '' 119 148 64
%     'Semi-natural grassland' '''Acid Grassland''' 'Acid grassland' 8 4 4 4 '' '' '' 200 241 193
%     'Semi-natural grassland' '''Fen, Marsh and Swamp''' '''Fen, Marsh and Swamp''' 9 4 4 4 '' '' '' 253 251 38
%     'Mountain, heath, bog' '''Dwarf Shrub Heath''' 'Heather' 10 4 4 4 '' '' '' 129 21 127
%     'Mountain, heath, bog' '''Dwarf Shrub Heath''' 'Heather grassland' 11 4 4 4 '' '' '' 227 139 157
%     'Mountain, heath, bog' '''Bog''' '''Bog''' 12 4 4 4 '' '' '' 37 125 127
%     'Mountain, heath, bog' '''Montane Habitats''' '''Montane Habitats''' 13 4 4 4 '' '' '' 83 255 255
%     'Mountain, heath, bog' '''Inland Rock''' '''Inland Rock''' 14 4 4 4 '' '' '' 215 213 254
%     'Saltwater' 'Saltwater' 'Saltwater' 15 4 4 4 '' '' '' 18 0 145
%     'Freshwater' 'Freshwater' 'Freshwater' 16 4 4 4 '' '' '' 37 0 236
%     'Coastal' '''Supra-littoral Rock''' '''Supra-littoral Rock''' 17 4 4 4 '' '' '' 201 179 22
%     'Coastal' '''Supra-littoral Sediment''' '''Supra-littoral Sediment''' 18 4 4 4 '' '' '' 201 179 22
%     'Coastal' '''Littoral Rock''' '''Littoral Rock''' 19 4 4 4 '' '' '' 252 255 133
%     'Coastal' '''Littoral Sediment''' 'Littoral sediment' 20 4 4 4 '' '' '' 252 255 133
%     'Coastal' '''Littoral Sediment''' 'Saltmarsh' 21 4 4 4 '' '' '' 134 124 253
%     'Built-up areas and gardens' '''Built-up Areas and Gardens''' 'Urban' 22 1 15 15 '' '' '' 1 1 1
%     'Built-up areas and gardens' '''Built-up Areas and Gardens''' 'Suburban' 23 3 10 10 '' '' '' 129 126 134};
% %
% % Below is for Technical analysis of the cost of extending an 800 MHz
% % mobile broadband coverage obligation for the United Kingdom:
% % clu.tran.veh = {'Title' '' '' 'Index' ...
% %     'SD index for population: 1 urban/SU, 2 other' ...
% %     'for i=2 to n-1 in (1c)' 'for i=1 and n in (1c)' 'model (64)' ...
% %     'K_L (66) for veh'
% %     'Dense Urban' '' '' 1 1 17 17 1 5.1
% %     'Urban' '' '' 2 1 17 17 1 5.1
% %     'Industry' '' '' 3 1 15 15 1 4.9
% %     'Suburban' '' '' 4 1 14 14 1 4.9
% %     'Village' '' '' 5 1 10 10 1 4.9
% %     'Parks/Recreation' '' '' 6 2 4 4 2 4.4
% %     'Open' '' '' 7 2 4 4 2 4.4
% %     'Open in Urban' '' '' 8 2 4 4 2 4.4
% %     'Forest' '' '' 9 2 15 15 1 4.4
% %     'Water' '' '' 10 2 4 4 2 4.4};
% clu.tran.veh = {'Aggregate class' 'Broad habitat' 'LCM2007 class' 'LCM2007 class number' 'Hata branch' 'for i=2 to n-1 in (1c)' 'for i=1 and n in (1c)' 'model (64)' 'SD index' 'K_L' 'R' 'G' 'B'
%     'Broadleaf woodland' '''Broadleaved, Mixed and Yew Woodland''' 'Broadleaved woodland' 1 4 15 15 '' '' '' 232 0 0
%     'Coniferous woodland' '''Coniferous Woodland''' '''Coniferous woodland''' 2 4 15 15 '' '' '' 25 103 16
%     'Arable' '''Arable and Horticulture''' '''Arable and horticulture''' 3 4 4 4 '' '' '' 97 53 0
%     'Improved grassland' '''Improved Grassland''' '''Improved grassland''' 4 4 4 4 '' '' '' 71 252 50
%     'Semi-natural grassland' 'Rough Grassland' 'Rough grassland' 5 4 4 4 '' '' '' 247 171 8
%     'Semi-natural grassland' '''Neutral Grassland''' '''Neutral Grassland''' 6 4 4 4 '' '' '' 139 229 133
%     'Semi-natural grassland' '''Calcareous Grassland''' '''Calcareous Grassland''' 7 4 4 4 '' '' '' 119 148 64
%     'Semi-natural grassland' '''Acid Grassland''' 'Acid grassland' 8 4 4 4 '' '' '' 200 241 193
%     'Semi-natural grassland' '''Fen, Marsh and Swamp''' '''Fen, Marsh and Swamp''' 9 4 4 4 '' '' '' 253 251 38
%     'Mountain, heath, bog' '''Dwarf Shrub Heath''' 'Heather' 10 4 4 4 '' '' '' 129 21 127
%     'Mountain, heath, bog' '''Dwarf Shrub Heath''' 'Heather grassland' 11 4 4 4 '' '' '' 227 139 157
%     'Mountain, heath, bog' '''Bog''' '''Bog''' 12 4 4 4 '' '' '' 37 125 127
%     'Mountain, heath, bog' '''Montane Habitats''' '''Montane Habitats''' 13 4 4 4 '' '' '' 83 255 255
%     'Mountain, heath, bog' '''Inland Rock''' '''Inland Rock''' 14 4 4 4 '' '' '' 215 213 254
%     'Saltwater' 'Saltwater' 'Saltwater' 15 4 4 4 '' '' '' 18 0 145
%     'Freshwater' 'Freshwater' 'Freshwater' 16 4 4 4 '' '' '' 37 0 236
%     'Coastal' '''Supra-littoral Rock''' '''Supra-littoral Rock''' 17 4 4 4 '' '' '' 201 179 22
%     'Coastal' '''Supra-littoral Sediment''' '''Supra-littoral Sediment''' 18 4 4 4 '' '' '' 201 179 22
%     'Coastal' '''Littoral Rock''' '''Littoral Rock''' 19 4 4 4 '' '' '' 252 255 133
%     'Coastal' '''Littoral Sediment''' 'Littoral sediment' 20 4 4 4 '' '' '' 252 255 133
%     'Coastal' '''Littoral Sediment''' 'Saltmarsh' 21 4 4 4 '' '' '' 134 124 253
%     'Built-up areas and gardens' '''Built-up Areas and Gardens''' 'Urban' 22 1 15 15 '' '' '' 1 1 1
%     'Built-up areas and gardens' '''Built-up Areas and Gardens''' 'Suburban' 23 3 10 10 '' '' '' 129 126 134};
% %
% % Below is for no termination losses:
% % clu.tran.veh = {'Title' '' '' 'Index' ...
% %     'SD index for population: 1 urban/SU, 2 other' ...
% %     'for i=2 to n-1 in (1c)' 'for i=1 and n in (1c)' 'model (64)' ...
% %     'K_L (66) for veh'
% %     'Dense Urban' '' '' 1 1 20 0 1 5.1
% %     'Urban' '' '' 2 1 15 0 1 5.1
% %     'Industry' '' '' 3 1 10 0 1 4.9
% %     'Suburban' '' '' 4 1 10 0 1 4.9
% %     'Village' '' '' 5 1 10 0 1 4.9
% %     'Parks/Recreation' '' '' 6 2 0 0 2 4.4
% %     'Open' '' '' 7 2 0 0 2 4.4
% %     'Open in Urban' '' '' 8 2 0 0 2 4.4
% %     'Forest' '' '' 9 2 15 0 1 4.4
% %     'Water' '' '' 10 2 0 0 2 4.4};
% % clu.tran.pv = '';  % Initialise selector of pop veh clutter heights
% 
% % Read the clutter map
% % fid = fopen(fullfile(Sim.dropPath,'Clutter data','InfoTerraUK_sea.txt'));
% % for n = 1 : 16
% %     fgets(fid);
% % end
% % clear n
% % tmp = repmat('%u8 ',1,14406);
% % tmp(end) = [];
% % C = textscan(fid,tmp);
% % clear tmp
% % fclose(fid);
% % % *NB: These values are hard-coded in calc_PL as well for
% % % faster computation
% % clu.A = cell2mat(C);
% % clu.R = maprasterref('RasterSize',[27947 14406],...
% %     'YLimWorld',-97350+[0 27947.*50],'ColumnsStartFrom','north',...
% %     'XLimWorld',-13000+[0 14406.*50]);
% % clear C
% %
% try
%     load('clu','clu')
% catch
%     [wsclu.A,wsclu.R] = geotiffread(fullfile('D:',...
%         'Land cover map 2007','data','lcm2007_25m_raster_GB',...
%         '25m_target_class','lcm2007_25m_gb.tif'));
%     [I(1),J(1)] = worldToSub(wsclu.R,studyA.R.XWorldLimits(1)+eps,...
%         studyA.R.YWorldLimits(2)-eps);
%     [I(2),J(2)] = worldToSub(wsclu.R,studyA.R.XWorldLimits(2)-eps,...
%         studyA.R.YWorldLimits(1)+eps);
%     % figure
%     % imagesc(wsclu.A(I(1):I(2),J(1):J(2)))
%     % axis equal
%     clu.A = wsclu.A(I(1):I(2),J(1):J(2));
%     [XLimWorld(1),YLimWorld(1)] = wsclu.R.intrinsicToWorld(J(1),I(2));
%     [XLimWorld(2),YLimWorld(2)] = wsclu.R.intrinsicToWorld(J(2),I(1));
%     XLimWorld = XLimWorld + wsclu.R.CellExtentInWorldX./2.*[-1 1];
%     YLimWorld = YLimWorld + wsclu.R.CellExtentInWorldY./2.*[-1 1];
%     
%     clu.R = maprasterref('RasterSize',size(clu.A),'YLimWorld',YLimWorld,...
%         'ColumnsStartFrom','north','XLimWorld',XLimWorld);
% 
%     save('clu','clu')
%     
%     clear wsclu I J XLimWorld YLimWorld
% end
end

%% Calculate statistics
function c_stats(clu,studyA)
% c_stats calculates the breakdown statistics of land use.
% 
% studyA is a structure or list of structures with at least the following
% fields:
% X,Y - Coordinates of the study area polygon

% Calculate breakdown
[J,I] = meshgrid(1:clu.R.RasterSize(2),1:clu.R.RasterSize(1));
[X,Y] = clu.R.intrinsicToWorld(J,I);
in2 = false(size(X));

% Calculate breakdown. studyA can be expressed either in a nan-delimited or
% a cell array format; both are supported.
if iscell(studyA.X)
    for n = 1 : length(studyA.X)  % For each cell of the study area of the cell array format
        % Find which clutter pixels are within the bounding box of the polygon
        in1 = find(min(studyA.X{n})<=X(:) & X(:)<=max(studyA.X{n}) & ...
            min(studyA.Y{n})<=Y(:) & Y(:)<=max(studyA.Y{n}));

        % From these within the bounding box, find which are within the polygon
        if any(in1)
            % Convert the polygon of the study area to cell format
            [Xcells,Ycells] = polysplit(studyA.X{n},studyA.Y{n});

            % find which are within the polygon
            for m = 1 : length(Xcells)
                if ispolycw(Xcells{m},Ycells{m})
                    in2(in1(inpoly([X(in1) Y(in1)],[Xcells{m} Ycells{m}]))) = true;
                else
                    in2(in1(inpoly([X(in1) Y(in1)],[Xcells{m} Ycells{m}]))) = false;
                end
            end
        end
    end
else
    % Find which clutter pixels are within the bounding box of the polygon
    in1 = find(min(studyA.X)<=X(:) & X(:)<=max(studyA.X) & ...
        min(studyA.Y)<=Y(:) & Y(:)<=max(studyA.Y));

    % From these within the bounding box, find which are within the polygon
    if any(in1)
        % Convert the polygon of the study area to cell format
        [Xcells,Ycells] = polysplit(studyA.X,studyA.Y);

        % find which are within the polygon
        for m = 1 : length(Xcells)
            if ispolycw(Xcells{m},Ycells{m})
                in2(in1(inpoly([X(in1) Y(in1)],[Xcells{m} Ycells{m}]))) = true;
            else
                in2(in1(inpoly([X(in1) Y(in1)],[Xcells{m} Ycells{m}]))) = false;
            end
        end
    end
end

% Display breakdown
C = unique(clu.A(:));
counts = hist(double(clu.A(in2)),double(min(C):max(C)));
[double(min(C):max(C)).' counts.'./sum(counts)]
end

%% Plot DSM map
function p_map(dsm,studyA)
% Create figure and set axis parameters
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 1])

% Create grid on pixels to plot
% [X,Y] = meshgrid(1:size(clu.A,2),1:size(clu.A,1));
% [X,Y] = clu.R.intrinsicToWorld(X,Y);

% Find which pixels belong to each clutter type
% [C,~,ic] = unique(clu.A(:));

% Plot the clutter pixels
% tran = cell2mat(clu.tran.pop(2:end,4));
% for n = 1 : length(C)
%     if any(tran==C(n))
%         [x,y] = polyjoin(num2cell(bsxfun(@plus,X(ic==n).',50.*[-1 -1 1 1 -1].'),1),...
%             num2cell(bsxfun(@plus,Y(ic==n).',50.*[-1 1 1 -1 -1].'),1));
%         [x2,y2] = polyunion(x,y,'non overlapping');
%         [F,V] = poly2fv(x2,y2);
%         patch('Faces',F,'Vertices',V,...
%             'facecolor',cell2mat(clu.tran.pop([false;tran==C(n)],11:13))./255,...
%             'edgecolor',cell2mat(clu.tran.pop([false;tran==C(n)],11:13))./255)
% %         drawnow
%     end
% end

imagesc(dsm.R.XWorldLimits,dsm.R.YWorldLimits([2,1]),dsm.A)
demcmap([-1,90])
set(gca,'ydir','normal')

% Plot the studyA outline
plot(studyA.X,studyA.Y,'k','linewidth',1)

% Plot decorations
ch1 = colorbar;
% cmap = cell2mat(clu.tran.pop(C+1,11:13));
% set(ch1,'ytick',1:size(cmap,1))
% colormap(cmap./255)
% set(gca,'CLim',[0.5 size(cmap,1)+0.5])

% Map decorations
add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

% Print figure
% print(fh1,fullfile('OutpM','clu'),'-djpeg','-r300')
% savefig(fullfile('OutpM','dsm'))
export_fig(fullfile('OutpM','dsm.jpg'),'-jpg','-q100','-r300')

% Delete figure
delete(fh1)

% fprintf(1,'Plotting the clutter (InfoTerra) data...');
% [X,Y] = meshgrid(1:size(clu.A,2),1:size(clu.A,1));
% [X,Y] = clu.R.intrinsicToWorld(X,Y);
% %
% ph1 = patch(repmat(X(:).',5,1) + ...
%     repmat(clu.R.CellExtentInWorldX./2.*[-1 1 1 -1 -1].',1,numel(X)),...
%     repmat(Y(:).',5,1) + ...
%     repmat(clu.R.CellExtentInWorldY./2.*[-1 -1 1 1 -1].',1,numel(Y)),...
%     repmat(clu.A(:).',5,1),'edgecolor','none','facealpha',0.5);
% %
% ch1 = colorbar;
% cmap = cell2mat(clu.tran.pop(2:end,11:13));
% set(ch1,'ytick',1:size(cmap,1))
% colormap(cmap./255)
% set(gca,'CLim',[0.5 size(cmap,1)+0.5])
% print(Sim.fh1,fullfile('OutpM','clu'),'-djpeg','-r300')
% colorbar('off')
% delete(ph1)
% clear X Y ph1 ch1
% fprintf(1,'DONE\n');
end

%% Plot clutter map
function p_clutter_map(dsm,ter,studyA,Sim)
% Create figure and set axis parameters
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio],'color','w');
hold on
axis equal off
set(gca,'pos',[0 0 1 1])

% Create grid on pixels to plot
% [X,Y] = meshgrid(1:size(clu.A,2),1:size(clu.A,1));
% [X,Y] = clu.R.intrinsicToWorld(X,Y);

% Find which pixels belong to each clutter type
% [C,~,ic] = unique(clu.A(:));

% Plot the clutter pixels
% tran = cell2mat(clu.tran.pop(2:end,4));
% for n = 1 : length(C)
%     if any(tran==C(n))
%         [x,y] = polyjoin(num2cell(bsxfun(@plus,X(ic==n).',50.*[-1 -1 1 1 -1].'),1),...
%             num2cell(bsxfun(@plus,Y(ic==n).',50.*[-1 1 1 -1 -1].'),1));
%         [x2,y2] = polyunion(x,y,'non overlapping');
%         [F,V] = poly2fv(x2,y2);
%         patch('Faces',F,'Vertices',V,...
%             'facecolor',cell2mat(clu.tran.pop([false;tran==C(n)],11:13))./255,...
%             'edgecolor',cell2mat(clu.tran.pop([false;tran==C(n)],11:13))./255)
% %         drawnow
%     end
% end

imagesc(dsm.R.XWorldLimits,dsm.R.YWorldLimits([2,1]),dsm.A-ter.A)
c = jet(16);
c(1:8,:) = [];
c = [[1 1 1]; c];
colormap(c)
set(gca,'clim',[0 12])
set(gca,'ydir','normal')

% Plot the studyA outline
plot(studyA.X,studyA.Y,'k','linewidth',1)

% Plot decorations
ch1 = colorbar;
% cmap = cell2mat(clu.tran.pop(C+1,11:13));
% set(ch1,'ytick',1:size(cmap,1))
% colormap(cmap./255)
% set(gca,'CLim',[0.5 size(cmap,1)+0.5])

% Map decorations
add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

% Print figure
% print(fh1,fullfile('OutpM','clu'),'-djpeg','-r300')
% savefig(fullfile('OutpM','dsm'))
export_fig(fullfile('OutpM','clu.jpg'),'-jpg','-q100','-r300')

% Delete figure
delete(fh1)

% fprintf(1,'Plotting the clutter (InfoTerra) data...');
% [X,Y] = meshgrid(1:size(clu.A,2),1:size(clu.A,1));
% [X,Y] = clu.R.intrinsicToWorld(X,Y);
% %
% ph1 = patch(repmat(X(:).',5,1) + ...
%     repmat(clu.R.CellExtentInWorldX./2.*[-1 1 1 -1 -1].',1,numel(X)),...
%     repmat(Y(:).',5,1) + ...
%     repmat(clu.R.CellExtentInWorldY./2.*[-1 -1 1 1 -1].',1,numel(Y)),...
%     repmat(clu.A(:).',5,1),'edgecolor','none','facealpha',0.5);
% %
% ch1 = colorbar;
% cmap = cell2mat(clu.tran.pop(2:end,11:13));
% set(ch1,'ytick',1:size(cmap,1))
% colormap(cmap./255)
% set(gca,'CLim',[0.5 size(cmap,1)+0.5])
% print(Sim.fh1,fullfile('OutpM','clu'),'-djpeg','-r300')
% colorbar('off')
% delete(ph1)
% clear X Y ph1 ch1
% fprintf(1,'DONE\n');
end
    end
end