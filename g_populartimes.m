function P3 = g_populartimes(studyA,Sim)

% Define place types, plaTy
% https://developers.google.com/places/supported_types#table1
plaTy = {'accounting'
    'airport'
    'amusement_park'
    'aquarium'
    'art_gallery'
    'atm'
    'bakery'
    'bank'
    'bar'
    'beauty_salon'
    'bicycle_store'
    'book_store'
    'bowling_alley'
    'bus_station'
    'cafe'
    'campground'
    'car_dealer'
    'car_rental'
    'car_repair'
    'car_wash'
    'casino'
    'cemetery'
    'church'
    'city_hall'
    'clothing_store'
    'convenience_store'
    'courthouse'
    'dentist'
    'department_store'
    'doctor'
    'drugstore'
    'electrician'
    'electronics_store'
    'embassy'
    'fire_station'
    'florist'
    'funeral_home'
    'furniture_store'
    'gas_station'
    'grocery_or_supermarket'
    'gym'
    'hair_care'
    'hardware_store'
    'hindu_temple'
    'home_goods_store'
    'hospital'
    'insurance_agency'
    'jewelry_store'
    'laundry'
    'lawyer'
    'library'
    'light_rail_station'
    'liquor_store'
    'local_government_office'
    'locksmith'
    'lodging'
    'meal_delivery'
    'meal_takeaway'
    'mosque'
    'movie_rental'
    'movie_theater'
    'moving_company'
    'museum'
    'night_club'
    'painter'
    'park'
    'parking'
    'pet_store'
    'pharmacy'
    'physiotherapist'
    'plumber'
    'police'
    'post_office'
    'primary_school'
    'real_estate_agency'
    'restaurant'
    'roofing_contractor'
    'rv_park'
    'school'
    'secondary_school'
    'shoe_store'
    'shopping_mall'
    'spa'
    'stadium'
    'storage'
    'store'
    'subway_station'
    'supermarket'
    'synagogue'
    'taxi_stand'
    'tourist_attraction'
    'train_station'
    'transit_station'
    'travel_agency'
    'university'
    'veterinary_care'
    'zoo'};

% 500 (urban), 1299 (suburban), 1732 (rural). One macrocell ISD, Rep. ITU-R M.2135-1
% The above roughly correspond to: 15 (urban), 30 arcsec (suburban), 60 arcsec (rural)

% Parse with rural raster
% BoundingBox = [min(x) min(y)
%         max(x) max(y)]
inc = 60;  % Increment in arcsec
[X,Y] = meshgrid(floor(studyA.buf.BoundingBoxLatLon(1,1)./inc.*3600) : ...
    floor(studyA.buf.BoundingBoxLatLon(2,1)./inc.*3600),...
    floor(studyA.buf.BoundingBoxLatLon(1,2)./inc.*3600) : ...
    floor(studyA.buf.BoundingBoxLatLon(2,2)./inc.*3600));

P3 = [];
for n = 1 : numel(X)
    % BoundingBox = [min(x) min(y)
    %     max(x) max(y)];
    BoundingBox = [(X(n)+[0 1]).*inc./3600
        (Y(n)+[0 1]).*inc./3600].';
    
    if BoundingBox(1,2) >= 0
        str = 'N';
    else
        str = 'S';
    end
    str = [str sprintf('%06.3f',abs(BoundingBox(1,2)))];
    if BoundingBox(1,1) >= 0
        str = [str ' E'];
    else
        str = [str ' W'];
    end
    str = [str sprintf('%07.3f',abs(BoundingBox(1,1)))];
    filename = [str '.txt'];
    clear str

    if exist(fullfile(Sim.path.gPla,filename),'file')
        fileID = fopen(fullfile(Sim.path.gPla,filename));
        text = fread(fileID,'*char').';
        fclose(fileID);
        P3 = [P3; jsondecode(text)];
        
%         P2 = jsondecode(text);
%         [C,~,ic] = unique({P2.type});
%         [C.' num2cell(accumarray(ic,ones(size(ic))))]
    else
        t1 = py.tuple({BoundingBox(1,2),BoundingBox(1,1),1,2,'def'});
        t2 = py.tuple({BoundingBox(2,2),BoundingBox(2,1),1,2,'def'});

        P2 = [];
        p_ = randperm(length(plaTy));
        LABEL = 'Parsing places';
        multiWaitbar(LABEL,0);
        for pn = 1 : length(p_)
            % []
            variable = py.populartimes_get.populartimes_get(...
                Sim.GmAPIkey,py.textwrap.wrap(plaTy{p_(pn)}),t1,t2);
        %     job = createJob(c);
        %     createTask(job,@populartimes_get,1,{Sim.GmAPIkey,plaTy{p_(pn)},t1,t2});
        %     submit(job)
        %     wait(job)
        %     job.fetchOutputs
        %     results = arrayfun(@(x) fetchOutputs(x),job,'UniformOutput',false).';
        %     job = [];

        %     timeout(fExpr,25)

            C = cell(variable);
            for Cn = 1 : length(C)
                P2n = length(P2) + 1;

                P = struct(C{Cn});
                P2(P2n).name = char(P.name);

                if isfield(P,'time_spent')
                    P2(P2n).time_spent = uint16(cellfun(@int64,cell(P.time_spent)));  % Typical time spent in minutes
                end

                P2(P2n).type = plaTy{p_(pn)};
                P2(P2n).types = cellfun(@char,cell(P.types),'UniformOutput',false);
                P2(P2n).address = char(P.address);
                P2(P2n).lat = struct(P.coordinates).lat;
                P2(P2n).lon = struct(P.coordinates).lng;
                P2(P2n).id = char(P.id);
                P2(P2n).populartimes = uint8(cell2mat(arrayfun(@(y) cellfun(@(x) int64(x),...
                    cell(y.data)),cellfun(@(x) struct(x),cell(P.populartimes)).',...
                    'UniformOutput',false)));
            end

    %         disp(['Place size: '  num2str(length(P2))])
            pause(15.*rand())

            multiWaitbar(LABEL,pn./length(p_));
        end
        multiWaitbar(LABEL,'Close');
        clear C Cn P pn t1 t2

        fileID = fopen(fullfile(Sim.path.gPla,filename),'w');
        text = jsonencode(P2);
        fprintf(fileID,'%s',text);
        fclose(fileID);
        P3 = [P3; jsondecode(text)];
    end
end

[~,ia] = unique(strcat({P3.id}.',{P3.type}.'));
assert(length(ia)==length(P3),'There are duplciates to be removed')

% figure
% plot([P3.lon],[P3.lat],'.')
