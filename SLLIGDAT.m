classdef SLLIGDAT
    % SLLIGDAT class for management of the lighting database
    % The SLLIGDAT class is responsible for setting the properties of [].
    % More specifically it is responsible for delivering the following:
    %
    % *	creates the SLLIGDAT object
    % * []
    
    properties
        X;  % Easting
        Y;  % Northing
        poTyp;  % Pole type
        heigm;  % Height above local ground in metres
    end
    
    methods
%% Read Slough Lighting Data
function SlLigDat = SLLIGDAT(Sim,studyA)
[~,~,raw] = xlsread(fullfile(Sim.path.on,'RND Server - General',...
    'Slough','Slough Lighting Data.xlsx'));
SlLigDat.X = cell2mat(raw(4:12598,7));
SlLigDat.Y = cell2mat(raw(4:12598,8));
SlLigDat.poTyp = raw(4:12598,10);
IND = SlLigDat.X == 0;
SlLigDat.X(IND) = [];
SlLigDat.Y(IND) = [];
SlLigDat.poTyp(IND) = [];
for n = 1 : size(SlLigDat.X,1)
    if strfind(SlLigDat.poTyp{n},'2m')
        SlLigDat.heigm(n,1) = 2;
    elseif strfind(SlLigDat.poTyp{n},'3M')
        SlLigDat.heigm(n,1) = 3;
    elseif strfind(SlLigDat.poTyp{n},'3m')
        SlLigDat.heigm(n,1) = 3;
    elseif strfind(SlLigDat.poTyp{n},'4m')
        SlLigDat.heigm(n,1) = 4;
    elseif strfind(SlLigDat.poTyp{n},'5M')
        SlLigDat.heigm(n,1) = 5;
    elseif strfind(SlLigDat.poTyp{n},'5m')
        SlLigDat.heigm(n,1) = 5;
    elseif strfind(SlLigDat.poTyp{n},'6M')
        SlLigDat.heigm(n,1) = 6;
    elseif strfind(SlLigDat.poTyp{n},'6m')
        SlLigDat.heigm(n,1) = 6;
    elseif strfind(SlLigDat.poTyp{n},'7M')
        SlLigDat.heigm(n,1) = 7;
    elseif strfind(SlLigDat.poTyp{n},'8M')
        SlLigDat.heigm(n,1) = 8;
    elseif strfind(SlLigDat.poTyp{n},'8m')
        SlLigDat.heigm(n,1) = 8;
    elseif strfind(SlLigDat.poTyp{n},'8mtr')
        SlLigDat.heigm(n,1) = 8;
    elseif strfind(SlLigDat.poTyp{n},'10M')
        SlLigDat.heigm(n,1) = 10;
    elseif strfind(SlLigDat.poTyp{n},'10m')
        SlLigDat.heigm(n,1) = 10;
    elseif strfind(SlLigDat.poTyp{n},'10 FAB')
        SlLigDat.heigm(n,1) = 10;
    elseif strfind(SlLigDat.poTyp{n},'12m')
        SlLigDat.heigm(n,1) = 12;
    elseif strfind(SlLigDat.poTyp{n},'12M')
        SlLigDat.heigm(n,1) = 12;
    elseif strfind(SlLigDat.poTyp{n},'UNKNOWN')
        SlLigDat.heigm(n,1) = nan;
    elseif strfind(SlLigDat.poTyp{n},'Sugg custom tapered column')
        SlLigDat.heigm(n,1) = nan;
    elseif strfind(SlLigDat.poTyp{n},'No post')
        SlLigDat.heigm(n,1) = nan;
    else
        gfdgsfgs
    end
end

% Clip Read Slough Lighting Data within study area
IN = inpolygon(SlLigDat.X,SlLigDat.Y,studyA.buf.X,studyA.buf.Y);
SlLigDat.X(~IN) = [];
SlLigDat.Y(~IN) = [];
SlLigDat.poTyp(~IN) = [];
SlLigDat.heigm(~IN) = [];
end

%%
function p_SlLigDat(SlLigDat,studyA,Sim)
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio])
set(gca,'DataAspectRatio',[10 10 1])
set(gca,'pos',[0 0 1 1])
hold on
[C,~,ic] = unique(SlLigDat.heigm);
c = jet(nnz(~isnan(C)));
for n = 1 : size(c,1)
    IND = ic == n;
    line(SlLigDat.X(IND).'.*ones(2,1),...
        SlLigDat.Y(IND).'.*ones(2,1),...
        [zeros(1,nnz(IND));SlLigDat.heigm(IND).'],'color',c(n,:),...
        'linewidth',2)
end
% plot3(SlLigDat.X,SlLigDat.Y,SlLigDat.heigm,'ko')
view(10,70)
axis off
camproj('perspective')

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap',...
    'brighten',0.5,'grayscale',1)

warning off all
export_fig(fullfile('OutpM','SlLigDat.jpg'),'-djpeg','-r600')
warning on all
delete(gcf)
end

%%
function p_v_SlLigDat(SlLigDat,studyA,Sim,demPerImpro)
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio])
set(gca,'DataAspectRatio',[10 10 1])
set(gca,'pos',[0 0 1 1])
hold on
view(10,70)
axis off
camproj('perspective')

% Sort the lampposts
demPerImpro(isnan(demPerImpro)) = 0;
[~,I] = sort(demPerImpro./max(demPerImpro,[],1),'descend');

% Plot the hoi poloi
line(SlLigDat.X(I(2:end)).'.*ones(2,1),...
    SlLigDat.Y(I(2:end)).'.*ones(2,1),...
    [zeros(1,size(SlLigDat.X(I(2:end)),1));SlLigDat.heigm(I(2:end)).'],...
    'color',[0.5 0.5 0.5],'linewidth',2)

% Plot the top 5
IND = [1,3,16,26];
demPerImpro(I(IND))
line(SlLigDat.X(I(IND)).'.*ones(2,1),SlLigDat.Y(I(IND)).'.*ones(2,1),...
    [zeros(1,size(SlLigDat.X(I(IND)),1));SlLigDat.heigm(I(IND)).'],...
    'color','r','linewidth',2)
plot3(SlLigDat.X(I(IND)),SlLigDat.Y(I(IND)),SlLigDat.heigm(I(IND)),'ro',...
    'linewidth',2)
for n = 1 : length(IND)
    text(SlLigDat.X(I(IND(n)))+5,SlLigDat.Y(I(IND(n)))+5,...
        SlLigDat.heigm(I(IND(n)))+5,num2str(n),'fontweight','bold',...
        'back','w','Margin',1)
end

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap',...
    'brighten',0.5,'grayscale',1)

warning off all
export_fig(fullfile('OutpM','SlLigDat v 1.jpg'),'-djpeg','-r600')
warning on all
delete(gcf)
end
    end
end
        