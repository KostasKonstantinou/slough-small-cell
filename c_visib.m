function visib = c_visib(MS,Cell,fn,studyA,bund1,thisDem,fs1,Sim,bund2,...
    SINR2tput,isCell)
%% Calculate all the 'visible' links in the system level simulation
% c_visib() returns visib, a structure containing parameters of all the
% propagation paths for this year, family of use cases (i.e. service),
% traffic class (e.g. streaming), frequency band (multiple channels). 
% c_visib contains the following fields:
% studyAind - An Mx1 vector of linear indices (not multiple subscripts,
%             i.e. row, column) to the studyA object of where the UE of
%             each path is located. M is the number of propagation paths.
% thisDem_IND - An Mx1 vector of indices to thisDem objects of this year
%               that point to the MS/UE of each path.
% SE_DL - An MxN vector of downlink spectral efficiency of each path,
%         expressed in bit/s/Hz. N is the number of servers allowed for
%         via the multi-connectivity enabler, e.g. N=2 for a service that
%         supports dual connectivity. The first column refers to the best
%         server, the second to the second best, etc.
% noisFl_DL - An Mx1 vector of the noise floor (thermal+figure) of each
%             path in downlink, expressed in dBm/180kHz.
% ii - An Mx1 cell vector containing information on the received signal
%      strength from all assessed-path ('visible' from the propagation
%      module) BS. Each cell is an Ax4 matrix, where A is the number of
%      visible BS. The 4 columns are: 1) the site index within Cell, 2) the
%      sector index within the site (e.g. 1,2,3 for 3-sector sites,
%      representing sectors pointing 0,120,240 deg due north), 3) received
%      signal strength in dBm/180kHz, 4) standard deviation due to slow
%      fading (shadowing) in dB.
% SINRthres_DL - An MxB matrix, where B=Sim.utilisN*N, containing the SINR
%                threshold value required to provide the service throughput
%                requirement for a variable network load (spectrum resource
%                utilisation), in dB. For example, with Sim.utilisN=10,
%                SINRthres_DL(2,3) is the SINR threshold value for the UE
%                of the 2nd path, best server, and 3rd quantised network
%                load value i.e. 25%. For another example,
%                SINRthres_DL(9,16) is the SINR threshold value for the UE
%                of the 9th path, second best server, and 6th quantised
%                network load value.
% distm - An MxN vector of the 2D separation distance between the
%         communication ends (BS and UE) for each path, in metres.
% bund1 - An MxN vector of indices to bund1.ia for each path.
% bund2 - An Mx1 vector of indices to bund2.ia for each path.
% faFadMargi - An MxN vector of the fast fade margin for each
%              path, expressed in dB. The first column is the fast fade
%              margin if the connection is done to a single server, the
%              second column is the fast fade margin if the connection is
%              done to a couple of servers, etc.
% NLayers - An MxB matrix of the number of tranmission layers for which the
%           SINRthres_DL value corresponds to.
% pwrAmpClasMS - An Mx1 vector of cells, each containing a string that
%                indicates the power amplifier class of the MS/UE, must be
%                one of Sim.linBud.UE.pwrAmpClas_.
% situMS - An Mx1 vector of cells, each containing a string that indicates
%          the situation of the MS/UE, must be one of Sim.linBud.UE.situ_.
% famI - An Mx1 vector of indices to Sim.dem.famUseCase_.
% 
% The following are for uplink and are currently commented out:
% PRx_across_system_BW_UL - An Mx1 vector of uplink received signal
%                           strength across system BW.
% Pt - An Mx1 vector of uplink power controled output power as defined in
%      36.942.
% SINRthres_UL - An Mx1 vector of uplink SINR threshold to deliver the
%                service for each path, expressed in dB.
%  
% The input MS is a structure relevant to the MS parameters which we wish
% propagation paths for, and must contain the following fields:
% IND - The index of the MS within Sim.linBud.UE.
% pwrAmpClas - The MS power amplifier class, must be one of
%              Sim.propag.UE(:,1) and of
%              Sim.dem.trafVol.(fam).devic.pwrAmpClas.
% situ - The MS situation, must be one of Sim.dem.trafVol.(fam).devic.situ.
% 
% Cell is a array of CELL objects, and must contain the following fields:
% Lbc, MCL - Median path loss, and minimum coupling loss
% Operator - Operator of BS
% HPBWhor, HPBWver - Antenna characteristics
% tilt - Cell tilt
% EIRP_ovAllAnt_10MHz_100pcLoad_dBm - Cell output power
% maxSuppAppliDatRat, maxSuppVel - Admission control parameters
% band.capa - Cell installed spectrum quantity
% It is usual that only some of the simulation's CELL objects are passed as
% input to c_visib. The input isCell is an index of the CELL objects that
% are passed as inputs to c_visib within the simulation's CELL object
% array.
% See class for more information on its properties.
% 
% The input fn is an index to Sim.linBud.spotFreq.
% 
% studyA is a structure or list of structures with at least the following
% fields:
% INfoc - Logical matrix at the size of the simulation raster whose
%         elements are true at locations which are within focus.
% R.RasterSize - Simulation area raster size.
% 
% The input structure bund1 contains the following fields:
% C - A M�1 vector of structures, where M=length(Cell_of_parent_function),
%     of the bundling parameters, Cell_of_parent_function is the array of
%     CELL objects of the parent function, hence use of isCell, e.g.
%     bund1.C(isCell(Cell)).
% ia - The ia index vector as returned by function unique.
% ic - The ic index vector as returned by function unique.
% 
% The input bund2 vector of M cells (M traffic classes), each containing a
% logical 1xN array whose elements are true when the UE type (N UE types)
% is considered for this traffic class thisDem is an array of demand
% objects and must contain the following fields, see class for definition:
% ia - The ia index vector as returned by function unique.
% 
% The input thisDem is an array of demand objects, contains the following
% fields:
% IND - Index within the simulation area raster
% aeCou - Number of antennas at the UE
% satis - Logical vector with true if demand satisfied in both coverage and
%         capacity aspects
% See class for more information on its properties.
% 
% The input fs1 = Sim.linBud.str{fn}.
% 
% Sim is a SIM object and must contain the following fields,
% see class for definition:
% dem.trafClas_ - List of traffic class strings
% dem.modeAlTrafClasAs - Override instructions to model all traffic classes
%                        as a single one
% dem.famUseCase_ - List of strings of teh family of use cases (later
%                   called services)
% dem.hourDistri - Matrix of the percentage of traffic associated with the
%                  modelled hours
% dem.trafVol.(fam).CEtput.ServCouMax - Maximum number of servers supported
%                                       by the service
% dem.trafVol.(fam).CEtput.DL - Downlink throughput requirement at
%                               cell-edge
% dem.trafVol.(fam).CEtput.UL - Uplink throughput requirement at cell-edge
% dem.trafVol.(fam).CEtput.servFacto - Service factor (multiplicative
%                                      factor to the default SE) for this
%                                      service
% dem.operators - List of operator strings
% dem.mobiClaVel - Maximum (service requirement) velocity of the mobility
%                  class
% yea.thisYea - Scalar of the current year
% yea.start - Scalar of the starting year of simulation
% shari.from - Scalar of the first year sharing started from
% shari.is - Logical matrix that indicates which service can be hosted by
%            which infrastructure provider
% demTranslati - Mirrored abbrevation of dem.translati
% linBud.isSupp(famI,fn) - Logical matrix that indicates if a service can
%                          be hosted by a particular band
% linBud.freq - Vector of modelled frequencies
% linBud.(pwrAmpClasBS).(situBS).BPGmean.(fs1).ur - Building penetration
%                                                   gain (mean, urban) for
%                                                   frequency fs1
% linBud.(pwrAmpClasBS).(situBS).GA.(fs1) - BS antenna gain
% linBud.(pwrAmpClasBS).(situBS).GB.(trafCl) - BS body loss
% linBud.(pwrAmpClasBS).(situBS).GC - BS cabling and connector gain
% linBud.(pwrAmpClasMS).(situMS).GA.(fs1) - MS antenna gain
% linBud.(pwrAmpClasMS).(situMS).GB.(trafCl) - MS body loss
% linBud.(pwrAmpClasMS).(situMS).GC - MS cabling and connector gain
% linBud.(pwrAmpClasMS).(situMS).NoiFloo - Downlink noise floor
% linBud.(pwrAmpClasMS).(situMS).NoiFigu.(fs1) - MS noise figure
% linBud.AeCouBS_ - Vector of BS antenna count, for which link budget is
%                   pre-calculated
% linBud.AeCouMS_ - Vector of MS antenna count, for which link budget is
%                   pre-calculated
% linBud.FBMC_ - Vector of FBMC parameters, for which link budget is
%                pre-calculated
% linBud.BW_ - Vector of bandwidth values, for which link budget is
%              pre-calculated 
% linBud.interfSuppr - Vector of interference suppression values for each
%                      modelled year
% linBud.dop - Doppler margin
% linBud.BS.pwrAmpClas_ - List of BS power amplifier classes, for which
%                         link budget is pre-calculated
% linBud.SINRmean - Mean SINR assumption as read from the link budget csv
% propag.UE - Cell matrix containing information on the UE properties
% propag.Am - Antenna characteristic parameter
% propag.SLAv - Antenna characteristic parameter
% servDem.isInterfeExpl.cov - Logical scalar that is true if interference
%                             is calculated explicitly for coverage
%                             assessment
% servDem.isInterfeExpl.capa - Logical scalar that is true if interference
%                             is calculated explicitly for capacity
%                             assessment
% utilisN - Number of interference indices
% 
% SINR2tput is a SINR2tput object and must contain the following fields,
% see class for definition.
% 
% * v1.0 Kostas Konstantinou Oct 2016 - Aug 2019

% Initialise
visib = [];

% Return if Cell is empty
if isempty(Cell)
    return
end

% Simplify
trafClI = find(Sim.dem.modeAlTrafClasAs);
trafCl = Sim.dem.trafClas_{trafClI};

% If there are cells that can be used to serve this family of use cases
% (i.e. service), the considered traffic class (e.g. streaming), and
% operate in the frequency band, then proceed. Otherwise, the output visib
% is empty.
for famI = 1 : length(Sim.dem.famUseCase_)  % For each family of services
    % Simplifications.
    % Horizontal vector isCell2 contains the site indices that can be used
    % to serve this family.
    BHcou = size(Sim.dem.hourDistri,1);
    aeCou = reshape([thisDem.aeCou],[],length(thisDem)).';
    fam = Sim.dem.famUseCase_{famI};
    isCell2 = isCell{famI};
    ServCouMax = Sim.dem.trafVol.(fam).CEtput.ServCouMax(trafClI);
    CEtputI = find(Sim.dem.trafVol.(fam).CEtput.yea==Sim.yea.thisYea) + ...
        (0:length(Sim.dem.trafClas_)-1);
    
    % Next iteration if there is no cell that can serve this family of
    % services
    if isempty(isCell2)
        continue
    end
    
    % Find which columns of thisDem are related to the family of services
    % and traffic class in question
    IND2c = strcmp(Sim.demTranslati(:,1),fam) & ...
        strcmp(Sim.demTranslati(:,2),trafCl);

    % For each UE power amplifier class and situation, find propagation
    % paths that are relevant and append in output visib.
    for tn = 1 : length(MS(famI).pwrAmpClas)
        % Find the power amplifier class and situation of the UE
        pwrAmpClasMS = MS(famI).pwrAmpClas{tn};
        situMS = MS(famI).situ{tn};

        % If the generic service can be served by this band, then []
        if Sim.linBud.isSupp(famI,fn)
            % Get from each CELL object the propagation data for the
            % frequency band and UE type in question, and deal the data
            % into:
            % row,col - UE location within the study area raster for which
            %           the propagation entry corresponds to
            % Lbc - median path loss in dB
            % sigmaLe - standard devation of shadowing, in dB
            % distm - 2D separation distance between BS and MS, in metres
            % v2 - the BS to UE unit vector
            % raiAtmDB - rain and atmospheric absorption attenuation, in dB
            % Celli is the index to CELL which the propagation entry is
            % for.
            IND = strcmp(pwrAmpClasMS,Sim.propag.UE(:,1)) & ...
                strcmp(fam,Sim.propag.UE(:,5));
            if length(Cell) > 1
                tmp = arrayfun(@(x) x.Lbc{fn,IND},Cell.',...
                    'UniformOutput',false);
                Celli = cell2mat(cellfun(@(x,y) ones(size(x,1),1)*y,...
                    tmp,num2cell((1:size(tmp,1)).'),'UniformOutput',false));
                tmp = cell2mat(tmp);
            else
                tmp = Cell.Lbc{fn,IND};
                Celli = ones(size(tmp,1),1);
            end
            if ~isempty(tmp)
                if Sim.linBud.spotFreq(fn) < 6000
                    tmp = mat2cell(tmp,size(tmp,1),[1,1,1,1,1,3]);
                else
                    tmp = mat2cell(tmp,size(tmp,1),[1,1,1,1,1,3,1]).';
                end
                if Sim.linBud.spotFreq(fn) < 6000
                    [row,col,Lbc,sigmaLe,distm,v2] = deal(tmp{:});
                    raiAtmDB = zeros(size(Lbc));
                else
                    [row,col,Lbc,sigmaLe,distm,v2,raiAtmDB] = ...
                        deal(tmp{:});
                end
            else
                [row,col,Lbc,sigmaLe,distm,v2] = deal([]);
            end
            clear tmp

            % If there are any propagation entries for this frequency band
            % and this UE power amplifier class and situation, then
            % proceed. Otherwise, assess next UE power amplifier class and
            % situation.
            if ~isempty(Celli)
                % Prepare indices of assessed paths.
                % Vector studyAind is the index within studyA of each path.
                % Vector bund1IcCelli is the index within isCell2 of each
                % path.
                studyAind = sub2ind(size(studyA.INfoc),row,col);
                bund1IcCelli = bund1.ic(isCell2(Celli));

                % Simplification.
                % OperatorI is the index to the InP that corresponds to the
                % assessed path.
                OperatorI = arrayfun(@(x) find(strcmp(x,Sim.dem.operators)),{Cell.Operator}).';
                OperatorI = OperatorI(Celli);
                
                % Remove propagation entries (paths) whose pixel is not
                % within thisDem. The propagation is assessed for all the
                % pixels that may be requested within the simulation
                % time frame and regardless of family of use. The input
                % thisDem is likely to be a subset of the pixels which we
                % have assessed propagation for. The logical vector
                % isReducReso has a misleading name (is reduced
                % resolution), and should be changed to something more
                % relevant in the future.
%                 figure
%                 axis equal
%                 hold on
%                 plot(col,row,'x')
%                 plot([thisDem.J],[thisDem.I],'ro')
%                 set(gca,'ydir','reverse')
                [~,Locb] = ismember(studyAind,[thisDem.IND]);
                isReducReso = Locb == 0;
                Locb(isReducReso) = [];
                Celli(isReducReso) = [];
                OperatorI(isReducReso) = [];
                
                % Find if the demand is fully satisfied
                [C,~,ic] = unique(Locb,'stable');
                tmp = false(length(C),length(Sim.dem.operators));
                tmp2 = false(length(OperatorI),length(Sim.dem.operators));
                for on = 1 : length(Sim.dem.operators)
                    % For each tenant, find if the demand is fully
                    % satisfied
                    IND2 = strcmp(Sim.demTranslati(:,1),fam) & ...
                        strcmp(Sim.demTranslati(:,2),trafCl) & ...
                        strcmp(Sim.demTranslati(:,3),pwrAmpClasMS) & ...
                        strcmp(Sim.demTranslati(:,4),situMS) & ...
                        strcmp(Sim.demTranslati(:,5),Sim.dem.operators{on});
                    if any(IND2)
                        satis = reshape([thisDem(C).satis],...
                            [BHcou,length(IND2),length(C)]);
                        tmp(:,on) = squeeze(all(all(satis(:,IND2,:),2),1));
                    else
                        tmp(:,on) = true;
                    end
                    
                    % Find if the tenant can be served by the InP of the path
                    if Sim.yea.thisYea >= Sim.shari.from
                        k = find(Sim.shari.is(:,on));
                    else
                        k = on;
                    end
                    if length(k) == 1
                        tmp2(:,on) = OperatorI == k;
                    elseif length(k) > 1
                        tmp2(:,on) = any(bsxfun(@eq,OperatorI,k.'),2);
                    else
                        sfgfre
                    end
                end
                isSatis = all(tmp(ic,:)|~tmp2,2);  % if satisfied or cannot be served by InP, then remove
%                 clear C ic tmp tmp2 IND2 k
                
                % Clear variables that are no longer used to free up memory
                clear Locb
                
                % If there is any propagation entry that correspnds to a
                % demand point that has not been satisfied, then proceed.
                % Otherwise, assess next UE power amplifier class and
                % situation.
                if ~all(isSatis)
                    % Remove propagation entries (paths) whose pixel is not
                    % within thisDem; second phase.
                    Lbc(isReducReso) = [];
                    sigmaLe(isReducReso) = [];
                    distm(isReducReso) = [];
                    v2(isReducReso,:) = [];
                    raiAtmDB(isReducReso) = [];
                    studyAind(isReducReso) = [];
                    bund1IcCelli(isReducReso) = [];
                    
                    % Clear variables that are no longer used to free up
                    % memory.
                    clear isReducReso
                    
                    % Delete propagation entries for paths whose demand is
                    % fully satisfied.
                    Lbc(isSatis) = [];
                    sigmaLe(isSatis) = [];
                    distm(isSatis) = [];
                    v2(isSatis,:) = [];
                    raiAtmDB(isSatis) = [];
                    Celli(isSatis) = [];
                    studyAind(isSatis) = [];
                    bund1IcCelli(isSatis) = [];
%                     OperatorI(isSatis) = [];
                    
                    % Clear variables that are no longer used to free up
                    % memory
                    clear isSatis

                    % Find unique values in bund1IcCelli.
                    % We find the unique sites, because some operations are
                    % per site, and we wish to reduce runtime. For example,
                    % the path loss, and attenuation due to antenna patter
                    % are only needed to performed once.
                    bund1IcCelli_C = unique(bund1IcCelli);

                    % Calculate the median path loss including rain,
                    % atmospheric absorption attenuation, and building
                    % penetration, PL, in dB.
                    % Calculate the median coupling loss, CL, in dB.
                    HPBWhor = reshape([Cell.HPBWhor],...
                        length(Sim.linBud.freq),length(Cell)).';
                    HPBWver = reshape([Cell.HPBWver],...
                        length(Sim.linBud.freq),length(Cell)).';
                    MCL = [Cell.MCL].';
                    MCL = MCL(Celli);
                    CL = cell(length(bund1IcCelli_C),1);
                    for pn = 1 : length(bund1IcCelli_C)
                        % Find propagation entries that correspond to this
                        % bundle1
                        IND = bund1IcCelli == bund1IcCelli_C(pn);
                        
                        if any(IND)
                            % Simplification
                            pwrAmpClasBS = bund1.C(bund1.ia(bund1IcCelli_C(pn))).pwrAmpClas;
                            situBS = bund1.C(bund1.ia(bund1IcCelli_C(pn))).situ;
                            sectoCou = bund1.C(bund1.ia(bund1IcCelli_C(pn))).sectoCou;
                            if length(isCell2) == 1
                                tilt = Cell(bund1.ic(isCell2)==bund1IcCelli_C(pn)).tilt;
                            else
                                tilt = [Cell(bund1.ic(isCell2)==bund1IcCelli_C(pn)).tilt];
                            end
                            
                            % Calculate the antenna boresight unit vector,
                            % v1
                            % [length(sites)*length(sectors) within
                            % bund1.ic(isCell2)==bund1IcCelli_C(pn),3D]
                            % The first dimension is structured as in the
                            % following: Site1_Sector1, Site2_Sector1, etc.
                            az0 = linspace(0,360-360./(sectoCou),...
                                sectoCou).';
                            x = bsxfun(@times,cosd(90-az0),...
                                sind(90+tilt(fn,:))).';
                            y = bsxfun(@times,sind(90-az0),...
                                sind(90+tilt(fn,:))).';
                            z = repmat(cosd(90+tilt(fn,:)).',1,sectoCou);
                            v1 = [x(:) y(:) z(:)];

                            % Calculate total pathloss, i.e. path loss +
                            % BPL mean, for all assessed paths
                            PL = Lbc(IND) + raiAtmDB(IND) - ...
                                Sim.linBud.(pwrAmpClasBS).(situBS).BPGmean.(fs1).ur - ...
                                Sim.linBud.(pwrAmpClasMS).(situMS).BPGmean.(fs1).ur;
                            
                            % Calculate the antenna pattern losses
                            % az - Azimuth of v2 wrt v1.
                            % elev - Elevation of v2 wrt v1.
                            % AH - Attenuation due to horizontal pattern.
                            % AV - Attenuation due to vertical pattern.
                            % GainM - Gain including boresight gain, body
                            %         loss, cabling and connector loss,
                            %         attenuation due to patterns.
                            % Celli2 - Version of Celli for the
                            %          propagation entries of this bundle1,
                            %          i.e. Celli(IND). Celli2 are indices
                            %          within bund1.ic(isCell2)==bund1IcCelli_C(pn), whereas
                            %          Celli(IND) are indices within
                            %          Cell(isCell2).
                            %          Note that Celli2 is redefined later
                            %          in this function.
                            %          Note that az has sectoCou
                            %          times the entries of elev;
                            %          v1(Celli2,3) is the z-dimension of
                            %          v1 for each site. This is because
                            %          the sectorisation is irrelevant to
                            %          attenuation due to elevation angle.
                            % Celli3 - Extended version of Celli2 by that
                            %          many times as the number of sectors.
                            if length(unique(Celli(IND))) == 1
                                % There is only site remaining in Celli
                                Celli2 = ones(nnz(IND),1);
                            else
                                % There are several sites remaining in
                                % Celli
                                if ~all(bund1.ic==bund1IcCelli_C(pn))
                                    bund1_S = sparse(1:nnz(bund1.ic(isCell2)==bund1IcCelli_C(pn)),...
                                        isCell2(bund1.ic(isCell2)==bund1IcCelli_C(pn)),true,...
                                        nnz(bund1.ic(isCell2)==bund1IcCelli_C(pn)),length(bund1.ic));
                                    [Celli2,~] = find(bund1_S(:,...
                                        isCell2(Celli(IND))));
                                else
                                    Celli2 = Celli;
                                end
                            end
                            Celli3 = reshape(bsxfun(@plus,Celli2,...
                                (0:sectoCou-1).*nnz(bund1.ic(isCell2)==bund1IcCelli_C(pn))),...
                                sectoCou*length(Celli2),1);
                            az = wrapTo180(repmat(atan2d(v2(IND,2),...
                                v2(IND,1)),sectoCou,1) - ...
                                atan2d(v1(Celli3,2),v1(Celli3,1)));
                            elev = abs(acosd(v2(IND,3)) - ...
                                acosd(v1(Celli2,3)));
                            AH = min(12.*(az./repmat(HPBWhor(Celli(IND),fn),...
                                sectoCou,1)).^2,Sim.propag.Am);
                            AV = min(12.*(elev./HPBWver(Celli(IND),fn)).^2,...
                                Sim.propag.SLAv);
                            GainM = Sim.linBud.(pwrAmpClasBS).(situBS).GA.(fs1) + ...
                                Sim.linBud.(pwrAmpClasBS).(situBS).GB.(trafCl) + ...
                                Sim.linBud.(pwrAmpClasBS).(situBS).GC + ...
                                Sim.linBud.(pwrAmpClasMS).(situMS).GA.(fs1) + ...
                                Sim.linBud.(pwrAmpClasMS).(situMS).GB.(trafCl) + ...
                                Sim.linBud.(pwrAmpClasMS).(situMS).GC - ...
                                min(AH+repmat(AV,sectoCou,1),Sim.propag.Am);
                            
%                             figure
%                             hold on
%                             axis equal
%                             set(gca,'ydir','reverse')
%                             colorbar
%                             [row,col] = ind2sub(studyA.R.RasterSize,...
%                                 studyAind(Celli==101));
%                             %
% %                             scatter(col,row,6,az(Celli3==find(bund1_S(:,101))),'s','filled')
% %                             scatter(col,row,6,az(Celli3==find(bund1_S(:,101))+size(bund1_S,1)),'s','filled')
%                             scatter(col,row,6,az(Celli3==find(bund1_S(:,101))+2*size(bund1_S,1)),'s','filled')
%                             %
%                             plot(Cell(101).J,Cell(101).I,'ro')
%                             plot(Cell(101).J,Cell(101).I,'r.')
                            
%                             figure
%                             hold on
%                             axis equal
%                             set(gca,'ydir','reverse')
%                             colorbar
%                             [row,col] = ind2sub(studyA.R.RasterSize,...
%                                 studyAind(Celli==101));
%                             %
% %                             scatter(col,row,6,AH(Celli3==find(bund1_S(:,101))),'s','filled')
% %                             scatter(col,row,6,AH(Celli3==find(bund1_S(:,101))+size(bund1_S,1)),'s','filled')
%                             scatter(col,row,6,AH(Celli3==find(bund1_S(:,101))+2*size(bund1_S,1)),'s','filled')
%                             %
%                             plot(Cell(101).J,Cell(101).I,'ro')
%                             plot(Cell(101).J,Cell(101).I,'r.')

%                             figure
%                             hold on
%                             axis equal
%                             set(gca,'ydir','reverse')
%                             colorbar
%                             [row,col] = ind2sub(studyA.R.RasterSize,...
%                                 studyAind(Celli==101));
%                             %
% %                             scatter(col,row,6,GainM(Celli3==find(bund1_S(:,101))),'s','filled')
% %                             scatter(col,row,6,GainM(Celli3==find(bund1_S(:,101))+size(bund1_S,1)),'s','filled')
%                             scatter(col,row,6,GainM(Celli3==find(bund1_S(:,101))+2*size(bund1_S,1)),'s','filled')
%                             %
%                             plot(Cell(101).J,Cell(101).I,'ro')
%                             plot(Cell(101).J,Cell(101).I,'r.')

                            % Calculate coupling loss.
                            % When bound at coupling loss, we insert a
                            % small additional loss to differentiate across
                            % sectors.
                            CL{pn} = max(repmat(PL,sectoCou,1) - ...
                                GainM,repmat(MCL(IND),sectoCou,1));
                            CL{pn} = CL{pn} + AH.*1e-9;
                            
%                             figure
%                             hold on
%                             axis equal
%                             set(gca,'ydir','reverse','clim',[90 140])
%                             colorbar
%                             %
%                             [row,col] = ind2sub(studyA.R.RasterSize,...
%                                 studyAind(Celli==101));
% %                             scatter(col,row,6,CL{bund1IcCelli_C(pn)}(Celli3==find(bund1_S(:,101))),'s','filled')
% %                             scatter(col,row,6,CL{bund1IcCelli_C(pn)}(Celli3==find(bund1_S(:,101))+size(bund1_S,1)),'s','filled')
%                             scatter(col,row,6,CL{bund1IcCelli_C(pn)}(Celli3==find(bund1_S(:,101))+2.*size(bund1_S,1)),'s','filled')
%                             %
%                             plot([Cell.J],[Cell.I],'ko')
%                             plot([Cell.J],[Cell.I],'k.')
%                             %
%                             plot(Cell(101).J,Cell(101).I,'ro',...
%                                 'markerfacecolor','r')
%                             plot(Cell(101).J,Cell(101).I,'r.')
%                             %
%                             colormap('jet')
%                             %
%                             title('Coupling loss  (dB)')
                        end
                    end
                    clear pn pwrAmpClasBS situBS IND elev AH AV GainM PL v1 HPBWver MCL Celli2 Celli3 sectoCou

                    clear v2
                    
        %             figure
        %             hist(PL)
        %             grid on
        %             xlabel('path loss + BPL mean  (dB)')
        %             ylabel('number of pixels with the value of the x-axis')

                    % Find which sites are referenced in the assessed paths
                    [C,~,ic] = unique(Celli,'stable');
                    S = sparse(1:length(ic),ic,true);

                    % Calculate EIRP_ovAllAnt_10MHz_100pcLoad_dBm, DL
                    if isempty(Cell(C(1)).PTx_dBm)
                        tmp = arrayfun(@(x) x.EIRP_ovAllAnt_10MHz_100pcLoad_dBm(fn),...
                            Cell(C));
                        EIRP_ovAllAnt_10MHz_100pcLoad_dBm = tmp * S.';
                        if size(EIRP_ovAllAnt_10MHz_100pcLoad_dBm,1) < ...
                                size(EIRP_ovAllAnt_10MHz_100pcLoad_dBm,2)
                            EIRP_ovAllAnt_10MHz_100pcLoad_dBm = ...
                                EIRP_ovAllAnt_10MHz_100pcLoad_dBm.';
                        end
                    else
                        tmp = arrayfun(@(x) x.PTx_dBm(fn),Cell(C));
                        PTx_dBm = tmp * S.';
                        if size(PTx_dBm,1) < size(PTx_dBm,2)
                            PTx_dBm = PTx_dBm.';
                        end
                    end
                    
        %             figure
        %             hist(PRx_across_system_BW_DL)
        %             grid on
        %             xlabel('PRx_across_system_BW_DL  (dBm)')
        %             ylabel('number of pixels with the value of the x-axis')

                    % Find AeCouMS.
                    % The number of antennas does not vary with the
                    % mobility class, so it does not matter so much which
                    % mobility class is used.
                    IND2 = IND2c & strcmp(Sim.demTranslati(:,3),pwrAmpClasMS) & ...
                        strcmp(Sim.demTranslati(:,4),situMS);
                    AeCouMS = unique(nonzeros(aeCou(:,IND2)),'stable');
%                     assert(length(AeCouMS)==1,'Amend this!')
%                     assert(AeCouMS~=0,'Amend this!')
                    
                    % For each BS pwrAmpClas situ AeCou and FBMC, find
                    % best server and redundancy level of all pixels
                    % with satis<1
                    RxPowerRB_DL = cell(length(bund1IcCelli_C),1);
                    Celli2 = cell(length(bund1IcCelli_C),1);
                    Celli3 = cell(length(bund1IcCelli_C),1);
                    Celli4 = cell(length(bund1IcCelli_C),1);
                    studyAind3 = cell(length(bund1IcCelli_C),1);
                    sigmaLe2 = cell(length(bund1IcCelli_C),1);
                    distm2 = cell(length(bund1IcCelli_C),1);
                    capa_ = cell(length(bund1IcCelli_C),1);
                    L_ = cell(length(bund1IcCelli_C),1);
                    maxSuppAppliDatRat = cell(length(bund1IcCelli_C),1);
                    maxSuppVel = cell(length(bund1IcCelli_C),1);
                    bund1ind = cell(length(bund1IcCelli_C),1);
                    for pn = 1 : length(bund1IcCelli_C)
                        % Find the index of propagation enties that
                        % correspond to this BS bundle.
                        IND = bund1IcCelli == bund1IcCelli_C(pn);

                        % If any propagation entries refer to this BS
                        % bundle, then proceed. Otherwise, assess next BS
                        % bundle.
                        if any(IND)
                            % The BS power amplifier class, situation, and
                            % FBMC property for this BS bundle
                            pwrAmpClasBS = bund1.C(bund1.ia(bund1IcCelli_C(pn))).pwrAmpClas;
                            situBS = bund1.C(bund1.ia(bund1IcCelli_C(pn))).situ;
                            AeCouBS = bund1.C(bund1.ia(bund1IcCelli_C(pn))).AeCouBS;
                            sectoCou = bund1.C(bund1.ia(bund1IcCelli_C(pn))).sectoCou;
%                             FBMC = bund1.C(bund1.ia(bund1IcCelli_C(pn))).FBMC;

                            % Find maxSuppAppliDatRat and maxSuppVel
                            tmp = arrayfun(@(x) x.maxSuppAppliDatRat(2,...
                                sum(Sim.yea.thisYea >= x.maxSuppAppliDatRat(1,:))),...
                                Cell(C));
                            maxSuppAppliDatRat{pn} = repmat(tmp*S(IND,:).',...
                                1,sectoCou).';
                            tmp = [Cell(C).maxSuppVel];
                            maxSuppVel{pn} = repmat(tmp*S(IND,:).',1,sectoCou).';

                            % Calculate an appropriate CFI according to the BW
                            if length(Cell) > 1
                                capa = arrayfun(@(x) x.band.capa(fn),Cell.');
                            else
                                capa = Cell.band.capa(fn);
                            end
                            capa(capa==8.79) = 10;
                            capa(capa==18.467) = 20;
%                             capa(capa>20) = 20;
                            L = zeros(size(capa));
                            if any(capa<10)
                                % CFI = 3 for 1.4, 3 and 5 MHz system bandwidths
                                L(capa<10) = 3;
                            end
                            if any(capa>=10)
                                if all(Sim.dem.trafVol.(fam).CEtput.DL(:,CEtputI(trafClI))<150.8.*2./3 | ...
                                        isnan(Sim.dem.trafVol.(fam).CEtput.DL(:,CEtputI(trafClI)))) && ...
                                        all(Sim.dem.trafVol.(fam).CEtput.UL(:,CEtputI(trafClI))<51.0.*2./3 | ...
                                        isnan(Sim.dem.trafVol.(fam).CEtput.UL(:,CEtputI(trafClI))))
                                    % CFI = 2 for 10, 15 and 20 MHz system bandwidth
                                    L(capa>=10) = 2;
                                else
                                    % Use CFI = 1 for Cat 4 or higher max throughput case
                                    beep
                                    disp('Code this case!')
                                    keyboard
                        %                         User equipment category	Maximum L1 datarate
                        %                         downlink	Maximum
                        %                         number of DL MIMO
                        %                         layers	Maximum L1 datarate
                        %                         uplink	3GPP release
                        %                         Category 4	150.8 Mbit/s	2	51.0 Mbit/s	Release 8
                                    L(capa>=10) = 1;
                                end
                            end
                            capa_{pn} = repmat(capa(Celli(IND)),sectoCou,1);
                            L_{pn} = repmat(L(Celli(IND)),sectoCou,1);
                            
                            % Expand studyAind, sigmaLe, sectoCou by
                            % repetition for the sectors of each site
                            studyAind3{pn} = repmat(studyAind(IND),sectoCou,1);
                            sigmaLe2{pn} = repmat(sigmaLe(IND),sectoCou,1);
                            distm2{pn} = repmat(distm(IND),sectoCou,1);
                            
                            % Calculate the DL received signal strength,
                            % for all assessed paths, RxPowerRB_DL,
                            % dBm/180kHz
                            if isempty(Cell(C(1)).PTx_dBm)
                                tmp = repmat(EIRP_ovAllAnt_10MHz_100pcLoad_dBm(IND) - ...
                                    (Sim.linBud.(pwrAmpClasBS).(situBS).GA.(fs1) + ...
                                    Sim.linBud.(pwrAmpClasBS).(situBS).GB.(trafCl) + ...
                                    Sim.linBud.(pwrAmpClasBS).(situBS).GC) - ...
                                    10.*log10(AeCouBS),sectoCou,1);
                                RxPowerRB_DL{pn} = tmp + ...
                                    10.*log10(capa_{pn}./10) - CL{pn} - ...
                                    10.*log10(capa_{pn}.*5);
                            else
                                tmp = repmat(PTx_dBm(IND) - ...
                                    10.*log10(AeCouBS),sectoCou,1);
                                RxPowerRB_DL{pn} = tmp - CL{pn} - ...
                                    10.*log10(capa_{pn}.*5);
                            end

                            % Calculate indices.
                            % Celli2 is a unique sector ID. It is
                            % constructed by appending to Celli as many
                            % times as sectoCou indicates, and for each
                            % addition the ID are increased by length(Cell)
                            % to be distribuishable from the previous ID.
                            % For example
                            % if Celli(IND)=[1;1;2;3], and sectoCou=2, then
                            % Celli2=[1;1;2;3; 4;4;5;6]. The first 4
                            % entries as the same as Celli, and the rest
                            % are Celli+length(Cell).
                            % Celli3 is an index to Cell of the parent that
                            % calls this function, as opposed to the Cell
                            % argument of this function.
                            % Celli4 is the sector ID within the site. It
                            % is constructed from the unique sector ID,
                            % Celli2, however it is repeatitive. For
                            % example Celli2=[1;1;2;3; 4;4;5;6] corresponds
                            % to Celli4=[1;1;1;1; 2;2;2;2].
                            % Reminder that Celli is an index to Cell input
                            % of this function, and typically this function
                            % is called with a subset of a bigger array of
                            % Cell objects.
                            % The indices are complicated because some
                            % operations are done per site whereas other
                            % per sector, and the function is called with
                            % a subset of Cell. 
                            Celli2{pn} = reshape(bsxfun(@plus,Celli(IND),...
                                (0:sectoCou-1).*length(Cell)),...
                                sectoCou*nnz(IND),1);
                            Celli3{pn} = repmat(isCell2(Celli(IND)).',...
                                sectoCou,1);
                            Celli4{pn} = ceil(Celli2{pn}./length(Cell));
                            bund1ind{pn} = bund1IcCelli_C(pn) .* ones(sectoCou*nnz(IND),1);
                            
%                             figure
%                             hold on
%                             axis equal
%                             set(gca,'ydir','reverse')
%                             colorbar
%                             %
%                             row = [Cell(Celli3{pn}(studyAind3{pn}==122197 & ...
%                                 Celli4{pn}==3)).I];
%                             col = [Cell(Celli3{pn}(studyAind3{pn}==122197 & ...
%                                 Celli4{pn}==3)).J];
% %                             [row,col] = ind2sub(studyA.R.RasterSize,...
% %                                 studyAind(Celli==4));
%                             scatter(col,row,6*100,CL{pn}(studyAind3{pn} == ...
%                                 122197 & Celli4{pn}==3),'s','filled')
% %                             scatter(col,row,6,CL{pn}(Celli3{pn}==4+length(Cell)),'s','filled')
% %                             scatter(col,row,6,CL{pn}(Celli3{pn}==4+2.*length(Cell)),'s','filled')
%                             %
%                             plot([Cell.J],[Cell.I],'ko')
%                             plot([Cell.J],[Cell.I],'k.')
%                             %
%                             [row,col] = ind2sub(studyA.R.RasterSize,...
%                                 122197);
%                             plot(col,row,'kx')
%                             %
% %                             plot(Cell(4).J,Cell(4).I,'ro',...
% %                                 'markerfacecolor','r')
% %                             plot(Cell(4).J,Cell(4).I,'r.')
%                             %
% %                             colormap('hot')
%                             %
%                             set(gca,'xlim',[327.9365  365.6869],...
%                                 'ylim',[28.1850   57.9590],...
%                                 'clim',[100 135])
%                             %
%                             title('Coupling loss  (dB)')
                            
%                             figure
%                             hold on
%                             axis equal
%                             set(gca,'ydir','reverse')
%                             colorbar
%                             %
%                             [row,col] = ind2sub(studyA.R.RasterSize,...
%                                 studyAind3{pn}(Celli2{pn}==3));
%                             scatter(col,row,6*10,CL{pn}(Celli2{pn}==3),'s','filled')
% %                             scatter(col,row,6*10,CL{pn}(Celli2{pn}==3+length(Cell)),'s','filled')
% %                             scatter(col,row,6*10,CL{pn}(Celli2{pn}==3+2*length(Cell)),'s','filled')
%                             %
%                             plot([Cell.J],[Cell.I],'ko')
%                             plot([Cell.J],[Cell.I],'k.')
%                             plot(Cell(54).J,Cell(54).I,'kx')
%                             %
%                             [row,col] = ind2sub(studyA.R.RasterSize,...
%                                 122197);
%                             plot(col,row,'kx')
%                             %
% %                             plot(Cell(4).J,Cell(4).I,'ro',...
% %                                 'markerfacecolor','r')
% %                             plot(Cell(4).J,Cell(4).I,'r.')
%                             %
% %                             colormap('hot')
%                             %
% %                             set(gca,'xlim',[327.9365  365.6869],...
% %                                 'ylim',[28.1850   57.9590],...
% %                                 'clim',[100 135])
%                             set(gca,'xlim',[231.8025  326.7585],...
%                                 'ylim',[97.9787  172.8714],...
%                                 'clim',[100 135])
%                             %
%                             title('Coupling loss  (dB)')

%                             CL{pn}(studyAind3{pn}==122197 & Celli4{pn}==1 & ...
%                                 Celli2{pn}==3)
                        end
                    end
                    RxPowerRB_DL = cell2mat(RxPowerRB_DL);
%                     Celli2 = cell2mat(Celli2);
                    Celli3 = cell2mat(Celli3);
                    Celli4 = cell2mat(Celli4);
                    studyAind3 = cell2mat(studyAind3);
                    sigmaLe2 = cell2mat(sigmaLe2);
                    distm2 = cell2mat(distm2);
                    capa = cell2mat(capa_);
                    L = cell2mat(L_);
                    maxSuppAppliDatRat = cell2mat(maxSuppAppliDatRat);
                    maxSuppVel = cell2mat(maxSuppVel);
                    bund1ind = cell2mat(bund1ind);
                    clear pn IND pwrAmpClasBS situBS AeCouBS sectoCou FBMC tmp capa_ L_

                    % Select the propagation entries that have the
                    % greatest RxPowerRB_DL. Many cells
                    % can be selected if the service allows for
                    % multiple servers, i.e. ServCouMax>1.
                    I = accumarray(studyAind3,(1:length(studyAind3)).',...
                        [prod(studyA.R.RasterSize) 1],@(x) keep_max(x,...
                        RxPowerRB_DL));
                    studyAc = unique(studyAind3,'stable');
%                     assert(all(find(~cellfun(@isempty,I))==sort(studyAc)))
                    if Sim.servDem.isInterfeExpl.cov
                        % Explicit calculation of interference
                        ii = cellfun(@(x) [Celli3(x) Celli4(x) ...
                            RxPowerRB_DL(x) sigmaLe2(x)],I(studyAc),...
                            'UniformOutput',false);
                    else
                        % Implicit calculation of interference
                        ii = cellfun(@(x) [Celli3(x(1)) Celli4(x(1)) ...
                            RxPowerRB_DL(x(1)) sigmaLe2(x(1))],I(studyAc),...
                            'UniformOutput',false);
                    end
                    if ServCouMax == 1
                        toKeep = cellfun(@(x) x(1),I(studyAc));
                    else
                        toKeep = cell2mat(cellfun(@(x) ...
                            x(1:ServCouMax).',I(studyAc),...
                            'UniformOutput',false));
                    end

                    % Keep only entries up to the number of servers that
                    % the service supports. For example, let a {pixel,
                    % family of services, traffic class, MS power amplifier
                    % class, MS situation} has propagation entries to two
                    % BS, and let ServCouMax=1, then the second entry is
                    % not needed. Its power contribution to interference
                    % has been saved in ii, in the paragraph above. It is
                    % reminded that the order is in reducing RSRP.
                    capa = capa(toKeep);
                    distm2 = distm2(toKeep);
                    L = L(toKeep);
                    maxSuppAppliDatRat = maxSuppAppliDatRat(toKeep);
                    maxSuppVel = maxSuppVel(toKeep);
%                     RxPowerRB_DL = RxPowerRB_DL(toKeep);
%                     sigmaLe2 = sigmaLe2(toKeep);
                    studyAind3 = studyAind3(toKeep(:,1));
                    bund1ind = bund1ind(toKeep);
%                     Celli2 = Celli2(toKeep);
%                     Celli3 = Celli3(toKeep);
%                     Celli4 = Celli4(toKeep);
                    if ~all(size(capa)==size(toKeep))
                        capa = capa.';
                        distm2 = distm2.';
                        L = L.';
                        maxSuppAppliDatRat = maxSuppAppliDatRat.';
                        maxSuppVel = maxSuppVel.';
                        studyAind3 = studyAind3.';
                        bund1ind = bund1ind.';
                    end

                    % Calculate power controled output power as defined
                    % in 36.942, UL
%                             assert(all(INDw1(:,2)==0),'Code this!')
%                     fun = @(x) pctile(x,0.95,'nearest');
%                     CLxile = accumarray(Celli2,CL{pn}(toKeep),[],fun);
%                     Pt = Sim.linBud.(pwrAmpClasMS).(...
%                         situMS).PTx_antConn_perAnt_100pcLoad_dBm.(fs1) .* ...
%                         min(1,CL{pn}(toKeep)./CLxile(Celli2));
%                     clear CLxile fun

                    % Calculate received signal strength across system
                    % BW, for all assessed paths, UL
%                     PRx_across_system_BW_UL = Pt - CL{pn}(toKeep);

%                     figure
%                     hist(PRx_across_system_BW_UL)
%                     grid on
%                     xlabel('PRx_across_system_BW_UL  (dBm)')
%                     ylabel('number of pixels with the value of the x-axis')

                    % Check:
                    % a) if the propagation entry corresponds to
                    %    a service/UE bundle (budle 2) that be
                    %    served in this frequency band, and
                    % b) if the reported application data rate and
                    %    mobile velocity are within the cell's
                    %    capabilities.
                    % For propagation entries that pass these
                    % checks, visib is appended with entries.
                    tmp = c_SINR(bund2.(fam),trafClI,fam,famI,Sim,fn,L,...
                        pwrAmpClasMS,AeCouMS,SINR2tput,...
                        maxSuppAppliDatRat,maxSuppVel,studyAind3,...
                        distm2,ii,situMS,fs1,tn,MS(famI),thisDem,...
                        capa,ServCouMax,bund1ind,bund1,bund1IcCelli_C,...
                        CEtputI);
                    if ~isempty(tmp)
                        tmp.pwrAmpClasMS = repmat({pwrAmpClasMS},...
                            length(tmp.ii),1);
                        tmp.situMS = repmat({situMS},...
                            length(tmp.ii),1);
                        tmp.famI = repmat(famI,...
                            length(tmp.ii),1);
                        if isempty(visib)
                            visib = tmp;
                        else
                            visib = appen(visib,tmp);
                        end
                    end
                    clear tmp
%                     clear pn pwrAmpClasBS situBS SINRthres_UL SINRthres_DL IND
                end
%                 clear IND row col Lbc sigmaLe2 Celli studyAind3
            end
        end
    end
% clear tn pwrAmpClasMS
end
end

%% c_SINR used to calculate the SINR (hence the name) but now is limited to populate visib with the quantities needed for SINR calculation
function out = c_SINR( bund2, trafClI, fam, famI, Sim, fn, L, pwrAmpClasMS, AeCouMS,...
                       SINR2tput, maxSuppAppliDatRat, maxSuppVel, studyAind, distm, ii,...
                       situMS, fs1, tn, MS, thisDem, capa, ServCouMax, bund1ind, bund1,...
                       bund1IcCelli_C, CEtputI )
% c_SINR returns out, which is essentially visib of the parent function
%
% * v0.1 Kostas Konstantinou Oct 2016

% Associate indices between studyA and thisDem
[~,thisDem_IND] = ismember(studyAind,[thisDem.IND]);

% Simplifications
AeCouBS_ = Sim.linBud.AeCouBS_;
AeCouMS_ = Sim.linBud.AeCouMS_;
FBMC_ = Sim.linBud.FBMC_;
BW_ = Sim.linBud.BW_;

% Define duplex mode in string format
% switch Sim.linBud.dupMode(fn)
%     case 1
%         XDD = 'TDD';
%     case 2
%         XDD = 'FDD';
% end

N = length(thisDem_IND);
utilisN = Sim.utilisN;
interfSuppr = Sim.linBud.interfSuppr(Sim.yea.thisYea == ...
    Sim.linBud.interfSupprYea);

% Calculate the noise floor (thermal+figure) of each path in
% downlink, expressed in dBm/180kHz.
noisFl = Sim.linBud.(pwrAmpClasMS).(situMS).NoiFloo + ...
    Sim.linBud.(pwrAmpClasMS).(situMS).NoiFigu.(fs1) + ...
    10.*log10(0.18.*1000000);

% For each bundle associated with service requirement, and UE power
% amplifier classs and situation (bundle 2), then populate visib output.
out.studyAind = [];
out.thisDem_IND = [];
out.SE_DL = [];
out.noisFl_DL = [];
out.ii = {};
% out.PRx_across_system_BW_UL = [];
% out.Pt = [];
out.SINRthres_DL = [];
out.SINRthresPer_DL = [];
% out.SINRthres_UL = [];
% out.sigmaLe = [];
out.distm = [];
out.bund1 = [];
out.bund2 = [];
out.faFadMargi = [];
out.NLayers = [];
assert(~Sim.servDem.isInterfeExpl.cov,...
    'The code for variable load and multiple throughput reporting has not been developed')
for mn = 1 : length(bund2.ia)
    thisBund2 = bund2.C(bund2.ia(mn));

    SE_DL_ = zeros(N,ServCouMax);
    IND = false(N,ServCouMax);
    SINRthres_DL = zeros(N,utilisN,ServCouMax);
    SINRthresPer_DL = zeros(N,length(Sim.optimis.repoTput),ServCouMax);
    NLayers = zeros(N,utilisN,ServCouMax);
    for pn = 1 : length(bund1IcCelli_C)
        AeCouBS = bund1.C(bund1.ia(bund1IcCelli_C(pn))).AeCouBS;
        pwrAmpClasBS = bund1.C(bund1.ia(bund1IcCelli_C(pn))).pwrAmpClas;
        FBMC = bund1.C(bund1.ia(bund1IcCelli_C(pn))).FBMC;

        % If this service/UE bundle (budle 2) can be served in this frequency
        % band, then populate visib output.
        if Sim.linBud.dop(bund2.ia(mn),fn)<999 && ~isnan(thisBund2.DL) && ...
                thisBund2.pwrAmpClasMSsituMS(MS.IND(tn)) && AeCouBS>0
            % Get the service cell-edge throughput in Mbit/s
            CEtput_DL = Sim.dem.trafVol.(fam).CEtput.DL(bund2.ia(mn),...
                CEtputI(trafClI));
            
            % []
            IND2 = bund1ind == bund1IcCelli_C(pn);
            
            % For admission control, find propagation entries for which the
            % reported application data rate and mobile velocity are within the
            % cell's capabilities.
            IND(IND2) = maxSuppAppliDatRat(IND2) >= CEtput_DL & ...
                maxSuppVel(IND2) >= Sim.dem.mobiClaVel(bund2.ia(mn));
            
            % []
            if any(any(IND2,1),2)
                [SE_DL_(IND2),SINRthres_DL0,NLayers0,...
                    SINRthresPer_DL0] = c_SINRthres_DL(CEtput_DL,Sim,...
                    capa(IND2),L(IND2),fn,SINR2tput,pwrAmpClasBS,...
                    AeCouBS,AeCouBS_,AeCouMS,AeCouMS_,FBMC_,FBMC,famI,...
                    trafClI,BW_,fam,interfSuppr);
                [row,col] = find(IND2);
                if ServCouMax>1 && N==1
                    row = row.';
                    col = col.';
                end
                tmp = repmat(1:utilisN,length(row),1);
                linearInd = sub2ind(size(SINRthres_DL),...
                    repmat(row,utilisN,1),tmp(:),repmat(col,utilisN,1));
                SINRthres_DL(linearInd) = SINRthres_DL0(:);
                NLayers(linearInd) = NLayers0(:);
                SINRthresPer_DL(row,:) = SINRthresPer_DL0;
            end
        end
    end
    if ServCouMax>1 && any(xor(IND(:,1),IND(:,2)))
        beep
        disp('One of the sites cannot serve the pixel!')
        keyboard
    end
            
    if any(any(IND,1),2)
        % Calculate fast fading margin in dB, faFadMargi.
        % According to feedback from Gerd (DT) fade margin is not
        % needed in 5G.
        faFadMargi = zeros(1,ServCouMax);
%                 switch Sim.dem.trafVol.(fam).CEtput.laten(trafClI)
%                     case {100,200}
%                         faFadMargi = zeros(1,ServCouMax);
%                     case 2
%                         PER = Sim.dem.trafVol.(fam).CEtput.PER(trafClI);
%                         faFadMargi = max(-10*log10(gammaincinv(PER,...
%                             AeCouBS.*AeCouMS.*(1:ServCouMax)).*2.*0.5.^2),0);
%                         if any(faFadMargi>0) && any(NLayers(:)>1)
%                             beep
%                             disp('Code this; faFadMargi is impacted by the diversity provided by NLayers!')
%                             keyboard
%                         end
%                     otherwise
%                         error('Insert this!')
%                 end

        %% out
        % Append to visib structure entries from this BS bundle
        % (pwrAmpClas, situ, AeCou, and FBMC; bundle 1), InP,
        % service/UE bundle (bundle 2)
        if size(IND,2)>1 && ~all(IND(:,1))
            beep
            disp('Check this because only one site can serve the pixel!')
            keyboard
        end
        out.studyAind = [out.studyAind; studyAind(IND(:,1))];
        out.thisDem_IND = [out.thisDem_IND; thisDem_IND(IND(:,1))];
        out.SE_DL = [out.SE_DL; SE_DL_(IND(:,1),:)];
        out.noisFl_DL = [out.noisFl_DL; noisFl.*ones(nnz(IND(:,1)),1)];
        out.ii = [out.ii; ii(IND(:,1))];
    %             out.PRx_across_system_BW_UL = [out.PRx_across_system_BW_UL
    %                 PRx_across_system_BW_UL];
    %             out.Pt = [out.Pt; Pt];
        out.SINRthres_DL = [out.SINRthres_DL
            SINRthres_DL(IND(:,1),:)];
    %             out.SINRthres_UL = [out.SINRthres_UL
    %                 full(sparse((1:INDn).',ic2,1))*SINRthres_UL];
    %                 out.sigmaLe = [out.sigmaLe
    %                     reshape(sigmaLe(IND),size(IND))];
        out.distm = [out.distm; distm(IND(:,1),:)];
        out.bund1 = [out.bund1; bund1ind(IND(:,1),:)];
        out.bund2 = [out.bund2
            bund2.ia(mn) .* ones(length(studyAind(IND(:,1))),1)];
        out.NLayers = [out.NLayers; NLayers(IND(:,1),:)];
        if ServCouMax == 1
            out.faFadMargi = [out.faFadMargi
                faFadMargi.*ones(nnz(IND),1)];
        else
            out.faFadMargi = [out.faFadMargi
                bsxfun(@times,faFadMargi,ones(size(IND)))];
        end
        out.SINRthresPer_DL = [out.SINRthresPer_DL
            SINRthresPer_DL(IND(:,1),:)];
    end
end
end

%% Append in structure of arrays
function x = appen(x,y)
% appen returns x that is a structure of arrays that contain the entries of
% input x, apended with the entries of structure of arrays y.
% 
% * v0.1 Kostas Konstantinou Oct 2016

names = fieldnames(x);
for n = 1 : length(names)
    szdimX = size(x.(names{n}),2);
    szdimY = size(y.(names{n}),2);
    
    if szdimX == szdimY
        x.(names{n}) = [x.(names{n})
            y.(names{n})];
    elseif szdimX < szdimY
        x.(names{n}) = [[x.(names{n}) nan(size(x.(names{n}),1),...
            szdimY-szdimX)]
            y.(names{n})];
    else
        x.(names{n}) = [x.(names{n})
            [y.(names{n}) nan(size(y.(names{n}),1),szdimX-szdimY)]];
    end
end
end

%% []
function [SE_DL_,SINRthres_DL,NLayers,SINRthresPer_DL] = c_SINRthres_DL(...
    CEtput_DL,Sim,capa,L,fn,SINR2tput,pwrAmpClasBS,AeCouBS,AeCouBS_,...
    AeCouMS,AeCouMS_,FBMC_,FBMC,famI,trafClI,BW_,fam,interfSuppr)
% If there are propagation entries within the cell's capabilities,
% then populate visib output.

% []
[~,ia2,ic2] = unique(capa.*10+L,'stable');

%% DL
% Calculate the downlink SINR threshold to deliver the
% service for each path, expressed in dB, SINRthres_DL.
% The dimensions are [propagation entry, PRB utilisation
% see Sim.utilisN, path in multi-connectivity]. If 
% single-connectivity then the third dimension is 1.
% Calculate the number of tranmission layers for which the
% SINRthres_DL value corresponds to.
SINRthres_DL = inf(length(L),Sim.utilisN);
SINRthresPer_DL = inf(length(L),length(Sim.optimis.repoTput));
NLayers = zeros(length(L),Sim.utilisN);
xIND = min(round((Sim.linBud.PRButilisCovera(fn) + 1./Sim.utilisN./2) ./ ...
    (1./Sim.utilisN)),Sim.utilisN) + 1;
for n = 1 : max(ic2)
    x = SINR2tput{fn,L(ia2(n)),...
        strcmp(pwrAmpClasBS,Sim.linBud.BS.pwrAmpClas_),...
        AeCouBS==AeCouBS_,AeCouMS==AeCouMS_,...
        FBMC_==FBMC,famI,trafClI,BW_==capa(ia2(n))};
    k = sum(CEtput_DL>=x(2:end-2,:),2) + 1;
    
    IND2 = k <= size(x,2);
%         [dim1Sub,dim3Sub] = find(ic3==n);
%         if size(ic3,1)==1 && size(ic3,2)>1
%             dim1Sub = dim1Sub.';
%             dim3Sub = dim3Sub.';
%         end
%         linearInd = sub2ind(size(SINRthres_DL),...
%             repmat(dim1Sub,nnz(IND2),1),...
%             reshape(repmat(find(IND2),1,length(dim1Sub)).',[],1),...
%             repmat(dim3Sub,nnz(IND2),1));
    SINRthres_DL(ic2==n,IND2) = repmat(x(1,k(IND2)),nnz(ic2==n),1);
    NLayers(ic2==n,IND2) = repmat(x(end,k(IND2)),nnz(ic2==n),1);
%         SINRthres_DL(linearInd) = reshape(repmat(...
%             x(1,k(IND2)).',1,length(dim1Sub)).',[],1);
%         NLayers(linearInd) = reshape(repmat(...
%             x(end,k(IND2)).',1,length(dim1Sub)).',[],1);

    if ~isempty(Sim.optimis.repoTput)
        k2 = sum(bsxfun(@ge,Sim.optimis.repoTput.',x(xIND,:)),2) + 1;
        IND3 = k2 <= size(x,2);
        SINRthresPer_DL(ic2==n,IND3) = repmat(x(1,k2(IND3)),nnz(ic2==n),1);
    end
end

% Calculate median SE, DL.
% Note that the QoS factor, servFacto, affects the spectral
% efficiency. Ideally, the QoS factor affects the
% calculation of required cell capacity to serve the
% offered demand. However, until this feature is
% implemented, the QoS factor can be applied here.
SE_DL_ = zeros(length(L),1);
for n = 1 : max(ic2)
    x = SINR2tput{fn,L(ia2(n)),...
        strcmp(pwrAmpClasBS,Sim.linBud.BS.pwrAmpClas_),...
        AeCouBS==AeCouBS_,AeCouMS==AeCouMS_,...
        FBMC_==FBMC,famI,trafClI,BW_==capa(ia2(n))};
%         [dim1Sub,dim2Sub] = find(ic3==n);
%         linearInd = sub2ind(size(SE_DL_),dim1Sub,dim2Sub);
    if Sim.servDem.isInterfeExpl.capa
        beep
        disp('For capacity let''s use the average cell SE as provided by our partners')
        keyboard
        SE_DL_ = interp1(x(1,:),x(3,:),SINR_DL_,'nearest','extrap');
    else
        if interfSuppr == 0
            SE_DL_(ic2==n) = interp1(x(1,:),x(end-1,:),...
                Sim.linBud.SINRmean(1)) .* ...
                Sim.dem.trafVol.(fam).CEtput.servFacto(trafClI);
        else
            SE_DL_(ic2==n) = interp1(x(1,:),x(end-1,:),...
                Sim.linBud.SINRmean(1)+interfSuppr) .* ...
                Sim.dem.trafVol.(fam).CEtput.servFacto(trafClI);
        end
    end
end

%% UL
% Calculate the SINR if transmission BW is 1, 2, ..., all PRB
% x rows are:
%   1. SINR, in dB
%   2. goodput if all PRB are utilised, in Mbit/s
%   3. SE, is bit/s/Hz
%             CEtput_UL = Sim.dem.trafVol.(fam).CEtput.UL(bund2.ia(mn),trafClI);
%             SINRthres_UL = cell(max(ic2),1);
%             for n = 1 : max(ic2)
%                 N = floor(capa(INDf(ia2(n))).*Sim.linBud.PRButilisCovera(fn).*5);
%                 x = SINR2tput{fn,L(INDf(ia2(n))),strcmp(pwrAmpClasBS,Sim.linBud.BS.pwrAmpClas_),...
%                     AeCouBS==AeCouBS_,AeCouMS==AeCouMS_,FBMC_==FBMC,1,...
%                     famI,trafClI,BW_==capa(INDf(ia2(n))),2};
% %                 SINRthres_UL{n} = interp1(x(2,:),x(1,:),...
% %                     CEtput_UL.*(N-(1:N)+1)) - 10.*log10(1:N);
%                 SINRthres_UL{n} = interp1(x(2,:)./N,x(1,:),...
%                     CEtput_UL./(1:N)) + 10.*log10(1:N);
% %                 SINRthres_UL{n} = zeros(1,N);
% %                 for n2 = 1 : N
% %                     SINRthres_UL{n}(n2) = interp1(x(2,:)./N.*n2,x(1,:),...
% %                         CEtput_UL) + 10.*log10(n2);
% %                 end
%             end

%             figure
%             plot(x(1,:),x(2,:)./N)
%             xlabel('SINR if BW=1PRB (dB)')
%             ylabel('Goodput if BW=1PRB (Mbit/s)')
%             grid on

%             figure
%             plot(x(1,:),x(2,:))
%             xlabel('SINR if BW=1PRB (dB)')
%             ylabel('Goodput if BW=1PRB (Mbit/s)')
%             grid on

%             figure
%             plot(x(1,:)-10.*log10(N),x(2,:))
%             xlabel('SINR if BW=85PRB (dB)')
%             ylabel('Goodput if BW=85PRB (Mbit/s)')
%             grid on

% Chop SINR values that are lower than the minimum
%             NantRx = AeCouBS;
%             M_ = min(AeCouMS,NantRx) ./ [1 2 4 6 8];
%             M_(M_<1) = [];
%             M_(2:end) = [];
%             [~,divGain] = give_mulFact_divGain(Sim,XDD,M_,NantRx,'SU_MIMO');
%             for n = 1 : length(SINRthres_UL)
%                 SINRthres_UL{n}(SINRthres_UL{n}<Sim.linBud.SNIRmin(2) - ...
%                     divGain) = Sim.linBud.SNIRmin(2) - divGain;
%             end

%         figure
%         plot(SINRthres_UL{1})

% Find the minimum uplink SINR that satisfies the required
% throughput
%             SINRthres_UL = cellfun(@min,SINRthres_UL);
%             if any(isnan(SINRthres_UL)) && strcmp(XDD,'FDD')
%                 beep
%                 disp('Check what should happen next')
%                 keyboard
%             end

% Calculate median SINR, UL
% PRx_across_system_BW_UL - 10.*log10(1:N)
%             if ~Sim.servDem.isInterfeExpl.cov
%                 SINR_UL_ = PRx_across_system_BW_UL(IND&IND2) - ...
%                     (Sim.linBud.(pwrAmpClasBS).(situBS).NoiFloo + ...
%                     Sim.linBud.(pwrAmpClasBS).(situBS).NoiFigu.(fs1) + ...
%                     10.*log10(0.18.*1000000) + ...
%                     Sim.linBud.(pwrAmpClasBS).(situBS).IM + ...
%                     Sim.linBud.dop(bund2.ia(mn),fn));
%             else
%                 SINR_UL_ = PRx_across_system_BW_UL(IND&IND2) - ...
%                     (Sim.linBud.(pwrAmpClasBS).(situBS).NoiFloo + ...
%                     Sim.linBud.(pwrAmpClasBS).(situBS).NoiFigu.(fs1) + ...
%                     10.*log10(0.18.*1000000) + noisRis + ...
%                     Sim.linBud.(pwrAmpClasBS).(situBS).IM - ...
%                     Sim.linBud.(pwrAmpClasMS).(situMS).IM + ...
%                     Sim.linBud.dop(bund2.ia(mn),fn));
%             end

%         figure
%         hist(SINR_UL_)
%         grid on
%         xlabel('SINR_UL_  (dBm)')
%         ylabel('number of pixels with the value of the x-axis')

% Calculate median SE, UL
%         SE_UL_ = interp1(x(1,:),x(3,:),SINR_UL_,'nearest','extrap');
end
