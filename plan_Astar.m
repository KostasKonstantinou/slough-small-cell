function States = plan_Astar(studyA,start,goal)
%% Calculate path with A* planner

% Simplify for increased speed
R = [studyA.R.XWorldLimits(1)
    studyA.R.YWorldLimits(2)
    studyA.R.CellExtentInWorldX
    studyA.R.CellExtentInWorldY
    studyA.R.RasterSize.'];

% Convert start/goal to map coordinates
I = floor((R(2)-start(2))./R(4)) + 1;
J = floor((start(1)-R(1))./R(3)) + 1;
startIJ = [I J];
I = floor((R(2)-goal(2))./R(4)) + 1;
J = floor((goal(1)-R(1))./R(3)) + 1;
goalIJ = [I J];

% The open and closed sets
openset = false(studyA.R.RasterSize);
closedset = false(studyA.R.RasterSize);
G = zeros(studyA.R.RasterSize);
H = zeros(studyA.R.RasterSize);
parentI = zeros(studyA.R.RasterSize);
parentJ = zeros(studyA.R.RasterSize);

% The search tree, Tree, is used by the algorithm. Each Tree row represents
% a tree node. The colulmns are:
% 1. The node's parent row
% 2. I-coordinate
% 3. J-coordinate
% 4. Cost to reach node from root, calculated as travelled distance without
%    smoothing interpolation nor change of direction penalties
% 5. Status code:
%         GoalReached = 1
%         MaxIterationsReached = 2
%         MaxNumTreeNodesReached = 3
%         MotionInCollision = 4
%         InProgress = 5
%         GoalReachedButContinue = 6
%         ExistingState = 7
% 6. G+H
% 7. is in openset
TRee = zeros(14879,6);
TRee(1,:) = [0 startIJ 0 5 sqrt(sum((startIJ-goalIJ).^2,2))];
tn = 1;
TReeM = zeros(studyA.R.RasterSize);
TReeINopenset = true;

isPlot = false;
if isPlot
    figure
    imagesc(studyA.INobst)
    axis equal
    hold on
    plot(startIJ(2),startIJ(1),'ro')
    plot(goalIJ(2),goalIJ(1),'mo')
end

% Create obstacles
H(studyA.INobst) = inf;
closedset(studyA.INobst) = true;

% Current point is the starting point
current = startIJ;

% Add the starting point to the open set
openset(current(1),current(2)) = true;

% Simplification
X0 = ones(3,1) * (-1:1);
Y0 = (-1:1).' * ones(1,3);
Gsave = sqrt(X0.^2+Y0.^2);

% While the open set is not empty
while any(TReeINopenset,1)
    % Find the item in the open set with the lowest G + H score
%         [I,J] = find(openset);
%         [~,minI] = min(G(openset)+H(openset));
    tmp = TRee(TReeINopenset,:);
    [~,minI] = min(tmp(:,6));
    current = tmp(minI,2:3);

    % If it is the item we want, retrace the path and return it
    if all(current==goalIJ,2)
        Path = [];
        tmp = current;
        while parentI(tmp(1),tmp(2)) ~= 0
            Path(end+1,:) = tmp;
            tmp = [parentI(tmp(1),tmp(2)) parentJ(tmp(1),tmp(2))];
        end
        Path(end+1,:) = tmp;
        Path = flipud(Path);

        if isPlot
            plot(Path(:,2),Path(:,1),'color',[0.5 0.5 0.5])
        end

        % Convert to world coordinates
        PathXY = [R(1)+(Path(:,2)-0.5).*R(3) ...
            R(2)-(Path(:,1)-0.5).*R(4)];
        PathXY(1,:) = start;
        PathXY(end,:) = goal;

%             figure
%             axis equal
%             hold on
%             plot(studyA.X,studyA.Y,'k')
%             plot(studyA.obst.X,studyA.obst.Y,'k')
%             plot(PathXY(:,1),PathXY(:,2),'r-','LineWidth',2)
%             plot(start(1),start(2),'ro')
%             plot(goal(1),goal(2),'mo')

        % Simplify path
        isTurn = [false
            abs(diff(atan2d(PathXY(2:end,2)-PathXY(1:end-1,2),...
            PathXY(2:end,1)-PathXY(1:end-1,1)),[],1)) > 1e-3
            false];
        PathXY2 = [PathXY(1,:); PathXY(isTurn,:); PathXY(end,:)];

%             figure
%             axis equal
%             hold on
%             plot(studyA.X,studyA.Y,'k')
%             plot(studyA.obst.X,studyA.obst.Y,'k')
%             plot(PathXY2(:,1),PathXY2(:,2),'r-','LineWidth',2)
%             plot(start(1),start(2),'ro')
%             plot(goal(1),goal(2),'mo')

        States = {PathXY2; NaN};

        break
    %         figure
    %         hold on
    %         plot(path(end:-1:1,1),path(end:-1:1,2),'color','w')
    end

    % Remove the item from the open set
    openset(current(1),current(2)) = false;
    TReeINopensetF = find(TReeINopenset);
    TReeINopenset(TReeINopensetF(minI)) = false;

    % Add it to the closed set
    closedset(current(1),current(2)) = true;

    % Create mesh with 8-connectivity
    X = current(2) + X0;
    Y = current(1) + Y0;

    % Find mesh pixels outside the study area and removed them
    IND = X>0 & Y>0 & X<=R(6) & Y<=R(5);
    n_ = find(IND).';
    
    % If it is already in the closed set, skip it
    n_(closedset(Y(n_)+(X(n_)-1).*size(closedset,1))) = [];
    
    % Loop through the node's children/siblings
    for n = n_
        % Simplification
        Xn = X(n);
        Yn = Y(n);
        
        % Otherwise if it is already in the open set
        if openset(Yn,Xn)
            % Check if we beat the G score 
            new_g = G(current(1),current(2)) + Gsave(n);
            if G(Yn,Xn)-new_g > 1e-10
                % If so, update the node to have a new parent
                G(Yn,Xn) = new_g;
                parentI(Yn,Xn) = current(1);
                parentJ(Yn,Xn) = current(2);

                TRee(TReeM(Yn,Xn),6) = new_g + H(Yn,Xn);
            end
        else
            % If it isn't in the open set, calculate the G and H score for the node
            G(Yn,Xn) = G(current(1),current(2)) + Gsave(n);
            H(Yn,Xn) = sqrt((Xn-goalIJ(2)).^2+(Yn-goalIJ(1)).^2);

            % Set the parent to our current item
            parentI(Yn,Xn) = current(1);
            parentJ(Yn,Xn) = current(2);

            % Add it to the set
            openset(Yn,Xn) = true;

            tn = tn + 1;
            TRee(tn,[2,3,6]) = [Yn Xn G(Yn,Xn)+H(Yn,Xn)];
            TReeM(Yn,Xn) = tn;
            TReeINopenset(end+1,1) = true;
        end
    end
end

% Throw an exception if there is no path
if ~any(TReeINopenset,1)
    beep
    disp('No Path Found')
    keyboard
    
    figure
    imagesc(studyA.INobst)
    axis equal
    hold on
    plot(startIJ(2),startIJ(1),'ro')
    plot(goalIJ(2),goalIJ(1),'mo')
end

%     if isDisp
%         fprintf('DONE\n');
%     end
end
