function neig = c_neig(Cell,isCell0)

% Find the cells that are in the same band bundle
ch = [Cell.ch];

% Index of which of the [28] channels the site(s) support
% []
m_ = find(ch(:,isCell0).');

N = length(Cell);

A = logical(sparse(N,N));

% Calculate triangulation connectivity matrix
for m = m_
    isSupp = ch(m,:);
    if nnz(isSupp) > 1
        dt = delaunayTriangulation([Cell(isSupp).X].',[Cell(isSupp).Y].');
%         N = length(isSupp);
        ConnectivityList = dt.ConnectivityList;
        if ~isempty(ConnectivityList)
            isSupp_S = sparse(1:nnz(isSupp),find(isSupp),true,...
                nnz(isSupp),length(isSupp));
            [ConnectivityList(:),~] = find(isSupp_S(ConnectivityList(:),:).');
            A = A | ...
                sparse(ConnectivityList(:,1),ConnectivityList(:,2),true,N,N) | ...
                sparse(ConnectivityList(:,1),ConnectivityList(:,3),true,N,N) | ...
                sparse(ConnectivityList(:,2),ConnectivityList(:,3),true,N,N);
            A = A.' | A;
        end
    end
end

neig = A(isCell0,:);

% figure
% axis equal
% hold on
% plot([Cell(~isSupp).X],[Cell(~isSupp).Y],'^','color',[0.5 0.5 0.5],...
%     'disp','Other bands')
% plot([Cell(isSupp).X],[Cell(isSupp).Y],'^','color',[1 0 0],...
%     'disp','Same band')
% plot(Cell(isCell0).X,Cell(isCell0).Y,'k^','disp','Reference site')
% plot([Cell(neig).X],[Cell(neig).Y],'b^','disp','Neighbours')
% text(Cell(isCell0).X,Cell(isCell0).Y,[' ' num2str(find(isCell0))])
% text([Cell(neig).X],[Cell(neig).Y],cellfun(@(x) [' ' num2str(x)],...
%     num2cell(find(neig)),'UniformOutput',false))
% legend('location','best')
