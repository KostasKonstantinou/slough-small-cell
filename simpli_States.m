function out = simpli_States(States,R,start,goal,map,kStar)

% figure
% plot(States{1}(:,1),States{1}(:,2))
% axis equal

% orienta = [NaN; atan2d(States{1}(2:end,2)-States{1}(1:end-1,2),...
%     States{1}(2:end,1)-States{1}(1:end-1,1))];
% k_ = sign(wrapTo180(orienta(2:end)-orienta(1:end-1)));
% k_ = find(k_~=k_(2));

k_ = kStar : 2 : size(States{1},1);

ValidationDistance = 0.1 ./ 3 .* sqrt(sum((start-goal).^2,2)) .* 0.1401;
poi4del = [];
for k = 2 : length(k_)
    interpValid = isMotionValid(States{1}(k_(k),:),States{1}(k_(k-1),:),...
        ValidationDistance,map,R);
    
    if interpValid
        poi4del = [poi4del, k_(k-1)+1:k_(k)-1];
    end
end

if isempty(poi4del)
    out = States;
else
    States{1}(poi4del,:) = [];

    out = simpli_States(States,R,start,goal,map,kStar);
end
