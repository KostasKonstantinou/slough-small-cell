function plot_map_unserv(Sim,Cell,studyA,thisDem,unserv,focBuf,titl,...
    filepart2,Xlim,Ylim)
%% Plot map of unserved demand
% plot_map_unserv plots a map of unserved demand.
% 
% Sim is a SIM object and must contain the following fields,
% see class for definition:
% yea.thisYea
% dem.operators
% rul
% path.ProgramData
% mstruct
% GmAPIkey
% 
% Cell is a array of CELL objects and must contain the following fields,
% see class for definition:
% INfoc
% pwrAmpClas
% expiYea
% Operator
% EIRP_ovAllAnt_10MHz_100pcLoad_dBm
% X,Y
% RGB
% rul
% 
% studyA is a structure or list of structures with at least the following
% fields:
% [TBC]
% 
% thisDem is an array of demand objects
% 
% unserv is the unserved demand expressed in Mbit/s in MxN, where M =
% length(thisDem) and N = length(Sim.dem.famUseCase_). unserv could be
% derived from thisDem.satis and thisDem.v, but it is faster to work with
% unserv, thus this function updates both thisDem and unserv.
% 
% The input focBuf identifies if the map should include pixels in the
% buffer, must be one of the following:
% ('foc','foc+buf')
% 
% The input titl is the title on the axis. The input filepart2 is part of
% the filename of the saved maps.
%
% * v0.1 Kostas Konstantinou Mar 2017

% Find which sites and demand points are within the polygon we wish the map
% for
switch focBuf
    case 'foc'
        % Plot only within focus
        Cell_INfoc = [Cell.INfoc];
        INfoc = studyA.INfoc(sub2ind(size(studyA.INfoc),...
            [thisDem.I],[thisDem.J])).';
    case 'foc+buf'
        % Plot within focus and buffer
        Cell_INfoc = true(1,length(Cell));
        INfoc = true(length(thisDem),1);
    otherwise
        sdfdfd
end
%     [row,col] = find(construct(:,:,pwrAmpClasI));

% Plot figure at the correct aspect ratio
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 1])

% Plot the study area polygon
plot(studyA.X,studyA.Y,'k','linewidth',1)

% Simplifications
pwrAmpClas = {Cell.pwrAmpClas};
pwrAmpClas_ = unique(pwrAmpClas);
CellIsExpi = Sim.yea.thisYea > [Cell.expiYea];

% Plot the pixels which lack at least one hour of service
Col2 = lines(size(unserv,2));
CellExtentInWorldX = studyA.R.CellExtentInWorldX;
CellExtentInWorldY = studyA.R.CellExtentInWorldY;
for n = 1 : size(unserv,2)
    [X,Y] = studyA.R.intrinsicToWorld([thisDem(unserv(:,n)>0&INfoc).J],...
        [thisDem(unserv(:,n)>0&INfoc).I]);
    if ~isempty(X)
        X = bsxfun(@plus,X,CellExtentInWorldX./2.*[-1,-1,1,1,-1].');
        Y = bsxfun(@plus,Y,CellExtentInWorldY./2.*[-1,1,1,-1,-1].');
        patch(X,Y,ones(size(X)),'facecolor',Col2(n,:),...
            'edgecolor','none','facealpha',0.5,'tag','patch',...
            'disp',strrep(Sim.dem.operators{n},'_',' '))
    end
end
%     [X,Y] = studyA.R.intrinsicToWorld(J(1),I(1));
%     plot(X+cosd(0:360).*Sim.propag.BS{pwrAmpClasI,5}./2,...
%         Y+sind(0:360).*Sim.propag.BS{pwrAmpClasI,5}./2,'k--')
%     text(X+Sim.propag.BS{pwrAmpClasI,5}./2,Y,...
%         [' ' num2str(Sim.propag.BS{pwrAmpClasI,5}./2) 'm'])
%     [X,Y] = studyA.R.intrinsicToWorld(col,row);
%     plot(X,Y,'kx')
%     % [row,col] = find(construct(:,:,2,1));
%     % [X,Y] = studyA.R.intrinsicToWorld(col,row);
%     % plot(X,Y,'ko')

if exist('Xlim','var')
    set(gca,'xlim',Xlim,'ylim',Ylim)
end

% Plot the BS locations
for n = 1 : length(pwrAmpClas_)
    for on = 1 : length(Sim.dem.operators)
        % Find BS from the same InP and power class amplifier
        IND = find(strcmp(pwrAmpClas_{n},pwrAmpClas) & ~CellIsExpi & ...
            strcmp({Cell.Operator},Sim.dem.operators{on}) & Cell_INfoc);

        % Plot BS of this InP and power class amplifier, if any
        if ~isempty(IND)
            % Define the radius of the clover markers that are to be used
            % for BS locations
            if isempty(Cell(IND(1)).EIRP_ovAllAnt_10MHz_100pcLoad_dBm)
                radiu2 = max(Cell(IND(1)).PTx_dBm,[],1);
            else
                radiu2 = max(Cell(IND(1)).EIRP_ovAllAnt_10MHz_100pcLoad_dBm,[],1);
            end
            
            radiu = 0.01629 .* min([diff(xlim),diff(ylim)]) .* ...
                radiu2 ./ 63.5;

            % Plot the clover markers
            switch pwrAmpClas_{n}
                case 'macrocell'
                    X = bsxfun(@plus,[Cell(IND).X],...
                        bsxfun(@times,radiu,cosd([90,90-0+(-69./2:69./2),90,...
                        90-120+(-69./2:69./2),90,...
                        90-240+(-69./2:69./2),90]).'));
                    Y = bsxfun(@plus,[Cell(IND).Y],...
                        bsxfun(@times,radiu,sind([0,90-0+(-69./2:69./2),0,...
                        90-120+(-69./2:69./2),0,...
                        90-240+(-69./2:69./2),0]).'));
                case 'smaCel5W'
                    X = bsxfun(@plus,[Cell(IND).X],...
                        bsxfun(@times,radiu,cosd([90,90-0+(-69./2:69./2),90,...
                        90-180+(-69./2:69./2),90]).'));
                    Y = bsxfun(@plus,[Cell(IND).Y],...
                        bsxfun(@times,radiu,sind([0,90-0+(-69./2:69./2),0,...
                        90-180+(-69./2:69./2),0]).'));
                case 'smaCel0W25'
                    X = bsxfun(@plus,[Cell(IND).X],...
                        bsxfun(@times,radiu,cosd(0:360).'));
                    Y = bsxfun(@plus,[Cell(IND).Y],...
                        bsxfun(@times,radiu,sind(0:360).'));
                otherwise
                    error('Undefined')
            end
            RGB = Cell(IND(1)).RGB;
            patch(X,Y,ones(size(X)),'facecolor',RGB./255,...
                'edgecolor','none','facealpha',0.5,'tag','del')
            plot([Cell(IND).X],[Cell(IND).Y],'.','color',RGB./255,...
                'tag','del')
        end

        % Add letters next to the BS markers:
        % n: new site
        % u: upgraded site
        % r: refreshed site (either simply refreshed or upgraded at the same time)
        for m = IND
            if Cell(m).rul > 0
                if Sim.rul(Cell(m).rul).isUpg && ...
                        Sim.rul(Cell(m).rul).isDecomm
                    % Upgrade expiring site
                    sty = 'u';
                elseif Sim.rul(Cell(m).rul).isUpg && ...
                        ~Sim.rul(Cell(m).rul).isDecomm
                    % Upgrade existing site
                    sty = 'u';
                elseif ~Sim.rul(Cell(m).rul).isUpg && ...
                        Sim.rul(Cell(m).rul).isDecomm
                    % Reinstatement of expiring or expired site
%                     sty = 'r';
                else
                    % Commission new site
                    sty = 'n';
                end
            else
                sty = '';
            end
            text(Cell(m).X,Cell(m).Y,[' ' sty],'fontweight','bold')
        end
    end
end
axis equal
title(titl)
%     title(['Demand not served  (Mbit/s): ' num2str(round(sum(unserv(:))))])
% plot_map(suggest_scale(gca),[],gca);
% clear INfoc pwrAmpClas pwrAmpClas_ n on CellIsExpi Cell_INfoc Marker Col

if exist('Xlim','var')
    set(gca,'xlim',Xlim,'ylim',Ylim)
end

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)

% Map decorations
add_scale('xy',0.60,0.05);

% Add legend
h = findobj(fh1,'tag','patch');
if ~isempty(h)
    legend(h,'location','best')
end

% Save map
% print(fh1,fullfile('OutpM','BSexi'),'-djpeg','-r300')
% savefig(fullfile('OutpM',[filepart2 ' dem']))
warning off all
export_fig(fullfile('OutpM',[filepart2 ' dem.jpg']),'-jpg','-q100','-r300')
warning on all

% Delete map
delete(fh1)
