classdef CELL
    % CELL class for management of cells (sectors)
    % The CELL class is responsible for setting the properties of cells
    % (sectors). A multisector site is abstracted by an omni directional
    % cell in the horizontal plane. If no such abstraction is required,
    % then a cell object corresponds to one sector. More specifically CELL
    % is responsible for delivering the following:
    %
    % *	creates CELL objects
    % * reads existing site databases
    % * plots maps
    % * [TBC]
    
    properties
        Operator;  % operator string
        siteID;  % site ID from operator database
        antID;  % antenna ID from operator database
        sitChaiConfig;  % site chain fonfiguration, must be one of the following: (1,2). Set to 1 for D-RAN (backhaul optical to central cloud), and to 2 for C-RAN (CPRI/ORI-like fronthaul)
        typ;  % string with information on the type of the site chain
        HWresouCateg;  % string with information on the type of the hardware resource category
%         BBU;
%         maxDist_m;  % maximum distance for propagation calculations
        BoundingBox;  % bounding box of where propagation calculations take place
        band;  % structure of band support (supp), capacity (capa), utilisation (utilis)
        FBMC;  % logical variable that is true if the site supports FBMC
        laten;  % minimum latency
        connTowaCloTyp;  % connection towards cloud - type
        connTowaCloID;  % connection towards cloud - ID of edge cloud or 0 for connection to central
        connTowaCloBW;  % connection towards cloud - bandwidth
        X;  % easting in metres
        Y;  % northing in metres
        lat;  % latitude
        lon;  % longitude
        heiAgl;  % height above ground level
        NGR;  % National Grid Reference (only for the UK)
        tilt;  % Downtilt, deg
        HPBWhor;  % Half power horizontal beamwidth, deg
        HPBWver;  % Half power horizontal beamwidth, deg
        MCL;  % Minimum coupling loss
        I;  % row of the site location within studyA.R
        J;  % column of the site location within studyA.R
        INfoc;  % logical variable that is true if the site is within focus
        fileName = [];  % filename of the propagation
        established;  % year the site was established
        rul = 0;  % network improvement rule applied on this site this year; 0 indicates no rule has been applied
        upg = {};  % structure that contains the upgrades this year
        ch;  % channels that are used by the site
        AeCou;  % number of antenna elements
        sectoCou;  % number of sectors
        pwrAmpClas;  % power amplifier class
        situ;  % situation
        maxSuppVel;  % maximum supported UE velocity
        maxSuppAppliDatRat;  % maximum supported application datarate
        EIRP_ovAllAnt_10MHz_100pcLoad_dBm;
        PTx_dBm;  % The tranmsit power over all antennas from the amplifier in dBm
        replEver;  % end-of-life cycle duration
        expiYea;  % year of expiry
        co_ch;  % co-channel cells
        Lbc;  % median path loss
        n;  % increase of median path loss with distance
        sigmaLe;  % standard deviation of the median path loss - UE external situation
        bareMetaBBUcou = 0;  % Base Band Unit (BBU) count
        cellProce;  % [cell processing] as defined in 5G Norma D2.2
        userDyn;  % [user dynamic] as defined in 5G Norma D2.2
        userRema;  % [user remaining] as defined in 5G Norma D2.2
        edgNodeProceCou;  % processor count of the edge cloud node
        capexCivWoAnAcqV = zeros(1,10);  % CAPEX civil works and acquisition for current and 9 trailing years
        capexBackhauV = zeros(1,10);  % CAPEX backhaul for current and 9 trailing years
        capexAntenFeedeV = zeros(1,10);  % CAPEX antennas and feeders for current and 9 trailing years
        capexBBbareMetaV = zeros(1,10);  % CAPEX baseband baremetal for current and 9 trailing years
        capexBBedgNodV = zeros(1,10);  % CAPEX baseband edge node for current and 9 trailing years
        capexRFfronEndV = zeros(1,10);  % CAPEX RF front end for current and 9 trailing years
        capexLabV = zeros(1,10);  % CAPEX labour for current and 9 trailing years
        capexV = zeros(1,10);  % CAPEX for current and 9 trailing years
        capexPv = 0;  % CAPEX present value
        opexRenV = zeros(1,10);  % OPEX rent for current and 9 trailing years
        opexRaAnUtV = zeros(1,10);  % OPEX rates and utilities for current and 9 trailing years
        opexVeSeV = zeros(1,10);  % OPEX vendor services for current and 9 trailing years
        opexLicensiAnMaV = zeros(1,10);  % OPEX licensing and maintainance for current and 9 trailing years
        opexBackhauV = zeros(1,10);  % OPEX backhaul for current and 9 trailing years
        opexV = zeros(1,10);  % OPEX for current and 9 trailing years
        opexPv = 0;  % OPEX present value
%         demServ;
        spatBeamCou;  % number of spatial beams
        MIMOstreaCou;  % number of MIMO streams
        RGB;  % colour for plotting the cell markers
        distBordm;  % distance from border
        residuErr;  % residual noise rise wrt linear fit, due to non-coordination
    end
    
    methods
function Cell = CELL(sty,Sim,studyA,edgCloSit,clu,IorX,JorY,heiAgl,operato,...
        siteID,sitChaiConfig,pwrAmpClas,situ,sectoCou,AeCou,BW,FBMC,...
        established,rul,replEver)
%%  CELL class constructor
% CELL is the class constructor of CELL objects, Cell.
% 
% The input sty identifies if the cell is a new commissioned site to expand
% the network or an existing operator site , and must be one of the
% following:
% ('exi','new')
% 
% If sty is 'new', then the following inputs are needed:
% sitChaiConfig is the site chain configuration index. 
%   if sitChaiConfig==1, then the site chain configuration is D-RAN with:
%     typ = 'Type 1: Antenna installation in combination with Bare Metal Node';
%     HWresouCateg = 'Programmable, purpose-built HW';
%     connTowaClo  = 'backhaul optical to central cloud';
%   if sitChaiConfig==2, then the site chain configuration is C-RAN with:
%     typ = 'Type 1: Antenna installation in combination with Bare Metal Node';
%     HWresouCateg = 'Non-programmable, purpose-built HW';
%     connTowaClo = 'CPRI/ORI-like fronthaul';
% The input pwrAmpClas is the BS power amplifier class which must be one of
% Sim.propag.BS(:,1).
% The input operato is the BS operator and must be one of
% Sim.dem.operators.
% The input situ is the BS situation, situ = 'outd'.
% The input AeCou is 1xN array, where N is the number of bands, with the
% number of antenna elements per band.
% The input BW is 1xN array, where N is the number of bands, with the
% bandwidth per band expressed in MHz.
% The input sectoCou is the number of sectors of the site.
% The input I,J is the BS location in row,column within the simulation map.
% The input heiAgl is the BS height agl in metres.
% The input established is the established date in integer.
% The input replEver is the period of the life-cycle in integer years.
% The input FBMC is always off.
% The input siteID is a string of the site ID.
% The input rul is the index to Sim.rul.
% 
% studyA is the study area object. [A list of the required fields is pending]
% edgCloSit is an array of EDGCLOSIT objects. [A list of the required fields is pending]
% Sim is the SIM object. [A list of the required fields is pending]
% 
% Note that for existing site it is unlikely that we know the date a site
% was last refreshed. We wish to avoid having a large number of sites
% expiring in the same year, and we wish to model a network that has been
% evolving evenly across the years. We model this by setting the estblish
% dates of the site as random uniform distribution, i.e.
% Sim.yea.start-randi(BSexi(n).replEver)
% 
% * v0.1 Kostas Konstantinou Jul 2017

% HO: realised that KK elminates
% multiple sectors or sites within 250m distance (correct, we are assuming
% each site is omni with max gain in every direction). Does this mean that
% Cell represent one BS? how do you consider sectors? @HO: why there are
% 4x4 cells in Cell().Lbc? KK this defines 4 frequency band + seomthing
% else (look at Cell.Lbc usage..) probablity mobility. WE actually have
% 4x4=16 cases. Probably the other 4 cases are defined in the first column
% of Sim.propag.UE: handh, mac, veh and tr? -> Yes.

if nargin ~= 0  % Allow nargin == 0 syntax
    switch sty
        case 'exi'
            % Existing cells
            % Prepare existing network
            if ~isempty(Sim.exiCel.filename)
                beep
                disp('Code this!')
                keyboard
    %             [~,~,raw] = xlsread( Sim.exiCel.filename );
    %             Sim.yea.thisYea = 2016;
    %             for n = 2 : size(raw,1)
    %                 Cell(n-1) = create_new_cell(raw{n,1},raw{n,2},raw{n,3},raw{n,4},...
    %                     raw{n,5},raw{n,6},raw{n,10},raw{n,11},raw{n,12},raw{n,13},...
    %                     raw{n,14},raw{n,15},raw{n,16},cell2mat(raw(n,17:20)),raw{n,21},...
    %                     raw{n,22},raw{n,23},edgCloSit,sitCivWrk,backhauEq,...
    %                     procesBlad,storagBlad,routinBlad,replEver,studyA,Sim);
    %             end
            else
                BSexi = prep_exi_net(Sim,studyA,clu);
            end
            
            % Contruct objects
            Cell(length(BSexi)) = CELL();
            for n = 1 : length(BSexi)
                Cell(n) = Cell(n).creat_cell(BSexi(n).sitChainConfig,...
                    BSexi(n).pwrAmpClas,BSexi(n).situ,BSexi(n).AeCou,...
                    BSexi(n).sectoCou,BSexi(n).Band,...
                    BSexi(n).X,BSexi(n).Y,BSexi(n).lat,BSexi(n).lon,Sim,...
                    BSexi(n).Operator,BSexi(n).Operator_Ref,...
                    BSexi(n).Antenna_height,BSexi(n).FBMC,studyA,...
                    BSexi(n).replEver,...
                    Sim.yea.start-randi(BSexi(n).replEver),BSexi(n).rul,...
                    edgCloSit);
            end
            
            % Remove non Jawwal cells:
%             Cell(~strcmp({BSexi.Operator_Real},'Jawwal')) = [];
            % Remove non Ooredoo cells:
%             Cell(~strcmp({BSexi.Operator_Real},'Ooredoo')) = [];
            % Remove sites that do not belong to the wanted InP
            switch Sim.BsExiFromSitefinder.(Sim.dem.operators{:})
              case {'Jawwal','Ooredoo'}
                Cell(~strcmp({BSexi.Operator_Real},Sim.BsExiFromSitefinder.(...
                    Sim.dem.operators{:}))) = [];
              case 'Thaleth'
                Cell(1:2:end) = [];
              case {'Sc_5','Sc_6','Sc_14','Sc_15','Israeli'}
              otherwise
                error('Undefined')
            end
        case {'new at row column','new at Easting Northing'}
            % New cell
            % Contruct object
            Cell = Cell.creat_cell(sty,sitChaiConfig,pwrAmpClas,situ,AeCou,...
                sectoCou,BW,IorX,JorY,[],[],Sim,operato,siteID,heiAgl,FBMC,...
                studyA,replEver,established,rul,edgCloSit);
        otherwise
            error('Check this!')
    end
end
end

%%
function fileName = c_fileName(Cell)
%% Create filename from Cell properties for the propagation database
% This function creates a filename from Cell properties for the propagation
% database
%
% * v0.1 KK 27Jul17 Created

% fileName = [DataHash(struct('X',Cell(cn).X,'Y',Cell(cn).Y,...
%     'heiAgl',Cell(cn).heiAgl)) '.mat'];
fileName = ['E' num2str(floor(Cell.X./1000)) ...
    'N' num2str(floor(Cell.Y./1000)) '_Lbc1000GRID.mat'];
% fileName = ['E' num2str(floor(Cell.X./5000).*5) ...
%     'N' num2str(floor(Cell.Y./5000).*5) '_Lbc5000GRID.mat'];
% fileNameFAT = ['E' num2str(floor(Cell.X./5000).*5) ...
%     'N' num2str(floor(Cell.Y./5000).*5) '_Lbc5000FAT.mat'];
end

%%
function Cell = set_co_ch(Cell)
% For each cell, find which other cells are co-channel with it

% %HO: set_co_ch is a method defined Cell. It finds the co-channel
% interference for that cell and save results in boolean matrix Cell.co_ch.
% @HO: Do we really use this info in the model? Yes look at the method. 

for n = 1 : length(Cell)
    Cell(n).co_ch = full(any(repmat(Cell(n).ch,1,length(Cell)) & ...
        [Cell.ch],1)).';
end
end

%% 
function Cell = s_tilt(Cell,Sim)

tilt = [Cell.tilt];
    
ch = full([Cell.ch]);
for m = 1 : size(ch,1)
    IND = find(ch(m,:));
    L = length(IND);
    if L > 1
        X = [Cell(IND).X].';
        Y = [Cell(IND).Y].';
        
        % Calculate the ISD for setting the tilt
        ISDm = nan(size(length(IND),1),6);
        inc = 360 ./ size(ISDm,2);
        ISDm(1,:) = inc./2 : inc : 360;
        for n = 1 : length(IND)
            oth = [1:n-1 n+1:length(IND)];
            Xdiff = X(oth) - X(n);
            Ydiff = Y(oth) - Y(n);
            distm2 = Xdiff.^2 + Ydiff.^2;
            az = wrapTo360(atan2d(Ydiff,Xdiff));
            subs = floor(az./(360./size(ISDm,2))) + 1;
            ISDm = [ISDm
                sqrt(accumarray(subs,distm2,[size(ISDm,2) 1],@min,nan)).'];
        end

        tilt(Sim.linBud.chRaste{2,4+m},IND) = floor(nanmean(...
            bsxfun(@plus,atand([Cell(IND).heiAgl].' ./ ...
            (nanmean(ISDm(2:end,:),2).*Sim.propag.ISDfact)),...
            reshape([Cell(IND).HPBWver],...
            length(Sim.linBud.freq),nnz(IND)).'./Sim.propag.V3dBfact),2));
    elseif L == 1
        tilt(Sim.linBud.chRaste{2,4+m},IND) = 0;
    end
end

tilt = num2cell(tilt,1);
[Cell.tilt] = deal(tilt{:});
end

%%
function p_map_tilt(Cell,studyA,Sim,fn)

% Plot 10dB att map

% Plot map
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 0.9])

plot(studyA.X,studyA.Y,'k','linewidth',1)

Xlim = xlim();
Ylim = ylim();
set(gca,'xlim',Xlim,'ylim',Ylim)

Col = jet(length(Cell));
for n = 1 : length(Cell)
    if Cell(n).band.supp(fn) && ...
            strcmp(Cell(n).pwrAmpClas,'macrocell')
    else
        continue
    end
    sectoCou = Cell(n).sectoCou;
    az0 = linspace(0,360-360./(sectoCou),sectoCou).';
    for s = 1 : sectoCou
        % ISO, physics
        figure
        [az,el] = meshgrid(0:360,0:180);
        AH = min(12.*((az-wrapTo360(90-az0(s))) ./ ...
            Cell(n).HPBWhor(fn)).^2,Sim.propag.Am);
        AV = min(12.*((el-wrapTo180(90+Cell(n).tilt(fn,s))) ./ ...
            Cell(n).HPBWver(fn)).^2,Sim.propag.SLAv);
        G = -min(AH+AV,Sim.propag.Am);
%         C = contour(az,el,G,[-Inf -10]);
        C = contour(az,el,G,[-Inf -3]);
        delete(gcf)
        S = contourdata(C);
        az = cell2mat(arrayfun(@(x) [x.xdata;nan],S,'UniformOutput',false).');
        el = cell2mat(arrayfun(@(x) [x.ydata;nan],S,'UniformOutput',false).');
        
        zc10 = 1.5 - Cell(n).heiAgl;
        t = zc10 ./ cosd(el);
        t(isinf(t)) = NaN;
        t(t<1) = NaN;
        xc10 = t .* sind(el) .* cosd(az);
        yc10 = t .* sind(el) .* sind(az);
        
        plot(Cell(n).X+xc10,Cell(n).Y+yc10,'color',Col(n,:),'linewidth',1)
        plot(Cell(n).X,Cell(n).Y,'marker','^','color',Col(n,:))
    end
end
% clear s n lat lon
%
% plot_google_map('ShowLabels',0,'MapType','terrain');
%
% print(gcf,'10dB att map v0_1 Sites 21 v1_3','-djpeg','-r300','-zbuffer')
% plotm(lat(isAlt(IN)),lon(isAlt(IN)),'.')

end

%%
function Cell = set_connTowaClo(Cell,edgCloSit,Sim)
% For each cell, find which other cells are co-channel with it
[Cell,connTowaClo] = Cell.c_connTowaClo;
switch connTowaClo
    case 'backhaul optical to central cloud'
        % sitChaiConfig is 1 or 3, S1 link
        trReq = 187.5 .* sum(Cell.band.capa./20 .* ...
            Cell.spatBeamCou.*Cell.MIMOstreaCou) ./ 2 .* 1e-3 .* ...
            Cell.sectoCou;  % Gbit/s
        Cell.connTowaCloID = 0;
    case 'CPRI/ORI-like fronthaul'
        % sitChaiConfig is 2
        trReq = ceil(2.451e3.*sum(Cell.band.capa./20 .* ...
            Cell.spatBeamCou.*Cell.MIMOstreaCou)./2./614.4) .* ...
            614.4e-3 .* Cell.sectoCou;  % Gbit/s
        [~,Cell.connTowaCloID] = min(abs((Cell.X-[edgCloSit.X]).^2 + ...
            (Cell.Y-[edgCloSit.Y]).^2));
    otherwise
        error('Include this!')
end
IND = [Sim.cost.tr.lim1]<=trReq & trReq<[Sim.cost.tr.lim2];

% Calculate backhauUpg
if isempty(Cell.connTowaCloTyp)
    % New site
    Cell.upg = [Cell.upg
        {'backhau',{'backhauNew'}}];
else
    % Existing site
    if ~strcmp(Cell.connTowaCloTyp,Sim.cost.tr(IND).v{Cell.sitChaiConfig})
        % Backhaul type upgrade
        Cell.upg = [Cell.upg
            {'backhau',{'backhauUpgTyp'}}];
    elseif Cell.connTowaCloBW < Sim.cost.tr(IND).lim2
        % Backhaul BW upgrade
        Cell.upg = [Cell.upg
            {'backhau',{'backhauUpgBW',Cell.connTowaCloBW}}];
    else
    end
end

Cell.connTowaCloTyp = Sim.cost.tr(IND).v{Cell.sitChaiConfig};
Cell.connTowaCloBW = Sim.cost.tr(IND).lim2;
end

%%
function [Cell,connTowaClo] = c_connTowaClo(Cell)
switch Cell.sitChaiConfig
    case 1
        typ = 'Type 1: Antenna installation in combination with Bare Metal Node';
        HWresouCateg = 'Programmable, purpose-built HW';
        connTowaClo  = 'backhaul optical to central cloud';
    case 2
        typ = 'Type 1: Antenna installation in combination with Bare Metal Node';
        HWresouCateg = 'Non-programmable, purpose-built HW';
        connTowaClo = 'CPRI/ORI-like fronthaul';
    case 3
        typ = 'Type 2: Antenna installation in combination with Bare Metal Node and Edge Cloud Node, Variant 1';
        HWresouCateg = 'Non-programmable, purpose-built HW';
        connTowaClo = 'backhaul optical to central cloud';
    otherwise
        error('Not coded')
end

Cell.typ = typ;
Cell.HWresouCateg = HWresouCateg;

% Antenna site latency
% If the cell is of Type 1, with Programmable HW, then it cannot support low latency traffic
if strcmp('Type 1: Antenna installation in combination with Bare Metal Node',Cell.typ) && ...
        strcmp('Programmable, purpose-built HW',Cell.HWresouCateg)
    Cell.laten = 10;
else
    Cell.laten = 2;
end
end

%% creat_cell
function Cell = creat_cell(Cell,sty,sitChaiConfig,pwrAmpClas,situ,AeCou,...
    sectoCou,BW,X,Y,lat,lon,Sim,operato,siteID,heiAgl,FBMC,studyA,...
    replEver,established,rul,edgCloSit)
% Set fields that do not need calculation
Cell.Operator = operato;
Cell.siteID = siteID;
Cell.antID = '1';
% Cell.maxDist_m = 0;
Cell.band = struct('supp',BW>0,...
    'utilis',zeros(size(Sim.dem.hourDistri,1),length(Sim.dem.operators),...
    length(BW),length(Sim.dem.famUseCase_)),...
    'capa',BW,...
    'utilisBefRese',zeros(size(Sim.dem.hourDistri,1),length(Sim.dem.operators),...
    length(BW),length(Sim.dem.famUseCase_)));
Cell.FBMC = FBMC;
Cell.heiAgl = heiAgl;
Cell.MCL = Sim.linBud.(pwrAmpClas).(situ).MCL;
% Cell.fileName = fileName;
% Cell.propagBandAvai = Sim.propag.BS{strcmp(pwrAmpClas,Sim.propag.BS(:,1)),6};
Cell.established = established;
Cell.rul = rul;
Cell.sectoCou = sectoCou;
Cell.pwrAmpClas = pwrAmpClas;
Cell.situ = situ;
Cell.AeCou = AeCou;
Cell.RGB = [226 0 122];
Cell.n = zeros(length(BW),size(Sim.propag.UE,1));
Cell.tilt = nan(length(BW),1);

% Set fields relevant to the coordinates
if isempty(lat)
    switch sty
      case 'new at row column'
        % I,J are provided as input
        I = X;
        J = Y;
        [X,Y] = studyA.R.intrinsicToWorld(J,I);
      case 'new at Easting Northing'
        [I,J] = studyA.R.worldToDiscrete(X,Y);
      otherwise
        error('Undefined')
    end
    
    if isfield(Sim.mstruct,'ostn')
        [lat,lon] = easNor2latLon(X,Y,Sim.mstruct.ostn);
    elseif isfield(Sim.mstruct,'mapprojection')
        [lat,lon] = minvtran(Sim.mstruct,X,Y);
    else
        error('Code this!')
    end
else
    % X,Y and lat,lon are provided as input
    [I,J] = studyA.R.worldToDiscrete(X,Y);
end
Cell.X = X;
Cell.Y = Y;
Cell.I = I;
Cell.J = J;
Cell.lat = lat;
Cell.lon = lon;
Cell.INfoc = studyA.INfoc(I,J);
% Cell.NGR = EN2NGR(X,Y);

% Set fields relevant to proximity to border
switch Sim.bordCoor
  case 'ignore'
%     Cell.isIN250 = false;
%     Cell.isIN2000 = false;
    Cell.distBordm = inf;
    Cell.residuErr = NaN;
  case {'coordinated','non_coordinated'}
%     Cell.isIN250 = studyA.IN250(I,J);
%     Cell.isIN2000 = studyA.IN2000(I,J);
    Cell.distBordm = p_poly_dist(X,Y,...
        studyA.bordSimplif(:,1).',studyA.bordSimplif(:,2).',false);
    Cell.residuErr = studyA.residuErr(I,J);
  otherwise
    error('Undefined')
end

% Set fields spatBeamCou and MIMOstreaCou
Cell.spatBeamCou = zeros(1,length(Sim.linBud.str));
Cell.MIMOstreaCou = zeros(1,length(Sim.linBud.str));
[~,col] = find(bsxfun(@eq,...
    [Sim.cost.spatBeamAndMIMOstreaCou.BSanteCou],Cell.AeCou.'));
supp = Cell.band.supp;
Cell.spatBeamCou(supp) = [Sim.cost.spatBeamAndMIMOstreaCou(col).spatBeamCou];
Cell.MIMOstreaCou(supp) = [Sim.cost.spatBeamAndMIMOstreaCou(col).MIMOstreaCou];
clear col

% Set fields relevant to the site chain configuration
if iscell(sitChaiConfig)
    beep
    disp('Code this functionality!')
    keyboard
else
    Cell.sitChaiConfig = sitChaiConfig;
    Cell = Cell.set_connTowaClo(edgCloSit,Sim);
end

% Find channels that are used by the site
tmp = cell2mat(Sim.linBud.chRaste([false;true;false;false
    strcmp(pwrAmpClas,Sim.linBud.chRaste(5:end,2)) & ...
    strcmp(operato,Sim.linBud.chRaste(5:end,3))],5:end));
ch = sparse((Cell.band.supp(tmp(1,:))&tmp(2,:)==1).');
Cell.ch = ch;

%%
assert(all(round(Cell.band.capa./5).*5==Cell.band.capa) || ...
    all(Cell.band.capa==[0	18.467	0	0]) || ...
    all(Cell.band.capa==[0	18.467	20	0]) || ...
    all(Cell.band.capa==[0	8.79	0	0]) || ...
    all(Cell.band.capa==[0	8.79	20	0]),...
    'FFT minimum BW should be multiples of 5MHz')

% Bare metal base band unit
Cell = Cell.c_bareMetaBBUcou;

% Antenna site processing
% Cell processing (FFT)
Cell.cellProce = sum(Cell.band.capa./20.*Cell.spatBeamCou .* ...
    Cell.MIMOstreaCou.*0.27./Sim.cost.procImprov) .* Cell.sectoCou;

% Further antenna parameters
% Set vertical half power beamwidth per band
% For macrocells, < 3500 MHz:
%   https://amphenol-antennas.com/datasheets/6800400.pdf
%   f = [698 806 790 862 824 894 880 960 1695 1880 1850 1990 1920 2180 2300 ...
%      2500 2490 2690 1695 1880 1850 1990 1920 2180 2300 2500 2490 2690 1695 1880 1850 1990 1920 2180 2300 2500 2490 2690 1695 1880 1850 1990 1920 2180 2300 2500 2490 2690];
%   v = [8.7 7.9 7.7 7.3 7.4 6.6 6.1 5.4 5.0 8.0 7.3 6.7 5.4 5.2 7.2 6.7 ...
%         6.1 5.3 5.0 8.0 7.2 6.3 5.4 5.2];
%   figure
%   plot((f(1:2:end)+f(2:2:end))./2,v,'.')
%   grid on
% For macrocells, at 3500 MHz:
%   D:\Dropbox (Real Wireless)\rW shared new\Projects\ComReg under Oxera Lead
%   - Mar 2017\12 Investi\Link Budget\Background\MA421X39.pdf
% For small cells:
%   We endorse the assumption of TR 36.814 'Use of antenna downtilt and
%   vertical antenna FFS', and we set the HPBWver to zero to cancel any
%   effect of it.
%   We set the HPBWhor to 18000 deg to effectively remove any attenuation
%   due to horizontal pattern.
Cell = Cell.set_EIRP_ovAllAnt_10MHz_100pcLoad_dBm(Sim);
Cell.maxSuppVel = Sim.linBud.(Cell.pwrAmpClas).(Cell.situ).maxSuppVel;
switch Cell.pwrAmpClas
    case 'macrocell'
%         Cell.maxSuppAppliDatRat = [2015 2020 2025 2030; 2 34 50 50];
        Cell.maxSuppAppliDatRat = [2015 2020 2025 2030; 34 34 50 50];
        Cell.HPBWhor = [65.*ones(1,length(Sim.linBud.str)-1) nan];
        Cell.HPBWver = [11.5 10.8 9.9 9.9 7.7 6.4 5.8 5.8 5.8 4.8 4.8 4.8 NaN];
    case 'smaCel5W'
%         Cell.maxSuppAppliDatRat = [2015 2020 2025 2030; 17 38 100 100];
        Cell.maxSuppAppliDatRat = [2015 2020 2025 2030; 38 38 100 100];
%         Cell.HPBWhor = [nan 65./120.*180 65./120.*180 nan];
%         Cell.HPBWver = [nan 18000 18000 nan];
        Cell.HPBWhor = 36000 .* ones(1,length(Sim.linBud.str));
        Cell.HPBWver = 31 .* ones(1,length(Sim.linBud.str));
    case 'smaCel0W25'
%         Cell.maxSuppAppliDatRat = [2015 2020 2025 2030
%             20 50 1000 1000];
        Cell.maxSuppAppliDatRat = [2015 2020 2025 2030
            50 50 1000 1000];
        Cell.HPBWhor = [nan nan nan 18000];
        Cell.HPBWver = [nan nan nan 0];
end

% The maximum distance for current BS power amplifier class
% Cell.maxDist_m = Sim.propag.BS{strcmp(Cell.pwrAmpClas,...
%     Sim.propag.BS(:,1)),5};

Cell.replEver = replEver;
Cell.expiYea = Cell.established + Cell.replEver;

% The filename
Cell.fileName = Cell.c_fileName;
end

%%
function Cell = set_EIRP_ovAllAnt_10MHz_100pcLoad_dBm(Cell,Sim)
tmp = structfun(@(x) x,Sim.linBud.(Cell.pwrAmpClas...
    ).outd.EIRP_ovAllAnt_10MHz_100pcLoad_dBm);
if ~any(isnan(tmp))
    Cell.EIRP_ovAllAnt_10MHz_100pcLoad_dBm = tmp;
    Cell.EIRP_ovAllAnt_10MHz_100pcLoad_dBm(~Cell.band.supp) = -999;
    Cell.PTx_dBm = [];
    return
end

tmp = structfun(@(x) x,...
    Sim.linBud.(Cell.pwrAmpClas).outd.PTx_antConn_perAnt_100pcLoad_dBm);
if ~any(isnan(tmp))
    Cell.EIRP_ovAllAnt_10MHz_100pcLoad_dBm = tmp + ...
        10.*log10(Cell.AeCou.') + structfun(@(x) x,...
        Sim.linBud.(Cell.pwrAmpClas).outd.GA);
    Cell.EIRP_ovAllAnt_10MHz_100pcLoad_dBm(~Cell.band.supp) = -999;
    Cell.PTx_dBm = [];
    return
end

tmp = structfun(@(x) x,...
    Sim.linBud.(Cell.pwrAmpClas).outd.PTx_ovAllAnt_100pcLoad_dBm);
if ~any(isnan(tmp))
    Cell.PTx_dBm = tmp;
    Cell.PTx_dBm(~Cell.band.supp) = -999;
    Cell.EIRP_ovAllAnt_10MHz_100pcLoad_dBm = [];
    return
end
end

%%
function Cell = c_userDyn(Cell,Sim)
for n = 1 : length(Cell)
    % Antenna site processing
    % user processing dynamic (modulation and forward error correction)
    % user processing remainder (RMSE, headers, higher layer)
    % utilis(size(Sim.dem.hourDistri,1),length(Sim.dem.operators),...
%     length(BW),length(Sim.dem.famUseCase_))
    BW = bsxfun(@times,Cell(n).band.capa,...
        squeeze(max(sum(Cell(n).band.utilis,2),[],1)).');
    tmp = bsxfun(@times,BW./20,Cell(n).spatBeamCou.*Cell(n).MIMOstreaCou);
    Cell(n).userDyn = sum(tmp.*0.87./Sim.cost.procImprov,2) .* ...
        Cell(n).sectoCou;
    Cell(n).userRema = sum(tmp.*0.24,2) .* Cell(n).sectoCou;

    % Edge node processors
    switch Cell(n).pwrAmpClas
        case 'macrocell'
            switch Cell(n).sitChaiConfig
                case 1
                    peaMarg = [];
                case 2
                    peaMarg = [];
                case 3
                    peaMarg = 1.5;
                otherwise
                    error('Not coded')
            end
        case 'smaCel5W'
            peaMarg = [];
        case 'smaCel0W25'
            peaMarg = [];
        otherwise
            error('Not coded')
    end
    if ~isempty(peaMarg)
        % D-RAN
        Cell(n).edgNodeProceCou = Cell(n).cellProce + ...
            Cell(n).userDyn.*peaMarg + Cell(n).userRema;
    else
        % C-RAN
        Cell(n).edgNodeProceCou = [];
    end
end
end

%%
function coreCou = c_coreCou(Cell)
peaMarg = 1.25;
tmp = [Cell.userDyn].*peaMarg + [Cell.userRema];  %HO: cell processing, user dynamic and user remaining are described in the processing requirement documentations
tmp2 = sum(tmp,2);
coreCou = tmp2./sum(tmp2)*[Cell.cellProce] + tmp;
end

%%
function Cell = c_bareMetaBBUcou(Cell)
capa = Cell.band.capa;
switch Cell.pwrAmpClas
    case 'macrocell'
    case 'smaCel5W'
        if capa(2) == 8.79
            capa(2) = 10;
        elseif capa(2) == 18.467
            capa(2) = 20;
        elseif capa(2) == 23.084
            capa(2) = 25;
        elseif round(capa(2)./5).*5 == capa(2)
        else
            error('Undefined')
        end
    case 'smaCel0W25'
    otherwise
        sdgfd
end

switch Cell.pwrAmpClas
    case {'macrocell','smaCel5W','smaCel0W25'}
        switch Cell.sitChaiConfig
            case 1
                proc = true;
            case 2
                proc = false;
            case 3
                proc = false;
            otherwise
                error('Not coded')
        end
    otherwise
        error('Not coded')
end
if proc
    Cell.bareMetaBBUcou = sum(capa./20.*Cell.spatBeamCou .* ...
        Cell.MIMOstreaCou./2);
else
    Cell.bareMetaBBUcou = 0;
end
% clear proc
end

%% 
function Cell = apply_rul(Cell,rn,Sim,edgCloSit)
% This code runs only for existing (upgrading) sites
    
% Update rul property
Cell.rul = rn;

% Store current properties
bareMetaBBUcou = Cell.bareMetaBBUcou;
AeCou = Cell.AeCou;
capexRFfronEndV = Cell.c_capexRFfronEndV(Sim);
capexBBbareMetaV = Cell.c_capexBBbareMetaV(Sim);

% Update sitChaiConfig and bareMetaBBUcou properties
if Cell.sitChaiConfig ~= Sim.rul(rn).upg.sitChainConfig
    Cell.sitChaiConfig = Sim.rul(rn).upg.sitChainConfig;
    Cell.bareMetaBBUcou = 0;
end

tmp2 = Cell.band.capa ~= Sim.rul(rn).upg.Band;
tmp3 = Cell.AeCou ~= Sim.rul(rn).upg.AeCou;
if any(tmp2|tmp3)
    % Spectrum and/or antenna upgrade
    % Apply property changes
    Cell.band.capa = Sim.rul(rn).upg.Band;
    Cell.AeCou = Sim.rul(rn).upg.AeCou;
    Cell.band.supp = Cell.band.capa > 0;
    Cell = Cell.set_EIRP_ovAllAnt_10MHz_100pcLoad_dBm(Sim);
    %
    Cell.spatBeamCou(:) = 0;
    Cell.MIMOstreaCou(:) = 0;
    [~,col] = find(bsxfun(@eq,...
        [Sim.cost.spatBeamAndMIMOstreaCou.BSanteCou],Cell.AeCou.'));
    supp = Cell.band.supp;
    Cell.spatBeamCou(supp) = [Sim.cost.spatBeamAndMIMOstreaCou(col).spatBeamCou];
    Cell.MIMOstreaCou(supp) = [Sim.cost.spatBeamAndMIMOstreaCou(col).MIMOstreaCou];
    clear col
    %
    Cell = Cell.c_bareMetaBBUcou;
    %
    Cell.cellProce = sum(Cell.band.capa./20.*Cell.spatBeamCou .* ...
        Cell.MIMOstreaCou.*0.27./Sim.cost.procImprov) .* Cell.sectoCou;
    
    % Simplification
    curre = Sim.rul(rn).curre.Band;
    upg = Sim.rul(rn).upg.Band;
    isMoreBand = any(curre==0&upg>0,2);
    isMoreBWinExisBand = any(curre~=upg,2) & ~isMoreBand;
    
    % Run propagation if more bands
    if any(tmp2)
        % More spectrum in existing bands, or more bands
        if isMoreBWinExisBand
            % More spectrum in existing bands
        elseif isMoreBand
            % More bands
            tmp = cell2mat(Sim.linBud.chRaste([false;true;false;false
                strcmp(Cell.pwrAmpClas,Sim.linBud.chRaste(5:end,2)) & ...
                strcmp(Cell.Operator,Sim.linBud.chRaste(5:end,3))],5:end));
            Cell.ch = sparse((Cell.band.supp(tmp(1,:))&tmp(2,:)==1).');
        else
            beep
            disp('Check why here')
            keyboard
        end
    else
        % More antennas
    end
    
    % Update upg with property changes
    if any(tmp3)
        Cell.upg = [Cell.upg
            {'antenFeede',[AeCou; Cell.AeCou]}];
    end
    Cell.upg = [Cell.upg
        {'RFfronEnd',capexRFfronEndV}];
    BBbareMeta = [bareMetaBBUcou; Cell.bareMetaBBUcou];
    if diff(BBbareMeta) > 0
        % The structure is [count before, cost per year before]}
        Cell.upg = [Cell.upg
            {'BBbareMeta',{bareMetaBBUcou,capexBBbareMetaV}}];
    end
else
end

if Cell.sectoCou ~= Sim.rul(rn).upg.sectoCou
    dfhrgfthth
end

if Sim.rul(rn).curre.sitChainConfig == Sim.rul(rn).upg.sitChainConfig || ...
        Sim.yea.thisYea == Cell.expiYea
    % If the site switches site chain configuration, then do not update its
    % birthday, unless it's the expiry date anyway
    Cell.established = Sim.yea.thisYea;
    Cell.expiYea = Sim.yea.thisYea + Cell.replEver;
end

Cell = Cell.set_connTowaClo(edgCloSit,Sim);
end

%%
function capexRFfronEndV = c_capexRFfronEndV(Cell,Sim)
tmp = Sim.cost.capex.RFfronEnd;

tmp2 = cellfun(@(x) ['band' num2str(x)],num2cell(1:13),...
    'UniformOutput',false);
v = cell2mat(cellfun(@(x) [tmp.(x)].',tmp2,'UniformOutput',false));

capexRFfronEndV = zeros(1,10);
for n = 1 : length(Cell.AeCou)
    if Cell.AeCou(n) > 0
        IND = strcmp({tmp.pwrAmpClas},Cell.pwrAmpClas) & ...
            [tmp.sectoCou]==Cell.sectoCou & [tmp.antenCou]==Cell.AeCou(n);
        this = Sim.cost.capex.RFfronEnd(IND);
        capexRFfronEndV = capexRFfronEndV + Cell.band.capa(n) ./ 20 .* ...
            v(IND,n) .* ...
            (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
    end
end
end

%%
function capexBBbareMetaV = c_capexBBbareMetaV(Cell,Sim)
this = Sim.cost.capex.BBbareMeta;
if strcmp(this.pwrAmpClas,Cell.pwrAmpClas)
    capexBBbareMetaV = this.cost_per_baseline_BBU_k .* Cell.bareMetaBBUcou .* ...
        (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
else
    capexBBbareMetaV = zeros(1,10);
end
end

%%
function Cell = c_cost(Cell,Sim)
%% Common calculations
capexRFfronEndV = Cell.c_capexRFfronEndV(Sim);
capexBBbareMetaV = Cell.c_capexBBbareMetaV(Sim);

%% CAPEX Calculation
if Cell.rul ~= 0
    % This site has changed this year
    if ~Sim.rul(Cell.rul).isDecomm
        % Commission new site or upgrade existing site
        if ~Sim.rul(Cell.rul).isUpg
            % Commission new site
            Cell = Cell.c_capex(Sim,capexRFfronEndV,capexBBbareMetaV,'new');
        else
            % Upgrade existing site
            Cell = Cell.c_capex(Sim,capexRFfronEndV,capexBBbareMetaV,'upg');
        end
    else
        % Reinstatement of expiring or expired site
        Cell = Cell.c_capex(Sim,capexRFfronEndV,capexBBbareMetaV,'decomm');
    end
end

%% OPEX Calculation
Cell = Cell.c_opex(Sim,capexRFfronEndV,capexBBbareMetaV);
end

%%
function Cell = c_capex(Cell,Sim,capexRFfronEndV,capexBBbareMetaV,sty)
% calc_CAPEX_cost is a helper function to calculate the CAPEX cost of the cell. cur is the
% base station type and cost is the structure with the CAPEX cost values. costVari shows the price
% decrease year on year for the different CAPEX elements while the costEros is the price decrease of
% the optical fibre costs. Active costs are the costs for the RF front end and base band

% Find the years in which the site is refreshed
isRefre = zeros(1,10);
isRefre(1:Cell.replEver:10) = 1;

switch sty
    case 'new'
        % Commission new site
        tmp = 'antenSitNew';  % 'antenSitNew' or 'antenSitUpg'
        IND = strcmp({Sim.cost.capex.(tmp).pwrAmpClas},Cell.pwrAmpClas) & ...
            [Sim.cost.capex.(tmp).sitChaiConfig] == Cell.sitChaiConfig;
        if isempty(Cell.upg)
            tmp2 = 'backhauNew';  % 'backhauNew', 'backhauUpgTyp' or 'backhauUpgBW'
        else
            INDupg = strcmp(Cell.upg(:,1),'backhau');
            tmp2 = Cell.upg{INDupg,2}{1};  % 'backhauNew', 'backhauUpgTyp' or 'backhauUpgBW'
        end
        tmp3 = Sim.cost.capex.(tmp2);
        thisBackhau = tmp3(strcmp(Cell.connTowaCloTyp,{tmp3.typ}) & ...
            Cell.connTowaCloBW==[tmp3.BW_Gbps]);
        Cell = Cell.c_capexPerYeaNew(Sim,capexRFfronEndV,capexBBbareMetaV,...
            isRefre,Sim.cost.capex.(tmp)(IND),thisBackhau);
    case {'upg','decomm'}
        % Upgrade existing site, or reinstatement of expiring or expired site
        tmp = 'antenSitUpg';  % 'antenSitNew' or 'antenSitUpg'
        IND = strcmp({Sim.cost.capex.(tmp).pwrAmpClas},Cell.pwrAmpClas) & ...
            [Sim.cost.capex.(tmp).sitChaiConfig] == Cell.sitChaiConfig;
        if isempty(Cell.upg)
            thisBackhau = struct('cost_k',0,'costVariat',0,'yea',2015);
        else
            INDupg = strcmp(Cell.upg(:,1),'backhau');
            if any(INDupg)
                tmp2 = Cell.upg{INDupg,2}{1};  % 'backhauNew', 'backhauUpgTyp' or 'backhauUpgBW'
                tmp3 = Sim.cost.capex.(tmp2);
                switch tmp2
                    case 'backhauUpgTyp'
                        assert(strcmp(Cell.connTowaCloTyp,{tmp3.typTo}))
                        thisBackhau = tmp3;
                        assert(strcmp(thisBackhau.cost,'new'))
                        tmp2 = 'backhauNew';  % 'backhauNew', 'backhauUpgTyp' or 'backhauUpgBW'
                        tmp3 = Sim.cost.capex.(tmp2);
                        thisBackhau = tmp3(strcmp(Cell.connTowaCloTyp,{tmp3.typ}) & ...
                            Cell.connTowaCloBW==[tmp3.BW_Gbps]);
                    case 'backhauUpgBW'
                        thisBackhau = tmp3(Cell.upg{INDupg,2}{2}==[tmp3.typFrom] & ...
                            Cell.connTowaCloBW==[tmp3.typTo]);
                    otherwise
                end
            else
                thisBackhau = struct('cost_k',0,'costVariat',0,'yea',2015);
            end
        end
        
        Cell = Cell.c_capexPerYeaNew(Sim,capexRFfronEndV,capexBBbareMetaV,...
            isRefre,Sim.cost.capex.(tmp)(IND),thisBackhau);
end
end

%% function c_capexPerYeaNew
function Cell = c_capexPerYeaNew(Cell,Sim,capexRFfronEndV,capexBBbareMetaV,...
    isRefre,antenSit,thisBackhau)
% Simplifications
antenSitRepl = Sim.cost.capex.antenSitRepl;

% Calculate sty
if Cell.rul > 0
    if ~Sim.rul(Cell.rul).isDecomm
        % Commission new site or upgrade existing site
        if ~Sim.rul(Cell.rul).isUpg
            % Commission new site
            sty = 'new';
        else
            % Upgrade existing site
            sty = 'upg';
        end
    else
        % Reinstatement of expiring or expired site
        sty = 'new';
    end
else
    sty = 'new';
end

% Add to capexPerYea cost related to different components
f_ = {'sitCivWoAnAcq','backhau','antenFeede','RFfronEnd','BBbareMeta',...
    'BBedgNod'};
for fn = 1 : length(f_)
    f = f_{fn};
    if logical(antenSit.(f))
        thisIsRefre = isRefre;
        if ~logical(antenSitRepl.(f))
            thisIsRefre(2:end) = 0;
        end

        % Create this cost element
        switch f
            case 'sitCivWoAnAcq'
                this = Sim.cost.capex.(f)(strcmp({Sim.cost.capex.(f).pwrAmpClas},...
                    Cell.pwrAmpClas));
            case 'backhau'
                this = thisBackhau;
            case 'antenFeede'
                switch sty
                    case 'new'
                        tmp = Sim.cost.capex.(f);
                        this = struct('cost_k',0,'costVariat',-0.03,'yea',2016);
                        
                        for m = 1 : size(Sim.cost.anten,1)
                            AeCou = max(Cell.AeCou(Sim.cost.anten(m,:)));
                            if AeCou > 0
                                tmp2 = tmp(strcmp({tmp.pwrAmpClas},Cell.pwrAmpClas) & ...
                                    [tmp.sectoCou]==Cell.sectoCou & ...
                                    [tmp.antenCou]==AeCou);
                                assert(tmp2.yea==this.yea)
                                assert(tmp2.costVariat==this.costVariat)
                                this.cost_k = this.cost_k + ...
                                    tmp2.antenCost_k + tmp2.othCostNew_k;
                            end
                        end
                    case 'upg'
                        if ~isempty(Cell.upg)
                            INDupg = strcmp(Cell.upg(:,1),'antenFeede');
                            this = struct('cost_k',0,'costVariat',-0.03,'yea',2016);
                            if any(INDupg)
                                tmp = Sim.cost.capex.(f);
                                
                                for m = 1 : size(Sim.cost.anten,1)
                                    if nnz(Sim.cost.anten(m,:)) == 1
                                        AeCou1 = Cell.upg{INDupg,2}(1,...
                                            Sim.cost.anten(m,:));
                                        AeCou2 = Cell.upg{INDupg,2}(2,...
                                            Sim.cost.anten(m,:));
                                    else
                                        AeCou1 = max(Cell.upg{INDupg,2}(1,...
                                            Sim.cost.anten(m,:)));
                                        AeCou2 = max(Cell.upg{INDupg,2}(2,...
                                            Sim.cost.anten(m,:)));
                                    end
                                    
                                    AeCouDiff = AeCou2 - AeCou1;
                                    if AeCouDiff > 0
                                        tmp2 = tmp(strcmp({tmp.pwrAmpClas},Cell.pwrAmpClas) & ...
                                            [tmp.sectoCou]==Cell.sectoCou & ...
                                            [tmp.antenCou]==AeCou2);
                                        assert(tmp2.yea==this.yea)
                                        assert(tmp2.costVariat==this.costVariat)
                                        this.cost_k = this.cost_k + ...
                                            tmp2.antenCost_k + tmp2.othCostNew_k;
                                    end
                                end
                            else
                                this = struct('cost_k',0,'costVariat',0,'yea',2015);
                            end
                        else
                            this = struct('cost_k',0,'costVariat',0,'yea',2015);
                        end
                    otherwise
                        sdgfergre
                end
            case 'RFfronEnd'
                switch sty
                    case 'new'
                    case 'upg'
                        if ~isempty(Cell.upg)
                            INDupg = strcmp(Cell.upg(:,1),'RFfronEnd');
                            if any(INDupg)
                                if all(Cell.upg{INDupg,2}==capexRFfronEndV)
                                    capexRFfronEndV(:) = 0;
                                else
                                    capexRFfronEndV = capexRFfronEndV - ...
                                        Cell.upg{INDupg,2};
                                end
                            else
                                capexRFfronEndV(:) = 0;
                            end
                        else
                            this = struct('cost_k',0,'costVariat',0,'yea',2015);
                        end
                otherwise
                    sdfsd
                end
            case 'BBbareMeta'
                switch sty
                    case 'new'
                        this = struct('cost_k',0,'costVariat',0,'yea',2015);
                    case 'upg'
                        if ~isempty(Cell.upg)
                            INDupg = strcmp(Cell.upg(:,1),'BBbareMeta');
                            if any(INDupg)
                                if all(Cell.upg{INDupg,2}{2}==capexBBbareMetaV)
                                    capexBBbareMetaV(:) = 0;
                                else
                                    capexBBbareMetaV = capexBBbareMetaV - ...
                                        Cell.upg{INDupg,2}{2};
                                end
                            else
                                capexBBbareMetaV(:) = 0;
                            end
                        else
                            this = struct('cost_k',0,'costVariat',0,'yea',2015);
                        end
                    otherwise
                        sdgfergre
                end
            case 'BBedgNod'
                sdgbrf
            otherwise
                sdfsdfds
        end
        
        % Update capexPerYea
        switch f
            case 'sitCivWoAnAcq'
                Cell.capexCivWoAnAcqV = this.cost_k .* ...
                    (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea) .* ...
                    thisIsRefre;
            case 'backhau'
                Cell.capexBackhauV = this.cost_k .* ...
                    (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea) .* ...
                    thisIsRefre;
            case 'antenFeede'
                Cell.capexAntenFeedeV = this.cost_k .* ...
                    (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea) .* ...
                    thisIsRefre;
            case'BBbareMeta'
                Cell.capexBBbareMetaV = capexBBbareMetaV .* thisIsRefre;
            case 'BBedgNod'
                Cell.capexBBedgNodV = this.cost_k .* ...
                    (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea) .* ...
                    thisIsRefre;
            case 'RFfronEnd'
                Cell.capexRFfronEndV = capexRFfronEndV .* thisIsRefre;
            otherwise
                sdfsfdsd
        end
    end
end

% CAPEX - Upgrade labour
if Cell.rul>0
    tmp = 'antenSitUpgLab';
    this = Sim.cost.capex.(tmp)(strcmp({Sim.cost.capex.(tmp).pwrAmpClas},...
        Cell.pwrAmpClas));
    Cell.capexLabV = this.cost_k .* ...
        (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea) .* isRefre;
else
    Cell.capexLabV = zeros(1,10);
end

Cell.capexV = Cell.capexCivWoAnAcqV + Cell.capexBackhauV + ...
    Cell.capexAntenFeedeV + Cell.capexBBbareMetaV + ...
    Cell.capexBBedgNodV + Cell.capexRFfronEndV + Cell.capexLabV;
Cell.capexPv = sum(Cell.capexV./(1+Sim.cost.disc).^(0:10-1));
end

%%
function Cell = c_opex(Cell,Sim,capexRFfronEndV,capexBBbareMetaV)
% calc_OPEX_cost is a helper function to calculate the OPEX cost of the cell. cur is the
% base station type and cost is the structure with the OPEX cost values. OPEXincr shows the price
% increase year on year for the different OPEX elements while the costEros is the price decrease of
% the optical fibre costs. Active costs are the costs for the RF front end and base band
this = Sim.cost.opex.antenSit(strcmp({Sim.cost.opex.antenSit.pwrAmpClas},...
    Cell.pwrAmpClas)&[Sim.cost.opex.antenSit.sitChaiConfig] == ...
    Cell.sitChaiConfig);

% Write output
thisBackhau = Sim.cost.opex.backhau(strcmp(Cell.connTowaCloTyp,...
    {Sim.cost.opex.backhau.typ}) & ...
    Cell.connTowaCloBW==[Sim.cost.opex.backhau.BW_Gbps]);
Cell.opexRenV = this.sitRen .* ...
    (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
Cell.opexRaAnUtV = this.raAnUt .* ...
    (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
Cell.opexVeSeV = this.veSe .* ...
    (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
Cell.opexLicensiAnMaV = str2num(this.licensiAnMa(3:4)) ./ 100 .* ...
    capexRFfronEndV + str2num(this.licensiAnMa(11:12))./100 .* ...
    capexBBbareMetaV;
Cell.opexBackhauV = thisBackhau.cost_k .* ...
    (1+thisBackhau.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
Cell.opexV = Cell.opexRenV + Cell.opexRaAnUtV + Cell.opexVeSeV + ...
    Cell.opexLicensiAnMaV + Cell.opexBackhauV;
Cell.opexPv = sum(Cell.opexV./(1+Sim.cost.disc).^(0:10-1));
end

%%
function c_stats(Cell,studyA)
pwrAmpClas = {Cell.pwrAmpClas};
Cell_INfoc = [Cell.INfoc];

IND = find(Cell_INfoc & ...
    strcmp('Deutsche_Telekom',{Cell.Operator}) & ...
    strcmp('macrocell',pwrAmpClas));
isHam = ~inpolygon([Cell(IND).X],[Cell(IND).Y],studyA.HPA.X,studyA.HPA.Y);
isHPA = ~isHam;

A = [studyA.Ham.SHAPE_Area./nnz(isHam)
    studyA.HPA.SHAPE_Area./nnz(isHPA)] ./ 1e6;
1./A
ISD = sqrt(A.*2.*sqrt(3));
end

%%
function Cell = rese(Cell,Sim)
[Cell.rul] = deal(0);
[Cell.upg] = deal({});
[Cell.capexCivWoAnAcqV] = deal(zeros(1,10));
[Cell.capexBackhauV] = deal(zeros(1,10));
[Cell.capexAntenFeedeV] = deal(zeros(1,10));
[Cell.capexBBbareMetaV] = deal(zeros(1,10));
[Cell.capexBBedgNodV] = deal(zeros(1,10));
[Cell.capexRFfronEndV] = deal(zeros(1,10));
[Cell.capexLabV] = deal(zeros(1,10));
[Cell.capexV] = deal(zeros(1,10));
[Cell.capexPv] = deal(0);
[Cell.opexRenV] = deal(zeros(1,10));
[Cell.opexRaAnUtV] = deal(zeros(1,10));
[Cell.opexVeSeV] = deal(zeros(1,10));
[Cell.opexLicensiAnMaV] = deal(zeros(1,10));
[Cell.opexBackhauV] = deal(zeros(1,10));
[Cell.opexV] = deal(zeros(1,10));
[Cell.opexPv] = deal(0);
% [Cell.demServ] = deal([]);

% Initialise the utilisation metrics
for n = 1 : length(Cell)
%     Cell(n).band.utilisActu(:) = 0;
    Cell(n).band.utilisBefRese = Cell(n).band.utilis;
    Cell(n).band.utilis(:) = 0;
end

% []
% for n = 1 : length(Cell)
%     if Sim.yea.thisYea < 2020
%         Cell(n).FBMC = false(length(Sim.linBud.freq),1);
%     elseif Sim.yea.thisYea < 2027
%         Cell(n).FBMC = [true false false false].';
%     else
%         Cell(n).FBMC = [true true false false].';
%     end
% end
end

%% Plot map of locations for BSexi
function p_map(Cell,studyA,Sim)
% Plot map
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 1])

plot(studyA.X,studyA.Y,'k','linewidth',1)
[F,V] = poly2fv(studyA.X,studyA.Y);
hPatch1 = patch('Faces',F,'Vertices',V,...
    'facecolor','b','facealpha',0.33,...
    'edgecolor','none');

% plot(studyA.HPA.X,studyA.HPA.Y,'k','linewidth',1)
% [F,V] = poly2fv(studyA.HPA.X,studyA.HPA.Y);
% hPatch2 = patch('Faces',F,'Vertices',V,...
%     'facecolor','g','facealpha',0.33,...
%     'edgecolor','none');

% plot(studyA.cruShipTer(2).X,studyA.cruShipTer(2).Y,'k','linewidth',1)
% [F,V] = poly2fv(studyA.cruShipTer(2).X,studyA.cruShipTer(2).Y);
% hPatch2 = patch('Faces',F,'Vertices',V,...
%     'facecolor','y','facealpha',0.33,...
%     'edgecolor','none');

pwrAmpClas = {Cell.pwrAmpClas};
pwrAmpClas_ = unique(pwrAmpClas);
for n = 1 : length(pwrAmpClas_)  % per power amplifier class, e.g. macrocell, small cell, etc.
    for on = 1 : length(Sim.dem.operators)  % per InP (MNO)
        IND = find(strcmp(pwrAmpClas_{n},pwrAmpClas) & ...
            strcmp({Cell.Operator},Sim.dem.operators{on}));
        
        if ~isempty(IND)
            if isempty(Cell(IND(1)).EIRP_ovAllAnt_10MHz_100pcLoad_dBm)
                radiu2 = max(Cell(IND(1)).PTx_dBm,[],1);
            else
                radiu2 = max(Cell(IND(1)).EIRP_ovAllAnt_10MHz_100pcLoad_dBm,[],1);
            end
            
            radiu = 0.01629 .* min([diff(xlim),diff(ylim)]) .* ...
                radiu2 ./ 63.5;
            
            switch pwrAmpClas_{n}
                case 'macrocell'
                    X = bsxfun(@plus,[Cell(IND).X],...
                        bsxfun(@times,radiu,cosd([90,90-0+(-69./2:69./2),90,...
                        90-120+(-69./2:69./2),90,...
                        90-240+(-69./2:69./2),90]).'));
                    Y = bsxfun(@plus,[Cell(IND).Y],...
                        bsxfun(@times,radiu,sind([0,90-0+(-69./2:69./2),0,...
                        90-120+(-69./2:69./2),0,...
                        90-240+(-69./2:69./2),0]).'));
                case 'smaCel5W'
                    X = bsxfun(@plus,[Cell(IND).X],...
                        bsxfun(@times,radiu,cosd([90,90-0+(-69./2:69./2),90,...
                        90-180+(-69./2:69./2),90]).'));
                    Y = bsxfun(@plus,[Cell(IND).Y],...
                        bsxfun(@times,radiu,sind([0,90-0+(-69./2:69./2),0,...
                        90-180+(-69./2:69./2),0]).'));
%                 case 1
%                     X = bsxfun(@plus,[Cell(IND).X],Marker(MarkerI).radiu .* ...
%                         cosd([90,90-0+(-360./2:360./2),90]).');
%                     Y = bsxfun(@plus,[Cell(IND).Y],Marker(MarkerI).radiu .* ...
%                         sind([0,90-0+(-360./2:360./2),0]).');
                otherwise
                    error('Undefined')
            end
            RGB = Cell(IND(1)).RGB;
            patch(X,Y,ones(size(X)),'facecolor',RGB./255,...
                'edgecolor','none','facealpha',0.5,'tag','del')
            plot([Cell(IND).X],[Cell(IND).Y],'.','color',RGB./255,...
                'tag','del')
        end
    end
end

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)

% Map decorations
add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

% print(fh1,fullfile('OutpM','BSexi'),'-djpeg','-r300')
% savefig(fullfile('OutpM','BSexi'))
warning off all
export_fig(fullfile('OutpM','BSexi.jpg'),'-jpg','-q100','-r300')
warning on all

delete(fh1)
end

%% Plot path loss vs distance of one cell
function p_path_loss_vs_dist(Cell,studyA)
% [dem(:,3:4),Lbc,sigmaLe,distm,(gammaR+atmDBkm).*dist3Dm./1e3]
I = Cell.Lbc{2,1}(:,1);
J = Cell.Lbc{2,1}(:,2);
Lbc = Cell.Lbc{2,1}(:,3);
% sigmaLe = Cell.Lbc{2,1}(:,4);
distm = Cell.Lbc{2,1}(:,5);
if size(Cell.Lbc{2,1},2) == 6
    beep
    disp('Include (gammaR+atmDBkm).*dist3Dm./1e3')
    keyboard
end

[I1,J1] = ind2sub(studyA.R.RasterSize,126432);

figure
hold on
grid on
plot(distm,Lbc,'.')
plot(distm(I==I1&J==J1),Lbc(I==I1&J==J1),'o')
xlabel('2D separation distance  (m)')
ylabel('Median Extended Hata path loss  (dB)')
end

%% Plot path loss map
function p_map_path_loss(Cell,studyA,Sim,thisDem,clu)

% [dem(:,3:4),Lbc,sigmaLe,distm,(gammaR+atmDBkm).*dist3Dm./1e3]
I = Cell.Lbc{2,1}(:,1);
J = Cell.Lbc{2,1}(:,2);
Lbc = Cell.Lbc{2,1}(:,3);
% sigmaLe = Cell.Lbc{2,1}(:,4);
% distm = Cell.Lbc{2,1}(:,5);
if size(Cell.Lbc{2,1},2) == 6
    beep
    disp('Include (gammaR+atmDBkm).*dist3Dm./1e3')
    keyboard
end

% [row_clu,col_clu] = worldToSub(clu.R,Cell.X,Cell.Y);
% col_idx_clut_h_extreme = 7;  % 'for i=1 and n in (1c)'
% Cell.heiAgl >= clu.tran.pop{clu.A(row_clu,col_clu)+1,...
%     col_idx_clut_h_extreme}
% % Above rooftop, <6GHz, use Extended Hata model
% spotFreqMhz = Sim.linBud.spotFreq(2);
% hm = 1.5;
% clu.tran.pop(clu.A(row_clu,col_clu)+1,:)
% give_LdBnoAngleNoMCL(spotFreqMhz,distm(I==I1&J==J1)./1e3,Cell.heiAgl,hm,...
%     4,'RW_Ofcom')
% % [X,Y,I,J,clu_Hata]
% % Sim.propag.UE(1,:)
% Sim.propag.pix{1}(Sim.propag.pix{1}(:,3)==I1&Sim.propag.pix{1}(:,4)==J1,:)
% [row_clu,col_clu] = worldToSub(clu.R,565275,5933375);
% clu.tran.pop(clu.A(row_clu,col_clu)+1,:)

% demPlot = thisDem(studyA.INfoc([thisDem.IND]));
[X,Y] = studyA.R.intrinsicToWorld(J,I);
% X = num2cell(X);
% Y = num2cell(Y);
% [demPlot(:).X] = deal(X{:});
% [demPlot(:).Y] = deal(Y{:});

% Plot map
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 0.9])

plot(studyA.X,studyA.Y,'k','linewidth',1)

Cmap = {-inf 80 'x<80' 0 255 0
    80 90 '80\lex<90' 0 255 255
    90 100 '90\lex<100' 255 255 0
    100 110 '100\lex<110' 0 0 255
    110 120 '110\lex<120' 255 165 0
    120 130 '120\lex<130' 255 192 203
    130 140 '130\lex<140' 255 0 0
    140 inf 'x\ge140' 165 42 42};
for n = 1 : size(Cmap,1)
    IND = Cmap{n,1}<Lbc & Lbc<=Cmap{n,2};
    if any(IND)
        patch(bsxfun(@plus,X(IND).',...
            studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
            bsxfun(@plus,Y(IND).',...
            studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),...
            ones(5,nnz(IND)).*n,'edgecolor','none',...
            'facecolor',cell2mat(Cmap(n,4:6))./255)
    end
end

plot(Cell.X,Cell.Y,'r.')
plot(Cell.X,Cell.Y,'ro')

% txt = {['Service map, ' Sim.demTranslati{n,5} ', ' ...
%     Sim.demTranslati{n,3} ', '];
%     [Sim.demTranslati{n,4} ', ' Sim.demTranslati{n,6} ', ' ...
%     num2str(Sim.yea.thisYea) ', ' Sim.dem.hourDistri{hn,1} ' ' ...
%     Sim.dem.hourDistri{hn,2}]};
% for k = 1 : length(txt)
%     txt{k}(strfind(txt{k},'_')) = ' ';
% end
% title(txt)

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)

% Map decorations
add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

% for k = 1 : length(txt)
%     txt{k}(strfind(txt{k},',')) = '';
%     txt{k}(strfind(txt{k},'/')) = '_';
% end

% warning off all
% export_fig(fullfile('OutpM',[[txt{:}] '.jpg']),'-djpeg','-r600')
% warning on all
% delete(gcf)
end

%% Plot SINR map
function p_map_SINR(Cell,studyA,Sim,thisDem)
% Plot map
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 0.9])

plot(studyA.X,studyA.Y,'k','linewidth',1)

INfoc = studyA.INfoc([thisDem.IND]);

BHcou = size(Sim.dem.hourDistri,1);
IND2n = size(Sim.demTranslati,1);
SINR_DL = reshape([thisDem(INfoc).SINR_DL],[BHcou,IND2n,nnz(INfoc)]);

demPlot = thisDem(INfoc);
[X,Y] = studyA.R.intrinsicToWorld([demPlot.J],[demPlot.I]);

% cellfun(@(x) pctile(SINR_DL(~isnan(SINR_DL)),x,'nearest'),...
%     num2cell((1:7)./7))
% figure;plot(ans)

% SINR_DL = [demPlot.SINR_DL];
% SINR_DL = reshape(SINR_DL(:),[BHcou,size(Sim.demTranslati,1),...
%     length(demPlot)]);
% figure
% hold on
% grid on
% xlabel('SINR  (dB)')
% ylabel('PDF')
% title('10 MHz at 2100 MHz')
% ksdensity(squeeze(SINR_DL(1,1,~isnan(SINR_DL(1,1,:)))))
% ksdensity(squeeze(SINR_DL(2,1,~isnan(SINR_DL(2,1,:)))))
% ksdensity(squeeze(SINR_DL(3,1,~isnan(SINR_DL(3,1,:)))))
% ksdensity(squeeze(SINR_DL(4,1,~isnan(SINR_DL(4,1,:)))))
% ksdensity(squeeze(SINR_DL(5,1,~isnan(SINR_DL(5,1,:)))))
% ksdensity(squeeze(SINR_DL(6,1,~isnan(SINR_DL(6,1,:)))))

Cmap = {-inf 10 'x<10' 165 42 42
    10 20 '10\lex<20' 255 0 0
    20 25 '20\lex<25' 255 192 203
    25 30 '25\lex<30' 255 165 0
    30 35 '30\lex<35' 0 0 255
    35 40 '35\lex<40' 255 255 0
    40 60 '40\lex<60' 0 255 255
    60 inf 'x\ge60' 0 255 0};
for n = 1 : size(Cmap,1)
    IND = Cmap{n,1}<SINR_DL(1,1,:) & SINR_DL(1,1,:)<=Cmap{n,2};
    if any(IND)
        patch(bsxfun(@plus,X(IND),...
            studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
            bsxfun(@plus,Y(IND),...
            studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),...
            ones(5,nnz(IND)).*n,'edgecolor','none',...
            'facecolor',cell2mat(Cmap(n,4:6))./255)
    end
end

plot([Cell.X],[Cell.Y],'k.')
plot([Cell.X],[Cell.Y],'ko')

% txt = {['Service map, ' Sim.demTranslati{n,5} ', ' ...
%     Sim.demTranslati{n,3} ', '];
%     [Sim.demTranslati{n,4} ', ' Sim.demTranslati{n,6} ', ' ...
%     num2str(Sim.yea.thisYea) ', ' Sim.dem.hourDistri{hn,1} ' ' ...
%     Sim.dem.hourDistri{hn,2}]};
% for k = 1 : length(txt)
%     txt{k}(strfind(txt{k},'_')) = ' ';
% end
% title(txt)

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)

% Map decorations
add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);
end

%% Plot service map
function p_map_serv(Cell,studyA,Sim,thisDem,isPlotBuf,cellI)
if isPlotBuf
    demPlot = thisDem;
else
    demPlot = thisDem(studyA.INfoc([thisDem.IND]));
end
[X,Y] = studyA.R.intrinsicToWorld([demPlot.J],[demPlot.I]);
X = num2cell(X);
Y = num2cell(Y);
[demPlot(:).X] = deal(X{:});
[demPlot(:).Y] = deal(Y{:});

for n = 1 : size(Sim.demTranslati,1)
    % Plot map
    ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
    fh1 = figure('paperpositionmode','auto','unit','in',...
        'pos',[1 1 7+1./16 (7+1./16).*ratio]);
    hold on
    axis equal off
    set(gca,'pos',[0 0 1 0.9])

    plot(studyA.X,studyA.Y,'k','linewidth',1)
    
    hn = 1;
    
    if isPlotBuf
        v = arrayfun(@(x) full(x.v(hn,n)),thisDem);
    else
        v = arrayfun(@(x) full(x.v(hn,n)),...
            thisDem(studyA.INfoc([thisDem.IND])));
    end
    assoc = cell2mat(arrayfun(@(x) cell2mat(x.assoc(:,n)),demPlot.',...
        'UniformOutput',false));
    assoc(~any(bsxfun(@eq,assoc(:),cellI),2)) = 0;
%     assoc = cell2mat(arrayfun(@(x) cell2mat(x.assoc(:,n)),demPlot(v>0).',...
%         'UniformOutput',false));

    [C,~,ic] = unique(assoc(hn,:));
    if nnz(C>1) <= 64
        Map = colorcube();
        Map = Map(1:nnz(C>1),:);
    else
        Map = colorcube(length(C)-1);
    end
    for mn = 2 : length(C)
        IND = ic == mn;
        if any(IND&v>0)
            patch(bsxfun(@plus,[demPlot(IND&v>0).X],...
                studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
                bsxfun(@plus,[demPlot(IND&v>0).Y],...
                studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),...
                ones(5,nnz(IND&v>0)).*mn,...
                'edgecolor','none',...
                'facecolor',Map(mn-1,:))

            if or(isPlotBuf,Cell(C(mn)).INfoc)
                plot(Cell(C(mn)).X,Cell(C(mn)).Y,'.',...
                    'markerfacecolor','none','markeredgecolor',Map(mn-1,:))
                plot(Cell(C(mn)).X,Cell(C(mn)).Y,'o',...
                    'markerfacecolor','none','markeredgecolor',Map(mn-1,:))

%                 % Utilis: (size(Sim.dem.hourDistri,1)
%                 % length(Sim.dem.operators),...
%                 % length(BW),length(Sim.dem.famUseCase_))
%                 IND2 = squeeze(Cell(C(mn)).band.utilis(hn,2,:,2));
%                 if sum(IND2([3:4]))==0
%                     beep
%                     disp('Remove 0.65')
%                     keyboard
%                     str = {[' ' num2str(round(IND2(1).*100)) ...
%                         '% of sub-1GHz used for ' Sim.demTranslati{n,5}]
%                         [' ' num2str(round((0.65-sum(Cell(C(mn)...
%                         ).band.utilis(hn,1,1,:))).*100)) '% of low bands free']
%                         [' ' num2str(round(IND2(2).*100)) ...
%                         '% of low bands used for ' Sim.demTranslati{n,5}]
%                         [' ' num2str(round((0.65-sum(Cell(C(mn)...
%                         ).band.utilis(hn,1,2,:))).*100)) '% of low bands free']};
%                 else
%                     sfdgdf
%                 end
%                 text(Cell(C(mn)).X,Cell(C(mn)).Y,str)
            end
        end
    end

%     Xlim = [Cell(C(C>0)).X];
%     Ylim = [Cell(C(C>0)).Y];
%     Xlim = [min(Xlim) max(Xlim)];
%     Ylim = [min(Ylim) max(Ylim)];
%     set(gca,'xlim',Xlim+(Xlim(2)-Xlim(1))./20.*[-1 1],...
%         'ylim',Ylim+(Ylim(2)-Ylim(1))./20.*[-1 1])

    txt = {['Service map, ' Sim.demTranslati{n,5} ', ' ...
        Sim.demTranslati{n,3} ', '];
        [Sim.demTranslati{n,4} ', ' Sim.demTranslati{n,6} ', ' ...
        num2str(Sim.yea.thisYea) ', ' Sim.dem.hourDistri{hn,1} ' ' ...
        Sim.dem.hourDistri{hn,2}]};
    for k = 1 : length(txt)
        txt{k}(strfind(txt{k},'_')) = ' ';
    end
    title(txt)
    
    % Add background map
    plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)

    % Map decorations
    add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

    for k = 1 : length(txt)
        txt{k}(strfind(txt{k},',')) = '';
        txt{k}(strfind(txt{k},'/')) = '_';
    end
    
    warning off all
    export_fig(fullfile('OutpM',[[txt{:}] '.jpg']),'-djpeg','-r600')
    warning on all
    delete(gcf)
end
end

%% Plot service map on Google Earth
function p_map_serv_GE(Cell,studyA,Sim,thisDem,isPlotBuf,cellI)
if isPlotBuf
    demPlot = thisDem;
else
    demPlot = thisDem(studyA.INfoc([thisDem.IND]));
end
[X,Y] = studyA.R.intrinsicToWorld([demPlot.J],[demPlot.I]);
X = num2cell(X);
Y = num2cell(Y);
[demPlot(:).X] = deal(X{:});
[demPlot(:).Y] = deal(Y{:});

BHcou = size(Sim.dem.hourDistri,1);

k = kml('studyA');
if isfield(Sim.mstruct,'ostn')
    [lat,lon] = easNor2latLon(studyA.X,studyA.Y,Sim.mstruct.ostn);
elseif isfield(Sim.mstruct,'mapprojection')
    [lat,lon] = minvtran(Sim.mstruct,studyA.X,studyA.Y);
else
    error('Code this!')
end
[latcells,loncells] = polysplit(lat,lon);
for m = 1 : length(latcells)
    k.plot(loncells{m},latcells{m},...
        'altitudeMode','clampToGround','lineWidth',2,...
        'lineColor','FF000000',...
        'name','studyA');
end
k.save('OutpM\studyA');

for sn = 1 : length(cellI)
    k = kml(['Site ' num2str(sn)]);
    k.plot3(Cell(cellI(sn)).lon.*ones(2,1),...
        Cell(cellI(sn)).lat.*ones(2,1),[0 Cell(cellI(sn)).heiAgl],...
        'altitudeMode','relativeToGround','lineWidth',5,...
        'lineColor','FF00FF00',...
        'name',['Cell ' num2str(cellI(sn))]);
    k.point(Cell(cellI(sn)).lon,Cell(cellI(sn)).lat,...
        Cell(cellI(sn)).heiAgl,'iconURL','wht-stars','iconScale',1,...
        'name',['Cell ' num2str(cellI(sn))]);
    k.save(['OutpM\Site ' num2str(sn)]);
end

dX = studyA.R.CellExtentInWorldX;
dY = studyA.R.CellExtentInWorldY;
for n = 1 : size(Sim.demTranslati,1)
    for hn = 1 : BHcou
%             v = arrayfun(@(x) full(x.v(hn,n)),demPlot);
        assoc = cell2mat(arrayfun(@(x) full(x.assoc(hn,n)),demPlot));

        txt = ['Service map_' Sim.demTranslati{n,5} '_' ...
            Sim.demTranslati{n,3} '_' Sim.demTranslati{n,4} '_' ...
            Sim.demTranslati{n,6} '_' num2str(Sim.yea.thisYea) ...
            '_' Sim.dem.hourDistri{hn,1} ' ' ...
            Sim.dem.hourDistri{hn,2}];

        c = colorcube(length(cellI));

        for sn = 1 : length(cellI)
            k = kml(['Site ' num2str(sn)]);

            IND = assoc == cellI(sn);
            if any(IND)
                X = num2cell(bsxfun(@plus,[demPlot(IND).X],...
                    [-1 -1 1 1 -1].'.*dX./2),1);
                Y = num2cell(bsxfun(@plus,[demPlot(IND).Y],...
                    [-1 1 1 -1 -1].'.*dY./2),1);
                [x,y] = polyjoin(X,Y);
                if any(isnan(x))
                    [x2,y2] = polyunion(x,y,'non overlapping');
                else
                    x2 = x;
                    y2 = y;
                end
                [F,V] = poly2fv(x2,y2);

                if isfield(Sim.mstruct,'ostn')
                    [lat,lon] = easNor2latLon(V(:,1),V(:,2),Sim.mstruct.ostn);
                elseif isfield(Sim.mstruct,'mapprojection')
                    [lat,lon] = minvtran(Sim.mstruct,V(:,1),V(:,2));
                else
                    error('Code this!')
                end

                colorHex = ['FF' reshape(dec2hex(round(fliplr(c(sn,:)) .* ...
                    255),2).',[],1).'];
                for m = 1 : size(F,1)
                    k.poly(lon(F(m,[1:end,1])),lat(F(m,[1:end,1])),...
                        'altitudeMode','clampToGround', ...
                        'name',['Cell ' num2str(cellI(sn))],...
                        'lineWidth',0,...
                        'polyColor',colorHex);
                end
            end

            k.save(['OutpM\' txt ' Site ' num2str(sn)]);
        end

%             IND = assoc==0 & v>0;
%             if any(IND)
%                 X = num2cell(bsxfun(@plus,[demPlot(IND).X],...
%                     [-1 -1 1 1 -1].'.*dX./2),1);
%                 Y = num2cell(bsxfun(@plus,[demPlot(IND).Y],...
%                     [-1 1 1 -1 -1].'.*dY./2),1);
%                 [x,y] = polyjoin(X,Y);
%                 if any(isnan(x))
%                     [x2,y2] = polyunion(x,y,'non overlapping');
%                 else
%                     x2 = x;
%                     y2 = y;
%                 end
%                 [F,V] = poly2fv(x2,y2);
%                 
%                 [lat,lon] = minvtran(Sim.mstruct,V(:,1),V(:,2));
%                 
%                 colorHex = ['FF' reshape(dec2hex(round(fliplr(c(end,:)) .* ...
%                     255),2).',[],1).'];
%                 for m = 1 : size(F,1)
%                     k.poly(lon(F(m,[1:end,1])),lat(F(m,[1:end,1])),...
%                         'altitudeMode','clampToGround', ...
%                         'name','No service',...
%                         'lineWidth',0,...
%                         'polyColor',colorHex);
%                 end
%             end
    end
end
end

%% Calculate range
function c_ra(Cell,studyA,Sim,thisDem,isPlotBuf,cellI)
if isPlotBuf
    demPlot = thisDem;
else
    demPlot = thisDem(studyA.INfoc([thisDem.IND]));
end
[X,Y] = studyA.R.intrinsicToWorld([demPlot.J],[demPlot.I]);
X = num2cell(X);
Y = num2cell(Y);
[demPlot(:).X] = deal(X{:});
[demPlot(:).Y] = deal(Y{:});

BHcou = size(Sim.dem.hourDistri,1);

for n = 1 : size(Sim.demTranslati,1)
    txt = [Sim.demTranslati{n,5} ', ' Sim.demTranslati{n,3} ', ' ...
        Sim.demTranslati{n,4} ', ' Sim.demTranslati{n,6} ', ' ...
        num2str(Sim.yea.thisYea)];
    txt(strfind(txt,'_')) = ' ';
    for hn = 1 : BHcou
%             v = arrayfun(@(x) full(x.v(hn,n)),demPlot);
        assoc = cell2mat(arrayfun(@(x) full(x.assoc(hn,n)),demPlot));

        maxRa = -500 .* ones(length(cellI),1);
        for sn = 1 : length(cellI)
            IND = assoc == cellI(sn);
            if any(IND)
                maxRa(sn) = max(sqrt(([demPlot(IND).X] - ...
                    Cell(cellI(sn)).X).^2 + ...
                    ([demPlot(IND).Y]-Cell(cellI(sn)).Y).^2));
            end
        end

        figure
%         subplot(2,3,hn)
        bar(maxRa)
        xlabel('Site index')
        ylabel({'Maximum (100th pc-tile) 2D distance'
            'from BS to served UE  (m)'})
        grid on
        set(gca,'ylim',[-500 12000])

        title([Sim.dem.hourDistri{hn,1} ' ' ...
            Sim.dem.hourDistri{hn,2}])
    end
end
end

%% Plots of served demand per site
function p_serv_dem_per_sit(Cell,studyA,thisDem,Sim,filepart2)
x = cellfun(@(x) c_unserv(Sim,thisDem,studyA,'serv','per site',Cell,x),...
    num2cell(find([Cell.INfoc])));

txt = filepart2;

figure
bar(x)
xlabel('Site index')
ylabel('Offered demand  (MB/6hr)')
grid on
print(gcf,fullfile('Outp',[txt ' serv_dem_per_sit bar']),'-djpeg','-r300')
delete(gcf)

figure
boxplot(x)
ylabel('Offered demand  (MB/6hr)')
grid on
set(gca,'ylim',[0 inf])
print(gcf,fullfile('Outp',[txt ' serv_dem_per_sit box']),'-djpeg','-r300')
delete(gcf)
end

%% Plots of served demand per site (Python)
function p_serv_dem_per_sit_Python(Cell,studyA,thisDem,Sim,filepart2)
x = cellfun(@(x) c_unserv(Sim,thisDem,studyA,'serv','per site',Cell,x),...
    num2cell(find([Cell.INfoc])));

txt = filepart2;

figure
bar(x)
xlabel('Site index')
ylabel('Offered demand  (MB/6hr)')
grid on
print(gcf,fullfile('Outp',[txt ' serv_dem_per_sit bar']),'-djpeg','-r300')
delete(gcf)

% Plot this with Python boxplot function
% KK edit start
% KK: The import and reload is not necessary
% % clear classes, % clear all, % close all % We can't do this here as we will delete all variables (this is needed if we are changing the Python Code)
% % Import the module
% mod = py.importlib.import_module('box_plot_mod'); % Name of the Python module
% % Reload module in Python versions 3.x (reload module in vers 2.7: py.reload(mod)
% py.importlib.reload(mod);
% KK edit end

% Convert x into numpy array
x1=x.';
x_nump_arr = py.numpy.reshape(x1(:).', int32(size(x1)));

% Call the box_plot function in Python
tit_graph = 'Demand MB/6hr';
tit_file = fullfile('Outp',[txt ' serv_dem_per_sit box.jpg']);
tit_yaxis = 'Offered demand  (MB/6hr)';
g={' '};
py.box_plot_mod.box_plot_py(x_nump_arr, tit_graph, tit_file, tit_yaxis, g.');

% figure
% boxplot(x)
% ylabel('Offered demand  (MB/6hr)')
% grid on
% set(gca,'ylim',[0 inf])
% print(gcf,fullfile('Outp',[txt ' serv_dem_per_sit box']),'-djpeg','-r300')
% delete(gcf)
end

%% Plot boxplot of PRB utilisation
function p_box_utilis(Cell,interfIequi,Sim,filepart2)
BHcou = size(Sim.dem.hourDistri,1);  % Number of busy hours
CellIsExpi = Sim.yea.thisYea >= [Cell.expiYea];
Operator = {Cell.Operator};
pwrAmpClas = {Cell.pwrAmpClas};
Cell_INfoc = [Cell.INfoc];

pwrAmpClas_ = unique(pwrAmpClas);
g = strcat(Sim.dem.hourDistri(:,1),{' '},Sim.dem.hourDistri(:,2));
for n = 1 : length(pwrAmpClas_)
    for fn = 1 : length(Sim.linBud.spotFreq)
        x = interfIequi(:,[Cell.INfoc]&strcmp(pwrAmpClas,pwrAmpClas_{n}) & ...
            arrayfun(@(x) x.band.supp(fn),Cell),fn);
        if ~isempty(x)
            txt = [filepart2 ' ' pwrAmpClas_{n} ' ' Sim.linBud.freq{fn}];
            
            figure
            boxplot(x.'./10-0.05,g)
            title([pwrAmpClas_{n} ', ' Sim.linBud.freq{fn} ...
                ' MHz band, 5Mbit/s'])
            ylabel('PRB utilisation for coverage')
            set(gca,'ylim',[0 1])
            xtickangle(45)

            print(gcf,fullfile('Outp',[txt ' utilis cov']),'-djpeg','-r300')
            
            delete(gcf)
        end
    end
end

n1 = 1;
for n2 = 1 : length(pwrAmpClas_)
    for fn = 1 : length(Sim.linBud.spotFreq)
        IND = strcmp(Sim.propag.BS{n2,1},pwrAmpClas) & ~CellIsExpi & ...
            strcmp(Operator,Sim.dem.operators{n1}) & Cell_INfoc & ...
            arrayfun(@(x) x.band.supp(fn),Cell);
        % Utilis: (size(Sim.dem.hourDistri,1)
        % length(Sim.dem.operators),...
        % length(BW),length(Sim.dem.famUseCase_))
        utilis = reshape(cell2mat(arrayfun(@(x) squeeze(sum(sum(...
            x.band.utilis,4),2)),Cell(IND),...
            'UniformOutput',false)),...
            [BHcou,length(Sim.linBud.spotFreq),nnz(IND)]);
        
        x = squeeze(utilis(:,fn,:));
        if ~all(x(:)==0)
            txt = [filepart2 ' ' pwrAmpClas_{n2} ' ' Sim.linBud.freq{fn}];
            
            figure
            boxplot(x.',g)
            title([pwrAmpClas_{n2} ', ' Sim.linBud.freq{fn} ...
                ' MHz band, 5Mbit/s'])
            ylabel('PRB utilisation for capacity')
            set(gca,'ylim',[0 1])
            xtickangle(45)

            print(gcf,fullfile('Outp',[txt ' utilis capa']),'-djpeg','-r300')
            
            delete(gcf)
        end
    end
end
end

%% Plot this with Python boxplot function
function p_box_utilis_Python(Cell,interfIequi,Sim,filepart2)
BHcou = size(Sim.dem.hourDistri,1);  % Number of busy hours
CellIsExpi = Sim.yea.thisYea >= [Cell.expiYea];
Operator = {Cell.Operator};
pwrAmpClas = {Cell.pwrAmpClas};
Cell_INfoc = [Cell.INfoc];

pwrAmpClas_ = unique(pwrAmpClas);
g = strcat(Sim.dem.hourDistri(:,1),{' '},Sim.dem.hourDistri(:,2));
g= [{' '}; g]; % Adding empty one to deal with issue in the x-axis tick boxplot in Python
for n = 1 : length(pwrAmpClas_)
    for fn = 1 : length(Sim.linBud.spotFreq)
        x = interfIequi(:,[Cell.INfoc]&strcmp(pwrAmpClas,pwrAmpClas_{n}) & ...
            arrayfun(@(x) x.band.supp(fn),Cell),fn);
        if ~isempty(x)
            txt = [filepart2 ' ' pwrAmpClas_{n} ' ' Sim.linBud.freq{fn}];
            
            %figure
            %boxplot(x.'./10-0.05,g)
            
            % KK edit start
            % KK: The import and reload is not necessary
%             % clear classes, % clear all, % close all % We can't do this here as we will delete all variables (this is needed if we are changing the Python Code)
%             % Import the module
%             mod = py.importlib.import_module('box_plot_mod'); % Name of the Python module
%             % Reload module in Python versions 3.x (reload module in vers 2.7: py.reload(mod)
%             py.importlib.reload(mod);
            % KK edit end
            
            % Convert x into numpy array
            x1=x.'./10-0.05;
            x_nump_arr = py.numpy.reshape(x1(:).', int32(size(x1)));
            
            % Call the box_plot function in Python
            tit_graph = [pwrAmpClas_{n} ', ' Sim.linBud.freq{fn} ...
                ' MHz band, 5Mbit/s'];
            tit_file = fullfile('Outp',[txt ' utilis cov.jpg']);
            tit_yaxis = 'PRB utilisation for coverage';
            py.box_plot_mod.box_plot_py(x_nump_arr, tit_graph, tit_file, tit_yaxis, g.');

%             title([pwrAmpClas_{n} ', ' num2str(Sim.linBud.freq(fn)) ...
%                 ' MHz band, 5Mbit/s'])
%             ylabel('PRB utilisation for coverage')
%             set(gca,'ylim',[0 1])
%             xtickangle(45)
%             print(gcf,fullfile('Outp',[txt ' utilis cov']),'-djpeg','-r300')
%             delete(gcf)
        end
    end
end

n1 = 1;
for n2 = 1 : length(pwrAmpClas_)
    for fn = 1 : length(Sim.linBud.spotFreq)
        IND = strcmp(Sim.propag.BS{n2,1},pwrAmpClas) & ~CellIsExpi & ...
            strcmp(Operator,Sim.dem.operators{n1}) & Cell_INfoc & ...
            arrayfun(@(x) x.band.supp(fn),Cell);
        % Utilis: (size(Sim.dem.hourDistri,1)
        % length(Sim.dem.operators),...
        % length(BW),length(Sim.dem.famUseCase_))
        utilis = reshape(cell2mat(arrayfun(@(x) squeeze(sum(sum(...
            x.band.utilis,4),2)),Cell(IND),...
            'UniformOutput',false)),...
            [BHcou,length(Sim.linBud.spotFreq),nnz(IND)]);
        
        x = squeeze(utilis(:,fn,:));
        if ~all(x(:)==0)
            txt = [filepart2 ' ' pwrAmpClas_{n2} ' ' Sim.linBud.freq{fn}];
            
            % KK edit start
            % KK: The import and reload is not necessary
%             % clear classes, % clear all, % close all % We can't do this here as we will delete all variables (this is needed if we are changing the Python Code)
%             % Import the module
%             mod = py.importlib.import_module('box_plot_mod'); % Name of the Python module
%             % Reload module in Python versions 3.x (reload module in vers 2.7: py.reload(mod)
%             py.importlib.reload(mod);
            % KK edit end
            
            % Convert x into numpy array
            x1=x.';
            x_nump_arr = py.numpy.reshape(x1(:).', int32(size(x1)));
            
            % Call the box_plot function in Python
            tit_graph = [pwrAmpClas_{n2} ', ' Sim.linBud.freq{fn} ...
                ' MHz band, 5Mbit/s'];
            tit_file = fullfile('Outp',[txt ' utilis capa.jpg']);
            tit_yaxis = 'PRB utilisation for capacity';
            py.box_plot_mod.box_plot_py(x_nump_arr, tit_graph, tit_file, tit_yaxis, g.');
            
%             figure
%             boxplot(x.',g)
%             title([pwrAmpClas_{n2} ', ' num2str(Sim.linBud.freq(fn)) ...
%                 ' MHz band, 5Mbit/s'])
%             ylabel('PRB utilisation for capacity')
%             set(gca,'ylim',[0 1])
%             xtickangle(45)
% 
%             print(gcf,fullfile('Outp',[txt ' utilis capa']),'-djpeg','-r300')
%             
%             delete(gcf)
        end
    end
end
end

%% Plot map of radio resource utilisation
function p_map_utilis(Cell,studyA,Sim,filepart2,Xlim,Ylim)
% Plot map
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 1])

plot(studyA.X,studyA.Y,'k','linewidth',1)

RGB = jet(length(Sim.linBud.spotFreq));

if exist('Xlim','var')
    set(gca,'xlim',Xlim,'ylim',Ylim)
end

pwrAmpClas = {Cell.pwrAmpClas};
pwrAmpClas_ = unique(pwrAmpClas);
for n = 1 : length(pwrAmpClas_)
    for on = 1 : length(Sim.dem.operators)
        IND = find(strcmp(pwrAmpClas_{n},pwrAmpClas) & ...
            strcmp({Cell.Operator},Sim.dem.operators{on}));
        
        if ~isempty(IND)
            if isempty(Cell(IND(1)).EIRP_ovAllAnt_10MHz_100pcLoad_dBm)
                radiu2 = max(Cell(IND(1)).PTx_dBm,[],1);
            else
                radiu2 = max(Cell(IND(1)).EIRP_ovAllAnt_10MHz_100pcLoad_dBm,[],1);
            end
            
            radiu = 0.01629 .* min([diff(xlim),diff(ylim)]) .* ...
                radiu2 ./ 63.5;
            
            x = zeros(size(Cell(1).band.utilis,3)+1,length(IND));
            y = zeros(size(Cell(1).band.utilis,3),length(IND));
            for cn = 1 : length(IND)
                x(:,cn) = [0 cumsum(Cell(IND(cn)).band.capa)./35.*radiu.*2];
                y(:,cn) = squeeze(max(sum(sum(Cell(IND(cn)...
                    ).band.utilis,4),2),[],1)) ./ Sim.linBud.PRButilisCapa;
            end
            for fn = 1 : size(Cell(1).band.utilis,3)
                X = bsxfun(@plus,[Cell(IND).X] - (x(end,:)-x(1,:))./2,...
                    x(fn+[0,0,1,1,0],:));
                Y1 = bsxfun(@plus,[Cell(IND).Y],[0 1 1 0 0 ].'*radiu.*2);
                Y2 = bsxfun(@plus,[Cell(IND).Y],...
                    [0 1 1 0 0 ].'*(radiu.*y(fn,:)).*2);
                patch(X,Y1,ones(size(X)),'facecolor','none',...
                    'edgecolor','k','tag','del')
                patch(X,Y2,ones(size(X)),'facecolor',RGB(fn,:),...
                    'edgecolor','k','tag','del')
            end
%                     delete(findobj(gca,'tag','del'))

            plot([Cell(IND).X],[Cell(IND).Y],'.','color','k',...
                'tag','del')
        end
    end
end

if exist('Xlim','var')
    set(gca,'xlim',Xlim,'ylim',Ylim)
end

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)

% Map decorations
add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

% print(fh1,fullfile('OutpM','BSexi'),'-djpeg','-r300')
% savefig(fullfile('OutpM',[filepart2 ' utilis']))
warning off all
export_fig(fullfile('OutpM',[filepart2 ' utilis.jpg']),'-jpg','-q100',...
    '-r300')
warning on all

delete(fh1)
end

%% Plot map of interference indices at the equilibrium
function p_map_interfIequi(Cell,studyA,Sim,interfIequi,filepart2)
% Plot map
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 1])

plot(studyA.X,studyA.Y,'k','linewidth',1)

RGB = jet(length(Sim.linBud.spotFreq));

pwrAmpClas = {Cell.pwrAmpClas};
pwrAmpClas_ = unique(pwrAmpClas);
for n = 1 : length(pwrAmpClas_)
    for on = 1 : length(Sim.dem.operators)
        IND = find(strcmp(pwrAmpClas_{n},pwrAmpClas) & ...
            strcmp({Cell.Operator},Sim.dem.operators{on}));
        
        if ~isempty(IND)
            if isempty(Cell(IND(1)).EIRP_ovAllAnt_10MHz_100pcLoad_dBm)
                radiu2 = max(Cell(IND(1)).PTx_dBm,[],1);
            else
                radiu2 = max(Cell(IND(1)).EIRP_ovAllAnt_10MHz_100pcLoad_dBm,[],1);
            end
            
            radiu = 0.01629 .* min([diff(xlim),diff(ylim)]) .* ...
                radiu2 ./ 63.5;
            
            x = zeros(size(interfIequi,3)+1,length(IND));
            y = zeros(size(interfIequi,3),length(IND));
            for cn = 1 : length(IND)
                x(:,cn) = [0 cumsum(Cell(IND(cn)).band.capa)./35.*radiu.*2];
                y(:,cn) = max(interfIequi(:,IND(cn),:),[],1)./10 - 0.05;
                y(Cell(IND(cn)).band.capa==0,cn) = 0;
            end
            for fn = 1 : size(interfIequi,3)
                X = bsxfun(@plus,[Cell(IND).X] - (x(end,:)-x(1,:))./2,...
                    x(fn+[0,0,1,1,0],:));
                Y1 = bsxfun(@plus,[Cell(IND).Y],[0 1 1 0 0 ].'*radiu.*2);
                Y2 = bsxfun(@plus,[Cell(IND).Y],...
                    [0 1 1 0 0 ].'*(radiu.*y(fn,:)).*2);
                patch(X,Y1,ones(size(X)),'facecolor','none',...
                    'edgecolor','k','tag','del')
                patch(X,Y2,ones(size(X)),'facecolor',RGB(fn,:),...
                    'edgecolor','k','tag','del')
            end
%                     delete(findobj(gca,'tag','del'))

            plot([Cell(IND).X],[Cell(IND).Y],'.','color','k',...
                'tag','del')
        end
    end
end

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)

% Map decorations
add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

% print(fh1,fullfile('OutpM','BSexi'),'-djpeg','-r300')
% savefig(fullfile('OutpM',[filepart2 ' interfIequi']))
warning off all
export_fig(fullfile('OutpM',[filepart2 ' interfIequi.jpg']),'-jpg','-q100',...
    '-r300')
warning on all

delete(fh1)
end

%% []
function p_map_PL_sub1GHz(Cell,studyA)
% Plot map
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio

for k = find(arrayfun(@(x) ~isempty(x.Lbc{2,2}) & x.band.supp(2),Cell))
    fh1 = figure('paperpositionmode','auto','unit','in',...
        'pos',[1 1 7+1./16 (7+1./16).*ratio]);
    hold on
    axis equal off
    set(gca,'pos',[0 0 1 1],'clim',[90 140])

    plot(studyA.X,studyA.Y,'k','linewidth',1)
    
% strcmp('mac',Sim.propag.UE(:,1)) & ...
%     strcmp('automVeh',Sim.propag.UE(:,5))

    % [row,col,Lbc,sigmaLe,distm]
    [X,Y] = studyA.R.intrinsicToWorld(Cell(k).Lbc{2,2}(:,2),...
        Cell(k).Lbc{2,2}(:,1));
    scatter(X,Y,6,Cell(k).Lbc{2,2}(:,3),'filled','s')
    
    plot(Cell(k).X,Cell(k).Y,'r^')
    
    filepart2 = ['Cell' num2str(k)];
    
%     savefig(fullfile('OutpM',[filepart2 ' utilis']))
    warning off all
    export_fig(fullfile('OutpM',[filepart2 ' PL sub-1GHz.jpg']),...
        '-jpg','-q100','-r300')
    warning on all
    
    delete(gcf)
end
end

%%
function w_csv(Cell)
% Write the list of antenna sites in csv format
capa = zeros(length(Cell),4);
for n = 1 : size(capa,1)
    capa(n,:) = Cell(n).band.capa;
end
capa(capa==8.79) = 10;
capa(capa==18.467) = 20;
capa(capa==23.084) = 25;
AeCou = reshape([Cell.AeCou],[4 length(Cell)]).';
M = [{'Plz' 'Position laenge dezimal at' 'Position breite dezimal at' ...
    'Montageh�he �ber Grund from BNetzA  (m)' 'Power amplifier class' ...
    'Number of antennas' '' '' '' 'Capacity  (MHz)' '' '' ''}
    {'' '' '' '' '' 'Sub-1GHz' 'Low band' 'Medium band' 'High band' ...
    'Sub-1GHz' 'Low band' 'Medium band' 'High band'}
    {Cell.siteID}.' num2cell([Cell.lon]).' num2cell([Cell.lat]).' ...
    num2cell([Cell.heiAgl]).' {Cell.pwrAmpClas}.' ...
    num2cell(AeCou(:,1:4)) num2cell(capa(:,1:4))];
dlmcell(fullfile('Outp','Antenna sites in start of 2020 v0_1.csv'),M,',')
% clear INfoc lat lon AeCou capa n M
end

%%
function com(Cell,Sim)
% Compare Cell objects

% '8af3e02d25588602e362ac82bc1208b4.mat'  % end of 2019
% '86fc4f107995f57f2c61de30287933ac.mat'  % end of 2020

S = load(['D:\Dropbox (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication',...
    '\05 T6-4\06 Study item 4 - techno-economic method\Runs',...
    '\Hamburg eMBB 2\OutpAux\86fc4f107995f57f2c61de30287933ac.mat'],...
    'Cell');
Cell0 = S.Cell;
clear S


assert(length(Cell)==length(Cell0))
assert(all([Cell.I]+1i*[Cell.J]==[Cell0.I]+1i*[Cell0.J]))


IND = find([Cell0.rul]>0);
rul = [Cell0(IND).rul];
find([Cell0(IND).INfoc]&[Sim.rul(rul).isUpg]&~[Sim.rul(rul).isDecomm])

end

%%
% function Cell = c_demServ(Cell,Sim,thisDem,studyA)
% demPerSit = num2cell(c_unserv(Sim,thisDem,studyA,'serv','per site','MB/day',...
%     Cell,1:length(Cell)));
% [Cell.demServ] = deal(demPerSit{:});
% end

%% Calculate bundles of CELL objects with similar characeteristics
function bund1 = c_bundle1(Cell,fn)
% c_bundle1 calculates bundles of CELL objects with similar
% characeteristics: power amplifier class, situation (outdoor/indoor), MIMO
% order (antenna count), sector count (sectoCou), and FBMC.
% MATLAB runs faster when calculating in vector format. Cells of the same
% bundle are assessed in a single vector by c_visib (calculation of the
% wanted signal strength), and by c_sitWithiCov (assessment of coverage and
% service).
% 
% Bundling is done by use of function unique. Function unique does not
% accept structures as input, thus we need to create a unique identifier
% for each structure, hence the use of hash.
% 
% Cell is an array of CELL objects and must contain the following fields:
% pwrAmpClas, situ, AeCou, sectoCou, FBMC.
% 
% The output structure bund1 contains the following fields:
% C - A M�1 vector of structures, where M=length(Cell), of the bundling
%     parameters.
% ia - The ia index vector as returned by function unique.
% ic - The ic index vector as returned by function unique.

% Calculate a hash from the bundling parameters.
% DataHash was used in the past, but is too slow for practical purposes.
% Instead, we create a hash by summing the numeric codes of the ASCII
% character array.
bund1.C = struct('pwrAmpClas',{Cell.pwrAmpClas}.','situ',{Cell.situ}.',...
    'AeCouBS',arrayfun(@(x) x.AeCou(fn),Cell.','UniformOutput',false),...
    'sectoCou',{Cell.sectoCou}.',...
    'FBMC',arrayfun(@(x) x.FBMC(fn),Cell.','UniformOutput',false));
% Hash = arrayfun(@(x) DataHash(x),bund1.C,'UniformOutput',false);
Hash = arrayfun(@(x) sum(x.pwrAmpClas) + sum(x.situ) + ...
    sum(x.AeCouBS) + sum(x.sectoCou) + sum(x.FBMC),bund1.C);

% Calculate indices ia and ic
[~,bund1.ia,bund1.ic] = unique(Hash,'stable');
end
    end
end

%%
function BSexi_ = prep_exi_net(Sim,studyA,clu)
% Common for all site types
situ = 'outd';
FBMC = false(length(Sim.linBud.freq),1);  % SE gains from MIMO may not apply to FBMC
rul = 0;

% Hardcoded for macrocells
% antPol     = 'dual';
% antMod     = '600-2700';

% List of infrastructure owners
% operato_   = {'Vodafone'};
operato_   = Sim.dem.operators;

% Site characteristics
% sitCivWrk = 1;
% backhauEq = 1;

BSexi_ = [];
for rn = 1 : length(Sim.exiCel.rul)
    switch Sim.exiCel.rul(rn).pwrAmpClas
        case 'macrocell'
            anteTyp_ = {'Directional','MACRO','Macro','SECTOR','Sectored'};
        case 'smaCel5W'
            anteTyp_ = {'Micro','Microcell','OMNI','Omni','MICROCELL'};
        otherwise
            error('Introduce here')
    end
    
    for on = 1 : length(operato_)
        if strcmpi(Sim.BsExiFromSitefinder.(operato_{on}),'none')
            % This service provider does not have any sites
            continue
        end

        % Reset for each infrastructure owner so that in the sharing the
        % establish dates are the same
        rng(53)
        
        % Read Sitefinder
        operato = Sim.BsExiFromSitefinder.(operato_{on});
        if ~strcmpi(operato,'none')
            switch operato
              case {'Vodafone','EE'}
                BSexi = create_BSexi_from_Sitefinder(Sim,operato,...
                    studyA,anteTyp_);
              case 'Deutsche_Telekom'
                % Load the database - open source
%                     load(fullfile(Sim.path.ProgramData,...
%                         'Telekom_BSexi.mat'),'BSexi')
%                     BSexi(:) = [];
%                     beep
%                     keyboard

                % Load the database - from DT
                [~,~,raw] = xlsread(Sim.path.BSexi);
                lat = raw(2:end,8);
                lon = raw(2:end,7);
                [x,y] = mfwdtran(Sim.mstruct,...
                    cell2mat(lat),cell2mat(lon));
                BSexi = struct('X',num2cell(x),'Y',num2cell(y),...
                    'lat',lat,'lon',lon,...
                    'Antenna_height',raw(2:end,9),...
                    'StrasseNr',raw(2:end,1),...
                    'Operator_Ref',raw(2:end,5),...
                    'sitChainConfig',...
                    Sim.exiCel.rul(rn).upg.sitChainConfig,...
                    'pwrAmpClas',Sim.exiCel.rul(rn).pwrAmpClas,...
                    'situ',situ,...
                    'AeCou',Sim.exiCel.rul(rn).upg.AeCou,...
                    'sectoCou',Sim.exiCel.rul(rn).upg.sectoCou,...
                    'Band',Sim.exiCel.rul(rn).upg.Band,...
                    'Operator',operato_{on},'FBMC',FBMC,'rul',rul);
                clear raw lat lon x y

                % Remove these outside the buffer
                IN = inpolygon([BSexi.X],[BSexi.Y],...
                    studyA.buf.X,studyA.buf.Y);
                BSexi(~IN) = [];
              case {'Jawwal','Ooredoo','Thaleth','Sc_5','Sc_6',...
                    'Sc_14','Sc_15'}
                switch Sim.exiCel.rul(rn).pwrAmpClas
                  case 'macrocell'
                  case 'smaCel5W'
                    continue
                  otherwise
                    error('Undefined')
                end

                % Load the database - from Jawwal
                [~,sheets] = xlsfinfo(Sim.path.BSexi{1});
                varname = genvarname(sheets);
                switch Sim.SA.name
                  case 'West Bank'
                    [~,~,SiteData] = xlsread(Sim.path.BSexi{1},...
                        sheets{1});
                    [~,~,x3GSectorsData] = xlsread(Sim.path.BSexi{1},...
                        sheets{2});
                    [~,~,x2GCellsData] = xlsread(Sim.path.BSexi{1},...
                        sheets{3});
                  case 'Gaza'
                    [~,~,SiteData] = xlsread(Sim.path.BSexi{1},...
                        sheets{4});
                    [~,~,x3GSectorsData] = xlsread(Sim.path.BSexi{1},...
                        sheets{6});
                    [~,~,x2GCellsData] = xlsread(Sim.path.BSexi{1},...
                        sheets{5});
                    SiteData(453,2:3) = num2cell(cellfun(@str2num,...
                        SiteData(453,2:3)));
                    tmp = [1274,1398,1421:1424];
                    x3GSectorsData(tmp,5) = num2cell(cellfun(...
                        @str2num,x3GSectorsData(tmp,5)));
                    x2GCellsData(:,3) = [];
                    tmp = [1236,1360,1383:1386];
                    x2GCellsData(tmp,5) = num2cell(cellfun(...
                        @str2num,x2GCellsData(tmp,5)));
                  otherwise
                    error('Undefined')
                end

                lat = SiteData(2:end,3);
                lon = SiteData(2:end,2);
                [x,y] = mfwdtran(Sim.mstruct,cell2mat(lat),cell2mat(lon));

                Antenna_height = zeros(size(SiteData,1)-1,1);
                isOmni = false(size(SiteData,1)-1,1);
                for n = 1 : size(SiteData,1)-1
                    % Populate Antenna_height
                    IND3G = strcmpi(SiteData{n+1,1},...
                        x3GSectorsData(:,1));
                    IND2G = strcmpi(SiteData{n+1,1},...
                        x2GCellsData(:,1));
                    Antenna_height(n) = max([...
                        cell2mat(x3GSectorsData(IND3G,5))
                        cell2mat(x2GCellsData(IND2G,5))]);

                    % Find omni to exclude them
                    if any(IND3G) && any(IND2G)
                        isOmni(n) = ~any(cell2mat(SiteData(n+1,6:7))>1);
                    elseif any(IND3G)
                        isOmni(n) = SiteData{n+1,7} < 2;
                    elseif any(IND2G)
                        isOmni(n) = SiteData{n+1,6} < 2;
                    else
                        error('Undefined')
                    end
                end

                BSexi = struct('X',num2cell(x(~isOmni)),...
                    'Y',num2cell(y(~isOmni)),...
                    'lat',lat(~isOmni),'lon',lon(~isOmni),...
                    'Antenna_height',num2cell(Antenna_height(~isOmni)),...
                    'Operator_Ref',SiteData([false;~isOmni],1),...
                    'Operator_Real','Jawwal',...
                    'sitChainConfig',...
                    Sim.exiCel.rul(rn).upg.sitChainConfig,...
                    'pwrAmpClas',Sim.exiCel.rul(rn).pwrAmpClas,...
                    'situ',situ,...
                    'AeCou',Sim.exiCel.rul(rn).upg.AeCou,...
                    'sectoCou',Sim.exiCel.rul(rn).upg.sectoCou,...
                    'Band',Sim.exiCel.rul(rn).upg.Band,...
                    'Operator',operato_{on},'FBMC',FBMC,'rul',rul);

                % Load the database - from Ooredoo
                switch Sim.SA.name
                  case 'West Bank'
                    kmlStruct = kml2struct(Sim.path.BSexi{2});
                    lat = [kmlStruct.Lat].';
                    lon = [kmlStruct.Lon].';
                    Operator_Ref = {kmlStruct.Name}.';
                    [x,y] = mfwdtran(Sim.mstruct,lat,lon);
                  case 'Gaza'
                    [~,~,x2GSites_Gaza] = xlsread(Sim.path.BSexi{3});
                    lat = cell2mat(x2GSites_Gaza(2:end,2));
                    lon = cell2mat(x2GSites_Gaza(2:end,3));
                    Operator_Ref = x2GSites_Gaza(2:end,1);
                    [x,y] = mfwdtran(Sim.mstruct,lat,lon);
                  otherwise
                    error('Undefined')
                end

                % Ruba Zaghmouri rzaghmouri@quartetoffice.org
                % Mon 09/12/2019 14:46
                % Ooredoo masts are installed at a height of 9m in
                % urban areas and at 12m height in rural areas.

                [I,J] = clu.R.worldToDiscrete(x,y);
                thisClu = clu.A(sub2ind(size(clu.A),I,J));
                isUr = cell2mat(clu.tran.pop(thisClu+1,5)) == 1;
                isRur = cell2mat(clu.tran.pop(thisClu+1,5)) == 4;
                Antenna_height = isUr.*9 + (~isUr&~isRur).*12 + ...
                    isRur.*42;

                BSexi = [BSexi; struct(...
                    'X',num2cell(x),'Y',num2cell(y),...
                    'lat',num2cell(lat),'lon',num2cell(lon),...
                    'Antenna_height',num2cell(Antenna_height),...
                    'Operator_Ref',Operator_Ref,...
                    'Operator_Real','Ooredoo',...
                    'sitChainConfig',...
                    Sim.exiCel.rul(rn).upg.sitChainConfig,...
                    'pwrAmpClas',Sim.exiCel.rul(rn).pwrAmpClas,...
                    'situ',situ,...
                    'AeCou',Sim.exiCel.rul(rn).upg.AeCou,...
                    'sectoCou',Sim.exiCel.rul(rn).upg.sectoCou,...
                    'Band',Sim.exiCel.rul(rn).upg.Band,...
                    'Operator',operato_{on},'FBMC',FBMC,'rul',rul)];
              case 'Israeli'
                switch Sim.exiCel.rul(rn).pwrAmpClas
                  case 'macrocell'
                  case 'smaCel5W'
                    continue
                  otherwise
                    error('Undefined')
                end

                % Load the database
                S = shaperead(Sim.path.Towers);
                
                [lat,lon] = minvtran(Sim.mstruct,[S.X],[S.Y]);
                
                [I,J] = clu.R.worldToDiscrete([S.X],[S.Y]);
                thisClu = clu.A(sub2ind(size(clu.A),I,J));
                isUr = cell2mat(clu.tran.pop(thisClu+1,5)) == 1;
                isRur = cell2mat(clu.tran.pop(thisClu+1,5)) == 4;
                Antenna_height = isUr.*9 + (~isUr&~isRur).*12 + ...
                    isRur.*42;
                
                BSexi = struct('X',num2cell([S.X].'),'Y',num2cell([S.Y].'),...
                    'lat',num2cell(lat.'),'lon',num2cell(lon.'),...
                    'Antenna_height',num2cell(Antenna_height),...
                    'Operator_Ref',NaN,'Operator_Real',{S.Type_}.',...
                    'sitChainConfig',...
                    Sim.exiCel.rul(rn).upg.sitChainConfig,...
                    'pwrAmpClas',Sim.exiCel.rul(rn).pwrAmpClas,...
                    'situ',situ,'AeCou',Sim.exiCel.rul(rn).upg.AeCou,...
                    'sectoCou',Sim.exiCel.rul(rn).upg.sectoCou,...
                    'Band',Sim.exiCel.rul(rn).upg.Band,...
                    'Operator',operato_{on},'FBMC',FBMC,'rul',rul);
              case 'O2'
                switch Sim.exiCel.rul(rn).pwrAmpClas
                    case 'macrocell'
                    case 'smaCel5W'
                        continue
                    otherwise
                        error('Undefined')
                end

                % Load the database
                [~,~,raw] = xlsread(Sim.path.BSexi);

                beep
                dips('Continue from here')
                keyboard

                BSexi = struct('X',num2cell(x),'Y',num2cell(y),...
                    'lat',num2cell(lat),'lon',num2cell(lon),...
                    'Antenna_height',num2cell(Antenna_height),...
                    'Operator_Ref',Operator_Ref,...
                    'Operator_Real','Ooredoo',...
                    'sitChainConfig',...
                    Sim.exiCel.rul(rn).upg.sitChainConfig,...
                    'pwrAmpClas',Sim.exiCel.rul(rn).pwrAmpClas,...
                    'situ',situ,'AeCou',Sim.exiCel.rul(rn).upg.AeCou,...
                    'sectoCou',Sim.exiCel.rul(rn).upg.sectoCou,...
                    'Band',Sim.exiCel.rul(rn).upg.Band,...
                    'Operator',operato_{on},'FBMC',FBMC,'rul',rul);
              otherwise
                error('Insert the desired MNO')
            end
        else
            BSexi = [];
        end
        clear operato

        % Remove sites that are too low to be macrocells
%         if ~isempty(BSexi)
%             switch Sim.exiCel.rul(rn).pwrAmpClas
%                 case 'macrocell'
%     %                 BSexi([BSexi.Antenna_height]<geomean([8 25])) = [];
%                     BSexi([BSexi.Antenna_height]<prod([8 25])^(1/2)) = [];
%                 case 'smaCel5W'
%     %                 BSexi([BSexi.Antenna_height]<geomean([3 8])) = [];
%     %                 BSexi([BSexi.Antenna_height]>geomean([8 25])) = [];
%                     BSexi([BSexi.Antenna_height]<prod([3 8])^(1/2)) = [];
%                     BSexi([BSexi.Antenna_height]>prod([8 25])^(1/2)) = [];
%                 case 'smaCel0W25'
%                     dfgdf2
%     %                 BSexi([BSexi.Antenna_height]>geomean([3 8])) = [];
%                     BSexi([BSexi.Antenna_height]>prod([3 8])^(1/2)) = [];
%             end
%         end
        
        % Set replEver
        if ~isempty(BSexi)
            [BSexi.replEver] = deal(Sim.propag.BS{strcmp(Sim.exiCel.rul(...
                rn).pwrAmpClas,Sim.propag.BS(:,1)),8});
        end
        
        % Find sites within the study area
        %     [~,ia,ic] = unique({BSexi.Site_NGR},'stable');
        if ~isempty(BSexi)
            n = 1;
            while n <= length(BSexi)
                distm = sqrt((BSexi(n).X-[BSexi.X]).^2+(BSexi(n).Y-[BSexi.Y]).^2);
                switch Sim.exiCel.rul(rn).pwrAmpClas
                    case 'macrocell'
                        isDistSma = find(distm>=0 & distm<40 & ...
                            strcmp(BSexi(n).Operator_Real,...
                            {BSexi.Operator_Real}));
                    case 'smaCel5W'
                        isDistSma = find(distm>=0 & distm<40 & ...
                            strcmp(BSexi(n).Operator_Real,...
                            {BSexi.Operator_Real}));
                    case 'smaCel0W25'
                        isDistSma = find(distm>=0 & distm<40 & ...
                            strcmp(BSexi(n).Operator_Real,...
                            {BSexi.Operator_Real}));
                end

                if length(isDistSma) > 1
                    Antenna_height = [BSexi(isDistSma).Antenna_height];
                    [~,I] = max(Antenna_height);
                    BSexi(isDistSma([1:I-1 I+1:end])) = [];

                    n = 1;
                else
                    n = n + 1;
                end
            end
            clear n distm isDistSma Antenna_height I
        end

        % Manually select macrocells without sub-1GHz support
        switch Sim.exiCel.rul(rn).pwrAmpClas
            case 'macrocell'
                [BSexi.Band] = deal([0 Sim.exiCel.rul(rn).upg.Band(2:end)]);
                [BSexi.AeCou] = deal([0 Sim.exiCel.rul(rn).upg.AeCou(2:end)]);
%                 [X,Y] = studyA.R.intrinsicToWorld(...
%                     [394,45,548,293,504,420],[199,50,127,209,325,260]);
%                 min(sqrt(([BSexi.X]-X.').^2+([BSexi.Y]-Y.').^2),[],2)
                IND = [];
%                 IND = [55,58,122,32,19,62];
%                 IND = [2,3,23,32,54,62,77,101,119,153];
%                 IND = [2,4,5,14,19,20,23,32,44,54,58,59,62,68,77,101,...
%                     119,153];
%                 IND = [2,3,4,5,14,19,20,23,32,44,54,58,59,62,68,77,101,...
%                     109,119,122,153];
%                 IND = [2,3,4,5,14,19,20,23,30,32,44,54,58,59,61,62,68,...
%                     77,101,119,122,153];
%                 IND = 1 : length(BSexi);
                [BSexi(IND).Band] = deal(Sim.exiCel.rul(rn).upg.Band);
                [BSexi(IND).AeCou] = deal(Sim.exiCel.rul(rn).upg.AeCou);
            case 'smaCel5W'
            case 'smaCel0W25'
        end
        
        BSexi_ = [BSexi_;BSexi];
        
        % Add some more sites to factor in that Sitefinder was relevant to 2014
        % and there is 10% annual increase
%         [~,~,raw] = xlsread('addiCell.csv');
%         if size(raw,1) == 1
%             beep
%             disp('addiCell.csv is vacant!')
%         end
%         for n = 2 : size(raw,1)
%             if strcmp(operato_{on},raw{n,1})
%                 BSexi(end+1).Operator = raw{n,1};
%                 BSexi(end).Operator_Ref = raw{n,2};
%                 BSexi(end).Site_NGR = raw{n,3};
%                 BSexi(end).Antenna_height = raw{n,4};
%                 BSexi(end).Type = raw{n,5};
%                 BSexi(end).Band = raw{n,6};
%                 BSexi(end).Antenna_Type = raw{n,7};
%                 BSexi(end).Power_dBW = raw{n,8};
%                 BSexi(end).Max_Licensed_Power_Per_Carrier_dBW = raw{n,9};
%                 BSexi(end).Max_Licensed_Power_Per_Carrier_dBm = raw{n,10};
%                 BSexi(end).X = raw{n,11};
%                 BSexi(end).Y = raw{n,12};
%             end
%         end
%         clear raw n
    end
end
end
