function [IND,thisOperato] = c_IND(sty,pwrAmpClasI,Sim,I1,J1,si,I2,J2,...
    vargin)
%% Find the simulation-map pixel-index for the network improvement
% c_IND returns a 1xN logical array, where N is the length of I2, with a
% single true element pointing to the element of I2. thisOperato is a
% logical MxN matrix, where M is the number of InP, with elements that are
% true if the InP can build on the pixel.
% 
% I2,J2 are simulation-map pixel-indices, 1xN integer arrays, of candidate
% pixels of network improvments. si is the size of the simulation-map.
% I1,J1 are the row and column of the simulation-map pixel-index that has
% been selected as the focus of improving service. pwrAmpClasI is the index
% to the list of available power amplifier classes, Sim.propag.BS(:,5).
% 
% The input sty identifies if the calling function aims to build a new
% site, upgrade or refresh an existing site, and must be one of the
% following:
% ('new','upg','decomm')
% 
% Sim is a SIM object and must contain the following fields,
% see class for definition: propag.BS, SA.pixSize, rul
% 
% If sty=='new', vargin is expected to contain the following input
% variables:
% construct - a 4D matrix, [size(Sim.construct) M], where M is the
%             number of InP, with elements that are true when a site
%             of a certain type (e.g. macrocell) can be commissioned by a
%             certain InP on the study area
% on - index to the InP list to indicate which InP are considered by the
%      parent function
% If sty=='upg', vargin is expected to contain the following input
% variables:
% isExpi - a logical vector with elements that are true when a cell has
%          expired
% isOperato - a logical vector with elements that are true for the cells of
%             the InP into consideration
% rul - index to Sim.rul of the site in question, which could be nonzero
% rulI - index to Sim.rul for which we seek upgradeable sites
% isPwrAmpClas - a logical vector with elements that are true for the cells
%                of the power class into consideration
% Cell - array of CELL objects
% If sty=='decomm', vargin is expected to contain the following input
% variables, see above: isExpi,isOperato,rul
% 
% * v0.1 Kostas Konstantinou Dec 2016

% Calculate the maximum propagation range of the site expressed in number
% of pixels, noOfPixelsApart. Since the propagation extends up to a certain
% range, this range is the maximum that a new site could be placed from
% I1,J1.
noOfPixelsApart = Sim.propag.BS{pwrAmpClasI,5} ./ Sim.SA.pixSize;

% Calculate the index of sites that are closeby
minI = max(I1-noOfPixelsApart,1);
maxI = min(I1+noOfPixelsApart,si(1));
minJ = max(J1-noOfPixelsApart,1);
maxJ = min(J1+noOfPixelsApart,si(2));
IND = minI<=I2 & I2<=maxI & minJ<=J2 & J2<=maxJ;

% Remove indices according to this function's call. For example, if the
% function call is to upgrade an existing site, then ensure that this site
% has not expired and that it is owned by the operator that is intended.
switch sty
    case 'new'
        % Commission new site
        
        % Unpack vargin
        [construct,on] = deal(vargin{:});
        
        % Calculate thisOperato which is true if the infrastructure owner
        % can commission a new site on the pixel. The infrastructure owner
        % may not be able to commission a new site if the site is too close
        % to other existing infrastructure. For example, a macrocell cannot
        % be commissioned too close to other macrocells - but it can be
        % close to small cells - of the same infrastructure owner.
        if length(on) == 1
            thisOperato = construct(sub2ind(size(construct),...
                I2,J2,pwrAmpClasI.*ones(1,length(I2)),...
                on.*ones(1,length(I2))));
        else
            thisOperato = false(length(on),length(I2));
            for on2 = 1 : length(on)
                thisOperato(on2,:) = construct(sub2ind(size(construct),...
                    I2,J2,pwrAmpClasI.*ones(1,length(I2)),...
                    on(on2).*ones(1,length(I2))));
            end
        end
        
        % Update IND
        IND = find(IND & any(thisOperato,1));
        
        % Find the closest pixel
        [minV,minI] = min(sqrt((I2(IND)-I1).^2 + (J2(IND)-J1).^2));
    case 'upg'
        % Upgrade existing site
        
        % Unpack vargin
        [isExpi,isOperato,rul,isPwrAmpClas,rulI,Cell] = deal(vargin{:});
        
        % Initialise thisOperato
        thisOperato = [];
        
        % Find the three closest sites.
        % Delaunay triangulation could be another option but it may suffer
        % from edge effects.
        % rul==0 is ommitted here, because we wish to find the 3 closest
        % sites regardless of if they are upgradeable or not.
        IND = find(IND & ~isExpi & isOperato & isPwrAmpClas);
        [B,I] = sort((J2(IND)-J1).^2+(I2(IND)-I1).^2);
        I(4:end) = [];
        
        % While loop to find if the rule can be applied to any of the 3
        % closest sites
        n = 0;
        while n < length(I)
            % Advance closest site counter
            n = n + 1;

            % thisRul = rul(IND(I(n))) is the rule index of the site in
            % question which could be nonzero. rulI is the rule index for
            % which we seek upgradeable sites.
            
            % Find if the rule can be applied to the site
            thisRul = rul(IND(I(n)));
            if thisRul > 0
                % The site has already been considered for upgrade or
                % refresh this year, or it is new
                
                % Find if the rule in question can be applied to the site
                % in question:
                % a) current site specs should match
                % b) rule types (new, upgrade, refresh) should match
                % c) rules cannot be identical
                % d) current site specs can only advance
                tmp = rulI ~= thisRul && ...
                    strcmp(Sim.rul(rulI).curreHash,Sim.rul(thisRul).curreHash) && ...
                    ~xor(Sim.rul(rulI).isDecomm,Sim.rul(thisRul).isDecomm) && ...
                    all(Cell(IND(I(n))).band.capa<=Sim.rul(rulI).upg.Band) && ...
                    all(Cell(IND(I(n))).AeCou<=Sim.rul(rulI).upg.AeCou);
                if tmp && ~Sim.rul(rulI).isDecomm
                    tmp = tmp && ...
                        ~xor(Sim.rul(rulI).isUpg,Sim.rul(thisRul).isUpg);
                end

                % If the rule can be applied then break the while loop
                % search
                if tmp
                    % The site can be considered for further upgrade
                    minI = I(n);

                    % The site has been found and there is no need to
                    % search further
                    break
                end
            else
                % The site has not been considered for upgrade or
                % refresh this year, nor it is new
                if ~Sim.rul(rulI).isDecomm
                    % The rule in question is to commission new site or
                    % upgrade existing site
                    if ~Sim.rul(rulI).isUpg
                        % The rule in question is to commission new site
                        beep
                        disp('check this!')
                        keyboard
                        minI = I(n);
                        
                        break
                    else
                        % The rule in question is to upgrade existing site.
                        % Find if the rule in question can be applied to the site
                        % in question:
                        % a) current site specs should match
                        curre = struct('sitChainConfig',Cell(IND(I(n))).sitChaiConfig,...
                            'sectoCou',Cell(IND(I(n))).sectoCou,...
                            'AeCou',Cell(IND(I(n))).AeCou,...
                            'Band',Cell(IND(I(n))).band.capa);
                        tmp = strcmp(Sim.rul(rulI).curreHash,DataHash(curre));

                        % If the rule can be applied then break the while loop
                        % search
                        if tmp
                            % The site can be considered for upgrade
                            minI = I(n);

                            % The site has been found and there is no need to
                            % search further
                            break
                        end
                    end
                else
                    % The rule in question is about reinstatement of
                    % expiring or expired site. However, the site is not
                    % expiring, thus there is a mismatch.
                end
            end
        end
        
        if n == length(I)
            % The sites are not upgradeable
            minV = [];
        else
            minV = sqrt(B(n));
        end
    case 'decomm'
        % Reinstatement of expiring or expired site
        
        % Unpack vargin
        [isExpi,isOperato,rul] = deal(vargin{:});
        
        % Initialise thisOperato
        thisOperato = [];
        
        % Update IND
        IND = find(IND & isExpi & isOperato & rul==0);
        
        % Find the closest pixel
        [minV,minI] = min(sqrt((I2(IND)-I1).^2 + (J2(IND)-J1).^2));
    otherwise
        error('Code this!')
end

% If a site location was found and it is close enough, then update IND
if minV <= noOfPixelsApart
    IND = IND(minI);
else
    IND = [];
end
