function params = c_param_sitWithiCov1(visib,fn,Sim,clu,Cell,bund1,fs1,...
    thisDem,isCell)

% Unpack
% Celli: sitW
% Celli2: secW
% RxPowerRB_DL: wanted RSRP in dBm/180kHz
sit_ = cellfun(@(x) x(:,1),visib.ii,'UniformOutput',false);
sec_ = cellfun(@(x) x(:,2),visib.ii,'UniformOutput',false);
ii_ = cellfun(@(x) x(:,3),visib.ii,'UniformOutput',false);
sigmaLe_ = cellfun(@(x) x(:,4),visib.ii,'UniformOutput',false);
% if servCou == 1
%     sitW = cellfun(@(x) x(1,1),visib.ii);
% %     Celli2 = cellfun(@(x) x(1,2),visib.ii);
%     RxPowerRB_DL = cellfun(@(x) x(1,3),visib.ii);
% else
%     sitW = cell2mat(cellfun(@(x) x(1:servCou,1).',visib.ii,...
%         'UniformOutput',false));
% %     Celli2 = cell2mat(cellfun(@(x) x(1:servCou,2).',visib.ii,...
% %         'UniformOutput',false));
%     RxPowerRB_DL = cell2mat(cellfun(@(x) x(1:servCou,3).',visib.ii,...
%         'UniformOutput',false));
% end

% Assertion, but unsure if the 5th column was actually indended
assert(isequal(clu.tran.pop(2:end,5),clu.tran.veh(2:end,5)),...
    'For simplicity the code was developed with the assumption that the Hata branch used for both vehicular and population path loss calculations is the same')

if ~all(visib.faFadMargi(~isnan(visib.faFadMargi))==0,1)
    beep
    disp('Code this!')
    keyboard
end
faFadMargi = zeros(1,size(visib.faFadMargi,2));

interfMultip = Sim.interfMultip;

%% Assert priorty order is as expected
% n = 1;
% N = find(strcmp(Sim.dem.famUseCase_,'uMTC'));
% while 1
%     if ~(Sim.dem.servOrd(n,1)==N)
%         break
%     end
%     n = n + 1;
% end
% N = find(strcmp(Sim.dem.famUseCase_,'MBB'));
% while 1
%     if ~(Sim.dem.servOrd(n,1)==N)
%         break
%     end
%     n = n + 1;
% end
% N = find(strcmp(Sim.dem.famUseCase_,'MTC'));
% while n <= size(Sim.dem.servOrd,1)
%     if ~(Sim.dem.servOrd(n,1)==N)
%         break
%     end
%     n = n + 1;
% end
% assert(n==size(Sim.dem.servOrd,1)+1,...
%     'If this fails then the paragraph needs rethinking')

% Simplification
Operator = {Cell.Operator};  % InP
OperatorI = arrayfun(@(x) find(strcmp(x,Sim.dem.operators)),Operator);  % InP
BHcou = size(Sim.dem.hourDistri,1);
operatoN = length(Sim.dem.operators);
dupMode = Sim.linBud.dupMode(fn);

% []
if size(visib.bund2,2) > 1
    assert(all(visib.bund2(:,1)==visib.bund2(:,2)),...
        'Code this and check its use within the loop too!')
end

% Find entries of visib that have common bund2, pwrAmpClasMS, and situMS
% [~,~,ic1] = unique(visib_bund2);
[~,~,ic2] = unique(visib.pwrAmpClasMS,'stable');
[~,~,ic3] = unique(visib.situMS,'stable');
assert(max(visib.bund2(:))<10&&max(ic2)<10&&max(ic3)<10)
[~,visib_ia] = unique(visib.bund2(:,1).*100+ic2.*10+ic3,'stable');

%% Calculate BW availability for MTC
% switch fam
%     case 'MBB'
%         BWava = inf(BHcou,operatoN);  % [BHcou hours, InP]
%     case 'uMTC'
%         BWava = inf(BHcou,operatoN);
%     case 'MTC'
%         % Calculate how much BW is left for MTC
%         BWava = zeros(BHcou,operatoN);
%         for on = 1 : operatoN  % For each InP
%             if any(OperatorI==on)
%                 utilisO = cell2mat(arrayfun(@(x) sum(sum(x.band.utilis(:,:,fn,...
%                     [1:famI-1 famI+1:end]),4),2),Cell(OperatorI==on),...
%                     'UniformOutput',false));
%             else
%                 utilisO = zeros(BHcou,1);
%             end
%             BWava(:,on) = 1 - max(utilisO,[],2);
%         end
%         clear utilisO
%     otherwise
%         beep
%         disp('Coding needed?')
%         keyboard
% end
% BWava = inf(BHcou,operatoN);  % [BHcou hours, InP]

%% Serve links
% Simplify for increased speed
CellN = length(Cell);

% Simplify for increased speed
sectoCou = [Cell.sectoCou].';
capa = arrayfun(@(x) x.band.capa(fn),Cell) .* sectoCou.';
utilis = zeros(BHcou,operatoN,CellN,length(Sim.dem.famUseCase_));
for n = 1 : operatoN  % For each tenant
    if CellN > 1
        utilis(:,n,:,:) = reshape(cell2mat(arrayfun(@(x) squeeze(x.band.utilis(:,n,fn,:)),...
            Cell,'UniformOutput',false).'),[BHcou,CellN,...
            length(Sim.dem.famUseCase_)]);
    else
        utilis(:,n,:,:) = squeeze(Cell.band.utilis(:,n,fn,:));
    end
end
% utilisEff_save = utilisEff;
if length(bund1.ia) == 1
    pwrAmpClasBS = repmat({bund1.C(1).pwrAmpClas},length(bund1.C),1);
    situBS = repmat({bund1.C(1).situ},length(bund1.C),1);
else
    pwrAmpClasBS = arrayfun(@(x) x.pwrAmpClas,bund1.C,...
        'UniformOutput',false);
    situBS = arrayfun(@(x) x.situ,bund1.C,'UniformOutput',false);
end
distBordm = [Cell.distBordm].';
residuErr = [Cell.residuErr].';

% Simplify for increased speed
BPGstand.ur = cellfun(@(x,y) Sim.linBud.(x).(y).BPGstand.(fs1).ur,...
    pwrAmpClasBS,situBS);
BPGstand.ot = cellfun(@(x,y) Sim.linBud.(x).(y).BPGstand.(fs1).ot,...
    pwrAmpClasBS,situBS);
demTranslati7 = cell2mat(Sim.demTranslati(:,7));

% Simplify for increased speed
% if length(unique(OperatorI)) == 1
%     shariOperatoDemTranslati7 = Sim.shari.is(OperatorI(1),demTranslati7).';
% else
%     beep
%     disp('Code this!')
%     keyboard
% end

% Simplify for increased speed
if all(structfun(@(x) all(x==0),BPGstand)) && ...
        all(cellfun(@(y,z) all(structfun(@(x) x==0,...
        Sim.linBud.(y).(z).BPGstand.(fs1))),...
        visib.pwrAmpClasMS(visib_ia),visib.situMS(visib_ia)))
    isBPGstandZero = true;
else
    isBPGstandZero = false;
end

% Simplify for increased speed
% IND2d = false(size(Sim.demTranslati,1),length(visib_ia),...
%     length(Sim.dem.famUseCase_));
IND2c = false(size(Sim.demTranslati,1),length(Sim.dem.famUseCase_));
trafClI = Sim.dem.modeAlTrafClasAs;
trafCl = Sim.dem.trafClas_{trafClI};
for famI = 1 : length(Sim.dem.famUseCase_)
    fam = Sim.dem.famUseCase_{famI};
%     IND2d(:,:,famI) = strcmp(repmat(visib.pwrAmpClasMS(visib_ia).',...
%         size(Sim.demTranslati,1),1),...
%         repmat(Sim.demTranslati(:,3),1,length(visib_ia))) & ...
%         strcmp(repmat(visib.situMS(visib_ia).',size(Sim.demTranslati,1),1),...
%         repmat(Sim.demTranslati(:,4),1,length(visib_ia))) & ...
%         strcmp(repmat(Sim.dem.mobiClasses(visib.bund2(visib_ia,1)).',...
%         size(Sim.demTranslati,1),1),...
%         repmat(Sim.demTranslati(:,6),1,length(visib_ia)));
    
    % Find which columns of thisDem are related to the family of services and
    % traffic class in question
    IND2c(:,famI) = strcmp(Sim.demTranslati(:,1),fam) & ...
        strcmp(Sim.demTranslati(:,2),trafCl);
end

% Simplify
thisYea = Sim.yea.thisYea;

% Simplify
% if servCou == 1
%     isServCouMaxOne = true;
% else
%     isServCouMaxOne = false;
% end
if thisYea >= Sim.shari.from
    isShari = true;
else
    isShari = false;
end

% Simplify
% co_ch_ = [Cell.co_ch];
[~,Locb] = ismember({Cell.pwrAmpClas},Sim.propag.BS(:,1));
maxDist_m = cell2mat(Sim.propag.BS(Locb,5));
% maxDist_m = [Cell.maxDist_m];

% Simplify
isUr = cell2mat(clu.tran.pop([thisDem.clu]+1,5)) == 1;

% Create a logical sparse matrix version of studyAind (length(studyAind) x
% max(studyAind)). Some functions are faster when in sparse matrix format, for
% example look at uses of S.
% S = sparse(1:length(visib.studyAind),visib.studyAind,true,...
%     length(visib.studyAind),max(visib.studyAind));

% []
isCell_S = sparse(1:length(isCell),isCell,true,...
    length(isCell),length(bund1.C));
% isCell_S = false(length(bund1.C),1);
% isCell_S(isCell) = true;

% []
isCellN = length(isCell);

% []
% [col,~] = find(visib.PRx_across_system_BW_DL_S.');

% Find which columns of thisDem are related to each tenant
[~,LocOperato] = ismember(Sim.demTranslati(:,5),Sim.dem.operators);

params = {faFadMargi,interfMultip,OperatorI,BHcou,operatoN,dupMode,...
    CellN,capa,utilis,pwrAmpClasBS,demTranslati7,isBPGstandZero,thisYea,...
    isShari,maxDist_m,isUr,IND2c,isCell_S,isCellN,LocOperato,sectoCou,...
    sit_,sec_,ii_,sigmaLe_,situBS,distBordm,residuErr};
