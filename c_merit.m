function [thisDemBest,CellBest,edgCloSitBest,improBest,...
    interfIequiBest,bund1Best] = c_merit(thisDem,Cell,edgCloSit,thisImpro,Sim,...
    studyA,clu,ter,SINR2tput,unserv,Cell_save,thisDemI,InP_,tenan_,...
    interfIequi0,bund1,dsm)
%% Calculate merit of each improvement and apply the improvement that leads to the gretest merit
% c_merit calculates the merit of each improvement within list thisImpro
% and returns the improvement with the greatest merit, improBest. For this
% best improvement, it also returns: a) the demand status (e.g. which
% pixels are served), thisDemBest, b) the network upgraded with the best
% improvement, CellBest and edgCloSitBest, and the interference indices
% that lead to highest SINR, interfIequiBest.
% 
% thisDem is an array of demand objects and must contain the following
% fields, see class for definition: I, J, v, satis, assoc
% 
% unserv is the unserved demand expressed in Mbit/s in MxN, where M =
% length(thisDem) and N = length(Sim.dem.famUseCase_). unserv could be
% derived from thisDem.satis and thisDem.v, but it is faster to work with
% unserv, thus this function updates both thisDem and unserv.
% 
% Cell is a array of CELL objects and must contain almost all of its fields
% (a selection is listed below), see class for definition: I, J,
% capexPv, opexPv, connTowaCloID, band, spatBeamCou, MIMOstreaCou,
% pwrAmpClas, sitChaiConfig, cellProce, userDyn, userRema, rul, AeCou,
% sectoCou, bareMetaBBUcou, upg, connTowaCloTyp, connTowaCloBW, expiYea,
% laten, ch, etc.
% 
% edgCloSit is a array of edge cloud site objects and must contain almost
% all of its fields, see class for definition.
% 
% clu is a CLU object and must contain almost all of its fields, see class
% for definition.
% 
% ter is a TER object and must contain almost all of its fields, see class
% for definition.
% 
% SINR2tput is a SINR2tput object and must contain almost all of its
% fields, see class for definition.
% 
% thisImpro is an IMPRO object and must contain almost all of its
% fields, see class for definition.
% 
% Sim is a SIM object and must contain the following fields,
% see class for definition: dem.consiInMeritPerTena, dem.hourDistri,
% dem.trafVol, demTranslati, rul
% 
% studyA is a structure and must contain almost all of its
% fields, see class for definition.
% 
% InP_, tenan_ could be calculated by Sim.isShari, but to save on time are
% provided as input. InP_, tenan_ are cell lists of which InP can serve
% which tenant. For example InP_ = {[1]}, tenan_ = {[1,2]}, means InP 1 can
% serve Tenant 1 and Tenant 2, and that other InP (if any) do not serve any
% traffic.
% 
% Cell_save is an array of CELL objects. These correspond to the status of
% the network prior to application of any rule, in case it is needed for
% rule reconsideration. Rules can be applied before enterring this function
% because refreshes and D-RAN to C-RAN rules are forced rules. A
% reconsideration occurs when there is leftover unserved demand after the
% application of a rule, and a rule with more antennas or carriers could be
% re-considered.
% 
% The input thisDemI is an index to thisDem of the prime unserved point.
% 
% The input interfIequi0 is an MxN matrix, where M=BHcou, N=length(Cell).
% It contains integer values of the interference indices before any network
% improvement.
% 
% * v0.1 Kostas Konstantinou Aug 2018

% Save the demand and network state, prior to any improvement application
thisDem0 = thisDem;
Cell0 = Cell;
edgCloSit0 = edgCloSit;
bund10 = bund1;

% For each improvement, calculate the amount of newly served demand,
% demPerImpro, PV of improvement's cost, PVperImpro, and its merit, merit
demPerImpro = zeros(length(thisImpro),1);
PVperImpro = zeros(length(thisImpro),1);
merit = zeros(length(thisImpro),1);
for n = 1 : length(thisImpro)
    % Simplification
    ci = thisImpro(n).CellIND;

    % Apply improvement
%     [Cell,edgCloSit,thisDem(demI),interfIequi] = apply_impro(Cell0,...
%         edgCloSit0,thisImpro(n),Sim,studyA,clu,ter,thisDem0(demI),...
%         SINR2tput,Cell_save,interfIequi0,isCell0);
    [Cell,edgCloSit,interfIequi,bund1,thisImpro(n),isCell0,isXtunab] = ...
        apply_impro(Cell0,edgCloSit0,interfIequi0,thisImpro(n),Sim,...
        studyA,clu,ter,Cell_save,bund10,dsm);
    
    % Find which demand points should be evaluated for service
    demI = sum(unserv(:,tenan_{thisImpro(n).operatoI}) * ...
        Sim.dem.consiInMeritPerTena(InP_{thisImpro(n).operatoI}),2) > 0;
    if ci > length(Cell0)
        % New cell
    else
        % Existing cell
        if Cell0(ci).rul == 0
            % This is the first time this cell is considered for a rule. The
            % demand points affected are only these which lacked service. In a
            % more careful consideration, the demand points within the Voronoi
            % should be considered, regradless of their service satisfacation.
            % This is because the demand points may change serving cell,
            % however, in the interest of runtime only underserved points are
            % considered here, similar to CAPisce 1.0.
        else
            % The cell rule is being reconsidered
        end
    end

    % Simplifications
    BHcou = size(Sim.dem.hourDistri,1);
    isTxMultiCopiesDataToAllServ = cellfun(@(x) Sim.dem.trafVol.(x...
        ).CEtput.isTxMultiCopiesDataToAllServ(1),...
        Sim.demTranslati(:,1));
    
    % Simplifications
    assoc = [thisDem0.assoc];
    assoc = reshape(assoc(:),[BHcou,size(Sim.demTranslati,1),...
        length(thisDem0)]);
    assoc2 = assoc(:,isTxMultiCopiesDataToAllServ,:);
    assoc(:,isTxMultiCopiesDataToAllServ,:) = [];
    
    % Select demand objects that were associated with the cell
    % in question
%             demI = demI | shiftdim(any(any(reshape(Cell2Vec(assoc),...
%                 size(assoc))==ci,2),1) | ...
%                 any(any(cellfun(@(x) any(x==ci),assoc2),2),1),2);
    demI = demI | any(cell2mat(cellfun(@(y) squeeze(any(any(reshape(...
        Cell2Vec(assoc),size(assoc))==y,2),1) | ...
        any(any(cellfun(@(x) any(x==y),assoc2),2),1)),...
        num2cell(find(isXtunab.')),'UniformOutput',false)),2);
    
    % Serve demand with improvement
    thisDem = thisDem0;
    [Cell,edgCloSit,thisDem(demI),interfIequi] = serv_with_impro(Cell,...
        edgCloSit,thisDem0(demI),interfIequi,thisImpro(n),Sim,isCell0,...
        isXtunab,studyA,SINR2tput,clu,bund1);
    
    % If the improvement does not serve the prime unserved demand point,
    % then this improvement is unsuitable for this unserved demand point,
    % and the merit is set to 0. If no action is taken to force merit to 0,
    % then the code enters an infinite loop.
    % thisDemI is empty for refreshing sites, because there is no prime
    % unserved point, since the refreshment occurs even when not needed.
    if isempty(thisDemI) || ...
            any(thisDem(thisDemI).satis(:)&~thisDem0(thisDemI).satis(:))
        % Calculate the newly served demand
        isCell1 = isXtunab;
%         isCell1 = accumarray(nonzeros(...
%             unique(cell2mat(assoc(:,:,demI)))),true,[length(Cell) 1]);
        if ~Sim.rul(thisImpro(n).rulID).isDecomm
            % Commission new site or upgrade existing site
            demPerImpro(n) = sum(cellfun(@(x) c_unserv(Sim,...
                thisDem(demI),studyA,'newly serv','per site',Cell,x,...
                thisDem0(demI)),num2cell(find(isCell1))),1);
            
%             if sum(cellfun(@(x) c_unserv(Sim,...
%                     thisDem(demI),studyA,'newly serv','per site',Cell,x,...
%                     thisDem0(demI)),num2cell(find(isCell1))),1)
%                 beep
%                 disp('Inspect this interesting case')
%                 disp('If never stopped here then investigate further why swapping of association can happen towards sites that are not within isXtunab')
%                 keyboard
%             end
        else
            % Refreshing site
            isCell1(ci) = false;
            demPerImpro(n) = c_unserv(Sim,thisDem,studyA,'serv',...
                'per site',Cell,ci) + sum(cellfun(@(x) c_unserv(Sim,...
                thisDem(demI),studyA,'newly serv','per site',Cell,x,...
                thisDem0(demI)),num2cell(find(isCell1))),1);
        end
        
        % Calculate the PV cost of the improvement
        PVperImpro(n) = c_PVperSit(Cell,edgCloSit,ci);
        if ~Sim.rul(thisImpro(n).rulID).isDecomm
            % Commission new site or upgrade existing site
            if ~Sim.rul(thisImpro(n).rulID).isUpg
                % Commission new site
            else
                % Upgrade existing site
                PVperImpro(n) = PVperImpro(n) - ...
                    c_PVperSit(Cell0,edgCloSit,ci);
            end
        else
            % Refreshing site
        end
    else
        % The improvement does not serve the prime unserved demand point
        demPerImpro(n) = 0;
        PVperImpro(n) = 99999;
    end

    % If demPerImpro is negative, then set it to zero
    if demPerImpro(n) < 0
        demPerImpro(n) = 0;
    end
    
    % Calculate the merit.
    % There was an idea that the merit function would improve if it
    % included a penalty for the leftover demand. The leftover demand
    % penalty is currently multiplied by zero, because re-consideration of
    % rules was thought to work better than the panalty.
    merit(1:n) = demPerImpro(1:n) ./ PVperImpro(1:n);
    [B,I] = max(merit(1:n));
    assert(demPerImpro(I)>=0)
    if demPerImpro(I) > 0
        merit(1:n) = merit(1:n) ./ B + ...
            0 .* (demPerImpro(1:n)-max(demPerImpro)) ./ demPerImpro(I);
    end
    
    % Update the maximum merit across all tried improvements, meritMax
    if n == 1
        meritMax = -inf;
    else
        if ~all(merit(1:n-1)==0)
            meritMax = max(merit(1:n-1));
        else
            [~,I] = min(PVperImpro(1:n));
            if n == I
                meritMax = -inf;
            else
                meritMax = 0;
            end
        end
    end
    
    % If the merit of this improvement is better than meritMax, then update
    % the outputs to correspond to this improvement
    if merit(n) > meritMax
        thisDemBest = thisDem;
        CellBest = Cell;
        edgCloSitBest = edgCloSit;
        improBest = thisImpro(n);
        interfIequiBest = interfIequi;
        bund1Best = bund1;
    end
end

% If all improvements have zero merit and the improvement is not a refresh
% (refresh improvements occur even if merit=0), then output improBest as 
% empty, and demand & network state are returned unchanged from this
% function
% if Sim.rul(thisImpro(1).rulID).isDecomm
% else
if all(merit==0)
    thisDemBest = thisDem0;
    CellBest = Cell0;
    edgCloSitBest = edgCloSit0;
    improBest = [];
    interfIequiBest = interfIequi0;
    bund1Best = bund10;
end
% end
end
