function [Cell,thisDem,utilis] = c_sitWithiCov_per_connecti(Cell,visib,...
    thisDem,utilis,fn,fs1,trafClI,clu,Sim,isDispLinBud,utilisBand,...
    utilisN,servCou,params,params2)
%% [Title]
% [Description]
%
% * v0.1 KK 28Dec18 Created

warning( 'off', 'MATLAB:mat2cell:TrailingUnityVectorArgRemoved' );

% Load pre-calculated parameters
[faFadMargi,interfMultip,OperatorI,BHcou,operatoN,dupMode,CellN,capa,...
    pwrAmpClasBS,demTranslati7,isBPGstandZero,thisYea,isShari,maxDist_m,...
    isUr,IND2c,isCellN,LocOperato,sectoCou_,distBordm,residuErr] = ...
    deal(params{[1:8,10:18,20:21,27:28]});
[sitW,sitI,secW,secI,wN,iN,sigmaW,sigmaI,visibI,LocisCell,Un,...
    Pic,P,P2,I_ULic,I_UL0,IM_DL0,N2,subs,N2_S,row] = ...
    deal(params2{servCou}{:});

% Initialise isAssessed, which is a logical vector (length(visibI) x 1)
% with true for links that have been assessed
isAssessed = false(length(visibI),1);

% Simplify calls to size and length
L1 = length(demTranslati7);
L2 = length(thisDem);

% Simplify satis, SINR_DL and assoc
satis = reshape([thisDem.satis],BHcou,L1,L2);
SINR_DL = reshape([thisDem.SINR_DL],BHcou,L1,L2);
assoc = reshape([thisDem.assoc],BHcou,L1,L2);
thisDemBand = reshape([thisDem.band],BHcou,L1,L2);
tputOveMbps = reshape([thisDem.tputOveMbps],BHcou,L1,L2);

% Simplifications
interfSuppr = Sim.linBud.interfSuppr(Sim.yea.thisYea == ...
    Sim.linBud.interfSupprYea);

% Calculate index to interfMultip.
% Variable interfMultip is a pre-calculated matrix that contains all
% possible interference multipliers that can be queried.
% Variable interfMultip is a cell matrix, with each cell having 3
% dimensions, i.e. {secCou,secW}(loadW,loadI,secI), where secCou is the
% number of sectors (e.g. 2-sector small cells, or 3-sector macrocells),
% secW is the index of the wanted sector (i.e. for macrocells 1 for north,
% 2 for SE, 3 for SW), loadW is the interference index of the wanted
% sector, secI is the index of the interfering sector (similar to index of
% wanted sector), loadI is the interference index of the interfering
% sector.
% sub2ind(size(tmp),dim1Sub,dim2Sub,dim3Sub)
if servCou == 1
    dim1Sub = utilisBand(:,LocisCell);  % (BHcou,p)
    dim1Sub2 = utilisBand(:,LocisCell(subs));  % (BHcou,size(sitI,1)*p)
    dim2Sub = utilisBand(:,row);  % (BHcou,size(sitI,1)*p)
    dim3Sub = repmat(cell2mat(secI),1,BHcou).';  % (BHcou,size(sitI,1)*p)
else
    % Note: No vectorisation here because there is only a few pixels that
    % require MC.
    dim1Sub = zeros(BHcou,length(N2),servCou);
    for n = 1 : servCou
        dim1Sub(:,:,n) = utilisBand(:,LocisCell(:,n));  % (BHcou,p,servCou)
    end
    dim1Sub2 = zeros(BHcou,sum(N2),servCou);
    for n = 1 : servCou
        dim1Sub2(:,:,n) = utilisBand(:,LocisCell(subs,n));  % (BHcou,size(sitI,1)*p,servCou)
    end
    dim2Sub = zeros(BHcou,sum(N2),servCou);
    for n = 1 : servCou
        dim2Sub(:,:,n) = utilisBand(:,row(:,n));  % (BHcou,size(sitI,1)*p,servCou)
    end
    dim3Sub = shiftdim(repmat(cell2mat(secI),[1,1,BHcou]),2);  % (BHcou,size(sitI,1)*p)
end
linearInd = (dim3Sub-1).*utilisN.^2 + (dim2Sub-1).*utilisN + ...
    dim1Sub2;
clear dim1Sub2 dim2Sub dim3Sub

% Look up the interference multiplier within interfMultip of indices given
% by linearInd, and save as thisInterfMultip.
% Find if the wanted and interfering sectors belong to the same site,
% isSameSitOtSec.
% sectoCou is a matrix (pathCou x servCou)
sectoCou = sectoCou_(LocisCell(:,1:servCou));
if size(sectoCou,2) ~= servCou
    sectoCou = sectoCou.';
end
if servCou == 1
    secW2 = num2cell(secW);
    thisInterfMultip = cellfun(@(x,y,z) interfMultip{x,y}(z),...
        num2cell(sectoCou.'),secW2.',mat2cell(linearInd,BHcou,N2.'),...
        'UniformOutput',false);
    isSameSitOtSec = cellfun(@(x,y,z,a) x==y & z~=a,num2cell(sitW),sitI,...
        secW2,secI,'UniformOutput',false).';
else
    thisInterfMultip = cell(servCou,length(N2));
    isSameSitOtSec = cell(servCou,length(N2));
    for n = 1 : servCou
        thisInterfMultip(n,:) = cellfun(@(x,y,z) interfMultip{x,y}(z),...
            num2cell(sectoCou(:,n).'),num2cell(secW(:,n).'),...
            mat2cell(linearInd(:,:,n),BHcou,...
            cellfun(@(x) size(x,1),sitI).'),'UniformOutput',false);
        isSameSitOtSec(n,:) = cellfun(@(x,y,z,a) x==y(:,n) & z~=a(:,n),...
            num2cell(sitW(:,n)),sitI,num2cell(secW(:,n)),secI,...
            'UniformOutput',false);
    end
end
thisInterfMultip = cell2mat(thisInterfMultip);
isSameSitOtSec = cell2mat(isSameSitOtSec.').';
clear sectoCou secW2

% Calculate the frequency correction factor for border interference
IM_DL2correctiFacto = 20.*log10(Sim.linBud.spotFreq(fn)./2140);

% For a bunch of links in the visibI order, [...]
% Bunches are considered for vectorised code which improves runtime.
% isBreak = false;
isDispLinBudThisItera = false;
N = 180;
p_ = 1 : N : length(visibI);
linearInd3_ = [];
for pn = 1 : length(p_)
    % Find the indices to visibI of the bunch. Reminder that visibI sorts
    % the links in decreasing SINR_DL order.
    thisP = p_(pn) : min(p_(pn)+N-1,length(visibI));
    
    % Find the indices to visib entries of this bunch, and store it as
    % IND3.
    IND3 = visibI(thisP);
    
    % Mark the paths between the pixel and co-channel cells as assessed
    isAssessed(IND3) = true;
    
    % Incomplete code for automation in writing a link budget csv
%         if any(thisYea==[2020,2021]) && isDispLinBud && famI>1
    if any(thisYea==[2020,2021]) && isDispLinBud
        beep
        disp('Check this!')
        keyboard
        tmp = sitW(visibI(pn+1:end),:);
        if ~any(strcmp(pwrAmpClasBS(tmp(~isAssessed(visibI(pn+1:end)...
                ))),'macrocell'))
            % There will be no macrocell left to be assessed after this
            % iteration
            isDispLinBudThisItera = true;
        end
    else
        isDispLinBudThisItera = false;
    end
    
    % Simplifications
    % pwrAmpClasMS, situMS, thisDem_IND and bund2 are common across
    % visib(IND3), and it is OK to use visib(IND3(1))
    pwrAmpClasMS = visib.pwrAmpClasMS(IND3);
    situMS = visib.situMS(IND3);
    thisDem_IND = visib.thisDem_IND(IND3);
%     thisBund2 = bsxfun(@eq,bund2.ic,visib.bund2(IND3,1).');

    % Simplifications
%     N2 = cellfun(@(x) size(x,1),sitI(IND3));
    subs = (sum(bsxfun(@gt,1:sum(N2(IND3)),cumsum(N2(IND3))),1)+1).';
    
    % Calculate the SINR threshold for the bunch of links, c_SINRthres_DL,
    % based on the utilisation/load of wanted site, service requirements,
    % and MIMO order.
    SINRthres_DL = c_SINRthres_DL(IND3,visib,isUr,BHcou,utilisN,...
        isBPGstandZero,thisDem_IND,sitI(IND3),servCou,dim1Sub(:,IND3,:));

    % Find which columns of thisDem are related to
    % the family of services, traffic class,
    % pwrAmpClasMS, situation, and mobility class in question, and store it
    % asIND2.
    tmp = Sim.dem.famUseCase_(visib.famI(IND3));
    if size(tmp,1) == 1
        tmp = tmp.';
    end
    [~,Locb] = ismember(strcat(pwrAmpClasMS,situMS,...
        Sim.dem.mobiClasses(visib.bund2(IND3,1)),tmp),...
        strcat(Sim.demTranslati(:,3),Sim.demTranslati(:,4),...
        Sim.demTranslati(:,6),Sim.demTranslati(:,1)));
    IND2 = IND2c(:,visib.famI(IND3)) & ...
        full(sparse(Locb,(1:length(Locb)),true,...
        size(Sim.demTranslati,1),length(Locb)));
    
    % Link budget. Calculate if the link is covered, isCov. 
    [row2,~] = find(N2_S(IND3,:).');
    row3 = row(row2,:);
    [isCov,Mu_DL,noisRis,isCovPer] = c_isCov(SINRthres_DL,...
        sigmaW(IND3,:),sigmaI(row2,:),...
        P(Pic(IND3,:)).',P2(Pic(IND3,:)).',dupMode,visib.famI(IND3),...
        faFadMargi(servCou),iN(:,row2),thisInterfMultip(:,row2),...
        IM_DL0(I_ULic(IND3)),visib.noisFl_DL(IND3),...
        wN(IND3,:),Sim.linBud.dop(visib.bund2(IND3,1),fn),...
        I_UL0(I_ULic(IND3)),isSameSitOtSec(:,row2),Sim,...
        satis(:,:,thisDem_IND),servCou,IND2,BHcou,N2(IND3),subs,...
        sitW(IND3,:),LocisCell(IND3,1:servCou),capa,interfSuppr,row3,...
        visib.SINRthresPer_DL(IND3,:),...
        distBordm(LocisCell(IND3,1:servCou)),IM_DL2correctiFacto,...
        residuErr(LocisCell(IND3,1:servCou)));
    
    % Write link budget
    if isDispLinBudThisItera
        beep
        disp('Check this!')
        keyboard
        w_linBud(fn,Sim,visib,Cell(LocisCell(IND3,:)),fam,...
            trafClI,IND3,thisDem(thisDem_IND),IND2c,...
            thisBund2,fs1,SINRthres_DL,trafCl,Un,...
            P(Pic(IND3,:)),sigmaW,...
            Sim.linBud.(pwrAmpClasMS).(situMS),clu,noisRis)

        isDispLinBud = false;
    end

%     L3 = ones(L2,1);
% tmp = mat2cell(satis,BHcou,L1,L3);
% [thisDem.satis] = deal(tmp{:});
% tmp = mat2cell(SINR_DL,BHcou,L1,L3);
% [thisDem.SINR_DL] = deal(tmp{:});
% tmp = mat2cell(assoc,BHcou,L1,L3);
% [thisDem.assoc] = deal(tmp{:});
% [pn sum(c_unserv(Sim,thisDem,studyA,'unserv','per pixel'),1)]
%     disp({pn DataHash(utilis)})
    
    % If coverage check OK, then check if there is enough DL
    % capacity.
    if any(isCov(:))
        v = reshape(full([thisDem(thisDem_IND).v]),...
            [BHcou,size(IND2,1),length(thisDem_IND)]);
        [row2,~] = find(IND2);
        if size(row2,1) == 1
            row2 = row2.';
        end
        thisHour = repmat((1:BHcou).',1,length(thisP));
        dim2Sub = repmat(row2.',BHcou,1);
        dim3Sub = repmat(thisDem_IND.',BHcou,1);
        linearInd2 = sub2ind(size(satis),thisHour,dim2Sub,dim3Sub);
        linearInd3 = linearInd2(isCov);
        thisPi = repmat(1:N,BHcou,1);
        [satis(linearInd3),SINR_DL(linearInd3),assoc(linearInd3),...
            utilis] = u_utilis(...
            satis(linearInd3),SINR_DL(linearInd3),assoc(linearInd3),...
            utilis,isCov,IND3,visib,isCellN,LocisCell,IND2,BHcou,...
            operatoN,isShari,OperatorI,LocOperato,...
            LocisCell(IND3,1:servCou),capa,fn,Sim,trafClI,...
            Mu_DL,maxDist_m,servCou,...
            sitW(IND3,:),v,pwrAmpClasBS(sitW(IND3,1)),thisPi(isCov),...
            thisHour(isCov));
        linearInd3_ = [linearInd3_;linearInd3];
        
        % Populate band and achieved troughput
        if ~isempty(Sim.optimis.repoTput)
            thisDemBand(linearInd3) = fn;
            isCovPer2 = sum(isCovPer(:,:,:),3);
            [~,col] = find(isCov);
            linearInd4 = sub2ind(size(isCovPer2),thisHour(isCov),col);
            tputOveMbps(linearInd3(satis(linearInd3))) = ...
                Sim.optimis.repoTput(isCovPer2(linearInd4(satis(linearInd3)))).';
        end
    end
end

% Pack satis, SINR_DL, and assoc.
[~,~,I3] = ind2sub([BHcou L1 L2],linearInd3_);
I4 = unique(I3);
L3 = ones(length(I4),1);
tmp = mat2cell(satis(:,:,I4),BHcou,L1,L3);
[thisDem(I4).satis] = deal(tmp{:});
tmp = mat2cell(SINR_DL(:,:,I4),BHcou,L1,L3);
[thisDem(I4).SINR_DL] = deal(tmp{:});
tmp = mat2cell(assoc(:,:,I4),BHcou,L1,L3);
[thisDem(I4).assoc] = deal(tmp{:});
tmp = mat2cell(thisDemBand(:,:,I4),BHcou,L1,L3);
[thisDem(I4).band] = deal(tmp{:});
tmp = mat2cell(tputOveMbps(:,:,I4),BHcou,L1,L3);
[thisDem(I4).tputOveMbps] = deal(tmp{:});

% Update utilis
for n = 1 : CellN
    Cell(n).band.utilis(:,:,fn,:) = utilis(:,:,n,:);
%     Cell(n).band.utilisActu(:,:,fn,famI) = ...
%         Cell(n).band.utilisActu(:,:,fn,famI) + (utilisEff(:,:,n,famI) - ...
%         utilisEff_save(:,:,n,famI)) .* 0.9 .* ...
%         Sim.dem.trafVol.(fam).CEtput.servFacto(trafClI) .* ...
%         Sim.dem.trafVol.(fam).CEtput.facto4spaSlot4lowLaten(trafClI);
end

%% Reserve max across cells
% switch fam
%     case 'MBB'
%     case {'uMTC','MTC'}
%         for on = 1 : operatoN  % For each InP
%             IND = find(OperatorI==on);
%             [~,tmp] = max(cell2mat(arrayfun(@(x) sum(x.band.utilis(:,...
%                 :,fn,famI),2),Cell(IND),'UniformOutput',false)),[],2);
%             if length(unique(tmp)) > 1
%                 beep
%                 disp('Code this!')
%                 keyboard
%             end
%             for n = 1 : length(IND)
%                 Cell(IND(n)).band.utilis(:,:,fn,famI) = ...
%                     Cell(IND(tmp(1))).band.utilis(:,:,fn,famI);
%             end
%         end
%     otherwise
%         beep
%         disp('Coding needed?')
%         keyboard
% end
end
