classdef EDGCLOSIT
    % EDGCLOSIT class for management of edge cloud sites
    % The EDGCLOSIT class is responsible for setting the properties of edge
    % cloud sites. More specifically EDGCLOSIT
    % is responsible for delivering the following:
    %
    % *	creates EDGCLOSIT objects
    % * reads existing fixed line exchange databases
    % * plots maps
    % * [TBC]
    
    properties
        ID;  % ID
        DLE_ID;  % Information from the Exchange locations
        DLE_Name;  % Information from the Exchange locations
        Number;  % Information from the Exchange locations
        Address1;  % Information from the Exchange locations
        Address2;  % Information from the Exchange locations
        Town;  % Information from the Exchange locations
        Country;  % Information from the Exchange locations
        Postcode;  % Information from the Exchange locations
        X;  % easting in metres
        Y;  % northing in metres
        I;  % row of the site location within studyA.R
        J;  % column of the site location within studyA.R
        INfoc;  % logical variable that is true if the site is within focus
        cov;  % geographical catchment area
        coreCou;  % number of cores (whole number, i.e. 0, 1, etc.)
        coreCouRe;  % number of cores before quantisation
        workServCou;  % number of working servers
        spServCou;  % number of spare servers
        cabiCou = 0;  % number of cabinets (whole number, i.e. 0, 1, etc.)
        coreReplEver = 4;  % core end-of-life cycle duration
        connTowaCloTyp = [];  % connection towards cloud - type
        connTowaCloID = 0;  % connection towards cloud - ID of edge cloud or 0 for connection to central
        connTowaCloBW = [];  % connection towards cloud - bandwidth
        upg = cell(0,2);  % structure that contains the upgrades this year
        capexBackhauV = zeros(1,10);  % CAPEX backhaul for current and 9 trailing years
        capexServWorkV = zeros(1,10);  % CAPEX working servers for current and 9 trailing years
        capexServSpV = zeros(1,10);  % CAPEX spare servers for current and 9 trailing years
        capexCabiV = zeros(1,10);  % CAPEX cabinets for current and 9 trailing years
        capexSitV = zeros(1,10);  % CAPEX site set up for current and 9 trailing years
        capexLabV = zeros(1,10);  % CAPEX labour for current and 9 trailing years
        capexV = zeros(1,10);  % CAPEX for current and 9 trailing years
        capexPv = 0;  % CAPEX present value
        opexSitRenV = zeros(1,10);  % OPEX site rent for current and 9 trailing years
        opexCabiRenAnUtV = zeros(1,10);  % OPEX cabinet rent for current and 9 trailing years
        opexOperOverhV = zeros(1,10);  % OPEX opertional overhead for current and 9 trailing years
        opexVeSeV = zeros(1,10);  % OPEX vendor services for current and 9 trailing years
        opexLicensiAnMaV = zeros(1,10);  % OPEX licensing and maintainance for current and 9 trailing years
        opexBackhauV = zeros(1,10);  % OPEX backhaul for current and 9 trailing years
        opexV = zeros(1,10);  % OPEX for current and 9 trailing years
        opexPv = 0;  % OPEX present value
        utilisTr;  % utilisation of transport, 0<=x<=1
        workServExpiThisYeaCou;  % Number of working servers expired this year
        spServExpiThisYeaCou;  % Number of spare servers expired this year
    end
    
    methods
%%  EDGCLOSIT class constructor
function edgCloSit = EDGCLOSIT(Sim,studyA)
% EDGCLOSIT is the class constructor of EDGCLOSIT objects, edgCloSit.
% 
% The output edgCloSit is an array of EDGCLOSIT objects that contains the
% following fields:
% ID - 1
% DLE_ID - Field from the BT exchange locations database
% DLE_Name - Field from the BT exchange locations database
% Number - Field from the BT exchange locations database
% Address1 - Field from the BT exchange locations database
% Address2 - Field from the BT exchange locations database
% Town - Field from the BT exchange locations database
% Country - Field from the BT exchange locations database
% Postcode - Field from the BT exchange locations database
% X - 557225
% Y - 5932075
% I - row of the site location within studyA.R
% J - column of the site location within studyA.R
% INfoc - logical variable that is true if the site is within focus
% cov - Logical variable that is true if the site is within focus. It is
%       empty until temporarily populated by p_map with the coordinates of
%       the Voronoi.
% coreCou - Mx2 matrix, where M is the number of years from the start year
%           included until the current year. For example, in 2019,
%           coreCou could be [2017,0; 2018,0; 2019,48], and interpreted as
%           zero cores until 2018 included & 48 cores in 2019.
% coreCouRe - MxN matrix, where M is equal to that of coreCou, and
%             N=1+length(Sim.dem.famUseCase_). The columns are:
%             1) The year, as in coreCou,
%             2:end) The core count before quantisation per service.
% workServCou - Scalar equal to the number of working servers (whole
%               number)
% spServCou - Scalar equal to the number of spare servers (whole number)
% cabiCou - Scalar equal to the number of cabinets (whole number, i.e. 0,
%           1, etc.) 
% coreReplEver - core end-of-life cycle duration
% connTowaCloTyp - []
% connTowaCloID - connection towards cloud - ID of edge cloud or 0 for connection to central
% connTowaCloBW - []
% upg - Cell matrix Mx2, where M is the number of upgrades this year. Below
%       are the expected values:
%       Column A       Column B
%       'sitCou'       The number of edge cloud sites
%       'workServCou'  The number of working servers before the upgrade
%       'spServCou'    The number of spare servers before the upgrade
%       'cabiCou'      The number of cabinets before the upgrade
%       The number of each of these items is stored for upgrade
%       re-assessment, in case in the same year the site is assessed for
%       upgrade multiple times.
% capexBackhauV - CAPEX backhaul for current and 9 trailing years
% capexServWorkV - CAPEX working servers for current and 9 trailing years
% capexServSpV - CAPEX spare servers for current and 9 trailing years
% capexCabiV - CAPEX cabinets for current and 9 trailing years
% capexSitV - CAPEX site set up for current and 9 trailing years
% capexLabV - CAPEX labour for current and 9 trailing years
% capexV - CAPEX for current and 9 trailing years
% capexPv - CAPEX present value
% opexSitRenV - OPEX site rent for current and 9 trailing years
% opexCabiRenAnUtV - OPEX cabinet rent for current and 9 trailing years
% opexOperOverhV - OPEX opertional overhead for current and 9 trailing years
% opexVeSeV - OPEX vendor services for current and 9 trailing years
% opexLicensiAnMaV - OPEX licensing and maintainance for current and 9 trailing years
% opexBackhauV - OPEX backhaul for current and 9 trailing years
% opexV - OPEX for current and 9 trailing years
% opexPv - OPEX present value
% utilisTr - utilisation of transport, 0<=x<=1
% workServExpiThisYeaCou - Number of working servers expired this year
% spServExpiThisYeaCou - Number of spare servers expired this year
%
% * v0.1 KK 20Feb17 Created

if Sim.edgCloSitCou == 0
    edgCloSit = EDGCLOSIT.empty;
    return
end

% Read the Exchange locations
[~,~,raw] = xlsread(Sim.path.exLoc);

% % plot(cell2mat(raw(2:end,9))-409500+nanmean(studyA.Ham.X),...
% %     cell2mat(raw(2:end,10))-287500+nanmean(studyA.Ham.Y),'.')
% % axis equal
% raw(2:end,9) = num2cell(cell2mat(raw(2:end,9)) - 409500 + ...
%     nanmean(studyA.Ham.X));
% raw(2:end,10) = num2cell(cell2mat(raw(2:end,10)) - 287500 + ...
%     nanmean(studyA.Ham.Y));

% Find the BT exchanges within the study area
IN = find(inpolygon(cell2mat(raw(2:end,9)),cell2mat(raw(2:end,10)),...
    studyA.buf.X,studyA.buf.Y));

for n = 1 : length(IN)
    edgCloSit(n) = edgCloSit(1).create_edgCloSit(raw(IN(n)+1,:));
end

% Get rid of duplicate entries
[~,ia] = unique([edgCloSit.X]+1i*[edgCloSit.Y],'stable');
edgCloSit = edgCloSit(ia);

% Find the coordinates of the sites
[I,J] = studyA.R.worldToDiscrete([edgCloSit.X],[edgCloSit.Y]);
tmp = num2cell(studyA.INfoc(sub2ind(size(studyA.INfoc),I,J)));
[edgCloSit.INfoc] = deal(tmp{:});

% Remove randomly until only 6 remain in focus
switch Sim.edgCloSitRemoMeth
    case 'random'
        rng(1801)
        while nnz([edgCloSit.INfoc]) > Sim.edgCloSitCou-1
            edgCloSit(randi(length(edgCloSit))) = [];
        end
        [x,y] = mfwdtran(Sim.mstruct,...
            Sim.edgCloSitForc(1),Sim.edgCloSitForc(2));
        edgCloSit(end+1) = edgCloSit(end);
        edgCloSit(end).X = x;
        edgCloSit(end).Y = y;
        edgCloSit(end).INfoc = true;
        clear x y
    case 'equal'
        % Equal number of macrocells for each edge cloud site, as
        % calculated in:
        % D:\Dropbox (Real Wireless)\5G NORMA RW internal with Ade\07 WP2 -
        % Use cases and economic evaluation\11 CAPisce\02 Investi\edgCloSit
        % loca
        edgCloSit(8+1:end) = []; % edgCloSit(Sim.edgCloSitCou+1:end) = [];
        X = num2cell([528660
            528986
            525375
            525343
            532749
            523174
            529546
            530504]);
        Y = num2cell([180772
            179523
            182750
            181759
            182257
            180102
            178351
            181232]);
        [I,J] = studyA.R.worldToDiscrete([edgCloSit.X],[edgCloSit.Y]);
        tmp = num2cell(studyA.INfoc(sub2ind(size(studyA.INfoc),I,J)));
        [edgCloSit.X] = deal(X{:});
        [edgCloSit.Y] = deal(Y{:});
        [edgCloSit.INfoc] = deal(tmp{:});
    otherwise
        beep
        disp('Code this!')
        keyboard
end

% Updated ID
DLE_Name = {'Finkenwerder1','Finkenwerder2','Uhlenhorst','Veddel',...
    'Moorfleet','Altona-Altstadt','Steinwerder'};
for n = 1 : length(edgCloSit)
    edgCloSit(n).ID = n;
    edgCloSit(n).coreCouRe = [Sim.yea.start zeros(1,length(...
        Sim.dem.famUseCase_))];
    edgCloSit(n).coreCou = [Sim.yea.start 0];
    edgCloSit(n).workServCou = 0;
    edgCloSit(n).spServCou = 0;
    edgCloSit(n).DLE_Name = DLE_Name{n};
end
end

function edgCloSit = create_edgCloSit(edgCloSit,raw)
%% [Title]
% [Description]
%
% * v0.1 KK 20Feb17 Created

edgCloSit.DLE_ID = raw{1};
edgCloSit.DLE_Name = raw{2};
edgCloSit.Number = raw{3};
edgCloSit.Address1 = raw{4};
edgCloSit.Address2 = raw{5};
edgCloSit.Town = raw{6};
edgCloSit.Country = raw{7};
edgCloSit.Postcode = raw{8};
edgCloSit.X = raw{9};
edgCloSit.Y = raw{10};
end

function edgCloSit = upd_connTowaClo(edgCloSit,Cell,Sim)
    %HO: this is updating the edge cloud site objects. It finds the antenna
    %connected to the edge cloud sites and find the transport requirement.
    % A property in the Cell tells if it is connected to an edge cloud site
    % (and which one), Cell.connTowaCloID
    %of the edge cloud site.
    
    
BHcou = size(Sim.dem.hourDistri,1);
for en = 1 : length(edgCloSit)
    % Find antenna sites that are connected to this edge cloud site
    if length(edgCloSit) > 1
        IND = [Cell.connTowaCloID] == edgCloSit(en).ID;
    else
        IND = true(size(Cell));
    end

    utilis = reshape(cell2mat(arrayfun(@(x) squeeze(sum(sum(...
        x.band.utilis,4),2)),Cell(IND),...
        'UniformOutput',false)),[BHcou,4,nnz(IND)]); %HO: this is a simplification. Take the sites that are connected to this edge cloud site. Find all of the utilisation.
    capa = shiftdim(repmat(cell2mat(arrayfun(@(x) x.band.capa.',...
        Cell(IND),'UniformOutput',false)),[1,1,BHcou]),2); % HO: take all the sites that are connected to this edge cloud site.
    AeCou = reshape([Cell(IND).AeCou],4,[]);
    
    c_trReq = @(x,y) max(sum(sum(187.5.*x./20.*shiftdim(repmat(y,...
        [1,1,BHcou]),2)./2.*1e-3,3),2),[],1);  % Gbit/s
    
    if isempty(capa)
        trReq = 0;
        edgCloSit(en).utilisTr = 0;
    else
        trReq = c_trReq(capa,AeCou);  %HO: transport requirements. 
        edgCloSit(en).utilisTr = c_trReq(utilis.*capa,AeCou);
        
        IND2 = [Sim.cost.tr.lim1]<=trReq & trReq<[Sim.cost.tr.lim2]; %HO: Find if the transport requirement is within the transport mode. Find the index
        edgCloSit(en).connTowaCloTyp = Sim.cost.tr(IND2).v{2}; %HO: set the connection toward cloud type ( of the edge cloud site of the apporpriate value.
        edgCloSit(en).connTowaCloBW = Sim.cost.tr(IND2).lim2;
    end
    
    edgCloSit(en).connTowaCloID = 0;
end
end

%%
function edgCloSit = upd_coreCou(edgCloSit,Cell,Sim)
for en = 1 : length(edgCloSit)
    % Find antenna sites that are connected to this edge cloud site
    IND = [Cell.connTowaCloID] == edgCloSit(en).ID;

    % Update coreCou field
    isExpi = (Sim.yea.thisYea >= ...
        edgCloSit(en).coreCou(:,1) + edgCloSit(en).coreReplEver) & ...
        edgCloSit(en).coreCou(:,2) > 0;
    if any(IND)
        tmp = sum(c_coreCou(Cell(IND)),2).';
        coreCouRe = tmp - sum(edgCloSit(en).coreCouRe(~isExpi,2:end),1);
        coreCou = ceil((sum(tmp,2)-sum(sum(edgCloSit(en).coreCou(...
            ~isExpi,2:end),2),1))./Sim.coreCouPerServ) .* ...
            Sim.coreCouPerServ;
    else
        coreCouRe = zeros(1,length(Sim.dem.famUseCase_));
        coreCou = 0;
    end
    if edgCloSit(en).coreCou(end,1) == Sim.yea.thisYea
        % Adding more cores within the same year
        edgCloSit(en).coreCouRe(end,2:end) = ...
            edgCloSit(en).coreCouRe(end,2:end) + coreCouRe;
        edgCloSit(en).coreCou(end,2) = ...
            edgCloSit(en).coreCou(end,2) + coreCou;
    else
        edgCloSit(en).coreCouRe = [edgCloSit(en).coreCouRe
            Sim.yea.thisYea coreCouRe];
        edgCloSit(en).coreCou = [edgCloSit(en).coreCou
            Sim.yea.thisYea coreCou];
        isExpi = [isExpi; false];
    end
    
    % Update workServCou
    workServCou = sum(edgCloSit(en).coreCou(~isExpi,2),1) ./ ...
        Sim.coreCouPerServ;
    if edgCloSit(en).workServCou ~= workServCou
        if ~any(strcmp(edgCloSit(en).upg(:,1),'workServCou'))
            edgCloSit(en).upg = [edgCloSit(en).upg
                {'workServCou',{edgCloSit(en).workServCou}}];
        end
        if sum(edgCloSit(en).workServCou,2) == 0
            edgCloSit(en).upg = [edgCloSit(en).upg
                {'sitCou',{0}}];
        end
        edgCloSit(en).workServCou = workServCou;
    end
    
    % Update spServCou
    spServCou = 0;
    tmp = sum(edgCloSit(en).coreCouRe(~isExpi,2:end),1);
    tmp = tmp ./ sum(tmp,2);
    for n = 1 : length(Sim.dem.famUseCase_)
        if tmp(n) > 0
            spServCou = max(spServCou,Sim.linBud.resi(ceil(...
                workServCou .* tmp(n) ./ sum(tmp,2)),...
                Sim.dem.trafVol.(Sim.dem.famUseCase_{n}).CEtput.resi));
        end
    end
    if edgCloSit(en).spServCou ~= spServCou
        if ~any(strcmp(edgCloSit(en).upg(:,1),'spServCou'))
            edgCloSit(en).upg = [edgCloSit(en).upg
                {'spServCou',{edgCloSit(en).spServCou}}];
        end
        edgCloSit(en).spServCou = spServCou;
    end
    
    % Update cabiCou
    cabiCou = ceil((edgCloSit(en).workServCou + ...
        edgCloSit(en).spServCou) ./ Sim.servCouPerCabi);
    if edgCloSit(en).cabiCou ~= cabiCou
        if ~any(strcmp(edgCloSit(en).upg(:,1),'cabiCou'))
            edgCloSit(en).upg = [edgCloSit(en).upg
                {'cabiCou',{edgCloSit(en).cabiCou}}];
        end
        edgCloSit(en).cabiCou = cabiCou;
    end
end
end

%%
function edgCloSit = c_cost(edgCloSit,Cell,Sim)
IND = [Cell.connTowaCloID] == edgCloSit.ID;
if any(IND)
    [capexPerSitV,capexPerCabiV,capexPerServV] = edgCloSit.c_fixCost(Sim);
    edgCloSit = edgCloSit.c_capex(IND,Sim,Cell,...
        capexPerSitV,capexPerCabiV,capexPerServV);
    edgCloSit = edgCloSit.c_opex(IND,Sim,...
        capexPerSitV,capexPerCabiV,capexPerServV);
end
end

%%
function edgCloSit = c_capex(edgCloSit,IND,Sim,Cell,...
    capexPerSitV,capexPerCabiV,capexPerServV)
% Find the years in which the site is refreshed
isRefre = zeros(1,10);
isRefre(1:edgCloSit.coreReplEver:10) = 1;

% isExpi = Sim.yea.thisYea >= [Cell.expiYea];
% assert(~all(isExpi),'But some sites are offline at the end of the year? If so, then changes are needed within upd_connTowaClo')
edgCloSit_save = edgCloSit;
edgCloSit = edgCloSit.upd_connTowaClo(Cell(IND),Sim);

% Simplifications
edgCloSitRepl = Sim.cost.capex.edgCloSitRepl;

% CAPEX - Backhaul - Equipment
if ~(strcmp(edgCloSit_save.connTowaCloTyp,edgCloSit.connTowaCloTyp) && ...
        edgCloSit_save.connTowaCloBW >= edgCloSit.connTowaCloBW)
    f = 'backhau';

    thisIsRefre = isRefre;
    if ~logical(edgCloSitRepl.(f))
        thisIsRefre(2:end) = 0;
    end

    % Create this cost element
    if ~strcmp(edgCloSit_save.connTowaCloTyp,edgCloSit.connTowaCloTyp)
        % Backhaul type upgrade
        switch Sim.cost.capex.backhauUpgTyp.cost
            case 'new'
                this = Sim.cost.capex.backhauNew(...
                    strcmp({Sim.cost.capex.backhauNew.typ},...
                    edgCloSit.connTowaCloTyp) & ...
                    [Sim.cost.capex.backhauNew.BW_Gbps] == ...
                    edgCloSit.connTowaCloBW);
            otherwise
                dsfdf
        end
    elseif ~(edgCloSit_save.connTowaCloBW==edgCloSit.connTowaCloBW)
        % Backhaul BW upgrade
        this = Sim.cost.capex.backhauUpgBW(...
            [Sim.cost.capex.backhauUpgBW.typFrom] == ...
            edgCloSit_save.connTowaCloBW & ...
            [Sim.cost.capex.backhauUpgBW.typTo] == ...
            edgCloSit.connTowaCloBW);
    end

    edgCloSit.capexBackhauV = this.cost_k .* ...
        (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea) .* ...
        thisIsRefre;
else
    % Check if we tried to downgrade the fibre connection
    if edgCloSit_save.connTowaCloBW > edgCloSit.connTowaCloBW
       edgCloSit.connTowaCloBW = edgCloSit_save.connTowaCloBW; 
    end
    edgCloSit.capexBackhauV = zeros(1,10);
end

% CAPEX - Backhaul - Labour
%     if ~(strcmp(edgCloSit_save.connTowaCloTyp,edgCloSit.connTowaCloTyp) && ...
%             edgCloSit_save.connTowaCloBW == edgCloSit.connTowaCloBW)
    % Create this cost element
%         if ~strcmp(edgCloSit_save.connTowaCloTyp,edgCloSit.connTowaCloTyp)
        % Backhaul type upgrade
%             If going from managed fibre to dark fibre (i.e. transport
%             type upgrade) then the new dark fibre CAPEX (which includes a
%             large "connection cost" i.e. dig and install) would include
%             installation so no extra labour cost needed
%         elseif ~(edgCloSit_save.connTowaCloBW==edgCloSit.connTowaCloBW)
        % Backhaul BW upgrade
%             Dark fibre CAPEX is made up of a connection fee and equipment
%             cost. 
%             OPEX has an annual operations and maintenance charge
%             I'd assume that if we were upgrading an existing dark fibre
%             connection on an edge cloud site to support more BW we would
%             incur the extra equipment (i.e. new modem cost) but the cost
%             of installing this would already be capturing in the annual
%             maintainence we have in the OPEX
%         end
%     end

% CAPEX - Base band - Equipment
assert(edgCloSit.coreCou(end,1)==Sim.yea.thisYea)
f = 'edgCloNodeAnEdgCloSit';
for n = 1 : length(Sim.cost.capex.(f))
    this = Sim.cost.capex.(f)(n);
    
    thisIsRefre = isRefre;
    if ~logical(edgCloSitRepl.(this.unit))
        thisIsRefre(2:end) = 0;
    end
    
    switch this.unit
        case 'server'
            INDupg = strcmp(edgCloSit.upg(:,1),'workServCou');
            if any(INDupg)
                edgCloSit.capexServWorkV = sum(edgCloSit.workServCou - ...
                    edgCloSit.upg{INDupg,2}{1},2) .* capexPerServV .* ...
                    thisIsRefre;
            else
                edgCloSit.capexServWorkV = zeros(1,10);
            end
            
            INDupg = strcmp(edgCloSit.upg(:,1),'spServCou');
            if any(INDupg)
                edgCloSit.capexServSpV = (max(edgCloSit.spServCou,[],2) - ...
                    max(edgCloSit.upg{INDupg,2}{1},[],2)) .* capexPerServV .* ...
                    thisIsRefre;
            else
                edgCloSit.capexServSpV = zeros(1,10);
            end
        case 'cabinet'
            INDupg = strcmp(edgCloSit.upg(:,1),'cabiCou');
            if any(INDupg)
                edgCloSit.capexCabiV = (edgCloSit.cabiCou - ...
                    edgCloSit.upg{INDupg,2}{1}) .* capexPerCabiV .* ...
                    thisIsRefre;
            else
                edgCloSit.capexCabiV = zeros(1,10);
            end
        case 'site'
            INDupg = strcmp(edgCloSit.upg(:,1),'sitCou');
            if any(INDupg)
                edgCloSit.capexSitV = (1 - ...
                    edgCloSit.upg{INDupg,2}{1}) .* capexPerSitV .* ...
                    thisIsRefre;
            else
                edgCloSit.capexSitV = zeros(1,10);
            end
        otherwise
            error('N/A')
    end
end

% CAPEX - Base band - Labour
if edgCloSit.capexServWorkV(1)>0 || edgCloSit.capexServSpV(1)>0
    tmp = 'edgCloSitUpgLab';
    this = Sim.cost.capex.(tmp);
    edgCloSit.capexLabV = this.cost_k .* ...
        (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea) .* isRefre;
else
    edgCloSit.capexLabV = zeros(1,10);
end

edgCloSit.capexV = edgCloSit.capexBackhauV + ...
    edgCloSit.capexServWorkV + edgCloSit.capexServSpV + ...
    edgCloSit.capexCabiV + edgCloSit.capexSitV + edgCloSit.capexLabV;
edgCloSit.capexPv = sum(edgCloSit.capexV./(1+Sim.cost.disc).^(0:10-1));
end

%%
function [capexPerSitV,capexPerCabiV,capexPerServV] = c_fixCost(edgCloSit,Sim)
f = 'edgCloNodeAnEdgCloSit';
for n = 1 : length(Sim.cost.capex.(f))
    this = Sim.cost.capex.(f)(n);
    
    switch this.unit
        case 'server'
            capexPerServV = this.cost_k .* ...
                (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
        case 'cabinet'
            capexPerCabiV = this.cost_k .* ...
                (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
        case 'site'
            capexPerSitV = this.cost_k .* ...
                (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
        otherwise
            error('N/A')
    end
end
end

%%
function edgCloSit = c_opex(edgCloSit,IND,Sim,...
    capexPerSitV,capexPerCabiV,capexPerServV)
thisBackhau = Sim.cost.opex.backhau(strcmp(edgCloSit.connTowaCloTyp,...
    {Sim.cost.opex.backhau.typ}) & ...
    edgCloSit.connTowaCloBW==[Sim.cost.opex.backhau.BW_Gbps]);
this = Sim.cost.opex.edgCloSit;
if any(IND)
    edgCloSit.opexSitRenV = this.ren .* ...
        (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
    edgCloSit.opexCabiRenAnUtV = this.renAnUtPerCabi .* edgCloSit.cabiCou .* ...
        (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
    edgCloSit.opexOperOverhV = this.operOverh .* ...
        (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
    edgCloSit.opexVeSeV = this.veSe .* ...
        (1+this.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
    edgCloSit.opexBackhauV = thisBackhau.cost_k .* ...
        (1+thisBackhau.costVariat).^(Sim.yea.thisYea+(0:10-1)-this.yea);
end
edgCloSit.opexLicensiAnMaV = str2num(this.licensiAnMa(3:4)) ./ 100 .* ...
    (capexPerSitV + capexPerCabiV.*edgCloSit.cabiCou + ...
    capexPerServV .* (sum(edgCloSit.workServCou,2) + ...
    max(edgCloSit.spServCou,[],2)));

edgCloSit.opexV = edgCloSit.opexSitRenV + edgCloSit.opexCabiRenAnUtV + ...
    edgCloSit.opexOperOverhV + edgCloSit.opexVeSeV + ...
    edgCloSit.opexLicensiAnMaV + edgCloSit.opexBackhauV;
edgCloSit.opexPv = sum(edgCloSit.opexV./(1+Sim.cost.disc).^(0:10-1));
end

%%
% function edgCloSit = remove_coreCou(edgCloSit)
% for en = 1 : length(edgCloSit)
%     edgCloSit(en).coreCou = edgCloSit(en).coreCou(1,:);
% end
% end

%%
function edgCloSit = rese(edgCloSit,Sim)
[edgCloSit.upg] = deal(cell(0,2));
[edgCloSit.utilisTr] = deal(0);
[edgCloSit.capexBackhauV] = deal(zeros(1,10));
[edgCloSit.capexServWorkV] = deal(zeros(1,10));
[edgCloSit.capexServSpV] = deal(zeros(1,10));
[edgCloSit.capexCabiV] = deal(zeros(1,10));
[edgCloSit.capexSitV] = deal(zeros(1,10));
[edgCloSit.capexLabV] = deal(zeros(1,10));
[edgCloSit.capexV] = deal(zeros(1,10));
[edgCloSit.capexPv] = deal(0);
[edgCloSit.opexSitRenV] = deal(zeros(1,10));
[edgCloSit.opexCabiRenAnUtV] = deal(zeros(1,10));
[edgCloSit.opexOperOverhV] = deal(zeros(1,10));
[edgCloSit.opexVeSeV] = deal(zeros(1,10));
[edgCloSit.opexLicensiAnMaV] = deal(zeros(1,10));
[edgCloSit.opexBackhauV] = deal(zeros(1,10));
[edgCloSit.opexV] = deal(zeros(1,10));
[edgCloSit.opexPv] = deal(0);

% Adjust the server count to factor in expired cores
for en = 1 : length(edgCloSit)
    isExpi = (Sim.yea.thisYea >= ...
        edgCloSit(en).coreCou(:,1) + edgCloSit(en).coreReplEver) & ...
        edgCloSit(en).coreCou(:,2) > 0;
    workServCou = sum(edgCloSit(en).coreCou(~isExpi,2),1) ./ ...
        Sim.coreCouPerServ;
    edgCloSit(en).workServExpiThisYeaCou = edgCloSit(en).workServCou - workServCou;
    edgCloSit(en).workServCou = workServCou;
    
    spServCou = 0;
    if workServCou > 0
        tmp = sum(edgCloSit(en).coreCouRe(~isExpi,2:end),1);
        tmp = tmp ./ sum(tmp,2);
        for n = 1 : length(Sim.dem.famUseCase_)
            if tmp(n) > 0
                spServCou = max(spServCou,Sim.linBud.resi(ceil(...
                    workServCou .* tmp(n) ./ sum(tmp,2)),...
                    Sim.dem.trafVol.(Sim.dem.famUseCase_{n}).CEtput.resi));
            end
        end
    end
    edgCloSit(en).spServExpiThisYeaCou = edgCloSit(en).spServCou - spServCou;
    edgCloSit(en).spServCou = spServCou;
end
end

%%
function p_map(edgCloSit,studyA,Sim)
% Coverage of edgCloSit by Euclidean distance
[C,V] = voronoi_constra([[edgCloSit.X];[edgCloSit.Y]].',...
    [studyA.X studyA.Y]);
for n = 1 : length(C)
    edgCloSit(n).cov.X = V(C{n},1);
    edgCloSit(n).cov.Y = V(C{n},2);
end
% clear V C n xv yv
    
% Plot map
ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
fh1 = figure('paperpositionmode','auto','unit','in',...
    'pos',[1 1 7+1./16 (7+1./16).*ratio]);
hold on
axis equal off
set(gca,'pos',[0 0 1 1])

IND = [edgCloSit.INfoc];
plot([edgCloSit(IND).X],[edgCloSit(IND).Y],'rx','linewidth',1,'tag','del')
plot([edgCloSit(IND).X],[edgCloSit(IND).Y],'ro','linewidth',1,'tag','del')

for n = 1 : length(edgCloSit)
    plot(edgCloSit(n).cov.X,edgCloSit(n).cov.Y,'r','linewidth',1,'tag','del')
end

plot(studyA.X,studyA.Y,'k','linewidth',1)

% ch1 = colorbar;
% cmap = cell2mat(clu.tran(C+1,11:13));
% set(ch1,'ytick',1:size(cmap,1))
% colormap(cmap./255)
% set(gca,'CLim',[0.5 size(cmap,1)+0.5])

% Add background map
plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)

% Map decorations
add_scaleadd_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

for n = 1 : length(edgCloSit)
    if edgCloSit(n).INfoc
        text(edgCloSit(n).X,edgCloSit(n).Y,[' ' edgCloSit(n).DLE_Name],...
            'fontweight','bold')
    end
end

% print(fh1,fullfile('OutpM','clu'),'-djpeg','-r300')
% savefig(fullfile('OutpM','edgCloSit'))
export_fig(fullfile('OutpM','edgCloSit.jpg'),'-jpg','-q100','-r300')

delete(fh1)

% fprintf(1,'Plotting the clutter (InfoTerra) data...');
% [X,Y] = meshgrid(1:size(clu.A,2),1:size(clu.A,1));
% [X,Y] = clu.R.intrinsicToWorld(X,Y);
% %
% ph1 = patch(repmat(X(:).',5,1) + ...
%     repmat(clu.R.CellExtentInWorldX./2.*[-1 1 1 -1 -1].',1,numel(X)),...
%     repmat(Y(:).',5,1) + ...
%     repmat(clu.R.CellExtentInWorldY./2.*[-1 -1 1 1 -1].',1,numel(Y)),...
%     repmat(clu.A(:).',5,1),'edgecolor','none','facealpha',0.5);
% %
% ch1 = colorbar;
% cmap = cell2mat(clu.tran.pop(2:end,11:13));
% set(ch1,'ytick',1:size(cmap,1))
% colormap(cmap./255)
% set(gca,'CLim',[0.5 size(cmap,1)+0.5])
% print(Sim.fh1,fullfile('OutpM','clu'),'-djpeg','-r300')
% colorbar('off')
% delete(ph1)
% clear X Y ph1 ch1
% fprintf(1,'DONE\n');
end
    end
end
