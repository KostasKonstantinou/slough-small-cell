classdef RESU
    % RESU class for management of the results
    % The RESU class is responsible for creating a cell matrix that
    % contains the results that are printed in csv. More specifically it is
    % responsible for delivering the following:
    %
    % * creates the resu object
    % * calculates the entries required by the csv results file
    %
    % * Kostas Konstantinou Dec 2016
    
    properties
        M = {};  % cell matrix to be written in csv
        List_;  % list of cost categories
        demFoc;  % data volume in focus, in MB over the number of modelled hours
        demBuf;  % data volume in buffer, in MB over the number of modelled hours
        demUnservPerc;  % percentage of unserved demand
        C;
        servPerc;  % percentage of service
        BWperGovernora;  % BW requirement per governorate, 95th percentile
        BWperOslo;  % BW requirement per area, 95th percentile
    end
    
    methods
%% Contructor of the CLASS RESU
function resu = RESU(Sim)
% RESU is the class constructor of RESU object, resu.
% 
% Sim is the SIM ojbect and must contain at least the following fields:
% propag.BS
% stri.opexRaAnUtV
% linBud.freq
% dem
% rul
% yea

if nargin > 0
% Simplification
N2 = length(Sim.propag.BS(:,1));

% List_ contains the cost categories. For simplicity it is first created as
% a variable and then loaded onto resu.
List_ = {};
List_(end+1,:) = {'Site civil works and acquisition','Antenna Site','capexCivWoAnAcqV','CAPEX','number of sites','per site'};
List_(end+1,:) = {'Site rental','Antenna Site','opexRenV','OPEX','number of sites','per site'};
List_(end+1,:) = {Sim.stri.opexRaAnUtV,'Antenna Site','opexRaAnUtV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Vendor services','Antenna Site','opexVeSeV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Licensing and maintenance','Antenna Site','opexLicensiAnMaV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Antennas/feeder','Antenna Site','capexAntenFeedeV','CAPEX','number of sites','per site'};
List_(end+1,:) = {'RF front end','Antenna Site','capexRFfronEndV','CAPEX','number of sites','per site'};
List_(end+1,:) = {'Bare metal base band','Antenna Site','capexBBbareMetaV','CAPEX','number of baseline BBU','per BBU'};
List_(end+1,:) = {'Edge node processing','Antenna Site','capexBBedgNodV','CAPEX','number of cores','per core'};
List_(end+1,:) = {'Labour','Antenna Site','capexLabV','CAPEX','number of sites','per site'};
List_(end+1,:) = {'Transport CAPEX','Antenna Site','capexBackhauV','CAPEX','number of sites','per site'};
List_(end+1,:) = {'Transport OPEX','Antenna Site','opexBackhauV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Working servers','Edge Cloud','capexServWorkV','CAPEX','number of servers','per server'};
List_(end+1,:) = {'Spare servers','Edge Cloud','capexServSpV','CAPEX','number of servers','per server'};
List_(end+1,:) = {'Cabinets','Edge Cloud','capexCabiV','CAPEX','number of cabinets','per cabinet'};
List_(end+1,:) = {'Edge cloud sites','Edge Cloud','capexSitV','CAPEX','number of sites','per site'};
List_(end+1,:) = {'Site rental','Edge Cloud','opexSitRenV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Cabinet rent and utilities','Edge Cloud','opexCabiRenAnUtV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Operating overhead','Edge Cloud','opexOperOverhV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Vendor services','Edge Cloud','opexVeSeV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Licensing and maintenance','Edge Cloud','opexLicensiAnMaV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Labour','Edge Cloud','capexLabV','CAPEX','number of sites','per site'};
List_(end+1,:) = {'Transport CAPEX','Edge Cloud','capexBackhauV','CAPEX','number of sites','per site'};
List_(end+1,:) = {'Transport OPEX','Edge Cloud','opexBackhauV','OPEX','number of sites','per site'};
List_(end+1,:) = {'Working servers','Central Cloud',[],[],[],[]};
List_(end+1,:) = {'Spare servers','Central Cloud',[],[],[],[]};
List_(end+1,:) = {'Transport','Central Cloud',[],[],[],[]};

% Update the List_ property
resu.List_ = List_;

% Header line - void
m = 1;
% resu.M{m,1} = 'Site Configuration';
% resu.M{m,2} = 'Bands';
% resu.M{m,3} = 'Antennas';

% Header line - void
m = m + 1;
% resu.M{m,2} = Cell(1).band.capa;
% resu.M{m,3} = Cell(1).AeCou;

% Section 1 - 3.01
for n1 = {'new','upgraded','refreshed','decommissioned'}
    for n2 = 1 : N2
        m = m + 2;
        resu.M{m,1} = ['Number of units of equipment ' n1{1} ' - ' ...
            Sim.propag.BS{n2,1}];
        %Adding two lines,for year and operator
        m = m + 2;
        IND = strcmp(List_(:,2),'Antenna Site');
        resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
        resu.M(m+1:m+nnz(IND),2) = List_(IND,5);
        m = m + nnz(IND);
    end
    %
    m = m + 2;
    resu.M{m,1} = ['Number of units of equipment ' n1{1} ' - Edge Cloud'];
    %Adding two lines,for year and operator
    m = m + 2;
    IND = strcmp(List_(:,2),'Edge Cloud');
    resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
    resu.M(m+1:m+nnz(IND),2) = List_(IND,5);
    m = m + nnz(IND);
end

% Section 3.02
for n2 = 1 : N2
    m = m + 2;
    resu.M{m,1} = ['Installed base - ' Sim.propag.BS{n2,1}];
    %Adding two lines,for year and operator
    m = m + 2;
    IND = strcmp(List_(:,2),'Antenna Site');
    resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
    resu.M(m+1:m+nnz(IND),2) = List_(IND,5);
    m = m + nnz(IND);
end
%
m = m + 2;
resu.M{m,1} = 'Installed base - Edge Cloud';
%Adding two lines,for year and operator
m = m + 2;
IND = strcmp(List_(:,2),'Edge Cloud');
resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
resu.M(m+1:m+nnz(IND),2) = List_(IND,5);
m = m + nnz(IND);

% Section 3.1 : Number of sites active before optimisation loop

% Section 3.2 : Number of sites after optimisation loop
m = m + 2;
resu.M{m,1} = 'Number of sites after optimisation loop';
%Adding two lines,for year and operator
m = m + 2;
%Adding Active
resu.M(m+1:m+length(Sim.propag.BS(:,1)),1) = cellfun(@(x) [x ' - Active'],...
    Sim.propag.BS(:,1),'UniformOutput',false);
m = m + length(Sim.propag.BS(:,1));
%Adding New
resu.M(m+1:m+length(Sim.propag.BS(:,1)),1) = cellfun(@(x) [x ' - New'],...
    Sim.propag.BS(:,1),'UniformOutput',false);
m = m + length(Sim.propag.BS(:,1));
%Adding Upgraded
resu.M(m+1:m+length(Sim.propag.BS(:,1)),1) = cellfun(@(x) [x ' - Upgraded'],...
    Sim.propag.BS(:,1),'UniformOutput',false);
m = m + length(Sim.propag.BS(:,1));
%Adding Refreshed
resu.M(m+1:m+length(Sim.propag.BS(:,1)),1) = cellfun(@(x) [x ' - Refreshed'],...
    Sim.propag.BS(:,1),'UniformOutput',false);
m = m + length(Sim.propag.BS(:,1));

% Section 3.5
m = m + 2;
resu.M{m,1} = 'Total network expenditure per year  (k�)';
% Adding one line,for the results
m = m + 1;
% Adding two lines,for year and operator
m = m + 2;

% Section 3.6
% m = m + 2;
% resu.M{m,1} = 'Present Value (PV)  (k�)';
% % Adding one line,for the results
% m = m + 1;
% % Adding two lines,for year and operator
% m = m + 2;

% Section 4: Average Opex per unit of equipment
for n1 = {'new','upg','refreshe','decom','installeBas'}
    for n2 = 1 : N2
        m = m + 2;
        resu.M{m,1} = ['Average OPEX per unit of equipment  (k�) - ' ...
            Sim.propag.BS{n2,1} ' - ' n1{:}];
        %Adding two lines,for year and operator
        m = m + 2;
        IND = strcmp(List_(:,2),'Antenna Site');
        resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
        resu.M(m+1:m+nnz(IND),2) = List_(IND,6);
        m = m + nnz(IND);
    end
    %
    m = m + 2;
    resu.M{m,1} = ['Average OPEX per unit of equipment  (k�) - Edge Cloud - ' n1{:}];
    %Adding two lines,for year and operator
    m = m + 2;
    IND = strcmp(List_(:,2),'Edge Cloud');
    resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
    resu.M(m+1:m+nnz(IND),2) = List_(IND,6);
    m = m + nnz(IND);
end

% Section 5: Average Capex per unit of equipment
for n1 = {'new','upg','refreshe','decom','installeBas'}
    for n2 = 1 : N2
        m = m + 2;
        resu.M{m,1} = ['Average CAPEX per unit of equipment  (k�) - ' ...
            Sim.propag.BS{n2,1} ' - ' n1{:}];
        %Adding two lines,for year and operator
        m = m + 2;
        IND = strcmp(List_(:,2),'Antenna Site');
        resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
        resu.M(m+1:m+nnz(IND),2) = List_(IND,6);
        m = m + nnz(IND);
    end
    %
    m = m + 2;
    resu.M{m,1} = ['Average CAPEX per unit of equipment  (k�) - Edge Cloud - ' n1{:}];
    %Adding two lines,for year and operator
    m = m + 2;
    IND = strcmp(List_(:,2),'Edge Cloud');
    resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
    resu.M(m+1:m+nnz(IND),2) = List_(IND,6);
    m = m + nnz(IND);
end

% Section 5.5
for n2 = 1 : N2
    m = m + 2;
    resu.M{m,1} = ['Average CAPEX per unit of equipment of installed base if that was to be installed this year  (k�) - ' Sim.propag.BS{n2,1}];
    %Adding two lines,for year and operator
    m = m + 2;
    IND = strcmp(List_(:,2),'Antenna Site');
    resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
    resu.M(m+1:m+nnz(IND),2) = List_(IND,6);
    m = m + nnz(IND);
end
%
m = m + 2;
resu.M{m,1} = 'Average CAPEX per unit of equipment of installed base if that was to be installed this year  (k�) - Edge Cloud';
%Adding two lines,for year and operator
m = m + 2;
IND = strcmp(List_(:,2),'Edge Cloud');
resu.M(m+1:m+nnz(IND),1) = List_(IND,1);
resu.M(m+1:m+nnz(IND),2) = List_(IND,6);
m = m + nnz(IND);

% Section 6
for str1 = {'Average','Peak'}
    m = m + 2;
    resu.M{m,1} = [str1{:} ' bandwidth utilisation of spectrum in the busy hour of each site  (%)'];
    %Adding three lines,for year,operator and generic service
    for n = 1 : length(Sim.linBud.freq)
        m = m + 3;
        resu.M(m+1:m+length(Sim.propag.BS(:,1)),1) = cellfun(@(x) [x ' - ' ...
            Sim.linBud.freq{n}],Sim.propag.BS(:,1),...
            'UniformOutput',false);
    end
    
    m = m + 3;
end
clear str1

% Section 7
m = m + 2;
resu.M{m,1} = 'Utilisation of processors in the busy hour of each edge cloud site  (%)';
m = m + 1;
%Adding three lines,for year,operator and generic service
m = m + 3;

m = m + 2;
resu.M{m,1} = 'Transport requirements in the busy hour of each edge cloud site  (Gbit/s)';
m = m + 1;
%Adding three lines,for year,operator and generic service
m = m + 3;

m = m + 2;
resu.M{m,1} = 'Total number of servers across edge cloud sites';
m = m + 1;
%Adding three lines,for year,operator and generic service
m = m + 3;

m = m + 2;
resu.M{m,1} = 'Total number of servers across antenna sites';
m = m + 6;  % Title, year, operator, service, data, blank line

resu.M{m,1} = 'Demand to be served (in focus) in MB in each modelled hour';
m = m + 4;  % Title, year, operator, service
resu.M(m+(0:size(Sim.dem.hourDistri,1)-1),1) = ...
    strcat(Sim.dem.hourDistri(:,1),{' '},Sim.dem.hourDistri(:,2));
m = m + size(Sim.dem.hourDistri,1);  % Data
m = m + 1;  % Blank line

resu.M{m,1} = 'Demand to be served in buffer in MB in each modelled hour';
m = m + 4;  % Title, year, operator, service
resu.M(m+(0:size(Sim.dem.hourDistri,1)-1),1) = ...
    strcat(Sim.dem.hourDistri(:,1),{' '},Sim.dem.hourDistri(:,2));
m = m + size(Sim.dem.hourDistri,1);  % Data
m = m + 1;  % Blank line

resu.M{m,1} = 'Unserved demand in % over total';
m = m + 4;  % Title, year, operator, service
resu.M(m+(0:size(Sim.dem.hourDistri,1)-1),1) = ...
    strcat(Sim.dem.hourDistri(:,1),{' '},Sim.dem.hourDistri(:,2));
m = m + size(Sim.dem.hourDistri,1);  % Data
m = m + 1;  % Blank line

resu.M{m,1} = 'Breakdown of sites by specification';
m = m + 1;
%Adding 2 lines,for year,operator
m = m + 2;
tmp = arrayfun(@(x) x.upg,Sim.rul);
tmp2 = arrayfun(@(x) x.pwrAmpClas,Sim.rul,'UniformOutput',false);
[tmp.pwrAmpClas] = deal(tmp2{:});
Hash = arrayfun(@(x) DataHash(x),tmp,'UniformOutput',false);
[resu.C,ia,~] = unique(Hash,'stable');
for n = 1 : length(ia)
    resu.M{m,1} = ['pwrAmpClas:' tmp(ia(n)).pwrAmpClas ' ' ...
        'sectoCou:' num2str(Sim.rul(ia(n)).upg.sectoCou) ' ' ...
        'sitChainConfig:' num2str(Sim.rul(ia(n)).upg.sitChainConfig) ' ' ...
        'AeCou:[' num2str(Sim.rul(ia(n)).upg.AeCou) '] ' ...
        'Band:[' num2str(Sim.rul(ia(n)).upg.Band) ']'];
    m = m + 1;
end
end
end

%% Prepare the results for the current network and year
function resu = prep_results(resu,Cell,edgCloSit,Sim)
% prep_results prepares the results resu for the current network and year
%
% Cell is an array of CELL objects. [A list of the required fields is pending]
% edgCloSit is an array of EDGCLOSIT objects. [A list of the required fields is pending]
% Sim is the SIM object. [A list of the required fields is pending]
% 
% * v0.1 Tasos Karousos Nov 2016
% * v0.2 Kostas Konstantinou Dec 2016

% Simplification
BHcou = size(Sim.dem.hourDistri,1);  % Number of busy hours

% The column where the results should be placed for this year
yeaIND = find(Sim.yea.thisYea == Sim.yea.start:Sim.yea.fin);

% The total number of operators
oN = length(Sim.dem.operators);

% The total number of generic services
fN = length(Sim.dem.famUseCase_);

% All the cells
% cellID = 1 : length(Cell);

% Simplifications
isUpg = [Sim.rul.isUpg];
isDecomm = [Sim.rul.isDecomm];
CellIsExpi = Sim.yea.thisYea >= [Cell.expiYea];
Operator = {Cell.Operator};
pwrAmpClas = {Cell.pwrAmpClas};
Cell_INfoc = [Cell.INfoc];
rul = [Cell.rul];
N2 = length(Sim.propag.BS(:,1));
edgCloSit_INfoc = find([edgCloSit.INfoc]);
coreCou = arrayfun(@(x) x.coreCou(:,2),edgCloSit,'UniformOutput',false);
coreCouRe = arrayfun(@(x) x.coreCouRe(:,2:end),edgCloSit,...
    'UniformOutput',false);
tmp = arrayfun(@(x) x.coreCou(:,1),edgCloSit,'UniformOutput',false);
tmp{end+1} = [Sim.yea.start:Sim.yea.thisYea].';
if length(tmp) > 1
    assert(isequal(tmp{:}))
end
if any(arrayfun(@(x) size(x.coreCou,1)>0,edgCloSit))
    edgCloSitCoreIsExpi = arrayfun(@(x) (Sim.yea.thisYea >= ...
        x.coreCou(:,1) + x.coreReplEver) & ...
        x.coreCou(:,2)>0,edgCloSit,'UniformOutput',false);
else
    edgCloSitCoreIsExpi = {};
end

% Find the busy hour
% IND = ~CellIsExpi & [Cell.connTowaCloID]>0;
% if any(IND)
%     utilisEffAll = reshape(cell2mat(arrayfun(@(x) squeeze(sum(sum(...
%         x.band.utilis,4),2)),Cell(IND),'UniformOutput',false)),...
%         [BHcou,4,nnz(IND)]);
%     capa = shiftdim(repmat(cell2mat(arrayfun(@(x) x.band.capa.',...
%         Cell(IND),'UniformOutput',false)),[1,1,BHcou]),2);
%     [~,busyHour] = max(sum(sum(utilisEffAll.*capa,2),3) ./ ...
%         sum(sum(capa,2),3),[],1);
% end
% clear capa IND utilisEffAll

% Calculate the load on each edge cloud site by service provider
a = cell2mat(cellfun(@(x,y) sum(x(~y,:),1),...
    coreCouRe.',edgCloSitCoreIsExpi.','UniformOutput',false));
a = bsxfun(@rdivide,a,sum(a,2));
% a = zeros(length(edgCloSit),oN);
% for n1 = 1 : oN
%     for en = 1 : length(edgCloSit)
%         IND = ~CellIsExpi & [Cell.connTowaCloID] == edgCloSit(en).ID;
%         
%         if any(IND)
%             utilis = cell2mat(arrayfun(@(x) squeeze(sum(x.band.utilis(...
%                 busyHour,n1,:,:),4)),Cell(IND),'UniformOutput',false));
%             utilisEffAll = cell2mat(arrayfun(@(x) squeeze(sum(sum(x.band.utilis(...
%                 busyHour,:,:,:),4),2)),Cell(IND),'UniformOutput',false));
% 
%             a(en,n1) = sum(utilis(:)) ./ sum(utilisEffAll(:));
%         end
%     end
% end
% clear n1 en IND utilis utilisEffAll

% For simplification in writing output, assume that each service provider
% has only one type of service
if fN == 1
    fO = true(oN,1);
else
    [X,Y] = meshgrid(1:fN,1:oN);
    fO = reshape(cellfun(@(x,y) Sim.dem.trafVol.(x).subPenet.(y),...
        Sim.dem.famUseCase_(X(:)),Sim.dem.operators(Y(:))),oN,fN);
    fO = arrayfun(@(x) ~all(x.subPerc(:,x.year==Sim.yea.thisYea)==0),fO);
    if any( sum( fO, 1 ) >= 2 )
        beep
        disp('Check how a should change!')
        keyboard
    end
    clear X Y
end

% For each operator find the relative equipment count,cost and utilisations
IND2 = [edgCloSit.INfoc] & cellfun(@(x) sum(x,1),coreCou)>0;
% for n1 = 1 : max(oN,3)
for n1 = 1 : oN
    if n1==3 && n1>oN
        continue
    end
    
    m = 2;
    
    % The column in the report for the current operator and year
    curCol1 = (yeaIND-1)*oN + n1 + 2;
       
    %% Section 1: Number of new units of equipment deployed
    for n2 = 1 : N2
        m = m + 3;
        if Sim.yea.isF && Sim.isShufNsitEstablishedDatOnStartYea
            IND = find(Cell_INfoc & ...
                strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
                strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        else
            IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
                strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
                strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        end
        IND3 = find(rul(IND)>0);
        rul2 = rul(IND(IND3));
        IND = IND(IND3(~isUpg(rul2)&~isDecomm(rul2)));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','any',@nnz,'new');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','any',@nnz,'new',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end
    
    %% Section 2: Number of units of equipment upgraded
    for n2 = 1 : N2
        m = m + 3;
        IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        IND3 = find(rul(IND)>0);
        rul2 = rul(IND(IND3));
        IND = IND(IND3(isUpg(rul2)&~isDecomm(rul2)));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','any',@nnz,'upg');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','any',@nnz,'upg',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end
       
    %% Section 3: Number of units of equipment refreshed
    for n2 = 1 : N2
        m = m + 3;
        IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        IND3 = find(rul(IND)>0);
        rul2 = rul(IND(IND3));
        IND = IND(IND3(isDecomm(rul2)));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','any',@nnz,'refreshe');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','any',@nnz,'refreshe',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end
    
    %% Section 3.01: Number of units of equipment decommissioned
    m = m + 3*(nnz(strcmp(resu.List_(:,2),'Antenna Site'))+4);
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','any',@nnz,'decom',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end

    %% Section 3.02: Installed base
    for n2 = 1 : N2
        m = m + 3;
        IND = Cell_INfoc & [Cell.expiYea]>Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas);
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','any',@length,'installeBas');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','any',@length,'installeBas',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end
    
    %% Section 3.1: Number of sites active before optimisation loop
%     for n2 = 1:N2
%         IND = find(strcmp(Sim.propag.BS{n2,1},pwrAmpClas) & ~CellIsExpi & ...
%             strcmp(Operator,Sim.dem.operators{n1}) & INfoc);
%         IND3 = rul(IND) > 0;
%         rul2 = rul(IND(IND3));
%         
%         % Number of sites active
%         length(IND)
%     end
    
    %% Section 3.2: Number of sites after optimisation loop
    % This year
    m = m + 3;
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    for n2 = 1 : N2
        IND = find(strcmp(Sim.propag.BS{n2,1},pwrAmpClas) & ~CellIsExpi & ...
            strcmp(Operator,Sim.dem.operators{n1}) & Cell_INfoc);
        IND3 = find(rul(IND)>0);
        rul2 = rul(IND(IND3));
        
        % Number of sites active
        resu.M{m+N2*0+n2,curCol1} = length(IND);
        
        % Number of sites new
        resu.M{m+N2*1+n2,curCol1} = nnz(IND3(~isUpg(rul2)&~isDecomm(rul2)));
        
        % Number of sites upgraded
        resu.M{m+N2*2+n2,curCol1} = nnz(IND3(isUpg(rul2)&~isDecomm(rul2)));
        
        % Number of sites refreshed this year
        resu.M{m+N2*3+n2,curCol1} = nnz(IND3(isDecomm(rul2)));
    end

    %% Section 3.5: Total network expenditure per year � this is calculated as the total capex and opex of the solution over the system lifetime (assumed to be 7 years 2014-2020) without discounts
    % This year
    m = m + N2*4 + 3;
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    
    % Total network expenditure per year
    m = m + 1;
    IND1 = Cell_INfoc & [Cell.expiYea]>Sim.yea.thisYea & ...
        strcmp(Sim.dem.operators{n1},{Cell.Operator});
    tmp = sum(arrayfun(@(x) x.capexV(1),Cell(IND1))) + ...
        sum(arrayfun(@(x) x.opexV(1),Cell(IND1)));
    if ~isempty(edgCloSit)
        tmp = tmp + ...
            arrayfun(@(x) x.capexV(1),edgCloSit(IND2)) * a(IND2,n1) + ...
            arrayfun(@(x) x.opexV(1),edgCloSit(IND2)) * a(IND2,n1);
    end
    resu.M{m,curCol1} = tmp;
    
    %% Section 3.6: Average Opex per unit of equipment - new
    for n2 = 1 : N2
        m = m + 3;
        IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        IND3 = find(rul(IND)>0);
        rul2 = rul(IND(IND3));
        IND = IND(IND3(~isUpg(rul2)&~isDecomm(rul2)));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','OPEX',@mean,'new');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','OPEX',@mean,'new',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end

    %% Section 3.7: Average Opex per unit of equipment - upgraded
    for n2 = 1 : N2
        m = m + 3;
        IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        IND3 = find(rul(IND)>0);
        rul2 = rul(IND(IND3));
        IND = IND(IND3(isUpg(rul2)&~isDecomm(rul2)));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','OPEX',@mean,'upg');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','OPEX',@mean,'upg',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end

    %% Section 3.8: Average Opex per unit of equipment - refreshed
    for n2 = 1 : N2
        m = m + 3;
        IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        IND3 = find(rul(IND)>0);
        rul2 = rul(IND(IND3));
        IND = IND(IND3(isDecomm(rul2)));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','OPEX',@mean,'refreshe');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','OPEX',@mean,'refreshe',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end

    %% Section 3.9: Average Opex per unit of equipment - decommissioned
    m = m + 3*(nnz(strcmp(resu.List_(:,2),'Antenna Site'))+4);
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        IND = [];
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND),n1,Sim,curCol1,...
            'Edge Cloud','OPEX',@mean,'decom',...
            {coreCou(IND),edgCloSitCoreIsExpi(IND),a(IND,n1)});
    end

    %% Section 4: Average Opex per unit of equipment - Installed base
    for n2 = 1 : N2
        m = m + 3;
        IND = Cell_INfoc & [Cell.expiYea]>Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas);
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','OPEX',@mean,'installeBas');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','OPEX',@mean,'installeBas',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end
    
    %% Section 4.1: Average Capex per unit of equipment - new
    for n2 = 1 : N2
        m = m + 3;
        IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        IND3 = rul(IND) > 0;
        rul2 = rul(IND(IND3));
        IND = IND(~isUpg(rul2)&~isDecomm(rul2));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','CAPEX',@mean,'new');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','CAPEX',@mean,'new',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end

    %% Section 4.2: Average Capex per unit of equipment - upgraded
    for n2 = 1 : N2
        m = m + 3;
        IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        IND3 = rul(IND) > 0;
        rul2 = rul(IND(IND3));
        IND = IND(isUpg(rul2)&~isDecomm(rul2));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','CAPEX',@mean,'upg');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','CAPEX',@mean,'upg',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end

    %% Section 4.3: Average Capex per unit of equipment - refreshed
    for n2 = 1 : N2
        m = m + 3;
        IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas));
        IND3 = rul(IND) > 0;
        rul2 = rul(IND(IND3));
        IND = IND(isDecomm(rul2));
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','CAPEX',@mean,'refreshe');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','CAPEX',@mean,'refreshe',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end

    %% Section 4.4: Average Capex per unit of equipment - decommissioned
    m = m + 3*(nnz(strcmp(resu.List_(:,2),'Antenna Site'))+4);
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','CAPEX',@mean,'decom',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end

    %% Section 5: Average Capex per unit of equipment - Installed base
    for n2 = 1 : N2
        m = m + 3;
        IND = Cell_INfoc & [Cell.expiYea]>Sim.yea.thisYea & ...
            strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
            strcmp(Sim.propag.BS{n2,1},pwrAmpClas);
        [resu,m] = resu.upd_results_1(m,Cell(IND),n1,Sim,curCol1,...
            'Antenna Site','CAPEX',@mean,'installeBas');
    end
    m = m + 3;
    if isempty(edgCloSit)
        m = m + 13;
    else
        [resu,m] = resu.upd_results_1(m,edgCloSit(IND2),n1,Sim,curCol1,...
            'Edge Cloud','CAPEX',@mean,'installeBas',...
            {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
    end
    
    %% Section 5.5: Average CAPEX per unit of equipment of installed base if that was to be installed this year  (k�) - macrocell
    if Sim.yea.isF
        for n2 = 1 : N2
            m = m + 3;
            IND = Cell_INfoc & [Cell.expiYea]>Sim.yea.thisYea & ...
                strcmp(Sim.dem.operators{n1},{Cell.Operator}) & ...
                strcmp(Sim.propag.BS{n2,1},pwrAmpClas);
            Cell2 = Cell(IND);
            for n = 1 : length(Cell2)
                capexRFfronEndV = Cell2(n).c_capexRFfronEndV(Sim);
                capexBBbareMetaV = Cell2(n).c_capexBBbareMetaV(Sim);
                Cell2(n).upg = {};
                Cell2(n) = Cell2(n).c_capex(Sim,capexRFfronEndV,capexBBbareMetaV,'new');
            end
            [resu,m] = resu.upd_results_1(m,Cell2,n1,Sim,curCol1,...
                'Antenna Site','CAPEX',@mean,'installeBas');
        end
        m = m + 3;

        if isempty(edgCloSit)
            m = m + 13;
        else
            edgCloSit2 = edgCloSit(IND2);
            for n = 1 : length(edgCloSit2)
                edgCloSit2(n).coreCou = 0;
                edgCloSit2(n).connTowaCloTyp = [];
                edgCloSit2(n) = edgCloSit2(n).c_cost(Cell,Sim);
            end
            [resu,m] = resu.upd_results_1(m,edgCloSit2,n1,Sim,curCol1,...
                'Edge Cloud','CAPEX',@mean,'installeBas',...
                {coreCou(IND2),edgCloSitCoreIsExpi(IND2),a(IND2,n1)});
            clear capexRFfronEndV capexRFfronEnd Cell2 n edgCloSit2
        end
    else
        m = m + 3*(nnz(strcmp(resu.List_(:,2),'Antenna Site'))+4);
        m = m + 3;
        m = m + nnz(strcmp(resu.List_(:,2),'Edge Cloud')) + 1;
    end
    
    % The current slice of the tenant
    fn = find(fO(n1,:));
    
    %% Section 6: Average and peak BW utilisation per busy hour per operator
    % 'utilisActu',zeros(BHcou,oN,length(BW),length(Sim.dem.famUseCase_))
    for fun = {'mean','peak'}
        m = m + 3;
        if n1 == 1
            resu.M{m,curCol1} = Sim.yea.thisYea;
        end
        % Current operator
        m = m + 1;
        resu.M{m,curCol1} = Sim.dem.operators{n1};
        m = m + 1;
        
        for n2 = 1 : N2
            IND = strcmp(Sim.propag.BS{n2,1},pwrAmpClas) & ~CellIsExpi & ...
                strcmp(Operator,Sim.dem.operators{n1}) & Cell_INfoc;
            
            % The current generic service
            if ~isempty(fn)
                resu.M{m,curCol1} = Sim.dem.famUseCase_{fn};
            end
            
            % Update result
            % zeros(BHcou,oN,length(BW),length(Sim.dem.famUseCase_))
            tmp2 = m + n2 + (1:N2:N2*length(Sim.linBud.freq)) - 1;
            if any(IND)
                utilis = reshape(cell2mat(arrayfun(@(x) squeeze(sum(...
                    x.band.utilis(:,:,:,fn),2)),Cell(IND),...
                    'UniformOutput',false)),...
                    [BHcou,length(Sim.linBud.freq),nnz(IND)]);
                capa = shiftdim(repmat(cell2mat(arrayfun(@(x) x.band.capa.',...
                    Cell(IND),'UniformOutput',false)),[1,1,BHcou]),2);
                switch fun{:}
                    case 'mean'
                        tmp = max(sum(utilis.*capa,3)./sum(capa,3),[],1);
                    case 'peak'
                        tmp = shiftdim(max(utilis,[],1),1).';
                        tmp = pctile(tmp,0.95,'nearest');
                end
                resu.M(tmp2,curCol1) = num2cell(tmp);
                resu.M(tmp2(isnan(tmp)),curCol1) = {'N/A'};
            else
                resu.M(tmp2,curCol1) = {'N/A'};
            end
        end        
        
        m = m + length(Sim.linBud.freq)*N2;
    end
    
    %% Find how much is the served demand per operator
%     for n2 = 1 : N2
%         sum(c_unserv(Sim,thisDem,studyA,'serv','per site','MB/day',...
%             Cell,strcmp(Sim.propag.BS{n2,1},pwrAmpClas) & ...
%             ~CellIsExpi & strcmp(Operator,Sim.dem.operators{n1})))
%     end
    
    %% Section 7: Utilisation of processors in the busy hour of each edge cloud site
    m = m + 3;
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    m = m + 1;
     
    % The current generic service
    if ~isempty(fn)
        resu.M{m,curCol1} = Sim.dem.famUseCase_{fn};
    end
    
    % Update result
    % zeros(BHcou,oN,length(BW),length(Sim.dem.famUseCase_))
    % This paragraph has been commented out because the calculation is
    % invalid.
    % Beware that cellProce does not scale with utilisation.
    % A separate function has been included in this class to write a csv of
    % utilisation per edge cloud site.
%     utilis = zeros(length(edgCloSit_INfoc),1);
%     if ~isempty(fn)
%         for en = 1 : length(edgCloSit_INfoc)
%             IND = ~CellIsExpi & [Cell.connTowaCloID] == edgCloSit(edgCloSit_INfoc(en)).ID;
% 
%             if any(IND)
%                 utilis = reshape(cell2mat(arrayfun(@(x) squeeze(x.band.utilis(...
%                     :,n1,:,fn)),Cell(IND),'UniformOutput',false)),...
%                     [BHcou,4,nnz(IND)]);
%                 capa = shiftdim(repmat(cell2mat(arrayfun(@(x) x.band.capa.',...
%                     Cell(IND),'UniformOutput',false)),[1,1,BHcou]),2);
%                 utilis(edgCloSit_INfoc(en)) = max(sum(sum(utilis.*capa,2),3) ./ ...
%                     sum(sum(capa,2),3),[],1);
%             end
%         end
%     end
%     resu.M{m+1,curCol1} = mean(utilis);
    
    m = m + 1;
    
    %% Section 7: Transport requirements in the busy hour of each edge cloud site  (Gbit/s)
    m = m + 3;
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    m = m + 1;
        
    if ~isempty(fn)
        resu.M{m,curCol1} = Sim.dem.famUseCase_{fn};
    end
    
    if ~isempty(edgCloSit)
        resu.M{m+1,curCol1} = mean([edgCloSit(edgCloSit_INfoc).utilisTr] .* ...
            a(edgCloSit_INfoc,n1).');
    end
    
    clear curCol2
    m = m + 1;
    
    %% Total number of cores across edge cloud sites
    m = m + 3;
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    m = m + 1;
    if ~isempty(fn)
        resu.M{m,curCol1} = Sim.dem.famUseCase_{fn};
    end
    m = m + 1;

    resu.M{m,curCol1} = sum(cellfun(@(x,y) sum(x(~y),1),...
        coreCou(edgCloSit_INfoc),edgCloSitCoreIsExpi(edgCloSit_INfoc)),2);
    
    %% Total number of cores across antenna sites
    m = m + 3;
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    m = m + 1;
    if ~isempty(fn)
        resu.M{m,curCol1} = Sim.dem.famUseCase_{fn};
    end
    m = m + 1;
    
    IND = ~CellIsExpi & strcmp(Operator,Sim.dem.operators{n1}) & ...
        Cell_INfoc & strcmp('macrocell',pwrAmpClas);
    if any( IND )
        resu.M{m,curCol1} = sum(ceil(([Cell(IND).cellProce] + ...
            sum([Cell(IND).userDyn]+[Cell(IND).userRema],1))./8).*8,2);
    end

    %% Demand to be served (in focus) in MB/day
    m = m + 3;
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    m = m + 1;
    if ~isempty(fn)
        resu.M{m,curCol1} = Sim.dem.famUseCase_{fn};
    end
    m = m + 1;

    resu.M(m+(1:size(resu.demFoc,1))-1,curCol1) = ...
        num2cell(resu.demFoc(:,n1));
    m = m + size(resu.demFoc,1);  % Data
    m = m + 1;  % Blank
    
    %% Demand to be served in buffer in MB/day
    m = m + 1;  % Title
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    m = m + 1;
    if ~isempty(fn)
        resu.M{m,curCol1} = Sim.dem.famUseCase_{fn};
    end
    m = m + 1;

    resu.M(m+(1:size(resu.demBuf,1))-1,curCol1) = ...
        num2cell(resu.demBuf(:,n1));
    m = m + size(resu.demBuf,1);  % Data
    m = m + 1;  % Blank
    
    %% Unserved demand in % over total
    m = m + 1;  % Title
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    m = m + 1;
    if ~isempty(fn)
        resu.M{m,curCol1} = Sim.dem.famUseCase_{fn};
    end
    m = m + 1;

%     resu.M(m+(1:size(resu.demUnservPerc,1))-1,curCol1) = ...
%         num2cell(resu.demUnservPerc(:,n1));
    resu.M(m,curCol1) = {'Please check separate file for this information'};
    m = m + BHcou;  % Data
    m = m + 1;  % Blank
    
    %% Breakdown of sites by specification
    m = m + 1;  % Title
    if n1 == 1
        resu.M{m,curCol1} = Sim.yea.thisYea;
    end
    % Current operator
    m = m + 1;
    resu.M{m,curCol1} = Sim.dem.operators{n1};
    m = m + 1;
    
%     M = cell(0,3);
%     n2 = 1;
%     IND = find(strcmp(Sim.propag.BS{n2,1},pwrAmpClas) & ~CellIsExpi & ...
%         strcmp(Operator,Sim.dem.operators{n1}) & Cell_INfoc);
%     curre = struct('sitChainConfig',{Cell(IND).sitChaiConfig},...
%         'sectoCou',{Cell(IND).sectoCou},...
%         'AeCou',{Cell(IND).AeCou},...
%         'Band',arrayfun(@(x) x.band.capa,Cell(IND),'UniformOutput',false));
%     Hash = arrayfun(@(x) DataHash(x),curre,'UniformOutput',false);
%     [C,ia,ic] = unique(Hash);
%     counts = hist(ic,1:max(ic));
%     for cn = 1 : length(counts)
%         IND = strcmp(C{cn},M(:,1));
%         if any(IND)
%             fghfgh
%             M{IND,m+2} = counts(cn);
%         else
%             M{end+1,1} = C{cn};
%             M{end,2} = curre(ia(cn));
%             M{end,3} = [num2str(curre(ia(cn)).sectoCou) ' ' ...
%                 num2str(curre(ia(cn)).sitChainConfig) ' [' ...
%                 num2str(curre(ia(cn)).AeCou) '] [' ...
%                 num2str(curre(ia(cn)).Band) ']'];
%             M{end,4} = counts(cn);
%         end
%         resu.M{m,curCol1} = Sim.dem.operators{n1};
%         m = m + 1;
%     end
    
    IND = ~CellIsExpi & strcmp(Operator,Sim.dem.operators{n1}) & ...
        Cell_INfoc;
    if any(IND)
        curre = struct('sitChainConfig',{Cell(IND).sitChaiConfig},...
            'sectoCou',{Cell(IND).sectoCou},...
            'AeCou',{Cell(IND).AeCou},...
            'Band',arrayfun(@(x) x.band.capa,Cell(IND),'UniformOutput',false));
        [curre.pwrAmpClas] = deal(Cell(IND).pwrAmpClas);
        Hash = arrayfun(@(x) DataHash(x),curre,'UniformOutput',false);
        [C,~,ic] = unique(Hash,'stable');
        counts = hist(ic,1:max(ic));
        M = cell(length(resu.C),1);
        for cn = 1 : length(counts)
            IND = strcmp(C{cn},resu.C);
            M{IND} = counts(cn);
        end
        resu.M(m+(0:length(resu.C)-1),curCol1) = M;
    end
end

Hash = DataHash(struct('simYeaThisYea',Sim.yea.thisYea,...
    'simShariIs',Sim.shari.is));
save(['OutpAux\' Hash '.mat'],'resu','-append')
end

function [resu,m] = upd_results_1(resu,m,Cell,n1,Sim,col,sty1,sty2,sty3,...
    sty4,vargin)
% Simplification
List_ = resu.List_;

switch sty1
    case 'Antenna Site'
    case 'Edge Cloud'
        [coreCou,edgCloSitCoreIsExpi,a] = deal(vargin{:});
    otherwise
end

if n1 == 1
    resu.M{m,col} = Sim.yea.thisYea;
end

% Write operator
m = m + 1;
resu.M{m,col} = Sim.dem.operators{n1};

if ~isempty(Cell)
    for n = 1 : size(List_,1)
        if strcmp(List_{n,2},sty1)
            m = m + 1;
            if strcmp(sty2,'any') || strcmp(List_{n,4},sty2)
                switch List_{n,5}
                    case 'number of sites'
                        switch sty1
                            case 'Antenna Site'
                                resu.M{m,col} = sty3(arrayfun(@(x) x.(List_{n,3})(1),Cell));
                            case 'Edge Cloud'
                                switch sty4
                                    case 'new'
                                        if Sim.forcNetwCranIn == Sim.yea.thisYea
                                            switch func2str(sty3)
                                                case 'nnz'
                                                    resu.M{m,col} = length(Cell) .* mean(a);
                                                case 'mean'
                                                    tmp = arrayfun(@(x) x.(List_{n,3})(1),Cell);
                                                    resu.M{m,col} = sty3(tmp);
                                                otherwise
                                                    dfssdf
                                            end
                                        else
                                            resu.M{m,col} = 0;
                                        end
                                    case 'upg'
                                        if Sim.forcNetwCranIn == Sim.yea.thisYea
                                            resu.M{m,col} = 0;
                                        else
                                            switch List_{n,3}
                                                case 'capexBackhauV'
                                                    tmp = arrayfun(@(x) x.(List_{n,3})(1),Cell);
                                                    if any(tmp>0)
                                                        switch func2str(sty3)
                                                            case 'nnz'
                                                                resu.M{m,col} = sty3(tmp) .* mean(a(tmp>0));
                                                            case 'mean'
                                                                resu.M{m,col} = sty3(tmp);
                                                            otherwise
                                                                sdgds
                                                        end
                                                    else
                                                        resu.M{m,col} = 0;
                                                    end
                                                otherwise
                                                    resu.M{m,col} = 0;
                                            end
                                        end
                                    case 'refreshe'
                                        resu.M{m,col} = 0;
                                    case 'decom'
                                        resu.M{m,col} = 0;
                                    case 'installeBas'
                                        switch func2str(sty3)
                                            case {'nnz','length'}
                                                resu.M{m,col} = length(Cell) .* mean(a);
                                            case 'mean'
                                                resu.M{m,col} = sty3(arrayfun(@(x) x.(List_{n,3})(1),Cell));
                                            otherwise
                                        end
                                    otherwise
                                        dsfdfs
                                end
                            otherwise
                                fdsgtrgtr
                        end
                    case 'number of baseline BBU'
                        switch sty4
                            case 'upg'
                                tmp = nan(1,length(Cell));
                                for k = 1 : length(Cell)
                                    if ~isempty(Cell(k).upg)
                                        INDupg = strcmp(Cell(k).upg(:,1),'BBbareMeta');
                                        if any(INDupg)
                                            tmp(k) = Cell(k).bareMetaBBUcou - Cell(k).upg{INDupg,2}{1};
                                        else
                                        end
                                    else
                                    end
                                end
                            case {'new','refreshe','installeBas'}
                                tmp = [Cell.bareMetaBBUcou];
                            otherwise
                                sdfdf
                        end
                        switch func2str(sty3)
                            case {'nnz','length'}
%                                 resu.M{m,col} = sum(tmp(~isnan(tmp)).*a.');
                                resu.M{m,col} = sum(tmp(~isnan(tmp)));
                            case 'mean'
                                temp2 = arrayfun(@(x) x.(List_{n,3})(1),Cell) ./ tmp;
                                resu.M{m,col} = mean(temp2(~isnan(temp2)));
                            otherwise
                                htrrthtr
                        end
                    case 'number of cores'
                        switch func2str(sty3)
                            case {'nnz','length'}
                                switch sty1
                                    case 'Antenna Site'
                                        switch sty4
                                            case 'upg'
                                                tmp = nan(length(Cell),1);
                                                for k = 1 : length(Cell)
                                                    if ~isempty(Cell(k).upg)
                                                        assert(isempty(Cell(k).edgNodeProceCou))
%                                                         INDupg = strcmp(Cell(k).upg(:,1),'BBbareMeta');
%                                                         if any(INDupg)
%                                                             tmp(k) = diff(Cell(k).upg{INDupg,2}(1:2));
%                                                         else
%                                                         end
                                                    else
                                                    end
                                                end
                                                if ~all(tmp)
                                                    resu.M{m,col} = nanmean(tmp);
                                                else
                                                    resu.M{m,col} = 0;
                                                end
                                            case {'new','refreshe','installeBas'}
                                                resu.M{m,col} = sum([Cell.edgNodeProceCou]);
                                            otherwise
                                                assd
                                        end
                                    otherwise
                                        sdsffdgfd
                                end
                            case 'mean'
                                switch sty1
                                    case 'Antenna Site'
                                        if ~isempty([Cell.edgNodeProceCou])
                                            dfgdfgd
                                        end
                                    otherwise
                                        sdfdsfd
                                end
                            otherwise
                                dfd
                        end
                    case 'number of servers'
                        assert(strcmp(sty1,'Edge Cloud'),'code this!')
%                         tmp2 = zeros(1,length(Cell));
                        switch sty4
                            case 'upg'
                                if Sim.forcNetwCranIn == Sim.yea.thisYea
                                    tmp = zeros(1,length(Cell));
                                else
%                                     if any(any(cell2mat(edgCloSitCoreIsExpi),2),1)
%                                         tmp = cellfun(@(x,y) x(end)-x(find(y,1,'last')),coreCou,edgCloSitCoreIsExpi) ./ Sim.coreCouPerServ;
%                                         tmp2 = cellfun(@(x,y) x(find(y,1,'last')),coreCou,edgCloSitCoreIsExpi) ./ Sim.coreCouPerServ;
%                                     else
%                                         tmp = cellfun(@(x) x(end),coreCou) ./ Sim.coreCouPerServ;
%                                     end
                                    
                                    switch List_{n,1}
                                        case 'Working servers'
                                            str = 'workServCou';
                                        case 'Spare servers'
                                            str = 'spServCou';
                                        otherwise
                                            error('Undefined')
                                    end
                                    tmp = zeros(1,length(Cell));
                                    for k = 1 : length(Cell)
                                        INDupg = strcmp(Cell(k).upg(:,1),...
                                            str);
                                        if any(INDupg)
                                            tmp(k) = Cell(k).(str) - ...
                                                Cell(k).upg{INDupg,2}{1};
                                        end
                                    end
                                end
                            case 'new'
                                if Sim.forcNetwCranIn == Sim.yea.thisYea
                                    switch List_{n,1}
                                        case 'Working servers'
                                            str = 'workServCou';
                                        case 'Spare servers'
                                            str = 'spServCou';
                                        otherwise
                                            error('Undefined')
                                    end
                                    tmp = zeros(1,length(Cell));
                                    for k = 1 : length(Cell)
                                        INDupg = strcmp(Cell(k).upg(:,1),...
                                            str);
                                        if any(INDupg)
                                            tmp(k) = Cell(k).(str) - ...
                                                Cell(k).upg{INDupg,2}{1};
                                        end
                                    end
                                else
                                    tmp = zeros(1,length(Cell));
                                end
                            case 'installeBas'
%                                 tmp = cellfun(@(x,y) sum(x(~y),1),coreCou,edgCloSitCoreIsExpi) ./ Sim.coreCouPerServ;
                                switch List_{n,1}
                                    case 'Working servers'
                                        str = 'workServCou';
                                    case 'Spare servers'
                                        str = 'spServCou';
                                    otherwise
                                        error('Undefined')
                                end
                                tmp = zeros(1,length(Cell));
                                for k = 1 : length(Cell)
                                    tmp(k) = Cell(k).(str);
                                end
                            case 'refreshe'
                                if any(any(cell2mat(edgCloSitCoreIsExpi),2),1)
                                    switch List_{n,1}
                                        case 'Working servers'
                                            tmp = [Cell.workServExpiThisYeaCou];
                                        case 'Spare servers'
                                            tmp = [Cell.spServExpiThisYeaCou];
                                        otherwise
                                            error('Undefined')
                                    end
                                    
                                    switch List_{n,1}
                                        case 'Working servers'
                                            str = 'workServCou';
                                        case 'Spare servers'
                                            str = 'spServCou';
                                        otherwise
                                            error('Undefined')
                                    end
                                    tmp2 = zeros(1,length(Cell));
                                    for k = 1 : length(Cell)
                                        INDupg = strcmp(Cell(k).upg(:,1),...
                                            str);
                                        if any(INDupg)
                                            tmp2(k) = Cell(k).(str) - ...
                                                Cell(k).upg{INDupg,2}{1};
                                        end
                                    end
                                    
%                                     tmp = cellfun(@(x,y) x(find(y,1,'last')),coreCou,edgCloSitCoreIsExpi) ./ Sim.coreCouPerServ;
%                                     tmp2 = cellfun(@(x,y) x(end)-x(find(y,1,'last')),coreCou,edgCloSitCoreIsExpi) ./ Sim.coreCouPerServ;
                                else
                                    tmp = zeros(1,length(Cell));
                                end
                            case 'decom'
                                tmp = zeros(1,length(Cell));
                            otherwise
                                assd
                        end
                        switch func2str(sty3)
                            case {'nnz','length'}
                                resu.M{m,col} = sum(tmp.*a.');
                            case 'mean'
                                if all(tmp==0)
                                    resu.M{m,col} = 0;
                                else
                                    switch sty4
                                        case {'upg','new','installeBas','decom'}
                                            resu.M{m,col} = nanmean(arrayfun(@(x) x.(List_{n,3})(1),Cell)./tmp);
                                        case 'refreshe'
                                            resu.M{m,col} = nanmean(arrayfun(@(x) x.(List_{n,3})(1),Cell)./tmp2);
                                            clear tmp2
                                        otherwise
                                            assd54
                                    end
                                end
                        end
                    case 'number of cabinets'
                        assert(strcmp(sty1,'Edge Cloud'),'code this!')
                        switch sty4
                            case 'upg'
                                if Sim.forcNetwCranIn == Sim.yea.thisYea
                                    tmp = zeros(1,length(Cell));
                                else
                                    tmp = zeros(1,length(Cell));
                                    for k = 1 : length(Cell)
                                        INDupg = strcmp(Cell(k).upg(:,1),'cabiCou');
                                        if any(INDupg)
                                            tmp(k) = Cell(k).cabiCou - Cell(k).upg{INDupg,2}{1};
                                        end
                                    end
                                end
                            case 'new'
                                if Sim.forcNetwCranIn == Sim.yea.thisYea
                                    tmp = zeros(1,length(Cell));
                                    for k = 1 : length(Cell)
                                        INDupg = strcmp(Cell(k).upg(:,1),'cabiCou');
                                        if any(INDupg)
                                            tmp(k) = Cell(k).cabiCou - Cell(k).upg{INDupg,2}{1};
                                        end
                                    end
                                else
                                    tmp = zeros(1,length(Cell));
                                end
                            case 'installeBas'
                                tmp = [Cell.cabiCou];
                            case 'refreshe'
                                tmp = zeros(1,length(Cell));
                            case 'decom'
                                tmp = zeros(1,length(Cell));
                            otherwise
                                assd
                        end
                        switch func2str(sty3)
                            case {'nnz','length'}
                                resu.M{m,col} = sum(tmp.*a.');
                            case 'mean'
                                if all(tmp==0)
                                    resu.M{m,col} = 0;
                                else
                                    resu.M{m,col} = nanmean(arrayfun(@(x) x.(List_{n,3})(1),Cell)./tmp);
                                end
                        end
                    otherwise
                        sgdgrefre
                end
            else
                resu.M{m,col} = 0;
            end
        end
    end
else
    mCou = nnz(strcmp(List_(:,2),sty1));
    resu.M(m+(1:mCou),col) = {0};
    m = m + mCou;
end
end

%%
function resu = upd_demFoc(resu,Sim,thisDem,studyA)
resu.demFoc = c_unserv(Sim,thisDem,studyA,'all','sum over focus');
resu.demBuf = c_unserv(Sim,thisDem,studyA,'all','sum over buffer');
end

%%
function w_utilisPerEdgCloSit(resu,Sim,Cell,edgCloSit)
BHcou = size(Sim.dem.hourDistri,1);  % Number of busy hours

edgCloSit_INfoc = find([edgCloSit.INfoc]);
num = [[edgCloSit(edgCloSit_INfoc).workServCou]
    [edgCloSit(edgCloSit_INfoc).spServCou]
    [edgCloSit(edgCloSit_INfoc).cabiCou]].';
num = [num(:,1:2).*Sim.coreCouPerServ num];
M = [{'Name' 'Installed cores - working' 'Installed cores - spare' ...
    'Installed servers - working' 'Installed servers - spare' ...
    'Installed cabinets'}
    {'' '' '' '' '' ''}
    {'' '' '' '' '' ''}
    [[{edgCloSit(edgCloSit_INfoc).DLE_Name}.';'Total'],...
    num2cell([num;sum(num,1)])]];

CellIsExpi = Sim.yea.thisYea >= [Cell.expiYea];

% Beware that cellProce does not scale with utilisation
utilisPerEdgCloSit = zeros(length(edgCloSit_INfoc),BHcou);
for en = 1 : length(edgCloSit_INfoc)
    IND = ~CellIsExpi & ...
        [Cell.connTowaCloID] == edgCloSit(edgCloSit_INfoc(en)).ID;

    if any(IND)
        utilis = reshape(cell2mat(arrayfun(@(x) squeeze(sum(sum(...
            x.band.utilis,4),2)),Cell(IND),'UniformOutput',false)),...
            [BHcou,length(Sim.linBud.freq),nnz(IND)]);
        capa = shiftdim(repmat(cell2mat(arrayfun(@(x) x.band.capa.',...
            Cell(IND),'UniformOutput',false)),[1,1,BHcou]),2);

        % Antenna site processing - non FFT
        BW = utilis .* capa;
        tmp3 = shiftdim(repmat(reshape([Cell(IND).spatBeamCou].' .* ...
            [Cell(IND).MIMOstreaCou].',size(BW,2),size(BW,3)),...
            [1,1,BHcou]),2);
        tmp = BW ./ 20 .* tmp3;
        userDyn = bsxfun(@times,squeeze(sum(tmp.*0.87 ./ ...
            Sim.cost.procImprov,2)),[Cell(IND).sectoCou]);
        userRema = bsxfun(@times,squeeze(sum(tmp.*0.24,2)),...
            [Cell(IND).sectoCou]);
        
        % Antenna site processing - FFT
        cellProce = bsxfun(@times,squeeze(sum(capa./20.*tmp3.*0.27 ./ ...
            Sim.cost.procImprov,2)),[Cell(IND).sectoCou]);
        
        % []
        peaMarg = 1.25;
        tmp = userDyn.*peaMarg + userRema;
        coreCou = cellProce + tmp;
        
        edgCloSitCoreIsExpi = (Sim.yea.thisYea >= ...
            edgCloSit(edgCloSit_INfoc(en)).coreCou(:,1) + ...
            edgCloSit(edgCloSit_INfoc(en)).coreReplEver) & ...
            edgCloSit(edgCloSit_INfoc(en)).coreCou(:,2) > 0;
        
        utilisPerEdgCloSit(en,:) = sum(coreCou,2) ./ ...
            sum(edgCloSit(edgCloSit_INfoc(en)).coreCou(...
            ~edgCloSitCoreIsExpi,2),1);
    end
end

topStr = [ 'Utilisation of cores/servers' repmat( {''}, 1, ( size( Sim.dem.hourDistri, 1 ) - 1 ) ) ];
botStr = repmat( {''}, 1, size( Sim.dem.hourDistri, 1 ) );

M = [M [ topStr
    Sim.dem.hourDistri(:,1).'
    Sim.dem.hourDistri(:,2).'
    num2cell(utilisPerEdgCloSit)
    botStr ]];

cell2csv(fullfile('Outp',['edgCloSit utilis aft non refre impro ' ...
    num2str(Sim.yea.thisYea) '.csv']),M)
end

%% Calculate the unserved demand
function resu = c_demUnservPerc(resu,Sim,unserv2)
resu.demUnservPerc = [[repmat({' '},4,2);Sim.dem.hourDistri],...
    [Sim.demTranslati(:,[1,3,4,6]).';num2cell(unserv2)]];
end

%% Calculate service percentages
function resu = c_servPerc(resu,Sim,thisDem,BHcou,dem)
L1 = size(Sim.demTranslati,1);
L2 = length(thisDem);
thisDemBand = reshape([thisDem.band],BHcou,L1,L2);
tputOveMbps = reshape([thisDem.tputOveMbps],BHcou,L1,L2);

% Sort bands with decreasing frequency
[~,f_] = sortrows([Sim.linBud.actuFreq Sim.linBud.dupMode ...
    cellfun(@(x) str2num(x(1)),Sim.linBud.tecInterf)],[-1 2 -3]);

resu.servPerc = nan(BHcou * (size(Sim.demTranslati,1)+2),...
    length(Sim.linBud.spotFreq) * length(Sim.optimis.repoTput));
M = {};
for fn = f_.'
    for hn = 1 : BHcou
        for n = 1 : size(Sim.demTranslati,1)
            switch Sim.demTranslati{n,6}
              case 'popu'
                tmp2 = 3;
                str = {dem(1).layNam}.';
              case 'pedeStat'
                tmp2 = 1 : 3;
                str = {dem(6:8).layNam}.';
              otherwise
                error('Undefined')
            end
            
            M((hn-1)*(size(Sim.demTranslati,1)+2)+n-1+tmp2,1:7) = ...
                [repmat([Sim.dem.hourDistri(hn,:) ...
                Sim.demTranslati(n,[1,3,4,6])],length(tmp2),1) str];
            
            IND = thisDemBand(hn,n,:) == fn;
            
            if ~any(IND)
                continue
            end
            
            linearInd = sub2ind(size(dem(1).A),...
                [thisDem(IND).I],[thisDem(IND).J]);
            
            for m = 1 : length(Sim.optimis.repoTput)
                IND2 = tputOveMbps(hn,n,IND) >= Sim.optimis.repoTput(m);
                
                switch Sim.demTranslati{n,6}
                  case 'popu'
                    tmp = sum(full(dem(1).A(linearInd(IND2)))) ./ ...
                        sum(dem(1).A(:));
                  case 'pedeStat'
                    tmp = [sum(full(dem(6).A(linearInd(IND2)))) ./ ...
                        sum(dem(6).A(:))
                        sum(full(dem(7).A(linearInd(IND2)))) ./ ...
                        sum(dem(7).A(:))
                        sum(full(dem(8).A(linearInd(IND2)))) ./ ...
                        sum(dem(8).A(:))];
                  otherwise
                    error('Undefined')
                end
                
                resu.servPerc(...
                    (hn-1)*(size(Sim.demTranslati,1)+2)+n-1+tmp2,...
                    (fn-1)*length(Sim.optimis.repoTput)+m) = tmp;
            end
        end
    end
end

[X,Y] = meshgrid(1:length(Sim.linBud.freq),1:length(Sim.optimis.repoTput));
tmp = cellfun(@num2str,num2cell(resu.servPerc),'UniformOutput',false);
tmpX = cellfun(@num2str,Sim.linBud.freq(X(:)).','UniformOutput',false);
tmpY = cellfun(@num2str,num2cell(Sim.optimis.repoTput(Y(:))),...
    'UniformOutput',false);
resu.servPerc = [[repmat({' '},2,7) [tmpX;tmpY]]; [M tmp]];

resu.servPerc(cellfun(@isempty,resu.servPerc)) = deal({' '});
end

%% Calculate the BW used per governorate
function resu = c_BWperGovernora(resu,studyA,Cell,Sim)
pwrAmpClas = {Cell.pwrAmpClas};
CellIsExpi = Sim.yea.thisYea >= [Cell.expiYea];
Operator = {Cell.Operator};
Cell_INfoc = [Cell.INfoc];
BHcou = size(Sim.dem.hourDistri,1);  % Number of busy hours

IND = strcmp('macrocell',pwrAmpClas) & ~CellIsExpi & ...
    strcmp(Sim.dem.operators{1},Operator) & Cell_INfoc;

fn = 1;

utilis = reshape(cell2mat(arrayfun(@(x) squeeze(sum(...
    x.band.utilis(:,:,:,fn),2)),Cell(IND),...
    'UniformOutput',false)),...
    [BHcou,length(Sim.linBud.freq),nnz(IND)]);
capa = shiftdim(repmat(cell2mat(arrayfun(@(x) x.band.capa.',...
    Cell(IND),'UniformOutput',false)),[1,1,BHcou]),2);

BWperCell = utilis .* capa ./ ...
    repmat(Sim.linBud.PRButilisCapa.',[BHcou,1,nnz(IND)]);

governoraID = zeros(length(Cell),1);
for n = 1 : length(Cell)
    governoraID(n) = find(studyA.INGovernoratesPer(Cell(n).I,Cell(n).J,:));
end

BWperGovernora = zeros(length(studyA.Governorates),length(Sim.linBud.str));
for n = 1 : length(studyA.Governorates)
    IND2 = governoraID==n & IND.';
    if any(IND2)
        for m = 1 : length(Sim.linBud.str)
            BWperGovernora(n,m) = pctile(permute(max(...
                BWperCell(:,m,IND2),[],1),[3,2,1]),0.95,'nearest');
        end
    end
end

resu.BWperGovernora = [[' ' Sim.linBud.str.']
    [{studyA.Governorates.NAME_EN}.' ...
    cellfun(@num2str,num2cell(BWperGovernora),'UniformOutput',false)]];
end

%% Calculate the BW used per area
function resu = c_BWperOslo(resu,studyA,Cell,Sim)
pwrAmpClas = {Cell.pwrAmpClas};
CellIsExpi = Sim.yea.thisYea >= [Cell.expiYea];
Operator = {Cell.Operator};
Cell_INfoc = [Cell.INfoc];
BHcou = size(Sim.dem.hourDistri,1);  % Number of busy hours

IND = strcmp('macrocell',pwrAmpClas) & ~CellIsExpi & ...
    strcmp(Sim.dem.operators{1},Operator) & Cell_INfoc;

fn = 1;

utilis = reshape(cell2mat(arrayfun(@(x) squeeze(sum(...
    x.band.utilis(:,:,:,fn),2)),Cell(IND),...
    'UniformOutput',false)),...
    [BHcou,length(Sim.linBud.freq),nnz(IND)]);
capa = shiftdim(repmat(cell2mat(arrayfun(@(x) x.band.capa.',...
    Cell(IND),'UniformOutput',false)),[1,1,BHcou]),2);

BWperCell = utilis .* capa ./ ...
    repmat(Sim.linBud.PRButilisCapa.',[BHcou,1,nnz(IND)]);

switch Sim.SA.name
  case 'West Bank'
    OsloID = zeros(length(Cell),1);
    for n = 1 : length(Cell)
        OsloID(n) = find(studyA.INOsloPer(Cell(n).I,Cell(n).J,:));
    end
    
    BWperOslo = zeros(length(studyA.Oslo),length(Sim.linBud.str));
    for n = 1 : length(studyA.Oslo)
        IND2 = OsloID==n & IND.';
        if any(IND2)
            for m = 1 : length(Sim.linBud.str)
                BWperOslo(n,m) = pctile(permute(max(...
                    BWperCell(:,m,IND2),[],1),[3,2,1]),0.95,'nearest');
            end
        end
    end
  case 'Gaza'
  otherwise
    error('Undefined')
end

resu.BWperOslo = [[' ' Sim.linBud.str.']
    [{studyA.Oslo.CLASS}.' ...
    cellfun(@num2str,num2cell(BWperOslo),'UniformOutput',false)]];
end
end
end
