function dem = read_road_rail(studyA,Sim,clu)
%% POIN class constructor
% POIN is the class constructor of POIN objects, poin. (At least that is
% the intent). Currently this is a function and the output is an
% intermediate to the poin objects, dem.
% 
% The input studyA is a study area object and must contain at least the
% following fields:
% R - See study area class
% X,Y - See study area class
% buf.X,buf.Y,buf.BoundingBox - See study area class
% HPA.X,HPA.Y - See study area class
% 
% The input structure Sim must contain at least the following fields:
% path.ProgramData - Path to ProgramData
% path.roa - Path to road database (note that rail database is pending)
% mstruct - Matlal native map projection structure
% 
% clu is a CLU object.

%% Lookup table
% Below is the association between code and	fclass as read in the database
% extract. Disclaimer for use in other regions of the world or future
% versions.
% 
% code	fclass
% 5111	motorway
% 5112	trunk
% 5113	primary
% 5114	secondary
% 5115	tertiary
% 5121	unclassified
% 5122	residential
% 5123	living_street
% 5124	pedestrian
% 5131	motorway_link
% 5132	trunk_link
% 5133	primary_link
% 5134	secondary_link
% 5135	tertiary_link
% 5141	service
% 5142	track
% 5143	track_grade1
% 5144	track_grade2
% 5145	track_grade3
% 5146	track_grade4
% 5147	track_grade5
% 5151	bridleway
% 5152	cycleway
% 5153	footway
% 5154	path
% 5155	steps
% 5199	unknown
% 6101	rail
% 6102	light_rail
% 6103	subway
% 6105	monorail
% 6106	narrow_gauge
% 6108	funicular

%% Constructor
% Try to load or create output
if exist(fullfile(Sim.path.ProgramData,'thorou.mat'),'file')
    % Load thoroughfare object
    load(fullfile(Sim.path.ProgramData,'thorou.mat'),'dem')
else
    % Initiate output
    roRl = {};

    % Read shapefiles of road and rail
    tmp = shaperead(Sim.path.roa);
%     switch Sim.SA.name
%       case 'West Bank'
%         tmp = shaperead(Sim.path.roa.WB);
%       case 'Gaza'
%         tmp = shaperead(Sim.path.roa.Gaza);
%       case 'Settlements'
%         tmp = shaperead(Sim.path.roa.Settlements);
%       otherwise
%         error('Undefined')
%     end
    S = struct('Geometry',{tmp.Geometry}.',...
        'BoundingBox',{tmp.BoundingBox}.',...
        'X',{tmp.X}.','Y',{tmp.Y}.','code',{tmp.code}.',...
        'fclass',{tmp.fclass}.','maxspeed',{tmp.maxspeed}.',...
        'tunnel',{tmp.tunnel}.');
%     tmp = shaperead(Sim.path.rail);
%     S = [S; struct('Geometry',{tmp.Geometry}.',...
%         'BoundingBox',{tmp.BoundingBox}.',...
%         'X',{tmp.X}.','Y',{tmp.Y}.','code',{tmp.code}.',...
%         'fclass',{tmp.fclass}.','maxspeed',nan,...
%         'tunnel','F')];
    clear tmp
    % figure
    % plot([S.X],[S.Y])

    % Convert to planar coordinates
    if isfield(Sim.mstruct,'ostn')
        lat = [S.Y].';
        lon = [S.X].';
        IND = ~isnan(lat);
        X = nan(1,length(lon));
        Y = nan(1,length(lat));
        [X(IND),Y(IND)] = latLon2easNor(lat(IND),lon(IND),[],...
            Sim.mstruct.ostn);
    elseif isfield(Sim.mstruct,'mapprojection')
        [X,Y] = mfwdtran(Sim.mstruct,[S.Y],[S.X]);
    else
        error('Code this!')
    end
    N = arrayfun(@(x) length(x.X),S);
    tmp = mat2cell(X,1,N);
    [S(:).X] = deal(tmp{:});
    tmp = mat2cell(Y,1,N);
    [S(:).Y] = deal(tmp{:});
    for n = 1 : length(S)
        S(n).BoundingBox = [min(S(n).X) min(S(n).Y)
            max(S(n).X) max(S(n).Y)];
    end
    % figure
    % plot([S.X],[S.Y])
    clear X Y tmp N
    
    % For each polyline, populate database roRl unless polyline is
    % tunnelled roadtype, or outside the study area. If crossing the study
    % area, then chop. Do this for focus AND HPA, for focus and nonHPA, and
    % for buffer.
    for n = 1 : length(S)
        % Assert that this is a line
        assert(strcmp(S(n).Geometry,'Line'))
%         figure
%         hold on
%         plot([S.X],[S.Y])
%         plot(studyA.buf.BoundingBox([1,1,2,2,1],1),...
%             studyA.buf.BoundingBox([1,2,2,1,1],2))
%         plot(S(n).BoundingBox([1,1,2,2,1],1),...
%             S(n).BoundingBox([1,2,2,1,1],2),'linewidth',2)
%         plot(studyA.buf.X,studyA.buf.Y,'k')
%         axis equal

        % If not a tunnel then proceed to populate database roRl
        if strcmp(S(n).tunnel,'F')
            % Is not a tunnel
            if studyA.buf.BoundingBox(1,1) < S(n).BoundingBox(2,1) && ...
                    studyA.buf.BoundingBox(2,1) > S(n).BoundingBox(1,1) && ...
                    studyA.buf.BoundingBox(2,2) > S(n).BoundingBox(1,2) && ...
                    studyA.buf.BoundingBox(1,2) < S(n).BoundingBox(2,2)
                % Is within the bouding box of the buffer.
                % Find if within focus, buffer and HPA polygons.
%                 INfoc = inpolygon(S(n).X,S(n).Y,studyA.X,studyA.Y);
%                 INbuf = inpolygon(S(n).X,S(n).Y,studyA.buf.X,studyA.buf.Y) & ...
%                     ~INfoc;
                INfoc = true(size(S(n).X));
                INbuf = false(size(S(n).X));
%                 INhpa = inpolygon(S(n).X,S(n).Y,studyA.HPA.X,studyA.HPA.Y);

                % Update roRl depending on which map polygons the polyline
                % segments belong to. Note that the map polygons are
                % mutually exclusive, so there is no double counting.
%                 if any(INfoc&INhpa)
%                     % In focus and in HPA
%                     roRl = compile_road_rail_matrix(roRl,INfoc&INhpa,...
%                         S(n),studyA,'focHPA');
%                 end
%                 if any(INfoc&~INhpa)
%                     % In focus and outside HPA, i.e. Hamburg City
%                     roRl = compile_road_rail_matrix(roRl,INfoc&~INhpa,...
%                         S(n),studyA,'focNonHPA');
%                 end
                if any(INfoc)
                    % In focus and in HPA
                    k = [0 find(isnan(S(n).X))];
                    for kn = 1 : length(k)-1
                        tmpS = S(n);
                        IND = k(kn)+1 : k(kn+1);
                        tmpS.X = tmpS.X(IND);
                        tmpS.Y = tmpS.Y(IND);
                        roRl = compile_road_rail_matrix(roRl,INfoc(IND),...
                            tmpS,studyA,'foc');
                    end
                end
                if any(INbuf)
                    % In buffer, i.e. not in focus
                    k = [0 find(isnan(S(n).X))];
                    for kn = 1 : length(k)-1
                        tmpS = S(n);
                        IND = k(kn)+1 : k(kn+1);
                        tmpS.X = tmpS.X(IND);
                        tmpS.Y = tmpS.Y(IND);
                        roRl = compile_road_rail_matrix(roRl,INbuf(IND),...
                            tmpS,studyA,'buf');
                    end
                end
            end
        end
        
        % Other information not saved at the moment is:
    %     S(n).name  % the name
    %     S(n).oneway  % if it's one way traffic
    %     S(n).bridge  % if that is a bridge
    end

    % Uncomment to get the association between code and fclass
%     [C,ia] = unique([S.code].');
%     [num2cell(C) {S(ia).fclass}.']
%     clear C ia
    
    % Save intermediate output thorou.mat
    thorou = roRl;
    clear roRl
    save(fullfile(Sim.path.ProgramData,'thorou.mat'),'thorou')
    
    %% Display breakdown
    % For each thoroughfare code display information on the length of
    % thoroughfare and percentage over total thoroughfare length
    [C,~,ic] = unique([thorou.code]);
    isRo = fix(C./1000) == 5;
    counts = zeros(length(C),1);
    %
    for n = 1 : length(C)
        counts(n) = sum(arrayfun(@(x) sum(abs(diff(x.X+1i*x.Y))),...
            thorou(ic==n&[thorou.isFoc].')));
    end
    counts_ = [repmat(sum(counts(isRo)),nnz(isRo),1)
        repmat(sum(counts(~isRo)),nnz(~isRo),1)];
    disp([C.' counts counts./counts_])
%     %
%     for n = 1 : length(C)
%         counts(n) = sum(arrayfun(@(x) sum(abs(diff(x.X+1i*x.Y))),...
%             thorou(ic==n&[thorou.isFoc].'&[thorou.isHPA].')));
%     end
%     counts_ = [repmat(sum(counts(isRo)),nnz(isRo),1)
%         repmat(sum(counts(~isRo)),nnz(~isRo),1)];
%     [C.' counts counts./counts_]
%     %
%     for n = 1 : length(C)
%         counts(n) = sum(arrayfun(@(x) sum(abs(diff(x.X+1i*x.Y))),...
%             thorou(ic==n&[thorou.isFoc].'&~[thorou.isHPA].')));
%     end
%     counts_ = [repmat(sum(counts(isRo)),nnz(isRo),1)
%         repmat(sum(counts(~isRo)),nnz(~isRo),1)];
%     [C.' counts counts./counts_]
    
    %% Plot map
    % Create figure
    ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
    fh1 = figure('paperpositionmode','auto','unit','in',...
        'pos',[1 1 7+1./16 (7+1./16).*ratio]);
    hold on
    axis equal off
    set(gca,'pos',[0 0 1 1])

    % Plot HPA and study area polygons
%     plot(studyA.HPA.X,studyA.HPA.Y,'k--','linewidth',1)
    plot(studyA.X,studyA.Y,'k','linewidth',1)
    
    % Plot the thoroughfare types with different colour. The intention is
    % to stop at teh end of each for loop to get a photo for each
    % thoroughfare type from street view.
    [C,~,ic] = unique([thorou.code]);
    Map = parula(length(C));
    for n = 1 : length(C)
        % Plot this thoroughfare type
%         n = n + 1;
        [X,Y] = polyjoin({thorou(ic==n).X},{thorou(ic==n).Y});
        C2 = unique({thorou(ic==n).fclass});
        assert(length(C2)==1)
        ph1 = plot(X,Y,'color',Map(n,:),'disp',num2str(C(n)),'disp',C2{1});
%         legend(ph1)

%         % Plot a marker and display the geographical coordinates to acquire
%         % a photo from street view
%         tmp = thorou(ic==n&[thorou.isFoc].');
% %         ph2 = plot(tmp(1).X(1),tmp(1).Y(1),'ro','disp','photo');
% %         [lat,lon] = minvtran(Sim.mstruct,tmp(1).X(1),tmp(1).Y(1));
% %         ph2 = plot(tmp(end).X(1),tmp(end).Y(1),'ro','disp','photo');
% %         [lat,lon] = minvtran(Sim.mstruct,tmp(end).X(1),tmp(end).Y(1));
%         ph2 = plot(tmp(1000).X(1),tmp(1000).Y(1),'ro','disp','photo');
%         [lat,lon] = minvtran(Sim.mstruct,tmp(1000).X(1),tmp(1000).Y(1));
%         disp([lat,lon])
%         
%         % Delete this thoroughfare type from the figure
%         delete([ph1,ph2])
    end
    clear X Y n ph1 C2 ph1 tmp lat lon
    
%     ch1 = colorbar;
%     cmap = cell2mat(clu.tran(C+1,11:13));
%     set(ch1,'ytick',1:size(cmap,1))
%     colormap(cmap./255)
%     set(gca,'CLim',[0.5 size(cmap,1)+0.5])

    % Add legend
%     legend('location','southwest')

    % Map decorations
    add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

    % Print
%     % print(fh1,fullfile('OutpM','clu'),'-djpeg','-r300')
%     savefig(fullfile('OutpM',['thorou ' Sim.SA.name]))
    export_fig(fullfile('OutpM','thorou.jpg'),'-jpg','-q100','-r300')
%     export_fig(fullfile('OutpM',['thorou' Sim.SA.name '.jpg']),'-jpg','-q100','-r300')

    % Delete graph
    delete(fh1)
    
    %% Rasterise
    % Populate BoundingBox field
    BoundingBox = arrayfun(@(x) [min(x.X),min(x.Y)
        max(x.X),max(x.Y)],thorou,'UniformOutput',false);
    [thorou.BoundingBox] = deal(BoundingBox{:});
    clear BoundingBox

    % Create mesh grid for rasterisation
    [X,Y] = meshgrid(studyA.R.XLimWorld(1) + ...
        studyA.R.CellExtentInWorldX./2 : studyA.R.CellExtentInWorldX : ...
        studyA.R.XLimWorld(2) - studyA.R.CellExtentInWorldX./2,...
        studyA.R.YLimWorld(1) + studyA.R.CellExtentInWorldY./2 : ...
        studyA.R.CellExtentInWorldY : ...
        studyA.R.YLimWorld(2)-studyA.R.CellExtentInWorldY./2);
    BoundingBox = reshape(cell2mat(arrayfun(@(x) x.BoundingBox,thorou,...
        'UniformOutput',false)),[2 2 length(thorou)]);
    rig = BoundingBox(2,1,:);
    lef = BoundingBox(1,1,:);
    bott = BoundingBox(1,2,:);
    top = BoundingBox(2,2,:);
    clear BoundingBox

    % Find unique thoroughfare codes
    code = unique([thorou.code]);
    
%     Define weights
%     'Motorway' [5111;5131]
%     'A road' [5112;5113;5132;5133;5114]
%     'B road' [5115;5134;5135;5141]
%     'Minor road' [5121;5122;5123;5124;5142;5143;5144;5145;5146;5147;5151;5153;5154;5155;5199]
%     w0 = [1;1e-1;1e-2;1e-3;1e-4;1];
%     w0 = [1;1;1;1];
    S = sparse([5111;5131],ones(2,1),1,5999,1) + ...
        sparse([5112;5113;5132;5133;5114],ones(5,1),1e-1,5999,1) + ...
        sparse([5115;5134;5135;5141],ones(4,1),1e-2,5999,1) + ...
        sparse([5121;5122;5123;5124;5142;5143;5144;5145;5146;5147;5151;...
        5153;5154;5155;5199],ones(15,1),1e-3,5999,1);
    w0 = full(S(code));
    clear S
    
    % Create poin objects
    multiWaitbar('Create poin objects',0);
    reso = studyA.R.CellExtentInWorldX;
    assert(studyA.R.CellExtentInWorldX==studyA.R.CellExtentInWorldY)
    poin = struct('X',[],'Y',[]);
    for n = 1 : numel(X)
        % Define the pixel
        x1 = X(n) + reso./2.*[-1 -1 1 1 -1];
        y1 = Y(n) + reso./2.*[-1 1 1 -1 -1];
        
        % Find if the pixel overlaps with the bounding box of each
        % line
        tmp = find(x1(1)<rig&x1(3)>lef);
        IND = tmp(y1(3)>bott(tmp)&y1(1)<top(tmp));
        x2 = [];  % Initialise a vector for storing the line segments within the pixel
        y2 = [];  % Initialise a vector for storing the line segments within the pixel
        typ2 = [];  % Initialise a vector for storing the type of the line segments within the pixel
        maxspeed = [];  % Initialise a vector for storing maxspeed of the line segments within the pixel
        for k = 1 : length(IND)
            %                     figure
            %                     axis equal
            %                     hold on
%                                 plot(x1,y1,'linewidth',2)
%                                 plot([lef(1) lef(1) rig(1) rig(1) lef(1)],...
%                                     [bott(1) top(1) top(1) bott(1) bott(1)],...
%                                     'linewidth',20)
            %                     plot(x,y)
            
            x = thorou(IND(k)).X;
            y = thorou(IND(k)).Y;
            %                         assert(~any(isnan(x)))
            [xi,yi,ii] = polyxpoly(x,y,x1,y1);
            [~,B] = sort(ii(:,1),1);
            xi = xi(B);
            yi = yi(B);
            ii = ii(B,:);
            if ~isempty(ii)
                % The polyline crosses the pixel
                for m = 1 : 2 : size(ii,1)
                    % Update x2, y2
                    if x1(1)<x(ii(m,1)) && x(ii(m,1))<x1(3) && ...
                            y1(1)<y(ii(m,1)) && y(ii(m,1))<y1(3)
                        % Starts inside
                        if m == 1
                            x2{end+1,1} = [x(1:ii(m,1))
                                xi(m)];
                            y2{end+1,1} = [y(1:ii(m,1))
                                yi(m)];
                        else
                            x2{end+1,1} = [xi(m-1)
                                x(ii(m-1,1)+1:ii(m,1))
                                xi(m)];
                            y2{end+1,1} = [yi(m-1)
                                y(ii(m-1,1)+1:ii(m,1))
                                yi(m)];
                        end
                    else
                        % Starts outside
                        if size(ii,1) >= m+1
                            x2{end+1,1} = [xi(m)
                                x(ii(m,1)+1:ii(m+1,1))
                                xi(m+1)];
                            y2{end+1,1} = [yi(m)
                                y(ii(m,1)+1:ii(m+1,1))
                                yi(m+1)];
                        else
                            x2{end+1,1} = [xi(m)
                                x(ii(m,1)+1:end)];
                            y2{end+1,1} = [yi(m)
                                y(ii(m,1)+1:end)];
                        end
                    end
                    
                    % Update typ2
                    typ2(end+1,1) = thorou(IND(k)).code;
                    maxspeed(end+1,1) = thorou(IND(k)).maxspeed;
                end
            else
                % The polyline is outside or inside the pixel
                if ~inpolygon(x(1),y(1),x1,y1)
                    % The polyline is outside the pixel
                else
                    % The polyline is inside the pixel
                    % Update x2, y2
                    x2{end+1,1} = x;
                    y2{end+1,1} = y;
                    
                    % Update typ2
                    typ2(end+1,1) = thorou(IND(k)).code;
                    maxspeed(end+1,1) = thorou(IND(k)).maxspeed;
                end
            end
        end
        
        %                         figure
        %                         axis equal
        %                         hold on
        %                         plot(x1,y1)
        %                         for test1 = 1 : length(x2)
        %                             plot(x2{test1},y2{test1})
        %                         end
        
        % Populate poin properties
        if ~isempty(x2)
            % Define index to poin array
            if isempty(poin(end).X)
                pn = 1;
            else
                pn = length(poin) + 1;
            end
            
            % Convert cell format to nan-delimited
            x = cell2mat(cellfun(@(x) [x;nan],x2,...
                'UniformOutput',false));
            y = cell2mat(cellfun(@(x) [x;nan],y2,...
                'UniformOutput',false));
            
            % Calculate the type of the line to logical
            C = bsxfun(@eq,typ2,code);
            
            % Populate properties of poin
%             poin(pn).X = X(n);
%             poin(pn).Y = Y(n);
            poin(pn).pixX = X(n);
            poin(pn).pixY = Y(n);
%             IND = any(C(:,[1,2,3,6]),2);
%             if any(IND)
%                 poin(pn).X_COORD = [num2cell(typ2(IND)) x2(IND)];
%                 poin(pn).Y_COORD = y2(IND);
%             end
            
            % Calculate the length
            leng = cellfun(@(x,y) sqrt((x(2:end)-x(1:end-1)).^2 + ...
                (y(2:end)-y(1:end-1)).^2),x2,y2,...
                'UniformOutput',false);
            
            % Calculate the weights for the centroid
            [col,~] = find(C.');
            w = cell2mat(cellfun(@(x,y) [nan;x*y;nan],...
                leng,num2cell(w0(col)),'UniformOutput',false));
            w(1) = [];
            
            % Calculate the centroid
            centroX = nansum((x(2:end)+x(1:end-1))./2.*w) ./ ...
                nansum(w);
            centroY = nansum((y(2:end)+y(1:end-1))./2.*w) ./ ...
                nansum(w);
            
            % Calculate the coordinates of the point closest to centro
            tmp = minimum_distance([x(1:end-1).';y(1:end-1).'],...
                [x(2:end).';y(2:end).'],[centroX;centroY]);
            if ~isnan(tmp(1)) && inpolygon(centroX,centroY,...
                    studyA.X,studyA.Y)
                if tmp(1) == X(n)+studyA.R.CellExtentInWorldX./2
                    poin(pn).X = tmp(1) - ...
                        studyA.R.CellExtentInWorldX./2.*0.01;
                else
                    poin(pn).X = tmp(1);
                end
                if tmp(2) == Y(n)-studyA.R.CellExtentInWorldY./2
                    poin(pn).Y = tmp(2) + ...
                        studyA.R.CellExtentInWorldY./2.*0.01;
                else
                    poin(pn).Y = tmp(2);
                end
                
%                 [I1,J1] = studyA.R.worldToDiscrete(poin(pn).X,poin(pn).Y);
%                 [I2,J2] = studyA.R.worldToDiscrete(X(n),Y(n));
%                 assert(all([I1,J1]==[I2,J2]))
                
                poin(pn).leng = C.' * cellfun(@(x) nansum(x),leng);
                poin(pn).maxspeed = C.'*maxspeed ./ sum(C,1).';
            else
                poin(pn) = [];
            end
        end
        
        % Update timer
        if rem(n,round(sqrt(numel(X)))) == 1
            multiWaitbar('Create poin objects',n./numel(X));
        end
    end
    multiWaitbar('Create poin objects','close');
    clear col

    % Populate clu property
%     [I,J] = clu.R.worldToDiscrete([poin.X],[poin.Y]);
%     tmp = num2cell(clu.A(sub2ind(size(clu.A),I,J)));
%     [poin(:).clu] = deal(tmp{:});
%     clear tmp

    % Save intermediate output thorouArra.mat
    save(fullfile(Sim.path.ProgramData,'thorouArra.mat'),'poin')
    
    % Covert to object of arrays
    poin = arraOfObj2objOfArr(poin);
    
    % Populate isUr
%     poin.isUr = sparse(strcmp(clu.tran([poin.clu]+2,16),'ur'));
    
    % Update with bin index
%     poin.bin = logical(sparse(length(poin.X),...
%         (size(bin.meshX,1)-1)*(size(bin.meshX,2)-1)));
%     for m = 1 : size(bin.meshX,1)-1
%         for n = 1 : size(bin.meshX,2)-1
%             IND = bin.meshX(m,n)<=poin.X & poin.X<bin.meshX(m+1,n+1) & ...
%                 bin.meshY(m,n)<=poin.Y & poin.Y<bin.meshY(m+1,n+1);
%             poin.bin(IND,m+(n-1)*bin.RasterSize(1)) = true;
%         end
%     end
    
    % Save intermediate output thorou.mat
    save(fullfile(Sim.path.ProgramData,'thorou.mat'),'thorou','poin')
    
    %% Convert to CAPisce dem structure
    % Convert to CAPisce dem structure
    [I,J] = studyA.R.worldToDiscrete([poin.X],[poin.Y]);
    code = [thorou.code].';
    C = unique(code);
    for n = 1 : length(C)
        IND = poin.leng(:,n) > 0;
        dem(n).A = sparse(I(IND),J(IND),poin.leng(IND,n),...
            studyA.R.RasterSize(1),studyA.R.RasterSize(2));
        C2 = unique({thorou(code==C(n)).fclass});
        assert(length(C2)==1)
        dem(n).layNam = C2{1};
        dem(n).code = C(n);
        dem(n).maxspeed = max([thorou(code==C(n)).maxspeed]);
    end
    dem(1).X = sparse(I,J,poin.X,...
        studyA.R.RasterSize(1),studyA.R.RasterSize(2));
    dem(1).Y = sparse(I,J,poin.Y,...
        studyA.R.RasterSize(1),studyA.R.RasterSize(2));

    % Before the change
%     isRo = fix([dem.code]./1000) == 5;
%     arrayfun(@(x) sum(full(x.A(studyA.INfoc))),dem(isRo).') * ...
%         [1 1./sum(arrayfun(@(x) sum(full(x.A(studyA.INfoc))),dem(isRo).'))]
%     arrayfun(@(x) sum(full(x.A(studyA.INfoc&studyA.INhpa))),dem(isRo).') * ...
%         [1 1./sum(arrayfun(@(x) sum(full(x.A(studyA.INfoc&studyA.INhpa))),dem(isRo).'))]
%     arrayfun(@(x) sum(full(x.A(studyA.INfoc&studyA.INham))),dem(isRo).') * ...
%         [1 1./sum(arrayfun(@(x) sum(full(x.A(studyA.INfoc&studyA.INham))),dem(isRo).'))]
%     arrayfun(@(x) sum(full(x.A(studyA.INfoc))),dem(~isRo).') * ...
%         [1 1./sum(arrayfun(@(x) sum(full(x.A(studyA.INfoc))),dem(~isRo).'))]
%     arrayfun(@(x) sum(full(x.A(studyA.INfoc&studyA.INhpa))),dem(~isRo).') * ...
%         [1 1./sum(arrayfun(@(x) sum(full(x.A(studyA.INfoc&studyA.INhpa))),dem(~isRo).'))]
%     arrayfun(@(x) sum(full(x.A(studyA.INfoc&studyA.INham))),dem(~isRo).') * ...
%         [1 1./sum(arrayfun(@(x) sum(full(x.A(studyA.INfoc&studyA.INham))),dem(~isRo).'))]
    
    save(fullfile(Sim.path.ProgramData,'thorouB4cap.mat'),'dem')
    
    %% Plot service
    % Plot map
%     ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
%     fh1 = figure('paperpositionmode','auto','unit','in',...
%         'pos',[1 1 7+1./16 (7+1./16).*ratio]);
%     hold on
%     axis equal off
%     set(gca,'pos',[0 0 1 1])
%     
%     plot(studyA.X,studyA.Y,'k','linewidth',1)
%     
%     IND = strcmp({thorou.fclass},'service');
%     [X,Y] = polyjoin({thorou(IND).X},{thorou(IND).Y});
%     plot(X,Y)
%     
%     plot(561125,5930325,'ko')
%     plot(561125,5930325,'k.')
%     
%     set(gca,'xlim',[5.6016 5.6235].*1e5,'ylim',[5.9295 5.9309].*1e6)
%     
%     X = studyA.R.XWorldLimits(1) : studyA.R.CellExtentInWorldX : ...
%         studyA.R.XWorldLimits(2);
%     line([1;1]*X,ylim.'*ones(1,length(X)),'color',[0.5 0.5 0.5])
%     Y = studyA.R.YWorldLimits(1) : studyA.R.CellExtentInWorldY : ...
%         studyA.R.YWorldLimits(2);
%     line(xlim.'*ones(1,length(Y)),[1;1]*Y,'color',[0.5 0.5 0.5])
%     
%     [row,col,v] = find(dem(15).A);
%     [X,Y] = studyA.R.intrinsicToWorld(col,row);
%     Xlim = xlim;
%     Ylim = ylim;
%     IND = find(Xlim(1)<X&X<Xlim(2)&Ylim(1)<Y&Y<Ylim(2));
%     for n = 1 : length(IND)
%         text(X(IND(n)),Y(IND(n)),...
%             num2str(round(dem(15).A(row(IND(n)),col(IND(n))))),...
%             'hor','center')
%     end
%     
%     txt = 'WhatCap';
%     savefig(fullfile('OutpM',txt))
%     warning off all
%     export_fig(fullfile('OutpM',[txt '.jpg']),...
%         '-djpeg','-r300')
%     warning on all
% %     delete(gcf)

    %% Plot motorway
    % Plot map
%     ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
%     fh1 = figure('paperpositionmode','auto','unit','in',...
%         'pos',[1 1 7+1./16 (7+1./16).*ratio]);
%     hold on
%     axis equal off
%     set(gca,'pos',[0 0 1 1])
%     
%     plot(studyA.X,studyA.Y,'k','linewidth',1)
%     
%     IND = strcmp({thorou.fclass},'service');
%     [X,Y] = polyjoin({thorou(IND).X},{thorou(IND).Y});
%     plot(X,Y)
%     
%     plot(561125,5930325,'ko')
%     plot(561125,5930325,'k.')
%     
%     set(gca,'xlim',[5.6016 5.6235].*1e5,'ylim',[5.9295 5.9309].*1e6)
%     
%     X = studyA.R.XWorldLimits(1) : studyA.R.CellExtentInWorldX : ...
%         studyA.R.XWorldLimits(2);
%     line([1;1]*X,ylim.'*ones(1,length(X)),'color',[0.5 0.5 0.5])
%     Y = studyA.R.YWorldLimits(1) : studyA.R.CellExtentInWorldY : ...
%         studyA.R.YWorldLimits(2);
%     line(xlim.'*ones(1,length(Y)),[1;1]*Y,'color',[0.5 0.5 0.5])
%     
%     [row,col,v] = find(dem(15).A);
%     [X,Y] = studyA.R.intrinsicToWorld(col,row);
%     Xlim = xlim;
%     Ylim = ylim;
%     IND = find(Xlim(1)<X&X<Xlim(2)&Ylim(1)<Y&Y<Ylim(2));
%     for n = 1 : length(IND)
%         text(X(IND(n)),Y(IND(n)),...
%             num2str(round(dem(15).A(row(IND(n)),col(IND(n))))),...
%             'hor','center')
%     end
% 
% 
% 
% 
%     figure
%     hist(nonzeros(dem(1).A))
% 
%     pctile(nonzeros(dem(1).A),0.50,'nearest')
%     pctile(nonzeros(dem(1).A),0.90,'nearest')
%     pctile(nonzeros(dem(1).A),0.95,'nearest')
    
    %% Remove pixels outside focus
    for n = 1 : length(dem)
        dem(n).A(~studyA.INfoc) = 0;
    end
    
    % Save output thorou.mat
    save(fullfile(Sim.path.ProgramData,'thorou.mat'),'thorou','poin','dem')
    
%     % Report road
%     tmp = arrayfun(@(x) sum(full(x.A(:).*x.w(:).*studyA.INfoc(:))),...
%         dem(isRo).');
%     tmp * [1 1./sum(tmp)]
%     tmp = arrayfun(@(x) sum(full(x.A(:).*x.w(:).*studyA.INfoc(:).*studyA.INhpa(:))),...
%         dem(isRo).');
%     tmp * [1 1./sum(tmp)]
%     tmp = arrayfun(@(x) sum(full(x.A(:).*x.w(:).*studyA.INfoc(:).*studyA.INham(:))),...
%         dem(isRo).');
%     tmp * [1 1./sum(tmp)]
%     
%     % Report rail
%     tmp = arrayfun(@(x) sum(full(x.A(:).*x.w(:).*studyA.INfoc(:))),...
%         dem(~isRo).');
%     tmp * [1 1./sum(tmp)]
%     tmp = arrayfun(@(x) sum(full(x.A(:).*x.w(:).*studyA.INfoc(:).*studyA.INhpa(:))),...
%         dem(~isRo).');
%     tmp * [1 1./sum(tmp)]
%     tmp = arrayfun(@(x) sum(full(x.A(:).*x.w(:).*studyA.INfoc(:).*studyA.INham(:))),...
%         dem(~isRo).');
%     tmp * [1 1./sum(tmp)]
end
end

%% 
function roRl = compile_road_rail_matrix(roRl,INfoc,S,studyA,typ)
% compile_road_rail_matrix is a method of roRl, becua se roRl is both the
% input and output. This method appends entries to roRl. roRl is an
% intermediate object for POIN, and hold information on the road and rail
% databases, i.e. the whole polylines. POIN can include polylines too but
% it becomes too memory-heavy, and only needs to record the point of
% path loss estimation, instead of the whole polyline. INfoc is a logical
% vector (N,1), where N is the length of S.X and S.Y, that is true when
% point (S.X, S.Y) is within the polygon. studyA is defined in teh parent
% function.
% 
% The input S is a structure created by shaperead and must contain at least
% the following fields:
% X,Y - See shaperead
% Additionally, S must contain at least the following fields:
% code, fclass, maxspeed - See openstreetmap.org
% 
% The input typ identifies the polygon, must be one of following:
% ('focHPA','focNonHPA','buf').

% Simplification
pos = [S.X;S.Y].';  % Position
% Str = c_Poly{n,2};  % Layer
% proper_name = c_Poly{n,3};  % Proper name
% road_number = c_Poly{n,4};   % Road number
% rail_ID = c_Poly{n,5};   % Rail ID

% Remove last entry which is NaN
assert(all(isnan(pos(end,:))))
pos(end,:) = [];
INfoc(end) = [];

% Assertions
assert(~any(isnan(pos(:))))

% If not all within the polygon, keep only these within the polygon
if ~all(INfoc)
    [x, y] = chop_curve(pos(:,1),pos(:,2),studyA,INfoc,typ);
    pos = [x, y];
    clear x y
end

% Append to roRl the polylines of thoroughfare
if iscell(pos)
    % Polylines are stored in a cell array
    for posn = 1 : size(pos,1)
        N = length(roRl) + 1;
        roRl(N).X = pos{posn,1};
        roRl(N).Y = pos{posn,2};
        roRl(N).code = S.code;
        roRl(N).fclass = S.fclass;
        roRl(N).maxspeed = S.maxspeed;
%         roRl(N).properNam = proper_name;
%         roRl(N).roaNu = road_number;
%         roRl(N).raiID = rail_ID;
        switch typ
%             case 'focHPA'
%                 % In focus and in HPA
%                 roRl(N).isFoc = true;
%                 roRl(N).isHPA = true;
%             case 'focNonHPA'
%                 % In focus and outside HPA, i.e. Hamburg City
%                 roRl(N).isFoc = true;
%                 roRl(N).isHPA = false;
            case 'foc'
                % In focus and in HPA
                roRl(N).isFoc = true;
%                 roRl(N).isHPA = true;
            case 'buf'
                % In buffer, i.e. not in focus
                roRl(N).isFoc = false;
%                 roRl(N).isHPA = false;
            otherwise
                sdgfgd
        end
%                             line(pos{posn,1},pos{posn,2},'color','k','linewidth',5)
    end
    clear posn
else
    % Polylines are stored in a vector
    N = length(roRl) + 1;
    roRl(N).X = pos(:,1);
    roRl(N).Y = pos(:,2);
    roRl(N).code = S.code;
    roRl(N).fclass = S.fclass;
    roRl(N).maxspeed = S.maxspeed;
%     roRl(N).properNam = proper_name;
%     roRl(N).roaNu = road_number;
%     roRl(N).raiID = rail_ID;
    switch typ
%         case 'focHPA'
%             % In focus and in HPA
%             roRl(N).isFoc = true;
%             roRl(N).isHPA = true;
%         case 'focNonHPA'
%             % In focus and outside HPA, i.e. Hamburg City
%             roRl(N).isFoc = true;
%             roRl(N).isHPA = false;
        case 'foc'
            % In focus and in HPA
            roRl(N).isFoc = true;
%             roRl(N).isHPA = true;
        case 'buf'
            % In buffer, i.e. not in focus
            roRl(N).isFoc = false;
%             roRl(N).isHPA = false;
        otherwise
            sdgfgdsdf
    end
end
%                     line(pos(:,1),pos(:,2),'color','k','linewidth',5)
end

%%
function [x_all, y_all] = chop_curve(x_all,y_all,studyA,INfoc,typ)
% Chop polyline to the desired polygon
% chop_curve returns the polyline (x_all,y_all) which is entirely within
% the desired polygon. The input polyline is (x_all,y_all). INfoc, typ, and
% studyA are defined in the parent function. The description here is done
% with the focus polygon as an example, but the methodology is replicable
% for any polygon.

if nnz(abs(INfoc(2:end) - INfoc(1:end-1))) > 1
    % Multiple changes out/in focus; chop_curve is called recursively.
    IND = find(abs(INfoc(2:end) - INfoc(1:end-1)));
    x_out1 = cell(length(IND),1);
    y_out1 = cell(length(IND),1);
    for n = 1 : length(IND)
        if n == 1
            % First change out/in focus
            [x, y] = chop_curve(x_all(1:IND(n)+1),y_all(1:IND(n)+1),...
                studyA,INfoc(1:IND(n)+1),typ);
        elseif n == length(IND)
            % Last change out/in focus
            [x, y] = chop_curve(x_all(IND(n-1)+1:end),y_all(IND(n-1)+1:end),...
                studyA,INfoc(IND(n-1)+1:end),typ);
        else
            % Not first nor last change out/in focus
            [x, y] = chop_curve(x_all(IND(n-1)+1:IND(n)+1),y_all(IND(n-1)+1:IND(n)+1),...
                studyA,INfoc(IND(n-1)+1:IND(n)+1),typ);
        end
        x_out1{n} = x;
        y_out1{n} = y;
        clear x y
    end
    clear n
    
    % Populate output; cell array
    x_all = x_out1;
    y_all = y_out1;
else
    % Single change out/in focus.
    % Find if it intersects with the buffer
    switch typ
%         case 'focHPA'
%             % In focus and in HPA
%             [X,Y] = curveintersectMulti(x_all,y_all,...
%                 studyA.HPA.X,studyA.HPA.Y);
%         case 'focNonHPA'
%             % In focus and outside HPA, i.e. Hamburg City
%             [X,Y] = curveintersectMulti(x_all,y_all,...
%                 studyA.X,studyA.Y);
        case 'foc'
            % In focus and in HPA
            [X,Y] = curveintersectMulti(x_all,y_all,...
                studyA.X,studyA.Y);
        case 'buf'
            % In buffer, i.e. not in focus
            [X,Y] = curveintersectMulti(x_all,y_all,...
                studyA.buf.X,studyA.buf.Y);
        otherwise
            error('Undefined')
    end
    
    % Find the intersecting segment and populate output; vector
    if ~isempty(X)
        IND = find(abs(INfoc(2:end) - INfoc(1:end-1)));
        if nonzeros(INfoc(2:end)-INfoc(1:end-1)) < 0
            % In-to-out change.
            % Populate output
            x_all(IND+1) = round(X);
            y_all(IND+1) = round(Y);

            % Remove polyline segments outside the polygon
            x_all(IND+2:end) = [];
            y_all(IND+2:end) = [];
        else
            % Out-to-in change.
            % Populate output
            x_all(IND) = round(X);
            y_all(IND) = round(Y);

            % Remove polyline segments outside the polygon
            x_all(1:IND-1) = [];
            y_all(1:IND-1) = [];
        end
    end
end
end

%% 
function [Xout,Yout] = curveintersectMulti(x_all,y_all,X,Y)
% Call multiple times the ml native function curveintersect
% curveintersectMulti finds the intersection points of the two curves on
% the X-Y plane identified by the polylines (x_all,y_all) and (X,Y).
% Polyline (x_all,y_all) must be provided as vector. (X,Y) must be provided
% as NaN-delimited. (Xout,Yout) are the coordinates of the intersection
% points.

% Call curveintersect for each segment of (X,Y) that does not contain NaN
[x,y] = polysplit(X,Y);
[Xout,Yout] = cellfun(@(x,y) curveintersect(x_all,y_all,x,y),...
    x,y,'UniformOutput',false);
IND = cellfun(@(x) isempty(x),Xout);
Xout(IND) = [];
Yout(IND) = [];
assert(length(Xout)<=1,'Code this!')
if isempty(Xout)
    Xout = [];
    Yout = [];
else
    Xout = Xout{1};
    Yout = Yout{1};
end

% If there are many intersection points, inspect if these coincide
if length(Xout) > 1
    [Xout,Yout] = resolve_multi_intersects(Xout,Yout);
end

%     if isempty(X)
%         % Select the points within the polygons of focus
%         
%         [x,y] = polysplit(studyA.X,studyA.Y);
% 
%         x1 = cell(length(x),1);
%         y1 = cell(length(y),1);
%         for m = 1 : length(x)
%             [x1{m}, y1{m}] = curveintersect(x_all,y_all,x{m},y{m});
%         end
% 
%         if any(cellfun(@(x) ~isempty(x),x1))
%             assert(nnz(cellfun(@(x) length(x),x1)==0)==length(x1)-1 && ...
%                 nnz(cellfun(@(x) length(x),y1)==0)==length(y1)-1)
% 
%             X = [X; cell2mat(x1)];
%             Y = [Y; cell2mat(y1)];
%         end
% 
%         clear x1 y1
% 
% %                 line(x_all,y_all,'color','r','linewidth',2)
% %                 plot(X(1),Y(1),'ro')
% %                 plot(X(2),Y(2),'ro')
% %                 plot(X(3),Y(3),'ro')
% 
%         if length(X) > 1
%             [X,Y] = resolve_multi_intersects(X,Y);
%         end
%     end
end

%%
function [X,Y] = resolve_multi_intersects(X,Y)
% Resolve multiple intersects
% resolve_multi_intersects returns the intersection points (X,Y) that is a
% subset of the input intersection points (X,Y). The subset is created to
% eliminate intersections from polylines running along each other.

% Version 0.1, 26 Oct 2011, Created, KK

% Copyright 2011 Kostas Konstantinou

% This code is for polylines that penetrate multiple counties
Same = (repmat(X,1,length(X)) == repmat(X.',length(X),1)) & ...
    (repmat(Y,1,length(Y)) == repmat(Y.',length(Y),1));
Same(sub2ind(size(Same),1:size(Same,1),1:size(Same,2))) = false;
Same = triu(Same);
[r,c] = find(Same);
% if length(r) > 1
%     sdfgdf
% end
X([r,c]) = [];
Y([r,c]) = [];
clear Same r c

% This part of the code is for capturing very short in/outs
if length(X) > 1
    distm = sqrt((repmat(X,1,length(X)) - repmat(X.',length(X),1)).^2 + ...
        (repmat(Y,1,length(Y)) - repmat(Y.',length(Y),1)).^2);
    distm = triu(distm)>0 & triu(distm)<5;
    [r,c] = find(distm);
%     if length(r) > 1
%         sdfgdf
%     end
    X([r,c]) = [];
    Y([r,c]) = [];
    clear distm r c
end

if length(X) > 1
    if sqrt((max(X)-min(X)).^2 + (max(Y)-min(Y)).^2) < 5000
        % This code is for roads that run along boundaries
        X = median(X);
        Y = median(Y);
    else
        srter
    end
end
end

% %% Convert array of objects to object of arrays
% function poin = arraOfObj2objOfArr(poin)
% % arraOfObj2objOfArr converts array of objects to object of arrays
% %
% % * v0.1 KK 16Aug17 Added header
% 
% % poin2 = POIN();
% poin2.X = [poin.X].';
% poin2.Y = [poin.Y].';
% % poin2.pixX = [poin.pixX].';
% % poin2.pixY = [poin.pixY].';
% % poin2.X_COORD = {poin.X_COORD}.';
% % poin2.Y_COORD = {poin.Y_COORD}.';
% % poin2.smaAreI = [poin.smaAreI].';
% % poin2.pop = [poin.pop].';
% poin2.leng = [poin.leng].';
% poin2.maxspeed = [poin.maxspeed].';
% % poin2.distClosesBuiM = [poin.distClosesBuiM].';
% poin2.clu = [poin.clu].';
% poin = poin2;
% end

%% Return minimum distance between line segment vw and point p
function y = minimum_distance(v,w,p)
% https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
% Consider the line extending the segment, parameterized as v + t (w - v).
% We find projection of point p onto the line. 
% It falls where t = [(p-v) . (w-v)]/|w-v|^2
% We clamp t from [0,1] to handle points outside the segment vw.

assert(size(v,1)==2)

% Calculate |w-v|^2 length_squared
l2 = sum((w-v).^2);

% Remove lines without length
if any(l2==0)
    w(:,l2==0) = [];
    v(:,l2==0) = [];
    l2(l2==0) = [];
end

% Calculate the minimum distance between line segment vw and point p
if length(l2) > 1
    t = max(0,min(1,dot(bsxfun(@minus,p,v),w-v)./l2));
    projection = v + bsxfun(@times,t,w-v);
    [~,minI] = min(sqrt(sum(bsxfun(@minus,p,projection).^2)));
    y = projection(:,minI);
else
    y = nan(2,1);
end

% figure
% axis equal
% hold on
% line([v(1,:);w(1,:)],[v(2,:);w(2,:)])
% plot(p(1),p(2),'o')
% plot(projection(1,:),projection(2,:),'x')
% plot(projection(1,minI),projection(2,minI),'s')
end
