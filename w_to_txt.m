function w_to_txt(Sim,header,data)
%% This function writes the info to a txt file. The info consists of a header  and data cell arrays
% If the file exists already then the info will added to the existing file

%% Inputs:
    % foldername: the name of the folder where the file should be saved
    % filename: Name of the file without the txt extension
    % header: Header of the info to be written in cell format. This is usually a vector with dimension of 1xM cells (string format in each cell)
    % data: Data to be written to the file. This is a cell array with dimension MxN, (string format in each cell)

%% Version history
% v0.1, 25/02/19, Created, HO
% v0.2, 28/02/19, little change to the code to let it accept header with length=1, HO


% Open the file
fileID = Sim.path.logPropagFileID;

% Check if the header is a complete one (this means this is the full header, otherwise (one cell) there is no need to write info about the header)
if length(header)~=1
    % Assert that inputs are valid
    assert(length(header)==size(data,1) || length(header)==size(data,2), 'Number of columns in header and data should be the same')
    % Write the header info
    for n=1:1:length(header)
        fprintf(fileID,'%12s\t',header{n}); % 12s means each column is a string with 12 characters
    end
    fprintf(fileID,'\r\n');
    % fprintf(fileID,'%6s %12s\r\n', header); % header (6s: 6 letters per colum)
end
    
% Write the data
%fprintf(fileID,'%6.2f %12.8f\r\n', data); % Info
% fprintf(fileID,'\n');
for ii = 1:size(data,1)
    fprintf(fileID,'%12s\t',data{ii,:}); % 12s means each column is a string with 12 characters
    fprintf(fileID,'\r\n');
end
