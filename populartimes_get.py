# populartimes_get.py
# https://github.com/m-wrzr/populartimes

import populartimes

def populartimes_get(GmAPIkey,plaTy,p1,p2):
    """
    Rus populartimes.get.

    Parameters
    ----------
    GmAPIkey : str; api key from google places web service; e.g. "your-api-key"
    plaTy : [str]; placetypes; see https://developers.google.com/places/supported_types; e.g. ["bar"]
    p1 : (float, float); lat/lng of point delimiting the search area; e.g. (48.132986, 11.566126)
    p2 : (float, float); lat/lng of point delimiting the search area; e.g. (48.142199, 11.580047)
    
    Returns
    -------
    variable : see https://github.com/m-wrzr/populartimes

    """

    variable = populartimes.get(GmAPIkey,plaTy,p1,p2)
    
    return variable