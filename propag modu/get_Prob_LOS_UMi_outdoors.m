function p_LOS = get_Prob_LOS_UMi_outdoors(d_km)

% compute LOS probability for Micro Urban scenario - outdoor [Metis D1.4]
% The same formula is suggested by 3GPP TR 38.901 (2017), METIS (2015), and
% mmMAGIC (2017) [1].
% 
% The UMi scenarios include high user density open areas and street canyons
% with BS heights below rooftops (e.g., 3�20 m), UE heights at ground level
% (e.g. 1.5 m), and intersite distances (ISDs) of 200 m or less. 
% 
% 5GCM (2015) provides two LOS probability models, the first one is
% identical in form to the 3GPP TR 38.901 outdoor model, but with slightly
% different curve-fit parameters (d1 and d2). The second LOS probability
% model is the NYU squared model, which improves the accuracy of the d1/d2
% model by including a square on the whole term. The NYU model was
% developed using a much finer resolution intersection test than used by
% 3GPP TR 38.901, and used a realworld database in downtown New York City.
% For UMi, the 5GCM d1/d2 model has a slightly smaller mean square error
% (MSE), but the NYU squared model has a more realistic and rapid decay
% over distance for urban clutter.
% 
% [1] RAPPAPORT et al.: OVERVIEW OF mmWAVE COMMUNICATIONS FOR 5G WIRELESS
% NETWORKS, IEEE TRANSACTIONS ON ANTENNAS AND PROPAGATION, VOL. 65, NO. 12,
% DECEMBER 2017

d = d_km .* 1000;
d_min = 18;
p_LOS = ones(size(d));

IND = d > d_min;
p_LOS(IND) = 18./d(IND) + (1-18./d(IND)).*exp(-d(IND)./36);
