classdef PATH
    % PATH class for management of paths between BS and MS
    % The PATH class is responsible for setting the properties of paths
    % between a BS and MS. More specifically PATH is responsible for
    % delivering the following:
    %
    % *	creates PATH objects
    
    properties
%         X;  % Vector with easting in metres
%         Y;  % Vector with northing in metres
        clu;  % Vector with the row on clu.tran
        d;  % Vector with path profile parameter d
        h;  % Vector with path profile parameter h
        g;  % Vector with path profile parameter g
%         isWater;  % Logical vector with true if the path point is water body
%         lat;  % latitude of the middle of the path
%         lon;  % longitude of the middle of the path
        termCluLossMod;  % Terminal clutter loss model. 1 is for (64a), 2 is for (64b).
%         Xmid;  % easting of the mid-point of the path
%         Ymid;  % northing of the mid-point of the path
%         DeltaN;  % meteorological data
%         N0;  % meteorological data
        N;  % length
	end
    
    methods
function Path = PATH()
%%  PATH class constructor
% PATH is the class constructor of PATH objects, Path. [WIP]
%
% * v0.1 KK Dec16 Created
% * v0.2 AK Jan17 Changed the Path for array of structs to struct arrays for speed
% * v0.3 AK Jan17 Changed the minvtran to a more simplified version, eqaInv, to boost performance

if nargin ~= 0  % Allow nargin == 0 syntax
    % figure('unit','in','pos',[1 1 7.5 2.63],'paperpositionmode','auto')
    % hold on
    % plot([Path.d].*1e3,[Path.h])
    % plot([Path.d].*1e3,[Path.g],'r')
    % % plot(0,hts(BSn(n)),'ro')
    % % plot(0,hts(BSn(n)),'r.')
    % % plot(di(end).*1e3,hrs(UEn(n)),'r.')
    % grid on
    % xlabel('Travel distance  (m)')
    % ylabel('Height  (m asl)')

    % figure('unit','in','pos',[1 1 3.5 2.63],'paperpositionmode','auto')
    % hold on
    % plot(Cell.X,Cell.Y,'ro')
    % plot(Cell.X,Cell.Y,'r.')
    % plot(pix.X,pix.Y,'.')
    % plot([Cell.X;pix.X],[Cell.Y;pix.Y])
    % axis equal
end
end

function Path = s_parame(Path,Cell,pixX,pixY,d,Sim,eleva,clu,DN50,N050)
%% Set properties of Path objects
% [Description]
%
% * v0.1 KK Dec16 Created

% Ofcom recommendations, A1.14, 4G Coverage Obligation Notice of Compliance Verification Methodology: LTE
% if d == 0
%     d = max((BS(BSn(n)).H-UEh)./1e3, 2./1e3);
% end

% Formatting
if size(pixX,1) > size(pixX,2)
    pixX = pixX.';
    pixY = pixY.';
    d = d.';
end

% Simplifications
% clu_tran = cellfun(@double,clu.tran(3:end,[8,9,10,13]));

% Calculate the path trajectory
N = max(1+ceil(d.*1e3./Sim.propag.Deltadt),3);  % Ofcom recommendations, A1.13, 4G Coverage Obligation Notice of Compliance Verification Methodology: LTE
Ncell = num2cell(N);
% [C,ia,ic] = unique(N);
% clear C ia ic
t = cellfun(@(N) linspace(0,1,N),Ncell,'UniformOutput',false);  % t parameter of parametric form of the line
% testX = cell2mat(cellfun(@(t,pixX) Cell.X+t.*(pixX-Cell.X),...
%     t,num2cell(pixX),'UniformOutput',false));
% testY = cell2mat(cellfun(@(t,pixY) Cell.Y+t.*(pixY-Cell.Y),...
%     t,num2cell(pixY),'UniformOutput',false));
tmp = Cell.X + 1i*Cell.Y + cell2mat(cellfun(@(t,pixX) t.*(pixX-Cell.X-1i*Cell.Y),...
    t,num2cell(pixX+1i*pixY),'UniformOutput',false));
X = real(tmp);
Y = imag(tmp);
clear tmp

% Calculate the d path profile parameter
Path.d = cellfun(@(t,d) single(t.*d),t,num2cell(d),...
    'UniformOutput',false).';  % (1a) of ITU.R P.1812
Path.N = N.';

clear t

% Calculate the number of sample points of each path
start = [0,cumsum(N(1:end-1))] + 1;
fini = cumsum(N);

% Calculate the path clutter
% [pathClutt,~] = find(cell2mat(arrayfun(@(x) inpolygon([Path.X],[Path.Y],...
%     x.X,x.Y),clu.S,'UniformOutput',false)));
% assert(length(pathClutt)==length(Path))
% tmp = num2cell(pathClutt);
% [Path(:).clu] = deal(tmp{:});
[I,J] = clu.R.worldToDiscrete(X,Y);
Path_clu = clu.A(sub2ind(size(clu.A),I,J));
tmp = mat2cell(Path_clu,1,N);
Path.clu = tmp.';

% Convert to path clutter height
% % Set unavailable clutter to open
% % pathClut(pathClut==0) = 7;
% colSub = ones(1,length(Path_clu));
% colSub(start) = 2;
% colSub(fini) = 2;
% representaCluttHeig = clu_tran(sub2ind(size(clu_tran),Path_clu,colSub));
    
% Calculate the h path profile parameter
[I,J] = eleva.R.worldToDiscrete(X,Y);
% Path_h = double(eleva.A(sub2ind(size(eleva.A),I,J)));
Path_h = eleva.A(sub2ind(size(eleva.A),I,J));
tmp = mat2cell(Path_h,1,N);
Path.h = tmp.';

% Calculate the g path profile parameter
% representaCluttHeig = double(clu.A(sub2ind(size(eleva.A),I,J))) - Path_h;
representaCluttHeig = clu.A(sub2ind(size(eleva.A),I,J)) - Path_h;
tmp = mat2cell(Path_h+representaCluttHeig,1,N);  % (1c) of ITU.R P.1812
Path.g = tmp.';

% Update isWater
% Path.isWater = mat2cell(clu_tran(Path_clu,4),N,1);

% Update termCluLossMod
% Path.termCluLossMod = [clu_tran(Path_clu(start),3),...
%     clu_tran(Path_clu(fini),3)];
Path.termCluLossMod = ones(length(Path.N),2);

% Unproject the mid-point of the path from simulation map to geographic
% coordinates
% N2 = ceil(N./2);
% Xmid = X(start+N2-1);
% Ymid = Y(start+N2-1);
% % [lat,lon] = minvtran(Sim.mstruct,Xmid,Ymid);
% [lat,lon] = easNor2latLon(Xmid,Ymid,Sim.mstruct.ostn);

% Find the location of the centre of the path within the database grid
% XI = 121 - (lat+90)./1.5;
% YI = wrapTo360(lon)./1.5 + 1;

% Perform bilinear interpolation
% Path.DeltaN = biLineInterp(row,col,DN50,XI,YI);
% Path.N0 = biLineInterp(row,col,N050,XI,YI);
% tmp = 121 - floor((lat+90)./1.5);
% row = min(tmp)-1 : max(tmp);
% tmp = floor(wrapTo360(lon)./1.5) + 1;
% col = min(tmp) : max(tmp)+1;
% [X,Y] = meshgrid(row,col);
% Path.DeltaN = interp2(X,Y,DN50(row,col).',XI,YI).';
% Path.N0 = interp2(X,Y,N050(row,col).',XI,YI).';

% Update lat
% % lat = num2cell(lat);
% % lon = num2cell(lon);
% Path.lat = lat.';
% % [Path.lon] = deal(lon{:});
end

function Path2 = se(Path,IND)
% Select paths

Path2 = PATH();
names = fieldnames(Path);
for n = 1 : length(names)
    if ~isempty(Path.(names{n}))
        Path2.(names{n}) = Path.(names{n})(IND,:);
    end
end
end
    end
end

% This function is not needed in this implementation, but it is useful for
% bi-linear interpolation
% function Path = c_h(Path,eleva)
% %% [Title]
% % [Description]
% %
% % * v0.1 KK 15Dec16 Created
% 
% % Simplify
% X = Path.X;
% Y = Path.Y;
% cellsize = eleva.R.DeltaX;
% xllcenter = eleva.R.XLimWorld(1) + cellsize./2;
% yllcenter = eleva.R.YLimWorld(1) + cellsize./2;
% nrows = eleva.R.RasterSize(1);
% 
% % Bi-linear interpolation, ITU-R P.1144-6
% N = size(eleva.A);
% C = floor((X-xllcenter+cellsize)./cellsize);
% % At the limits use reflection
% C( C == 0 ) = 1;
% C( C >= N( 2 ) ) = N( 2 ) - 1;
% R = floor((-Y+yllcenter+nrows.*cellsize)./cellsize);
% R( R == 0 ) = 1;
% R( R >= N( 1 ) ) = N( 1 ) - 1;
% 
% c = (X-xllcenter+cellsize)./cellsize;
% r = (-Y+yllcenter+nrows.*cellsize)./cellsize;
% elevaVal = eleva.A(sub2ind(N,R,C)) .* (R+1-r) .* (C+1-c) + ...
%            eleva.A(sub2ind(N,R+1,C)) .* (r-R) .* (C+1-c) + ...
%            eleva.A(sub2ind(N,R,C+1)) .* (R+1-r) .* (c-C) + ...
%            eleva.A(sub2ind(N,R+1,C+1)) .* (r-R) .* (c-C);
% 
% % Update field
% Path.h = elevaVal;
% end
