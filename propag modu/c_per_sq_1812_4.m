function Cell = c_per_sq_1812_4(Cell,fileName,Sim,clu,ter,dem,vx,vy,...
    studyA,BoundingBox,spotFreqMhz,UE,fn,Un,maxDist_m,sty,dsm)
% c_per_sq calculates the propagation of cells within an OS square. fileName is the string that
% corresponds to the name of the OS square. Sim is SIM object. Cell is an array of CELL objects. clu
% is a CLU object. ter is a TER object. pop, thorou, and geographi are POIN objects. vx and vy are
% the extent of coverage based on Voronoi geometry. DN50 and N050 are tables defined in 1812-4.
% c_per_sq saves on the hard disk and does not produce any output.
%
% * v0.1 KK 31Jul17 Added headers

% Simplification
hm = UE{2};  % UE height
clu_tran = UE{3};  % clutter translation
loc = UE{4};  % UE locations

Hash = arrayfun(@(x) DataHash(x),...
    struct('X',{Cell.X},...
    'Y',{Cell.Y},...
    'heiAgl',{Cell.heiAgl},...
    'spotFreqMhz',spotFreqMhz,...
    'hm',hm,...
    'clu_tran',clu_tran,...
    'propMod',Sim.propag.mod,...
    'loc',{loc}),'UniformOutput',false);

switch sty
  case 'pop'
    proc = true;
  case 'geographi'
    proc = false;
  otherwise
    error('Undefined')
end

% Check if we have results for this square
if proc
    [S,exitFlag] = loadDB(fullfile(Sim.path.DB.Lbc,fileName),'Lbc');
else
    exitFlag = -1;
end
if exitFlag == 0
	Lbc = S.Lbc;
else
	% File did not exist
    Lbc = struct('X',{},'Y',{},'heiAgl',{},'spotFreqMhz',{},'A',{},...
        'DataHash',{},'demSour',[]);
end
% Check if the current cell(s) have entries in the Lbc database
[~,LbcI] = ismember(Hash,{Lbc.DataHash});
col_idx_clut_h_extreme = 7;  % 'for i=1 and n in (1c)'
% For each site, calculate path loss
isSave = false;
for cn = 1 : length(Cell)
    % Calculate the row and column in the clutter database at the location of BS
    [row_clu,col_clu] = worldToSub(clu.R,Cell(cn).X,Cell(cn).Y);
    
    % Calculate the path loss exponent
    switch Sim.propag.mod
      case 'Extended_HATA'
        if Cell(cn).heiAgl >= clu.tran.(clu_tran){clu.A(row_clu,col_clu)+1,...
                col_idx_clut_h_extreme}
            % Above rooftop
            if spotFreqMhz < 6000
                Cell(cn).n(fn,Un) = (44.9 - ...
                    6.55.*log10(max(30,Cell(cn).heiAgl))) ./ 10;
            else
                error('Code this!')
            end
        else
            % Below rooftop
            if spotFreqMhz < 6000
                Cell(cn).n(fn,Un) = 2.5;
            else
                beep
                disp('Code IND as a few paragrpahs below')
                keyboard
                Cell(cn).n(fn,Un) = round((2.1.*nnz(IND) + ...
                    1.85.*nnz(~IND)) ./ length(IND),2);
            end
        end
      case 'ITU_1812_4'
        % Re-use the path loss exponent from Hata, since this is not
        % available for ITU 1812-4 model
        Cell(cn).n(fn,Un) = (44.9 - ...
            6.55.*log10(max(30,Cell(cn).heiAgl))) ./ 10;
      otherwise
        error('Undefined')
    end
    
    % If the current cell has an entry check extent, else calculate a new Lbc entry
    if LbcI(cn)>0 && proc
        % Check if the bounding box of the calculated Lbc is equal or greater than what needs be
        % calculated now - if yes then continue
        if Lbc(LbcI(cn)).BoundingBox(1,1) <= BoundingBox{cn}(1,1) && ...
                BoundingBox{cn}(2,1) <= Lbc(LbcI(cn)).BoundingBox(2,1) && ...
                Lbc(LbcI(cn)).BoundingBox(1,2) <= BoundingBox{cn}(1,2) && ...
                BoundingBox{cn}(2,2) <= Lbc(LbcI(cn)).BoundingBox(2,2)
            % Log the path loss values and continue
            Cell(cn).Lbc{fn,Un}         = Lbc(LbcI(cn)).A;
            Cell(cn).BoundingBox{fn,Un} = BoundingBox{cn};                        
            continue
        else
            % The propagation needs to be extended
            BoundingBox{cn} = [...
                min(BoundingBox{cn}(1,1),Lbc(LbcI(cn)).BoundingBox(1,1)),...
                min(BoundingBox{cn}(1,2),Lbc(LbcI(cn)).BoundingBox(1,2))
                max(BoundingBox{cn}(2,1),Lbc(LbcI(cn)).BoundingBox(2,1)),...
                max(BoundingBox{cn}(2,2),Lbc(LbcI(cn)).BoundingBox(2,2))];
            %             plot(BoundingBox2([1 1 2 2 1], 1), BoundingBox2([1 2 2 1 1], 2))
            Ln = LbcI(cn);
        end
    else
        % The propagation is new
        Ln = length(Lbc) + 1;
    end
    
    X = dem(:,1);
    Y = dem(:,2);
    clu2 = clu.A(sub2ind(size(clu.A),dem(:,3),dem(:,4)));
    isSave = true;
    
    % Set the frequency for path loss calculations
    spotFreqGhz = spotFreqMhz ./ 1e3;
    
    % Calculate path loss if path loss estimation points are within a sensible distance from a site.
    % There is a maximum cutoff distance due to RACHpream and an extended Voronoi to combine.
    %     BoundingBox = Cell(cn).BoundingBox;
    isClo1 = find(BoundingBox{cn}(1,1)<=X & X<=BoundingBox{cn}(2,1) & ...
        BoundingBox{cn}(1,2)<=Y & Y<=BoundingBox{cn}(2,2));
    A = [];
    if ~isempty(isClo1)
        distm = sqrt((X(isClo1)-Cell(cn).X).^2 + (Y(isClo1)-Cell(cn).Y).^2);
        distkm = distm ./ 1e3;
        
        % Find path loss estimation points are within a sensible distance from a site
        if ~all(isinf(vx{cn}))
            isClo2 = find(distm<=maxDist_m{cn} & ...
                inpolygon(X(isClo1),Y(isClo1),vx{cn},vy{cn}));
        else
            isClo2 = find(distm<=maxDist_m{cn});
        end
        
        if ~isempty(isClo2)
            % Construct path object
            Path = PATH();
            Path = Path.s_parame(Cell(cn),...
                X(isClo1(isClo2)),Y(isClo1(isClo2)),...
                distkm(isClo2),Sim,ter,dsm,Sim.DN50,Sim.N050);
            
            % Simplify
            clu2 = clu2(isClo1(isClo2));
            
            % Calculate SD of location variability (external situation)
            u = zeros(length(isClo2),1);
            isWater = ismember(clu2,clu.water);
            
            % The ground height of the end points
            h = cellfun(@(pat) pat(end),Path.h);
            % The ground and clutter height of the end points
            g = cellfun(@(pat) pat(end),Path.g);
            
            if any(~isWater)
                % On land
                IND1 = hm + h < g;
                if any(IND1)
                    u(IND1) = 1;
                end
                if any(~IND1)
                    IND2 = g <= hm + h & hm + h < g + 10;
                    if any(IND2)
                        u(~IND1&IND2) = 1 - (hm+h(~IND1&IND2)-g(~IND1&IND2))./10;
                    end
                end
            end
            
            % For the frequencies above 3 GHz, we need to add the rain fading and the
            % atmospheric absorption
            %             chaI = Sim.linBud.freq == Cell(cn).spotFreqMhz;
            %             Aatm = Sim.propag.atmDBkm(chaI) * distkm(isClo2);
            %             % The rain fading for each point
            %             rainFad = Sim.propag.gammaR(chaI) * distkm(isClo2);
            Aatm = 0; rainFad = 0;
            
            if spotFreqMhz <= 6000
                % The end clutter points of the paths
                endClut = cellfun(@(x) x(end),Path.clu);
                % By default, assume that all are rural
                sh_SD = 4.4 * ones(size(endClut));
                % Urban/suburban
                sh_SD(ismember(endClut,[clu.urban clu.suburban])) = 5.1;
                sh_SD = sh_SD + 1.3 .* log10(spotFreqMhz);
                sigmaLe = (sh_SD + interp1([0 0.5 2 50 100],[0 0 2 4 4],...
                    distkm(isClo2))) .* u;
                
                % For each point that is sensible distance from a site,
                % calculate path loss and SD of shadowing
                A = [dem(isClo1(isClo2),3:4),c_itu_1812_4(Cell(cn...
                    ).heiAgl,hm,'vertical',Path,spotFreqGhz,50,true),...
                    sigmaLe,distm(isClo2),zeros(length(isClo2),3)];
            else
                % THIS NEEDS UPDATING
                beep; keyboard;
                % The LoS vector; if the height of there is a clutter height higher than the ray
                % from the Tx to the Rx
                isLoS = cellfun(@(hei,clu,dist) ~any(...
                    (Cell(cn).heiAgl + hei(1) - hm - hei(end)) / dist(end) >=...
                    (Cell(cn).heiAgl + hei(1) - clu(2 : end - 1)) ./ dist(2 : end - 1) ),...
                    Path.h,Path.g,Path.d);
                % The standard deviation
                sigmaLe = 1.7 .* isLoS + 6.7 .* (~isLoS);
                % The path loss
                A = [poinID(isClo1(isClo2)),...
                    give_pathLoss_mmWave(spotFreqGhz,distkm(isClo2) * 1e3,isLoS,'RMa'),...
                    sigmaLe];
            end
            
            % For the frequencies above 3 GHz, we need to add the rain fading and the
            % atmospheric absorption
            A(:,3) = A(:,3) + Aatm + rainFad;
            
            % Calculate:
            %   h - terrain (ground) height asl (above mean sea level)
            %   v1 - antenna boresight unit vector (commented out as not needed)
            %   v2 - antenna to UE unit vector
            %   az - azimuth of v2 wrt v1 (commented out as not needed)
            %   elev - elevation of v2 wrt v1 (commented out as not needed)
            [I,J] = ter.R.worldToDiscrete([Cell(cn).X; dem(isClo1(isClo2),1)],...
                [Cell(cn).Y; dem(isClo1(isClo2),2)]);
            h = ter.A(sub2ind(size(ter.A),I,J));
%             v1 = [cosd(90-double(Cell(n).az)).*sind(90+Cell(n).tilt)
%                 sind(90-double(Cell(n).az)).*sind(90+Cell(n).tilt)
%                 cosd(90+Cell(n).tilt)].';
            v2 = [dem(isClo1(isClo2),1:2) hm+h(2:end)] - ...
                repmat([Cell(cn).X Cell(cn).Y Cell(cn).heiAgl+h(1)],...
                length(isClo2),1);
            v2 = v2 ./ repmat(sqrt(sum(v2.^2,2)),1,3);
%             az(n,:) = wrapTo180(atan2d(v2(:,2),v2(:,1)) - ...
%                 atan2d(v1(2),v1(1)));
%             elev = abs(acosd(v2(:,3)) - acosd(v1(3)));

            % []
            A(:,6:8) = v2;
        end
    end
    
    % Append entry
    Lbc(Ln).X           = Cell(cn).X;
    Lbc(Ln).Y           = Cell(cn).Y;
    Lbc(Ln).heiAgl      = Cell(cn).heiAgl;
    Lbc(Ln).hm          = hm;
    Lbc(Ln).spotFreqMhz = spotFreqMhz;
    Lbc(Ln).DataHash    = Hash{cn};
    Lbc(Ln).n(Un)       = Cell(cn).n(fn,Un);
    Lbc(Ln).A           = A;
    Lbc(Ln).clu_tran    = clu_tran;
    Lbc(Ln).loc         = loc;
    Lbc(Ln).BoundingBox = BoundingBox{cn};
           
    Cell(cn).Lbc{fn,Un}      = A;
    Cell(cn).BoundingBox{fn,Un} = BoundingBox{cn};
    
    clear X Y
end

if proc
    % Save the path loss calculations for that square into the database, only if we have added new
    % information into it. Avoiding thus creating empty files.
    if exist(fullfile(Sim.path.DB.Lbc,fileName),'file')
        % Release the file
        fileattrib(fullfile(Sim.path.DB.Lbc,fileName),'+w');
    end
    if isSave
        % Save the results
        waitCou = 0;
        decade  = 1;
        try
            save(fullfile(Sim.path.DB.Lbc,fileName),'Lbc')
        catch
            waitCou = waitCou + 1;
            if waitCou < decade * 100
                pause(rand())
            else
                decade = decade + 1;
                keyboard
            end
        end
    end
end
