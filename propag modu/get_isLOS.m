function isLOS = get_isLOS(Cell,dem,h_agl_ue,clu,studyA,ter,...
    col_idx_clut_h_extreme,col_idx_clut_h_intermediate,Sim,clu_tran,...
    get_Prob_LOS)
%% [Title]
% [Description]
%
% * v1.0 CG XXJun16 Created
% * v1.1 KK 10Aug16 Review
% * v1.2 KK 11Aug16 Review

% create output vector
isLOS = false(1,size(dem,1));
P_LOS = zeros(1,size(dem,1));

% initialize variable with parameters
x_bs = Cell.X;
y_bs = Cell.Y;
x = dem(:,1);
y = dem(:,2);

% Path length, d, km
d_km = sqrt((x_bs-x).^2+(y_bs-y).^2) ./ 1e3;

% if Sim.propag.isLOSc
% initialize variable with parameters
assert(clu.R.CellExtentInWorldX==clu.R.CellExtentInWorldY)
Deltadt = clu.R.CellExtentInWorldX;  % Target profile spacing
h_agl_bs = Cell.heiAgl;

% loop over pixels (NB linspace does not work with matrices, hence we need with loop)
if ~isempty(x)
    XWorldLimits = clu.R.XWorldLimits;
    YWorldLimits = clu.R.YWorldLimits;
    CellExtentInWorldX = clu.R.CellExtentInWorldX;
    CellExtentInWorldY = clu.R.CellExtentInWorldY;
end
for n = 1 : length(x) 
    %%% Calculate the path trajectory

    % number of points along the path 
    N = max(1+ceil(d_km(n).*1e3./Deltadt), 3);  % Ofcom recommendations, A1.13, 4G Coverage Obligation Notice of Compliance Verification Methodology: LTE

    % create height path using linear interpolation
    Path.X = linspace(x_bs,x(n),N);
    Path.Y = linspace(y_bs,y(n),N);
    Path.h_agl = linspace(h_agl_bs,h_agl_ue,N);

    % Calculate the path clutter
    J = round((Path.X-XWorldLimits(1))./CellExtentInWorldX+0.5);
    I = round(-(Path.Y-YWorldLimits(2))./CellExtentInWorldY+0.5);
    tmp = clu.tran.(clu_tran);
    pathClutHeight = [tmp{clu.A(I(1),J(1))+1,col_idx_clut_h_extreme},...
        cell2mat(tmp(clu.A(sub2ind(size(clu.A),I(2:end-1),J(2:end-1)))+1,col_idx_clut_h_intermediate)).',...
        tmp{clu.A(I(end),J(end))+1,col_idx_clut_h_extreme}];

    % Calculate the clutter and terrain path profile parameters
    [I,J] = ter.R.worldToDiscrete(Path.X,Path.Y);
    hi = ter.A(sub2ind(size(ter.A),I,J));  % h is the terrain (ground) height asl (above mean sea level)
    gi = hi + pathClutHeight;  % g is the path profile height asl

    % calculate clutter plus terrain path from BS to UE
    Path.h_asl = linspace(h_agl_bs+hi(1),h_agl_ue+hi(N),N);

    % check whether terrain and clutter obstruction determines LOS/NLOS
    % propagation
    if all(Path.h_asl>=gi)
        % Deterministic LOS
        isLOS(n) = true;
    elseif any(Path.h_asl<=hi)
        % Deterministic NLOS
        isLOS(n) = false;
    else
        % Probabilistic LOS/NLOS
        rng(sum(sort([sub2ind(size(studyA.INfoc),Cell.I,Cell.J),...
            sub2ind(size(studyA.INfoc),dem(n,3),dem(n,4))]) + ...
            [0 numel(studyA.INfoc)]) + Sim.seed.LOSprob)
%         P_LOS = get_Prob_LOS(d_km(n));
%         isLOS(n) = rand() < 1-(1-P_LOS).*nnz(Path.h_asl<gi)./length(gi);
        P_LOS(n) = get_Prob_LOS(d_km(n).*nnz(Path.h_asl<gi)./length(gi));
        isLOS(n) = rand() < P_LOS(n);
    end
end

%% Plots
% % Common for maps
% [lat,lon] = minvtran(Sim.mstruct,...
%     bsxfun(@plus,dem(:,1),Sim.SA.pixSize./2.*[-1 -1 1 1 -1]),...
%     bsxfun(@plus,dem(:,2),Sim.SA.pixSize./2.*[-1 1 1 -1 -1]));
% 
% % Map of isLOS
% name = ['isLOS ' Cell.siteID ' ' num2str(size(dem,1))];
% k = kml(name);
% for n = 1 : size(lat,1)
%     % AABBGGRR
%     if isLOS(n)
%         polyColor = 'BB00FF00';
%     else
%         polyColor = 'BB0000FF';
%     end
%     k.poly3(lon(n,:),lat(n,:),[1 1 1 1 1].*h_agl_ue,...
%         'polyColor',polyColor,'extrude',false,'lineWidth',0,...
%         'altitudeMode','relativeToGround');
% end
% k.point(Cell.lon,Cell.lat,Cell.heiAgl,'iconURL','wht-stars',...
%     'iconScale',1,'name',name);
% k.run;
% 
% % Map of P_LOS
% Cmap = {-inf 0 'x=0' 165 42 42
%     0 0.02 '0<x<2%' 255 0 0
%     0.02 0.025 '2%\lex<2.5%' 255 192 203
%     0.025 0.03 '2.5%\lex<3%' 255 165 0
%     0.03 0.035 '3%\lex<3.5%' 0 0 255
%     0.035 0.04 '3.5%\lex<4%' 255 255 0
%     0.04 0.1 '4%\lex<10%' 0 255 255
%     0.1 inf 'x\ge10%' 0 255 0};
% name = ['P_LOS ' Cell.siteID ' ' num2str(size(dem,1))];
% k = kml(name);
% for n = 1 : size(Cmap,1)
%     % AABBGGRR
%     IND = find(Cmap{n,1}<P_LOS & P_LOS(:,2)<=Cmap{n,2});
%     if ~isempty(IND)
%         str = flipud(dec2hex(cell2mat(Cmap(n,4:6)),2));
%         str = str.';
%         str = str(:).';
%         polyColor = ['BB' str];
%         for m = 1 : length(IND)
%             k.poly3(lon(IND(m),:),lat(IND(m),:),[1 1 1 1 1].*h_agl_ue,...
%                 'polyColor',polyColor,'extrude',false,'lineWidth',0,...
%                 'altitudeMode','relativeToGround');
%         end
%     end
% end
% k.point(Cell.lon,Cell.lat,Cell.heiAgl,'iconURL','wht-stars',...
%     'iconScale',1,'name',name);
% k.run;
