function LdBnoAngleNoMCL = give_LdBnoAngleNoMCL(f,d,hb,hm,clut, Abov_2GHz)
%% This function Calculates the median path loss according to SEAMCAT extended Hata
% see SEAMCAT manual at http://tractool.seamcat.org/wiki/Manual
% Notes about the validity of the model:
%       The model is valid when hb range is between 30 to 200m. If antenna height is above 200m, then this might lead to significant error.
%       This model is valid for hM from 1m up to 10m. If it's below 1m, a value of 1m should be used instead.
%       This model does not work properly above 3GHz.

%% Inputs:
%       d: 2D separation distance between BS and UE in km
%       hb:  Height of the BS in m. In other words, this is max(ht,hr)
%       f: Frequency in MHz
%       hm: Height of the UE in m. In other words, this is min(ht,hr)
%       clut: Clutter type, Urban clut=1, Suburban clut=3, Open clut=4.  Dense urban clut=2, however dense urban is not in the official spec. of the model. This has been added later.
%       Abov_2GHz: two options available for frequency above 2GHz:'Extended_Hata' and 'RW_Ofcom'. The second option is valid only for certain frequencies above 2GHz.

%% Outputs:
%       LdBnoAngleNoMCL: PL in dB

%% Version Control
% v0.1, Kostas Konstantinou, Created, 5May09
% v0.2, HO, Add comments before testing, 03Aug18
% v0.3, HO, Implement original version of Extended Hata for frequency above 2GHz. Keep in the new version the previous implemenation of KK (agreed
% with SS and Ofcom for certain frequencies t 2GHz). Add a new switch and an input for this function to deal with this new case.


%% Nargin
if nargin == 0
    d = 21;  % 2D separation distance between BS and UE 21km
    hb = 30;  % Height of the BS 30m
    f = 700;  % Frequency 700MHz
    hm = 1.5;  % Height of the UE 1.5m
    clut = 1;  % Urban clutter type
    Abov_2GHz = 'Extended_Hata';  % used the original version of extended Hata for PL above 2GHz
end

%% Input validation
% assert(strcmp(Abov_2GHz,'Extended_Hata') | strcmp(Abov_2GHz,'RW_Ofcom'), 'Above_2GHz input is not valid')
% assert(30<f & f<=3000 | f==3500, 'Not valid frequency selected')
% assert( ~(30<f & f<=150), 'This frequency is not implemented in the model')
% assert(d<100, 'Not valid distance')
% assert(hm>=1 & hm<=10, 'The UE height is not valid')
% assert(any(clut==[1,2,3,4]), 'The clutter type is not valid')
% if f==3500 % this frequency is valid only when RW_Ofcom option is selected
%     assert(strcmp(Abov_2GHz,'RW_Ofcom'), 'Selection of frequency and Above 2GHz option is not valid')
% end
% BS height warning
% if ~(hb>=30 && hb<= 200)
%     warning('The BS antenna height is not in the valid range')
% end


%% Path loss calculations
% Calculate the path loss according to ERC Report 68, 
% Modified Hata model, pp21-22. The following variables are used:
%
% * dS, the distances that are smaller than 40m
% * dL, the distances that are between 100m and 100km
% * dM, the distances that are between 40m and 100m
%
% Note that the maximum range is 100km because the variable
% $\alpha$
% is not defined beyond 100km.
%
% The following functions are used:
%
% * For short distances, dS, the give_PL_S function is used
% * For long distances, dL, the give_PL_L is used
% * For medium distances, dM, the path loss is given by a combination of
% the functions give_PL_S and give_PL_L

%     LdBnoAngleNoMCL = zeros(1,length(d));

    dS = d < 0.04;
    dL = 0.1<=d & d<100;
    dVL = d >= 100;
    dM = ~dS & ~dL & ~dVL;

    % When L is below the free space attenuation for the same distance, the
    % free space attenuation should be used instead
    LdBnoAngleNoMCL = give_PL_S(f,d,hb,hm);
    if any(dL)
        if length(f) == 1
            f__ = f;
        else
            f__ = f(dL);
        end
        if length(hb) == 1
            hb__ = hb;
        else
            hb__ = hb(dL);
        end
        if length(hm) == 1
            hm__ = hm;
        else
            hm__ = hm(dL);
        end

        tmp = give_PL_L(f__,d(dL),hb__,hm__,clut(dL), Abov_2GHz);
        dL_f = find(dL);
        IND = tmp > LdBnoAngleNoMCL(dL);
        LdBnoAngleNoMCL(dL_f(IND)) = tmp(IND);
    end
    if any(dM)
        if length(f) == 1
            f__ = f;
        else
            f__ = f(dM);
        end
        if length(hb) == 1
            hb__ = hb;
        else
            hb__ = hb(dM);
        end
        if length(hm) == 1
            hm__ = hm;
        else
            hm__ = hm(dM);
        end

        tmp = give_PL_S(f__,0.04,hb__,hm__) + ...
            (log10(d(dM))-log10(0.04)) ./ (log10(0.1)-log10(0.04)) .* ...
            (give_PL_L(f__,0.1,hb__,hm__,clut(dM), Abov_2GHz) - ...
            give_PL_S(f__,0.04,hb__,hm__));
        dM_f = find(dM);
        IND = tmp > LdBnoAngleNoMCL(dM);
        LdBnoAngleNoMCL(dM_f(IND)) = tmp(IND);
    end
    if any(dVL)
        LdBnoAngleNoMCL(dVL) = Inf;
    end
end

function PL_S = give_PL_S(f,d,hb,hm)
%% Path loss for short distances
% Calculate the path loss for short distances (less than 40m) using the
% following:
%
% $$ L = 32.4 + 20 \log_{10}{f} + 10 \log_{10}{\left(d^2+(h_{\mathrm{b}}-h_{\mathrm{m}})^2 / 10^6\right)} $$
%
% , where $$ d $$ is the distance in km, []

    PL_S = 32.4 + 20.*log10(f) + 10.*log10(d.^2+(hb-hm).^2./1e6);
end

function PL_L = give_PL_L(f,d,hb,hm,clut, Abov_2GHz)
%% Path loss for long distances
% Calculate the path loss for long distances (greater than 100m). The path
% loss is calculated for the urban clutter type. The path loss of the other
% clutter types are modified expressions of the urban path loss.
% The urban path loss is given by:
%
% * For $$ 150\mathrm{MHz} < f \le 1500\mathrm{MHz} $$
% $$ L = 69.6 + 26.2 \log_{10}{f} - 13.82 \log_{10}{\left(\max{\left\{30,h_\mathrm{b}\right\}}\right)} + \left[44.9-6.55\log_{10}{\max{\left\{30,h_\mathrm{b}\right\}}}\right] \log_{10}^\alpha{d} - a(h_\mathrm{m}) - b(h_\mathrm{b}) $$
% * For $$ 1500\mathrm{MHz} < f \le 2000\mathrm{MHz} $$
% $$ L = 46.3 + 33.9 \log_{10}{f} - 13.82
% \log_{10}{\left(\max{\left\{30,h_\mathrm{b}\right\}}\right)} + \left[44.9-6.55\log_{10}{\max{\left\{30,h_\mathrm{b}\right\}}}\right] \log_{10}^\alpha{d} - a(h_\mathrm{m}) - b(h_\mathrm{b}) $$
% * For $$ 2000\mathrm{MHz} < f \le 3000\mathrm{MHz} $$
% $$ L = 46.3 + 33.9 \log_{10}{2000} + 10\log_{10}{\left(f/2000\right)} - 13.82
% \log_{10}{\left(\max{\left\{30,h_\mathrm{b}\right\}}\right)} +
% \left[44.9-6.55\log_{10}{\max{\left\{30,h_\mathrm{b}\right\}}}\right] \log_{10}^\alpha{d} - a(h_\mathrm{m}) - b(h_\mathrm{b}) $$

   
    alphaL = give_alphaL(d,hb,f);

    aL = (1.1.*log10(f)-0.7).*min(10,hm) - (1.56.*log10(f)-0.8) + ...
        max(0,20.*log10(hm./10));
    bL = min(0,20*log10(hb./30));

    temp = [0; % HO: Urban
        3; %HO: dense urban
        -2.*log10(min(max(150,f),2000)./28).^2 - 5.4; %HO: Suburban
        -4.78.*log10(min(max(150,f),2000)).^2 + ... %HO: Open
        18.33.*log10(min(max(150,f),2000)) - 40.94];
%     if size(PL_L,1) < size(PL_L,2)
%         temp = temp.';
%     end
    
    if 150<f && f<=1500
        PL_L = 69.6 + 26.2.*log10(f) - 13.82.*log10(max(30,hb)) + ...
            (44.9-6.55.*log10(max(30,hb))) .* log10(d).^alphaL - aL - bL;
        PL_L = PL_L + temp(clut);
    elseif 1500<f && f<=2000
        PL_L = 46.3 + 33.9.*log10(f) - 13.82.*log10(max(30,hb)) + ...
            (44.9-6.55.*log10(max(30,hb))) .* log10(d).^alphaL - aL - bL;
        PL_L = PL_L + temp(clut);
    elseif (2000<f && f<=3000) || (f==3500)
        % For frequency above 2GHz  we need to check which implementation we need to use here
        switch Abov_2GHz
            case 'Extended_Hata'
                % This is the originally Extended Hata model (implemented in SEAMCAT simulator)
                PL_L = 46.3 + 33.9.*log10(2000) + 10*log10(f./2000) - 13.82.*log10(max(30,hb)) + ...
                    (44.9-6.55.*log10(max(30,hb))) .* log10(d).^alphaL - aL - bL;
                PL_L = PL_L + temp(clut);
            case 'RW_Ofcom'
                % This what was implemented in Ofcom previous project and agreed with them (2G Liberalisation)
                if any(f<=150||(f>2000&&f~=2100&&f~=2300&&f~=2600&&f~=3500))
                    error(['The path loss does not support frequencies outside of ' ...
                        'the range (150, 2000)MHz, apart from 2100, 2300, 2600 and 3500 MHz'])
                end
                
                if f == 2100
                    PL_L = give_PL_L(1800,d,hb,hm,clut, Abov_2GHz) + 1.7;
                elseif f == 2300
                    PL_L = give_PL_L(1800,d,hb,hm,clut, Abov_2GHz) + 2.7;
                elseif f == 2600
                    PL_L = give_PL_L(1800,d,hb,hm,clut, Abov_2GHz) + 4.0;
                elseif f == 3500
                    %         round(10.*2.478*log10(3500) - 80.6,1)
                    PL_L = give_PL_L(1800,d,hb,hm,clut, Abov_2GHz) + 7.2;
                    %     elseif 2000<f && f<=3000
                    %         PL_L = 46.3 + 33.9.*log10(f) + 10.*log10(f./2000) -...
                    %             13.82.*log10(30) +...
                    %             (44.9-6.55.*log10(30)) .* ...
                    %             repmat(log10(d),1,length(hb_)) .^ alphaL -...
                    %             aL - repmat(bL,length(d),1);
                    %         PL_L = PL_L + temp(clut);
                end %if
        end %switch
    end % elseif 1500<f && f<=3000
    
%% Path loss modification due to clutter type
% Modify the path loss according to the clutter type:
%
% * Urban: (baseline) no modification
% * Dense urban: $$ +3 $$
% * Suburban: $$ -2 \log_{10}^2{\left(f/28\right)} - 5.4 $$
% * Open: $$ -4.78 \log_{10}^2{f} + 18.33 \log_{10}{f} - 40.94 $$

end % end of the function

function alphaL = give_alphaL(d,hb,f)
% Gives the $\alpha$ coefficient of SEAMCAT extended Hata
% path loss model
% =============================
% by Kostas Konstantinou, 25Mar09
%
% Gives the $\alpha$ coefficient of SEAMCAT extended Hata
% path loss model, see SEAMCAT manual at
% http://tractool.seamcat.org/wiki/Manual

if nargin == 0
    d = 21;  % 2D separation distance between BS and UE 21km
    hb = 30;  % Height of the BS 30m
    f = 700;  % Frequency 700MHz
end

%%
%
% The following equation is implemented:
%
% $$
% \alpha = \left\{ \begin{array}{rl}
%  1 &\mbox{ if $d<=20$km} \\
%  1 + (0.14 + 1.87\times{10^{-4}} f + 1.07\times{10^{-3}} H_b) (\log_{10}{d/20})^{0.8} &\mbox{ otherwise}
%        \end{array} \right.
% $$
%
% The case where
% $d>=100\mathrm{km}$
% is avoided in the calling function.

IND = d > 20;
alphaL = ones(size(d));
if length(f) == 1
    arg1 = f;
else
    arg1 = f(IND);
end
if length(hb) == 1
    arg2 = hb;
else
    arg2 = hb(IND);
end
alphaL(IND) = 1 + (0.14 + 1.87e-4.*arg1 + 1.07e-3.*arg2) .* ...
    log10(d(IND)./20).^0.8;
end
