function Cell = run_propa_modu_med(Cell,Sim,isNew,studyA,clu,ter,fn_,dsm)
%% [Title]
% [Description]
%
% * v0.1 KK 16May15 Updated for 5G Norma
% * v0.2 KK 19May15 dem as input
% * v0.3-1.0 CG May-Jun15 development
% 
% Note that if we start using 1812, then the representative clutter height
% should be reconsidered for urban, perhaps from 15 to 20m

%% Calculations
% Parameter simplication
fMHz_ = Sim.linBud.spotFreq;

% Initiate extent of coverage based on Voronoi geometry
vx = cell(length(Cell),length(fMHz_));
vy = cell(length(Cell),length(fMHz_));

% Find the cells that are in the same band bundle
ch = [Cell.ch];

% []
[~,Locb] = ismember({Cell.pwrAmpClas},Sim.propag.BS(:,1));
maxDist_m = Sim.propag.BS(Locb,5);

% Find the extent of coverage based on Voronoi geometry for each channel
n_ = find(any(bsxfun(@and,isNew,ch),2));  % Index of which of the [28] channels the new site(s) support
for n = 1 : length(n_)
    fn = Sim.linBud.chRaste{2,n_(n)+4};
    if fn_(fn)
        assert(isempty(cell2mat(vx(ch(n_(n),:),fn))), 'There should be no overwritting' )
        if Sim.servDem.isInterfeExpl.capa
            beep
            disp('Consider changing the extend by 0.5 to larger, since interference is needed explicitely')
            keyboard
        end
        % Check if it is desired to use Voronoi or not
        if Sim.propag.useVoronoi
            [vx(ch(n_(n),:),fn), vy(ch(n_(n),:), fn) ] =  find_coverage_area_using_voronoi(...
                                Cell(ch(n_(n),:)), isNew(ch(n_(n),:)), studyA, 0.5, maxDist_m );
        else
            vx(ch(n_(n),:),fn) = cellfun( @( x, y ) x + [ -1 -1 1  1 -1 ].' .* y,...
                                        num2cell( [ Cell.X ] ).', maxDist_m, 'UniformOutput', false );
            vy(ch(n_(n),:),fn) = cellfun( @( x, y ) x + [ -1  1 1 -1 -1 ].' .* y,...
                                        num2cell( [ Cell.Y ] ).', maxDist_m, 'UniformOutput', false );
        end
    end
end

vx(~isNew,:) = [];
vy(~isNew,:) = [];
% figure
% hold on
% axis equal
% IND = arrayfun(@(x) x.band.supp(1),Cell);
% plot([Cell.X],[Cell.Y],'k.')
% plot([Cell.X],[Cell.Y],'ko')
% plot(Cell(56).X,Cell(56).Y,'r.')
% plot(Cell(56).X,Cell(56).Y,'ro')
% RGB = jet(4);
% for n = 1 : size(RGB,1)
%     plot(vx{56,n},vy{56,n},'color',RGB(n,:))
% end
% plot(vx{905},vy{905})
% plot(vx{906},vy{906})
% plot(vx{907},vy{907})

% If there is only one site and it is the only on that channel, then it
% indicative coverage is at the max range
% if size(vx,1)==1 && nnz(fn_)==1 && isempty(vx{:,fn_})
%     vx{:,fn_} = Cell(isNew).X + [-1 -1 1 1 -1].'.*Cell(isNew).maxDist_m;
%     vy{:,fn_} = Cell(isNew).Y + [-1 1 1 -1 -1].'.*Cell(isNew).maxDist_m;
% end

% Remove cell objects that are not new
Cell(~isNew) = [];
maxDist_m(~isNew) = [];

% For each UE type, calculate path loss
% Lbc_ = cell(length(fMHz_),size(Sim.propag.UE,1),length(Cell));
% sigmaLe_ = cell(length(fMHz_),size(Sim.propag.UE,1),length(Cell));
for Un = 1 : size(Sim.propag.UE,1)
    % Create structure for calc points, dem_struct, UE side
    dem_struct = Sim.propag.pix{Un};  % [X,Y,I,J,clu_Hata]

    if ~isempty(dem_struct)
        % For each frequency, calculate path loss
        for fn = 1 : length(fMHz_)
            if fn_(fn)
                BoundingBox = cellfun(@(x,y,z,vx,vy) ...
                    [max(x-z,min(vx)),max(y-z,min(vy))
                    min(x+z,max(vx)),min(y+z,max(vy))],...
                    num2cell([Cell.X]).',num2cell([Cell.Y]).',...
                    maxDist_m,vx(:,fn),vy(:,fn),'UniformOutput',false);

                if length(Cell) == 1
                    if ~isempty(vx{:,fn})
                        Cell = c_per_sq(Cell,Cell.fileName,Sim,clu,ter,...
                            dem_struct,vx(:,fn),vy(:,fn),studyA,...
                            BoundingBox,fMHz_(fn),Sim.propag.UE(Un,:),...
                            fn,Un,maxDist_m,'pop',dsm);
                    end
                else
                    % For each OS square, calculate path loss
                    IND = find(cellfun(@(vx) ~isempty(vx),vx(:,fn)));
                    if ~isempty(IND)
                        [C,~,ic] = unique({Cell(IND).fileName});
                        % Have a progress bar if the model is the ITU one, to check time
                        flagWait = strcmp( Sim.propag.mod, 'ITU_1812_4' );
                        if flagWait
                            multiWaitbar('Path loss',0);
                        end
                        for n = 1 : length(C)
                            IND2 = IND(ic==n);
                            Cell(IND2) = c_per_sq(Cell(IND2),C{n},Sim,...
                                clu,ter,dem_struct,...
                                vx(IND2,fn),vy(IND2,fn),...
                                studyA,BoundingBox(IND2),fMHz_(fn),...
                                Sim.propag.UE(Un,:),fn,Un,...
                                maxDist_m(IND2),'pop');
                            if flagWait
                                multiWaitbar('Path loss',n./length(C));
                            end
                        end
                        if flagWait
                            multiWaitbar('Path loss','close');
                        end
                    end
                end
            end
        end
    end
end
