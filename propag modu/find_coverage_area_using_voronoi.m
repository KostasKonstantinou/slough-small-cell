function [vx,vy] = find_coverage_area_using_voronoi(Cell,isNew,studyA,...
    extBy,maxDist_m)
%% Calculates the coverage polygon based on Voronoi geometry
% find_coverage_area_using_voronoi calculates the coverage polygon (vx,vy)
% of a site based on Voronoi geometry. A site's coverage cannot extend
% further than its 1st tier. This function can also be used to calculate
% the extent where interference should be calculated. 3GPP simulations
% usually take into account 2 tiers of interference, neglecting
% wrap-around. extBy is a multiplier of ISD for how much the Voronoi
% polygon should be extended by.
% extBy = 2.5 corresponds to 2 tiers, suggested for use for interference.
% extBy = 0.5 corresponds to 1 tier, suggested for use for coverage.
% isNew is a logical vector of size(Cell) that is true for cells we wish
% the coverage polygon for; to calculate the coverage polygon Voronoi
% requires the location of the 1st tier of neighbours.
% 
% The input Cell is an array of CELL objects and must contain at least the
% following fields:
% X,Y,maxDist_m - Note that maxDist_m could be set to something very big
% 
% The input structure studyA must contains at least the following fields:
% buf.BoundingBox
%
% Example 1: 
% Calculate the coverage polygons of 8 sites.
%   
% Cell = struct(...
%     'X',num2cell([71175,60975,60125,57275,65325,61725,65425,63025]),...
%     'Y',num2cell([31875,25425,34375,32775,27575,34175,24225,36725]),...
%     'maxDist_m',5000);
% isNew = true(size(Cell));
% studyA.buf.BoundingBox = [47151 20639
%     74674 38033];
% [vx,vy] = find_coverage_area_using_voronoi(Cell,isNew,studyA,0.5);
%
% figure
% axis equal
% hold on
% plot([Cell.X],[Cell.Y],'k.')
% plot([Cell.X],[Cell.Y],'ko')
% for n = 1 : length(vx)
%     plot(vx{n},vy{n},'b')
% end
% 
% Kostas Konstantinou Sep 2015

% Simplifications
X = [Cell.X].';
Y = [Cell.Y].';
maxDist_m = cell2mat(maxDist_m);
% maxDist_m = [Cell.maxDist_m].';

% Remove colocated sites
[C,ia,ic] = unique(X+1i.*Y,'stable');
X = real(C);
Y = imag(C);
clear C

if length(ia) >= 3
    % []
    if nnz(isNew) == 1
        tmp = maxDist_m + maxDist_m(isNew);
        isClo = find(sqrt((X-X(ic(isNew))).^2+(Y-Y(ic(isNew))).^2) <= ...
            tmp(ia));
        bs_ext = [X(ic(isNew))+tmp(isNew).*[-1;-1;1;1;-1],...
            Y(ic(isNew))+tmp(isNew).*[-1;1;1;-1;-1]];
    else
        isClo = (1:length(X)).';
        bs_ext = [studyA.buf.BoundingBox([1 1 2 2 1],1),...
            studyA.buf.BoundingBox([1 2 2 1 1],2)];
    end

    if length(isClo) >= 3
        % Voronoi decomposition
        Xp = X(isClo);
        Yp = Y(isClo);
    
        % Check if all points in line
        % Line equation, y = a*x + b, from points 1 and 2
        % uk.mathworks.com/matlabcentral/answers/351581-points-lying-within-line
        if Xp(2) ~= Xp(1)
            a = (Yp(2)-Yp(1)) ./ (Xp(2)-Xp(1));
            b = Yp(1) - a.*Xp(1);
            if all((a*Xp(3:end)+b)./Yp(3:end)-1<1e-3)
                % The points lie on the line
                proc = false;
            else
                % The points do not lie on the line
                proc = true;
            end
        else
            if all(Xp(3:end)==Xp(2))
                % The points lie on the line
                proc = false;
            else
                % The points do not lie on the line
                proc = true;
            end
        end
    else
        proc = false;
    end
    
    if proc
        C = cell(length(X),1);
%         A = logical(sparse(length(X),length(X)));
%         [C(isClo),V,A(isClo,isClo)] = voronoi_constra([Xp Yp],bs_ext);
        [C(isClo),V] = voronoi_constra([Xp Yp],bs_ext);
        
%     figure
%     hold on
%     axis equal
%     plot(bs_ext(:,1),bs_ext(:,2))
%     plot(X,Y,'b.')
%     plot(X(ic(isNew)),Y(ic(isNew)),'r.')
% %     plot(X(isNew),Y(isNew),'ro')
% %     plot(Xp,Yp,'rx')
%     plot(V(C{4},1),V(C{4},2))
%     plot([Cell(ic(isNew)).X],[Cell(ic(isNew)).Y],'r.')
%     plot([Cell(ic(isNew)).X],[Cell(ic(isNew)).Y],'ro')
%     plot([Cell(A(ic(4),:)).X],[Cell(A(ic(4),:)).Y],'bs')

        % Populate output
        vx = cell(length(Cell),1);
        vy = cell(length(Cell),1);
        for n = find(isNew)
%             ISD = sqrt((X(ic(n)) - X(A(ic(n),:))).^2 + ...
%                 (Y(ic(n)) - Y(A(ic(n),:))).^2);

            % Extend polygon
            if extBy > 0                
%                 try
%                     [tmpx,tmpy] = bufferm2('xy',V(C{ic(n)},1),V(C{ic(n)},2),...
%                         max(ISD).*extBy,'out');
%                 catch
%                     [tmpx,tmpy] = bufferm2('xy',V(C{ic(n)},1),V(C{ic(n)},2),...
%                         max(ISD).*extBy.*1.01,'out');
%                 end
                
                w = V(C{ic(n)}(1:end-1),1:2);
                v = V(C{ic(n)}(2:end),1:2);
                l2 = sum((w-v).^2,2);
                t = max(0,min(1,dot(bsxfun(@minus,...
                    [X(ic(n)) Y(ic(n))],v),w-v,2)./l2));
                vec2 = v + bsxfun(@times,t,w-v);
                B = sqrt(sum(bsxfun(@minus,vec2,[X(ic(n)) Y(ic(n))]).^2,2));
                
                tmpx = cell(length(C{ic(n)})-1,1);
                tmpy = cell(length(C{ic(n)})-1,1);
                for m = 1 : length(C{ic(n)})-1
                    [tmpx{m},tmpy{m}] = bufferm2('xy',...
                        V(C{ic(n)}(m:m+1),1),V(C{ic(n)}(m:m+1),2),B(m),...
                        'out');
                    
%                     m = 5;
%                     [tmpx2,tmpy2] = bufferm2('xy',...
%                         V(C{ic(n)}(m:m+1),1),V(C{ic(n)}(m:m+1),2),B(m),...
%                         'out');
%                     ispolycw(tmpx2,tmpy2)
% %                     plot(tmpx2,tmpy2)
                end
%                 ispolycw(tmpx,tmpy)
                [tmpx,tmpy] = polyjoin(tmpx,tmpy);
                [tmpx,tmpy] = polyunion(tmpx,tmpy,...
                    'potentially overlapping');
%                 plot(tmpx{1},tmpy{1})
                if any(isnan(tmpx{1}))
                    beep
                    disp('Relax tolerance')
                    keyboard
                end
%                 [tmpx,tmpy] = closePolygonParts(tmpx{1},tmpy{1});
%                 plot(tmpx,tmpy)
                
                tmpx = tmpx{1}([1:end,1]);
                tmpy = tmpy{1}([1:end,1]);                
                
                vx(n) = {tmpx};
                vy(n) = {tmpy};
            else
                vx{n} = V(C{ic(n)},1);
                vy{n} = V(C{ic(n)},2);
            end
%             figure
%             axis equal
%             hold on
%             plot(V(C{ic(n)},1),V(C{ic(n)},2),'b.-')
%             plot(X,Y,'.')
% %             plot(Xp(n),Yp(n),'r+')
%             plot(vx{n},vy{n},'k')
%     %         plot(Xp(n),Yp(n),'r+',V(C{ic(n)},1),V(C{ic(n)},2),'b.-')

    %         figure
    %         axis equal
    %         hold on
    %         plot(X,Y,'.')
    %         plot(X(ic(n)),Y(ic(n)),'rx')
    %         plot(X(A(ic(n),:)),Y(A(ic(n),:)),'ko')
    %         plot(vx{n},vy{n},'k')

    %         P1.x = xb{n}; P1.y = yb{n}; P1.hole = 0;
    %         P2.x = [Reg.xmin; Reg.xmax; Reg.xmax; Reg.xmin; Reg.xmin];
    %         P2.y = [Reg.ymin; Reg.ymin; Reg.ymax; Reg.ymax; Reg.ymin];
    %         P2.hole = 0;
    %         P3 = PolygonClip(P1,P2,1);
    % 
    %         if length(P3) == 1
    %             xb{n} = [P3.x; P3.x(1)];
    %             yb{n} = [P3.y; P3.y(1)];
    %         else
    %             xb(n) = {[]};
    %             yb(n) = {[]};
    %         end

    %     figure
    %     plot(P1.x,P1.y,'b')
    %     hold on
    %     plot(P2.x,P2.y,'g')
    %     plot([P3.x; P3.x(1)],[P3.y; P3.y(1)],'r')

    %     plot(xb{n},yb{n},'c')

    %     Line y = a*x+b
    %     Point (x0,y0)
        end
    %     vx(length(row2)+1:end) = [];
    %     vy(length(row2)+1:end) = [];
        % warning on all
        % clear n m extBy a b x1 y1 P1 P2 P3 V C

        % figure
        % plot(Cell(1).lon,Cell(1).lat,'r+',vx{4},vy{4},'b.-')
    else
        vx = cell(length(Cell),1);
        vy = cell(length(Cell),1);

        for n = find(isNew)
            vx{n} = [-1;-1;1;1;-1] .* inf;
            vy{n} = [-1;1;1;-1;-1] .* inf;
        end
    end
else
    % Find the line that runs between the two cells
    % The vector that joins the two cells is
%     [x(2)-x(1);y(2)-y(1)]
    % The vector that is perpendicular to the above is
%     [-(y(2)-y(1));x(2)-x(1)]
%     % The line that is parallel to the above vector is
%     (x(2)-x(1)).x+(y(2)-y(1)).y==c1
%     y == c - (x(2)-x(1)).(y(2)-y(1)).x
    % The midpoint is
%     [x(1);y(1)]+[x(2)-x(1);y(2)-y(1)].2
%     [x(1)+(x(2)-x(1)).2;y(1)+(y(2)-y(1)).2]
%     c = y(1)+(y(2)-y(1)).2 + (x(2)-x(1)).(y(2)-y(1)).(x(1)+(x(2)-x(1)).2);
%     plot([x(1) x(2)],c-(x(2)-x(1)).(y(2)-y(1)).[x(1) x(2)])
%     for n = 1  length(C)
%         polybool('intersection',lon([1;1;2;2;1]),lat([1;2;2;1;1]),...
%         y(n)  c - (x(2)-x(1)).(y(2)-y(1)).x(n)
%     end

    vx = cell(length(Cell),1);
    vy = cell(length(Cell),1);
    
    for n = 1 : length(vx)
        vx{n} = [-1;-1;1;1;-1] .* inf;
        vy{n} = [-1;1;1;-1;-1] .* inf;
    end
end

% figure
% plot(Cell(1).lon,Cell(1).lat,'r+',vx{1},vy{1},'b.-')
