function Cell = c_per_sq(Cell,fileName,Sim,clu,ter,dem,vx,vy,studyA,...
    BoundingBox,spotFreqMhz,UE,fn,Un,maxDist_m,sty,dsm)
%C_PER_SQ function calls the right function according to the selected propagation model to calculate
%       the corresponding path loss.

switch Sim.propag.mod
    case 'Extended_HATA'
        Cell = c_per_sq_Extended_Hata(Cell,fileName,Sim,clu,ter,dem,vx,vy,studyA,...
            BoundingBox,spotFreqMhz,UE,fn,Un,maxDist_m,sty,dsm);
    case 'ITU_1812_4'
        Cell = c_per_sq_1812_4(Cell,fileName,Sim,clu,ter,dem,vx,vy,studyA,...
            BoundingBox,spotFreqMhz,UE,fn,Un,maxDist_m,sty,dsm);
end