function [Lbc,Lba,Lbs,Ld50] = c_itu_1812_4(h_Tx_agl,h_Rx_agl,polarisation,...
    Path,fGHz,p,isJustBullAndFlatEarth)
%C_ITU_1812_4 calculates the path loss according to the ITU-R P.1812-4 specification. This current
%       implementation does not calculate the path loss though for any value less than p < 50% and
%       also that there are no paths over large bodies of water.
%
%       INPUT:
%           h_Tx_agl : the height of the transmitter
%           h_Rx_agl : the height of the receiver
%           polarisation : The polarisation of the transmitted signal
%           Path     : the path profile, a structure with information about
%                      the points that consist the path, i.e. a structure,
%                      with fields, X, Y, lat, lon, d, h and g. Can be an
%                      array. 
%           fGHz     : the frequency in GHz
%           p        : the percentage of time that the prediction is valid
%           isJustBullAndFlatEarth : logical scalar that is true when we
%                                    wish to assume that the Earth is flat
%                                    and that the dominant path loss
%                                    mechanisms are either LoS or
%                                    Bullington diffraction
%
%       OUTPUT:
%           Lbc      : the pathloss between the Tx and the Rx
%
% * v0.1 KK 26Jan16 Started based on 1812-3 lat/lon
% * v0.2 KK 27Jan16 Use OSGB36
% * v0.3 KK 15Feb16 clu.tran.(clu.tran.pv)
% * v0.4 AK Dec16 	Modified to a more independent format
% * v0.5 AK Mar17 	Corrected a bug with the height selection, hts/hrs; Added the water bodies and
%                   urban/suburban/woodland input
% * v0.6 KK Jul17   Eq 64a/b change so that clu is no longer an input to
%                   the function
% * v0.7 KK Aug17 Fixed erro in (88) and vectorised
% * v0.8 HO Aug18 Fix a bug under eq(35), the condition of 2+20*log10(K) was not taken into
% consideration
% * v0.9 HO Aug18 Fix a bug in eq(29a), (epsilonr-1).^2 -> "-1" was missing
% * v0.10 HO Sept18 Fix bug in eq (29), line #777 of the code (Kh adjusted to KH)
% * v0.11 KK Oct18 Lba,Lbs as output
% * v0.12 KK Nov19 Added isJustBullAndFlatEarth switch

% IND = find(cellfun(@(x) x(end),Path.d)>=250./1e3);
IND = (1:length(Path.d)).';

% Bundle paths of same length
[~,~,ic] = unique(Path.N(IND));

% For each bundle, calculate path loss
Lbc = -inf(length(Path.N),1);
Lba = -inf(length(Path.N),1);
Lbs = -inf(length(Path.N),1);
Ld50 = -inf(length(Path.N),1);
for n = 1 : max(ic)
    % Indices of this bundle
    IND2 = IND(ic==n);
    
    % The local path objects for this bundle
    Path2 = se(Path,IND2);
    
    % Calculate Lbc and intermediate loss for this bundle
    [Lbc(IND2),~,~,Ld50(IND2)] = ...
        run_itu_1812_4(h_Tx_agl,h_Rx_agl,polarisation,Path2,fGHz,p,...
        isJustBullAndFlatEarth);
end
end

function [Lbc,Lba,Lbs,Ld50] = run_itu_1812_4(h_Tx_agl,h_Rx_agl,polarisation,...
    Path,fGHz,p,isJustBullAndFlatEarth)
%% Calculate path loss according to 1812-4
% run_itu_1812_4 calculates the median path loss Lbc according to ITU R.
% 1812-4. h_Tx_agl and h_Rx_agl are the heights of the transmitter and
% reeiver respectively, polarisation is a string that is irrelevant for
% typical mobile network frequencies, Path is an array of PATH objects,
% fGHz is the frequency in GHz, p is the probability p of ITU R. 1812.  
%
% * v0.1  KK 24Aug17 Created

% Give di (distance), hi (height), and gi (height including clutter) for
% every point in the profile 
di = cell2mat(Path.d);  % (1a)
hi = cell2mat(Path.h);  % (1b)
gi = cell2mat(Path.g);  % (1c)

% The latidude of the middle point
if ~isJustBullAndFlatEarth
    lat_N2 = Path.lat;
end

% The total distance
d = di(:,end);

% Assertions
assert(all(d<=3e3),'Maximum distance is 3,000 km')

%% Find terminal distance from the coast
% Model feature not yet implemented
dct = Inf;
dcr = Inf;

%% Estimate incidence of ducting
if ~isJustBullAndFlatEarth
    % Longest continuous land (inland+coastal) section of the great-circle
    % path, km
    dtm = d;

    % Longest continuous inland section of the great-circle path, km
    dlm = d;

    % Parameter mu1
    tau = 1 - exp(-0.000412.*dlm.^2.41);  % (3)
    mu1 = min((10.^(-dtm./(16-6.6.*tau))+10.^(-5.*(0.496+0.354.*tau))).^0.2,1);  % (2)

    IND = abs(lat_N2) <= 70;

    % Parameters mu4 and beta0
    if all(IND)
        mu4 = mu1 .^ (-0.935+0.0176.*abs(lat_N2));  % (4)
        beta0 = 10.^(-0.015.*abs(lat_N2)+1.67) .* mu1 .* mu4;  % (5)
    else
        beep
        disp('Code this!')
        keyboard
    %     if abs(lat_N2) <= 70
    %         mu4 = mu1 .^ (-0.935+0.0176.*abs(lat_N2)); % (4)
    %     else
    %         mu4 = mu1 .^ 0.3;   % (4)
    %     end
    %     if abs(lat_N2) <= 70
    %         beta0 = 10.^(-0.015.*abs(lat_N2)+1.67).*mu1.*mu4;  % (5)
    %     else
    %         beta0 = 4.17 .* mu1 .* mu4;   % (5)
    %     end
    end
end

%% Estimate effective Earth radius
if ~isJustBullAndFlatEarth
    % Median effective Earth radius factor k50
    k50 = 157 ./ (157-Path.DeltaN);  % (6)

    % Median value of effective Earth radius, km
    ae = 6371 .* k50;  % (7a)

    % kbeta = 3.0;
    % abeta = 6371 .* kbeta;  % (7b)
end

%% Calculate parameters derived from the path profile analysis
% Calculate hts and hrs
hts = hi(:,1) + h_Tx_agl;
hrs = hi(:,end) + h_Rx_agl;

% Parameters htc, hrc, db, omega
htc = max(hts,gi(:,1));  % Table 5
hrc = max(hrs,gi(:,end));  % Table 5
if ~isJustBullAndFlatEarth
    db = sum(cell2mat(Path.isWater),2) .* di(:,2);  % Table 5 
    omega = db ./ d;  % Table 5
end

%% Calculation of line-of-sight propagation (including short-term effects)
% Basic transmission loss due to free-space propagation
Lbfs = 92.45 + 20.*log10(fGHz) + 20.*log10(d);  % (8)

% Correction for multipath and focusing effects
if p == 50
    Esp = 0;  % (9a)
else
    error('Code this branch!')
    % dlt and dlr are not defined
    Esp = 2.6 .* (1-exp(-(dlt+dlr)./10)) .* log10(p./50);  % (9a)
end

% Basic transmission loss not exceeded for time percentage p% due to LoS
% propagation (regardless of whether or not the path is actually LoS)
Lb0p = Lbfs + Esp;  % (10)

%     Lb0beta(n) = Lbfs(n) + Esbeta;  % eq(11)

%% Derive the smooth-Earth surface
% Straight line approximation to the terrain heights, metres amsl
v1 = sum((di(:,2:end)-di(:,1:end-1)).*(hi(:,2:end)+hi(:,1:end-1)),2);  % (85)
v2 = sum((di(:,2:end)-di(:,1:end-1)) .* ...
    (hi(:,2:end).*(2.*di(:,2:end)+di(:,1:end-1)) + ...
    hi(:,1:end-1).*(di(:,2:end)+2.*di(:,1:end-1))),2);  % (86)

% Height of the smooth-Earth surface (amsl) at the transmitting station
% location
hst = (2.*v1.*d-v2) ./ d.^2;  % (87)

% Height of the smooth-Earth surface (amsl) at the receiving station
% location
hsr = (v2-v1.*d) ./ d.^2;  % (88)

% Slope of the least-squares surface relative to sea level, m/km
% m = sum(3 .* (di(2:end)-di(1:end-1)) .*...
%     (di(2:end)+di(1:end-1)-d(n)) .*...
%     (hi(2:end)+hi(1:end-1)-2.*ha) + ...
%     (di(2:end)-di(1:end-1)).^2 .* (hi(2:end)-hi(1:end-1)))./d(n).^3;  % (86b)

%% Calculate the diffraction loss
% Wavelength, m
lambda = 0.2998 ./ fGHz;

if ~isJustBullAndFlatEarth
    % Effective Earth radius, km
    if p == 50
        ap = ae;
    else
        error('Continue coding from here!')
        ap = abeta;
    end

    % Effective Earth carvature, km-1
    Ce = 1 ./ ap;

    % Calculate smooth-Earth heights at tx and rx, Section 5.1.6.3
    [hstd,hsrd] = sec_562(hi,di,htc,hrc,d,hst,hsr);

    % Calculate hprimetc and hprimerc
    hprimetc = htc - hstd;  % (37a)
    hprimerc = hrc - hsrd;  % (37b)
end

% Complete "delta-Bullington" diffraction loss model
if isJustBullAndFlatEarth
    Ce = 0;
end
Lbulla = sec_431(Ce,gi             ,di,d,htc     ,hrc     ,lambda);
if isJustBullAndFlatEarth
    Lbulls = 0;
else
    Lbulls = sec_431(Ce,zeros(size(di)),di,d,hprimetc,hprimerc,lambda);
end

if isJustBullAndFlatEarth
    Ldsph = 0;
else
    % Spherical diffraction loss, dB
    htesph = hprimetc;  % (38a)
    hresph = hprimerc;  % (38b)
    Ldsph = sec_432(ap,htesph,hresph,d,fGHz,omega,lambda,polarisation);
end

% Diffraction loss for general path
Ld = Lbulla + max(Ldsph-Lbulls,0);  % (39)

%% The diffraction loss not exceeded for p% of the time
% Median diffraction loss
if p == 50
    Ld50 = Ld;
    Ldbeta = 0;  % Irrelevant value as multiplied with 0
else
    error('Check from here!')
    Ldbeta = Ld;
    Ld50 = 0;
end

% Interpolation factor
if p == 50
    Fi = 0;  % I_func(50./100)=0
elseif p<50 && p>beta0
    error('Check from here!')
    Fi = I_func(p./100) ./ I_func(beta0./100);  % (40a)
elseif p <= beta0
    error('Check from here!')
    Fi = 1;  % (40b)
end

% Diffraction loss not exceeded for p% of the time
Ldp = Ld50 + (Ldbeta-Ld50).*Fi;  % (41)

% Median transmission loss associated with diffraction
Lbd50 = Lbfs + Ld50;  % (42)

% Basic transmission loss associated with diffraction not exceeded for p%
% time
Lbd = Lb0p + Ldp;  % (43)

%% Path classification
if ~isJustBullAndFlatEarth
    % Elevation angle to the i-th terrain point
    C1 = bsxfun(@minus,hi,hts);
    C2 = bsxfun(@rdivide,di./2,ae);
    thetai = 1000 .* atan(C1./1e3./di-C2);  % (77)
    [thetamax,dltIND_ifnLoS] = max(thetai(:,2:end-1),[],2);  % (76)
    dltIND_ifnLoS = dltIND_ifnLoS + 1;  % (80)

    % Angle (relative to the transmitter�s local  horizontal) subtended by
    % the receiving antenna
    thetatd = 1000 .* atan((hrs-hts)./1e3./d-d./2./ae);

    % LoS classification
    isLOS = ~(thetamax>thetatd);
end

%% Receiving antenna horizon elevation angle above the local horizontal
if ~isJustBullAndFlatEarth
    % Initialise
    thetar = zeros(length(isLOS),1);

    % LoS path
    if any(isLOS)
        thetar(isLOS) = 1000 .* atan((hts(isLOS)-hrs(isLOS)) ./ 1e3 ./ ...
            d(isLOS) - d(isLOS)./2./ae(isLOS));  % (81)
    end

    % Path is trans-horizon
    if any(~isLOS)
        % Calculate the elevation angle to the j-th terrain point
        C1 = bsxfun(@minus,hi(~isLOS,:),hrs(~isLOS));
        C2 = bsxfun(@minus,d(~isLOS),di(~isLOS,:));
        C3 = bsxfun(@rdivide,C2./2,ae(~isLOS));
        thetaj = 1000 .* atan(C1./1e3./C2-C3);  % (82a)
        [thetar(~isLOS),dlrIND_ifnLoS] = max(thetaj(:,2:end-1),[],2);  % (82)
        dlrIND_ifnLoS = dlrIND_ifnLoS + 1;
    end
end

%% Transmitting antenna horizon elevation angle able the local horizontal, mrad
if ~isJustBullAndFlatEarth
    % Calculate thetat
    thetat = max(thetamax,thetatd);  % (79)
end

%% Transmitting antenna horizon distance
if ~isJustBullAndFlatEarth
    % Initialise
    ilt = zeros(length(isLOS),1);

    % LoS path
    if any(isLOS)
        C1 = bsxfun(@times,Ce(isLOS),di(isLOS,2:end-1));
        C2 = bsxfun(@minus,d(isLOS),di(isLOS,2:end-1));
        C3 = bsxfun(@times,hts(isLOS),C2);
        C4 = bsxfun(@times,hrs(isLOS),di(isLOS,2:end-1));
        C5 = bsxfun(@rdivide,C3+C4,d(isLOS));
        C6 = bsxfun(@rdivide,d(isLOS),lambda.*di(isLOS,2:end-1).*C2);
        [~,maxI] = max((hi(isLOS,2:end-1)+500.*C1.*C2-C5).*sqrt(2e-3.*C6),[],2);  % (80a)
        ilt(isLOS) = maxI + 1;
    end

    % Path is trans-horizon
    if any(~isLOS)
        ilt(~isLOS) = dltIND_ifnLoS(~isLOS);  % (80)
    end

    % Calculate dlt
    dlt = di(sub2ind(size(di),(1:size(di,1)).',ilt));
end

%% Receiving antenna horizon distance
if ~isJustBullAndFlatEarth
    % Initialise
    dlr = zeros(length(isLOS),1);
    ilr = zeros(length(isLOS),1);

    % LoS path
    if any(isLOS)
        dlr(isLOS) = d(isLOS) - dlt(isLOS);  % (83a)
        ilr(isLOS) = ilt(isLOS);
    end

    % Path is trans-horizon
    if any(~isLOS)
        dlr(~isLOS) = d(~isLOS) - di(sub2ind(size(di),find(~isLOS),dlrIND_ifnLoS));  % (83)
        ilr(~isLOS) = dlrIND_ifnLoS;
    end
end

%% Angular distance, mrad
if ~isJustBullAndFlatEarth
    % Calcualte theta
    theta = 1e3.*d./ae + thetat + thetar;
end

%% Calculate the propagation by tropospheric scatter
if ~isJustBullAndFlatEarth
    % Basic transmission loss due to troposcatter, not exceeded for any time
    % percentage
    Lf = 25.*log10(fGHz) - 2.5.*log10(fGHz./2).^2;  % (45)
    Lbs = 190.1 + Lf + 20.*log10(d) + 0.573.*theta - 0.15.*Path.N0 + ...
        10.125.*(log10(50./p)).^0.7;  % (44)
else
    Lbs = NaN;
end

%% Propagation by ducting/layer reflection
if ~isJustBullAndFlatEarth
    % Site-shielding diffraction losses for the transmitting station
    thetadoubleprimet = thetat - 0.1.*dlt;  % (48a)
    Ast = zeros(length(thetadoubleprimet),1);
    IND = thetadoubleprimet > 0;
    if any(IND)
        Ast(IND) = (20.*log10(1+0.361.*thetadoubleprimet(IND).*sqrt(fGHz.*dlt(IND))) + ...
            0.264.*thetadoubleprimet(IND).*fGHz.^(1./3));  % (48)
    end

    % Site-shielding diffraction losses for the receiving station
    thetadoubleprimer = thetar - 0.1.*dlr;
    Asr = zeros(length(thetadoubleprimer),1);
    IND = thetadoubleprimer > 0;
    if any(IND)
        Asr(IND) = (20.*log10(1+0.361.*thetadoubleprimer(IND).*sqrt(fGHz.*dlr(IND))) + ...
            0.264.*thetadoubleprimer(IND).*fGHz.^(1./3));
    end

    % Over-sea surface duct coupling corrections for the Tx/Rx stations
    Act = zeros(length(omega),1);
    Acr = zeros(length(omega),1);
    IND = omega >= 0.75 & dct <= dlt & dcr <= dlr & dct <= 5 & dcr <= 5;
    if any(IND)
        Act(IND) = -3 .* exp(-0.25.*dct(IND).^2) .* (1+tanh(0.07.*(50-hts(IND))));
        Acr(IND) = -3 .* exp(-0.25.*dcr(IND).^2) .* (1+tanh(0.07.*(50-hrs(IND))));
    end

    % Empirical correction to account for the increasing attenuation with
    % wavelength in ducted propagation
    if fGHz < 0.5
        Alf = 45.375 - 137.0.*fGHz + 92.5.*fGHz.^2;  % (47a)
    else
        Alf = 0;
    end

    % Total of fixed coupling losses between the antennas and the anomalous
    % propagation structure within the atmosphere
    Af = 102.45 + 20.*log10(fGHz) + 20.*log10(dlt+dlr) + Alf + Ast + Asr + ...
        Act + Acr;  % (47)

    % Calculate the smooth-Earth heights at transmitter and receiver as
    % required for the roughness factor
    hst = min(hst,hi(:,1));  % (92a)
    hsr = min(hsr,hi(:,end));  % (92b)
end

% Calculate hte and hre
htg = h_Tx_agl;
hrg = h_Rx_agl;
if ~isJustBullAndFlatEarth
    hte = htg + hi(:,1) - hst;  % (94a)
    hre = hrg + hi(:,end) - hsr;  % (94b)

    % Parameter mu2
    epsilon = 3.5;
    alpha = max(-0.6-tau.*d.^3.1.*epsilon.*1e-9,-3.4);  % (55a)
    mu2 = min((500./ae.*d.^2./(sqrt(hte)+sqrt(hre)).^2).^alpha,1);  % (55)

    % Slope of the smooth-Earth surface
    m = (hsr-hst) ./ d;

    % Terrain roughness parameter
    hm = cellfun(@(di,hi,hst,m,ilt,ilr) max(hi(ilt:ilr)-(hst+m.*di(ilt:ilr))),...
        num2cell(di,2),num2cell(hi,2),num2cell(hst),num2cell(m),...
        num2cell(ilt),num2cell(ilr));  % (95)

    % Clear hst and hsr because their values have been altered
    clear hst hsr

    % Parameter mu3
    mu3 = ones(length(hm),1);  % (56)
    IND = hm <= 10;
    if any(~IND)
        dI = min(d(~IND)-dlt(~IND)-dlr(~IND),40);  % (56a)
        mu3(~IND) = exp(-4.6e-5.*(hm(~IND)-10).*(43+6.*dI));  % (56)
    end

    % Angular distance to allow for the application of the site shielding model
    thetaprimet = thetat;
    IND = thetat <= 0.1.*dlt;
    if any(~IND)
        thetaprimet(~IND) = 0.1 .* dlt(~IND);
    end
    thetaprimer = thetar;
    IND = thetar <= 0.1 * dlr;
    if any(~IND)
        thetaprimer(~IND) = 0.1 .* dlr(~IND);
    end
    thetaprime = 1e3.*d./ae + thetaprimet + thetaprimer;

    % Angular-distance dependent losses within the anomalous propagation
    % mechanism
    gammad = 5e-5 .* ae .* fGHz.^(1./3);
    beta = beta0 .* mu2 .* mu3;
    Gamma = 1.076 ./ (2.0058-log10(beta)).^1.012 .*...
        exp(-(9.51-4.8.*log10(beta)+0.198.*log10(beta).^2) .* 1e-6 .* d.^1.13);  % (53a)
    Ap = -12 + (1.2+3.7e-3.*d).*log10(p./beta) + 12.*(p./beta).^Gamma;  % (53)
    Ad = gammad.*thetaprime + Ap;  % (50)

    % Basic transmission loss associated with ducting/layer-reflection
    Lba = Af + Ad;  % (46)
else
    Lba = NaN;
end

%% Basic transmission loss not exceeded for p% time ignoring the effects of terminal clutter
if isJustBullAndFlatEarth
    % Basic transmission loss not exceeded for p% time and 50% locations
    % ignoring the effects of terminal clutter
    Lbu = Lbd;  % (63)
else
    % Calculate an interpolation factor to take account of the path angular
    % distance
    Theta = 0.3;  % (57)
    ksi = 0.8;  % (57)
    Fj = 1.0 - 0.5.*(1.0 + tanh(3.0.*ksi.*(theta-Theta)./Theta));  % (57)

    % Calculate an interpolation factor to take account of the path
    % great-circle distance
    dsw = 20;  % (58)
    kappa = 0.5;  % (58)
    Fk = 1.0 - 0.5.*(1.0 + tanh(3.0.*kappa.*(d-dsw)./dsw));  % (58)

    % Calculate a notional minimum basic transmission loss associated with
    % line-of-sight propagation and over-sea sub-path diffraction
    Lminb0p = zeros(length(Lbd50),1);
    IND = p < beta0;
    if any(IND)
        Lminb0p(IND) = Lb0p(IND) + (1-omega(IND)).*Ldp(IND);  % (59)
    end
    if any(~IND)
        if Fi == 0
            Lminb0p(~IND) = Lbd50(~IND);
        else
            Lminb0p(~IND) = Lbd50(~IND) + ...
                (Lb0beta(~IND)+(1-omega(~IND)).*Ldp(~IND)-Lbd50(~IND)) .* Fi;  % (59)
        end
    end

    % Notional minimum basic transmission loss associated with LoS propagation
    % and transhorizon signal enhancements
    eta = 2.5;  % (60)
    Lminbap = eta .* log(exp(Lba./eta)+exp(Lb0p./eta));  % (60)

    % Notional basic transmission loss associated with diffraction and LoS or
    % ducting/layer-reflection enhancements
    Lbda = Lbd;  % (61)
    IND = Lminbap > Lbd;
    if any(~IND)
        Lbda(~IND) = Lminbap(~IND) + (Lbd(~IND)-Lminbap(~IND)).*Fk(~IND);  % (61)
    end

    % Modified basic transmission loss taking diffraction and line-of-sight
    % ducting/layer-reflection enhancements into account
    Lbam = Lbda + (Lminb0p-Lbda).*Fj;  % (62)
    
    % Basic transmission loss not exceeded for p% time and 50% locations
    % ignoring the effects of terminal clutter
    Lbu = -5 .* log10(10.^(-0.2.*Lbs)+10.^(-0.2.*Lbam));  % (63)
end

%% Additional losses due to terminal surroundings
% Simplify
termCluLossMod = Path.termCluLossMod;

% Transmitter end
Rt = gi(:,1) - hi(:,1);
Aht = zeros(length(Rt),1);
IND = htg >= Rt;
if any(~IND)
    IND1 = ~IND & termCluLossMod(:,1)==1;
    IND2 = ~IND & termCluLossMod(:,1)==2;

    % (64a)
    if any(IND1)
        hdif = Rt(IND1) - htg;  % (64d)
        ws = 27;  % Width of the street
        thetaclut = atand(hdif./ws);  % (64e)
        Knu = 0.342 .* sqrt(fGHz);  % (64g)
        v = Knu .* sqrt(hdif.*thetaclut);  % (64c)
        Aht(IND1) = give_single_edge_diffr(v) - 6.03;  % (64a)
    end

    % (64b)
    if any(IND2)
        Kh2 = 21.8 + 6.2.*log10(fGHz);  % (64f)
        Aht(IND2) = -Kh2 .* log10(htg./Rt(IND2));  % (64b)
    end
end

% Receiver end
Rr = gi(:,end) - hi(:,end);
Ahr = zeros(length(Rr),1);
IND = hrg >= Rr;
if any(~IND)
    IND1 = ~IND & termCluLossMod(:,end)==1;
    IND2 = ~IND & termCluLossMod(:,end)==2;

    % (64a)
    if any(IND1)
        hdif = Rr(IND1) - hrg;  % (64d)
        ws = 27;  % Width of the street
        thetaclut = atand(hdif./ws);  % (64e)
        Knu = 0.342 .* sqrt(fGHz);  % (64g)
        v = Knu .* sqrt(hdif.*thetaclut);  % (64c)
        Ahr(IND1) = give_single_edge_diffr(v) - 6.03;  % (64a)
    end
    
    % (64b)
    if any(IND2)
        Kh2 = 21.8 + 6.2.*log10(fGHz);  % (64f)
        Ahr(IND2) = -Kh2 .* log10(hrg./Rr(IND2));  % (64b)
    end
end

%% Basic transmission loss not exceeded for p% time and 50% locations, including the effects of terminal clutter losses, dB
% Calculate Lbc
Lbc = Lbu + Aht + Ahr;  % (65)
end

function [hstd,hsrd] = sec_562(hi,di,htc,hrc,d,hst,hsr)
%% [Hassan to add title]
% [Hassan to add description]
%
% * v0.1 HO 01Oct14 created to implement Section 5163 of the ITU 1812-2 model
% * v0.2 KK 02Oct14 code optimisation
% * v0.3 KK 24Aug17 vectorisation

% Calculate smooth-Earth heights at tx and rx, section 5.1.6.3

% [Hassan to add title]
C1 = bsxfun(@minus,d,di(:,2:end-1));
C2 = bsxfun(@times,htc,C1);
C3 = bsxfun(@times,hrc,di(:,2:end-1));
Hi = hi(:,2:end-1) - bsxfun(@rdivide,C2+C3,d);  % (90d)
hobs = max(Hi,[],2);  % (90a)
alphaobt = max(Hi./di(:,2:end-1),[],2);  % (90b)
alphaobr = max(Hi./C1,[],2);  % (90c)

% [Hassan to add title]
IND = hobs <= 0;
hstp = hst;  % (91a)
hsrp = hsr;  % (91b)
if any(~IND)
    gt = alphaobt(~IND) ./ (alphaobt(~IND)+alphaobr(~IND));  % (91e)
    gr = alphaobr(~IND) ./ (alphaobt(~IND)+alphaobr(~IND));  % (91f)
    hstp(~IND) = hst(~IND) - hobs(~IND).*gt;  % (91c)
    hsrp(~IND) = hsr(~IND) - hobs(~IND).*gr;  % (91d)
end

% [Hassan to add title]
IND = hstp > hi(:,1);
hstd = hi(:,1);  % (92a)
if any(~IND)
    hstd(~IND) = hstp(~IND);  % (92b)
end
IND = hsrp > hi(:,end);
hsrd = hi(:,end);  % (92c)
if any(~IND)
    hsrd(~IND) = hsrp(~IND);  % (92d)
end
end

function Lbull = sec_431(Ce,gi,di,d,htc,hrc,lambda)
%% [Hassan to add title]
% [Hassan to add description]
% 
% v0.1 HO 01Oct14 created to implement Section 4.3.1 of the ITU 1812-2 model
% v0.2 KK 02Oct14 d(n)->d, netw.f->fGHz
% v0.3 KK 03Oct14 output the profile points with the highest diffraction parameter
% v0.4 KK 07Oct14 initialise vmaxI
% v0.5 KK 22Jul15 1812-3
% v0.6 KK 23Jul15 1812-3
% v0.7 KK 24Aug17 Vectorisation

% Simplification
C1 = bsxfun(@minus,d,di(:,2:end-1));
C2 = bsxfun(@times,Ce,di(:,2:end-1));

% Find the intermediate profile point with the highest slope of the line
% from the transmitter to the point, m/km
Stim = max(bsxfun(@minus,gi(:,2:end-1)+500.*C2.*C1,htc)./di(:,2:end-1),...
    [],2);  % (13)

% Calculate the slope of the line from transmitter to receiver assuming a
% LoS path, m/km
Str = (hrc-htc) ./ d;  % (14)

%% Is diffraction path LoS?
% Calculate auxhiliary parameter IND that is true if the path is within LoS
IND = Stim < Str;

% The knife-edge loss for the Bullington point
Luc = zeros(length(IND),1);

%% Diffraction path is LoS
% Calculate Luc if path is LoS
if any(IND)
    % Find the intermediate profile point with the highest diffraction
    % parameter v
    C3 = bsxfun(@times,htc(IND),C1(IND,:));
    C4 = bsxfun(@times,hrc(IND),di(IND,2:end-1));
    C5 = bsxfun(@rdivide,C3+C4,d(IND));
    C6 = bsxfun(@rdivide,d(IND),lambda.*di(IND,2:end-1).*C1(IND,:));
    vmax = max((gi(IND,2:end-1)+500.*C2(IND,:).*C1(IND,:)-C5) .* ...
        sqrt(2e-3.*C6),[],2);  % (15)

    % The knife-edge loss for the Bullington point
    Luc(IND) = give_single_edge_diffr(vmax);  % (16)
end

%% Diffraction path is transhorizon
% Calculate Luc if path is transhorizon
if any(~IND)
    % Find the intermediate profile point with the highest slope of the
    % line from the receiver to the point
    C7 = bsxfun(@minus,gi(~IND,2:end-1)+500.*C2(~IND,:).*C1(~IND,:),hrc(~IND));
    Srim = max(C7./C1(~IND,:),[],2);  % (17)

    % Distance of the Bullington point from the transmitter
    dbp = (hrc(~IND)-htc(~IND)+Srim.*d(~IND)) ./ (Stim(~IND,:)+Srim);

    % Diffraction parameter for the Bullington point
    vb = (htc(~IND) + Stim(~IND).*dbp - ...
        (htc(~IND).*(d(~IND)-dbp)+hrc(~IND).*dbp)./d(~IND)) .* ...
        sqrt(2e-3.*d(~IND)./( lambda.*dbp.*(d(~IND)-dbp)));  % (19)

    % The knife-edge loss for the Bullington point
    Luc(~IND) = give_single_edge_diffr(vb);  % (20)
end

%% Bullington diffraction loss for the path
% Calcualte Lbull
Lbull = Luc + (1-exp(-Luc./6)).*(10+0.02.*d);  % (21)
end

function Ldsph = sec_432(ap,htesph,hresph,d,fGHz,omega,lambda,polarisation)
%% [Hassan to add title]
% [Hassan to add description]
% 
% v0.1 HO 01Oct14 created to implement Section 4.3.2 of the ITU 1812-2 model
% v0.2 KK 02Oct14 optimise code
% v0.3 KK 03Oct14 optimise code
% v0.4 KK 22Jul15 1812-3
% v0.5 KK 23Jul15 1812-3
% v0.6 KK 24Aug17 Vectorised

% Spherical-Earth diffraction loss, Section 4.3.2: Marginal LoS distance
dlos = sqrt(2.*ap) .* (sqrt(0.001.*htesph)+sqrt(0.001.*hresph));  % (22)

%%
% Calculate auxhiliary parameter IND that is true for paths that require
% calculations accoring to Section 4.3.3
IND = d >= dlos;
Ldsph = zeros(length(IND),1);

% Section 4.3.3 - calculating Ldsph for the points where d >= dlos
if any(IND)

    % Call function sec_433
    Ldft = sec_433(ap(IND),htesph(IND),hresph(IND),d(IND),fGHz,omega(IND),...
        polarisation);

    % Set Ldsph equal to Ldft (First paragraph under eq (22))
    Ldsph(IND) = Ldft;
end

% Calculating Ldsph for the points where d < dlos
if any(~IND)
    % Calculate for all points and filter later
    c  = ( htesph - hresph ) ./ ( htesph + hresph );  % (24d)
    mc = 250 .* d.^2 ./  (ap .* ( htesph + hresph ) );  % (24e)
    b  = 2 .* sqrt((mc+1)./3./mc) .* ...
         cos(pi./3+1./3.*acos(3.*c./2.*sqrt(3.*mc./(mc+1).^3)));  % (24c)
    dse1 = d ./ 2 .* (1+b);  % (24a)
    dse2 = d - dse1;  % (24b)
    hse  = ( ( htesph - 500 .* dse1.^2 ./ ap ) .* dse2 + ...
             ( hresph - 500 .* dse2.^2 ./ ap ) .* dse1  ) ./ d;  % (23), Smallest clearnace height between ..

    % Calculate the required clearance for zero diffraction loss
    hreq = 17.456 .* sqrt( dse1 .* dse2 .* lambda ./ d );  % (25)

    % [Hassan to add title]
    Ldft = zeros( length( hse ), 1 );
    IND2 = hse > hreq;
    if any(~IND2)
        % [Hassan to add title]
        aem = 500 .* (d(~IND2)./(sqrt(htesph(~IND2))+sqrt(hresph(~IND2)))).^2;  % (26), modified effective Earth radius

        % go to section 4.3.3 for adft=aem to give Ldft
        Ldft( ~IND2 ) = sec_433( aem, htesph( ~IND2 ), hresph( ~IND2 ), d( ~IND2 ),...
                               fGHz, omega( ~IND2 ), polarisation );
        
        % [Hassan to add title]
        IND3 = Ldft < 0;
        if any( ~IND3 )
            Ldsph( ~IND & ~ IND3 ) = ( 1 - hse( ~IND & ~IND3 ) ./ hreq( ~IND & ~IND3 ) ) .* ...
                                     Ldft( ~IND & ~IND3 );  % (27)
        end
    end
end
end

function Ldft = sec_433( adft, hte, hre, d, fGHz, omega, polarisation )
%% [Hassan to add title]
% [Hassan to add description]
% 
% v0.1 HO 01Oct14 created to implement Section 4.3.3 of the ITU 1812-2 model
% v0.2 KK 02Oct14 Is_land is no longer an input
% v0.3 KK 22Jul15 polarisation as input
% v0.4 KK 24Aug17 vectorisation

% [Hassan to add title]
epsilonr_land = 22.0;  % for land, propagation is assumed over the land
sigma_land    = 0.003; % for land, propagation is assumed over the land
epsilonr_sea  = 80.0;  % for sea, propagation is assumed over the sea
sigma_sea     = 5;     % for sea, propagation is assumed over the sea

% [Hassan to add title]
Ldft =  omega .* eq_29_36( hte, hre, d, fGHz, epsilonr_sea,  sigma_sea,  adft, polarisation ) + ...
    (1-omega) .* eq_29_36( hte, hre, d, fGHz, epsilonr_land, sigma_land, adft, polarisation );  % (28)
end

function Ldft = eq_29_36(hte,hre,d,fGHz,epsilonr,sigma,adft,polarisation)
%% [Hassan to add title]
% [Hassan to add description]
% 
% v0.1 HO 02Oct14 created to implement eq (29)-36) in Section 4.3.3 of the ITU 1812-2 model
% v0.2 KK 03Oct14 optimise code
% v0.3 KK 22Jul15 1812-3
% v0.4 KK 24Aug17 vectorisation

% [Hassan to add title]
KH = 0.036 .* (adft.*fGHz).^(-1./3) .* ...
    ((epsilonr-1).^2+(18.*sigma./fGHz).^2) .^ (-1/4);  % (29a) Normalized factor: Horizontal, HO fixed a bug here, it was epsilonr.^2 (-1 was missing)
Kv = KH .* (epsilonr.^2+(18.*sigma./fGHz).^2).^(1./2);  % (29b) Normalized factor: Vertical
if exist('polarisation','var')
    switch polarisation
        case 'vertical'
            K = Kv;
        case 'horizontal'
            K = KH;
        otherwise
            error('Code this brunch!')
    end
    betadft = (1+1.6.*K.^2+0.67.*K.^4) ./ (1+4.5.*K.^2+1.53.*K.^4);  % (30) K is KH or Kv according to polirization
else
    if fGHz <= 0.3
        error('Polarisation underfined')
    else
        betadft = 1;
    end
end

% Normalized distance
X = 21.88 .* betadft .* (fGHz./adft.^2).^(1./3) .* d;  % (31)

% Normalized transmitter and receiver heights
Yt = 0.9575 .* betadft .* (fGHz.^2./adft).^(1./3) .* hte;  % (32a)
Yr = 0.9575 .* betadft .* (fGHz.^2./adft).^(1./3) .* hre;  % (32b)

% Calculate the distance term
FX = zeros(length(X),1);
IND = X >= 1.6;
if any(IND)
    FX(IND) = 11 + 10.*log10(X(IND)) - 17.6.*X(IND);  % (33)
end
if any(~IND)
    FX(~IND) = -20.*log10(X(~IND)) - 5.6488.*X(~IND).^1.425;  % (33)
end

% Function of normalized height
GYt = func_GY(betadft,Yt,K);  % Call function "func_GY" (HO added K to fix bug)
GYr = func_GY(betadft,Yr,K);  % Call function "func_GY" (HO added K to fix bug)

% First spherical diffraction loss is given by:
Ldft = -FX - GYt - GYr;  % (36)
end

function GY = func_GY(betadft,Y,K)
%% [Hassan to add title]
% [Hassan to add description]
% 
% v0.1 HO 01Oct14 created to implement GY in Section 4.3.3 of the ITU 1812-2 model
% v0.2 KK 24Aug17 vectorisation

% Function of normalized height
B = betadft .* Y;  % (35)
GY = zeros(length(B),1);
IND = B > 2;
if any(IND)
    GY(IND) = 17.6.*(B(IND)-1.1).^0.5 - 5.*log10(B(IND)-1.1) - 8;  % (34)
end
if any(~IND)
    GY(~IND) = 20 .* log10(B(~IND)+0.1.*B(~IND).^3);
end

% Condition under eq (35) (bug fixed)
if GY < 2 + 20*log10(K)
    GY = 2 + 20*log10(K);
end

end

function J = give_single_edge_diffr(v)
%% Calculate single edge diffraction
% give_single_edge_diffr calculates the losses, J, attributed to single
% edge difraction with parameter v
% 
% v0.1 KK 22Jun11 Created

% Calculate the losses, J
IND = v >= -0.78;
J = zeros(length(IND),1);
if any(IND)
    J(IND) = 6.9 + 20.*log10(sqrt((v(IND)-0.1).^2+1)+v(IND)-0.1);
end
end
