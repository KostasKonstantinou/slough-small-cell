function [Lbc,sigmaLe] = c_Lbc_5GCM(dist3Dm,freqGhz,isLOS,isOp)
%% Calculate path loss at mmWave according to 5GCM [1]
% c_Lbc_5GCM calculates the path loss at a 3D separation distance of
% dist3Dm expressed in metres, at frequency freqGhz expressed in GHz. isLOS
% is a logical vector with the same dimensions as dist3Dm, with elements
% that are true for paths within LOS. isOp is a logical vector with the
% same dimensions as dist3Dm, with elements that are true for UMi-Open
% Square LOS/NLOS and false for UMi-Street Canyon LOS/NLOS ABG. sigmaLe is
% the standard deviation of shadowing.
% 
% [1] 5GCM, �5G channel model for bands up to 100 GHz,� Tech. Rep., Oct.
% 2016. [Online]. Available: http://www.5gworkshops.com/5GCM.html
% 
% Example 1: 
% Calculate path loss at mmWave according to 5GCM for a number of points
%
% dist3Dm = [200;180;158;150;158;200;140;200];
% freqGhz = 26;
% isLOS = [false,false,false,false,false,true,false,false];
% isOp = [false,false,false,false,false,false,false,true];
%
% The above example returns:   
% Lbc =
%   133.7653
%   132.1501
%   130.1515
%   129.3550
%   130.1515
%   109.0211
%   128.2973
%   133.3065
% sigmaLe =
%   Columns 1 through 6
%     7.8200    7.8200    7.8200    7.8200    7.8200    3.7600
%   Columns 7 through 8
%     7.8200    7.0000
% 
% Kostas Konstantinou Oct 2018

% Calculate path loss
Lbc = zeros(length(dist3Dm),1);
if any(isLOS&~isOp)
    % UMi-Street Canyon LOS
    Lbc(isLOS&~isOp) = 32.4 + 21.*log10(dist3Dm(isLOS&~isOp)) + ...
        20.*log10(freqGhz);
end
if any(~isLOS&~isOp)
    % UMi-Street Canyon NLOS
    Lbc(~isLOS&~isOp) = 35.3.*log10(dist3Dm(~isLOS&~isOp)) + 22.4 + ...
        21.3.*log10(freqGhz);
end
if any(isLOS&isOp)
    % UMi-Open Square LOS
    Lbc(isLOS&isOp) = 32.4 + 18.5.*log10(dist3Dm(isLOS&isOp)) + ...
        20.*log10(freqGhz);
end
if any(~isLOS&isOp)
    % UMi-Open Square NLOS
    Lbc(~isLOS&isOp) = 41.4.*log10(dist3Dm(~isLOS&isOp)) + 3.66 + ...
        24.3.*log10(freqGhz);
end

% Calculate the SD of shadowing
sigmaLe = 3.76.*(isLOS&~isOp) + 7.82.*(~isLOS&~isOp) + ...
    4.2.*(isLOS&isOp) + 7.0.*(~isLOS&isOp);
