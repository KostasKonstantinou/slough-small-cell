function Cell = c_per_sq_Extended_Hata(Cell,fileName,Sim,clu,ter,dem,vx,vy,studyA,...
    BoundingBox,spotFreqMhz,UE,fn,Un,maxDist_m)
%% Calculates path loss per OS square
% c_per_sq calculates the propagation of cells within an OS square.
% The input fileName is the string that corresponds to the name of the OS
% square. 
% The input dem is one of Sim.propag.pix which contains the locations of
% pixels that propagation is required in a 5-column format
% [X,Y,I,J,clu_Hata].
% The input vx and vy are 1xN cell arrays, where N is the number of cells,
% with the extent of coverage based on Voronoi geometry.
% c_per_sq saves on the hard disk and updates the Lbc and n fields of Cell.
% 
% The input BoundingBox
% spotFreqMhz
% fn
% Un
% 
% The input UE is one row of Sim.propag.UE.
% 
% Cell is an array of CELL objects. [A list of the required fields is pending]
% Sim is the SIM object. [A list of the required fields is pending]
% The input studyA is the study area object. [A list of the required fields is pending]
% clu is the CLU object. [A list of the required fields is pending]
% ter is the TER object. [A list of the required fields is pending]
%
% * v0.1 Kostas Konstantinou Jul 2017

% MAPL = 163.4;  % Maximum allowable path loss in dB

% Simplification
hm = UE{2};  % UE height
clu_tran = UE{3};  % clutter translation
loc = UE{4};  % UE locations

Hash = arrayfun(@(x) DataHash(x),...
                        struct( 'X',        { Cell.X },...
                                'Y',        { Cell.Y },...
                                'heiAgl',   { Cell.heiAgl },...
                                'spotFreqMhz',spotFreqMhz,...
                                'hm',       hm,...
                                'clu_tran', clu_tran,...
                                'propMod',  Sim.propag.mod,...
                                'loc',      {loc} ), 'UniformOutput', false );

% Check if we have results for this square
[S,exitFlag] = loadDr(fullfile(Sim.path.DB.Lbc,fileName),'Lbc');
if exitFlag == 0
	Lbc = S.Lbc;
else
	% File did not exist
    Lbc = struct('X',{},'Y',{},'heiAgl',{},'spotFreqMhz',{},'A',{},'DataHash',{},'demSour',[]);
end
[~,LbcI] = ismember(Hash,{Lbc.DataHash});

%%
% sh_SD: shadowing standard deviation
sh_SD.Hata = 0.65.*log10(spotFreqMhz).^2 - 1.3.*log10(spotFreqMhz) + ...
    [5.2 5.2 6.6 6.6];  % [urb, dnsUrb, sub, rur]

col_idx_clut_h_extreme      = 7;  % 'for i=1 and n in (1c)'
col_idx_clut_h_intermediate = 6;  % 'for i=2 to n-1 in (1c)'

% For each site, calculate path loss
isSave = false;
for cn = 1 : length(Cell)
    % Calculate the row and column in the clutter
    % database at the location of BS
    [row_clu,col_clu] = worldToSub(clu.R,Cell(cn).X,Cell(cn).Y);

%     if any(LbcI) && isempty(Lbc(LbcI).A)
%         Lbc(LbcI) = [];
%         LbcI(LbcI) = [];
%     end

    if LbcI(cn) > 0
        if Lbc(LbcI(cn)).BoundingBox(1,1) <= BoundingBox{cn}(1,1) && ...
                BoundingBox{cn}(2,1) <= Lbc(LbcI(cn)).BoundingBox(2,1) && ...
                Lbc(LbcI(cn)).BoundingBox(1,2) <= BoundingBox{cn}(1,2) && ...
                BoundingBox{cn}(2,2) <= Lbc(LbcI(cn)).BoundingBox(2,2)
            Cell(cn).Lbc{fn,Un} = Lbc(LbcI(cn)).A;
            Cell(cn).BoundingBox{fn,Un} = BoundingBox{cn};
            if ~isempty(Lbc(LbcI(cn)).A)
                if Cell(cn).heiAgl >= clu.tran.(clu_tran)...
                        {clu.A(row_clu,col_clu)+1,col_idx_clut_h_extreme}
                    if spotFreqMhz < 6000
                        Cell(cn).n(fn,Un) = (44.9 - ...
                            6.55.*log10(max(30,Cell(cn).heiAgl))) ./ 10;
                    else
                        error('Code this!')
                    end
                else
                    if spotFreqMhz < 6000
                        Cell(cn).n(fn,Un) = 2.5;
                    else
                        beep
                        disp('Code IND as a few paragrpahs below')
                        keyboard
                        Cell(cn).n(fn,Un) = round((2.1.*nnz(IND) + ...
                            1.85.*nnz(~IND)) ./ length(IND),2);
                    end
                end
            end
            % The propagation exists
%                 figure
%                 axis equal
%                 hold on
%                 plot(pop.X,pop.Y,'.')
%                 plot(Cell(cn).X,Cell(cn).Y,'ro')
%                 plot(Cell(cn).X,Cell(cn).Y,'r.')
%                 plot(Lbc(LbcI(cn)).BoundingBox([1 1 2 2 1],1),...
%                     Lbc(LbcI(cn)).BoundingBox([1 2 2 1 1],2))
%                 plot(BoundingBox{cn}([1 1 2 2 1],1),...
%                     BoundingBox{cn}([1 2 2 1 1],2))
            continue
        else
            % The propagation needs to be extended
            BoundingBox{cn} = ...
                [min(BoundingBox{cn}(1,1),Lbc(LbcI(cn)).BoundingBox(1,1)),...
                min(BoundingBox{cn}(1,2),Lbc(LbcI(cn)).BoundingBox(1,2))
                max(BoundingBox{cn}(2,1),Lbc(LbcI(cn)).BoundingBox(2,1)),...
                max(BoundingBox{cn}(2,2),Lbc(LbcI(cn)).BoundingBox(2,2))];
%                 plot(BoundingBox2([1 1 2 2 1],1),...
%                     BoundingBox2([1 2 2 1 1],2))
            Ln = LbcI(cn);
        end
    else
        % The propagation is new
        Ln = length(Lbc) + 1;
    end

    isSave = true;
%         dem  % [X,Y,I,J,clu_Hata]

    % Set the frequency for path loss calculations
%         spotFreqMhz = Cell(cn).spotFreqMhz;
%     spotFreqGhz = spotFreqMhz ./ 1e3;

    % Calculate path loss if path loss estimation points are within a
    % sensible distance from a site. There is a maximum cutoff distance
    % due to RACHpream and an extended Voronoi to combine.
%         plot(max(Cell(cn).X-Cell(cn).maxDist_m,min(vx{cn})),...
%             max(Cell(cn).Y-Cell(cn).maxDist_m,min(vy{cn})),'rx')
%         plot(min(Cell(cn).X+Cell(cn).maxDist_m,max(vx{cn})),...
%             min(Cell(cn).Y+Cell(cn).maxDist_m,max(vy{cn})),'rx')
%         plot(BoundingBox{cn}([1,1,2,2,1],1),BoundingBox{cn}([1,2,2,1,1],2))
%         plot(dem(:,1),dem(:,2),'.')
%         BoundingBox = Cell(cn).BoundingBox;
    isClo1 = find(BoundingBox{cn}(1,1) <= dem(:,1) & ...
        dem(:,1) <= BoundingBox{cn}(2,1) & ...
        BoundingBox{cn}(1,2) <= dem(:,2) & ...
        dem(:,2) <= BoundingBox{cn}(2,2));
    A = [];
    if ~isempty(isClo1)
        distm = sqrt((dem(isClo1,1) - Cell(cn).X).^2 + ...
            (dem(isClo1,2) - Cell(cn).Y).^2);
        distkm = distm ./ 1e3;

        % Find path loss estimation points are within a sensible
        % distance from a site
        if ~all(isinf(vx{cn}))
            isClo2 = find(distm<=maxDist_m{cn} & ...
                inpolygon(dem(isClo1,1),dem(isClo1,2),vx{cn},vy{cn}));
        else
            isClo2 = find(distm<=maxDist_m{cn});
        end

        if ~isempty(isClo2)
            % Calculate path loss
            if Cell(cn).heiAgl >= clu.tran.(clu_tran)...
                    {clu.A(row_clu,col_clu)+1,col_idx_clut_h_extreme}
                % For each frequency, calculate path loss
                if spotFreqMhz < 6000
                    % Above rooftop, <6GHz, use Extended Hata model

                    % find indices of pixels close to BS
%                         [I_isClo,J_isClo] = worldToSub(studyA.R,...
%                             dem(isClo1(isClo2),1),dem(isClo1(isClo2),2));
%                         ind_isClo = sub2ind(size(Lbc_{fn,Un,cn}),I_isClo,J_isClo);

                    % compute path-loss
                    tmp = give_LdBnoAngleNoMCL(...
                        spotFreqMhz,distkm(isClo2),Cell(cn).heiAgl,hm,...
                        dem(isClo1(isClo2),5),'RW_Ofcom');

                    % Add terrain variability losses
                    if spotFreqMhz < 1400
                        tmp2 = [0 0 3.8 12.6];
                    elseif spotFreqMhz < 2800
                        tmp2 = [0 0 mean([0,3.8]) mean([3.8,12.6])];
                    else
                        tmp2 = [0 0 0 3.8];
                    end
                    tmp = tmp + tmp2(dem(isClo1(isClo2),5)).';
                    
                    % Calculate the SD of shadowing (external situation)
                    sigmaLe = sh_SD.Hata(dem(isClo1(isClo2),5)).';

                    % Increase of path loss with distance
                    Cell(cn).n(fn,Un) = (44.9 - ...
                        6.55.*log10(max(30,Cell(cn).heiAgl))) ./ 10;

                    % For each point that is sensible distance from a site,
                    % calculate path loss and SD of shadowing
                    A = [dem(isClo1(isClo2),3:4),tmp,sigmaLe,...
                        distkm(isClo2).*1000,zeros(length(isClo2),3)];
                else
                    % Above rooftop, >6GHz, no model available
                    beep
                    disp('Code this!')
                    keyboard

                    % compute LOS link based on terrain and clutter
                    isLOS = get_isLOS(Cell(cn),...
                        dem(isClo1(isClo2),:),hm,clu,...
                        studyA,ter,col_idx_clut_h_extreme,...
                        col_idx_clut_h_intermediate,Sim,clu_tran,...
                        @get_Prob_LOS_UMa,@get_Prob_LOS_RMa);
                end
            else
                % For each frequency, calculate path loss
                if spotFreqMhz < 6000
                    % Below rooftop, <6GHz, use ITU-R 1411

                    % Find if co-located
                    isColoc = distm(isClo2).' == 0;

                    % compute LOS link based on terrain and clutter
                    isLOS = get_isLOS(Cell(cn),...
                        dem(isClo1(isClo2),:),hm,clu,studyA,ter,...
                        col_idx_clut_h_extreme,...
                        col_idx_clut_h_intermediate,Sim,clu_tran,...
                        @c_prob_LOS_UMi_5GCM);

                    tmp = zeros(length(isClo2),1);
                    sigmaLe = zeros(length(isClo2),1);

                    % Pathloss for co-location
                    tmp(isColoc) = 32.4 + 20.*log10(spotFreqMhz) + ...
                        20.*log10((Cell(cn).heiAgl-hm)./1e3);

                    % LOS path-loss
%                         [I_isClo_isLOS,J_isClo_isLOS] = ...
%                             worldToSub(studyA.R,...
%                             dem(isClo1(isClo2(isLOS)),1),...
%                             dem(isClo1(isClo2(isLOS)),2));
                    dist3Dm = sqrt(distm(...
                        isClo2(isLOS&~isColoc)).^2 + ...
                        (Cell(cn).heiAgl-hm).^2);
                    tmp(isLOS&~isColoc) = give_ITU_R_P_1411_7(...
                        spotFreqMhz,Cell(cn).heiAgl,hm,dist3Dm,...
                        {'LOS','bound','upper','traffic','heavy'});

                    % Calculate the SD of shadowing
                    sigmaLe(isLOS) = 2^-52;

                    % NLOS path-loss
%                         [I_isClo_is_N_LOS,J_isClo_is_N_LOS] = ...
%                             worldToSub(studyA.R,...
%                             X(isClo1(isClo2(~isLOS))),...
%                             Y(isClo1(isClo2(~isLOS))));
%                         ind_isClo_is_N_LOS = sub2ind(size(Lbc_{fn,Un,cn}),I_isClo_is_N_LOS,J_isClo_is_N_LOS);
                    if any(~isLOS)
                        environment_ = {'urban','residential'};
                        for n = 1 : length(environment_)
                            switch n
                                case 1
                                    IND = dem(isClo1(isClo2),5)<=2 & ~isLOS.';
                                case 2
                                    IND = dem(isClo1(isClo2),5)>=3 & ~isLOS.';
                            end

                            if any(IND)
                                environment = environment_{n};
                                tmp(IND) = give_ITU_R_P_1411_7(...
                                    spotFreqMhz,Cell(cn).heiAgl,...
                                    hm,distm(isClo2(IND)),...
                                    {'NLOS','a',pi./2,...
                                    'x_1',distm(isClo2(IND))./sqrt(2),...
                                    'x_2',distm(isClo2(IND))./sqrt(2),...
                                    'w_1',21,'w_2',21,...
                                    'traffic','heavy',...
                                    'environment',environment,...
                                    'bound','median'});
                            end
                        end
                    end

                    % Add terrain variability losses
                    if spotFreqMhz < 1400
                        tmp2 = [0 0 3.8 12.6];
                    elseif spotFreqMhz < 2800
                        tmp2 = [0 0 mean([0,3.8]) mean([3.8,12.6])];
                    else
                        tmp2 = [0 0 0 3.8];
                    end
                    tmp = tmp + tmp2(dem(isClo1(isClo2),5)).';
                    
                    % Calculate the SD of shadowing
                    sigmaLe(~isLOS) = ...
                        sh_SD.Hata(dem(isClo1(isClo2(~isLOS)),5));

                    % Increase of path loss with distance.
                    % Heuristically it was found that the service area
                    % of a small cell consistes solely of LOS pixels.
                    % For the considered frequency, 2100 MHz, the
                    % breakpoint distance is beyond the service area of
                    % the site, thus propagation exponent is about 2.5.
                    % It was found heuristically that the exponent is
                    % closer to 2.6 because of the slant range,
                    % however, for simplicity n is assumed at 2.5.
%                         Cell(cn).n{fn,Un} = zeros(length(isClo2),1);
%                         p = polyfit(10.*log10(distm(isClo2(isLOS&~isColoc))),...
%                             tmp(isLOS&~isColoc),1);
%                         Cell(cn).n{fn,Un}(isLOS&~isColoc) = p(1);
%                         p = polyfit(10.*log10(distm(isClo2(~isLOS))),...
%                             tmp(~isLOS),1);
%                         Cell(cn).n{fn,Un}(~isLOS) = p(1);
                    Cell(cn).n(fn,Un) = 2.5;

                    % For each point that is sensible distance from a site,
                    % calculate path loss and SD of shadowing
                    A = [dem(isClo1(isClo2),3:4),tmp,sigmaLe,...
                        distm(isClo2),zeros(length(isClo2),3)];
                else
                    % Below rooftop, >6GHz, use mmWave model

                    % Estimate LOS property based on terrain and
                    % clutter
                    isLOS = get_isLOS(Cell(cn),...
                        dem(isClo1(isClo2),:),hm,clu,...
                        studyA,ter,col_idx_clut_h_extreme,...
                        col_idx_clut_h_intermediate,Sim,clu_tran,...
                        @c_prob_LOS_UMi_5GCM);

                    % Calculate path loss and SD of shadowing
                    spotFreqGhz = spotFreqMhz ./ 1e3;
                    dist3Dm = sqrt(distm(isClo2).^2 + ...
                        (Cell(cn).heiAgl-hm).^2);
                    IND = dem(isClo1(isClo2),5).' <= 3;  % urban, suburban
                    [tmp,sigmaLe] = c_Lbc_5GCM(dist3Dm,spotFreqGhz,...
                        isLOS,~IND);

                    % Increase of path loss with distance.
                    % Similar to cmWave small cells, the service area
                    % of the site is within LOS.
                    Cell(cn).n(fn,Un) = round((2.1.*nnz(IND) + ...
                        1.85.*nnz(~IND)) ./ length(IND),2);

                    % Margin for attenuation due to rain (dB/km)
                    Rp = geointerp(Sim.Rp.A.(UE{5}),Sim.Rp.R,...
                        Cell.lat,Cell.lon);
                    gammaR = c_itu_838_3(spotFreqGhz,Rp,0,'slant');

                    % Margin for atmospheric loss in (dB/km)
                    atmDBkm = c_itu_676_11(spotFreqGhz);

                    % For each point that is sensible distance from a site,
                    % calculate path loss and SD of shadowing
                    A = [dem(isClo1(isClo2),3:4),tmp,sigmaLe.',...
                        distm(isClo2),zeros(length(isClo2),3),...
                        (gammaR+atmDBkm).*dist3Dm./1e3];
                end
            end
            
            % Calculate:
            %   h - terrain (ground) height asl (above mean sea
            %       level)
            %   v1 - antenna boresight unit vector (commented out as not
            %        needed)
            %   v2 - antenna to UE unit vector
            %   az - azimuth of v2 wrt v1 (commented out as not
            %        needed)
            %   elev - elevation of v2 wrt v1 (commented out as not needed)
            [I,J] = ter.R.worldToDiscrete(...
                [Cell(cn).X;dem(isClo1(isClo2),1)],...
                [Cell(cn).Y;dem(isClo1(isClo2),2)]);
            h = ter.A(sub2ind(size(ter.A),I,J));
%             v1 = [cosd(90-double(Cell(n).az)).*sind(90+Cell(n).tilt)
%                 sind(90-double(Cell(n).az)).*sind(90+Cell(n).tilt)
%                 cosd(90+Cell(n).tilt)].';
            v2 = [dem(isClo1(isClo2),1:2) hm+h(2:end)] - ...
                repmat([Cell(cn).X Cell(cn).Y Cell(cn).heiAgl+h(1)],...
                length(isClo2),1);
            v2 = v2 ./ repmat(sqrt(sum(v2.^2,2)),1,3);
%             az(n,:) = wrapTo180(atan2d(v2(:,2),v2(:,1)) - ...
%                 atan2d(v1(2),v1(1)));
%             elev = abs(acosd(v2(:,3)) - acosd(v1(3)));

            % []
            A(:,6:8) = v2;
        end
    end

    % Append entry
    Lbc(Ln).X = Cell(cn).X;
    Lbc(Ln).Y = Cell(cn).Y;
    Lbc(Ln).heiAgl = Cell(cn).heiAgl;
    Lbc(Ln).spotFreqMhz = spotFreqMhz;
    Lbc(Ln).DataHash = Hash{cn};
    Lbc(Ln).A = A;
    Lbc(Ln).n(Un) = Cell(cn).n(fn,Un);
    Lbc(Ln).hm = hm;
    Lbc(Ln).clu_tran = clu_tran;
    Lbc(Ln).loc = loc;
    Lbc(Ln).BoundingBox = BoundingBox{cn};
    
    Cell(cn).Lbc{fn,Un} = A;
    Cell(cn).BoundingBox{fn,Un} = BoundingBox{cn};
end

% if isempty(Lbc(1).X)
%     fsdfgdffgd
% end

% Save the path loss calculations for that square into the
% database, only if we have added new information into it. Avoiding
% thus creating empty files.
if isSave
    argum = true;
    while argum
        try
            save(fullfile(Sim.path.DB.Lbc,fileName),'Lbc')
            argum = false;
        catch
            pause(rand())
        end
    end
end
