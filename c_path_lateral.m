function [Lbd50,States] = c_path_lateral(p1,p2,ter,dsm,fGHz)

if nargin == 0
    Sim.propag.Deltadt = 1;
    
    fGHz = 3.5;
    
    p1 = [497900 179950 8];
    p2 = [498000 180000 1.5];
    
    rng(1636+0)
    while true
        p1 = [(ter.R.XWorldLimits(2)-ter.R.XWorldLimits(1)).*rand() + ...
            ter.R.XWorldLimits(1) ...
            (ter.R.YWorldLimits(2)-ter.R.YWorldLimits(1)).*rand() + ...
            ter.R.YWorldLimits(1) 8];
        p2 = [p1(1:2)+rand(1,2).*100 1.5];
        [I,J] = ter.R.worldToDiscrete([p1(1) p2(1)],[p1(2) p2(2)]);
        
        if ~any(dsm.A(sub2ind(size(dsm.A),I,J)) - ...
                ter.A(sub2ind(size(dsm.A),I,J)) > [8 1.5])
            break
        end
    end
    
    p1 = [497520 179720 8];
    p2 = [497530 179670 1.5];
end

% plot(p1(1),p1(2),'bo')
% plot(p2(1),p2(2),'bx')

% Define a box for calculation
start = p1(1:2);
goal = p2(1:2);
conte = 0.20;  % Context around the start/goal locations
BoundingBox = [min([start;goal],[],1); max([start;goal],[],1)] + ...
    max(abs(start-goal)).*conte.*repmat([-1;1],1,2);
BoundingBox = [max([BoundingBox(1,:);
    [ter.R.XWorldLimits(1) ter.R.YWorldLimits(1)]],[],1);
    min([BoundingBox(2,:);
    [ter.R.XWorldLimits(2) ter.R.YWorldLimits(2)]],[],1)];

% Define the projection of p1 onto xOy
p3 = [p1(1:2) 0];

% create direction vectors
v1 = p2 - p1;
v2 = p3 - p1;

% Find the rotation matrix for 90deg around p1-p2 (v1)
v1 = v1 ./ norm(v1);
C = [0 -v1(3) v1(2); v1(3) 0 -v1(1); -v1(2) v1(1) 0];
Rv1 = eye(3) + C.*sind(90) + C*C.*(1-cosd(90));
% Rv1 * v1.'
% Rv1 * v2.'

% figure
% hold on
% % patch('Faces',TR.faces,'Vertices',TR.vertices);
% set(gca,'DataAspectRatio',[1 1 1])
% alpha(1)
% plot3(p1(1),p1(2),p1(3),'o')
% plot3(p2(1),p2(2),p2(3),'o')
% axis equal
% grid on
% view(3)
% xlabel('x'); ylabel('y'); zlabel('z');
% P2 = p1 + (Rv1*v2.').';
% % plot3(P2(1),P2(2),P2(3),'o')
% patch([p1(1);p2(1);P2(1)],[p1(2);p2(2);P2(2)],[p1(3);p2(3);P2(3)],0)
% Xlim = xlim();
% Ylim = ylim();
% Zlim = zlim();
% set(gca,'xlim',Xlim,'ylim',Ylim,'zlim',Zlim)
% Divider = [p1; p2; P2];
% m = [Divider(2,:)-Divider(1,:); Divider(3,:)-Divider(1,:)];
% %         (x-x1).*det(m(:,2:3)) - (y-y1).*det(m(:,[1,3])) + ...
% %             (z-z1).*det(m(:,1:2)) == 0
% [XlimM,YlimM] = meshgrid(Xlim,Ylim);
% z = ((YlimM-Divider(1,2)).*det(m(:,[1,3])) - ...
%     (XlimM-Divider(1,1)).*det(m(:,2:3))) ./ det(m(:,1:2)) + ...
%     + Divider(1,3);
% patch(XlimM([1,2,4,3]),YlimM([1,2,4,3]),z([1,2,4,3]),0)
% text(p1(1),p1(2),p1(3),' Tx')
% text(p2(1),p2(2),p2(3),' Rx')

% Rotate v2 with the rotation matrix calculated above.
% Define the plane that passes from points p1 p2 and P2, i.e. the
% 'horizontal' propagation plane.
% Find the pixels of clutter that exceed the 'horizontal' propagation
% plane.
P2 = p1 + (Rv1*v2.').';
Divider = [p1; p2; P2];
m = [Divider(2,:)-Divider(1,:); Divider(3,:)-Divider(1,:)];
z = @(x,y) ((y-Divider(1,2)).*det(m(:,[1,3])) - ...
    (x-Divider(1,1)).*det(m(:,2:3))) ./ det(m(:,1:2)) + Divider(1,3);
clear P2 Divider m

[I,J] = ter.R.worldToDiscrete(BoundingBox(:,1),BoundingBox(:,2));

% figure
% imagesc(double(dsm.A(I(2):I(1),J(1):J(2)) - ter.A(I(2):I(1),J(1):J(2))))
% axis equal

[J2,I2] = meshgrid(J(1):J(2),I(2):I(1));
[X,Y] = dsm.R.intrinsicToWorld(J2,I2);

% figure
% plot3(X(:),Y(:),reshape(z(X,Y),[],1),'.')

% figure
% imagesc(double(dsm.A(I(2):I(1),J(1):J(2)) - ...
%     ter.A(I(2):I(1),J(1):J(2))) - z(X,Y) > 0)
% axis equal

% addpath('D:\Real Wireless\01 RW Shared - General\Modelling\Bitbucket\KK\royal-docks\Library')
map.R = dsm.R;
map.INobst = true(map.R.RasterSize(1),map.R.RasterSize(2));
map.INobst(I(2):I(1),J(1):J(2)) = double(dsm.A(I(2):I(1),J(1):J(2)) - ...
    ter.A(I(2):I(1),J(1):J(2)))-z(X,Y) > 0;
States = plan_Astar(map,start,goal);

% Simplify for increased speed
R = [map.R.XWorldLimits(1)
    map.R.YWorldLimits(2)
    map.R.CellExtentInWorldX
    map.R.CellExtentInWorldY];

States = simpli_States(States,R,start,goal,map,1);
States = simpli_States(States,R,start,goal,map,2);

% figure
% hold on
% plot(States{1}(:,1),States{1}(:,2))

orienta = [NaN; atan2d(States{1}(2:end,2)-States{1}(1:end-1,2),...
    States{1}(2:end,1)-States{1}(1:end-1,1))];
k_ = [sign(wrapTo180(orienta(2:end)-orienta(1:end-1))); NaN];
k0 = k_(2);
star = 1;
fini = [];
k = 3;
while true
    if k_(k) ~= k0
        fini = [fini; k];
        star = [star; k];
        k0 = k_(k+1);
        k = k + 2;
    else
        k = k + 1;
    end
    
    if k > length(k_)
        star(end) = star(end) - 0.5;
        fini(end) = fini(end) - 0.5;
        fini = [fini; length(k_)];
        break
    elseif k == length(k_)
        fini = [fini; length(k_)];
        break
    end
end
clear orienta k_ k0 k

lambda = 0.2998 ./ fGHz;

% The knife-edge loss for the Bullington point
Luc = zeros(length(star),1);

% Distance, metres
d = zeros(length(star),1);

for n = 1 : length(star)
    if round(star(n)) == star(n)
        stateStar = States{1}(star(n),:);
    else
        stateStar = mean(States{1}(floor(star(n))+(0:1),:),1);
    end
    
    if round(fini(n)) == fini(n)
        stateFini = States{1}(fini(n),:);
    else
        stateFini = mean(States{1}(floor(fini(n))+(0:1),:),1);
    end
    
    stateDiff = stateFini - stateStar;

%         dist = sqrt(sum(stateDiff.^2));

%         % Calculate the path trajectory; Ofcom recommendations, A1.13, 4G Coverage Obligation Notice of
%         % Compliance Verification Methodology: LTE
%         N = max(1+ceil(dist./Sim.propag.Deltadt),3);

%         % Interpolate between start and finish states
%         interpStates = bsxfun(@times,stateDiff,linspace(0,1,N).') + ...
%             stateStar;

%         % Check all interpolated states for validity
%         % [I,J] = studyA.R.worldToDiscrete(interpStates(:,1),interpStates(:,2));
%         I = floor((R(2)-interpStates(:,2))./R(4)) + 1;
%         J = floor((interpStates(:,1)-R(1))./R(3)) + 1;
%         INobst = map.INobst(sub2ind(size(map.INobst),I,J));

%         figure
%         imagesc(map.INobst)
%         axis equal
% %         hold on
% %         plot(J([1,end]),I([1,end]))

%         figure
%         axis equal
%         hold on
%         plot(States{1}(star(n):fini(n),1),States{1}(star(n):fini(n),2))
%         plot([stateStar(1) stateFini(1)],[stateStar(2) stateFini(2)])

    theta = atan2d(stateDiff(2),stateDiff(1));

    % Rotation matrix
    rotatiM = [cosd(theta) -sind(theta)
        sind(theta) cosd(theta)];

%         % Find which pixels need to be considered
%         cellsize = map.R.CellExtentInWorldX;
%         X = [floor(min(States{1}(star(n):fini(n),1))./cellsize) ...
%             ceil(max(States{1}(star(n):fini(n),1))./cellsize)] .* cellsize;
%         Y = [floor(min(States{1}(star(n):fini(n),2))./cellsize) ...
%             ceil(max(States{1}(star(n):fini(n),2))./cellsize)] .* cellsize;
%         [X,Y] = meshgrid(X(1):X(2),Y(1):Y(2));

    % []
    thisState = States{1}(ceil(star(n)):floor(fini(n)),:);
    if round(star(n)) ~= star(n)
        thisState(1,:) = mean(States{1}(floor(star(n))+(0:1),:),1);
    end
    if round(fini(n)) ~= fini(n)
        thisState(end,:) = mean(States{1}(floor(fini(n))+(0:1),:),1);
    end

    % Create a bounding box at the extremes of the states
    BoundingBox = [min(thisState(:,1)) min(thisState(:,2))
        max(thisState(:,1)) max(thisState(:,2))];

    % Ensure that there is enough context for a 45 degree rotation
    BoundingBox = [-1;1]*abs(stateDiff)./sind(45)./2 + BoundingBox;
    
    % Chop at the ter map limits
    BoundingBox = [max([BoundingBox(1,:);
        [ter.R.XWorldLimits(1) ter.R.YWorldLimits(1)]],[],1);
        min([BoundingBox(2,:);
        [ter.R.XWorldLimits(2) ter.R.YWorldLimits(2)]],[],1)];
    
%         figure
%         s = imagesc(J0,I0(2:-1:1),map.INobst(I0(end):I0(1),J0(1):J0(end)));
%         axis equal
%         hold on
%         plot([J(1) J(end)],[I(1) I(end)])

    % Create an occupancy matrix with the centre point roughly in the
    % middle
    [I0,J0] = map.R.worldToDiscrete(BoundingBox(:,1),...
        BoundingBox(:,2));
    if round((I0(2)-I0(1))./2) ~= (I0(2)-I0(1))./2
        if I0(1) > 1
            I0(1) = I0(1) - 1;
        else
            I0(2) = I0(2) + 1;
        end
    end
    if round((J0(2)-J0(1))./2) ~= (J0(2)-J0(1))./2
        if J0(1) > 1
            J0(1) = J0(1) - 1;
        else
            J0(2) = J0(2) + 1;
        end
    end
    thisMap = map.INobst(I0(end):I0(1),J0(1):J0(end));
    
    % Ensure map for rotation is square
    if size(thisMap,1) > size(thisMap,2)
        N = (size(thisMap,1)-size(thisMap,2)) ./ 2;
        thisMap = [true(size(thisMap,1),N) thisMap true(size(thisMap,1),N)];
    elseif size(thisMap,1) < size(thisMap,2)
        N = -(size(thisMap,1)-size(thisMap,2)) ./ 2;
        thisMap = [true(N,size(thisMap,2));thisMap;true(N,size(thisMap,2))];
    end
    clear N
    
    img = py.numpy.reshape(int8(thisMap(:).'),int32(size(thisMap)),'F');
    imgRot = py.scipy.ndimage.rotate(img,...
        -atan2d(stateDiff(2),stateDiff(1)),pyargs('reshape',false));
    mapRot = reshape(logical(py.array.array('d',...
        py.numpy.nditer(imgRot))),fliplr(size(thisMap))).';

%         figure
%         imagesc(thisMap)
%         axis equal
%   
%         figure
%         imagesc(mapRot)
%         axis equal

    % Source: https://www.hindawi.com/journals/ijap/2018/8737594/

    J0ce = (J0(1)+J0(end)) ./ 2;
    I0ce = (I0(1)+I0(end)) ./ 2;

    [Jinterins,Iinterins] = map.R.worldToIntrinsic(...
        [stateStar(1) stateFini(1)],[stateStar(2) stateFini(2)]);
    XY0 = bsxfun(@plus,rotatiM*[Jinterins([1,end])-J0ce
        Iinterins([1,end])-I0ce],...
        fliplr(size(mapRot)).'./2).';
    clear Jinterins Iinterins
        
    [Jinterins,Iinterins] = map.R.worldToIntrinsic(...
        thisState(:,1),thisState(:,2));
    XY = bsxfun(@plus,rotatiM*[Jinterins.'-J0ce;Iinterins.'-I0ce],...
        fliplr(size(mapRot)).'./2).';
    clear Jinterins Iinterins

%         figure
%         imagesc(mapRot)
%         axis equal
%         hold on
%         plot(XY0(:,1),XY0(:,2))
%         plot(XY(:,1),XY(:,2))

    d(n) = XY0(2,1) - XY0(1,1);

    m_ = ceil(XY0(1,1)) : floor(XY(end,1));
    di = m_ - XY0(1,1);  % distance
    gi = zeros(1,length(m_));  % height including clutter
    d1 = zeros(1,length(m_));
    d2 = zeros(1,length(m_));
    cleara = zeros(1,length(m_));
    vq = interp1(XY(:,1),XY(:,2),m_);
    for m = 1 : length(m_)
        [Min,minI] = min([vq(m),XY0(1,2)]);
        Max = max(vq(m),XY0(1,2));
        y = linspace(Min,Max,201);
        gi(m) = sum(mapRot(round(y(2:2:end)),m_(m))) ./ 100 .* (Max-Min);
        d1(m) = sqrt(di(m).^2+gi(m).^2);
        d2(m) = sqrt((d(n)-di(m)).^2+gi(m).^2);
        cleara(m) = 0.78 ./ sqrt(2./lambda.*(1./d1(m)+1./d2(m)));
%         if minI == 2
%             if sum(mapRot(round(ceil(Max) + ...
%                     cleara(m).*linspace(-0.4999,0.5)),m_(m)),1)<50 || ...
%                     sum(mapRot(round(Max+cleara(m).*linspace(-0.5,0.5)),...
%                     m_(m)),1)<50
%             else
%                 disp('There is not enough clearance')
%                 beep
%                 keyboard
%             end
%         else
%             dsgvfr
%         end
    end
    clear m vq tmp minI

%         figure
%         plot(di,gi)
    
    if all(gi==0)
        vmax = -0.78 - 0.0001;
    else
        [vmax,vmaxI] = max(gi(2:end-1) .* ...
            sqrt(2./lambda.*(1./d1(2:end-1)+1./d2(2:end-1))));
        if gi(vmaxI+1) == 0
            beep
            disp('Not sure this should occur')
            keyboard
        end
    end
    
    % Calculate the losses, J
    if vmax >= -0.78
        J = 6.9 + 20.*log10(sqrt((vmax-0.1).^2+1)+vmax-0.1);
    else
        J = 0;
    end
    Luc(n) = J;
    clear J
end

% Basic transmission loss due to free-space propagation
Lbfs = 92.45 + 20.*log10(fGHz) + 20.*log10(sum(d./1000,1));

% Median transmission loss associated with diffraction
Lbd50 = Lbfs + sum(Luc,1);
