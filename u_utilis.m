function [satis,SINR_DL,assoc,utilis] = u_utilis(satis,SINR_DL,assoc,...
    utilis,isCov,IND3,visib,isCellN,LocisCell,IND2,BHcou,operatoN,...
    isShari,OperatorI,LocOperato,LocisCellIND3,capa,fn,Sim,trafClI,...
    Mu_DL,maxDist_m,servCou,sitW,v,pwrAmpClasBS,thisPi,thisHour,isIN2000)

% Simplications for capacity
%     N = length(LocisCell(IND3,:));
%     IND3n = length(IND3);
SE_DL = visib.SE_DL(IND3,1:servCou);

% If coverage check OK, then check if maxDist_m is
% restricting the real coverage range. This check can only
% be done when there is a number of antenna sites
% considered.
% if isCellN > 1
IND = (visib.distm(IND3,1)./maxDist_m(LocisCell(IND3,1))).^2 > 0.95;
% k_ = find(IND&strcmp(pwrAmpClasBS,'macrocell'));
k_ = find(IND);
if any(IND,1)
    if Sim.yea.thisYea < 2020
%                 disp('maxDist_m may be restricting the real coverage range')
    else
        for k = k_.'
            % Prepare data to be written to file
            header ={''}; % There is no need anymore for a header (we have already covered this earlier)
            % header = {'pixel_number', 'distance', 'Year', 'Cell', 'mobility_class', 'UE_pwr_amp_class', 'situation', 'Frequency'};
            data = {num2str(visib.studyAind(IND3(k))),...
                num2str(visib.distm(IND3(k),1)),...
                num2str(Sim.yea.thisYea),...
                num2str(visib.ii{IND3(k)}(1,1)),...
                Sim.dem.mobiClasses{visib.bund2(IND3(k))},...
                visib.pwrAmpClasMS{IND3(k)},visib.situMS{IND3(k)},...
                Sim.linBud.freq{fn}};
            % Write to file
            w_to_txt(Sim,header,data)
        end

%                 [X,Y] = studyA.R.intrinsicToWorld(...
%                     thisDem(visib.thisDem_IND(IND3)).J,...
%                     thisDem(visib.thisDem_IND(IND3)).I);
%                 [lat,lon] = minvtran(Sim.mstruct,X,Y);
%                 k = kml('my kml file');
%                 k.plot3([Cell(LocisCell(IND3)).lon lon],...
%                     [Cell(LocisCell(IND3)).lat lat],...
%                     [Cell(LocisCell(IND3)).heiAgl Sim.propag.UE{Un,2}],...
%                     'altitudeMode','relativeToGround',...
%                     'lineWidth',5,'lineColor','FF00FF00',...
%                     'name','wanted');
%                 for n = 1 : length(IND4)
%                     k.plot3([Cell(LocisCell(IND4(n))).lon lon],...
%                         [Cell(LocisCell(IND4(n))).lat lat],...
%                         [Cell(LocisCell(IND4(n))).heiAgl Sim.propag.UE{Un,2}],...
%                         'altitudeMode','relativeToGround',...
%                         'lineWidth',5,'lineColor','FF00FF00',...
%                         'name',['Interferer' ...
%                         num2str(LocisCell(IND4(n)))]);
%                 end
%     %                         k.poly3([t(i) t(i+1) t(i+1) t(i) t(i)],[0 0 1 1 0]+5, [1 1 3 3 1]*1e6,'polyColor','BB00FF00','extrude',false,'lineWidth',0)
%                 k.run;
%                 clear n
% 
%                 IND1 = Sim.propag.pix{Un}(:,3:4)*[1;1j] == ...
%                     thisDem(visib.thisDem_IND(IND3)).I + ...
%                     1j .* thisDem(visib.thisDem_IND(IND3)).J;
%                 col_idx_clut_h_extreme = 7;  % 'for i=1 and n in (1c)'
%                 col_idx_clut_h_intermediate = 6;  % 'for i=2 to n-1 in (1c)'
%                 get_isLOS(Cell(LocisCell(IND3)),...
%                     Sim.propag.pix{Un}(IND1,:),...
%                     Sim.propag.UE{Un,2},clu,studyA,ter,...
%                     col_idx_clut_h_extreme,...
%                     col_idx_clut_h_intermediate,Sim,...
%                     Sim.propag.UE{Un,3},@c_prob_LOS_UMi_5GCM)
    end
end
% end

% [Title]
% A is an (BHcou) x p matrix, where p is the number of paths that are
% vectorised for capacity assessment, holding the amount of demand of
% thisDem.v.
A = reshape(v(shiftdim(repmat(IND2,[1,1,BHcou]),2)),BHcou,length(IND3));
% @HO:
% Why do we serve the demand for this pixel by looking at one demand
% category at a time? 

if ~all(A(:)==0)
    % For each tenant that can be served by the InP of
    % the examined link visib(IND3), calculate the amount of
    % resources needed at the InP to serve the tenant's demand. The
    % dimension is Sim.shari.is(InP,tenant). The dimensions
    % of BW are [BHcou hours, tenants, sites, paths]; for example,
    % BW(1,2,3,4) is the amount of BW needed for the 4th path at 0.30am to
    % serve tenant EE on the 3rd site.
    BW = zeros(BHcou,operatoN,servCou,length(IND3));
    if isShari
        % Sharing
        % Find the index of the InP that can serve the examined
        % link visib(IND3). Not sur why that happens, example is
        % needed.
        n_ = find(any(Sim.shari.is(OperatorI(LocisCell(IND3,:)),:),1));
    else
        % No sharing
        % The index of the InP is simply the owner of the cell with
        % the highest received signal strength
        n_ = OperatorI(LocisCell(IND3));
        n_ = unique(n_);
        assert(length(n_)==1,'If failure, then rethink how to do sharing!')
    end
    for n = n_  % For each tenant
        % Find the columns of thisDem.v that are relevant to the
        % visib(IND3) link, i.e. have the examined pwrAmpClasMS,
        % situMS, mobility class, and tenant.
        % It could be modelled as another use of c_IND2b, but it is
        % provided as a single line here. It is different use of
        % c_IND2b because there is no implication from netowrk
        % sharing.
        IND2b = bsxfun(@and,IND2,LocOperato==n);

        IND2bS = sum(IND2b,1);
        if all(IND2bS==0)
            continue
        end
        IND4 = IND2bS > 0;

        % Pool the demand
        if any(IND2bS>1)
            beep
            disp('Do something along the lines of the commented code!')
            keyboard
        end
%         if IND2bS > 1
%             A = sum(A,2);
%         end

        % Calculate the resources to satisfy the tenant
        for m = 1 : servCou
            BW(:,n,m,IND4) = bsxfun(@rdivide,A(:,IND4),SE_DL(IND4,m).');
        end
    end

%             figure
%             %
%             subplot(2,1,1)
%             bar([0.5:23.5],sum(sum(thisDem(thisDem_IND).(fam).(trafCl).(...
%                 pwrAmpClasMS).v(:,thisBund2,:),2),3).*1e6)
%             xlabel('Hours of the day')
%             ylabel('Traffic volume  (bit/s)')
%             grid on
%             set(gca,'xlim',[0 BHcou])
%             %
%             subplot(2,1,2)
%             bar([0.5:23.5],BW.*1e6)
%             xlabel('Hours of the day')
%             ylabel('BW required  (Hz)')
%             grid on
%             set(gca,'xlim',[0 BHcou])

    % Find if there is capacity to serve MTC demand
%             BWava = inf(BHcou,length(visib_IND3_Celli));
%             BWavaCol = OperatorI(visib_IND3_Celli);
%             isCapa = squeeze(sum(BW,2)) <= BWava(:,BWavaCol);

    % Find if there is spare capacity in DL to serve the
    % demand. isCapa has size [BHcou,InP]. isCapa is false if
    % isCov is false.
    if servCou == 1
        if length(capa) == 1
            tmp = squeeze(sum(BW,2)) ./ capa;
        else
            tmp = squeeze(sum(BW,2)) ./ repmat(capa(LocisCellIND3),BHcou,1);
        end
    else
        tmp = squeeze(sum(BW,2)) ./ ...
            shiftdim(repmat(capa(LocisCellIND3).',[1,1,BHcou]),2);
    end
    if servCou == 1
        B = squeeze(sum(sum(utilis(:,:,LocisCellIND3,:),4),2));
        switch Sim.bordCoor
          case 'coordinated'
            isCapa0 = tmp <= bsxfun(@minus,(1-isIN2000.'.*0.5) .* ...
                Sim.linBud.PRButilisCapa(fn),B);
          case {'ignore','non_coordinated'}
            isCapa0 = tmp <= (Sim.linBud.PRButilisCapa(fn)-B);
          otherwise
            error('Undefined')
        end
        
        % Record 
        if any(any(B==0&~isCapa0,2),1)
            beep
            disp('Pixel size may too big. Check log_demCapa.txt for more info.')
            keyboard
            if Sim.isCoor
                disp('Log not available. Code this in u_utilis!')
            else
                [row,col] = find(B==0&~isCapa0);
                for t = 1 : size(row,1)
                    fprintf(Sim.path.logDemCapaFileID,...
                        '%s,%d,%f,%f,%f\r\n',Sim.linBud.freq{fn},...
                        visib.studyAind(IND3(col(t))),...
                        visib.distm(IND3(col(t))),...
                        tmp(row(t),col(t)).*capa(LocisCellIND3(col(t))),...
                        Sim.linBud.PRButilisCapa(fn) .* ...
                        capa(LocisCellIND3(col(t))));
                end
            end
            clear t row col
        end
    else
        beep
        disp('Think isIN2000!')
        keyboard
        isCapa0 = squeeze(all(tmp <= (Sim.linBud.PRButilisCapa(fn) - ...
            reshape(sum(sum(utilis(:,:,LocisCellIND3.',:),4),2),...
            [BHcou,size(LocisCellIND3.')])),2));
    end
    m_ = find(any(isCov&isCapa0,1));
    famI = visib.famI(IND3);
    for m = m_
        % []
        IND5 = thisPi == m;
        
        % []
        if servCou == 1
            switch Sim.bordCoor
              case 'coordinated'
                isCapa = tmp(:,m) <= ((1-isIN2000(m).'.*0.5) .* ...
                    Sim.linBud.PRButilisCapa(fn) - ...
                    sum(sum(utilis(:,:,LocisCellIND3(m),:),4),2));
              case {'ignore','non_coordinated'}
                isCapa = tmp(:,m) <= (Sim.linBud.PRButilisCapa(fn) - ...
                    sum(sum(utilis(:,:,LocisCellIND3(m),:),4),2));
              otherwise
                error('Undefined')
            end
        else
            beep
            disp('Think isIN2000!')
            keyboard
            isCapa = all(tmp(:,:,m) <= (Sim.linBud.PRButilisCapa(fn) - ...
                squeeze(sum(sum(utilis(:,:,LocisCellIND3(m,:),:),4),2))),2);
        end
%         if operatoN == 1
%             beep
%             disp('Check if a simplication can be done as above!')
%             keyboard
%             tmp = BW ./ repmat(capa(LocisCellIND3),BHcou,1);
%             isCapa = tmp <= (Sim.linBud.PRButilisCapa(fn) - ...
%                 squeeze(sum(sum(utilis(:,:,LocisCellIND3,:),4),2)));
%         end
        
        % Bundle hours that have common InP availability
        % Using binary conversion to a decimal identifier because
        % there are only two states: isCapa and ~isCapa.
%             if all(isCapa,1)
%                 C = isCapa(1);
%                 ic = ones(BHcou,1);
%             else
%                 [C,~,ic] = unique(isCapa);
%             end

        % For each bundle, update thisDem satisfaction and
        % association, utilis which is a simplified matrix of
        % Cell.utilis, and the isServ logical vector
%         for n = 1 : length(C)
%             if C(n) ~= 0
        % Proceed if there is any demand to be associated
        % with antenna sites
        if any(any(any(BW(thisHour(IND5),:,:,m) .* ...
                repmat(isCapa(thisHour(IND5)),[1,operatoN,servCou]),3),2),1)
            % []
            IND5_f = find(IND5);
            isCapa_f = find(isCapa);
        
            % Calculate the percentage of resoruces needed to
            % serve the demand. The dimensions are [BHcou hours, 3
            % tenants, 2 sites]
            if servCou == 1
                tmp2 = BW(thisHour(IND5),:,:,m) ./ capa(LocisCellIND3(m));
            else
                if nnz(IND5) == 1
                    tmp2 = BW(thisHour(IND5),:,:,m) ./ shiftdim(repmat(capa(...
                        LocisCellIND3(m,:)),[operatoN 1]),-1);
                else
                    tmp2 = BW(thisHour(IND5),:,:,m) ./ shiftdim(repmat(capa(...
                        LocisCellIND3(m,:)),[operatoN 1 nnz(IND5)]),2);
                end
            end

            % thisHour is a logical vector (BHcou x 1) that is true for the hours
            % considered within this function run. The capacity needs to be ensured
            % only within these hours.
            % It is reminded from above that isCapa (BHcou x InP) is true if there is
            % spare capacity in DL to serve the demand.
            % isCapa2 is a logaical vector (BHcou x 1) that is true for the hours
            % considered and if there is any InP that has capacity.
            if servCou == 1
                isCapa2 = any(isCapa(thisHour(IND5))&sum(tmp2,2)>0,2);
            else
                if size(tmp2,1) == 1
                    isCapa2 = isCapa(thisHour(IND5)) & ...
                        any(squeeze(sum(tmp2,2)>0).',2);
                else
                    isCapa2 = isCapa(thisHour(IND5)) & ...
                        any(squeeze(sum(tmp2,2)>0),2);
                end
            end
            
            IND6 = IND5_f(isCapa2);
            
            % Set satisfaction property to true, for the hours of the day that
            % capacity is adequate, and for the dem.v columns in question. If the
            % code is here, then this means that the site in question can serve the
            % demand from all service providers it tries to serve. The dimension is
            % Sim.shari.is(InP,tenant).
            satis(IND6) = true;
            
            % Update SINR_DL
            SINR_DL(IND6) = Mu_DL(thisHour(IND6),m);
            
            % Update utilis, which is a simplification of Cell.utilis. Update
            % thisDem.assoc with the serving site(s).
            if servCou == 1
                % One site serves the considered demand
                utilis(thisHour(IND6),:,LocisCell(IND3(m),:),famI(m)) = ...
                    utilis(thisHour(IND6),:,LocisCell(IND3(m),:),famI(m)) + ...
                    tmp2(isCapa(thisHour(IND5)),:);
                assoc(IND6) = {sitW(m)};
            else
                % More than one sites are needed to serve this pixel, thus capacity
                % is reduced from both sites, and the association is done to
                % multiple sites.
%                 if Sim.dem.trafVol.(Sim.dem.famUseCase_{famI(m)}...
%                         ).CEtput.isTxMultiCopiesDataToAllServ(trafClI)
%                     % Resources from all servers that receive the signal are used
%                     beep
%                     disp('Amend as below!')
%                     keyboard
%             %         for n = 1 : length(visibI)
%             %             utilis(isCapa(:,n),:,LocisCell(visibI(n)),famI) = ...
%             %                 utilis(isCapa(:,n),:,LocisCell(visibI(n)),famI) + ...
%             %                 BW(isCapa(:,n),:,n);
%             %             IND2b = c_IND2b(IND2,Sim,OperatorI,demTranslati7,isShari,...
%             %                 LocisCell(visibI),shariOperatoDemTranslati7);
%             %         end
%             %         thisDem.assoc(isCapa2,IND2b) = {sitW};
%                 else
                % Resources from best server are used
                utilis(thisHour(IND6),:,LocisCell(IND3(m),1),famI(m)) = ...
                    utilis(thisHour(IND6),:,LocisCell(IND3(m),1),famI(m)) + ...
                    tmp2(isCapa(thisHour(IND5)),:,1);
                assoc(IND6) = {sitW(m,1)};
%                 end
            end            
            
            % update thisDem satisfaction and association,
            % utilis which is a simplified matrix of
            % Cell.utilis, and the isServ logical vector
%             [satis(IND5),SINR_DL(IND5),assoc(IND5),...
%                 utilis(thisHour(IND5),:,:,:)] = ...
%                 upd_thisDem_and_Cell(satis(IND5),SINR_DL(IND5),...
%                 assoc(IND5),utilis(thisHour(IND5),:,:,:),...
%                 isCapa(thisHour(IND5)),trafClI,tmp2,sitW(m),Sim,...
%                 LocisCell(IND3(m),:),servCou,...
%                 visib.famI(IND3(m)));
        else
%                         % If there is no demand that needs to be served
%                         % then isServ (BHcou x 1) is true.
%                         beep
%                         disp('Check this. isCapa is only a vector!')
%                         keyboard
%                         isServ = any(isCapa & ...
%                             repmat(ic==n,1,size(isCapa,2)),2);
        end
%             end
%         end
    end
end
end
