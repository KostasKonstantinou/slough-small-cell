function poin = read_addresses(Sim,studyA)
%% POIN class constructor
% POIN is the class constructor of POIN objects, poin. (At least that is
% the intent). Currently this is a function and the output is an
% intermediate to the poin objects, poin.
% 
% The input studyA is a study area object and must contain at least the
% following fields:
% buf.X,buf.Y - See study area class
% 
% The input structure Sim must contain at least the following fields:
% path.ProgramData - Path to ProgramData
% path.add - Path to address database
% path.dr - Path to dropbox
% mstruct - Matlab native map projection structure
% 
% * v0.1 Kostas Konstantinou May 2016

% Create address demand points
if exist(fullfile(Sim.path.ProgramData,'pop2.mat'),'file')
    load(fullfile(Sim.path.ProgramData,'pop2.mat'),'poin')
else
    %% Based on Census, e.g. UK
    if exist(fullfile(Sim.path.ProgramData,'add.mat'),'file')
        S = load(fullfile(Sim.path.ProgramData,'add.mat'),'add');
        add = S.add;
        clear S
    else
        if exist(fullfile(Sim.path.ProgramData,'add UK.mat'),'file')
            S = load(fullfile(Sim.path.ProgramData,'add UK.mat'),'add');
            add = S.add;
            clear S
        else
            % Read all addresses, then remove NI, Channel islands and NI. This is
            % faster than using the selector functionality of shaperead.
            % add = shaperead(fullfile([dropPath,'GEOPLAN\09_13\Data'],...
            %     'Unc_09_13a_PLUS.shp'),...
            %     'Attributes',{'X_COORD','Y_COORD','COUNTRY','DELR'},'Selector',...
            %     {@(x) strcmp(x,'E92000001') || strcmp(x,'S92000003') || ...
            %     strcmp(x,'W92000004'),'COUNTRY'});
            add = shaperead(fullfile(Sim.dropPath,'GEOPLAN','09_13','Data',...
                'Unc_09_13a_PLUS.shp'),...
                'Attributes',{'X_COORD','Y_COORD','COUNTRY','DELR','DELN','DELL',...
                'COM_UNIT'});
            % {'E92000001' 'England'
            %     'L93000001' 'Channel Islands'
            %     'M83000001' 'Isle of Man'
            %     'N92000002' 'NI'
            %     'S92000003' 'Scotland'
            %     'W92000004' 'Wales'}
            % DELR
            % This is a count of the number of Residential delivery counts associated with the Postcode.
            % DELN
            % This is a count of the number of Non residential delivery counts associated with the Postcode i.e. Businesses receiving less than 50 items of mail a day.
            % DELL
            % This is a count of the number of Large user delivery counts associated with the Postcode i.e. Businesses receiving more than 50 items of mail a day.
            add(strcmp('L93000001',{add.COUNTRY}) | ...
                strcmp('M83000001',{add.COUNTRY}) | ...
                strcmp('N92000002',{add.COUNTRY})) = [];

            % Remove addresses without residential delivery addresses
            add([add.DELR]==0&[add.DELN]==0&[add.DELL]==0) = [];

            % Read OA of England and Wales, extent of the realm to capture addresses on
            % sea
            OA_EW = shaperead(fullfile(Sim.path.on,'05 RW DB - General',...
                'United Kingdom','Population','Census 2011',...
                'England and Wales',...
                'Output_areas_(E+W)_2011_Boundaries_(Full_Extent)_V2',...
                'OA_2011_EW_BFE_V2.shp'));

            % Read OA of Scotland, extent of the realm to capture addresses on
            % sea
            OA_S = shaperead(fullfile(Sim.path.on,'05 RW DB - General',...
                'United Kingdom','Population','Census 2011','Scotland',...
                'OutputArea2011_EoR','OutputArea2011_EoR.shp'));

            if exist(fullfile(Sim.path.ProgramData,'IN.mat'),'file')
                S = load(fullfile(Sim.path.ProgramData,'IN.mat'),'IN');
                IN = S.IN;
                clear S
            else
                % Initialise matrix
                IN = sparse([],[],false(0,0),length(add),length(OA_EW)+length(OA_S),...
                    length(add));

                % Assign addresses to OA, England and Wales
                IND = find(strcmp('E92000001',{add.COUNTRY}) | ...
                    strcmp('W92000004',{add.COUNTRY}));
                X = [add(IND).X];
                Y = [add(IND).Y];
                h = waitbar(0,'Assigning add to OA, in England and Wales. Please wait...');
                for n = 1 : length(OA_EW)
                    ind = OA_EW(n).BoundingBox(1,1)<=X & X<=OA_EW(n).BoundingBox(2,1) & ...
                        OA_EW(n).BoundingBox(1,2)<=Y & Y<=OA_EW(n).BoundingBox(2,2);

                    if any(ind)
                        IN(IND(ind),n) = inpolygon([add(IND(ind)).X],[add(IND(ind)).Y],...
                            OA_EW(n).X,OA_EW(n).Y);
                    end

                    if rem(n,100) == 0
                        waitbar(n./length(OA_EW),h);
                    end
                end
                delete(h)
                clear X Y ind

                % Assign addresses to OA, Scotland
                IND = find(strcmp('S92000003',{add.COUNTRY}));
                X = [add(IND).X];
                Y = [add(IND).Y];
                h = waitbar(0,'Assigning add to OA, in Scotland. Please wait...');
                for n = 1 : length(OA_S)
                    ind = OA_S(n).BoundingBox(1,1)<=X & X<=OA_S(n).BoundingBox(2,1) & ...
                        OA_S(n).BoundingBox(1,2)<=Y & Y<=OA_S(n).BoundingBox(2,2);

                    if any(ind)
                        IN(IND(ind),length(OA_EW)+n) =...
                            inpolygon([add(IND(ind)).X],[add(IND(ind)).Y],...
                            OA_S(n).X,OA_S(n).Y);
                    end

                    if rem(n,100) == 0
                        waitbar(n./length(OA_S),h);
                    end
                end
                delete(h)
                clear X Y ind

                % Treat outliers, no OA
            %     ind = find(~any(IN,2));
                %
            %     find(strcmp('E00139360',{OA_EW.OA11CD}) & ...
            %         strcmp('E06000048',{OA_EW.LAD11CD}))
            %     figure
            %     plot(OA_EW(135603).X,OA_EW(135603).Y)
            %     hold on
            %     plot(add(859627).X_COORD,add(859627).Y_COORD,'rx')
            %     axis equal
            %
            %     find(strcmp('S00120198',{OA_S.code}))
            %     figure
            %     plot(OA_S(31717).X,OA_S(31717).Y)
            %     hold on
            %     plot(add(975395).X_COORD,add(975395).Y_COORD,'rx')
            %     axis equal
            %
                IN(859627,135603) = true;
                IN(975395,length(OA_EW)+31717) = true;

                save('IN','IN')
            end

            % Treat outliers, multiple OA
            %     ind = find(sum(IN,2)>1);

            % Read population in England and Wales
            OA11CD = {OA_EW.OA11CD};
            multiWaitbar('Population files analysed in England and Wales',0);
            for n = 1 : 10
                [~,~,raw] = xlsread(fullfile(Sim.path.on,...
                    '05 RW DB - General','United Kingdom','Population',...
                    'Census 2011','England and Wales','Population density',...
                    ['Pop dns ' num2str(n) '.csv']));

                multiWaitbar('Output areas within file analysed',0);
                for m = 2 : size(raw,1)
                    ind = strcmp(raw{m,2},OA11CD);

                    OA_EW(ind).pop = raw{m,5};

                    if rem(m,1000) == 0
                        multiWaitbar('Output areas within file analysed',m./size(raw,1));
                    end
                end
                multiWaitbar('Output areas within file analysed','Close');

                multiWaitbar('Population files analysed in England and Wales',n./10);
            end
            multiWaitbar('Population files analysed in England and Wales','Close');
            clear raw OA11CD m n

            % Distribute population into addresses, England and Wales
            h = waitbar(0,'Distributing pop, in England and Wales. Please wait...');
            for n = 1 : length(OA_EW)
                ind = find(IN(:,n));
                v = OA_EW(n).pop .* [add(ind).DELR] ./ sum([add(ind).DELR]);
                for m = 1 : nnz(IN(:,n))
                    add(ind(m)).pop = v(m);
                end

                if rem(n,1000) == 0
                    waitbar(n./length(OA_EW),h);
                end
            end
            delete(h)
            clear ind h m v

            % Distribute population into addresses, Scotland
            h = waitbar(0,'Distributing pop, in Scotland. Please wait...');
            for n = 1 : length(OA_S)
                ind = find(IN(:,length(OA_EW)+n));
                v = OA_S(n).Popcount .* [add(ind).DELR] ./ sum([add(ind).DELR]);
                for m = 1 : nnz(IN(:,length(OA_EW)+n))
                    add(ind(m)).pop = v(m);
                end

                if rem(n,1000) == 0
                    waitbar(n./length(OA_S),h);
                end
            end
            delete(h)
            clear ind h m v

            clear n IND OA_EW OA_S IN

            % Update addresses with their altitude ASL
            % tmp = give_terrain_height(add,ter);
            % for n = 1 : length(add)
            %     add(n).alt = tmp(n);
            % end
            % clear tmp

            %     add = rmfield(add,'Geometry');
            %     for n = 1 : length(add)
            %         add(n).Geometry = 'Point';
            %     end

            [add(:).X] = deal(add(:).X_COORD);
            add = rmfield(add,'X_COORD');

            [add(:).Y] = deal(add(:).Y_COORD);
            add = rmfield(add,'Y_COORD');

            for n = 1 : length(add)
                add(n).COUNTRY = add(n).COUNTRY(1);
            end

            % add = rmfield(add,'DELR');

            % addpath([Sim.dropPath 'MATLAB\More accurate coordinate conversion'])
            % load ostn
            % h = waitbar(0,'Calculating the latitude of addresses. Please wait...');
            % for n = 1 : length(add)
            %     add(n).phi = easNor2latLon(add(n).X,add(n).Y,ostn);
            % 
            %     if rem(n,1000) == 0
            %         waitbar(n./length(add),h);
            %     end
            % end
            % delete(h)

            %     % Header info
            % %     nrows = 401;
            % %     ncols = 401;
            %     cellsize = 50;
            %     for n = 1 : length(add)
            %         fileName = cell(1,1);
            %         OSname = ['xxyy'
            %             'xxyy'
            %             'xxyy'
            %             'xxyy'];
            % 
            %         % Find the pixel coordinates that include the (x,y) location
            %         X = (floor((add(n).X-cellsize./2)./cellsize)+[0,1]).*cellsize;
            %         Y = (floor((add(n).Y-cellsize./2)./cellsize)+[1,0]).*cellsize;
            %         [X_,Y_] = meshgrid(X,Y);
            % 
            %         % Find the files that contain the terrain data
            %         mn = fix(rem([X_(:) Y_(:)],100000)./10000./2).*2;
            %         for m = 1 : numel(X_)
            %             OSname(m,:) = [lower(EN2grid(X_(m),Y_(m))) ...
            %                 sprintf('%02d',mn(m,1).*10+mn(m,2))];
            %         end
            % 
            %         if strcmp(OSname(1,:),OSname(2,:)) && ...
            %                 strcmp(OSname(1,:),OSname(3,:)) && ...
            %                 strcmp(OSname(1,:),OSname(4,:))
            %             fileName = OSname(1,:);
            %         else
            %             fileName = cell(size(OSname,1),1);
            %             for m = 1 : size(OSname,1)
            %                 fileName{m} = OSname(m,:);
            %             end
            %             fileName = unique(fileName,'stable');
            %         end
            %         
            %         add(n).fileName = fileName;
            %     end

    %         add2.Geometry: 'Point'
            add2.X = [add.X].';
            add2.Y = [add.Y].';
    %         add2.X_COORD: 394251
    %         add2.Y_COORD: 806376
            add2.COUNTRY = {add.COUNTRY}.';
            add2.DELR = [add.DELR].';
            add2.DELN = [add.DELN].';
            add2.DELL = [add.DELL].';
            add2.COM_UNIT = {add.COM_UNIT}.';
            add2.pop = zeros(length(add2.X),1);
            IND = arrayfun(@(x) isempty(x.pop),add);
            add2.pop(~IND) = [add(~IND).pop].';

            clear add
            add = add2;
            clear add2

            save(fullfile(Sim.path.ProgramData,'add UK.mat'),'add')
        end
        
        IND = ~inpolygon(add.X,add.Y,studyA.buf.X,studyA.buf.Y);
        add.X(IND) = [];
        add.Y(IND) = [];
        add.COUNTRY(IND) = [];
        add.DELR(IND) = [];
        add.DELN(IND) = [];
        add.DELL(IND) = [];
        add.COM_UNIT(IND) = [];
        add.pop(IND) = [];
        
%         figure
%         scatter(add.X,add.Y,6,add.pop,'s','filled')
%         axis equal
        
        save(fullfile(Sim.path.ProgramData,'add.mat'),'add')
    end
    
    %% Based on Census
    poin = objOfArr2arraOfObj(add);
    clear add

%     figure
%     hold on
%     scatter([poin.X].',[poin.Y].',6,[poin.pop].','filled','s')
%     plot([poin([poin.pop]==0).X].',[poin([poin.pop]==0).Y].','o')
%     axis equal
%     grid on
    
    %% Based on OpenStreetMap, e.g. Hamburg
    % Read the address database.
    % In Hamburg we read the address locations:
%     poin = shaperead(Sim.path.add,'Attributes',{});
    % In UNOPS project the address locations will be assumed to be
    % everywhere within the built up areas.
%     [I,J] = find(studyA.INBuiltuparea);
%     [X,Y] = studyA.R.intrinsicToWorld(J,I);
%     poin = struct('X',num2cell(X),'Y',num2cell(Y));
    
    % Below is from OpenStreetMap, which was not too successful as visual
    % result for Palestine.
%     % Read the population
%     S = shaperead(fullfile(Sim.path.on,'05 RW DB - General','Palestine',...
%         'israel-and-palestine-latest-free.shp','gis_osm_places_free_1'));
%     [x,y] = mfwdtran(Sim.mstruct,[S.Y],[S.X]);
%     x = num2cell(x);
%     y = num2cell(y);
%     [S.X] = deal(x{:});
%     [S.Y] = deal(y{:});
%     clear x y
%     
%     % Remove entries without population, or fclass='city' because the
%     % city population seems to be double counted. The population of Hamburg
%     % Sate should be around 1,788,000 according to:
%     % https://en.wikipedia.org/wiki/States_of_Germany
%     S([S.population]==0) = [];
% %     S(strcmp({S.fclass},'city')) = [];
%     
%     % Remove entries outside the country
%     S(~inpolygon([S.X],[S.Y],studyA.X,studyA.Y)) = [];
% 
%     % For each city/hamlet/island/locality/suburb/town/village, distribute
%     % the population to the addresses
%     BoundingBox = [min([[poin.X] [S.X]]) min([[poin.Y] [S.Y]])
%         max([[poin.X] [S.X]]) max([[poin.Y] [S.Y]])] + [-1 -1; 1 1];
%     [C,V] = voronoi_constra([[S.X];[S.Y]].',...
%         [BoundingBox([1,1,2,2,1],1) BoundingBox([1,2,2,1,1],2)]);
%     for n = 1 : length(C)
%         xv = V(C{n},1);
%         yv = V(C{n},2);
%         IND = inpolygon([poin.X],[poin.Y],xv,yv);
%         pop = num2cell(diff(fix(linspace(0,S(inpolygon([S.X],[S.Y],...
%             xv,yv)).population,nnz(IND)+1))));
%         [poin(IND).pop] = deal(pop{:});
%     end
%   
%     % Remove points ouside the buffer
%     IN = inpolygon([poin.X],[poin.Y],studyA.buf.X,studyA.buf.Y);
%     poin(~IN) = [];
% 
%     assert(nnz(arrayfun(@(x) isempty(x.pop),poin))==0,...
%         'There are some points that have not been assigned population')
    
    %% Based on built-up polygons, e.g. West Bank
%     % Below is for assuming the population is as provided by OQ
%     % For each built up area, distribute
%     % the population to the addresses.
%     poin = [];
%     for n = 1 : size(studyA.INBuiltupareaPer,3)
%         [I,J] = find(studyA.INBuiltupareaPer(:,:,n)&studyA.INfoc);
%         [X,Y] = studyA.R.intrinsicToWorld(J,I);
%         poin = [poin; struct('X',num2cell(X),'Y',num2cell(Y),...
%             'pop',max(studyA.Builtuparea(n).Population./length(X),0.01))];
%     end
%     clear n I J X Y
% 
% %     figure
% %     hold on
% %     scatter([poin.X].',[poin.Y].',6,[poin.pop].','filled','s')
% %     plot([poin([poin.pop]==0).X].',[poin([poin.pop]==0).Y].','o')
% %     axis equal
% %     grid on
    
    %% Common
    save(fullfile(Sim.path.ProgramData,'pop.mat'),'poin')
    
    %% DELN and DELL replacement by Google Places
    % The business delivery address count was not found to be aligning with
    % expectations of where the outdoor demand would be.
    % Perhaps we need an additional demand source for Google Places, but
    % some businesses would be double counted. Thus, Kostas tries a
    % replacement of the business addresses by Google Places data.
    
    % Read places from Google
    S = g_populartimes(studyA,Sim);
    
    % Convert the polygon of the study area to cell format
    [Xcells,Ycells] = polysplit(studyA.buf.X,studyA.buf.Y);

    % Find which entries are within the polygon
    if isfield(Sim.mstruct,'ostn')
        [SX,SY] = latLon2easNor([S.lat].',[S.lon].',[],Sim.mstruct.ostn);
        SX = SX.';
        SY = SY.';
    elseif isfield(Sim.mstruct,'mapprojection')
        [SX,SY] = mfwdtran(Sim.mstruct,[S.Y],[S.X]);
    else
        error('Code this!')
    end
    IN = false(size(SX));
    for m = 1 : length(Xcells)
        if ispolycw(Xcells{m},Ycells{m})
            IN(inpoly([SX.' SY.'],[Xcells{m} Ycells{m}])) = true;
        else
            IN(inpoly([SX.' SY.'],[Xcells{m} Ycells{m}])) = false;
        end
    end

    % Remove entries outside the study area
    S(~IN) = [];
    SX(~IN) = [];
    SY(~IN) = [];
    IN(~IN) = [];
    
    % Write kml
    k = kml('OutpM\Places');
    for n = 1 : length(S)
        k.point(S(n).lon,S(n).lat,1,'iconURL','wht-stars',...
            'iconScale',1,'name',S(n).type);
    end
    k.save;
    
    % Delete entries that do not have domestic addresses
    IND = poin.DELR == 0;
    for n = fieldnames(poin).'
        poin.(n{:})(IND) = [];
    end
    
    % Assign DELN and DELL to 0
    poin.DELN(:) = 0;
    poin.DELL(:) = 0;
    
    % Append data from Google Places
    poin.X(end+(1:length(SX))) = SX;
    poin.Y(end+(1:length(SX))) = SY;
    poin.COUNTRY(end+(1:length(SX))) = {''};
    poin.DELR(end+(1:length(SX))) = 0;
    poin.DELN(end+(1:length(SX))) = 1;
    poin.DELL(end+(1:length(SX))) = 0;
    poin.COM_UNIT(end+(1:length(SX))) = {''};
    poin.pop(end+(1:length(SX))) = 0;
    
    save(fullfile(Sim.path.ProgramData,'pop2.mat'),'poin')
end
