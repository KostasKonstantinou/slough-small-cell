function [Cell,thisDem,interfIequi] = serv_dem(Cell,thisDem,Sim,studyA,...
    SINR2tput,clu,isPlot,isCell0,interfIequi,isXtunab,triPoi,...
    isDispLinBud,bund1)
%% [Title]
% [Description]
%
% * v0.1 KK 01Nov16 Created

if isempty(Cell)
    return
end

% Find cell chains of the operator in question that support
% the latency of the service
isExpi = Sim.yea.thisYea > [Cell.expiYea];
laten = [Cell.laten];
ch = [Cell.ch];

% Bundle calculations for UE
assert(nnz(Sim.dem.modeAlTrafClasAs)==1,...
    'This paragraph and c_visib will not work!')
trafClI = Sim.dem.modeAlTrafClasAs;
isCell = cell(length(Sim.dem.famUseCase_),1);
isCell2 = false(length(Sim.dem.famUseCase_),length(Cell));
MS = struct('IND',[],'pwrAmpClas',[],'situ',[]);
for famI = 1 : length(Sim.dem.famUseCase_)
    % For each mobility class, find the power amplifier class and situation
    % of the devices that are in this family of use cases
    fam = Sim.dem.famUseCase_{famI};
    CEtputI = find(Sim.dem.trafVol.(fam).CEtput.yea==Sim.yea.thisYea) + ...
        (0:length(Sim.dem.trafClas_)-1);
    bund2.(fam).C = struct(...
        'DL',num2cell(Sim.dem.trafVol.(fam).CEtput.DL(:,CEtputI(trafClI))),...
        'UL',num2cell(Sim.dem.trafVol.(fam).CEtput.UL(:,CEtputI(trafClI))),...
        'mobiClaVel',num2cell(Sim.dem.mobiClaVel),...
        'dop',num2cell(Sim.linBud.dop,2));
    [bund2.(fam).C.pwrAmpClasMSsituMS] = deal(Sim.dem.trafVol.(fam).bund2{:});
    Hash = arrayfun(@(x) DataHash(x),bund2.(fam).C,'UniformOutput',false);
    [~,bund2.(fam).ia,bund2.(fam).ic] = unique(Hash,'stable');
    
    isLatencyOK = laten <= Sim.dem.trafVol.(fam).CEtput.laten(trafClI);
    isCell2(famI,isLatencyOK&~isExpi&isCell0) = true;
    isCell{famI} = find(isCell2(famI,:));
    
    MS(famI).IND = find(any(Sim.dem.trafVol.(fam).devic.perDemSour>=0,1));
    MS(famI).pwrAmpClas = Sim.dem.trafVol.(fam).devic.pwrAmpClas(MS(famI).IND);
    MS(famI).situ = Sim.dem.trafVol.(fam).devic.situ(MS(famI).IND);
end
% isCell1 = find(any(isCell2,1));
clear Hash famI fam trafClI isLatencyOK

% For each family and traffic class, select the one with the most
% delay sensitive requirement
% for sn = 1 : size(Sim.dem.servOrd,1)
%     famI = Sim.dem.servOrd(sn,1);
%     fam = Sim.dem.famUseCase_{famI};
%     trafClI = Sim.dem.servOrd(sn,2);
%     trafCl = Sim.dem.trafClas_{trafClI};

% Sort bands with decreasing frequency
[~,f_] = sortrows([Sim.linBud.actuFreq Sim.linBud.dupMode ...
    cellfun(@(x) str2num(x(1)),Sim.linBud.tecInterf)],[-1 2 -3]);

% For each band with decreasing frequency, serve demand across
% site chains
for fn = f_.'
    % Calculate bund1.
    % It is computationaly intesive, and only used sparingly, hence if
    % it's ready to be used is tracket by isBund1rea.
    isBund1rea = false;  % is bund1 ready

    % Simplification
    fs1 = Sim.linBud.str{fn};

    % Calculate visib
    % Index of which of the [28] channels the site(s) support
    % []
    m_ = find(any(ch,2).' & ...
        (fn == cell2mat(Sim.linBud.chRaste(2,5:end))));
    switch length(m_)
        case {0,1}
            % No reordering is needed
        case 2
            beep
            disp('Not sure this is needed in the 13-band version')
            keyboard
            switch Cell(find(ch(m_(1),:),1)).pwrAmpClas
                case 'macrocell'
                    switch Cell(find(ch(m_(2),:),1)).pwrAmpClas
                        case 'macrocell'
                            beep
                            disp('Check why here!')
                            keyboard
                        case 'smaCel5W'
                            % Flip m_
                            m_ = fliplr(m_);
                        otherwise
                            error('Undefined')
                    end
                case 'smaCel5W'
                    % Desired order already in m_
                otherwise
                    error('Undefined')
            end
        case 3
            beep
            disp('Code this!')
            keyboard
%             Sim.linBud.chRaste([false(4,1)
%                 any(cell2mat(Sim.linBud.chRaste(5:end,4+m_))==1,2) & ...
%                 strcmp(fam,Sim.linBud.chRaste(5:end,4))],2)
        otherwise
            error('Undefined')
    end

    % []
    for m = m_
        % []
        isSupp = logical(sparse(length(Sim.dem.famUseCase_),length(Cell)));
        for famI = 1 : length(Sim.dem.famUseCase_)
            isSupp(famI,isCell{famI}(ch(m,isCell{famI}) & ...
                arrayfun(@(x) x.band.supp(fn),Cell(isCell{famI})))) = true;
        end
        isSupp1 = find(any(isSupp,1));
        if length(Sim.dem.famUseCase_) > 1 && ...
                any(xor(isSupp(1,:),isSupp(2,:)),2) && ...
                Sim.yea.thisYea >= 2019
            beep
            disp('Verify the code for this case!')
            keyboard
        end

        % []
        if ~isempty(isSupp1)
            % It is computationaly intesive, and only used sparingly, hence if
            % it's ready to be used is tracket by isBund1rea.
            if ~isBund1rea
%                 bund1 = c_bundle1(Cell,fn);
    
                % Create the hash for grouping the entries
                Hash = arrayfun(@(x) sum(x.pwrAmpClas) + sum(x.situ) + ...
                         sum(x.AeCouBS) + sum(x.sectoCou) + sum(x.FBMC), bund1( fn ).C );

                % Calculate indices ia and ic
                [ ~, bund1( fn ).ia, bund1( fn ).ic ] = unique( Hash, 'stable' ); 
                
                isBund1rea = true;  % is bund1 ready
            end

            if length(isCell) == 1
                isCell3 = {find(isSupp)};
            else
                isCell3 = cellfun(@(x) find(x),num2cell(isSupp,2),...
                    'UniformOutput',false);
            end
            visib = c_visib(MS,Cell(isSupp1),fn,studyA,bund1( fn ),thisDem,...
                fs1,Sim,bund2,SINR2tput,isCell3);

            if ~isempty(visib)
                % Print progress text
                if nnz(isXtunab) > 20
                    fprintf(1,[Sim.linBud.freq{fn} ' Ch' num2str(m) '...'])
                end

                % Define initial solution
%                         BHcou = size(Sim.dem.hourDistri,1);
                x0 = interfIequi(:,isSupp1,fn);

                % Solve optimisation problem
                param1 = c_param_sitWithiCov1(visib,fn,Sim,clu,...
                    Cell(isSupp1),bund1( fn ),fs1,thisDem,isSupp1);
                trafClI = find(Sim.dem.modeAlTrafClasAs);
                for n = 1 : max(structfun(@(x) ...
                        x.CEtput.ServCouMax(trafClI),Sim.dem.trafVol))
                    param2{n} = c_param_sitWithiCov2(visib,n,fn,Sim,...
                        Cell(isSupp1),MS,isSupp1,fs1,param1);
                end
                fun = @(x,y) c_sitWithiCov(Cell(isSupp1),visib,thisDem,...
                    fn,fs1,trafClI,clu,Sim,param1,param2,false,x,y);
                if Sim.servDem.isInterfeExpl.cov && any(isXtunab(isSupp1))
                    [out1,out2,out3,exitflag] = c_interf(fun,x0,Sim,...
                        Cell(isSupp1),interfIequi(:,isSupp1,fn),...
                        isXtunab(isSupp1),triPoi,param1{9});
                    if exitflag > 0
                        x = out1;
                        Cell(isSupp1) = out2;
                        thisDem = out3;
                    else
                        x = x0;
                    end
                else
                    x = x0;
                    [Cell(isSupp1),thisDem] = fun(x0,Sim.utilisN);
                end
                interfIequi(:,isSupp1,fn) = x;
                if any(Sim.yea.thisYea==[2020,2020]) && isDispLinBud
                    fun = @(x,y) c_sitWithiCov(MS,Cell(isSupp1),...
                        visib,thisDem,bund1( fn ),bund2.(fam),fn,fs1,fam,famI,trafCl,...
                        trafClI,clu,Sim,isSupp1,isDispLinBud,x,y);
                    fun(x,Sim.utilisN);
                end
            end
        end
    end

    if isPlot == 1
        % Prepare for plots below
%                 demPlot = thisDem(studyA.INfoc([thisDem.IND]));
%                 [X,Y] = studyA.R.intrinsicToWorld([demPlot.J],[demPlot.I]);
%                 X = num2cell(X);
%                 Y = num2cell(Y);
%                 [demPlot(:).X] = deal(X{:});
%                 [demPlot(:).Y] = deal(Y{:});
%                 clear X Y
        [X,Y] = studyA.R.intrinsicToWorld([thisDem.J],[thisDem.I]);
        X = num2cell(X);
        Y = num2cell(Y);
        [thisDem(:).X] = deal(X{:});
        [thisDem(:).Y] = deal(Y{:});
        clear X Y

        % Plot maps of demand, per UE type, ml
        for on = 1 : length(Sim.dem.operators)
            for pn = 1 : length(Sim.linBud.UE.pwrAmpClas_)
                pwrAmpClasMS = Sim.linBud.UE.pwrAmpClas_{pn};
                situMS = Sim.linBud.UE.situ_{pn};

                for b2n = 1 : size(bund2.(fam).ia,1)
                    mobiClas = bund2.(fam).ia(b2n);

                    v = arrayfun(@(x) full(x.(fam).(trafCl).(pwrAmpClasMS).(...
                        situMS).(Sim.dem.operators{on}).v(22,mobiClas)),thisDem);
                    satis = arrayfun(@(x) full(x.(fam).(trafCl).(pwrAmpClasMS).(...
                        situMS).(Sim.dem.operators{on}).satis(22,mobiClas)),thisDem);

%                             IND = v>0 & studyA.INfoc([thisDem.IND]).';
%                             if any(IND)
%                                 figure('paperpositionmode','auto','unit','in',...
%                                     'pos',[1 1 7.5 2.63./3.5.*7.5])
%                                 hold on
%                                 
%                                 patch(bsxfun(@plus,[thisDem(IND).X],...
%                                     studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
%                                     bsxfun(@plus,[thisDem(IND).Y],...
%                                     studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),v(IND).',...
%                                     'facecolor',[255 140 0]./255,'edgecolor','none')
%                                 
%                                 title(['Demand, ' Sim.dem.operators{on} ', ' pwrAmpClasMS ', ' ...
%                                     situMS ', ' num2str(Sim.yea.thisYea) ', ' fam ', ' ...
%                                     num2str(bund2.(fam).C(mobiClas).DL) ' Mbit/s DL, ' ...
%                                     num2str(bund2.(fam).C(mobiClas).UL) ' Mbit/s UL, ' ...
%                                     num2str(bund2.(fam).C(mobiClas).mobiClaVel) ' km/h'])
%                                 axis equal off
%                                 filename = ['Demand ' Sim.dem.operators{on} ' ' pwrAmpClasMS ' ' ...
%                                     situMS ' ' num2str(Sim.yea.thisYea) ' ' fam ' ' ...
%                                     num2str(bund2.(fam).C(mobiClas).DL) ' Mbit_s DL ' ...
%                                     num2str(bund2.(fam).C(mobiClas).UL) ' Mbit_s UL ' ...
%                                     num2str(bund2.(fam).C(mobiClas).mobiClaVel) ' kph'];
%                                 filename(strfind(filename,'.')) = '_';
%                                 savefig(filename)
%                                 print(gcf,fullfile('OutpM',filename),...
%                                     '-djpeg','-r300')
%                                 delete(gcf)
%                             end

                    figure('paperpositionmode','auto','unit','in',...
                        'pos',[1 1 7.5 2.63./3.5.*7.5])
                    hold on
                    isPlotOK = false;
                    IND = v>0 & satis & studyA.INfoc([thisDem.IND]).';
                    IND2 = find(strcmp({visib.pwrAmpClasMS},pwrAmpClasMS) & ...
                        strcmp({visib.situMS},situMS) & [visib.bund2]==b2n & [visib.bund1]==1);
                    IND(sum(sparse(IND2,[visib(IND2).thisDem_IND],...
                        true,length(visib),length(thisDem)),1)<=0) = false;
                    if any(IND)
                        patch(bsxfun(@plus,[thisDem(IND).X],...
                            studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
                            bsxfun(@plus,[thisDem(IND).Y],...
                            studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),v(IND).',...
                            'facecolor',[0 0.5 0],'edgecolor','none')

                        isPlotOK = isPlotOK || true;
                    end
                    IND = v>0 & satis & studyA.INfoc([thisDem.IND]).';
                    IND2 = find(strcmp({visib.pwrAmpClasMS},pwrAmpClasMS) & ...
                        strcmp({visib.situMS},situMS) & [visib.bund2]==b2n & [visib.bund1]==2);
                    IND(sum(sparse(IND2,[visib(IND2).thisDem_IND],...
                        true,length(visib),length(thisDem)),1)<=0) = false;
                    if any(IND)
                        patch(bsxfun(@plus,[thisDem(IND).X],...
                            studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
                            bsxfun(@plus,[thisDem(IND).Y],...
                            studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),v(IND).',...
                            'facecolor',[0 0.5 0.5],'edgecolor','none')

                        isPlotOK = isPlotOK || true;
                    end
                    IND = v>0 & ~satis & studyA.INfoc([thisDem.IND]).';
                    IND2 = find(strcmp({visib.pwrAmpClasMS},pwrAmpClasMS) & ...
                        strcmp({visib.situMS},situMS) & [visib.bund2]==b2n);
                    IND(sum(sparse(IND2,[visib(IND2).thisDem_IND],...
                        true,length(visib),length(thisDem)),1)<=0) = false;
                    if any(IND)
                        patch(bsxfun(@plus,[thisDem(IND).X],...
                            studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
                            bsxfun(@plus,[thisDem(IND).Y],...
                            studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),v(IND).',...
                            'facecolor',[1 0 0],'edgecolor','none')

                        isPlotOK = isPlotOK || true;
                    end
                    if isPlotOK
%                                 IND = strcmp(Sim.propag.BS{n2,1},pwrAmpClas) & ~isExpi & ...
%                                     strcmp(Operator,Sim.dem.operators{n1}) & INfoc;
                        INfoc = [Cell.INfoc];
                        Operator = {Cell.Operator};
                        IND = ~isExpi & strcmp(Operator,Sim.dem.operators{on}) & INfoc;

%                                 IND2 = arrayfun(@(x) x.bareMeta.maxSuppVel,Cell) > 50;
%                                 plot([Cell(IND&IND2).X],[Cell(IND&IND2).Y],'ko')
%                                 plot([Cell(IND&IND2).X],[Cell(IND&IND2).Y],'k.')
%                                 
%                                 IND2 = arrayfun(@(x) x.bareMeta.maxSuppVel,Cell) <= 50;
%                                 plot([Cell(IND&IND2).X],[Cell(IND&IND2).Y],'ks')
%                                 plot([Cell(IND&IND2).X],[Cell(IND&IND2).Y],'k.')

                        title(['Service of demand, ' Sim.dem.operators{on} ', ' pwrAmpClasMS ', ' ...
                            situMS ', ' num2str(Sim.yea.thisYea) ', ' fam ', ' ...
                            num2str(bund2.(fam).C(mobiClas).DL) ' Mbit/s DL, ' ...
                            num2str(bund2.(fam).C(mobiClas).UL) ' Mbit/s UL, ' ...
                            num2str(bund2.(fam).C(mobiClas).mobiClaVel) ' km/h'])
                        axis equal off
                        filename = ['Service of demand ' Sim.dem.operators{on} ' ' pwrAmpClasMS ' ' ...
                            situMS ' ' num2str(Sim.yea.thisYea) ' ' fam ' ' ...
                            num2str(bund2.(fam).C(mobiClas).DL) ' Mbit_s DL ' ...
                            num2str(bund2.(fam).C(mobiClas).UL) ' Mbit_s UL ' ...
                            num2str(bund2.(fam).C(mobiClas).mobiClaVel) ' kph'];
                        % savefig(filename)
                        print(gcf,fullfile('OutpM',filename),...
                            '-djpeg','-r300')
                        delete(gcf)



                        disp(['Service of demand, ' Sim.dem.operators{on} ', ' pwrAmpClasMS ', ' ...
                            situMS ', ' num2str(Sim.yea.thisYea) ', ' fam ', ' ...
                            num2str(bund2.(fam).C(mobiClas).DL) ' Mbit/s DL, ' ...
                            num2str(bund2.(fam).C(mobiClas).UL) ' Mbit/s UL, ' ...
                            num2str(bund2.(fam).C(mobiClas).mobiClaVel) ' km/h'])
%                                 disp(sum(v(satis)))
                        disp(sum(v(satis&studyA.INfoc([thisDem.IND]).')))

                        squeeze(Cell(1).band.utilisEff(22,1,:))
                        IND = ~isExpi & ...
                            strcmp(Operator,Sim.dem.operators{on}) & INfoc;
                        cell2mat(arrayfun(@(x) squeeze(...
                            x.band.utilisEff(22,1,:)),Cell(IND),...
                            'UniformOutput',false))
                        mean(arrayfun(@(x) x.band.utilisEff(...
                            22,1,3),Cell(IND)))
                    end
                end
            end
        end
        clear on pn pwrAmpClasMS situMS mobiClas v satis IND IND2 isPlotOK INfoc Operator

        clear demPlot
    end

%             figure
%             bar(Cell(1).band.utilis)
%             xlabel('Hours of the day')
%             ylabel('BW required  (%)')
%             grid on
%             set(gca,'xlim',[0 24])
%             legend('f800MHz','f2100MHz','f3500MHz','f24GHz',...
%                 'location','northwest')
%             title('Cell 1')

        % Plot the handheld traffic volume, at 12.30, low mobility, Operator #1
%             figure
%             scatter([thisDem.I].',[thisDem.J].',6,arrayfun(@(x) x.(fam...
%                 ).(trafCl).handh.v(13,4,1),thisDem))

%             Plot the traffic volume
%             Assessed, Served: green
%             Assessed, Not served: red
%             switch fam
%                 case 'MBB'
%                     pwrAmpClasMS = 'handh';
%                     situMS = 'inVeh';
%                     on = 1;
%                     mobiClas = 4; thisBund2 = 2;
% %                     mobiClas = 5; thisBund2 = 3;
% %                     mobiClas = 6; thisBund2 = 4;
%                 case 'MTC'
%                     pwrAmpClasMS = 'mac';
%                     situMS = 'indo';
%                     on = 1;
%                     mobiClas = 1;
%                     thisBund2 = 1;
%                 case 'uMTC'
%                     pwrAmpClasMS = 'veh';
%                     situMS = 'outd';
%                     on = 2;
%                     mobiClas = 4;
%                     thisBund2 = 2;
%             end
%             figure
%             hold on
%             v = arrayfun(@(x) full(x.(fam).(trafCl).(pwrAmpClasMS).(...
%                 situMS).(Sim.dem.operators{on}).v(22,mobiClas)),thisDem);
%             satis = arrayfun(@(x) full(x.(fam).(trafCl).(pwrAmpClasMS).(...
%                 situMS).(Sim.dem.operators{on}).satis(22,mobiClas)),thisDem);
%             IND = v>0 & satis;
%             IND2 = find(strcmp({visib.pwrAmpClasMS},pwrAmpClasMS) & ...
%                 strcmp({visib.situMS},situMS) & [visib.bund2]==thisBund2);
%             IND(sum(sparse(IND2,[visib(IND2).thisDem_IND],...
%                 true,length(visib),length(thisDem)),1)<=0) = false;
%             if any(IND)
%                 patch(repmat([thisDem(IND).J],5,1) + ...
%                     repmat([-1 1 1 -1 -1].'./2,1,nnz(IND)),...
%                     repmat([thisDem(IND).I],5,1) + ...
%                     repmat([-1 -1 1 1 -1].'./2,1,nnz(IND)),v(IND).',...
%                     'facecolor',[0 0.5 0],'edgecolor','none')
%             end
%             IND = v>0 & ~satis;
%             IND2 = find(strcmp({visib.pwrAmpClasMS},pwrAmpClasMS) & ...
%                 strcmp({visib.situMS},situMS) & [visib.bund2]==thisBund2);
%             IND(sum(sparse(IND2,[visib(IND2).thisDem_IND],...
%                 true,length(visib),length(thisDem)),1)<=0) = false;
%             if any(IND)
%                 patch(repmat([thisDem(IND).J],5,1) + ...
%                     repmat([-1 1 1 -1 -1].'./2,1,nnz(IND)),...
%                     repmat([thisDem(IND).I],5,1) + ...
%                     repmat([-1 -1 1 1 -1].'./2,1,nnz(IND)),v(IND).',...
%                     'facecolor',[1 0 0],'edgecolor','none')
%             end
% %             scatter([thisDem.J].',[thisDem.I].',6,v)
%             %
%             plot([Cell.J],[Cell.I],'k^','markerfacecolor','k')
%             %
%             xlabel('Pixel on x-axis')
%             ylabel('Pixel on y-axis')
%             title([fam ', ' trafCl ', ' Sim.dem.operators{on} ', ' ...
%                 Sim.dem.mobiClasses{mobiClas} ', ' fs1 ', 21-22hrs'])
%             %
%             set(gca,'ydir','reverse')
%             axis equal
%             clear v IND IND2 satis pwrAmpClasMS situMS on

        % Plot PRx_across_system_BW_DL, Cell 1, bund2, hand, outd
%             IND = [visib.Celli]==1 & [visib.bund2]==2 & ...
%                 strcmp({visib.pwrAmpClasMS},'handh') & ...
%                 strcmp({visib.situMS},'outd');
%             if any(IND)
%                 figure
%                 hold on
%                 scatter([thisDem([visib(IND).thisDem_IND]).J],...
%                     [thisDem([visib(IND).thisDem_IND]).I],60,...
%                     [visib(IND).PRx_across_system_BW_DL],'s','filled')
%                 %
%                 plot([Cell(1).J],[Cell(1).I],'k^','markerfacecolor','k')
%                 %
%                 xlabel('Pixel on x-axis')
%                 ylabel('Pixel on y-axis')
%                 set(gca,'ydir','reverse','clim',[-70 -10])
%                 axis equal
%                 colorbar
%                 title([fam ', ' trafCl ', ' Sim.dem.mobiClasses{mobiClas} ...
%                     ', ' fs1 ', Cell 1, bund2.(fam), hand, outd'])
%             end
%             clear IND

%             % Plot SE_DL, Cell 1, bund2, hand, outd
%             IND = [visib.Celli]==1 & [visib.bund2]==2 & ...
%                 strcmp({visib.pwrAmpClasMS},'handh') & ...
%                 strcmp({visib.situMS},'outd');
% %             accumarray([visib.thisDem_IND].',1,[length(thisDem) 1])
% %             sum(sparse(IND2,[visib(IND).thisDem_IND],...
% %                 true,length(visib),length(thisDem)),1)
%             if any(IND)
%                 figure
%                 hold on
%                 scatter([thisDem([visib(IND).thisDem_IND]).J],...
%                     [thisDem([visib(IND).thisDem_IND]).I],60,...
%                     [visib(IND).SE_DL],'s','filled')
%                 %
%                 plot([Cell(1).J],[Cell(1).I],'k^','markerfacecolor','k')
%                 %
%                 xlabel('Pixel on x-axis')
%                 ylabel('Pixel on y-axis')
%                 set(gca,'ydir','reverse','clim',[0 6])
%                 axis equal
%                 colorbar
%                 colormap(parula(6))
%                 title([fam ', ' trafCl ', ' Sim.dem.mobiClasses{mobiClas} ...
%                     ', ' fs1 ', Cell 1, bund2.(fam), hand, outd'])
%             end
%             clear IND
%         multiWaitbar('For each frequency band, serve demand',...
%             (length(Sim.linBud.spotFreq) - fn + 1) ./ ...
%             length(Sim.linBud.spotFreq));
end
