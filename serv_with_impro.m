function [Cell,edgCloSit,thisDem,interfIequi] = serv_with_impro(Cell,...
    edgCloSit,thisDem,interfIequi,impro,Sim,isCell0,isXtunab,studyA,...
    SINR2tput,clu,bund1)

% For each upgrading site, temporarily release demand served by the
% site to recalculate utilis
if ~isempty(impro) && ~isempty(thisDem)
    BHcou = size(Sim.dem.hourDistri,1);
    isTxMultiCopiesDataToAllServ = cellfun(@(x) Sim.dem.trafVol.(x...
        ).CEtput.isTxMultiCopiesDataToAllServ(1),Sim.demTranslati(:,1));
    
    for n = 1 : length(impro)
        % Simplification
        IND = impro(n).CellIND;

        % Reset utilisation so that the cell is not utilised
%         Cell(IND).band.utilis(:) = 0;
        for m = find(isXtunab).'
            Cell(m).band.utilis(:) = 0;
        end

        % Select demand objects that were associated with the cell
        % in question, thisDem2
        assoc = [thisDem.assoc];
        assoc = reshape(assoc(:),[BHcou,size(Sim.demTranslati,1),...
            length(thisDem)]);
        assoc2 = assoc(:,isTxMultiCopiesDataToAllServ,:);
        assoc(:,isTxMultiCopiesDataToAllServ,:) = [];
%         thisDemI = any(any(reshape(Cell2Vec(assoc),size(assoc))==IND,2),1) | ...
%             any(any(cellfun(@(x) any(x==IND),assoc2),2),1);
        thisDemI = any(cell2mat(cellfun(@(y) squeeze(any(any(reshape(...
            Cell2Vec(assoc),size(assoc))==y,2),1) | ...
            any(any(cellfun(@(x) any(x==y),assoc2),2),1)),...
            num2cell(find(isXtunab.')),'UniformOutput',false)),2);
    
        % []
        thisDemIf = find(thisDemI);
        thisDemIn = nnz(thisDemI);

        % Reset demand points thisDem(thisDemI) so that they require service
        assoc = [thisDem(thisDemI).assoc];
        assoc = reshape(assoc(:),[BHcou,size(Sim.demTranslati,1),thisDemIn]);
        if any(isTxMultiCopiesDataToAllServ)
            disp('Check what should happen when there is multiconnectivity!')
            beep
            keyboard
        else
            assoc = cell2mat(assoc);
        end
        for m = 1 : thisDemIn
    %                     if any(cellfun(@(x) any(x==IND),thisDem2(m).assoc(:,isTxMultiCopiesDataToAllServ)))
    %                         disp('Check what should happen when there is multiconnectivity!')
    %                         beep
    %                         keyboardrhtr
    %                     end
            tmp2 = false(size(assoc,1),size(assoc,2));
            tmp3 = assoc(:,~isTxMultiCopiesDataToAllServ,m);
%             tmp2(:,~isTxMultiCopiesDataToAllServ) = tmp3 == IND;
            tmp2(:,~isTxMultiCopiesDataToAllServ) = reshape(any(...
                bsxfun(@eq,tmp3(:),find(isXtunab).'),2),size(tmp3));
            thisDem(thisDemIf(m)).assoc(tmp2) = {0};
            thisDem(thisDemIf(m)).satis(tmp2) = false;
            thisDem(thisDemIf(m)).satis(~tmp2&tmp3>0) = true;
            thisDem(thisDemIf(m)).SINR_DL(tmp2) = nan;
            thisDem(thisDemIf(m)).band(tmp2) = nan;
            thisDem(thisDemIf(m)).tputOveMbps(tmp2) = nan;
        end
    end

    % Serve demand points thisDem2 with the cell in question
%     isCell0 = false(size(Cell));
%     isCell0(IND) = true;
%     isXtunab = (isCell0 | c_neig(Cell,isCell0)).';  % are x elements tunable
%     isCell0(1:182) = true;
%     beep
%     disp('Hack for now!')
%     keyboard
%     [Cell,thisDem(thisDemI),interfIequi2] = serv_dem(Cell,thisDem2,...
%         Sim,studyA,SINR2tput,clu,false,isCell0,interfIequi,isXtunab,...
%         false);
%     if ~all(interfIequi2(:)==interfIequi(:))
%         beep
%         disp('Maybe combine the serve demand into one?!')
%         keyboard
%     end
end

% Serve demand
assert(length(unique(Sim.linBud.PRButilisCovera))==1,'Code this!')
triPoi = 1 : min(round((Sim.linBud.PRButilisCovera(1) + ...
    1./Sim.utilisN./2)./(1./Sim.utilisN)),Sim.utilisN)-1;
[Cell,thisDem,interfIequi] = serv_dem(Cell,thisDem,Sim,studyA,SINR2tput,...
    clu,false,isCell0,interfIequi,isXtunab,triPoi,false,bund1);
Cell = Cell.c_userDyn(Sim);
edgCloSit = edgCloSit.upd_coreCou(Cell,Sim);

% Calculate the cost
for n = find(isCell0)
    Cell(n) = Cell(n).c_cost(Sim);
end

C = unique([Cell(isCell0).connTowaCloID]);
C(C==0) = [];
for n = C
    edgCloSit(n) = edgCloSit(n).c_cost(Cell,Sim);
end
