function optim_per_yea(Sim,dem,studyA,Cell,edgCloSit,clu,ter,dsm,SlLigDat)
%% [Title]
% [Description]
%
% * v0.1 KK 13May16 Created
% * v0.2 AK Sep17   Fixed the priority order (prio) for the slices

%% Initialising the results
resu = RESU(Sim);

%% Pre-calculate the throughput that corresponds to SINR levels
fprintf(1,'Pre-calculate the throughput that corresponds to SINR levels...');
rul = Sim.rul;
rulAeCou = cell2mat(arrayfun(@(x) x.upg.AeCou,rul,'UniformOutput',false));
Sim.linBud.AeCouBS_ = unique(rulAeCou(rulAeCou>0)).';
% A = structfun(@(x) x.devic.pwrAmpClas,Sim.dem.trafVol,...
%     'UniformOutput',false);
% B = structfun(@(x) x.devic.situ,Sim.dem.trafVol,...
%     'UniformOutput',false);
% A = struct2cell(A).';
% B = struct2cell(B).';
% A = [A{:}];
% B = [B{:}];
% unique(cellfun(@(x,y) unique(Sim.linBud.(x).(y).AeCou),A,B))
Sim.linBud.AeCouMS_ = [1 2 4];
Sim.linBud.FBMC_ = [false true];
Band = unique(cell2mat(arrayfun(@(x) x.upg.Band,Sim.rul,...
    'UniformOutput',false)));
Band(Band==0) = [];
Band(Band==8.79) = 10;
Band(Band==18.467) = 20;
Band = unique(Band);
Sim.linBud.BW_ = Band.';
SINR2tput = c_SINR2tput(Sim);
% p_SINR2tput(SINR2tput,Sim,studyA,Cell,dem)
clear Band rul rulAeCou A
fprintf(1,'DONE\n');

%% Pre-calculations
% Pre-calculate the interfence multipliers
Sim = Sim.c_interf_multip(Cell);

% Pre-calculate the SINR standard deviation
Sim = Sim.c_SINR_SD;

% Pre-calculate stats for multi-server connectivity
Sim = Sim.c_mvncdf;

% Pre-calculate the bundles
bund1 = u_bund1(Sim,Cell);

%% Calculations per year
m_ = Sim.yea.start : Sim.yea.fin;

for m = 1 : length(m_)
    % Update the year
    Sim.yea.thisYea = m_(m);
    
    % Is this the first year that is modelled?
    if m == 1
        Sim.yea.isF = true;
    else
        Sim.yea.isF = false;
    end
    
    % Reset the rule for each cell to zero
    Cell = Cell.rese(Sim);
    edgCloSit = edgCloSit.rese(Sim);
    
    % Distribute demand into pixels
    clear thisDem  % Clearing it before the new structure is prepared in memory
    [thisDem,Sim] = dist_dem_year(dem,Sim,studyA,clu,false);
    
    % Write stats about the demand
    resu = resu.upd_demFoc(Sim,thisDem,studyA);
    
    %% Perform improvements
    [Cell,edgCloSit,resu,bund1] = do_impro(Cell,edgCloSit,resu,thisDem,...
        Sim,studyA,SINR2tput,clu,ter,dem,bund1,dsm,SlLigDat);

    %% Preparing this years results
    fprintf(1,'Generating results for %d...',Sim.yea.thisYea);
    resu = resu.prep_results(Cell,edgCloSit,Sim);
    fprintf(1,'DONE\n');
    
    % Write the results into a file
    fprintf(1,'Writing the results into file...');
    cell2csv(fullfile('Outp',Sim.results.filenames),resu.M)
    fprintf(1,'DONE\n');

    % Write utilisation of computational resources per edge cloud site
    if ~isempty(edgCloSit)
        fprintf(1,'Writing the utilisation of computational resources per edge cloud site into file...');
        resu.w_utilisPerEdgCloSit(Sim,Cell,edgCloSit)
        fprintf(1,'DONE\n');
    end
    
%     frewind(Sim.path.unservDemFileID)
    if ~isempty(resu.demUnservPerc)
        fprintf(Sim.path.unservDemFileID,'%s\n',...
            ['Year ' num2str(Sim.yea.thisYea)]);
        for n = 1 : 4
            fprintf(Sim.path.unservDemFileID,'%s\n',...
                strjoin(resu.demUnservPerc(n,:),','));
        end
        for n = 1 : size(Sim.dem.hourDistri,1)
            fprintf(Sim.path.unservDemFileID,'%s',...
                [strjoin(resu.demUnservPerc(4+n,1:2),',') ',']);
            fprintf(Sim.path.unservDemFileID,'%s\n',...
                strjoin(cellfun(@num2str,resu.demUnservPerc(4+n,3:end),...
                'UniformOutput',false),','));
        end
        fprintf(Sim.path.unservDemFileID,'\n');
    end
    
    fprintf(Sim.path.servPopuThorouFileID,'%s\n',...
        ['Year ' num2str(Sim.yea.thisYea)]);
    for n = 1 : size(resu.servPerc,1)
        fprintf(Sim.path.servPopuThorouFileID,'%s\n',...
            strjoin(resu.servPerc(n,:),','));
    end
    fprintf(Sim.path.servPopuThorouFileID,'\n');
    
    fprintf(Sim.path.BWperGovernora,'%s\n',...
        ['Year ' num2str(Sim.yea.thisYea)]);
    for n = 1 : size(resu.BWperGovernora,1)
        fprintf(Sim.path.BWperGovernora,'%s\n',...
            strjoin(resu.BWperGovernora(n,:),','));
    end
    fprintf(Sim.path.BWperGovernora,'\n');

    switch Sim.SA.name
      case 'West Bank'
        fprintf(Sim.path.BWperOslo,'%s\n',...
            ['Year ' num2str(Sim.yea.thisYea)]);
        for n = 1 : size(resu.BWperOslo,1)
            fprintf(Sim.path.BWperOslo,'%s\n',...
                strjoin(resu.BWperOslo(n,:),','));
        end
        fprintf(Sim.path.BWperOslo,'\n');
      case 'Gaza'
      otherwise
        error('Undefined')
    end
    
    % Calculate geographical coverage
%     c_geographi_cov(studyA,clu,ter,Cell,Sim,SINR2tput)
    
    % Write
%     if Sim.yea.thisYea == 2019
%         Cell.w_csv
%     end
end
