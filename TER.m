classdef TER
    % TER class for management of the terrain height
    % The TER class is responsible for creating the matrix that contains
    % the height of the Digital Terrain Map. More specifically it is
    % responsible for delivering the following:
    %
    % * creates the TER object
    % * plots a map of TER object
    
    properties
        A;  % Values
        R;  % maprasterref object
    end
    
    methods
function ter = TER(Sim,studyA)
%% TER class constructor
% TER is the class constructor of TER objects, ter. Sim is the Sim object.
%
% * v0.1 KK 25Jul17 Added headers

if nargin ~= 0  % Allow nargin == 0 syntax
    if exist(fullfile(Sim.path.ProgramData,'ter.mat'),'file')
        % Load DTM from file into workspace
        S = load(fullfile(Sim.path.ProgramData,'ter.mat'),'ter');
        
        % Prepare output
        ter = TER();
        ter.A = S.ter.A;
        ter.R = S.ter.R;
    else
        X = floor(studyA.BoundingBox(:,1)./1000).*1000 + 1;
        Y = floor(studyA.BoundingBox(:,2)./1000).*1000 + 1;
        [X,Y] = meshgrid(X(1):1000:X(2),Y(1):1000:Y(2));
        
        % Initialise object
        % Header info
        nrows = 1000;
        ncols = 1000;
        cellsize = 1;

        % Create raster of DTM
        XLimWorld = [floor(min(studyA.X)./cellsize) ...
            ceil(max(studyA.X)./cellsize)] .* cellsize;
        YLimWorld = [floor(min(studyA.Y)./cellsize) ...
            ceil(max(studyA.Y)./cellsize)] .* cellsize;
        RasterSize = [diff(YLimWorld) diff(XLimWorld)] ./ cellsize;
        ter.R = maprasterref('RasterSize',RasterSize,...
            'YLimWorld',YLimWorld,'ColumnsStartFrom','north',...
            'XLimWorld',XLimWorld);
        ter.A = -9999 .* ones(RasterSize(1),RasterSize(2),'single');
        clear XLimWorld YLimWorld RasterSize
        
%         [~,ia] = unique(floor(X./1000)+1i*floor(Y./1000));
        for n = 1 : numel(X)
            natGrid = EN2NGR(X(n),Y(n));
            natGrid = natGrid{1};

            if str2num(natGrid(4))<5 && str2num(natGrid(9))<5
                tmp = 'sw';
            elseif str2num(natGrid(4))<5 && str2num(natGrid(9))>=5
                tmp = 'nw';
            elseif str2num(natGrid(4))>=5 && str2num(natGrid(9))<5
                tmp = 'se';
            else
                tmp = 'ne';
            end
            f = fullfile(Sim.path.ter,...
                ['LIDAR-DTM-1M-' natGrid([1,2,3,8]) tmp]);
            if ~exist(f,'dir')
                error('Download data from DTM server!')
            end
            f = fullfile(f,...
                [lower(natGrid(1:2)) natGrid([3,4,8,9]) '_DTM_1M.asc']);
            if ~exist(f,'file')
%                 beep
%                 disp('DTM data missing!')
                continue
            end
            fileID = fopen(f);

            % Skip header
            for k = 1 : 2
                fgetl(fileID);
            end

            % Read header info
            %             xllcenter = fscanf(fileID,'xllcenter %f\n');
            %             yllcenter = fscanf(fileID,'yllcenter %f\n');
            xllcorner = fscanf(fileID,'xllcorner %f\n');
            yllcorner = fscanf(fileID,'yllcorner %f\n');
            assert(xllcorner==floor(X(n)./1000).*1000)
            assert(yllcorner==floor(Y(n)./1000).*1000)

            %             [X,Y] = meshgrid(xllcenter+[0:1:(ncols-1)].*cellsize,...
            %                 yllcenter+[(nrows-1):-1:0].*cellsize);
            assert(eps(xllcorner+cellsize./2+(ncols-1).*cellsize) < ...
                cellsize./2,'change indexing to integers')
            assert(eps(yllcorner+cellsize./2+(nrows-1).*cellsize) < ...
                cellsize./2,'change indexing to integers')
            [X2,Y2] = meshgrid(...
                xllcorner+cellsize./2+[0:1:(ncols-1)].*cellsize,...
                yllcorner+cellsize./2+[(nrows-1):-1:0].*cellsize);

            % Skip header
            fgetl(fileID);
            fgetl(fileID);

            % Read all file
            tmp = cell2mat(textscan(fileID,repmat('%f ',1,ncols)));
            [I,J] = ter.R.worldToDiscrete(X2(:),Y2(:));
            TF = ~isnan(I);
            ter.A(sub2ind(size(ter.A),I(TF),J(TF))) = tmp(TF);

            fclose(fileID);
        end

        % Save object to file
%         save(fullfile(Sim.path.ProgramData,'ter.mat'),'ter','-v7.3')
    end
end
end

function p_map(ter,studyA)
%% Plots terrain map
% p_map plots a terrain map that corresponds to the input ter object
%
% * v0.1 KK 25Jul17 Added header

N = 50;

% Convert the database coordinates to world ones
[X,Y] = meshgrid(1:N:size(ter.A,2),1:N:size(ter.A,1));
[X,Y] = ter.R.intrinsicToWorld(X,Y);

% Calculate the aspect ratio
ratio = (max(Y(:))-min(Y(:))) ./ (max(X(:))-min(X(:)));

% Create map
figure('paperpositionmode','auto','unit','in',...
    'pos',[1 -1 7.5 7.5.*ratio])
hold on
axis equal tight
ph1 = patch(bsxfun(@plus,X(:).',...
    ter.R.CellExtentInWorldX./2.*N.*[-1 1 1 -1 -1].'),...
    bsxfun(@plus,Y(:).',...
    ter.R.CellExtentInWorldY./2.*N.*[-1 -1 1 1 -1].'),...
    repmat(reshape(double(ter.A(1:N:end,1:N:end)),numel(X),1).',5,1),...
    'edgecolor','none','facealpha',0.5);

% Adjust colourmap
ch1 = colorbar;
demcmap([-1 max(ter.A(:))])

% Adjust axes
axis off
set(gca,'pos',[0 0 0.85 1])

% Add scale
add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);

plot(studyA.X,studyA.Y,'k','linewidth',1)

% Print map
% print(gcf,fullfile('OutpM','ter'),'-djpeg','-r300')
export_fig(fullfile('OutpM','ter.jpg'),'-jpg','-q100','-r300')
end
    end
end
