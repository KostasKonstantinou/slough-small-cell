function interpValid = isMotionValid(stateNew,stateNN,...
    ValidationDistance,studyA,R)
% Interpolate between state1 and state2 with ValidationDistance
dist = sqrt(sum((stateNew-stateNN).^2));
interval = ValidationDistance ./ dist;
interpStates = bsxfun(@times,stateNew-stateNN,[0:interval:1 1].') + ...
    stateNN;

% Check all interpolated states for validity
% [I,J] = studyA.R.worldToDiscrete(interpStates(:,1),interpStates(:,2));
I = floor((R(2)-interpStates(:,2))./R(4)) + 1;
J = floor((interpStates(:,1)-R(1))./R(3)) + 1;
interpValid = ~any(studyA.INobst(sub2ind(size(studyA.INobst),I,J)));
end