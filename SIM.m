classdef SIM
    % SIM class for management of the simulation
    % The SIM class is responsible for setting the properties of how to run 5GM. More specifically
    % it is responsible for delivering the following:
    %
    % *	creates the SIM object
    % * reads inputs from files
    % *	sets the object's properties
    % * reads meteorological data and stores an extract relevant to the study area for quick queries
    % * calculates where cells can be commisioned, common calculation accross years (proximity to
    %   infrastructure is dependent on where the new sites are placed during runtime)
    % * calculates which pixels will need path loss prediction
    % * calculates the priority in serving the demand
    
    properties
        path;  % Database paths
        SA;  % Study area
        yea;  % Year configuration
        seed;  % Seed configuration
        propag;  % Propagation configuration
        optimis;  % Optimisation configuration
        dem;  % Demand configuration
        exiCel;  % Existing cell configuration
        servDem;  % Service of demand configuration
        stri;  % String database
        linBud;  % Link budget configuration
        shari;  % Sharing configuration
        BsExiFromSitefinder;  % Existing cells from Sitefinder configuration
        results;  % Results configuration
        forcNetwCranIn;  % Force network to CRAN configuration
        forcNetwCranTyp;  % Force network to CRAN configuration
        edgCloSitCou;  % Edge cloud site configuration: desirable number
        edgCloSitForc; % Edge cloud site configuration: force location
        edgCloSitRemoMeth;  % Edge cloud site configuration: removal method
        coreCouPerServ;  % Edge cloud site configuration: number of cores per server
        servCouPerCabi;  % Edge cloud site configuration: number of servers per cabinet
        isShufNsitEstablishedDatOnStartYea;  % Logical variable that is true if in the first year we wish to shuffle new site establish dates
%         iteraCou;
        cost;  % Cost configuration
        rul;  % Rulebook
        RACHpream;  % RACH preamble configuration
        mstruct;  % Matlab native map projection configuration
        construct;  % Information on where cells can be commisioned
        demTranslati;  % Information on the demand columns
        DN50;  % Meteorogical data for ITU.R P.1812 model 
        N050;  % Meteorogical data for ITU.R P.1812 model
        Rp;  % Rainfall rate exceeded for the desired probability of exceedance  (mm/h)
        GmAPIkey;  % Google Maps API key
        dpsimplify;  % Recursive Douglas-Peucker Polyline Simplification tolerance
        utilisN = 10;  % Increments of utilis for its equilibrium calculation
        interfMultip;  % Percentage multipliers to interference
        rele;  % Release number for this code (a module global variable)
        SINRsd;  % SINR standard deviation based on IsameSitLin/IotSitLin and sigma of wanted and other site
        mvncdfY2;  % Pre-calculated matrix of multivariate normal cumulative distribution function
        bordCoor;  % Coordination at the border
        IM_DL2;  % Noise rise due to interference close to border
        disp;  % Display preferences
    end
    
    methods
%% SIM class constructor
function Sim = SIM()
% SIM is the class constructor of SIM objects, Sim.
% 
% The output structure Sim contains the following fields:
% path - Database path substructure
% SA - Study area substructure
% yea - Year configuration substructure
% seed - Seed configuration substructure
% propag - Propagation configuration substructure
% optimis - Optimisation configuration substructure
% dem - Demand configuration substructure
% exiCel - Existing cell configuration substructure
% servDem - Service of demand configuration substructure
% stri - String database substructure
% linBud - Link budget configuration substructure
% shari - Sharing configuration substructure
% BsExiFromSitefinder - Existing cells from Sitefinder configuration
%                       substructure
% results - Results configuration substructure
% forcNetwCranIn - Year when network switches from DRAN to CRAN
% forcNetwCranTyp - Cell array of cell type strings that switch from DRAN
%                   to CRAN, e.g. [{'macrocell'} {'smaCel5W'}]
% edgCloSitCou - Number of edge cloud sites
% edgCloSitForc - [lat;lon] vector with the coordinates for the forced edge
% 			      cloud site
% edgCloSitRemoMeth - Edge cloud removal method ('random','equal'). Equal
%                     has not been tried in locations other than Central
%                     London.
% coreCouPerServ - Number of cores per server
% servCouPerCabi - Number of servers per cabinet
% isShufNsitEstablishedDatOnStartYea - Logical variable that is true if in
%                                      the first year we wish to shuffle
%                                      new site establish dates
% cost - Cost configuration substructure
% rul - Rulebook substructure
% RACHpream - RACH preamble configuration
% mstruct - Matlab native map projection configuration substructure
% construct - MxNxA logical matrix (MxN is the study area raster, A is the
%             number of BS types) with elements that are true when a site
%             of a certain type (e.g. macrocell) can be commissioned on the
%             study area
% demTranslati - Array Mx8 matrix with information on the demand columns
%                thisDem.v. The 8 columsn of demTranslati are:
%                family of use cases, e.g. 'MBB'
%                traffic class, e.g. 'Streaming'
%                UE power amplifier class class, e.g. 'handh'
%                UE situation, e.g. 'outd'
%                tenant, e.g. 'Deutsche_Telekom'
%                mobility class, e.g. 'pedeStat'
%                tenant index, e.g. 1
%                UE antenna count, e.g. 2
% Rp - Rainfall rate exceeded for the desired probability of exceedance
%      (mm/h) substructure
% GmAPIkey - (string) set your own API key which you obtained from Google:
%            http://developers.google.com/maps/documentation/staticmaps/#api_key
%            Kostas has registered an API key and is available for a
%            sensible number of map requests:
%            \rW shared new\Bitbucket Local\KK\Code Library\Plotting\Google Map\API key
% dpsimplify - Recursive Douglas-Peucker Polyline Simplification tolerance
% utilisN - Increments of utilis for its equilibrium calculation
% interfMultip - Percentage multipliers to interference
% rele - Release number for this code (a module global variable)
% SINRsd - SINR standard deviation based on IsameSitLin/IotSitLin and sigma of wanted and other site
% mvncdfY2 - Pre-calculated matrix of multivariate normal cumulative distribution function
% 
% SA is a sub-structure relating to the study area and contains the
% following fields:
% pixSize - the pixel size of the map in the world, studyA.R.CellExtentInWorldX
% 
% yea is a sub-structure relating to the simulation year configuration and
% contains the following fields:
% start - start year of simulation
% fin - finish year of simulation
% thisYea - current year in simulation
% isF - logical variable that is true when the simulation is on the first
%       year
% 
% dem is a sub-structure relating to the demand configuration and contains
% the following fields:
% trafClas_ - cell vector that contains modelled traffic classes, e.g. {'Streaming';'Computing';'Storage';'Gaming';'Communicating'}
% filenames - filename configuration substructure
% prio - priority configuration substructure
% consiInMerit - tenant consideration in merit configuration substructure
% hourDistri - cell matrix that contains the modelled hours, e.g. {'Midweek in Jan','17to18';'Fri in Jun','16to17';'Sat in Jul','16to17';'Sat in Sep','06to07';'Sat in Oct','07to08';'Sat in Nov','07to08'}
% mobiClasses - cell vector that contains the mobility classes, e.g. {'popu';'DELN';'DELL';'pedeStat';'lowMobi';'hiMobi'}
% demSources - cell vector that contains the demand sources, e.g. {'popu';'DELN';'DELL';'Motorway';'A_road';'B_road';'Minor_road';'Railway';'Tunnelled_railway'}
% famUseCase_ - cell array that contains the tenants {'MBB'  'AGV'}
% trafVol - traffic volume configuration substructure
% operators - cell array that contains the InP {'Deutsche_Telekom'  'HPAautomVeh'}
% mobiToDemSour - mobility class, demand source, family of use cases, UE type & situation configuration substructure
% mobiClaVel - vector of the maximum velocity per mobility class
% modeAlTrafClasAs - logical vector that is expected to have only one true
%                    element. During the project it was decided that
%                    modelling traffic classes individually was not
%                    desirable, and for simplicity we should consider all
%                    eMBB demand as buffered video. The name of the
%                    variable stands for model all traffic classes as.
% operat.canBui - logical array that is true if the InP can build new
%                 infrastructure
% servOrd - matrix of indices of the priority order that traffic classes
%           and service families should be considered for service. The
%           first column is an index to the family of services, the second
%           column is an index to the tarffic class.
% consiInMeritPerTena - logical vector that is true if the tenant is
%                       considered in merit calculations
% 
% dem.trafVol is a vector of structures relating to the traffic volume
% configuration of each family of use cases, e.g. dem.trafVol.MBB.
% dem.trafVol.(MBB) is a structure relating to the traffic volume
% configuration of family of use cases MBB and contains the following
% fields:
% mean - mean traffic volume configuration substructure
% ratio - ratio of traffic volumes configuration substructure
% distri - distribution around the mean traffic volume configuration substructure
% hourDistri - an MxN matrix of ratios that indicate how the traffic is distributed across the M traffic classes and the N modelled hours
% subPenet - subscriber penetration configuration substructure
% CEtput - [1�1 struct]ghjtyjyt
% devic - UE configuration substructure
% bund2 - vector of M cells (M traffic classes), each containing a logical
%         1xN array whose elements are true when the UE type (N UE types)
%         is considered for this traffic class
% 
% servDem.isInterfeExpl is a substructure with the following fields:
% cov - Logical scalar that is true when the end-user has selected the
%       interference to be calculated explicitely (i.e. based on received
%       signal strength and loading levels of wanted and interfering
%       cells) for coverage assessment. The end-user can select this in the
%       link budget csv. 
% capa - Logical scalar that is true when the end-user has selected the
%        interference to be calculated explicitely (i.e. based on received
%        signal strength and loading levels of wanted and interfering
%        cells) for capacity assessment. The end-user can select this in
%        the link budget csv. Code implementation has not finished for the
%        case this switch is set to true.
% 
% stri is a substructure that is intended to hold all strings (similar to
% mobile app design where there is a database of all strings for ease of
% finding them) used with the following fields:
% opexRaAnUtV - String of rates and utilities. The UK has business rates,
%               hence the string should include the word rates.
% 
% linBud is a substructure that holds information on the link budget. It
% contains the following fields.
% filenames.dop - String of the csv filename that contains the Doppler
%                 information, i.e. how many dB the Doppler margin is.
% filenames.resi - String of the csv filename that contains the resilience
%                  information, i.e. how many spare servers are required
%                  for a given number of working servers.
% filenames.chRaste - String of the csv filename that contains the channel
%                     raster information.
% BS.pwrAmpClas_ - List of strings of BS power amplifier classes, as read
%                  from link budget csv.
% BS.situ_ - List of strings of BS situations, as read from link budget
%            csv.
% UE.pwrAmpClas_ - List of strings of UE power amplifier classes, as read
%                  from link budget csv.
% UE.situ_ - List of strings of UE situations, as read from link budget
%            csv.
% UE.pwrAmpClasUniq_ - Result of unique(UE.pwrAmpClas_).
% framStru.filenames - String of the csv filename that contains fram
%                      structure information, e.g. how much of the
%                      resources are attributed to nondata channels.
% framStru.DL - A 1x3 cell array of 3xM matrices. The first cell
%               corresponds to CFI=1, the 2nd to CFI=2, the 3rd to CFI=3.
%               Each matrix has 3 rows: 1) the duplex mode indicated by 1
%               for TDD and 2 for FDD, 2) the bandwidth in MHz (note that
%               in TDD the entry is NaN becuase the BW is irrelevant, 3)
%               the decimal fraction of recources allocated to non-data
%               channels. The number of columns is as many are provided in
%               the frame structure csv input.
% framStru.UL - A scalar equal to the decimal fraction of recources
%               allocated to non-data channels.
% str - List of strings as appearing in link budget csv, rows 6 to 9 and
%       3rd column. Therefore, currently the number of assessed bands is
%       hardcoded to 4.
% SEmean.(XU_MIMO).(XDD) - Cell matrix containing the assumption on cell
%                          average SE for different number of antennas at
%                          the Tx and Rx sides, as read from the link
%                          budget csv. XU_MIMO can be either SU_MIMO or
%                          MU_MIMO, XDD can be either FDD or TDD.
% (pwrAmpClas).(situ).(param).(freq).(teledensity) - Structure created
%   dynamically by reading the link budget csv. The dynamic field
%   pwrAmpClas can be the power amplifier class of BS or UE, situ
%   corresponds to the situation, param to the link budget parameter (e.g.
%   GA, GB, GC for antenna, body, cabling gain) (note that the names of the
%   parameters are read from the link budget csv). Note that some
%   parameters are frequency dependent (see link budget csv) and these have
%   the freq substructure, and some are frequency and teledensity dependent
%   and these have additionally the teledensity substructure.
% (param).(freq) - Structure created dynamically by reading the link budget
%                  csv (variable with the frequency band section). For
%                  example, spotFreq.(freq) is the spot frequency for path
%                  loss calculations in each band, PRButilisCovera.(freq)
%                  is the maximum PRB utilisation for coverage
%                  calculations.
% 
% shari is a substructure that holds information on infrastructure sharing,
% or otherwise stated on which tenant (service provider) can be
% accommodated by which infrastructure provider.
% It contains the following fields:
% filenames - The filename where the end-user selects the sharing
%             requirement.
% from - A scalar equal to the year that sharing commences. Prior to that
%        year, an infrastructure provider serves only the its own service
%        provider.
% is - An MxN logical matrix, where M is the number of infrastructure
%      providers and N is the number of service providers, with elements
%      set to true when a service provider can be hosted by an
%      infrastructure provider. The matrix is read from the end-user
%      filled-in shariInfr_v0_3.csv. 
% 
% results.filenames - String containing the filename of the results csv
%                     file.
% 
% rul is an array of structures relating to the rulebook configuration and
% contains the following fields:
% rulID - rule ID, depricated because rules are referenced by their index
%         within Sim.rul
% pwrAmpClas - power class amplifier of the rule, and is one string out of
%              Sim.propag.BS(:,1)
% isUpg - logical variable that is true if the rule corresponds to a site
%         upgrade
% ValidFrom - year which this rule is valid from
% ValidTo - year which this rule is valid until, inclusive
% curre - current Cell configuration substructure
% curreHash - hash of current Cell configuration substructure
% upg - upgrade Cell configuration substructure
% upgHash - hash of upgrade Cell configuration substructure
% isDecomm - logical variable that is true if the rule corresponds to a
%            site refresh
% 
% RACHpream is a substructure with the following fields:
% form - A scalar integer 0, 1, 2, 3 equal to the format of RACH preamble
% ran_m - A scalar that is equal to the maximum cell range as dictated by
%         RACH preamble, in metres. UE further than the maximum range will
%         experience degradation of link quality, and as such we cap the
%         propagation prediction (and chances of service) up to this range. 
% 
% path is a sub-structure relating to the path configuration and contains
% the following fields:
% dr - path to Dropbox, e.g. 'Z:\Dropbox\'
% prog - path to main program
% ProgramData - path to program data
% ter - path to terrain file, e.g. 'D:\Dropbox (Real Wireless)\DB\Germany\DGM10_2x2KM_XYZ_130226'
% clu - path to clutter fiel, e.g. 'D:\Dropbox (Real Wireless)\DB\Projects\5G Monarch\Corine Land Cover\g100_12_5GM.tif'
% DB.Lbc - path to calculated databases - median path loss, e.g. 'D:\Dropbox (Real Wireless)\DB\Projects\5G Monarch\Lbc'
% add - path to address database, e.g. 'D:\Dropbox (Real Wireless)\DB\Germany\ALKIS_Adressen_HH_2018-01-02\Adressen'
% roa - path to road database, e.g. 'D:\Dropbox (Real Wireless)\DB\Germany\hamburg-latest-free.shp\gis.osm_roads_free_1'
% rail - path to rail database, e.g. 'D:\Dropbox (Real Wireless)\DB\Germany\hamburg-latest-free.shp\gis.osm_railways_free_1'
% HPApoly - path to HPA polygon, e.g. 'D:\Dropbox (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication\05 T6-4\04 Study item 2 - demand distribution\Inputs from HPA\HPA_Outlines\HPA_Outlines ARE_Border_HPA'
% cruShipTerPoly - path to cruise ship terminal polgyons, e.g. 'D:\Dropbox (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication\05 T6-4\04 Study item 2 - demand distribution\Inputs from HPA\HPA_Outlines\HPA_Outlines ARE_CruiseShipTerminals'
% exLoc - path to exchange locations, e.g. 'D:\Dropbox (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication\05 T6-4\06 Study item 4 - techno-economic method\Received from partners\Exchange locations.xls'
% BSexi - path to MNO's existing BS locations, e.g. 'D:\Dropbox (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication\05 T6-4\06 Study item 4 - techno-economic method\Received from partners\Hamburg KK.xlsx'
% automLoc - path to polygons of automated vehicles of HPA, e.g. 'D:\Dropbox (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication\05 T6-4\04 Study item 2 - demand distribution\Input from Ken\Automation locations v2.xlsx'
% earth - path to databases about Earth, e.g. 'D:\Dropbox (Real Wireless)\DB\Earth'
% 
% propag is a sub-structure relating to the path configuration and contains
% the following fields:
% BS - Mx8 cell matrix, where M is the number of BS types, and the columns
%      are detailed inline.
% UE - Mx5 cell matrix, where M is the number of UE types, and the columns
%      are detailed inline.
% 
% swit - propagation switch configuration substructure
% pix - 1xN cell array, where N is [TBC], e.g. {[29677�5 double]  [886�5 double]  [0�5 double]  [0�5 double]}
% 
% utilisN - Number of increments of utilis for its equilibrium calculation.
%           The equilibrium calculation is an optimisation of interference
%           levels (with number of increments equal to utilisN), so that
%           the unserved demand quantity is minimised (which would
%           correspond to network SINR maximisation across all services)
%           given the constraint that the interference levels correspond to
%           loading levels of at least the bandwidth utilisation. Note that
%           if utilisN=10, then the first bin is from 0 to 10% loading and
%           with nominal loading of 5%. This means that the optimisation
%           algorithm never polls the case of effectively switching off the
%           cell (0% loading); this could be improved in future versions.
% 
% interfMultip - An MxN cell matrix, where M is the number of sectors in a
%                site (secCou), N is the index of the wanted sector (secW).
%                Each cell is either empty or an AxBxC matrix, where A is
%                the interference index of the wanted cell (loadW), B is
%                the interference index of the interfering cell (loadI), C
%                is the index of the interfering sector (secI). The sector
%                indices are 1,2,3 for 0,120,240 degrees due North,
%                respectively. See method c_interfMultip and do_impro
%                slidepack  for more details on the methodology.
% 
% SINRsd - SINR standard deviation. When there is a dominant interferer
%          the SINR consists of three quantities: S the RSRP from the
%          wanted cell, I the RSRP from the interfering cell (times) the
%          interference multiplier, N the thermal noise contribution. In
%          the typical scenario that N is several dB lower than I, N can be
%          ignored, and SINR becomes SIR. As a negation of two Random
%          Variables of normal distribution, the result follows normal
%          distribution that depends on the standard devation of the two
%          component R.V. When I becomes comparable to N, and because N is
%          constant (not an R.V.), the standard deviation of SINR reduces.
%          For the extreme case that the I is zero (or very small compared
%          to N), the standard deviation of SINR is equal to that of S.
%          Reminder that S and I follow normal distribution because we
%          assume that the total path loss (path loss including shadowing)
%          follows normal distribution, which is the typical assumaption
%          for shadowing. See method c_SINRsd and do_impro slidepack for
%          more details on the methodology.
% 
% * v0.1 Kostas Konstantinou Sep 2018

% Running user
% Find the running user
[~,userName] = system('echo %USERNAME%');
switch userName(1:end-1)
    case 'Hassan' % server
        Sim.path.dr = 'D:\Real Wireless\';
        % Check if this LN laptop and Not server
        if ~exist(Sim.path.dr, 'dir')
            Sim.path.dr = 'C:\Users\Hassan\Real Wireless\';
        end
		Sim.path.on = Sim.path.dr;  % OneDrive
        Sim.path.drSSD = Sim.path.dr;
        
        Sim.GmAPIkey   = 'AIzaSyBspljapy9mJptxv2_9Yw3G46KQDwP3Xkw';
    case 'Hassan_Dell'
        Sim.path.dr = 'C:\Users\Hassan_Dell\Real Wireless\';
        Sim.path.on = Sim.path.dr;  % OneDrive
        Sim.path.drSSD = Sim.path.dr;
    case 'Kosta'
        Sim.path.on = 'D:\Real Wireless\';  % OneDrive
        Sim.path.onSSD = [];  % OneDrive SSD
        
        % Kostas' personal API key:
        Sim.GmAPIkey = inputdlg({'Enter Google API key:'},'API key');
        Sim.GmAPIkey = Sim.GmAPIkey{1};
    case 'akaro'
        Sim.path.on    = 'D:\Real Wireless\'; % OneDrive
        Sim.path.drSSD = Sim.path.on;
        % Google Maps API Key
        Sim.GmAPIkey   = 'AIzaSyBspljapy9mJptxv2_9Yw3G46KQDwP3Xkw';
    otherwise
        error('Fill this in!')
end

% Check if pool is already open
% if ~isdeployed
%     v = ver;
%     Flags.isPar = any(strcmp('Parallel Computing Toolbox',{v.Name}));
%     if Flags.isPar
%         if isempty(gcp('nocreate'))
%             parpool;
%         end
%         
%         p = gcp('nocreate');
%         Flags.poolsize = p.NumWorkers;
%     else
%         Flags.poolsize = 1;
%     end
% end
% clear v

% []
Sim.path.prog = fileparts(mfilename('fullpath'));
k = find(Sim.path.prog=='\',2,'last');
Sim.path.ProgramData = fullfile(Sim.path.prog(1:k(1)-1),...
    'ProgramData','Slough-small-cell');
% e.g. fullfile(Sim.path.ProgramData,'thoroufare_lengths.mat')

% Check if python has been installed loaded
[v,e] = pyversion;
assert(strcmp(v,'3.5') && strfind(e,'Anaconda3'),...
    'Please install Python v3.5, Anaconda3')
py.sys.path;  % Forces Python to be loaded
[~,~,loaded] = pyversion;
assert(loaded,'Python has not managed to load')
end

%% Simulation Parameters
function Sim = s_parame(Sim)
% % []
% 
% Note that there is a lot of hardcoded values in this function that should
% be externalised. 

% The choise of buffer width is down to the user and project requirements. It is expected that 1
% tier of sites would not be beyond 1 ISD. Thus, for an expected ISD of 500 m, which is the IMT
% Advanced default for urban, the end-user could select the buffer size to be 500 m. For the
% suburban case it could be 1299 m.
% Sim.SA.Buff    = 1;   % 500 (urban), 1299 (suburban). One macrocell ISD, Rep. ITU-R M.2135-1
Sim.SA.pixSize = 25; % 25x25 m
% The profile resolution in meters; should it be equal to Sim.SA.pixSize?
Sim.propag.Deltadt = 1; % Target profile spacing, m
% Recursive Douglas-Peucker Polyline Simplification tolerance (maximal euclidean distance allowed
% between the new line and a vertex). To create the buffer around the focus area faster, we use the
% simplification algorithm. We choose a value for the tolerance similar to that of the pixel size,
% since the level of detail we wish is rough a pixel size. 
% Sim.dpsimplify.tol = 250;

% Create Sim.yea object
Sim.yea.start = 2020;
Sim.yea.fin = 2030;

% Create Sim.seed object
Sim.seed.subs = 1830; % Seed for subscriber selection (home and office)
Sim.seed.LOSprob = 11;   % Seed for LOS probability function when the path is under clutter height
Sim.seed.distri = 924;  % Seed for distribution of traffic amongst people

% Propagation related, BS - The columns are:
% 1. BS amplifier class
% 2. New BS height. The source of these is in the scope document of 5G
% Monarch. In 5G Monarch we assumed 8m for new small cells. If NaN, then
% the height is populated within apply_impro.
% 3. Demand sources that if present on a pixel enable the pixel as a candidate for building a BS
% 4. Maximum range for path loss calculations
% 5. Logical array with true elements for bands that are supported at some stage within the BS's lifetime
% 6. Minimum separation distance between same-type BS
% 7. End-of-life duration in years
Sim.propag.BS = {'macrocell'  25 '' {'DELR' 'DELN' 'DELL' 'Motorway' 'A road' 'B road' 'Minor road' 'Railway'}  -1 logical([1 1 1 1]) 250 7
                 'smaCel5W'  NaN '' {'DELR' 'DELN' 'DELL' 'Motorway' 'A road' 'B road' 'Minor road' 'Railway'} 2000 logical([0 1 1 0])  65 5
                 'smaCel0W25'  4 '' {'DELR' 'DELN' 'DELL' 'Motorway' 'A road' 'B road' 'Minor road' 'Railway'} 200 logical([0 0 0 1])  65 5};
assert(isequal(Sim.propag.BS(:,1), {'macrocell';'smaCel5W';'smaCel0W25'} ),...
        'Consider which BS type has priority in accommodating demand in c_visib and c_sitWithiCov')

% Propagation related, MS - The columns are:
% 1. UE amplifier class, 
% 2. UE height, used by the propagation module
% 3. clutter translation table, used by the propagation module
% 4. containing polygon name (e.g. {'INbuf'} for in buffer i.e. whole sim area), list of demand sources e.g. {'Motorway' 'A road'},
% 5. family of services (AKA service),
% 6. situation (would be added if there was a mix of situations),
% 7. demand source pixes [not sure this is a good name for it; in ComReg it was used to associate to
%                         which Lbc file in the DB for each service ]
% Function run_propa_modu_med() uses the height, clutter translation, and location.
Sim.propag.UE = ...
    {'handh' 1.5 'pop' {{'INbuf'} {'DELR' 'DELN' 'DELL' 'Motorway' 'A road' 'B road' 'Minor road' 'Railway'}} 'MBB'};

% % Propagation related, BS and MS
% % Commented lines of code. If we wish to insert a switch in the future,
% % like we had in the past this could be inspiration.
% % Sim.propag.isTerFlat = true;  % If true then terrain path profile is set to flat, if false then the actual terrain is taken into account
% % Sim.propag.isLOSc    = false; % If true then LOS calculations take place, if false then NLOS is assumed

% Optimisation related
Sim.optimis.decomm = false;  % If true then decommissions are allowed to take place, if false then sites are refreshed even when not needed
% Sim.optimis.repoTput = [0.5 1.5 2 3 5 10 25 30 50];  % Array of throughputs that we want reports for
Sim.optimis.repoTput = [];  % Array of throughputs that we want reports for

% Database paths - London

% Database paths - Hamburg
% Sim.path.ter = fullfile(Sim.path.on,'RND Server - General','Germany',...
%     'DGM10_2x2KM_XYZ_130226');
% Sim.path.clu = fullfile(Sim.path.earth,...
%     'Global Land Cover Characteristics Data Base (GLCC World)',...
%     'gigbp2_0ll.img');
% Sim.path.add = fullfile(Sim.path.on,'RND Server - General','Germany',...
%     'ALKIS_Adressen_HH_2018-01-02','Adressen');
% Sim.path.rail = fullfile(Sim.path.on,'RND Server - General','Germany',...
%     'hamburg-latest-free.shp','gis.osm_railways_free_1');
% Sim.path.HPApoly = fullfile(Sim.path.on,'5G_MONARCH_RW',...
%     '09 WP6-Verfication','05 T6-4',...
%     '04 Study item 2 - demand distribution','Inputs from HPA',...
%     'HPA_Outlines','HPA_Outlines ARE_Border_HPA');
% Sim.path.cruShipTerPoly = fullfile(Sim.path.on,'5G_MONARCH_RW',...
%     '09 WP6-Verfication','05 T6-4',...
%     '04 Study item 2 - demand distribution','Inputs from HPA',...
%     'HPA_Outlines','HPA_Outlines ARE_CruiseShipTerminals');
% Sim.path.exLoc = fullfile(Sim.path.on,'5G_MONARCH_RW',...
%     '09 WP6-Verfication','05 T6-4',...
%     '06 Study item 4 - techno-economic method','Received from partners',...
%     'Exchange locations.xls');  % Exchange locations
% Sim.path.automLoc = fullfile(Sim.path.on,'5G_MONARCH_RW',...
%     '09 WP6-Verfication','05 T6-4',...
%     '04 Study item 2 - demand distribution','Input from Ken',...
%     'Automation locations v2.xlsx');

% Database paths - Palestine
% palestineProj = fullfile(Sim.path.on,'RND Server - General','Projects',...
%     '5G Palestine');
% palestineDB = fullfile(Sim.path.on,'RND Server - General','Palestine');
% palestineDBgeo = fullfile(palestineDB,'Geographic areas');
% palestineDBsit = fullfile(palestineDB,'Sites');
% Sim.path.clu = fullfile(Sim.path.earth,...
%     'Global Land Cover Characteristics Data Base (GLCC World)',...
%     'gigbp2_0ll.img');
% Sim.path.roa.WB = fullfile(palestineProj,'gis_osm_roads_free_Oslo_1');
% Sim.path.roa.Gaza = fullfile(palestineProj,'gis_osm_roads_free_Gaza_1');
% Sim.path.roa.Settlements = fullfile(palestineProj,...
%     'gis_osm_roads_free_Settlements_1');
% Sim.path.Builtuparea = fullfile(palestineDBgeo,...
%     'ICT - builtup with population','Builtuparea');
% Sim.path.Governorates = fullfile(palestineDBgeo,'Governorates');
% Sim.path.IsraeliLocalities = fullfile(palestineDBgeo,'IsraeliLocalities');
% Sim.path.Oslo = fullfile(palestineDBgeo,'Oslo');
% Sim.path.Settlements = fullfile(palestineDBgeo,'Settlements');
% Sim.path.Towers = fullfile(palestineDB,'Towers','Towers');  % These are Israeli sites
% Sim.path.BSexi{1} = fullfile(palestineDBsit,...
%     'Quartet_ Radio Sites Data and coverage maps nov 5 2019.xlsx');  % Existing BS
% Sim.path.BSexi{2} = fullfile(palestineDBsit,'Ooredoo Sites - Numbers.kml');  % Existing BS
% Sim.path.BSexi{3} = fullfile(palestineDBsit,...
%     'Gaza Sites Coordinates - Ooredoo.xlsx');  % Existing BS
% Sim.path.setPop = fullfile(Sim.path.on,...
%     '02 Ext Shared - General','J1002 2G-5G analysis for OQ',...
%     '09 Received from client','Compiled data','Statistics',...
%     'Israel','5. Israeli data.xlsx');  % Settlement population
% Sim.path.tra.WB = fullfile(Sim.path.on,'RND Server - General','Palestine',...
%     'israel-and-palestine-latest-free.shp','gis_osm_transport_free_1');
% Sim.path.POI.WB = fullfile(Sim.path.on,'RND Server - General','Palestine',...
%     'israel-and-palestine-latest-free.shp','gis_osm_pois_free_1');
% Sim.path.tra.Gaza = Sim.path.tra.WB;
% Sim.path.POI.Gaza = Sim.path.POI.WB;
% Sim.path.tra.Settlements = fullfile(palestineProj,...
%     'gis_osm_transport_free_Settlements_1');
% Sim.path.POI.Settlements = fullfile(palestineProj,...
%     'gis_osm_pois_free_Settlements_1');

% Database paths - Slough
Sim.path.clu = fullfile(Sim.path.ProgramData,'lcm2007_25m_gb_Sl.tif');
Sim.path.roa = fullfile(Sim.path.on,'05 RW DB - General',...
    'Projects','Slough small cell','gis_osm_roads_free_1_clipped');
Sim.path.tra = fullfile(Sim.path.on,'05 RW DB - General',...
    'Projects','Slough small cell','gis_osm_transport_free_1_clipped');
Sim.path.POI = fullfile(Sim.path.on,'05 RW DB - General',...
    'Projects','Slough small cell','gis_osm_pois_free_1_clipped');
Sim.path.dsm = fullfile(Sim.path.on,'05 RW DB - General',...
    'United Kingdom','Digital Surface Map DSM','DEFRA');
Sim.path.ter = fullfile(Sim.path.on,'05 RW DB - General',...
    'United Kingdom','Terrain','DEFRA');
Sim.path.BSexi = fullfile(Sim.path.on,'01 RW Shared - General',...
    'Projects','Mentor','J10XX Title','09 Received from client',...
    'Slough Cell Site Data.xlsx');  % Existing BS

% Database paths - Common
Sim.path.earth = fullfile(Sim.path.on,'05 RW DB - General','Earth');
Sim.path.gPla = fullfile(Sim.path.earth,'Google Places');

% Log related - Restriction due to maximum range considered.
% a+ to write and keep existing info in the file. If we want to overwrite
% exisitng info then use: "w".
% Initialise logfile. Prepare data to be written to file.
foldername = 'Outp';
filename = 'log_maxdist';
Sim.path.logPropagFileID = fopen([foldername '\' filename '.txt'],'a+');
header = {'New run','','','','','','',''};
data = {'pixel_number','distance','Year','Cell','mobility_class',...
    'UE_pwr_amp_class','situation','Frequency'};
% Write to file
w_to_txt(Sim,header,data)
clear filename header data foldername

% Log related - Restriction due to demand being too large for the pixel.
Sim.path.logDemCapaFileID = fopen(fullfile('Outp','log_demCapa.csv'),'a+');
fprintf(Sim.path.logDemCapaFileID,'%s\r\n','New run');
fprintf(Sim.path.logDemCapaFileID,'%s,%s,%s,%s,%s\r\n',...
    'Frequency band','study area index',...
    '2D separation distance between Tx and Rx',...
    'BW requested  (MHz/site)','BW available  (MHz/site)');

% % Unserved demand file.
% Sim.path.unservDemFileID = fopen(fullfile('Outp','unserv dem 0_1.csv'),'a+');
% Sim.path.servPopuThorouFileID = fopen(fullfile('Outp','serv popu thorou 0_1.csv'),'a+');
% Sim.path.BWperGovernora = fopen(fullfile('Outp','BW per governora 0_1.csv'),'a+');

% Map projection related
% http://georepository.com/crs_28192/Palestine-1923-Palestine-Belt.html
% mstruct = defaultm('tranmerc');
% mstruct.origin = [31.73409694444445 35.21208055555556 0];
% mstruct.falseeasting = 170251.555;
% mstruct.falsenorthing = 1126867.909;
% mstruct.scalefactor = 1;
% mstruct.geoid = referenceEllipsoid('Clarke 1880');
% Sim.mstruct = defaultm(mstruct);
S = load(fullfile(Sim.path.on,'RND Server - General','United Kingdom','OSGM02','ostn.mat'),...
    'ostn');
Sim.mstruct.ostn = S.ostn;
clear S

%% RACH preamble
% % RACH preamble format - Currently not in use because the site max propagation distances are shorter
% % than what the PRACH allows. 
% Sim.RACHpream.form = 0;  % Results in radius about 15 km
% 
% % RACH preamble
% RACHpream = {'Preamble format' 'Cell range (km) [=C*RTD/2=C*GuardTime/2]'
%     0 14.53125
%     1 77.34375
%     2 29.53125
%     3 107.34375};
% 
% Sim.RACHpream.ran_m = RACHpream{...
%      [false;Sim.RACHpream.form==cell2mat(RACHpream(2:end,1))],2} .* 1e3;
% assert(all(Sim.RACHpream.ran_m>cell2mat(Sim.propag.BS(:,5))),...
%     'Take into account PRACH format')
% 
% % Load the distances of sites close to the border
% num = xlsread(fullfile(Sim.path.on,'01 RW Shared - General','Projects',...
%     'Paltel','J635 - Framwork Agreement P2 - RAN share analysis',...
%     '03 Received from customer',...
%     'SiteData and PopsPoints_receivedOn22Jul15','Qalqeley.xlsx'));
% [x,y] = mfwdtran(Sim.mstruct,num(1:3:end,3),num(1:3:end,2));
% %
% num = xlsread(fullfile(Sim.path.on,'01 RW Shared - General','Projects',...
%     'Paltel','J748 - LTE to 3G interference','03 Model',...
%     '03 Docs received from Paltel','2015_06_09',...
%     'Interferes on  Qalqeleyeh.xlsx'),'Cell Phone');
% [xInt,yInt] = mfwdtran(Sim.mstruct,num(:,1),num(:,2));
% %
% distm = min(abs(bsxfun(@minus,x+1i*y,xInt.'+1i*yInt.')),[],2) ./ 2;
% 
% % Load the noise rise that is caused by interference close to border
% S = load(fullfile(Sim.path.ProgramData,'IM_DL2'),'y');
% Sim.IM_DL2 = [distm ...
%     accumarray(fix((0:length(S.y)-1).'./3)+1,S.y,[length(S.y)/3 1],@mean)];
% Sim.IM_DL2 = sortrows(Sim.IM_DL2,1);
% % figure
% % plot(Sim.IM_DL2(:,1),Sim.IM_DL2(:,2),'o')
% % grid on
% % xlabel('Half distance to closest Israeli site  (m)')
% % ylabel('Noise rise  (dB)')
% 
% % Value beyond which noise rise should be set to 0
% % Sim.IM_DL2 = [Sim.IM_DL2
% %     max(distm)+mean(diff(sort(distm))) 0];

%% Display preferences
Sim.disp.scale.x = 0.60;  % See add_scale
Sim.disp.scale.y = -0.05;  % See add_scale
end

function Sim = read_inp(Sim,configFile)
%% REA_INP function reads the families of use cases from the configFile and updates the SIM model.
%       configFile contains a section, FAMILIES_OF_USE_CASES_INFORMATION where the different
%       families of use cases to be used  in this run are included,  as well as the necessary files
%       from where we load their data. 
%
%       Example families of use cases are MBB (mobile broadband), MTC (machine type
%       communication) etc.
%
%       After importing the files, the data are saved in the respective family use case in the Sim
%       object.        
%
%       INPUT:
%               configFile: the location and name of the configuration file
%                      Sim: the structure that holds all the information
%                           about the simulation that we want to run
%
%       OUTPUT
%                      Sim: the structure that holds all the information
%                           about the simulation that we want to run,
%                           including the information about the use cases

%% Traffic Classes
% The list of traffic classes
Sim.dem.trafClas_ = { 'Streaming'
                      'Computing'
                      'Storage'
                      'Gaming'
                      'Communicating' };

%% Read configuration file
% Open file
fileID = fopen(configFile);

% Finding the use cases section
tline = fgetl(fileID);
while ~strcmp(tline,'FAMILY_OF_USE_CASES_INFORMATION')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the family use case information')
    end
end

% The first family of use case ID
famUseCase = fgetl(fileID);

% Creating a list of the family use cases
listFamUseCases{1} = famUseCase;

% Reading the files for the use case 
stop = false;
while ~stop
    Sim.dem.filenames.(famUseCase).Mean = fgetl(fileID);
    Sim.dem.filenames.(famUseCase).Ratio = fgetl(fileID);
    Sim.dem.filenames.(famUseCase).Distribution = fgetl(fileID);
    Sim.dem.filenames.(famUseCase).HourDistribution = fgetl(fileID);
    Sim.dem.filenames.(famUseCase).SubscriberPenetration = fgetl(fileID);
    Sim.dem.filenames.(famUseCase).CellEdgeThroughput = fgetl(fileID);
    Sim.dem.filenames.(famUseCase).devicPerDemSour = fgetl(fileID);
    Sim.dem.filenames.(famUseCase).mobiCla = fgetl(fileID);
        
%     tline = fgetl(fileID);
%     Sim.dem.interf.(famUseCase) = lower(tline(1:find(isstrprop(tline,...
%         'wspace'), 1, 'first') - 1));
    
    tline = fgetl(fileID);
    Sim.dem.prio.(famUseCase) = lower(tline(1:find(isstrprop(tline,...
        'wspace'),1,'first')-1));
    
    % Set prority
    switch Sim.dem.prio.(famUseCase)
        case 'low'
            Sim.dem.prio.(famUseCase) = 3;
        case 'normal'
            Sim.dem.prio.(famUseCase) = 2;
        case 'high'
            Sim.dem.prio.(famUseCase) = 1;
        otherwise
            error('priority should be low, normal or high')
    end
    
    tline = fgetl(fileID);
    Sim.dem.consiInMerit.(famUseCase) = logical(str2double(lower(tline(...
        1:find(isstrprop(tline,'wspace'),1,'first')-1))));
    
    tline = fgetl(fileID);
    if strcmp(tline,'')        
        % Stop if the next line is not a use case related.
        stop = true;
        continue;
    else
        famUseCase = tline;
        listFamUseCases{end + 1} = famUseCase;
    end
end
frewind(fileID);

% Check with the end-user if it is OK to have all services at same priority
% level. This is becuase the priority was set to the same level in
% config.txt.
tmp = structfun(@(x) x,Sim.dem.prio);
if length(tmp)>1 && length(unique(tmp))==1
    beep
    disp('Is it intentional that all services have the same priority?')
    keyboard
end

% Link budget input file
tline = fgetl(fileID);
while ~strcmp(tline,'LINK_BUDGET')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the link budget information')
    end
end
Sim.dem.filenames.linBud = fgetl(fileID);
frewind(fileID);

% Read existing antenna sites from file, if any
tline = fgetl(fileID);
while ~strcmp(tline,'EXISTING_ANTENNA_SITES')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we ignore
    if tline == -1; break; end;    
end
if tline == -1
    % Default value
    Sim.exiCel.filename = '';
else
    Sim.exiCel.filename = fgetl(fileID);
end
frewind(fileID);

% Read switches
tline = fgetl(fileID);
while ~strcmp(tline,'SWITCHES')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we break
    if tline == -1; error('Define SWITCHES'); end;    
end
%
tline = fgetl(fileID);
switch lower(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1))
    case 'true'
        Sim.propag.swit.isPropagCalcOnSimGrid = true;
    case 'false'
        Sim.propag.swit.isPropagCalcOnSimGrid = false;
    otherwise
        error('Error in switches')
end
tline = fgetl(fileID);
switch lower(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1))
    case 'true'
        Sim.servDem.isInterfeExpl.cov = true;
    case 'false'
        Sim.servDem.isInterfeExpl.cov = false;
    otherwise
        error('Error in switches')
end
%
tline = fgetl(fileID);
switch lower(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1))
    case 'true'
        Sim.servDem.isInterfeExpl.capa = true;
    case 'false'
        Sim.servDem.isInterfeExpl.capa = false;
    otherwise
        error('Error in switches')
end
%
tline = fgetl(fileID);
% Sim.SA.name = tline;
% assert(any(strcmp(Sim.SA.name,{'West Bank','Gaza','Settlements'})))
%
tline = fgetl(fileID);
% switch lower(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1))
%     case {'true','na'}
%         Sim.optimis.canBuiAreC = true;
%     case 'false'
%         Sim.optimis.canBuiAreC = false;
%     otherwise
%         error('Error in switches')
% end
%
tline = fgetl(fileID);
Sim.bordCoor = lower(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1));
%
frewind(fileID);

% Read Voronoi use
tline = fgetl(fileID);
while ~strcmp(tline,'USE_VORONOI')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we break
    if tline == -1; break; end;    
end
%
if tline ~= 1
    tline = fgetl(fileID);
    switch lower(tline)
        case 'true'
            Sim.propag.useVoronoi = true;
        case 'false'
            Sim.propag.useVoronoi = false;
    end
else
    % Default value if the USE_VORONOI switch is not present is true
    Sim.propag.useVoronoi = true;
end
frewind(fileID);

% Read propagation model
tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),...
        'PROPAGATION_MODEL')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the cutoff distance and PL DB for this simulation')
    end
end
Sim.propag.mod = fgetl(fileID);
frewind(fileID);

% Read the location of the pathloss files
tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),...
        'PATHLOSS_FILES')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the cutoff distance and PL DB for this simulation')
    end
end
%
Sim.propag.pathFiles = fgetl(fileID);
frewind(fileID);

% switch Sim.SA.name
%   case 'West Bank'
%     Sim.path.BWperOslo = fopen(fullfile('Outp','BW per area 0_1.csv'),'a+');
%   case {'Gaza','Settlements'}
%   otherwise
%     error('Undefined')
% end

% Cost input file
tline = fgetl(fileID);
while ~strcmp(tline,'COST')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the cost information.')
    end
end
Sim.dem.filenames.cost = fgetl(fileID);
tline = fgetl(fileID);
k = strfind(tline,'"');
Sim.stri.opexRaAnUtV = tline(k(1)+1:k(2)-1);
frewind(fileID);

% Rulebook file
tline = fgetl(fileID);
while ~strcmp(tline,'RULEBOOK')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the rulebook information')
    end
end
Sim.dem.filenames.rulebook = fgetl(fileID);
frewind(fileID);

% Site type file
tline = fgetl(fileID);
while ~strcmp(tline,'SIT_TYP')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide information on site types')
    end
end
Sim.dem.filenames.sitTyp = fgetl(fileID);
frewind(fileID);

% Can build file
tline = fgetl(fileID);
while ~strcmp(tline,'CAN_BUILD')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide information on which MNO can build sites')
    end
end
Sim.dem.filenames.canBui = fgetl(fileID);
frewind(fileID);

% Interference suppression file
tline = fgetl(fileID);
while ~strcmp(tline,'INTERF_SUPPR')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide information on interference suppression')
    end
end
Sim.dem.filenames.interfSuppr = fgetl(fileID);
frewind(fileID);

% Doppler file
tline = fgetl(fileID);
while ~strcmp(tline,'DOPPLER')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the doppler information')
    end
end
Sim.linBud.filenames.dop = fgetl(fileID);
frewind(fileID);

% Doppler file
tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),...
        'RESILIENCE')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the resilience (computational availability) information')
    end
end
Sim.linBud.filenames.resi = fgetl(fileID);
frewind(fileID);

% Frame structure file
tline = fgetl(fileID);
while ~strcmp(tline,'FRAME_STRUCTURE')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the frame structure information')
    end
end
Sim.linBud.framStru.filenames = fgetl(fileID);
frewind(fileID);

% Sharing file
tline = fgetl(fileID);
while ~strcmp(tline,'SHARING')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the sharing information')
    end
end
Sim.shari.filenames = fgetl(fileID);
frewind(fileID);

% Channel raster file
tline = fgetl(fileID);
while ~strcmp(tline,'CHANNEL_RASTER')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the frame structure information')
    end
end
Sim.linBud.filenames.chRaste = fgetl(fileID);
frewind(fileID);

% Which database should we load for existing BS from Sitefinder
tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),...
        'BS_EXISTING_FROM_SITEFINDER')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide information on exisitng sites at sim start')
    end
end
tline = fgetl(fileID);
while ~all(isstrprop(tline,'wspace'))
    C = textscan(tline,'%s %s');
    Sim.BsExiFromSitefinder.(C{1}{1}) = C{2}{1};
    
    tline = fgetl(fileID);
end
frewind(fileID);

% Result file
tline = fgetl(fileID);
while ~strcmp(tline,'RESULTS')
    tline = fgetl(fileID);
    
    if tline == -1; break; end;
end
if tline == -1
    % Default value
    Sim.results.filenames = 'Results.csv';
else
    Sim.results.filenames = fgetl(fileID);
end
frewind(fileID);

% Force network to CRAN
tline = fgetl(fileID);
while ~strcmp(tline,'FORCE_NETWORK_TO_CRAN_IN')
    tline = fgetl(fileID);
    
    if tline == -1; break; end;
end
tline = fgetl(fileID);
Sim.forcNetwCranIn = str2double(tline);
tline = fgetl(fileID);
Sim.forcNetwCranTyp = {};
while ~all(isstrprop(tline,'wspace'))
    Sim.forcNetwCranTyp{end+1} = tline;
    tline = fgetl(fileID);
    assert(any(strcmp(Sim.forcNetwCranTyp{end},Sim.propag.BS(:,1))))
end
frewind(fileID);

% Edge cloud site
tline = fgetl(fileID);
while ~strcmp(tline,'EDGE_CLOUD_SITE')
    tline = fgetl(fileID);
    
    if tline == -1; break; end;
end
tline = fgetl(fileID);
Sim.edgCloSitCou = str2double(tline(1:find(isstrprop(tline,'wspace'), 1,'first')-1));
tline = fgetl(fileID);
Sim.edgCloSitForc = sscanf(tline,'%f %f %*s');  % Forced edge cloud site
tline = fgetl(fileID);
Sim.edgCloSitRemoMeth = tline(1:find(isstrprop(tline,'wspace'), 1,'first')-1);
tline = fgetl(fileID);
Sim.coreCouPerServ = str2double(tline(1:find(isstrprop(tline,'wspace'), 1,'first')-1));
tline = fgetl(fileID);
Sim.servCouPerCabi = str2double(tline(1:find(isstrprop(tline,'wspace'), 1,'first')-1));
frewind(fileID);

% Cost input file
tline = fgetl(fileID);
while ~strcmp(tline,'CELL')
    
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the cell information.')
    end
end
tline = fgetl(fileID);
Sim.isShufNsitEstablishedDatOnStartYea = logical(str2double(tline(1:find(...
    isstrprop(tline,'wspace'),1,'first')-1)));
tline = fgetl(fileID);
Sim.propag.SLAv = str2double(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1));

tline = fgetl(fileID);
Sim.propag.Am = str2double(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1));

tline = fgetl(fileID);
Sim.propag.ISDfact = str2double(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1));

tline = fgetl(fileID);
Sim.propag.V3dBfact = str2double(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1));
frewind(fileID);

% Read cut-off distance and DB for PLs
tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1), 'CUTOFF_DISTANCE_AND_PL_DB')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the cutoff distance and PL DB for this simulation')
    end
end
tline = fgetl(fileID); % Cutoff distance
Sim.propag.BS(1,5) = {str2double(tline)}; % Set the cutoff distance
frewind(fileID);

% Modelled hours
tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1), 'MODELLED_HOURS')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we cannot continue
    if tline == -1
        error('Please provide the modelled hours information.')
    end
end
tline = fgetl(fileID);
k = strfind(tline,'''');
Sim.dem.hourDistri = cellfun(@(x,y) tline(x+1:y-1),...
    num2cell(k(1:2:end)),num2cell(k(2:2:end)),'UniformOutput',false);
tline = fgetl(fileID);
k = strfind(tline,'''');
Sim.dem.hourDistri = [Sim.dem.hourDistri
    cellfun(@(x,y) tline(x+1:y-1),...
    num2cell(k(1:2:end)),num2cell(k(2:2:end)),'UniformOutput',false)];
Sim.dem.hourDistri = Sim.dem.hourDistri.';
frewind(fileID);

%% Read the traffic class properties
% The list of mobility classes
Sim.dem.mobiClasses = { 'popu'
                        'DELN'
                        'DELL'
                        'pedeStat'
                        'lowMobi'
                        'hiMobi'};

% The list of the demand sources
Sim.dem.demSources = { 'popu'
                       'DELN'
                       'DELL'
                       'Motorway'
                       'A_road'
                       'B_road'
                       'Minor_road'
                       'Railway'
                       'Tunnelled_railway'};

% The list of families of use cases
Sim.dem.famUseCase_ = listFamUseCases;
assert(all(ismember(Sim.dem.famUseCase_,Sim.propag.UE(:,5))),...
    'There is a family that is not in the 5th column of Sim.propag.UE')

%% Importing the Use Cases' data
% fprintf(1,'Importing the files...');
for ii = listFamUseCases
    curNames = fieldnames(Sim.dem.filenames.(ii{:}));
        
    for jj = curNames'
        curFile = Sim.dem.filenames.(ii{:}).(jj{:});
        
        switch jj{:}
            case 'Mean'
                % This is the mean traffic per year for each family of use case
                [~,~,data] = xlsread(curFile);                
                data = ReplaceCell(data,'N/A',0);
                
                Sim.dem.trafVol.(ii{:}).mean.year = cell2mat(data(2,4:end));
                Sim.dem.trafVol.(ii{:}).mean.GbPerMonthPerSqKm = cell2mat(data(3,4:end));
                Sim.dem.trafVol.(ii{:}).mean.areThatTheMeaAbovCorre = data{5,4};
                Sim.dem.trafVol.(ii{:}).mean.popuThatTheMeaAbovCorre = data{6,4};
                Sim.dem.trafVol.(ii{:}).mean.ratOfWorkDayPopuOveResidentiPopuInStudyAre = data{7,4};
                Sim.dem.trafVol.(ii{:}).mean.downlinOveUplinRat = data{8,4};
                
                switch data{9,4}
                    case 'Focus + buffer'
                        Sim.dem.trafVol.(ii{:}).mean.inPoly = 'INbuf';
                    case 'Hamburg Port Authority'
                        Sim.dem.trafVol.(ii{:}).mean.inPoly = 'INhpa';
                    case 'AGV'
                        Sim.dem.trafVol.(ii{:}).mean.inPoly = 'INagv';
                    case 'cruShipTer'
                        Sim.dem.trafVol.(ii{:}).mean.inPoly = 'INcruShipTer';
                    case 'CCTV'
                        Sim.dem.trafVol.(ii{:}).mean.inPoly = 'INcctv';
                    case 'AR'
                        Sim.dem.trafVol.(ii{:}).mean.inPoly = 'INar';
                    otherwise
                        error('Undefined')
                end
            case 'Ratio'
                % There are five (5) ratio entries, which are the ratios between the mobility
                % classes (i.e. SME/HOME, LARGE SME/SME etc.). Every column in the ratio table
                % corresponds an hour in the day and every row corresponds to a traffic class.
                [~,~,data] = xlsread(curFile);                
                data = ReplaceCell(data,'N/A',0);
                
                Sim.dem.trafVol.(ii{:}).ratio.hour = data(3,5:end);
                Sim.dem.trafVol.(ii{:}).ratio.smeToHome = cell2mat(data(6:10,5:end));
                Sim.dem.trafVol.(ii{:}).ratio.largeToSme = cell2mat(data(13:17,5:end));
                Sim.dem.trafVol.(ii{:}).ratio.pedeToLowMobi = cell2mat(data(20:24,5:end));
                Sim.dem.trafVol.(ii{:}).ratio.lowMobiToHiMobi = cell2mat(data(27:31,5:end));
                Sim.dem.trafVol.(ii{:}).ratio.inToOutdoor = cell2mat(data(34:38,5:end));
                
                assert(strcmp(DataHash(Sim.dem.hourDistri(:,2).'),...
                    DataHash(Sim.dem.trafVol.(ii{:}).ratio.hour)))
            case 'Distribution'
                % Distribution amongst users
                [~,~,data] = xlsread(curFile);
                data = ReplaceCell(data,'N/A',nan);
                
                Sim.dem.trafVol.(ii{:}).distri.year = cell2mat(data(5,2:end));                
                
                m = 0;
                for demSour = Sim.dem.demSources'
                    
                    Sim.dem.trafVol.(ii{:}).distri.(demSour{:}).perOfAdd = cell2mat(data(6 + 55 * m :56 + 55 * m,1));
                    Sim.dem.trafVol.(ii{:}).distri.(demSour{:}).perOfDat = cell2mat(data(6 + 55 * m :56 + 55 * m,2:end));
                    m = m + 1;
                end
            case 'SubscriberPenetration'
                % There are two customers, and every column is per year, while each column
                % correspond to a specific case.
                % The traffic percentage is the percentage of traffic volume for each mobility class
                % that is attributed to that operator and that year.
                % The subscription percentage is the percentage of subcribers that this operator has
                % at each year for each demand source.
                [~,~,data] = xlsread(curFile);
                data = ReplaceCell(data,'N/A',nan);
                
                n_ = find(cellfun(@(x) ~isnan(x(1)),data(1,:)));
                
                for n = 1 : length(n_)
                    start = n_(n);
                    if n < length(n_)
                        fini = n_(n+1) - 1;
                    else
                        fini = size(data,2);
                    end
                    
                    Sim.dem.trafVol.(ii{:}).subPenet.(data{1,start}).year = ...
                        cell2mat(data(2,start:fini));
                    Sim.dem.trafVol.(ii{:}).subPenet.(data{1,start}).trafPerc = ...
                        cell2mat(data(5:10,start:fini));
                    Sim.dem.trafVol.(ii{:}).subPenet.(data{1,start}).subPerc = ...
                        cell2mat(data(14:22,start:fini));
                    Sim.dem.trafVol.(ii{:}).subPenet.(data{1,start}).MScou = ...
                        cell2mat(data(26:34,start:fini));
%                     if all(all(cell2mat(data(17:22,start:fini))==1,1),2) || ...
%                             all(all(cell2mat(data(17:22,start:fini))==0,1),2)
%                     else
%                         beep
%                         disp('Autocorrelation in service should be considered')
%                         keyboard
%                     end
                end
                
                % The list of operators
                Sim.dem.operators = data(1,n_);
            case 'CellEdgeThroughput'
                % The columns correspond to the traffic classes, while the rows correspond to users
                % (?)
                [~,~,data] = xlsread(curFile);
                
                Sim.dem.trafVol.(ii{:}).CEtput.unit = data{3,3};
                
                data = ReplaceCell(data,'N/A',nan);
             
                Sim.dem.trafVol.(ii{:}).CEtput.yea = cell2mat(data(1,3:end));
                Sim.dem.trafVol.(ii{:}).CEtput.DL = cell2mat(data(4:9,3:end));
                Sim.dem.trafVol.(ii{:}).CEtput.UL = cell2mat(data(10:15,3:end));
                Sim.dem.trafVol.(ii{:}).CEtput.laten = cell2mat(data(19,3:7));
                Sim.dem.trafVol.(ii{:}).CEtput.celAreConfLeve = cell2mat(data(21,3:end));
                Sim.dem.trafVol.(ii{:}).CEtput.A = cell2mat(data(22,3:7));
                Sim.dem.trafVol.(ii{:}).CEtput.PER = cell2mat(data(23,3:7));
                Sim.dem.trafVol.(ii{:}).CEtput.ServCouMax = cell2mat(data(24,3:7));
                Sim.dem.trafVol.(ii{:}).CEtput.isTxMultiCopiesDataToAllServ = cell2mat(data(25,3:7));
                Sim.dem.trafVol.(ii{:}).CEtput.servFacto = cell2mat(data(26,3:7));
                Sim.dem.trafVol.(ii{:}).CEtput.facto4spaSlot4lowLaten = cell2mat(data(27,3:7));
                
                assert(all(Sim.dem.trafVol.(ii{:}).CEtput.PER<1),...
                    'PER<1')
            case 'HourDistribution'
                % Each row corresponds to a traffic class, while each row corresponds to an hour
                % between 00 to 24 
                
                % Get the extension of curFile
                [~,~,ext] = fileparts(curFile);
                
                switch ext
                    case {'.xls','.xlsx','.csv'}
                        [~,~,data] = xlsread(curFile);
                        data = ReplaceCell(data,'N/A',nan);
                        
                        assert(strcmp(DataHash(data(3,3:end)),...
                            DataHash(Sim.dem.hourDistri(:,2).')))
                        Sim.dem.trafVol.(ii{:}).hourDistri =...
                            cell2mat(data(4:end,3:end));
                    case '.jpg'
                        % For eMBB the input is an image that was used
                        % to apportion the demand into the modelled
                        % hours and traffic classes. This image was
                        % digitised for increased speed and saved under
                        % ProgramData as DistTraffic.mat. It contains a
                        % Mx24 matrix ProgramData, where M is the number of
                        % traffic classes and 24 are the hours per day. The
                        % values of DistTraffic are proportions of the
                        % daily volume associated with a particular traffic
                        % class in each hour of the day.
                        try
                            load(fullfile(Sim.path.ProgramData,...
                                'DistTraffic.mat'),'DistTraffic')
                        catch
                            DistTraffic = GetDigitalImage(Sim,true);
                            
                            save(fullfile(Sim.path.ProgramData,...
                                'DistTraffic.mat'),'DistTraffic')
                        end
                        
                        % Save in Sim.dem.trafVol only the hours of the day
                        % that we have chosen to model
                        [~,Locb] = ismember(Sim.dem.hourDistri(:,2),...
                            DistTraffic(1,2:end));
                        Sim.dem.trafVol.(ii{:}).hourDistri =...
                            cell2mat(DistTraffic(2:end,1+Locb));
                end
            case 'devicPerDemSour'
                [~,~,data] = xlsread(curFile);
                data = ReplaceCell(data,'FALSE',0);
                data = ReplaceCell(data,'TRUE',1);
                
                Sim.dem.trafVol.(ii{:}).devic.pwrAmpClas = data(1,2:end);
                Sim.dem.trafVol.(ii{:}).devic.situ = data(2,2:end);
                Sim.dem.trafVol.(ii{:}).devic.perDemSour = cell2mat(data(3:11,2:end));
                Sim.dem.trafVol.(ii{:}).devic.lifet = cell2mat(data(13,2:end));
            case 'mobiCla'
                % Read mobility class vs thoroughfare type conversion table
                [~,~,data] = xlsread(curFile);

                data = ReplaceCell(data,'N/A',nan);

                m_ = 2 : 10 : size(data,1)-2;
                Sim.dem.mobiToDemSour.(ii{:}).assoc = cell(length(m_),1);
                for m = 1 : length(m_)
                    Sim.dem.mobiToDemSour.(ii{:}).assoc{m} = ...
                        cell2mat(data(m_(m)+3:m_(m)+8,4:12));
                end

                assert(strcmp(data{22,2},'handh') && ...
                    strcmp(data{23,2},'inVeh') && ...
                    strcmp(data{30,3},'hiMobi') && ...
                    strcmp(data{24,8},'A road') && ...
                    strcmp(data{24,9},'B road') && ...
                    strcmp(data{24,10},'Minor road'),...
                    'mobiCla file format changed; reflect in code')
                assert(Sim.dem.mobiToDemSour.(ii{:}).assoc{3}(6,5)==0 && ...
                    Sim.dem.mobiToDemSour.(ii{:}).assoc{3}(6,6)==0 && ...
                    Sim.dem.mobiToDemSour.(ii{:}).assoc{3}(6,7)==0,...
                    'hiMobi should not be impacted by POI weights')
                
                Sim.dem.mobiClaVel = cell2mat(data(5:10,14));
                
                carRatio = cell2mat(data(73,4:12));
                carRatio(isnan(carRatio)) = 0;
                Sim.dem.mobiToDemSour.carRatio = carRatio;
                
                % For each mobility class, find the power amplifier class and situation
                % of the devices that are in this family of use cases
                IND = cell(size(Sim.dem.mobiClaVel,1),1);
                for n = 1 : size(Sim.dem.mobiClaVel,1)
                    %         IND{n} = any(Sim.dem.trafVol.(fam).devic.perDemSour(...
                    %             Sim.dem.mobiToDemSour.(fam).assoc{}(n,:)>0,:) >= ...
                    %             Sim.dem.mobiClaVel(n),1);
                    IND{n} = any(Sim.dem.trafVol.(ii{:}).devic.perDemSour >= ...
                        Sim.dem.mobiClaVel(n) & cell2mat(cellfun(@(x) x(n,:)>0,...
                        Sim.dem.mobiToDemSour.(ii{:}).assoc,'UniformOutput',false)).',1);
                end
                Sim.dem.trafVol.(ii{:}).bund2 = IND;
            otherwise
                fgjgfjnty
                % If it not from any above just import the raw file
                [~,~,Sim.dem.trafVol.(ii{:}).(jj{:})] = xlsread(curFile);
        end                
    end
end

% Read MODEL_ALL_TRAFFIC_CLASSES_AS
tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),...
        'MODEL_ALL_TRAFFIC_CLASSES_AS')
    tline = fgetl(fileID);
    
    % If we reach the eof without that section then we break
    if tline == -1; break; end;     
end
tline = fgetl(fileID);
for fn = 1 : length(listFamUseCases)
    IND = strcmp(tline,Sim.dem.trafClas_);
    Sim.dem.trafVol.(listFamUseCases{fn}).hourDistri(IND,:) = ...
        sum(Sim.dem.trafVol.(listFamUseCases{fn}).hourDistri,1);
    Sim.dem.trafVol.(listFamUseCases{fn}).hourDistri(~IND,:) = 0;
    Sim.dem.modeAlTrafClasAs = IND;
end
frewind(fileID);

% Print
fclose(fileID);

% Validation of input
% useCases = fieldnames(Sim.dem.trafVol)';
% for uu = useCases
%     if ~any(Sim.yea.thisYea == Sim.dem.trafVol.(uu{:}).mean.year)
%         error(['This year (', num2str(m),') is missing from the mean of ', uu{:}])
%     end
% 
%     if ~any(Sim.yea.thisYea == Sim.dem.trafVol.(uu{:}).distri.year)
%         error(['This year (', num2str(m),') is missing from the distribution of ', uu{:}])
%     end
% 
%     customers = fieldnames(Sim.dem.trafVol.(uu{:}).subPenet)';
% 
%     for cc = customers
%         if ~any(Sim.yea.thisYea == Sim.dem.trafVol.(uu{:}).subPenet.(cc{:}).year)
%             error(['This year (', num2str(m),') is missing from the subscription penetration of ',...
%                 cc{:}])
%         end
%     end
% end
% clear useCases uu customers cc

%% Read link budget parameters
[~,~,raw] = xlsread(Sim.dem.filenames.linBud);

Sim.linBud.BS.pwrAmpClas_ = {};
Sim.linBud.BS.situ_ = {};
Sim.linBud.UE.pwrAmpClas_ = {};
Sim.linBud.UE.situ_ = {};

Sim.linBud.str = raw(6:18,3);

% For each column until the last non-NaN entry, []
for m = 6 : find(~cellfun(@(x) ~ischar(x)&&isnan(x),raw(2,:)),1,'last')
    % Link budget parameters variable with the frequency band and tranceiver power amplifier class
    for n = 6 + (0:length(Sim.linBud.str)*5-1)
        n2 = fix((n-6)./length(Sim.linBud.str)).*length(Sim.linBud.str) + 6;
        Sim.linBud.(raw{2,m}).(raw{3,m}).(raw{n2,2}).(raw{n,3}) = raw{n,m};
    end

    % Link budget parameters variable with the frequency band, tranceiver power amplifier class and geotype
    tmp1 = length(Sim.linBud.str)*5 + 8;
    tmp2 = length(Sim.linBud.str) * 2;
    for n = tmp1 + (0:4*length(Sim.linBud.str)-1)
        n2 = fix((n-tmp1)./tmp2).*tmp2 + tmp1;
        n3 = fix((n-tmp1)./2).*2 + tmp1;
        Sim.linBud.(raw{2,m}).(raw{3,m}).(raw{n2,2}).(raw{n3,3}).(raw{n,4}) = raw{n,m};
    end

    % Link budget parameters variable with demand class and tranceiver power amplifier class
    for n = 127 : 131
        n2 = 127;
        Sim.linBud.(raw{2,m}).(raw{3,m}).(raw{n2,2}).(raw{n,3}) = raw{n,m};
    end

    % Link budget parameters variable with tranceiver power amplifier class
    for n = 134 : 138
        Sim.linBud.(raw{2,m}).(raw{3,m}).(raw{n,2}) = raw{n,m};
    end
    if ~isnan(raw{n,m})
        Sim.linBud.BS.pwrAmpClas_ = [Sim.linBud.BS.pwrAmpClas_ raw{2,m}];
        Sim.linBud.BS.situ_ = [Sim.linBud.BS.situ_ raw{3,m}];
    else
        Sim.linBud.UE.pwrAmpClas_ = [Sim.linBud.UE.pwrAmpClas_ raw{2,m}];
        Sim.linBud.UE.situ_ = [Sim.linBud.UE.situ_ raw{3,m}];
    end
    
    % Link budget parameters variable with the simulation year
    n = 141;
    if ~isnan(raw{n,m})
        k2 = find(cellfun(@ischar,raw(n+0:end,3)),1,'first') - 2;
        star = find(cell2mat(raw(n+(0:find(cellfun(@isnan,raw(n+(0:k2),...
            3)),1,'first')-2),3)) == Sim.yea.start);
        Sim.linBud.(raw{2,m}).(raw{3,m}).([raw{n,2} 'Yea']) = ...
            cell2mat(raw(n+star-1+(0:Sim.yea.fin-Sim.yea.start),3));
        
        Sim.linBud.(raw{2,m}).(raw{3,m}).(raw{n,2}) = raw(n + star-1 + ...
            (0:Sim.yea.fin-Sim.yea.start),m);
        assert(length(unique(Sim.linBud.(raw{2,m}).(raw{3,m}).(...
            raw{n,2})))==1,'Unfinished code from accu_into_pix onwards')
    end
end
Sim.linBud.UE.pwrAmpClasUniq_ = unique(Sim.linBud.UE.pwrAmpClas_,'stable');
clear m n n2 n3

% Link budget parameters variable with the frequency band
n1 = find(strcmp(raw(:,1),...
    'Link budget parameters variable with the frequency band')) + 1;
n2 = find(cellfun(@(x) isnan(x(1)),raw(n1:end,2)),1,'first') + n1 - 2;
for n = n1 : length(Sim.linBud.str) : n2
    Sim.linBud.(raw{n,2}) = raw(n+(0:length(Sim.linBud.str)-1),6);
    if ~any(cellfun(@ischar,Sim.linBud.(raw{n,2})))
        Sim.linBud.(raw{n,2}) = cell2mat(Sim.linBud.(raw{n,2}));
    else
        Sim.linBud.(raw{n,2}) = Sim.linBud.(raw{n,2});
    end
end
clear n

if Sim.servDem.isInterfeExpl.cov && ...
        any([Sim.linBud.PRButilisCovera;Sim.linBud.PRButilisCapa]~=1,1)
    beep
    ButtonName = questdlg_timer(15,...
        'Did you mean to run with capping max cov/capa?', ...
        'Confirm question','Confirm','Cancel','Confirm');
    switch ButtonName
        case 'Confirm'
        case 'Cancel'
            error('Exit requested by the user')
    end
end

% Link budget parameters variable with communication direction
n1 = find(strcmp(raw(:,1),...
    'Link budget parameters variable with communication direction')) + 1;
n2 = find(cellfun(@(x) isnan(x(1)),raw(n1:end,2)),1,'first') + n1 - 2;
for n = n1 : 2 : n2
    Sim.linBud.(raw{n,2}) = cell2mat(raw(n+(0:1),6));
end
clear n

% Link budget parameters variable with MIMO order
n1 = find(strcmp(raw(:,1),...
    'Link budget parameters variable with MIMO order')) + 2;
for n = n1 : 10 : n1+10*4-1
    Sim.linBud.SEmean.(raw{n,3}).(raw{n,4}) = raw(n+1:n+9,5:10);
end
clear n

% Link budget parameters variable with mobility
% Sim.linBud.

clear raw

% %% Read cell edge throughput
% [~,~,raw] = xlsread(Sim.dem.filenames.CEtput);
% 
% for m = 5 : size(raw,2)
%     for n = 5 : 40
%         n2 = fix((n-5)./12).*12 + 5;
%         n3 = fix((n-5)./6).*6 + 5;
%         Sim.linBud.SUth.(raw{2,m}).(raw{n2,2}).(raw{n3,3}).(raw{n,4}) = raw{n,m};
%     end
% end
%     
% clear raw m n
% 
% %% Read device capabilities
% [~,~,Sim.dem.devicPerDemSour.BB] = ...
%     xlsread(Sim.dem.filenames.BB.devicPerDemSour);
% [~,~,Sim.dem.devicPerDemSour.I2V] = ...
%     xlsread(Sim.dem.filenames.I2V.devicPerDemSour);

%% Read cost information
fileID = fopen(Sim.dem.filenames.cost);

Sim.cost.spatBeamAndMIMOstreaCou = read_tab(fileID,...
    'SPATIAL_BEAM_COUNT_AND_MIMO_STREAM_COUNT',true,'%f %f %f');
Sim.cost.procImprov = read_tab(fileID,'PROCESSOR_IMPROVEMENT',false,'%f');
Sim.cost.disc = read_tab(fileID,'DISCOUNT_RATE',false,'%f');

tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),'START')
    tline = fgetl(fileID);
end
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),...
        'TRANSPORT')
    tline = fgetl(fileID);
end
tline = fgetl(fileID);
N = str2double(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1));
C = {};
tline = fgetl(fileID);
while ~all(isstrprop(tline,'wspace'))
    C = [C; textscan(tline,['%f %f ' repmat('%s ',1,N)])];
    tline = fgetl(fileID);
end
Sim.cost.tr = struct('lim1',C(:,1),'lim2',C(:,2));
v = cellfun(@(x) x,C(:,3:end));
for n = 1 : size(C,1)
    Sim.cost.tr(n).v = v(n,:);
end
frewind(fileID)

Sim.cost.capex.antenSitNew = read_tab(fileID,'CAPEX_ANTENNA_SITE_NEW',true,...
    '%s %f %f %f %f %f %f %f');
Sim.cost.capex.antenSitUpg = read_tab(fileID,'CAPEX_ANTENNA_SITE_UPGRADE_AND_REFRESH',true,...
    '%s %f %f %f %f %f %f %f');
Sim.cost.capex.antenSitUpgLab = read_tab(fileID,'CAPEX_ANTENNA_SITE_UPGRADE_AND_REFRESH_LABOUR',true,...
    '%s %f %f %f');
Sim.cost.capex.edgCloSitUpgLab = read_tab(fileID,'CAPEX_EDGE_CLOUD_SITE_UPGRADE_AND_REFRESH_LABOUR',true,...
    '%f %f %f');
Sim.cost.opex.antenSit = read_tab(fileID,'OPEX_ANTENNA_SITE',true,...
    '%s %f %f %f %f %s %s %f %f');
Sim.cost.capex.backhauNew = read_tab(fileID,'CAPEX_BACKHAUL_NEW',true,...
    '%s %f %f %f %f');
Sim.cost.capex.backhauUpgTyp = read_tab(fileID,'CAPEX_BACKHAUL_UPGRADE_AND_REFRESH_TYPE',true,...
    '%s %s %s');
Sim.cost.capex.backhauUpgBW = read_tab(fileID,'CAPEX_BACKHAUL_UPGRADE_AND_REFRESH_BW',true,...
    '%f %f %f %f %f');
Sim.cost.opex.backhau = read_tab(fileID,'OPEX_BACKHAUL',true,...
    '%s %f %f %f %f');
Sim.cost.capex.antenFeede = read_tab(fileID,'CAPEX_ANTENNA_FEEDER',true,...
    '%s %f %f %f %f %f %f %f');
Sim.cost.capex.RFfronEnd = read_tab(fileID,'CAPEX_RF_FRONT_END',true,...
    ['%s %f ' repmat('%f ',1,length(Sim.linBud.str)) '%f %f %f']);
Sim.cost.capex.sitCivWoAnAcq = read_tab(fileID,...
    'SITE_CIVIL_WORKS_AND_ACQUISITION',true,'%s %f %f %f');
Sim.cost.capex.BBbareMeta = read_tab(fileID,...
    'CAPEX_BASEBAND_BARE_METAL',true,'%s %f %f %f %f %f');
Sim.cost.capex.edgCloNodeAnEdgCloSit = read_tab(fileID,...
    'CAPEX_EDGE_CLOUD_NODE_AND_EDGE_CLOUD_SITE',true,'%s %f %f %f');
Sim.cost.opex.edgCloSit = read_tab(fileID,...
    'OPEX_EDGE_CLOUD_SITE',true,'%f %f %f %f %s %s %f %f');
Sim.cost.capex.antenSitRepl = read_tab(fileID,...
     'CAPEX_ANTENNA_SITE_REPLACEMENT',true,'%f %f %f %f %f %f');
Sim.cost.capex.edgCloSitRepl = read_tab(fileID,...
     'CAPEX_EDGE_CLOUD_SITE_REPLACEMENT',true,'%f %f %f %f');
Sim.cost.anten = read_tab(fileID,'ANTEN',true,...
    repmat('%d ',1,length(Sim.linBud.str)));

Sim.cost.anten = logical(cell2mat(struct2cell(Sim.cost.anten))).';

fclose(fileID);
clear fileID

%% Read rulebook (upgrade) strategy
% Read all the file
[~,~,raw] = xlsread(Sim.dem.filenames.rulebook);

% Read Existing table
Start = find(strcmp(raw(:,1),'Existing')) + 4;
fini = Start-2 + find(cellfun(@(x) all(isnan(x)),raw(Start:end,2)),1);
L = length(Sim.linBud.str);
curre = struct('sitChainConfig',raw(Start:fini,5),...
    'sectoCou',raw(Start:fini,6),...
    'AeCou',num2cell(cell2mat(raw(Start:fini,6+(1:L))),2),...
    'Band',num2cell(cell2mat(raw(Start:fini,6+L+(1:L))),2));
upg = struct('sitChainConfig',raw(Start:fini,7+2*L),...
    'sectoCou',raw(Start:fini,8+2*L),...
    'AeCou',num2cell(cell2mat(raw(Start:fini,8+2*L+(1:L))),2),...
    'Band',num2cell(cell2mat(raw(Start:fini,8+3*L+(1:L))),2));
Sim.exiCel.rul = struct('rulID',raw(Start:fini,2),...
    'pwrAmpClas',raw(Start:fini,3),...
    'isUpg',raw(Start:fini,4),...
    'ValidFrom',raw(Start:fini,9+4*L),...
    'ValidTo',raw(Start:fini,10+4*L));
for n = 1 : length(Sim.exiCel.rul)
    Sim.exiCel.rul(n).curre = curre(n);
    Sim.exiCel.rul(n).curreHash = DataHash(Sim.exiCel.rul(n).curre);
    Sim.exiCel.rul(n).upg = upg(n);
    Sim.exiCel.rul(n).upgHash = DataHash(Sim.exiCel.rul(n).upg);
end

% Read New table
Start = find(strcmp(raw(:,1),'New')) + 4;
fini = Start-2 + find(cellfun(@(x) all(isnan(x)),raw(Start:end,2)),1);
curre = struct('sitChainConfig',raw(Start:fini,5),...
    'sectoCou',raw(Start:fini,6),...
    'AeCou',num2cell(cell2mat(raw(Start:fini,6+(1:L))),2),...
    'Band',num2cell(cell2mat(raw(Start:fini,6+L+(1:L))),2));
upg = struct('sitChainConfig',raw(Start:fini,7+2*L),...
    'sectoCou',raw(Start:fini,8+2*L),...
    'AeCou',num2cell(cell2mat(raw(Start:fini,8+2*L+(1:L))),2),...
    'Band',num2cell(cell2mat(raw(Start:fini,8+3*L+(1:L))),2));
Sim.rul = struct('rulID',raw(Start:fini,2),...
    'pwrAmpClas',raw(Start:fini,3),...
    'isUpg',raw(Start:fini,4),...
    'ValidFrom',raw(Start:fini,9+4*L),...
    'ValidTo',raw(Start:fini,10+4*L));
upg_ = raw(Start:fini,:);

tmp = cell2mat(arrayfun(@(x) xor(x.AeCou>0,x.Band>0),upg,...
    'UniformOutput',false));
if any(any(tmp,2),1)
    disp(tmp)
    error(['Check the rulebook file. There are some bands with BW and ' ...
        'no antennas, or vice versa. Check the trues above.'])
end
clear tmp

% Read Upgrades and Refreshed
Start = find(strcmp(raw(:,1),'Upgrades and Refreshed')) + 2;
fini = size(raw,1);
assert(all(strcmp(raw(Start:fini,4),'all')),'Only ''all'' is supported currently')
for n = 1 : fini-Start+1
    IND = strcmp(upg_(:,3),raw{Start+n-1,3});
%     assert(~any(cell2mat(raw(IND,3))),'The rulebook was requested to do all combinations of upgrades, but was also provided with a list of upgrades?')
    assert(issorted(cell2mat(upg_(IND,9+4*L))),'Code so that unsorted rules can be valid here')
    
    m_ = find(IND);
    for m1 = 1 : length(m_)
        for m2 = m1+1 : length(m_)
            curre(length(curre)+1) = struct('sitChainConfig',upg_(m_(m1),7+2*L),...
                'sectoCou',upg_(m_(m1),8+2*L),...
                'AeCou',num2cell(cell2mat(upg_(m_(m1),8+2*L+(1:L))),2),...
                'Band',num2cell(cell2mat(upg_(m_(m1),8+3*L+(1:L))),2));
            upg(length(upg)+1) = struct('sitChainConfig',upg_(m_(m2),7+2*L),...
                'sectoCou',upg_(m_(m2),8+2*L),...
                'AeCou',num2cell(cell2mat(upg_(m_(m2),8+2*L+(1:L))),2),...
                'Band',num2cell(cell2mat(upg_(m_(m2),8+3*L+(1:L))),2));
            assert(~isequal(curre(end),upg(end)),'Tautology in rule book detected')
            Sim.rul(length(Sim.rul)+1) = struct('rulID',Sim.rul(end).rulID+1,...
                'pwrAmpClas',raw{Start+n-1,3},...
                'isUpg',true,...
                'ValidFrom',upg_(m_(m2),9+4*L),...
                'ValidTo',upg_(m_(m2),10+4*L));
        end
    end
end

for n = 1 : length(Sim.rul)
    Sim.rul(n).curre = curre(n);
    Sim.rul(n).curreHash = DataHash(Sim.rul(n).curre);
    Sim.rul(n).upg = upg(n);
    Sim.rul(n).upgHash = DataHash(Sim.rul(n).upg);
end

clear curre upg

%% Append rulebook with reinstatement of expiring sites
tmp2 = Sim.rul;
tmp = num2cell(100+(1:length(Sim.rul)));
[tmp2.rulID] = deal(tmp{:});
[Sim.rul(1:length(Sim.rul)).isDecomm] = deal(false);
[tmp2.isDecomm] = deal(true);

for n = 1 : length(tmp2)
    if isnan(tmp2(n).curre.sitChainConfig)
        tmp2(n).curre = tmp2(n).upg;
        tmp2(n).curreHash = tmp2(n).upgHash;
    end
end

Sim.rul = [Sim.rul; tmp2];

%% Ensure that the BW is never reduced
curre = cell2mat(arrayfun(@(x) x.curre.Band,Sim.rul,...
    'UniformOutput',false));
upg = cell2mat(arrayfun(@(x) x.upg.Band,Sim.rul,...
    'UniformOutput',false));
Sim.rul(any(curre>upg,2)) = [];
curre = cell2mat(arrayfun(@(x) x.curre.AeCou,Sim.rul,...
    'UniformOutput',false));
upg = cell2mat(arrayfun(@(x) x.upg.AeCou,Sim.rul,...
    'UniformOutput',false));
Sim.rul(any(curre>upg,2)) = [];

%% Check if there are rulebook paths where the sites downgrade
curre = cell2mat(arrayfun(@(x) x.curre.Band,Sim.rul,...
    'UniformOutput',false));
upg = cell2mat(arrayfun(@(x) x.upg.Band,Sim.rul,...
    'UniformOutput',false));

% Refresh to same specs
isRefr = any(curre==upg,2);

% More bands
isMoreBand = any(curre==0&upg>0,2);

% More spectrum in existing bands
isMoreBWinExisBand = any(curre~=upg,2) & ~isMoreBand;

assert(~any(~(isMoreBand|isMoreBWinExisBand) & ~[Sim.rul.isUpg].' & ...
    ~isRefr),'Check why here!')

%% Read the frame structure for overheads and TDD configurations
[~,~,raw] = xlsread(Sim.linBud.framStru.filenames);
 
raw = ReplaceCell(raw,'any',nan);

% framStru
Sim.linBud.framStru.DL{1} = cell2mat(raw(4:6,4:end));
Sim.linBud.framStru.DL{2} = cell2mat(raw(8:10,4:end));
Sim.linBud.framStru.DL{3} = cell2mat(raw(12:14,4:end));
Sim.linBud.framStru.UL = raw{16,4};
 
% TDDcfg
Sim.linBud.TDDconfig_ = cell2mat(raw(21:27,[2,4:7]));

%% Read the sharing information
[~,~,raw] = xlsread(Sim.shari.filenames);
Sim.shari.from = raw{1,1};
assert(strcmp(DataHash(raw(4:end,2).'),DataHash(Sim.dem.operators)))
assert(strcmp(DataHash(raw(3,3:end)),DataHash(Sim.dem.operators)))
Sim.shari.is = logical(cell2mat(raw(4:end,3:end)));
clear raw

%% Read the channel raster information
[~,~,Sim.linBud.chRaste] = xlsread(Sim.linBud.filenames.chRaste);
for n = 1 : length(Sim.dem.famUseCase_)
    if ~any(strcmp(Sim.linBud.chRaste(:,4),Sim.dem.famUseCase_{n}))
        error([Sim.dem.famUseCase_{n} ...
            ' family was not found in channel raster'])
    end
end

Sim.linBud.chRasteFind = cell2mat(Sim.linBud.chRaste(2,5:end));

Sim.linBud.isSupp = false(length(Sim.dem.famUseCase_),...
    length(Sim.linBud.spotFreq));
for fn = 1 : length(Sim.linBud.spotFreq)
    for famI = 1 : length(Sim.dem.famUseCase_)
        row = strcmp(Sim.linBud.chRaste(:,4),Sim.dem.famUseCase_{famI});
        col = Sim.linBud.chRasteFind == fn;
        Sim.linBud.isSupp(famI,fn) = any(any(~isnan(cell2mat(...
            Sim.linBud.chRaste(row,[false(1,4) col]))),2),1);
    end
end

%% Read the doppler information
[~,~,raw] = xlsread(Sim.linBud.filenames.dop);
Sim.linBud.dop = cell2mat(raw(3:end,5+(0:length(Sim.linBud.str)-1)));
clear raw

%% Read the resilience (computational availability) information
% Calcualtions were done in matlab in:
% D:\Dropbox (Real Wireless)\rW shared new\Bitbucket Local\KK\code-library\Resilience
% Sim.linBud.resi is an MxN matrix of resilience statistics, where M is
% defined by the number of rows in the csv that is being read
% 'Sim.linBud.filenames.resi', and N=3. The columns are:
% 1. Number of working servers
% 2. Number of spare servers needed for 99.9% service availability
% 3. Number of spare servers needed for 99.999% service availability
[~,~,raw] = xlsread(Sim.linBud.filenames.resi);
assert(nnz(Sim.dem.modeAlTrafClasAs)==1,'Amend this paragraph!')
for n = 1 : length(Sim.dem.famUseCase_)
    k = Sim.dem.trafVol.(Sim.dem.famUseCase_{n}).CEtput.A(...
        Sim.dem.modeAlTrafClasAs) == cell2mat(raw(2,:));
    assert(nnz(k)==1,['There is a service with resilience ' ...
        '(computational availability) requirement that is not ' ...
        'included within the precalculated file'])
    Sim.dem.trafVol.(Sim.dem.famUseCase_{n}).CEtput.resi = k(2:end);
end
Sim.linBud.resi = cell2mat(raw(3:end,2:3));
clear raw k n

%%
assert(nnz(Sim.dem.modeAlTrafClasAs)==1,...
    'Search for Sim.dem.modeAlTrafClasAs(trafClI) in all code!')

%% Read the information about site types
[~,~,raw] = xlsread(Sim.dem.filenames.sitTyp);
assert(isequal(Sim.linBud.BS.pwrAmpClas_,raw(2,4:end)),...
    'Check sitTyp file file')
assert(isequal(Sim.linBud.BS.situ_,raw(3,4:end)),...
    'Check sitTyp file file')
for n = 1 : length(Sim.linBud.BS.pwrAmpClas_)
    Sim.linBud.(Sim.linBud.BS.pwrAmpClas_{n}).(...
        Sim.linBud.BS.situ_{n}).maxSuppVel = raw{4,3+n};
end
clear raw n

%% Read the information about which operator and build infrastructure
[~,~,raw] = xlsread(Sim.dem.filenames.canBui);
N = length(Sim.linBud.BS.pwrAmpClas_);
assert(isequal(Sim.dem.operators,raw(3:N:end,2).'),...
    'Check canBui file file')
for n = 3 : N : size(raw,1)
    assert(isequal(Sim.linBud.BS.pwrAmpClas_,raw(n+(0:N-1),3).'),...
        'Check canBui file file')
end
for n = 1 : length(Sim.dem.operators)
    Sim.dem.operat(n).canBuiYea = cell2mat(raw(2,4:end));
    Sim.dem.operat(n).canBui = false(1,length(Sim.dem.operators));
    Sim.dem.operat(n).canBui = cell2mat(raw(3+N*(n-1)+(0:N-1),4:end));
end
clear N raw n

%% Read the information about interference suppression
[~,~,raw] = xlsread(Sim.dem.filenames.interfSuppr);
Sim.linBud.interfSupprYea = cell2mat(raw(3,2:end));
Sim.linBud.interfSuppr = cell2mat(raw(4,2:end));
clear raw

%% Find the service order
Sim = Sim.s_servOrd;

%% For each tenant, find if should be considered in merit
Sim.dem.consiInMeritPerTena = false(length(Sim.dem.operators),1);
for n = 1 : length(Sim.dem.operators)
    for m = 1 : length(Sim.dem.famUseCase_)
        if Sim.dem.consiInMerit.(Sim.dem.famUseCase_{m})
            if any(any(Sim.dem.trafVol.(Sim.dem.famUseCase_{m}...
                    ).subPenet.(Sim.dem.operators{n}).subPerc>0,2),1)
                Sim.dem.consiInMeritPerTena(n) = true;
            end
        end
    end
end

%% Print
% fprintf(1,'DONE\n');
end
    
function Sim = s_meteor(Sim,studyA)
%% Read meteorological data
switch Sim.propag.mod
    case 'ITU_1812_4'
        % Read DN50.TXT file
        fileID = fopen(fullfile(Sim.path.earth,...
            'R-REC-P.1812-3-201309-I!!ZIP-E','DN50.TXT'));
        Sim.DN50 = fscanf(fileID,'%f',[241 121]).';
        fclose(fileID);
        clear fileID
        
        % Read N050.TXT file
        fileID = fopen(fullfile(Sim.path.earth,...
            'R-REC-P.1812-3-201309-I!!ZIP-E','N050.TXT'));
        Sim.N050 = fscanf(fileID,'%f',[241 121]).';
        fclose(fileID);
        clear fileID
end

% Rainfall rate exceeded for the desired probability of exceedance  (mm/h)
assert(nnz(Sim.dem.modeAlTrafClasAs)==1,'Amend this paragraph!')
LatitudeLimits = [floor(studyA.buf.BoundingBoxLatLon(1,2)./0.25),...
    ceil(studyA.buf.BoundingBoxLatLon(2,2)./0.25)] .* 0.25;
LongitudeLimits = [floor(studyA.buf.BoundingBoxLatLon(1,1)./0.25),...
    ceil(studyA.buf.BoundingBoxLatLon(2,1)./0.25)] .* 0.25;
x = LongitudeLimits(1)+0.125 : 0.25 : LongitudeLimits(2)-0.125;
y = LatitudeLimits(2)-0.125 : -0.25 : LatitudeLimits(1)+0.125;
[lon,lat] = meshgrid(x,y);
Sim.Rp.R = georasterref(...
    'RasterSize',[diff(LatitudeLimits) diff(LongitudeLimits)]./0.25,...
    'RasterInterpretation','cells','ColumnsStartFrom','north',...
    'LatitudeLimits',LatitudeLimits,'LongitudeLimits',LongitudeLimits);
for famI = 1 : length(Sim.dem.famUseCase_)
    p = 100 - Sim.dem.trafVol.(Sim.dem.famUseCase_{famI}...
        ).CEtput.A(Sim.dem.modeAlTrafClasAs).*100;
 %   Sim.Rp.A.(Sim.dem.famUseCase_{famI}) = c_itu_837_7(lat,lon,p,Sim); %HO
 %   commented
end
end

function Sim = s_construct(Sim,tmp,isWzero,studyA)
%%
tmp2 = [tmp(isWzero).code].';
IND = any(bsxfun(@eq,[tmp.code],tmp2),1);
sz = [size(tmp(1).A,1) size(tmp(1).A,2) nnz(IND)];
Sim.construct = sum(reshape(full([tmp(IND).A]),sz),3) > 1;

% % Do not allow new sites on minor roads within Nature Reserve
% switch Sim.SA.name
%   case 'West Bank'
%     assert(strcmp(studyA.Oslo(3).CLASS,'Nature Reserve'))
%     Sim.construct = Sim.construct & ~studyA.INOsloPer(:,:,3);
%   case {'Gaza','Settlements'}
%   otherwise
%     error('Undefined')
% end
end

function Sim = s_construct2(Sim,dem,studyA)
%% Find pixels where cells can be commisioned
% []
Sim.construct = repmat(Sim.construct,[1 1 size(Sim.propag.BS,1)]);
for n = 1 : size(Sim.propag.BS,1)
    IND = find(ismember({dem.layNam},Sim.propag.BS{n,4}));        
    for m = 1 : length(IND)
        Sim.construct(:,:,n) = Sim.construct(:,:,n) | dem(IND(m)).A>0;
    end
end

%% For Palestine
% % Restrict new sites to only where Jawwal or Ooredoo sites are deployed
% switch Sim.BsExiFromSitefinder.(Sim.dem.operators{:})
%   case {'Jawwal','Ooredoo','Israeli'}
%   case {'Thaleth','Sc_5','Sc_6','Sc_14','Sc_15'}
%     Sim.construct(:) = false;
%     
%     switch Sim.SA.name
%       case 'West Bank'
%         % Read Jawwal sites
%         S = load(fullfile(Sim.path.on,'01 RW Shared - General','Projects',...
%             'UNOPS-OQ','J1002 2G-5G analysis for OQ','06 Runs','Sc_E3_508',...
%             'OutpAux','2f741d10c7afdc7bd9ef4b625a978644.mat'),'Cell');
%         linearInd = sub2ind(studyA.R.RasterSize,[S.Cell.I],[S.Cell.J]);
%         Sim.construct(linearInd) = true;
% 
%         % Read Ooredoo sites
%         S = load(fullfile(Sim.path.on,'01 RW Shared - General','Projects',...
%             'UNOPS-OQ','J1002 2G-5G analysis for OQ','06 Runs','Sc_E1_499',...
%             'OutpAux','82b3a3aa71a87d157287bc80daf752f3.mat'),'Cell');
%         linearInd = sub2ind(studyA.R.RasterSize,[S.Cell.I],[S.Cell.J]);
%         Sim.construct(linearInd) = true;
%       case 'Gaza'
%         % Read Jawwal sites
%         S = load(fullfile(Sim.path.on,'01 RW Shared - General','Projects',...
%             'UNOPS-OQ','J1002 2G-5G analysis for OQ','06 Runs','Sc_E12_502',...
%             'OutpAux','82b3a3aa71a87d157287bc80daf752f3.mat'),'Cell');
%         linearInd = sub2ind(studyA.R.RasterSize,[S.Cell.I],[S.Cell.J]);
%         Sim.construct(linearInd) = true;
% 
%         % Read Ooredoo sites
%         S = load(fullfile(Sim.path.on,'01 RW Shared - General','Projects',...
%             'UNOPS-OQ','J1002 2G-5G analysis for OQ','06 Runs','Sc_E10_501',...
%             'OutpAux','82b3a3aa71a87d157287bc80daf752f3.mat'),'Cell');
%         linearInd = sub2ind(studyA.R.RasterSize,[S.Cell.I],[S.Cell.J]);
%         Sim.construct(linearInd) = true;
%       otherwise
%         error('Undefined')
%     end
%     
%     Sim.construct(:,:,2) = Sim.construct(:,:,1);
%     Sim.construct(:,:,3) = Sim.construct(:,:,1);
%   otherwise
%     error('Undefined')
% end
% 
% % []
% if ~Sim.optimis.canBuiAreC
%     assert(strcmp(studyA.Oslo(4).CLASS,'Area C'))
%     Sim.construct = bsxfun(@and,~studyA.INOsloPer(:,:,4),Sim.construct);
% end
end

function Sim = c_propag_pix(Sim,dem,studyA,clu,col_Hata_branch)
%% [Title]
% Updates Sim.propag.pix with a table that contains the pixels where
% propagation is needed, i.e. where there are UE. This is done regradless
% of the years, in case the subscribers increase, so as to have all
% propagation information complete.
%
% * v0.1 KK 20Jan17 Created

% HO: This function populates the sim propagation pixels with x,y, row and
% columns, clutter type. Where you can have demand then you need
% propagation calc. Identify pixels where you need propagation, hence there
% is a demand.

% []
for Un = 1 : size(Sim.propag.UE,1)
    if any(strcmp(Sim.propag.UE{Un,5},Sim.dem.famUseCase_))
        % Initialise
        IN = logical(sparse(size(dem(1).A,1),size(dem(1).A,2)));

        % Find pixels of the demand sources that are associated with the this
        % UE type
        switch Sim.dem.trafVol.(Sim.propag.UE{Un,5}).mean.inPoly
            case {'INbuf','INhpa'}
                % Restrict on roads or addresses
                IND = find(ismember({dem.layNam},Sim.propag.UE{Un,4}{2}));
                for m = 1 : length(IND)
                    IN(dem(IND(m)).A>0) = true;
                end
            case {'INagv','INcruShipTer','INcctv','INar'}
                % Do nothing
            otherwise
                error('Insert this!')
        end

        % Restrict within HPA or other polygon, if needed
        switch Sim.propag.UE{Un,4}{1}{1}
            case 'INbuf'
                % Do nothing
            case 'INhpa'
                % Restrict within HPA
                IN = IN & studyA.INhpa;
            case 'INagv'
                % Restrict within AGV polygon
                for m = 1 : length(studyA.AGV)
                    IN = IN | studyA.(['INagv' num2str(m)]);
                end
            case 'INcruShipTer'
                % Restrict within cruShipTer polygon
                for m = 1 : length(studyA.cruShipTer)
                    IN = IN | studyA.(['INcruShipTer' num2str(m)]);
                end
            case 'INcctv'
                % Restrict within CCTV polygon
                IN = studyA.INhpa;
            case 'INar'
                % Restrict within AR polygon
                IN = studyA.INhpa;
            otherwise
                error('Insert this!')
        end

        % Restrict to a few pixels, if needed.
        % This is done here, and not by means of subPenet.csv, is because the
        % latter is expressed in %, rather than absolute numbers.
    %     if ischar(Sim.propag.UE{Un,4}{2})
    %         switch Sim.propag.UE{Un,4}{2}
    %             case 'all'
    %                 % Do nothing
    %             otherwise
    %                 error('Undefined')
    %         end
    %     elseif isnan(Sim.propag.UE{Un,4}{2})
    %         error('Undefined')
    %     else
    %         rng(Sim.seed.UEloc)
    %         randperm(nnz(IN>0),Sim.propag.UE{Un,4}{2})
    %         fghryt
    %     end

        % Store result in Sim.propag.pix
        [row,col] = find(IN>0);
        [x,y] = intrinsicToWorld(studyA.R,col,row);
        linearInd = sub2ind(size(dem(5).X),row,col);
        tmp = full(dem(5).X(linearInd));
        x(tmp>0) = tmp(tmp>0);
        tmp = full(dem(5).Y(linearInd));
        y(tmp>0) = tmp(tmp>0);
        Sim.propag.pix{Un} = [x y row col];
        
        % Store result in Sim.propag.pix - clu
        [row_clu,col_clu] = worldToSub(clu.R,x,y);
        Sim.propag.pix{Un}(:,end+1) = cell2mat(clu.tran.(...
            Sim.propag.UE{Un,3})(clu.A(sub2ind(size(clu.A),...
            row_clu,col_clu))+1,col_Hata_branch));

        % Correct the size to the expected 5 columns, if needed
        if all(size(Sim.propag.pix{Un})==[0,4])
            Sim.propag.pix{Un} = zeros(0,5);
        end
    else
        Sim.propag.pix{Un} = zeros(0,5);
    end
end
end

%% Calculate interference multipliers
function Sim = c_interf_multip(Sim,Cell)
assert(all(or([Cell.sectoCou]==2,[Cell.sectoCou]==3)),...
    'This paragraph will not work')

if exist(fullfile(Sim.path.ProgramData,'interfMultip.mat'),'file')
    % Load the pre-calculated interference multipliers
    load(fullfile(Sim.path.ProgramData,'interfMultip.mat'),'B');
else
    % Simplify
    utilisN = Sim.utilisN;

    % For each loading level of wanted and interfering cells, for each
    % sector within a site, for 2- and 3-sectored sites, select which PRB
    % will be utilised by the wanted and interfering cells pseudorandomly.
    % Repeat 10,000 times and calcualte the likelihood that a PRB chosen by
    % the wanted cell is also chosen by the interfering cell.
    A = (1./utilisN./2 : 1./utilisN : 1);
    B2 = zeros(10000,1);
    for n = 1 : utilisN
        for m = 1 : utilisN
            % For 2-sectored sites, calculate the likelihood that a PRB
            % chosen by the wanted cell is also chosen by the interfering
            % cell.
            for secW = 1 : 2
                for secI = 1 : 2
                    for k = 1 : 10000
                        % Select which PRB will be utilised by the
                        % interfering cell
                        Si = false(100,1);
                        if A(n) < 1./2
                            % If loading is up to 50%, then use spectrum
                            % within the first half for sectors with index
                            % 1, and the second half for sectors with index
                            % 2.
                            Si(randperm(50,round(A(n).*100))+(secI-1)*50) = true;
                        else
                            % If loading is beyond 50%, then use all the
                            % spectrum allocated to the sector (i.e. 0-50%
                            % for sectors with index 1, 50-100% for sectors
                            % with index 2) and then fullfill the remainder
                            % loading from the remainder spectrum with
                            % equal chance. For example, if loading is 75%
                            % and sector has index 1, then the PRB set is
                            % the union of [1,2,...,50] and a random
                            % selection of 25 PRB out of [51,100].
                            Si(1+(secI-1)*50:50+(secI-1)*50) = true;
                            IND = [1:(secI-1)*50, 50+(secI-1)*50+1:100];
                            Si(IND(randperm(50,round(A(n).*100-50)))) = true;
                        end

                        % Select which PRB will be utilised by the wanted
                        % cell
                        Sw = false(100,1);
                        if A(m) < 1./2
                            Sw(randperm(50,round(A(m).*100))+(secW-1)*50) = true;
                        else
                            Sw(1+(secW-1)*50:50+(secW-1)*50) = true;
                            IND = [1:(secW-1)*50, 50+(secW-1)*50+1:100];
                            Sw(IND(randperm(50,round(A(m).*100-50)))) = true;
                        end

                        % Calculate the likelihood that a PRB chosen by the
                        % wanted cell is also chosen by the interfering
                        % cell.
                        B2(k) = nnz(Si(Sw)) ./ 100;
                    end
                    B{2,secW}(m,n,secI) = mean(B2);
                end
            end

            % For 3-sectored sites, calculate the likelihood that a PRB
            % chosen by the wanted cell is also chosen by the interfering
            % cell.
            % Reminder of the format: B{secCou,secW}(loadW,loadI,secI).
            for secW = 1 : 3
                for secI = 1 : 3
                    for k = 1 : 10000
                        % Select which PRB will be utilised by the
                        % interfering cell
                        Si = false(99,1);
                        if A(n) < 1./3
                            Si(randperm(33,round(A(n).*99))+(secI-1)*33) = true;
                        else
                            Si(1+(secI-1)*33:33+(secI-1)*33) = true;
                            IND = [1:(secI-1)*33, 33+(secI-1)*33+1:99];
                            Si(IND(randperm(66,round(A(n).*99-33)))) = true;
                        end

                        % Select which PRB will be utilised by the wanted
                        % cell
                        Sw = false(99,1);
                        if A(m) < 1./3
                            Sw(randperm(33,round(A(m).*99))+(secW-1)*33) = true;
                        else
                            Sw(1+(secW-1)*33:33+(secW-1)*33) = true;
                            IND = [1:(secW-1)*33, 33+(secW-1)*33+1:99];
                            Sw(IND(randperm(66,round(A(m).*99-33)))) = true;
                        end

                        % Calculate the likelihood that a PRB chosen by the
                        % wanted cell is also chosen by the interfering
                        % cell.
                        B2(k) = nnz(Si(Sw)) ./ 99;
                    end
                    B{3,secW}(m,n,secI) = mean(B2);
                end
            end
        end
    end
    clear n A tmp B2 Si Sw secW secI

    save(fullfile(Sim.path.ProgramData,'interfMultip.mat'),'B')
    
%     {secCou,secW}(loadW,loadI,secI)
%     figure
%     for secW = 1 : 3
%         for secI = 1 : 3
%             subplot(3,3,(secW-1)*3+secI)
%             imagesc(Sim.interfMultip{3,secW}(:,:,secI))
%             set(gca,'ydir','normal','clim',[0 1])
%             daspect([1 1 1])
%             xlabel('loadI')
%             ylabel('loadW')
%             title(['secW=' num2str(secW) ', secI=' num2str(secI)])
%         end
%     end
    
    % % Raise-cosine filter response
    % % Beta = 0.00001;  % square
    % Beta = 0.25;
    % % Beta = 0.5;
    % H = @(f,T) 1.*(abs(f)<=(1-Beta)./2./T) + ...
    %     0.5 .* (1+cos(pi.*T./Beta.*(abs(f)-(1-Beta)./2./T))) .* ...
    %     ((1-Beta)./2./T<abs(f) & abs(f)<=(1+Beta)./2./T);
    % 
    % % figure
    % % hold on
    % % plot(linspace(-0.5,1.5),H(linspace(-0.5,1.5)-0.5./3,3))
    % % plot(linspace(-0.5,1.5),H(linspace(-0.5,1.5)-1.5./3,3))
    % % plot(linspace(-0.5,1.5),H(linspace(-0.5,1.5)-2.5./3,3))
    % % grid on
    % 
    % % figure
    % % hold on
    % % plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-0.333./2,1./0.333))
    % % plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-1.5./3,1./0.333))
    % % plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-(1-0.333./2),1./0.333))
    % % grid on
    % 
    % % figure
    % % hold on
    % % plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-0.8098./2,1./0.8098))
    % % plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-1.5./3,1./0.4806))
    % % plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-(1-0.3904./2),1./0.3904))
    % % grid on
    % 
    % % figure
    % % hold on
    % % plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-0.9500./2,1./0.9500))
    % % plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-(1-0.0500./2),1./0.0500))
    % % grid on
    % 
    % A = (1./utilisN./2 : 1./utilisN : 1);
    % for n = 1 : utilisN
    %     for m = 1 : utilisN
    %     %     fun2str(H1)
    % 
    %     %     C = {@(f) H(f-A(n)./2,1./A(n)),...
    %     %         @(f) H(f-1.5./3,1./A(n)),...
    %     %         @(f) H(f-(1-A(n)./2),1./A(n))};
    %     
    %         B{2,1}(m,n,1) = integral(@(f) H(f-A(m)./2,1./A(m)) .* ...
    %             H(f-A(n)./2,1./A(n)),-0.5,1.5) ./ A(n);
    %         B{2,1}(m,n,2) = integral(@(f) H(f-A(m)./2,1./A(m)) .* ...
    %             H(f-(1-A(n)./2),1./A(n)),-0.5,1.5) ./ A(n);
    %         B{2,2}(m,n,1) = integral(@(f) H(f-(1-A(m)./2),1./A(m)) .* ...
    %             H(f-A(n)./2,1./A(n)),-0.5,1.5) ./ A(n);
    %         B{2,2}(m,n,2) = integral(@(f) H(f-(1-A(m)./2),1./A(m)) .* ...
    %             H(f-(1-A(n)./2),1./A(n)),-0.5,1.5) ./ A(n);
    %     
    %         % B{secCou,secW}(loadW,loadI,secI)
    %         B{3,1}(m,n,1) = integral(@(f) H(f-A(m)./2,1./A(m)) .* ...
    %             H(f-A(n)./2,1./A(n)),-0.5,1.5) ./ A(n);
    % %         figure
    % %         hold on
    % %         plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-A(m)./2,1./A(m)))
    % %         plot(linspace(-0.5,1.5,1000),H(linspace(-0.5,1.5,1000)-1.5./3,1./A(n)))
    % %         plot(linspace(-0.5,1.5,1000),...
    % %             H(linspace(-0.5,1.5,1000)-A(m)./2,1./A(m)) .* ...
    % %             H(linspace(-0.5,1.5,1000)-1.5./3,1./A(n)))
    %         B{3,1}(m,n,2) = integral(@(f) H(f-A(m)./2,1./A(m)) .* ...
    %             H(f-1.5./3,1./A(n)),-0.5,1.5) ./ A(n);
    %         B{3,1}(m,n,3) = integral(@(f) H(f-A(m)./2,1./A(m)) .* ...
    %             H(f-(1-A(n)./2),1./A(n)),-0.5,1.5) ./ A(n);
    %         B{3,2}(m,n,1) = integral(@(f) H(f-1.5./3,1./A(m)) .* ...
    %             H(f-A(n)./2,1./A(n)),-0.5,1.5) ./ A(n);
    %         B{3,2}(m,n,2) = integral(@(f) H(f-1.5./3,1./A(m)) .* ...
    %             H(f-1.5./3,1./A(n)),-0.5,1.5) ./ A(n);
    %         B{3,2}(m,n,3) = integral(@(f) H(f-1.5./3,1./A(m)) .* ...
    %             H(f-(1-A(n)./2),1./A(n)),-0.5,1.5) ./ A(n);
    %         B{3,3}(m,n,1) = integral(@(f) H(f-(1-A(m)./2),1./A(m)) .* ...
    %             H(f-A(n)./2,1./A(n)),-0.5,1.5) ./ A(n);
    %         B{3,3}(m,n,2) = integral(@(f) H(f-(1-A(m)./2),1./A(m)) .* ...
    %             H(f-1.5./3,1./A(n)),-0.5,1.5) ./ A(n);
    %         B{3,3}(m,n,3) = integral(@(f) H(f-(1-A(m)./2),1./A(m)) .* ...
    %             H(f-(1-A(n)./2),1./A(n)),-0.5,1.5) ./ A(n);
    %     end
    % end
    % clear n A

    % A = repmat((1./utilisN./2:1./utilisN:1).',[1,utilisN]);
    % % B{2,1,1} = A;
    % % B{2,1,2} = max(A+A.'-1,0);
    % % B{2,2,1} = max(A.'+A-1,0);
    % % B{2,2,2} = A;
    % B{2,1} = cat(3,A,max(A+A.'-1,0));
    % B{2,2} = cat(3,max(A.'+A-1,0),A);
    % % B{3,1,1} = A;
    % % B{3,1,2} = max(min(A-0.5+A.'./2,A.'),0);
    % % B{3,1,3} = max(A+A.'-1,0);
    % % B{3,2,1} = max(min(A.'-0.5+A./2,A),0);
    % % B{3,2,2} = A;
    % % B{3,2,3} = max(min(0.5+A./2-1+A.',A),0);
    % % B{3,3,1} = max(A.'+A-1,0);
    % % B{3,3,2} = max(min(0.5+A.'./2-1+A,A.'),0);
    % % B{3,3,3} = A;
    % B{3,1} = cat(3,A,max(min(A-0.5+A.'./2,A.'),0),max(A+A.'-1,0));
    % B{3,2} = cat(3,max(min(A.'-0.5+A./2,A),0),A,max(min(0.5+A./2-1+A.',A),0));
    % B{3,3} = cat(3,max(A.'+A-1,0),max(min(0.5+A.'./2-1+A,A.'),0),A);
    % clear A
end

% Load on Sim
Sim.interfMultip = B;
end

function Sim = c_SINR_SD(Sim)
%% Calculate SINR standard devation
% For a number of ratio values, and standard deviation values of wanted and
% interfering received signal strength, calculate the standard deviation of
% SINR. The ratio here is the ratio of same site interference over other
% site interference r = IsameSitLin ./ IotSitLin.

if ~Sim.servDem.isInterfeExpl.cov
    return
end

fprintf(1,'Pre-calculate the SINR standard deviation...');

sigma0_ = [0 6.5:0.1:10.5];
sigma2_ = [0 6.5:0.1:10.5];
r = 10 .^ ((-30:30)./10);
y = zeros(length(r),length(sigma0_),length(sigma2_));
for m = 1 : length(sigma0_)
    for n = 1 : length(sigma2_)
        % Create two (log)-normal distributions of sigma0_(m) and
        % sigma2_(n) standard deviation.
        rng(936)
        v0 = randn(10e3,1);
        v2 = 0.5.*v0 + sqrt(1-0.5.^2).*randn(10e3,1);
        v0 = v0 .* sigma0_(m);
        v2 = v2 .* sigma2_(n);
        v0 = 10 .^ (v0./10);
        v2 = 10 .^ (v2./10);

        % Verify RV
        % mean(10.*log10(v0))  % mu=0
        % mean(10.*log10(v2))  % mu=0
        % std(10.*log10(v0))  % sigma=1
        % std(10.*log10(v2))  % sigma=1
        % corrcoef(10.*log10(v0),10.*log10(v2))  % rho=0.5

        % For each considered ratio calculate the standard deviation of the
        % SINR.
        % x = zeros(length(r),1);
        % SINR_lin = P0*g0*LN(0,sigma0)/(IsameSitLin*LN(0,sigma0)+IotSitLin*LN(0,sigma1))
        % SINR_lin = P0*g0*sigma0*LN(0,1)/(IsameSitLin*sigma0*LN(0,1)+IotSitLin*sigma1*LN(0,1))
        % SINR_lin = P0*g0/IotSitLin*LN(0,sigma0)/(r*LN(0,sigma0)+LN(0,sigma1))
        for k = 1 : length(r)
        %     x(k) = mean(10.*log10(v0./(r(k).*v0+v2)));
            y(k,m,n) = std(10.*log10(v0./(r(k).*v0+v2)));
        %     distributionFitter(10.*log10(v0./(r(k).*v0+v2)))
        %     kstest(10.*log10(v0./(r(k).*v0+v2)))  % 1 if the test rejects the null hypothesis that the data in vector x comes from a standard normal distribution
        %     kstest(10.*log10(v0./(r(k).*v0+v2)) - ...
        %         mean(10.*log10(v0./(r(k).*v0+v2))))

        %     figure
        %     hist(10.*log10(v0./(r(k).*v0+v2)))
        end
    end
end
% figure
% plot(10.*log10(r),y.*sqrt(1./((sigma0-sigma2.*0.5).^2+sigma2.^2.*(1-0.5.^2))),'.-')
% grid on
% figure
% plot(10.*log10(r),y,'.-')
% grid on
% xlabel('r = IsameSitLin ./ IotSitLin')
% ylabel('Sigma')

% figure
% hold on
% [X,Y] = meshgrid(8:0.1:9.6,8:0.1:9.6);
% [xslice,~] = find(bsxfun(@eq,-50:20:60,(-50:0.1:60).'));
% for n = 1 : length(xslice)
%     surf(X,Y,squeeze(Sim.SINRsd(xslice(n),2:end,2:end)))
%     mean(mean(Sim.SINRsd(xslice(n),2:end,2:end)))
% end
% % colorbar
% view(3)
% grid on
% xlabel('\sigma_0')
% ylabel('\sigma_1')
% zlabel('\sigma_{S_1}')
% title('\rho=0.5')

% figure
% [X,Y,Z] = meshgrid(8:0.1:9.6,-50:0.1:60,8:0.1:9.6);
% yslice = -50 : 20 : 60;
% lvls = 0 : 10;
% contourslice(X,Y,Z,Sim.SINRsd(:,2:end,2:end),[],yslice,[],lvls)
% colorbar
% view(3)
% grid on

Sim.SINRsd = y;

fprintf(1,'DONE\n');
end

function Sim = c_mvncdf(Sim)
%% Calculate success rates

trafClI = Sim.dem.modeAlTrafClasAs;
if max(structfun(@(x) x.CEtput.ServCouMax(trafClI),Sim.dem.trafVol)) == 1
    return
end

fprintf(1,'Pre-calculate stats for multi-server connectivity...');

[S1,S2] = meshgrid(linspace(-8,4,401),linspace(-5,3,201));
mvncdfY2 = nan(size(S1));
SIGMA = [1 -0.5;-0.5 1];
SIGMA_array = py.numpy.reshape(SIGMA(:).',int32(size(SIGMA)));
for t = 1 : numel(S1)
%     mvncdfY2(t) = 1 - mvncdf(zeros(2,1),[S1(t);S2(t)],[1 -0.5;-0.5 1]);
    rv = py.scipy.stats.multivariate_normal([S1(t);S2(t)],SIGMA_array);
    mvncdfY2(t) = 1 - rv.cdf(zeros(1,2));
end
% S1(1,2)-S1(1,1)
% S2(2,1)-S2(1,1)

% figure
% contour(S1,S2,mvncdfY2,[-999 max(P(:,m))])
% axis equal

Sim.mvncdfY2 = mvncdfY2;

fprintf(1,'DONE\n');
end

function Sim = s_servOrd(Sim)
%% Find the service order
% For each family and traffic class find latency
laten = zeros(length(Sim.dem.famUseCase_),...
    length(Sim.dem.trafClas_));
for fn = 1 : length(Sim.dem.famUseCase_)
    laten(fn,:) = Sim.dem.trafVol.(Sim.dem.famUseCase_{fn}).CEtput.laten;
end

[~,prio] = sort(structfun(@(x) x,Sim.dem.prio).');
B_ = [];
I_ = [];
for n = prio
    [B,I] = sort(laten(n,:));
    B_ = [B_; B.'];
    I_ = [I_; I.'+(n-1)*size(laten,2)];
end
B = B_;
I = I_;
[trafClI,famI] = ind2sub(size(laten.'),I(~isnan(B)));
Sim.dem.servOrd = [famI trafClI];
end

%%
function split_cost(Sim)

% isUpg = [Sim.rul.isUpg];
% isDecomm = [Sim.rul.isDecomm];

m_ = 2020 : 2030;
% Sim.shari.is = true;
OPEXlega = zeros(length(m_),1);
CAPEXnew = zeros(length(m_),1);
OPEXnew = zeros(length(m_),1);
CAPEXnewMacro = zeros(length(m_),1);
CAPEXnewsmaCel5W = zeros(length(m_),1);
CAPEXnewedgCloSit = zeros(length(m_),1);
CAPEXnewMacroCivWoAnAcq = zeros(length(m_),1);
CAPEXnewMacroBackhau = zeros(length(m_),1);
CAPEXnewMacroAntenFeede = zeros(length(m_),1);
CAPEXnewMacroBBbareMeta = zeros(length(m_),1);
CAPEXnewMacroBBedgNod = zeros(length(m_),1);
CAPEXnewMacroRFfronEnd = zeros(length(m_),1);
CAPEXnewMacroLab = zeros(length(m_),1);
for m = 1 : length(m_)
    % Update the year
    Sim.yea.thisYea = m_(m);
    
    Hash = DataHash(struct('simYeaThisYea',Sim.yea.thisYea,...
        'simShariIs',Sim.shari.is));
    load(['OutpAux\' Hash '.mat'],'Cell','edgCloSit')
    
    Cell_INfoc = [Cell.INfoc];
    pwrAmpClas = {Cell.pwrAmpClas};
    
    % legacy OPEX
    IND = find(Cell_INfoc & [Cell.established]<2020);
    assert(sum(arrayfun(@(x) x.capexV(1),Cell(IND)))==0)
    OPEXlega(m) = sum(arrayfun(@(x) x.opexV(1),Cell(IND)));
    
    % new CAPEX and OPEX
    IND = find(Cell_INfoc & [Cell.established]>=2020);
%     IND = find(Cell_INfoc & [Cell.established]==Sim.yea.thisYea);
    IND2 = [edgCloSit.INfoc];
    CAPEXnew(m) = sum(arrayfun(@(x) x.capexV(1),Cell(IND))) + ...
        sum(arrayfun(@(x) x.capexV(1),edgCloSit(IND2)));
    OPEXnew(m) = sum(arrayfun(@(x) x.opexV(1),Cell(IND))) + ...
        sum(arrayfun(@(x) x.opexV(1),edgCloSit(IND2)));
    
    % CAPEX components
    IND = find(Cell_INfoc & [Cell.established]>=2020 & ...
        strcmp('macrocell',pwrAmpClas));
    CAPEXnewMacro(m) = sum(arrayfun(@(x) x.capexV(1),Cell(IND)));
    IND = find(Cell_INfoc & [Cell.established]>=2020 & ...
        strcmp('smaCel5W',pwrAmpClas));
    CAPEXnewsmaCel5W(m) = sum(arrayfun(@(x) x.capexV(1),Cell(IND)));
    CAPEXnewedgCloSit(m) = sum(arrayfun(@(x) x.capexV(1),edgCloSit(IND2)));
    
    % CAPEX macrocell components
    IND = find(Cell_INfoc & [Cell.established]>=2020 & ...
        strcmp('macrocell',pwrAmpClas));
    CAPEXnewMacroCivWoAnAcq(m) = sum(arrayfun(@(x) x.capexCivWoAnAcqV(1),Cell(IND)));
    CAPEXnewMacroBackhau(m) = sum(arrayfun(@(x) x.capexBackhauV(1),Cell(IND)));
    CAPEXnewMacroAntenFeede(m) = sum(arrayfun(@(x) x.capexAntenFeedeV(1),Cell(IND)));
    CAPEXnewMacroBBbareMeta(m) = sum(arrayfun(@(x) x.capexBBbareMetaV(1),Cell(IND)));
    CAPEXnewMacroBBedgNod(m) = sum(arrayfun(@(x) x.capexBBedgNodV(1),Cell(IND)));
    CAPEXnewMacroRFfronEnd(m) = sum(arrayfun(@(x) x.capexRFfronEndV(1),Cell(IND)));
    CAPEXnewMacroLab(m) = sum(arrayfun(@(x) x.capexLabV(1),Cell(IND)));
end

% figure
% hold on
% plot(2020:2030,CAPEXnewMacroCivWoAnAcq,'disp','CivWoAnAcq')
% plot(2020:2030,CAPEXnewMacroBackhau,'disp','Backhau')
% plot(2020:2030,CAPEXnewMacroAntenFeede,'disp','AntenFeede')
% plot(2020:2030,CAPEXnewMacroBBbareMeta,'disp','BBbareMeta')
% plot(2020:2030,CAPEXnewMacroBBedgNod,'disp','BBedgNod')
% plot(2020:2030,CAPEXnewMacroRFfronEnd,'disp','RFfronEnd')
% plot(2020:2030,CAPEXnewMacroLab,'disp','Lab')
% plot(2020:2030,CAPEXnewMacro,'disp','Total')
% legend('location','best')

% xlRange = 'B28:L28';  % middle
% % xlRange = 'B42:L42';  % low
% CRANaccCost = xlsread(fullfile(Sim.dropPath,...
%     '5G NORMA RW internal with Ade',...
%     '07 WP2 - Use cases and economic evaluation','Revenue',...
%     'Business case results 0.3.xlsx'),'BusinessCase',xlRange).';

% OPEXlega+CAPEXnew+OPEXnew

% CRANaccCostOPEXlega = CRANaccCost .* OPEXlega ./ (OPEXlega+CAPEXnew+OPEXnew);
% CRANaccCostCAPEXnew = CRANaccCost .* CAPEXnew ./ (OPEXlega+CAPEXnew+OPEXnew);
% CRANaccCostOPEXnew = CRANaccCost .* OPEXnew ./ (OPEXlega+CAPEXnew+OPEXnew);
CRANaccCostOPEXlega = OPEXlega;
CRANaccCostCAPEXnew = CAPEXnew;
CRANaccCostOPEXnew = OPEXnew;

tmp = CAPEXnewMacro + CAPEXnewsmaCel5W + CAPEXnewedgCloSit;
CAPEXnewMacro = CRANaccCostCAPEXnew .* CAPEXnewMacro ./ tmp;
CAPEXnewsmaCel5W = CRANaccCostCAPEXnew .* CAPEXnewsmaCel5W ./ tmp;
CAPEXnewedgCloSit = CRANaccCostCAPEXnew .* CAPEXnewedgCloSit ./ tmp;

% Sales & Admin as proportion of cost of sales
% Cell: C12
% Tab: Background
% File: Business case results 0.3.xlsx
% Folder: D:\Dropbox (Real Wireless)\5G NORMA RW internal with Ade\07 WP2 - Use cases and economic evaluation\Revenue
r = (4349+6080) ./ 34576;

% M = ['Legacy RAN OPEX' num2cell(CRANaccCostOPEXlega.')
%     'Legacy core network' num2cell(CRANaccCostOPEXlega.'.*0.1)
%     'Legacy administrative' num2cell(CRANaccCostOPEXlega.'.*1.1./(1-r).*r)
%     'New RAN CAPEX' num2cell(CRANaccCostCAPEXnew.')
%     'New RAN OPEX' num2cell(CRANaccCostOPEXnew.')
%     'New core network' num2cell((CRANaccCostCAPEXnew+CRANaccCostOPEXnew).'.*0.1)
%     'New administrative' num2cell((CRANaccCostCAPEXnew+CRANaccCostOPEXnew).'.*1.1./(1-r).*r)];
% M = [M num2cell(sum(cell2mat(M(:,2:end)),2))];
% M = [M
%     'Total cost' num2cell(sum(cell2mat(M(:,2:end)),1))];
% 
% cellfun(@num2str,num2cell(round(cell2mat(M(:,2:end)),2)),...
%     'UniformOutput',false)

M2 = ['Macrosites' num2cell(CAPEXnewMacro.')
    'Small Cells' num2cell(CAPEXnewsmaCel5W.')
    'Edge Cloud' num2cell(CAPEXnewedgCloSit.')]; %KK suggeste this change, New Core network
M2 = [M2 num2cell(sum(cell2mat(M2(:,2:end)),2))];
M2 = [M2
    'Total CAPEX' num2cell(sum(cell2mat(M2(:,2:end)),1))];

% KK suggested to comment this out
% M3 = ['Legacy RAN OPEX' num2cell(CRANaccCostOPEXlega.')
%     'Legacy core network' num2cell(CRANaccCostOPEXlega.'.*0.1)
%     'Legacy administrative' num2cell(CRANaccCostOPEXlega.'.*1.1./(1-r).*r)
%     'New RAN OPEX' num2cell(CRANaccCostOPEXnew.')
%     'New core network' num2cell((CRANaccCostCAPEXnew+CRANaccCostOPEXnew).'.*0.1)
%     'New administrative' num2cell((CRANaccCostCAPEXnew+CRANaccCostOPEXnew).'.*1.1./(1-r).*r)];
% M3 = [M3 num2cell(sum(cell2mat(M3(:,2:end)),2))];
% M3 = [M3
%     'Total OPEX' num2cell(sum(cell2mat(M3(:,2:end)),1))];

% KK suggested to comment this out
% M4 = ['CAPEX (total)' M2(end,2:end-1)
%     'OPEX (total)' M3(end,2:end-1)];
% M4 = [M4 num2cell(sum(cell2mat(M4(:,2:end)),2))];
% M4 = [M4
%     'TCO' num2cell(sum(cell2mat(M4(:,2:end)),1))];

% cellfun(@num2str,num2cell(round(cell2mat(M2(:,2:end)),2)),...
%     'UniformOutput',false)
% cellfun(@num2str,num2cell(round(cell2mat(M3(:,2:end)),2)),...
%     'UniformOutput',false)
% cellfun(@num2str,num2cell(round(cell2mat(M4(:,2:end)),2)),...
%     'UniformOutput',false)

load(['OutpAux\' Hash '.mat'],'resu')

%HO: This is the number of sites (total number of sites)
k = find(strcmp(resu.M(:,1),'Installed base - macrocell'));
tmp = cell2mat(resu.M(k+3+(0:16:32),3:end));
tot_num_sites = ['Macrosites' num2cell(tmp(1,1:2:end)+tmp(1,2:2:end)) %HO change "M" to "tot_num_sites"
    'Small cell sites' num2cell(tmp(2,1:2:end)+tmp(2,2:2:end))];

%HO: This is the RAN cost (this is actually what is needed), TCO
k = find(strcmp(resu.M(:,1),'Total network expenditure per year  (k�)'));
tmp = cell2mat(resu.M(k+3,3:end));
TCO = ['eMBB' num2cell(tmp(1,1:2:end)+tmp(1,2:2:end))]; %HO change "M" to "TCO"

% CAPEXmacrocell
k1 = find(strcmp(resu.M(:,1),'Installed base - macrocell'));
k2 = find(strcmp(resu.M(:,1),...
    'Average CAPEX per unit of equipment  (k�) - macrocell - installeBas'));
tmp2 = resu.M(k2+3+(0:11),3:end);
tmp2(cellfun(@(x) isempty(x),tmp2)) = {0};
tmp2(cellfun(@(x) isnan(x),tmp2)) = {0};
tmp = cell2mat(resu.M(k1+3+(0:11),3:end)) .* cell2mat(tmp2);
CAPEXmacrocell = tmp(:,1:2:end) + tmp(:,2:2:end);
% sum(CAPEXmacrocell,1)

% CAPEXsmaCel5W
k1 = find(strcmp(resu.M(:,1),'Installed base - smaCel5W'));
k2 = find(strcmp(resu.M(:,1),...
    'Average CAPEX per unit of equipment  (k�) - smaCel5W - installeBas'));
tmp2 = resu.M(k2+3+(0:11),3:end);
tmp2(cellfun(@(x) isempty(x),tmp2)) = {0};
tmp2(cellfun(@(x) isnan(x),tmp2)) = {0};
tmp = cell2mat(resu.M(k1+3+(0:11),3:end)) .* cell2mat(tmp2);
CAPEXsmaCel5W = tmp(:,1:2:end) + tmp(:,2:2:end);
% sum(CAPEXsmaCel5W,1)

% CAPEXedgCloSit
k1 = find(strcmp(resu.M(:,1),'Installed base - Edge Cloud'));
k2 = find(strcmp(resu.M(:,1),...
    'Average CAPEX per unit of equipment  (k�) - Edge Cloud - installeBas'));
tmp2 = resu.M(k2+3+(0:10),3:end);
tmp2(cellfun(@(x) isempty(x),tmp2)) = {0};
tmp2(cellfun(@(x) isnan(x),tmp2)) = {0};
beep
disp('Check the following line')
keyboard
tmp = cell2mat(resu.M(k1+3+(0:10),3:end)) .* cell2mat(tmp2);
CAPEXedgCloSit = tmp(:,1:2:end) + tmp(:,2:2:end);
% sum(CAPEXedgCloSit,1)

% OPEXmacrocell
k1 = find(strcmp(resu.M(:,1),'Installed base - macrocell'));
k2 = find(strcmp(resu.M(:,1),...
    'Average OPEX per unit of equipment  (k�) - macrocell - installeBas'));
tmp2 = resu.M(k2+3+(0:11),3:end);
tmp2(cellfun(@(x) isempty(x),tmp2)) = {0};
tmp2(cellfun(@(x) isnan(x),tmp2)) = {0};
tmp = cell2mat(resu.M(k1+3+(0:11),3:end)) .* cell2mat(tmp2);
OPEXmacrocell = tmp(:,1:2:end) + tmp(:,2:2:end);
% sum(OPEXmacrocell,1)

% OPEXsmaCel5W
k1 = find(strcmp(resu.M(:,1),'Installed base - smaCel5W'));
k2 = find(strcmp(resu.M(:,1),...
    'Average OPEX per unit of equipment  (k�) - smaCel5W - installeBas'));
tmp2 = resu.M(k2+3+(0:11),3:end);
tmp2(cellfun(@(x) isempty(x),tmp2)) = {0};
tmp2(cellfun(@(x) isnan(x),tmp2)) = {0};
tmp = cell2mat(resu.M(k1+3+(0:11),3:end)) .* cell2mat(tmp2);
OPEXsmaCel5W = tmp(:,1:2:end) + tmp(:,2:2:end);
% sum(OPEXsmaCel5W,1)

% OPEXedgCloSit
k1 = find(strcmp(resu.M(:,1),'Installed base - Edge Cloud'));
k2 = find(strcmp(resu.M(:,1),...
    'Average OPEX per unit of equipment  (k�) - Edge Cloud - installeBas'));
tmp2 = resu.M(k2+3+(0:11),3:end);
tmp2(cellfun(@(x) isempty(x),tmp2)) = {0};
tmp2(cellfun(@(x) isnan(x),tmp2)) = {0};
tmp = cell2mat(resu.M(k1+3+(0:11),3:end)) .* cell2mat(tmp2);
OPEXedgCloSit = tmp(:,1:2:end) + tmp(:,2:2:end);
% sum(OPEXedgCloSit,1)

%HO: This is the RAN cost (this is actually what is needed)
M = ['Civil works & acq.' num2cell(sum(CAPEXmacrocell(1,4:end),2))
    'Antennas/feeder' num2cell(sum(CAPEXmacrocell(6,4:end),2))
    'RF front end' num2cell(sum(CAPEXmacrocell(7,4:end),2))
    'Labour' num2cell(sum(CAPEXmacrocell(10,4:end),2))
    'Transport' num2cell(sum(CAPEXmacrocell(11,4:end),2))
    'Rent' {[]}
    'Power' {[]}
    'Maintenance visits' {[]}
    'RAN equip. licensing' {[]}
    'Working servers' {[]}
    'Spare servers' {[]}
    'Cabinets' {[]}
    'Cabinet rent & utilities' {[]}
    'Operating overhead' {[]}];
M = [M,...
    [{[]}
    {[]}
    {[]}
    {[]}
    num2cell(sum(OPEXmacrocell(12,4:end),2))
    num2cell(sum(OPEXmacrocell(2,4:end),2))
    num2cell(sum(OPEXmacrocell(3,4:end),2))
    num2cell(sum(OPEXmacrocell(4,4:end),2))
    num2cell(sum(OPEXmacrocell(5,4:end),2))
    {[]}
    {[]}
    {[]}
    {[]}
    {[]}]];
M = [M,...
    [num2cell(sum(CAPEXsmaCel5W(1,4:end),2))
    num2cell(sum(CAPEXsmaCel5W(6,4:end),2))
    num2cell(sum(CAPEXsmaCel5W(7,4:end),2))
    num2cell(sum(CAPEXsmaCel5W(10,4:end),2))
    num2cell(sum(CAPEXsmaCel5W(11,4:end),2))
    {[]}
    {[]}
    {[]}
    {[]}
    {[]}
    {[]}
    {[]}
    {[]}
    {[]}]];
M = [M,...
    [{[]}
    {[]}
    {[]}
    {[]}
    num2cell(sum(OPEXsmaCel5W(12,4:end),2))
    num2cell(sum(OPEXsmaCel5W(2,4:end),2))
    num2cell(sum(OPEXsmaCel5W(3,4:end),2))
    num2cell(sum(OPEXsmaCel5W(4,4:end),2))
    num2cell(sum(OPEXsmaCel5W(5,4:end),2))
    {[]}
    {[]}
    {[]}
    {[]}
    {[]}]];
M = [M,...
    [{[]}
    {[]}
    {[]}
    num2cell(sum(CAPEXedgCloSit(10,4:end),2))
    {[]}
    {[]}
    {[]}
    {[]}
    {[]}
    num2cell(sum(CAPEXedgCloSit(1,4:end),2))
    num2cell(sum(CAPEXedgCloSit(2,4:end),2))
    num2cell(sum(CAPEXedgCloSit(3,4:end),2))
    {[]}
    {[]}]];
M = [M,...
    [{[]}
    {[]}
    {[]}
    {[]}
    num2cell(sum(OPEXedgCloSit(12,4:end),2))
    {[]}
    {[]}
    {[]}
    num2cell(sum(OPEXedgCloSit(9,4:end),2))
    {[]}
    {[]}
    {[]}
    num2cell(sum(OPEXedgCloSit(6,4:end),2))
    num2cell(sum(OPEXedgCloSit(7,4:end),2))]];
CAP_OP_detailed = [{[]} num2cell((sum(sum(CAPEXmacrocell(:,4:end),2),1) + ... %HO changed "M" to "CAP_OP_detailed"
    sum(sum(OPEXmacrocell(:,4:end),2),1) + ...
    sum(sum(CAPEXsmaCel5W(:,4:end),2),1) + ...
    sum(sum(OPEXsmaCel5W(:,4:end),2),1) + ...
    sum(sum(CAPEXedgCloSit(:,4:end),2),1) + ...
    sum(sum(OPEXedgCloSit(:,4:end),2),1)) .* ones(1,6))
    M];

%HO: This is the RAN cost (this is actually what is needed), note that we
%are overritting M here..
CAP_OP = ['Macrocell CAPEX' {sum(sum(CAPEXmacrocell(:,4:end),2),1)} %HO changed "M" to "CAP_OP"
    'Macrocell OPEX' {sum(sum(OPEXmacrocell(:,4:end),2),1)}
    'Small cell CAPEX' {sum(sum(CAPEXsmaCel5W(:,4:end),2),1)}
    'Small cell OPEX' {sum(sum(OPEXsmaCel5W(:,4:end),2),1)}
    'Edge cloud CAPEX' {sum(sum(CAPEXedgCloSit(:,4:end),2),1)}
    'Edge cloud OPEX' {sum(sum(OPEXedgCloSit(:,4:end),2),1)}];

save('OutpAux\Res.mat','tot_num_sites','TCO','CAP_OP_detailed','CAP_OP',...
    'OPEXlega','CAPEXnew','OPEXnew')
end

%%
function Sim = s_rele(Sim,txt)
Sim.rele = txt;
end

%%
function Sim = pathlossDB(Sim)
% Set the pathloss file database according to the input in the config file
f = fullfile(Sim.path.on,'05 RW DB - General','Projects',...
    'Slough small cell');
switch Sim.propag.pathFiles
  case 'New_database'
    Sim.path.DB.Lbc = fullfile(f,['Lbc_' strjoin(cellfun(@(x) num2str(x...
        ),num2cell(clock),'UniformOutput',false),'_')]);

    % Check if this DB already exists
    if exist(Sim.path.DB.Lbc,'dir')
        error('Path loss folder already exists. Restart the sim.')
    end
  case 'Reuse_database'
    Sim.path.DB.Lbc = fullfile(f,'Lbc_Central');
  otherwise
    error('Undefined')
end

%% Make directory for path loss database
if ~exist(Sim.path.DB.Lbc,'dir')
    mkdir(Sim.path.DB.Lbc)
end
end
    end
end

%% LOCAL FUNCTIONS
function output = Replace(inputStr,word,replace)
%% REPLACE subfunction
% Replaces the word in input string with a 'replace' value
if strcmp(inputStr,word)
    output = replace;
else
    output = inputStr;
end
end

function output = ReplaceCell(cellArray,word,replace)
%% REPLACECELL subfunction
% Replaces all the entries in the cellArray with the 'replace' value
output = cellfun(@(x) Replace(x,word,replace),cellArray,'UniformOutput',false);
end

function numberOfEl = FindNumberOfElements(data)
%% FINDNUMBEROFELEMENTS subfunction
% This subfunction finds the number of elements in a respective section. This is assumed that it is
% the first line where there is an empty value, i.e. a NaN cell.
numberOfEl = find(cell2mat(cellfun(@(x) any(isnan(x)),data,'UniformOutput',false)),1) - 1;

if isempty(numberOfEl)
    numberOfEl = length(data);
end
end

function structArray = read_tab(fileID,heade,isTab,Format)
%% []
tline = fgetl(fileID);
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),'START')
    tline = fgetl(fileID);
end
while ~strcmp(tline(1:find(isstrprop(tline,'wspace'),1,'first')-1),heade) && ...
        ~strcmp(tline,heade)
    tline = fgetl(fileID);
end
tline = fgetl(fileID);
if isTab
    fields = textscan(tline,'%s');
    
    tline = fgetl(fileID);
    assert(length(fields)==1)
    M = {};
    while ~all(isstrprop(tline,'wspace'))
%         M = [M; num2cell(sscanf(tline,'%d')).'];
        C = textscan(tline,Format);
        k = strfind(Format,'%s');
        if ~isempty(k)
            for n = find(any(bsxfun(@eq,strfind(Format,'%'),k.'),1))
                C{n} = C{n}{1};
            end
        end
        M = [M; C];
        
        tline = fgetl(fileID);
        
        if feof(fileID)
            break
        end
    end
    structArray = cell2struct(M,fields{1},2);
else
    structArray = sscanf(tline,'%f');
end
frewind(fileID)
end