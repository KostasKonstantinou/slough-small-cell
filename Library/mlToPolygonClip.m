function P = mlToPolygonClip(X,Y)
% Convert polygon from matlab to PolygonClip representation
% Converts polygon X,Y from matlab representation (only nan-delimited
% supported) to PolygonClip representation P. A polygon may contain
% multiple regions, and each region may contian multiple holes.
% 
% Example 1: 
% Convert a simple polygon from matlab to PolygonClip representation
%
% X = [5
%     5
%     7
%     7
%     5];
% Y = [1
%     3
%     3
%     1
%     1];
% P = mlToPolygonClip(X,Y)
%
% The above example returns:   
% P = struct with fields:
%        x: [5 5 7 7 5]
%        y: [1 3 3 1 1]
%     hole: 0
%
% Kostas Konstantinou Aug 2018

% Find regions and their holes within the polygon
k = [0; find(isnan([X;nan]))];

% For each region or hole, create the PolygonClip representation P
for n = 1 : length(k)-1
    IND = k(n)+1 : k(n+1)-1;
    P(n) = struct('x',X(IND).','y',Y(IND).',...
        'hole',double(~ispolycw(X(IND),Y(IND))));
end
