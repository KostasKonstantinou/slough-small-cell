function plo_m(Sim,varargin)
%% Plot background map from Google Maps
% plo_m plots a map from Google Maps as background to the active figure.
% The active figure is expected to be in a rectilinear coordinate system.
% Google Maps server provides images in geodesic coordinates, thus plo_m
% performs image processing to convert the geodesic to rectilinear. plo_m
% saves the background image under ProgramData directory to avoid multiple
% server calls of the same map.
% 
% plo_m(Sim,Property,Value,...)
% 
% PROPERTIES:
%    MapType    - Type of map to return. Any of [roadmap, 
%                 satellite, terrain, hybrid]. See the Google Maps API for
%                 more information. 
%    ShowLabels - (0/1) Controls whether to display city/street textual labels on the map
%    brighten   - Set to 0.5 to brighten the map to increase contrast
%                 with other plotted map features, such as lines
% 
% Note that the resolution of Google Maps may appear bland for practical
% purposes. It is recommended that the function was improved to zoom in at
% multiple segments of the map, acquire individual images and then combine
% them into a single high-resolution verion.
% 
% Sim is a structure and must contain the following fields:
% path.ProgramData - The path to program data
% mstruct - Matlab native map projection structure
% GmAPIkey - (string) set your own API key which you obtained from Google:
%            http://developers.google.com/maps/documentation/staticmaps/#api_key
%            Kostas has registered an API key and is available for a
%            sensible number of map requests:
%            \rW shared new\Bitbucket Local\KK\Code Library\Plotting\Google Map\API key
% 
% Example 1: Plot the background image map of an area that is plotted on
% coordinate system ETRS89 / UTM zone N32
% 
% Map projection related
% mstruct = defaultm('tranmerc');
% mstruct.origin = [0 9 0];
% mstruct.falseeasting = 500000;
% mstruct.falsenorthing = 0;
% mstruct.scalefactor = 0.9996;
% mstruct.geoid = referenceEllipsoid('grs80');
% Sim.mstruct = defaultm(mstruct);
% 
% figure
% axis equal
% hold on
% plot(1.0e+05.*[5.6570 5.6570 5.6780 5.6780 5.6570],...
%     [5933425 5935075 5935075 5933425 5933425])
% Sim.GmAPIkey = 'SomeLongStringObtaindFromGoogle';
% plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,...
%     'grayscale',1)
%
% * v0.1 Kostas Konstantinou Jun 2016

% Unpack varargin
for n = 1 : 2 : length(varargin)
    switch lower(varargin{n})
        case 'showlabels'
            showLabels = varargin{n+1};
        case 'maptype'
            maptype = varargin{n+1};
        case 'brighten'
            beta = varargin{n+1};
            assert(beta==0.5,'Code this!')
        case 'grayscale'
            grayscale = varargin{n+1};
        otherwise
            error('Undefined')
    end
end

% Create a unique filename
DeltaX = 10 .^ fix(log10(diff(xlim())));
DeltaY = 10 .^ fix(log10(diff(ylim())));
Hash = DataHash(struct('xlim',round(xlim./DeltaX).*DeltaX,...
    'ylim',round(ylim./DeltaY).*DeltaY));
filepart2 = [maptype ' ' num2str(showLabels) ' ' Hash ' ' ...
    num2str(grayscale) '.mat'];

% Load the image if available on server or locally
[S,exitFlag] = loadDr(fullfile(Sim.path.ProgramData,filepart2),'Vq');
if exitFlag == 0
    % Unpack the image
    Vq = S.Vq;
elseif exitFlag == -1
    % Get the image from the web
    
    % Query axis rectilinear limits
    Xlim = xlim;
    Ylim = ylim;
    
    % Convert the limits to geodesic and get the image from the web
    if isfield(Sim.mstruct,'ostn')
        [lat,lon] = easNor2latLon(Xlim([1,1,2,2]).',Ylim([1,2,2,1]).',...
            Sim.mstruct.ostn);
    elseif isfield(Sim.mstruct,'mapprojection')
        [lat,lon] = minvtran(Sim.mstruct,Xlim([1,1,2,2]),Ylim([1,2,2,1]));
    else
        error('Code this!')
    end
    lonWorldLimits = [min(lon) max(lon)];
    latWorldLimits = [min(lat) max(lat)];
    latWorldLimits = latWorldLimits([1 2 2 1 1]);
    lonWorldLimits = lonWorldLimits([1 1 2 2 1]);
    figure('paperpositionmode','auto','unit','in',...
        'pos',[1 1 7.5 2.63./3.5.*7.5])
    plot(lonWorldLimits,latWorldLimits)
    [lonVec,latVec,Imag] = plot_google_map('ShowLabels',showLabels,...
        'MapType',maptype,'APIKey',Sim.GmAPIkey);
    delete(gcf)
    
    % Convert the (latWorldLimits,lonWorldLimits) into
    % (xWorldLimits,yWorldLimits)
    if isfield(Sim.mstruct,'ostn')
        [x,y] = latLon2easNor(latWorldLimits.',lonWorldLimits.',[],...
            Sim.mstruct.ostn);
    elseif isfield(Sim.mstruct,'mapprojection')
        [x,y] = mfwdtran(Sim.mstruct,latWorldLimits,lonWorldLimits);
    else
        error('Code this!')
    end
    x = [min(x) max(x)];
    y = [min(y) max(y)];
    [x,y] = meshgrid(x,y);

    % Create raster of iamge in rectilinear
    [LonVec,LatVec] = meshgrid(lonVec,latVec);
    assert(size(LonVec,1)==size(LonVec,2))
    [Xq,Yq] = meshgrid(linspace(x(1,1),x(1,end),size(LonVec,1)),...
        linspace(y(1,1),y(end,1),size(LonVec,2)));
    if isfield(Sim.mstruct,'ostn')
        [Yq(:),Xq(:)] = easNor2latLon(Xq(:),Yq(:),Sim.mstruct.ostn);
    elseif isfield(Sim.mstruct,'mapprojection')
        [Yq,Xq] = minvtran(Sim.mstruct,Xq,Yq);
    else
        error('Code this!')
    end

    % Resample geodesic image in planar coordinates
    switch grayscale
        case 0
            Vq.A = cat(3,...
                interp2(LonVec,LatVec,Imag(:,:,1).^beta,Xq,Yq,'linear'),...
                interp2(LonVec,LatVec,Imag(:,:,2).^beta,Xq,Yq,'linear'),...
                interp2(LonVec,LatVec,Imag(:,:,3).^beta,Xq,Yq,'linear'));
        case 1
            Vq.A = interp2(LonVec,LatVec,rgb2gray(Imag).^beta,Xq,Yq,'linear');
        otherwise
            error('Undefined!')
    end
    Vq.x = [x(1,1) x(1,end)];
    Vq.y = [y(1,1) y(end,1)];

    % Construct geographic raster reference object
    assert(size(LonVec,1)==size(LonVec,2))
    R = georasterref('RasterSize',size(LonVec),...
        'ColumnsStartFrom','north',...
        'LatitudeLimits',[LatVec(end) LatVec(1)] + ...
        0.5.*(LatVec(1,1)-LatVec(2,1)).*[-1 1],...
        'LongitudeLimits',[LonVec(1) LonVec(end)] + ...
        0.5.*(LonVec(1,2)-LonVec(1,1)).*[-1 1]);

    % Save the image for future queries
    save(fullfile(Sim.path.ProgramData,filepart2),'Vq','Imag','R')
end

% ratio = (max(Vq.y)-min(Vq.y)) ./ (max(Vq.x)-min(Vq.x));

% Plot the image as background
% figure('paperpositionmode','auto','unit','in',...
%     'pos',[1 -1 7.5 7.5.*ratio])
switch grayscale
    case 0
        im = image(Vq.x,Vq.y,Vq.A);
    case 1
        im = image(Vq.x,Vq.y,repmat(Vq.A,[1 1 3]));
    otherwise
        error('Undefined!')
end
% uistack(im,'down',1)
uistack(im,'bottom')
% hold on
% axis equal tight
% xlabel('Easting  (m)')
% ylabel('Northing  (m)')
% grid on
set(gca,'ydir','normal')
% axis off
% set(gca,'pos',[0 0 1 1])
% add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);
