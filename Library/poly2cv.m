function [C,V] = poly2cv(X,Y)
% Convert a number of polygons to polygonal regions C and vertices matrix V
% Matlab has a function to convert X,Y into F,V; but not to C,V. F,V
% consists of triangles.
% 
% Example 1: 
% Convert four polygons to C V
%   
% X = {[576587;563982;558915;559216;576587;576587]
%     [576587;562955;563982;576587;576587]
%     [563982;562955;548688;558915;563982]
%     [558915;548688;518463;518463;559216;558915]};
% Y = {[5939945;5931392;5960412;5964687;5964687;5939945]
%     [5906563;5906563;5931392;5939945;5906563]
%     [5931392;5906563;5906563;5960412;5931392]
%     [5960412;5906563;5906563;5964687;5964687;5960412]};
% [C,V] = poly2cv(X,Y)
% figure
% axis equal
% hold on
% for n = 1 : length(C)
%     [Xcells,Ycells] = polysplit(V(C{n},1),V(C{n},2));
%     for m = 1 : length(Xcells)
%         patch(Xcells{m},Ycells{m},zeros(length(Xcells{m}),1),'r')
%     end
% end
%
% The above example returns:   
% C = {[6;5;7;9;10;6]
%     [4;3;5;6;4]
%     [5;3;2;7;5]
%     [7;2;1;8;9;7]}
% V = [518463 5906563
%     548688 5906563
%     562955 5906563
%     576587 5906563
%     563982 5931392
%     576587 5939945
%     558915 5960412
%     518463 5964687
%     559216 5964687
%     576587 5964687];

% Convert to C,V format
V2 = [cell2mat(X) cell2mat(Y)];
[V3,~,ic] = unique(V2*[1;1j]);
V = [real(V3) imag(V3)];

IND = isnan(V(:,1));
if any(IND)
    k = find(isnan(V(:,1)),1);
    V(k+1:end,:) = [];
    C = mat2cell(min(ic,k),cellfun(@length,X),1);
else
    C = mat2cell(ic,cellfun(@length,X),1);
end
