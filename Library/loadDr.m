function [S,exitFlag] = loadDr(filename,variables)
% Load from Dropbox
% loadDr loads the variables, variables, from file, filename, and outputs
% these variables as S. The exit flag, exitFlag, is 0 if the file was
% successfully loaded, and 1 if the file was not found. loadDr ensures that
% a local copy is downloaded from the server, and that no conflict occurs
% when multiple file access requests coincide.
% 
% * Kostas Konstantinou Jul 2018

% Try to open it to force dropbox to download a local copy, if available on
% the cloud
fileID = fopen(filename);
if fileID ~= -1
    % The file existed in the cloud and has been downloaded
    fclose(fileID);
end

% Load the requested variables, if the file exists
if exist(filename,'file')
    % Load the requested variables.
    % The while loop ensures that the file is loaded when available, if
    % multiple requests to the same file coincide.
    argum = true;
    while argum
        try
            S = load(filename,variables);
            argum = false;
        catch
            pause(rand())
        end
    end
    
    % File loaded
    exitFlag = 0;
else
    % File not found
    S = [];
    exitFlag = -1;
end
