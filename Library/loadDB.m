function [ S, exitFlag ] = loadDB( filename, var )
%LOADDB loads the var variable from the file as given in the filename in the database. When loading
%       a file for processing, it renders it inaccesible to other actors (it is made accessible
%       again upon finishing the tasks later on).

% Try to open it to force dropbox to download a local copy, if available on
% the cloud
fileID = fopen( filename );
if fileID ~= -1
    % The file existed in the cloud and has been downloaded
    fclose( fileID );
end

if exist( filename, 'file' )
    % Check if the file is writeable
    [ ~, values ] = fileattrib( filename );
    % Counters for acknowledging the time
    waitCou = 0;
    decade  = 1;
    while ~values.UserWrite
        % Wait for the file to become available
        pause( rand( ) )
        % A counter for acknowledging the waiting time
        waitCou = waitCou + 1;
        if waitCou == 100
            % After waiting for too long, then unlock the file and move on
            fprintf( 'Unlocking %s\n', filename )
            fileattrib( filename, '+w' );
        elseif waitCou == decade * 10
            fprintf( 'Incorrect lock of %s? Tries: %2.0f\n', filename, waitCou )
            % Increase the decade waiting time
            decade = decade + 1;
        end
        % Check the file again
        [ ~, values ] = fileattrib( filename );
    end
    
    try
        % Try loading the file
        S = load( filename, var );
        % Make the file not writeable until we calculate the values, in order to avoid corrupt files and
        % data deletion
        fileattrib( filename, '-w' );
        
        % File loaded
        exitFlag = 0;
    catch
        % If you cannot load it, then it is corrupt and it should be recreated
        S = [];
        exitFlag = -1;
    end

else
    % File not found
    S = [];
    exitFlag = -1;
end
