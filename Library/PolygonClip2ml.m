function [out1,out2] = PolygonClip2ml(P,sty)
%% Convert polygon from PolygonClip to matlab representation
% Converts polygon from PolygonClip representation, P, to matlab
% cell array format (sty='XY', out1=X, out2=Y) or fv format (sty='fv',
% out1=f, out2=v). A polygon may contain
% multiple regions, and each region may contain multiple holes.
% 
% Example 1: 
% Convert a simple polygon from matlab to PolygonClip representation
%
% P(1).x = [32; 18];
% P(1).y = [10; 33];
% P(1).hole = 0;
% P(2).x = [58097; 61106; 60679; 58017; 57916];
% P(2).y = [33327; 33141; 26816; 27536; 33360];
% P(2).hole = 0;
% [X,Y] = PolygonClip2ml(P,'XY')
%
% The above example returns:   
% X = 2�1 cell array
% Y = 2�1 cell array
% X{1} = 
%     32
%     18
%     32
% X{2} =
%     58097
%     61106
%     60679
%     58017
%     57916
%     58097
% Y{1} =
%     10
%     33
%     10
% Y{2} =
%     33327
%     33141
%     26816
%     27536
%     33360
%     33327
%
% Kostas Konstantinou May 2016

% For each polygon region, update the ouputs out1,out2
isHole = find([P.hole]);
isNotHole = find(~[P.hole]);
X = cell(length(isNotHole),1);
Y = cell(length(isNotHole),1);
%     IN = logical(sparse(length(isNotHole),length(isHole)));
for m = 1 : length(isNotHole)
    % Update X,Y with the polygon region
    X{m} = P(isNotHole(m)).x;
    Y{m} = P(isNotHole(m)).y;

    % Update X,Y with the polygon region's holes, if any
    k_ = find(inpolygon(arrayfun(@(x) x.x(1),P(isHole)),...
        arrayfun(@(x) x.y(1),P(isHole)),...
        P(isNotHole(m)).x,P(isNotHole(m)).y));
    for k = 1 : length(k_)
        assert(~ispolycw(P(isHole(k_(k))).x,P(isHole(k_(k))).y))
        X{m} = [X{m}; NaN; P(isHole(k_(k))).x];
        Y{m} = [Y{m}; NaN; P(isHole(k_(k))).y];
    end

    % Convert X,Y into fv, if needed
    switch sty
        case 'XY'
            out1 = X;
            out2 = Y;
        case 'fv'
            [f,v] = poly2fv(X{m},Y{m});
            out1 = f;
            out2 = v;
    end

%     patch('Faces',f,'Vertices',v,'FaceColor','r',...
%         'EdgeColor','none','facealpha',0.5)
end
% clear m k
