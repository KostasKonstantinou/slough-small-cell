function poin = arraOfObj2objOfArr(poin)
%% Convert array of objects to object of arrays
% arraOfObj2objOfArr converts array of objects to object of arrays
%
% * v0.1 KK 16Aug17 Added header

fields = fieldnames(poin);

% poin2 = POIN();
for n = 1 : length(fields)
    poin2.(fields{n}) = [poin.(fields{n})].';
end

poin = poin2;
end