function Fu = Jakes(pcE,sigmaOverN,mode)
% Jakes calculates the fraction of the area above a threshold
%
%       INPUT:
%           pcE        : the desired edge probability
%           sigmaOverN : the sigma over n (sigma is the standard deviation and n is the order of
%                        signal strength propagation
%           mode       : if the calculation is for area or road
%
%       OUTPUT:
%           Fu         : the coverage percentage
%
% * v0.1 Oct16 AK   Created

[ sigmaOverN, pcE ] = meshgrid( sigmaOverN, pcE );

a = erfinv( 1 - 2 * pcE );

b = 10 * log10( exp(1) ) / sqrt( 2 ) ./ sigmaOverN;

Fu = zeros( size( pcE ) );

for n= 1 : numel( sigmaOverN )
    switch mode
        case 'area'
            Fu( n ) = .5 * ( 1 - erf( a( n ) ) + exp( ( 1 - 2 * a( n ) .* b( n ) )./ b( n ).^2 ) .* ( 1 - erf( ( 1 - a( n ) * b( n ) )./ b( n ) ) ) );
        case 'road'
            Fu( n ) = .5 * ( 1 - erf( a( n ) ) + exp( ( 1 - 4 * a( n ) .* b( n ) ) ./ 4 ./ b( n ).^2 ) .* ( 1 - erf( ( 1 - 2 * a( n ) * b( n ) ) ./ 2 ./b( n ) ) ) );
    end
end