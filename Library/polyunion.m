function [x2,y2] = polyunion(x,y,sty)
%% Calculate the union of polygons
% polyunion calculates the union of polygons provided in NaN-delimited
% format, x and y. x2 and y2 are in provided in cell array format.
%
% * v0.1 KK 11Jun17 Created

switch sty
    case 'non overlapping'
        % This code will only work OK if the input polygons are
        % non-overlapping. This is OK if the input polygons have been
        % created by the developer, for example if pixel union is the
        % requirement.
        [Ycells,Xcells] = polysplit(y,x);
        N = floor(length(Xcells)./2);
        N = N + find(ispolycw(Xcells(N+1:end),Ycells(N+1:end)),1,'first') - 1;
        P1 = struct('x',Xcells(1:N),'y',Ycells(1:N),...
            'hole',num2cell(double(~ispolycw(Xcells(1:N),Ycells(1:N)))));
        P2 = struct('x',Xcells(N+1:end),'y',Ycells(N+1:end),...
            'hole',num2cell(double(~ispolycw(Xcells(N+1:end),Ycells(N+1:end)))));
        OutPol = PolygonClip(P1,P2,3);
        A = arrayfun(@(x) polyarea(x.x,x.y),OutPol);
        OutPol(A<1) = [];
        [x2,y2] = PolygonClip2ml(OutPol,'XY');
    case 'potentially overlapping'
        % This code is slower but allows overlapping input polygons. Use
        % this if unsure if the polygons are overlapping.
        
        % Split the polygons into cells and split them in two
        [Ycells,Xcells] = polysplit(y,x);
        isPolyCW = ispolycw(Xcells,Ycells);
        isPolyCW_f = [find(ispolycw(Xcells,Ycells));length(Xcells)+1];

        % The array of P1 polygons cannot have overlapping entries
        P1 = struct('x',[1 1 1 1],'y',[1 1 1 1],'hole',0);
        for n = 1 : length(isPolyCW_f)-1
            IND = isPolyCW_f(n) : isPolyCW_f(n+1)-1;
            P1 = PolygonClip(P1,struct('x',Xcells(IND),'y',Ycells(IND),...
                'hole',num2cell(double(isPolyCW(IND)))),3);
        end
        A = arrayfun(@(x) polyarea(x.x,x.y),P1);
        P1(A./max(A)<1e-3) = [];
        [x2,y2] = PolygonClip2ml(P1,'XY');
end

% Plot P1
% figure
% for n = 1 : length(P1)
%     if P1(n).hole == 0
%         patch(P1(n).x,P1(n).y,'r','facealpha',0.5)
%     else
%         patch(P1(n).x,P1(n).y,'b','facealpha',0.5)
%     end
% end
% for n = 1 : length(P1)
%     text(mean(P1(n).x),mean(P1(n).y),num2str(n))
% end

% Plot result
% figure
% plot(x2{1},y2{1})
