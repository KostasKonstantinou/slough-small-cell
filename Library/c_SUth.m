function [SUth,SE,NLayers] = c_SUth(SINR,Sim,L,scen,XL,bareMeta,...
    AeCouMS,fam,tn,fn,isPlot)
%% [Title]
% XL is 1 for DL and 2 uplink
%
% * v0.1 KK 32Jun16 Created
% * v0.2 KK 22Sep16 Added 24GHz and UL
% 
% bareMeta = struct('pwrAmpClas','macrocell','situ','outd','AeCou',2,'FBMC',false)
% c_SUth(4.59,Sim,3,[2 5 2100],1,1,bareMeta,AeCouMS,'capa','MBB',1,2,false)
% c_SUth(-1.1,Sim,3,[2 5 2100],1,1,bareMeta,AeCouMS,'cov','MBB',1,2,false)

%% Constants
subBW = 15;  % this is the size of the subcarrier in kHz

%% Simplifications
TDDcfg_ = Sim.linBud.TDDconfig_;
SE_max = Sim.linBud.SE_max(XL);
SNIRmin = Sim.linBud.SNIRmin(XL);
pwrAmpClas = bareMeta.pwrAmpClas;
situ = bareMeta.situ;
celGeomFact = Sim.linBud.(pwrAmpClas).(situ).celGeomFact;

switch XL
    case 1
        OHpc_ = Sim.linBud.framStru.DL{L};
        NantTx = bareMeta.AeCou;
        NantRx = AeCouMS;
    case 2
        OHpc_ = Sim.linBud.framStru.UL;
        NantTx = AeCouMS;
        NantRx = bareMeta.AeCou;
end

%% Calculations
% Define duplex mode in string format
switch scen(1)
    case 1
        XDD = 'TDD';
    case 2
        XDD = 'FDD';
end

% Define style.
% The style has become obsolete. There is one SINR vs SE conversion,
% regardless of if estimating SIRNthres from a desired SEthres or
% estimating achieved SE from an achieved SINR. Note that at the time of
% writting the switch of hte latter is still off, i.e. the achieved SE is
% set from csv input to correspond to cell-average, rather than estiamted
% from achieves SINR.
% MU-MIMO is assumed because CAPisce currently does not know the number of
% users.
% The QoS factor has been removed from here because it is inserted at the
% conversion between offered traffic and required capacity.
% switch styl
%     case 'cov'
%         XU_MIMO = 'SU_MIMO';
%         reaTraf = 1 .* Sim.dem.trafVol.(fam).CEtput.facto4spaSlot4lowLaten(tn);
%     case 'capa'
%         XU_MIMO = 'MU_MIMO';
%         reaTraf = Sim.dem.trafVol.(fam).CEtput.servFacto(tn) .* ...
%             Sim.dem.trafVol.(fam).CEtput.facto4spaSlot4lowLaten(tn);
% end
XU_MIMO = 'MU_MIMO';
reaTraf = Sim.dem.trafVol.(fam).CEtput.facto4spaSlot4lowLaten(tn);

% Calculate a
switch XL
    case 1
        a = Sim.linBud.SEmean.(XU_MIMO).(XDD){4,3} ./ 2 ./ ...
            log2(10.^(Sim.linBud.SINRmean(1)./10)+1);
    case 2
        a = Sim.linBud.SEmean.(XU_MIMO).(XDD){4,3} ./ 2 ./ ...
            log2(10.^(Sim.linBud.SINRmean(1)./10)+1) ./ ...
            Sim.linBud.SINRmean(2);
end
a = a .* reaTraf .* celGeomFact .* Sim.linBud.freqFacto(fn);
SE_max = SE_max .* reaTraf .* celGeomFact .* Sim.linBud.freqFacto(fn);

% Allow for 64QAM in uplink and 256QAM in downlink
switch XL
    case 1
        % DL
        SE_max = SE_max .* 5.25 ./ 3.2;
    case 2
        % UL
        SE_max = SE_max .* 5.25 ./ 3.2;
end

if bareMeta.FBMC
    a = Sim.linBud.FBMC(XL) .* a;
end

if scen(1) == 1
    % TDD
    TDDcfg = scen(4);
    switch XL
        case 1
            TDDpc = TDDcfg_(TDDcfg_(:,1)==TDDcfg,3) ./ 10 + ...
                TDDcfg_(TDDcfg_(:,1)==TDDcfg,5) ./ 10 .* 0.4;
        case 2
            TDDpc = TDDcfg_(TDDcfg_(:,1)==TDDcfg,4) ./ 10;
    end
    
    switch XL
        case 1
            % DL
            IND = OHpc_(1,:) == scen(1);
            assert(sum(IND)==1,'Code this!')
            OHpc = OHpc_(3,IND);
        case 2
            % UL
            OHpc = OHpc_;
    end
else
    % FDD
    TDDpc = NaN;
    
    switch XL
        case 1
            % DL
            BW = min(scen(2),20);
            IND = all(OHpc_(1:2,:) == repmat([scen(1);BW],1,size(OHpc_,2)));
            assert(sum(IND)==1,'Code this!')
            OHpc = OHpc_(3,IND);
        case 2
            % UL
            OHpc = OHpc_;
    end
end

NLayers_ = [1 2 4 8];
NLayers_(NLayers_>NantTx) = [];

SE = zeros(length(NLayers_),length(SINR));
for n = 1 : length(NLayers_)
    [mulFact,divGain] = give_mulFact_divGain(Sim,XDD,NantTx,NantRx,...
        NLayers_(n),XU_MIMO);
    SE(n,:) = log(10.^((SINR+divGain)./10)+1)./log(2) .* mulFact .* a;
    SE(n,SE(n,:)>SE_max.*mulFact) = SE_max .* mulFact;
    SE(n,SINR+divGain<SNIRmin) = 0;
end

% % spatial multiplexing
% [mulFact,divGain] = give_mulFact_divGain(Sim,XDD,NantTx,NantRx,XU_MIMO);
% SE1 = log(10.^((SINR+divGain)./10)+1)./log(2) .* mulFact .* a;
% SE1(SE1>SE_max.*mulFact) = SE_max .* mulFact;
% SE1(SINR+divGain<SNIRmin) = 0;
% 
% % diversity
% M_ = min(NantTx,NantRx) ./ [2 4 6 8];
% M_(M_<1) = [];
% SE2 = zeros(length(M_),length(SINR));
% for m = 1 : length(M_)
%     [mulFact,divGain] = give_mulFact_divGain(Sim,XDD,M_(m),NantRx,XU_MIMO);
%     
%     SE2(m,:) = log(10.^((SINR+divGain)./10)+1)./log(2) .* mulFact .* a;
%     SE2(m,SE2(m,:)>SE_max.*mulFact) = SE_max .* mulFact;
%     SE2(m,SINR+divGain<SNIRmin) = 0;
% end

if isPlot
%     figure('PaperPositionMode','auto','unit','in',...
%         'pos',[1 1 7.5 2.63./3.5.*7.5])
    figure('PaperPositionMode','auto','unit','in',...
        'pos',[1 1 3.5 2.63])
    xlabel('SINR  (dB)')
    ylabel('Spectral efficiency  (bit/s/Hz)')
    hold on
    grid on
    for m = 1 : length(NLayers_)
        plot(SINR,SE(m,:),'disp',[num2str(NLayers_(m)) ' layers'],...
            'linewidth',1)
    end
    legend('location','northwest')
    set(gca,'xlim',[-20 35],'ylim',[0 30],...
        'xtick',[-20:5:35],'ytick',[0:5:30])
    print(gcf,fullfile('OutpM','SINR vs SE'),'-djpeg','-r300')
end

[SE,I] = max(SE,[],1);
NLayers = NLayers_(I);

if scen(1) == 1
    SE = SE .* TDDpc;
end

ThPerDatRB = (subBW.*12) .* SE ./ 1000;  % Mbps
% if scen(1) == 1
%     ThPerDatRB = ThPerDatRB .* TDDpc;
% end

A = (1./Sim.utilisN./2 : 1./Sim.utilisN : 1);
SUth = bsxfun(@times,scen(2).*5.*ThPerDatRB.*(1-OHpc),A.');
end

%%
function [mulFact,divGain] = give_mulFact_divGain(Sim,XDD,M,N,NLayers,...
    XU_MIMO)
%% [Title]
% XL is 1 for DL and 2 uplink
%
% * v0.1 KK 32Jun16 Created

% M = min([M,N,NLayers]);

if M < N
    col = Cell2Vec(Sim.linBud.SEmean.(XU_MIMO).(XDD)(2,3:end)) == M;
%     divGain = 10 .* log10(min(N./M,4));
    divGain = 10 .* log10(N./M);
else
    col = Cell2Vec(Sim.linBud.SEmean.(XU_MIMO).(XDD)(2,3:end)) == N;
    divGain = 0;
end

if any(col)
    if M < N
        row = Cell2Vec(Sim.linBud.SEmean.(XU_MIMO).(XDD)(3:end,2)).' == M;
    else
        row = Cell2Vec(Sim.linBud.SEmean.(XU_MIMO).(XDD)(3:end,2)).' == M;
    end
    
    tmp = Sim.linBud.SEmean.(XU_MIMO).(XDD){[false(2,1);row],...
        [false(1,2),col]};
    if ischar(tmp)
        error(['MIMO ' num2str(M) 'x' num2str(N) ...
            ' not defined in link budget'])
    elseif isnan(tmp)
        error('The value cannot be NaN')
    else
        % OK
    end
    
    mulFact = tmp ./ Sim.linBud.SEmean.(XU_MIMO).(XDD){4,3} .* 2;
else
    mulFact = 1;
end
end
