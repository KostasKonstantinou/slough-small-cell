function [e2,n2] = osgb_en_etrs_en(e1,n1,ostn)
% [e2, n2] = osgb_en_etrs_en (e1, n1, [ostn]) returns etrs Easting and Northing of an 
% an osgb easting and northing -- using iterative calls using OSTN look-up interpolation method
%
% Inputs:  e1 - OSGB easting; n1 OSGB northing
% Outputs:  e2 - ETRS easting; n2 ETRS northing
%
%
% see: http://www.ordnancesurvey.co.uk/business-and-government/help-and-support/navigation-technology/os-net/formats-for-developers.html
% Transformations and OSGM02
%
% 	C.Chambers - 27 September 2013
% 	K.Konstantinou - 18 October 2013 Allow matrix inputs
% 	K.Konstantinou - 05 December 2013 ostn
%  C.Chambers - 24 April 2014 - fix errors on iterative convergence.
%  C.Chambers - 2 Oct 2014.  Inserted a loop count to detect non-convergence

%test data
%e1=651409.792;
%n1=313177.448;
%
% m1=[431955 279423];   With these data as input the max_delta_delta > 1.5cm.
decay=0.96;

DEBUG=0;

if nargin < 3
    load ostn
elseif isempty(ostn)
    load ostn
end

ip_e1=e1;       % this is the input osgb
ip_n1=n1;

% get an initial estimate of the etrs co-ords based on OSGB values
% a large correction can be done in this first step....
[e_e2,e_n2] = etrs_en_osgb_en(ip_e1,ip_n1,[],ostn);

factor=0.5;
delta_e=(e_e2-ip_e1);
delta_n=(e_n2-ip_n1);

if DEBUG
    [ip_e1 ip_n1 delta_e delta_n]   % used for debugging
end

%e2=e1-delta_e_old;
%n2=n1-delta_n_old;
e1=e1-delta_e*factor; n1=n1-delta_n*factor;      % make a large correction.
%
% now we iterate until error sufficiently small error based on changes to the correction values
count=0;
max_delta_delta=10;
while ((max_delta_delta > 0.001) & (count < 100))
    [e2,n2] = etrs_en_osgb_en(e1,n1,[],ostn);
    delta_e=e2-ip_e1;
    delta_n=n2-ip_n1;
    
    if DEBUG
        [count e1 n1 delta_e delta_n]
    end
    
    factor=factor*decay;
    e1=e1 - factor*delta_e;
    n1=n1 - factor*delta_n;
    
    %delta_delta_e = delta_e - delta_e_old;
    %delta_delta_n = delta_n - delta_n_old;
    %e2=e1-delta_delta_e;
    %n2=n1-delta_delta_n;
    %delta_e_old=delta_e;   e1=e2;
    %delta_n_old=delta_n;   n1=n2;
        
    %max_delta_delta = max(reshape(abs([delta_e_old-delta_e,...
    %        delta_n_old-delta_n]),[],1));
    %tmp=abs([delta_e_old - delta_e, delta_n_old - delta_n]);
    max_delta_delta=max(max(abs([delta_e, delta_n]))); 
    count = count+1;
end
e2=e1;
n2=n1;       %return the values that provide the inverse answers!!
if DEBUG
    max_delta_delta
end

%[e1, n1]= etrs_en_osgb_en(e2,n2,ostn)  -- Just a check.
return;




