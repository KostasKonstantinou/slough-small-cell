function [e, n] = etrs_ll_en (lat, lon)
% [E, N] = etrs_ll_en (lat, lon) returns Easting and Northing of an ETRS latitude and longitue location.
% (easting and northing) for points given by latitude and longitude 
%
% Inputs:  lat and lon are assumed to be in ETRS (almost the same as WGS84)
%	lat - latitude (in degrees (decimal))
%	lon - longitude (in degrees (decimal))
%
% see: http://www.ordnancesurvey.co.uk/business-and-government/help-and-support/navigation-technology/os-net/formats-for-developers.html
% Transformations and OSGM02
%
% 	C.Chambers - 27 September 2013
%           updated Aug 2014


%
% test input
%%lat=52+ (39/60) + (27.2531/3600)
%lon= 1 + 43/60 + (4.5177/3600)

phi    = lat .* (pi/180); % convert arguments to radians
lambda = lon .* (pi/180);

a = 6378137.000;	% GRS80 semi major axis a
b = 6356752.3141;     % GRS80 semi-minor axis b
F0 = 0.9996012717;    % NatGrid scale factor on central meridian
phi0 = 49*pi/180;     % \phi_0 
lambda0 = -2*pi/180;  % \lambda_0
N0 = -100000;         % NatGrid true origin 
E0 =  400000;         % northing & easting of true origin, metres
e2 = 1 - (b*b)/(a*a); % eccentricity squared
  
 
n = (a-b)/(a+b);                                             % C1
e2sin2phi = e2 * sin(phi).^2;
nu  = (a*F0) * (1-e2sin2phi).^(-0.5);
rho = (a*F0) * (1 - e2).*(1-e2sin2phi).^(-1.5);
  
eta2 = (nu./rho) - 1;                                        %C2
  
d_phi = phi - phi0;
p_phi = phi + phi0;
M = (b*F0) * ( ...
     (1 + n + 1.25*(n^2 + n^3))* (d_phi) - ...
     3*(n + n^2 + (0.875*n^3)) * (sin(d_phi) .* cos(p_phi)) + ...
     1.875*(n^2 + n^3)* (sin(2*d_phi).* cos(2*p_phi)) - ...
     (35/24)*n^3 * (sin(3*d_phi) .* cos(3*p_phi)) ...
    );                                                        %C3
I    = M + N0;
II   = (nu / 2) .* sin(phi) .* cos(phi);
III  = (nu / 24) .* sin(phi) .* (cos(phi).^3) .* (5 - tan(phi).^2 + (9 * eta2));
IIIA = (nu / 720) .* sin(phi) .* cos(phi).^5 .* ...
      (61 - (58 .* tan(phi).^2) + tan(phi).^4);
IV = nu .* cos(phi);
V  = (nu / 6) .* (cos(phi).^3 .* (nu./rho - tan(phi).^2));
VI = (nu / 120) .* cos(phi).^5 .* ...
      (5 - 18*tan(phi).^2 + tan(phi).^4 + 14*eta2 - (58*eta2).*tan(phi).^2);
  
d_lambda=lambda - lambda0;

n = I + II.* (d_lambda.^2) + III .* (d_lambda.^4) + IIIA.*(d_lambda.^6); %C4
e = E0 + IV .* d_lambda + V.*(d_lambda.^3) + VI.*(d_lambda.^5);  %C5

return