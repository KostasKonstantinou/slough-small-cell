function [e2,n2,h] = latLon2easNor(lat,lon,H,ostn)

[e2,n2] = etrs_ll_en(lat,lon);
if ~isempty(H)
    [e2,n2,h] = etrs_en_osgb_en(e2,n2,H,ostn);
else
    [e2,n2] = etrs_en_osgb_en(e2,n2,H,ostn);
    h = [];
end
