function [e,n,h] = etrs_en_osgb_en(x,y,H,ostn)
%% OSTN02 coordinate transformation (part-2) 
% This function returns OSGB36 Easting and Northing of an ETRS89 easting
% and northing -- using OSTN look-up interpolation method.
% The etrs_en_osgb_en function is called after etrs_ll_en.
% The ETRS89 easting and northing is first obtained using the
% algorithm, GRS80 ellipsoid parameters and National Grid projection
% parameters defined in etrs_ll_en.
%
% [1] Ordnance Survey, Transformations and OSGM02: User guide, v2.4, Dec13 
%
% * v3.0 RW 10Apr15 Released to Ofcom

%% Load the look up table
if nargin < 4
    load ostn
elseif isempty(ostn)
    load ostn
end

%% Define parameters according to Chapter 3 in [1], page 13
east_index = floor(x.*0.001);
north_index = floor(y.*0.001);

% Define se0, sn0, sg0
record_number = east_index + north_index.*701 + 1;
se0 = ostn(record_number,4);
sn0 = ostn(record_number,5);
sg0 = ostn(record_number,6:7);

% Define se1, sn1, sg1
record_number = east_index + 1 + north_index.*701 + 1;
se1 = ostn(record_number,4);
sn1 = ostn(record_number,5);
sg1 = ostn(record_number,6:7);

% Define se2, sn2, sg2
record_number = east_index + 1 + (north_index+1).*701 + 1;
se2 = ostn(record_number,4);
sn2 = ostn(record_number,5);
sg2 = ostn(record_number,6:7);

% Define se3, sn3, sg3
record_number = east_index + (north_index+1).*701 + 1;
se3 = ostn(record_number,4);
sn3 = ostn(record_number,5);
sg3 = ostn(record_number,6:7);

% Calculate offset
dx = x - east_index.*1000;
dy = y - north_index.*1000;

% Calculate the value of the east shift (se), north shift (sn) at the point x, y
t = dx .* 0.001;
u = dy .* 0.001;
se =(1 - t).*(1 - u).*se0 + t.*(1 - u).*se1 + t.*u.*se2 + (1-t).*u.*se3;
sn =(1 - t).*(1 - u).*sn0 + t.*(1 - u).*sn1 + t.*u.*sn2 + (1-t).*u.*sn3;

% Add the shifts to point x, y to give the National Grid position
e = x + se;
n = y + sn;

% Calculate the orthometric height using the Ordnance Survey Geoid model:
% OSGM02
if nargout == 3
    if sg0(2)==0 || sg1(2)==0 || sg2(2)==0 || sg3(2)==0
        s1 = ['The coordinate falls more than 10 km offshore or 2 km ' ...
            'beyond the Northern Ireland/Republic of Ireland border and ' ...
            'is not covered by the OSGM02 model'];
    %     h = errordlg(s1);
    %     uiwait(h)
    %     error(s1)
        N = NaN;
    else
        N = (1-t).*(1-u).*sg0(1) + t.*(1-u).*sg1(1) + t.*u.*sg2(1) + ...
            (1-t).*u.*sg3(1);
    end
    h = H - N;
end
