function [lat, lon] = etrs_en_ll (East, North)
% function [lat, lon] = etrs_en_ll (e, n) returns ETRS Latitude and longitude given the ETRS Easting and Northing
% of a location.
%
% Inputs:
%        Easting and Northing in ETRS ellipsoid
% Outputs:
%        lat, lon (in decimal degrees) (in ETRS Ellipsoid)
%
% see: http://www.ordnancesurvey.co.uk/business-and-government/help-and-support/navigation-technology/os-net/formats-for-developers.html
% Transformations and OSGM02
%
% 	C.Chambers - 27 September 2013
%       updated - April 2014.


%
% test input
%%lat=52+ (39/60) + (27.2531/3600)
%lon= 1 + 43/60 + (4.5177/3600)

a = 6378137.000;	% GRS80 semi major axis a
b = 6356752.3141;     % GRS80 semi-minor axis b
F0 = 0.9996012717;    % NatGrid scale factor on central meridian
phi0 = 49*pi/180;     % \phi_0 
lambda0 = -2*pi/180;  % \lambda_0
N0 = -100000;         % NatGrid true origin 
E0 =  400000;         % northing & easting of true origin, metres
e2 = 1 - (b*b)/(a*a); % eccentricity squared
  
n = (a-b)/(a+b);              % C1
n2 = n*n; 
n3 = n*n*n;

    % initialise some variables.
M   = double(zeros(size(North)));
inv_aF0= 1/(a*F0);
phi=(North-N0)*inv_aF0 + phi0;         % initial estimate of phi
count=0;
while (any(North-N0-M >= 0.00001))
    count=count+1;
    d_phi = phi - phi0;
    p_phi = phi + phi0;

    M = (b*F0) * ( ...
      (1 + n + 1.25*(n^2 + n^3))* (d_phi) - ...
      3*(n + n^2 + (0.875*n^3)) * (sin(d_phi) .* cos(p_phi)) + ...
      1.875*(n^2 + n^3)* (sin(2*d_phi).* cos(2*p_phi)) - ...
      (35/24)*n^3 * (sin(3*d_phi) .* cos(3*p_phi)) ...
      );                                   % meridional arc
      
    phi = inv_aF0*(North-N0-M) + phi;           % C6 C7
    if (count>= 100)
        max_error = max(max(North-N0-M))
        error ('etra_en_ll not converging');
        break;
    end
end  % ie until < 0.01mm

    % C2 below
e2sin2phi = e2 * sin(phi).^2;
nu  = (a*F0) * (1-e2sin2phi).^(-0.5);           % transverse radius of curvature
rho = (a*F0) * (1 - e2).*(1-e2sin2phi).^(-1.5); % meridional radius of curvature
eta2 = (nu./rho) - 1 ;  
  
tanLat = tan(phi);
tan2lat = tanLat.*tanLat; tan4lat = tan2lat.*tan2lat; tan6lat = tan4lat.*tan2lat;
secLat = 1./cos(phi);
nu3 = nu.^3; nu5 = nu.^5; nu7 = nu.^7;
VII = tanLat./(2*rho.*nu);
VIII = tanLat./(24*rho.*nu3).*(5+3*tan2lat+eta2-9*tan2lat.*eta2);
IX = tanLat./(720*rho.*nu5).*(61+90*tan2lat+45*tan4lat);
X = secLat./nu;
XI = secLat./(6*nu3).*(nu./rho+2*tan2lat);
XII = secLat./(120*nu5).*(5+28*tan2lat+24*tan4lat);
XIIA = secLat./(5040*nu7).*(61+662*tan2lat+1320*tan4lat+720*tan6lat);

dE = (East-E0); dE2 = dE.*dE; dE3 = dE2.*dE; dE4 = dE2.*dE2; dE5 = dE3.*dE2; dE6 = dE3.*dE3; dE7 = dE4.*dE3;
lat = phi - VII.*dE2 + VIII.*dE4 - IX.*dE6;             % C8
lon = lambda0 + X.*dE - XI.*dE3 + XII.*dE5 - XIIA.*dE7;    % C9


lon = lon .* (180/pi);
lat = lat .* (180/pi); 


return