function main()
% This is the main function that [...]
% =============================
% by Kostas Konstantinou, Mar 2015
%
% The purpose of this function is [...]

[~,~,raw] = xlsread('OSTN02_OSGM02Tests_Out.csv');

addpath('D:\Dropbox (Real Wireless)\MATLAB\More accurate coordinate conversion')
load ostn

out1 = cell(size(raw,1)-1,1);
for n = 1 : size(raw,1)-1
    lat = cell2mat(raw(n+1,6:8)) * [1;1./60;1./3600];
    lon = (1-2.*strcmp(raw{n+1,9},'W')) .* ...
        cell2mat(raw(n+1,10:12)) * [1;1./60;1./3600];
    
    [E,N] = latLon2easNor(lat,lon,0,ostn);
    
    if ~strcmp(raw{n+1,16},'N/A point outside polygon')
        out1{n} = sqrt((E-raw{n+1,16}).^2 + (N-raw{n+1,17}).^2);
    else
        out1{n} = 'N/A point outside polygon';
    end
end
clear n

clear ostn
rmpath('D:\Dropbox (Real Wireless)\MATLAB\More accurate coordinate conversion')
