function [lat2,lon2] = easNor2latLon(e2,n2,ostn)

% v0.1 KK 05Dec2012 Created

if nargin < 3
    ostn = [];
end

[e3,n3] = osgb_en_etrs_en(e2,n2,ostn);
[lat2,lon2] = etrs_en_ll(e3,n3);
