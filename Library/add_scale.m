function han = add_scale(sty,x,y,unit,han)
%% Add or modify map scale
% add_scale('xy') adds a map scale to the lower left corner. 'xy' is an
% idetifier that the map's projection is rectilinear and the map units are
% planar (not geographic, not geodesic).
% add_scale('xy',0.60,0.05,'km') forces the scale to be expressed in km.
% han = add_scale(...) returns an array of handles to the scale elements.
% The elements are grouped, and if the end-user selects an element then all
% are selected, for ease of deletion. Current version does not support
% relocation of the scale, neither as input to this function, nor at
% end-user level (callback functions are needed in this case).
% add_scale('xy','km',han) deletes the current scale and redraws it. This
% is useful when panning or zooming on the map.
%
% * v0.1 Kostas Konstantinou Jan 2014

% If the handle has been provided, then this is because the scale needs
% updating
if nargin == 5
    delete(han)
end

% Create group object
hScaleRuler = hggroup();

% Find the extent of the map
Xlim = xlim;
Ylim = ylim;

% Set the location of the scale
XLoc = Xlim(1) + diff(Xlim).*x;
YLoc = Ylim(1) + diff(Ylim).*y;

% []
switch sty
    case 'xy'
        % Calculate the location of 4 major and 5 minor ticks. The labels
        % of major ticks are 1,2,3,4 or 10,20,30,40 etc.
        MajorTick = linspace(0,10.^floor(log10(min(diff(Xlim),...
            diff(Ylim)))).*0.4,4+1);
%         MajorTick = linspace(0,10.^floor(log10(min(diff(Xlim),...
%             diff(Ylim)))).*0.8,4+1);
        MinorTick = mean([MajorTick(1:end-1);MajorTick(2:end)]);
        MinorTick = [MinorTick(1:end-1) MajorTick(end-1) MinorTick(end)];
        MajorTick(end-1) = [];
        
        % Calculate the length of major and minor ticks
        MajorTickLength = diff(MajorTick(1:2)) ./ 5;
        MinorTickLength = diff(MinorTick(1:2)) ./ 10;
    case 'll'
        error('Find this implementation from another project!')
    otherwise
        error('Undefined')
end

% Suggest a unit for the scale
if ~exist('unit','var')
    switch MajorTick(2)
        case {100,1000}
            unit = 'km';
        case {10,1}
            unit = 'm';
        otherwise
            beep
            disp('Insert this!')
            keyboard
    end
end

% Current version supports only km as the scale unit
switch unit
    case {'km','m'}
    case 'mi'
        error('Find this implementation from another project!')
        km = distdim(km,'km','mi');
    otherwise
        sdffd
end

% Plot base line and major ticks
switch sty
    case 'xy'
        x = XLoc + MajorTick;
        y = YLoc .* ones(size(x));
    case 'll'
        error('Find this implementation from another project!')
        [~,lonout] = reckon('rh',YLoc,lon,km0(IX).*1e3,90,E);
        lon = [lon lonout];
end
han(1) = plot(x([1 end]),y([1 end]),'k','linewidth',1,'Parent',hScaleRuler);
for n = 1 : length(x)
    han(n+1) = plot([x(n) x(n)],y(n)+[0 MajorTickLength],'k',...
        'linewidth',1,'Parent',hScaleRuler);
    switch unit
      case 'km'
        MajorTickFactor = 1 ./ 1e3;
      case 'm'
        MajorTickFactor = 1;
      otherwise
        error('Undefined')
    end
    han(length(x)+1+n) = text(x(n),y(n)+MajorTickLength,...
        num2str(MajorTick(n).*MajorTickFactor),'hor','center',...
        'ver','bottom','margin',1,'FontSize',9,'Parent',hScaleRuler);
end
switch unit
    case 'km'
        str1 = ' km';
    case 'm'
        str1 = ' m';
    case 'mi'
        str1 = ' mi';
    otherwise
        sdfsdf
end
% pos = han(end).Position(1);
w1 = han(end).Extent(3);
han(end).String = [han(end).String str1];
w2 = han(end).Extent(3);
han(end).Position(1) = han(end).Position(1) + (w2-w1)./2;
% delete(han)

% Plot minor ticks
switch sty
    case 'xy'
        x = XLoc + MinorTick;
        y = YLoc .* ones(size(x));
    case 'll'
        error('Find this implementation from another project!')
        [~,lonout] = reckon('rh',YLoc,lon,km0(IX).*1e3,90,E);
        lon = [lon lonout];
end
for n = 1 : length(x)
    han(2*length(MajorTick)+2+n) = plot([x(n) x(n)],...
        y(n)+[0 MinorTickLength],'k','linewidth',1,'Parent',hScaleRuler);
end

% Calculate the extent of the white background, x-axis
extent = [get(han(6),'Extent')
    get(han(9),'Extent')];
X = [extent(1,1);extent(1,1);extent(2,1)+extent(2,3);extent(2,1)+extent(2,3)];

% Calculate the extent of the white background, y-axis.
% 1 point = 1/72 inches.
% Text margin is hardcoded above at 1 point, thus 1 point is also allowed
% as margin under the base line.
assert(strcmp(get(gcf,'unit'),'inches'),'The following line will not work!')
pos = get(gcf,'pos');
if pos(3) > pos(4)
    Margin = diff(Xlim) ./ pos(3) .* 1./72;
else
    Margin = diff(Ylim) ./ pos(4) .* 1./72;
end
Y = [1 0;0 1;0 1;1 0]*[han(1).YData(1)-Margin; extent(1,2)+extent(1,4)];

% Plot a white background
han(16) = patch(X,Y,ones(4,1),'edgecolor','none','facecolor','w','Parent',hScaleRuler);
uistack(han(16),'down',length(han)-1)
% han(6).EdgeColor = 'k';
% han(6).EdgeColor = 'none';
% delete(han(16))
% delete(han(:))
