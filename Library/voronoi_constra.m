function [C,V,A] = voronoi_constra(XY,bs_ext,radius)
% Constrained Voronoi
% Returns Voronoi vertices V and the Voronoi cells C of the Voronoi diagram
% of XY, constrained by polygon bs_ext. radius is currently not utilised,
% but it may be utilised in the future if an update on the code forwards
% this variable into voronoi_finite_polygons_2d. A is the triangulation
% connectivity matrix, in sparse logical NxN format, where N is the number
% of XY points and true represents triangulation connectivity. In other
% words, A is true for the 1st tier of neighbours.
% 
% Example 1: 
% Compute constrained Voronoi vertices and diagram cells
%   
% XY = [566725 5934275
%     567675 5932875
%     560425 5933175
%     547525 5935625];
% bs_ext = [518463 5906563
%     518463 5964687
%     576587 5964687
%     576587 5906563
%     518463 5906563];
% [C,V] = voronoi_constra(XY,bs_ext)
% 
% figure
% axis equal
% hold on
% for n = 1 : length(C)
%     patch(V(C{n},1),V(C{n},2),zeros(length(C{n}),1),'r')
% end
% plot(XY(:,1),XY(:,2),'k.')
%
% The above example returns:   
% C = {[6;5;7;9;10;6]
%     [4;3;5;6;4]
%     [5;3;2;7;5]
%     [7;2;1;8;9;7]}
% V = [518463 5906563
%     548688.127906977 5906563
%     562955.020689655 5906563
%     576587 5906563
%     563982.412569691 5931391.63710086
%     576587 5939944.75000000
%     558915.316455696 5960412.27848101
%     518463 5964687
%     559215.882812500 5964687
%     576587 5964687];

% Convert XY to numpy
pointsp = py.numpy.reshape(XY(:).', int32(size(XY)),'F');

% Compute Voronoi tesselation
% If it fails here, add the path with:
% py_addpath(fullfile(Sim.path.prog,'Library'))
vor = py.scipy.spatial.Voronoi(pointsp);
tmp = py.voronoi_finite_polygons_2d.voronoi_finite_polygons_2d(vor);
C = cellfun(@(x) cellfun(@(y) double(y)+1,cell(x).'),cell(tmp{1}).',...
    'UniformOutput',false);
V = reshape(double(py.array.array('d',py.numpy.nditer(tmp{2}))),2,[]).';

% Constrain within bs_ext.
% Either use polybool or PolygonClip. Note that the Voronoi tesselation
% result is not always clockwise. PolygonClip does not need
% conversion to clockwise vertex ordering, because P2.hole=0.
% [x2,y2] = cellfun(@(x) poly2cw(V(x,1),V(x,2)),C,'UniformOutput',false);
% [X,Y] = cellfun(@(x,y) polybool('intersection',bs_ext(:,1),bs_ext(:,2),...
%     x,y),x2,y2,'UniformOutput',false);
% 
P1 = mlToPolygonClip(bs_ext(:,1),bs_ext(:,2));
x2 = cellfun(@(x) V(x,1),C,'UniformOutput',false);
y2 = cellfun(@(x) V(x,2),C,'UniformOutput',false);
X = cell(length(x2),1);
Y = cell(length(y2),1);
for n = 1 : length(x2)
    P2.x = x2{n}.';
    P2.y = y2{n}.';
    P2.hole = 0;  % {x2,y2} are NaN-separated vectors, without NaN, as result of Voronoi tesselation
    P3 = PolygonClip(P1,P2,1);  % case 1; title('A.and.B (standard)')
    if length(P3) == 1
        X{n} = P3.x([1:end,1]);
        Y{n} = P3.y([1:end,1]);
    elseif isempty(P3)
    else
        [tmpX,tmpY] = PolygonClip2ml(P3,'XY');
        [X{n},Y{n}] = polyjoin(tmpX,tmpY);
    end
end

% Convert constrained Voronoi tesselation to polygonal regions C and
% vertices matrix V
[C,V] = poly2cv(X,Y);

% figure
% axis equal
% hold on
% for n = 1 : length(C)
%     [Xcells,Ycells] = polysplit(V(C{n},1),V(C{n},2));
%     for m = 1 : length(Xcells)
%         patch(Xcells{m},Ycells{m},zeros(length(Xcells{m}),1),'r')
%     end
% end
% plot(XY(:,1),XY(:,2),'k.')
% % min_bound = double(py.array.array('d',py.numpy.nditer(vor.min_bound)));
% % max_bound = double(py.array.array('d',py.numpy.nditer(vor.max_bound)));
% % set(gca,'xlim',[min_bound(1) max_bound(1)] + ...
% %     0.1.*[-1 1].*(max_bound(1)-min_bound(1)),...
% %     'ylim',[min_bound(2) max_bound(2)] + ...
% %     0.1.*[-1 1].*(max_bound(2)-min_bound(2)))

% Calculate triangulation connectivity matrix
if nargout == 3
    dt = delaunayTriangulation(XY(:,1),XY(:,2));
    N = size(XY,1);
    ConnectivityList = dt.ConnectivityList;
    A = sparse(ConnectivityList(:,1),ConnectivityList(:,2),true,N,N) | ...
        sparse(ConnectivityList(:,1),ConnectivityList(:,3),true,N,N) | ...
        sparse(ConnectivityList(:,2),ConnectivityList(:,3),true,N,N);
    A = A.' | A;
end

% dt = delaunayTriangulation(XY(:,1),XY(:,2));
% [V,C] = voronoiDiagram(dt);  % vor.vertices but shuffled
% [vx,vy] = voronoi(XY(:,1),XY(:,2));
% 
% [IND1,~] = find(bsxfun(@eq,V(2:end,:)*[1;1j],[1 1j]*[vx(1,:);vy(1,:)]));
% [IND2,col] = find(bsxfun(@eq,V(2:end,:)*[1;1j],[1 1j]*[vx(2,:);vy(2,:)]));
% IND2 = full(sparse(col,ones(size(col)),IND2,size(vx,2),1));
% ridge_vertices = [IND2 IND1];
% 
% N = size(XY,1);
% A = sparse(dt.ConnectivityList(:,1),dt.ConnectivityList(:,2),true,N,N) | ...
%     sparse(dt.ConnectivityList(:,1),dt.ConnectivityList(:,3),true,N,N) | ...
%     sparse(dt.ConnectivityList(:,2),dt.ConnectivityList(:,3),true,N,N);
% A = A.' | A;
% [row,col] = find(triu(A,0));
% ridge_points = [row,col];
% 
% new_regions = [];
% new_vertices = V(2:end,:);
% 
% center = mean(XY,1);
% if nargin == 1
% %     radius = (max(vor(:))-min(vor(:))).*2
%     radius = max(max(XY,[],1)-min(XY,[],1),[],2) .* 2;
% end
% 
% % Construct a map containing all ridges for a given point
% all_ridges = cell(max(ridge_points(:)),1);
% p1 = ridge_points(:,1);
% p2 = ridge_points(:,2);
% v1 = ridge_vertices(:,1);  % Shuffled
% v2 = ridge_vertices(:,2);  % Shuffled
% for n = 1 : length(p1)
%     all_ridges(p1(n)) = {[all_ridges{p1(n)} {[p2(n),v1(n),v2(n)]}]};
%     all_ridges(p2(n)) = {[all_ridges{p2(n)} {[p1(n),v1(n),v2(n)]}]};
% end
% 
% 
% 
% % for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices)
% %     all_ridges.setdefault(p1,[]).append((p2,v1,v2))
% %     all_ridges.setdefault(p2,[]).append((p1,v1,v2))
% % end
% 
% % % Reconstruct infinite regions
% % for p1, region in enumerate(vor.point_region):
% %     vertices = vor.regions[region]
% % 
% %     if all([v >= 0 for v in vertices]):
% %         % finite region
% %         new_regions.append(vertices)
% %         continue
% %     end
% % 
% %     % reconstruct a non-finite region
% %     ridges = all_ridges[p1]
% %     new_region = [v for v in vertices if v >= 0]
% % 
% %     for p2, v1, v2 in ridges
% %         if v2 < 0:
% %             v1, v2 = v2, v1
% %         end
% %         if v1 >= 0:
% %             # finite ridge: already in the region
% %             continue
% %         end
% % 
% %         % Compute the missing endpoint of an infinite ridge
% % 
% %         t = vor.points[p2] - vor.points[p1] % tangent
% %         t /= np.linalg.norm(t)
% %         n = np.array([-t[1], t[0]])  % normal
% % 
% %         midpoint = vor.points[[p1, p2]].mean(axis=0)
% %         direction = np.sign(np.dot(midpoint - center, n)) * n
% %         far_point = vor.vertices[v2] + direction * radius
% % 
% %         new_region.append(len(new_vertices))
% %         new_vertices.append(far_point.tolist())
% %     end
% % 
% %     % sort region counterclockwise
% %     vs = np.asarray([new_vertices[v] for v in new_region])
% %     c = vs.mean(axis=0)
% %     angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
% %     new_region = np.array(new_region)[np.argsort(angles)]
% % 
% %     % finish
% %     new_regions.append(new_region.tolist())
% % end
    