function out11 = EN2NGR(E,N)
%% [Title]
% [Description]
%
% * v1.0 Jon Yearsley 22Apr98 Created
% * v1.1 KK 04Oct11 Amended
% * v1.2 KK 23Aug12 Amended
% * v1.3 KK 24Sep14 Code optimisation
% * v1.4 KK 01Jul15 Allow vector input

%% Ordnance Survey's grid conversion
% 		1st number						
% 		0	1	2	3	4	5	6
% 2nd number	12	HL	HM	HN	HO	HP	JL	JM
% 	11	HQ	HR	HS	HT	HU	JQ	JR
% 	10	HV	HW	HX	HY	HZ	JV	JW
% 	9	NA	NB	NC	ND	NE	OA	OB
% 	8	NF	NG	NH	NJ	NK	OF	OG
% 	7	NL	NM	NN	NO	NP	OL	OM
% 	6	NQ	NR	NS	NT	NU	OQ	OR
% 	5	NV	NW	NX	NY	NZ	OV	OW
% 	4	SA	SB	SC	SD	SE	TA	TB
% 	3	SF	SG	SH	SJ	SK	TF	TG
% 	2	SL	SM	SN	SO	SP	TL	TM
% 	1	SQ	SR	SS	ST	SU	TQ	TR
% 	0	SV	SW	SX	SY	SZ	TV	TW
Data = {'HL' 'HM' 'HN' 'HO' 'HP' 'JL' 'JM'
    'HQ' 'HR' 'HS' 'HT' 'HU' 'JQ' 'JR'
    'HV' 'HW' 'HX' 'HY' 'HZ' 'JV' 'JW'
    'NA' 'NB' 'NC' 'ND' 'NE' 'OA' 'OB'
    'NF' 'NG' 'NH' 'NJ' 'NK' 'OF' 'OG'
    'NL' 'NM' 'NN' 'NO' 'NP' 'OL' 'OM'
    'NQ' 'NR' 'NS' 'NT' 'NU' 'OQ' 'OR'
    'NV' 'NW' 'NX' 'NY' 'NZ' 'OV' 'OW'
    'SA' 'SB' 'SC' 'SD' 'SE' 'TA' 'TB'
    'SF' 'SG' 'SH' 'SJ' 'SK' 'TF' 'TG'
    'SL' 'SM' 'SN' 'SO' 'SP' 'TL' 'TM'
    'SQ' 'SR' 'SS' 'ST' 'SU' 'TQ' 'TR'
    'SV' 'SW' 'SX' 'SY' 'SZ' 'TV' 'TW'};

out11 = strcat(Data(sub2ind(size(Data),13-floor(N./1e5),floor(E./1e5)+1)),...
    cellfun(@(x) num2str(x),num2cell(round(E-floor(E./1e5).*1e5)),...
    'UniformOutput',false),...
    cellfun(@(x) num2str(x),num2cell(round(N-floor(N./1e5).*1e5)),...
    'UniformOutput',false));
% out11 = [Data{13-floor(N./1e5),floor(E./1e5)+1} ...
%     num2str(round(E-floor(E./1e5).*1e5)) ...
%     num2str(round(N-floor(N./1e5).*1e5))];

% % mat = {'HL 00 12';'HM 01 12';'HN 02 12';'HO 03 12';'HP 04 12';'Jl 05 12';...
% %     'HQ 00 11';'HR 01 11';'HS 02 11';'HT 03 11';'HU 04 11';'JQ 05 11';...
% %     'HV 00 10';'HW 01 10';'HX 02 10';'HY 03 10';'HZ 04 10';'JV 05 10';...
% %     'NA 00 09';'NB 01 09';'NC 02 09';'ND 03 09';'NE 04 09';'OA 05 09';...
% %     'NF 00 08';'NG 01 08';'NH 02 08';'NJ 03 08';'NK 04 08';'OF 05 08';...
% %     'NL 00 07';'NM 01 07';'NN 02 07';'NO 03 07';'NP 04 07';'OL 05 07';...
% %     'NQ 00 06';'NR 01 06';'NS 02 06';'NT 03 06';'NU 04 06';'OQ 05 06';...
% %     'NV 00 05';...
% %     'NW 01 05';'NX 02 05';'NY 03 05';'NZ 04 05';'OV 05 05';'OW 06 05';...
% %     'SA 00 04';...
% %     'SB 01 04';'SC 02 04';'SD 03 04';'SE 04 04';'TA 05 04';'TB 06 04';...
% %     'SG 01 03';'SH 02 03';'SJ 03 03';'SK 04 03';'TF 05 03';'TG 06 03';...
% %     'SM 01 02';'SN 02 02';'SO 03 02';'SP 04 02';'TL 05 02';'TM 06 02';...
% %     'SQ 00 01';'SR 01 01';'SS 02 01';'ST 03 01';'SU 04 01';'TQ 05 01';'TR 06 01';...
% %     'SV 00 00';'SW 01 00';'SX 02 00';'SY 03 00';'SZ 04 00';'TV 05 00'};
% % 
% % out11 = mat{cellfun(@(x) strcmp([sprintf('%02d',floor(E./1e5)) ' ' ...
% %     sprintf('%02d',floor(N./1e5))],x(4:8)),mat)}(1:2);
% 
% HL = [00 12]; HM = [01 12]; HN = [02 12]; HO = [03 12]; HP = [04 12];
% Jl = [05 12];
% 
% HQ = [00 11]; HR = [01 11]; HS = [02 11]; HT = [03 11]; HU = [04 11];
% JQ = [05 11];
% 
% HV = [00 10]; HW = [01 10]; HX = [02 10]; HY = [03 10]; HZ = [04 10];
% JV = [05 10];
% 
% MY = [-02 05]; MZ = [-01 05];
% 
% NA = [00 09]; NB = [01 09]; NC = [02 09]; ND = [03 09]; NE = [04 09];
% OA = [05 09];
% 
% NF = [00 08]; NG = [01 08]; NH = [02 08]; NJ = [03 08]; NK = [04 08];
% OF = [05 08];
% 
% NL = [00 07]; NM = [01 07]; NN = [02 07]; NO = [03 07]; NP = [04 07];
% OL = [05 07];
% 
% NQ = [00 06]; NR = [01 06]; NS = [02 06]; NT = [03 06]; NU = [04 06];
% OQ = [05 06];
% 
% NV = [00 05];
% 
% NW = [01 05]; NX = [02 05]; NY = [03 05]; NZ = [04 05]; OV = [05 05];
% OW = [06 05];
% 
% RD = [-02 04]; RE = [-01 04];
% 
% SA = [00 04];
% 
% SB = [01 04]; SC = [02 04]; SD = [03 04]; SE = [04 04]; TA = [05 04];
% TB = [06 04];
% 
% SG = [01 03]; SH = [02 03]; SJ = [03 03]; SK = [04 03]; TF = [05 03];
% TG = [06 03];
% 
% SM = [01 02]; SN = [02 02]; SO = [03 02]; SP = [04 02]; TL = [05 02];
% TM = [06 02];
% 
% SQ = [00 01]; SR = [01 01]; SS = [02 01]; ST = [03 01]; SU = [04 01];
% TQ = [05 01]; TR = [06 01];
% 
% SV = [00 00]; SW = [01 00]; SX = [02 00]; SY = [03 00]; SZ = [04 00];
% TV = [05 00];
% 
% % % IA = [00 04]; IB = [01 04]; IC = [02 04]; ID = [03 04]; IE = [04 04];
% % % 
% % % IF = [00 03]; IG = [01 03]; IH = [02 03]; IJ = [03 03]; IK = [04 03];
% % % 
% % % IL = [00 02]; IM = [01 02]; IN = [02 02]; IO = [03 02]; IP = [04 02];
% % % 
% % % IQ = [00 01]; IR = [01 01]; IS = [02 01]; IT = [03 01]; IU = [04 01];
% % % 
% % % IV = [00 00]; IW = [01 00]; IX = [02 00]; IY = [03 00]; IZ = [04 00];
% 
% %% Calculations
% str = who;
% for n = 1 : length(str)
%     if length(str{n}) == 2
%         eval(['temp = ' str{n} ';'])
%         if all(temp == [floor(E./1e5) floor(N./1e5)])
%             out11 = str{n};
%         end
%     end
% end
% clear n str
