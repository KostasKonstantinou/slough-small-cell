function SINR2tput = c_SINR2tput(Sim)
%% [Title]
% The output has three rows: 1 is SINR in dB, 2 is goodput in Mbit/s, 3 is
% SE in bit/s/Hz
%
% * v0.1 KK 01Jun17 Created
% * v0.2 AK Sep17   Fixed issue with Rx Diversity

% Simppifications
AeCouBS_ = Sim.linBud.AeCouBS_;
AeCouMS_ = Sim.linBud.AeCouMS_;
FBMC_ = Sim.linBud.FBMC_;
BW_ = Sim.linBud.BW_;
rul = Sim.rul;
rulPwrAmpClas = {rul.pwrAmpClas}.';
rulAeCou = cell2mat(arrayfun(@(x) x.upg.AeCou,rul,'UniformOutput',false));
rulBand = cell2mat(arrayfun(@(x) x.upg.Band,rul,'UniformOutput',false));
rulBand(rulBand==8.79) = 10;
rulBand(rulBand==18.467) = 20;

SNIRmin = min(floor(Sim.linBud.SNIRmin.' - ...
    10.*log10(max([AeCouBS_,AeCouMS_]))));
SINR = SNIRmin-0.1 : 0.1 : 30;
SINR2tput = cell(length(Sim.linBud.freq),3,...
    length(Sim.linBud.BS.pwrAmpClas_),length(AeCouBS_),length(AeCouMS_),...
    length(FBMC_),length(Sim.dem.famUseCase_),...
    length(Sim.dem.trafClas_),length(BW_));
for pn = 1 : length(Sim.linBud.BS.pwrAmpClas_)
    for an = 1 : length(AeCouBS_)
        for bn = 1 : length(AeCouMS_)
            for fbn = 1 : length(FBMC_)
%                 bareMeta = struct('pwrAmpClas',Sim.linBud.BS.pwrAmpClas_{pn},...
%                     'situ','outd','AeCou',AeCouBS_(an),'FBMC',FBMC_(fbn));
                bareMeta = struct('pwrAmpClas',Sim.linBud.BS.pwrAmpClas_{pn},...
                    'situ','outd','AeCou',AeCouBS_(an));
                
                for fn = 1 : length(Sim.linBud.freq)
                    for BWn = 1 : length(BW_)
                        scen = [Sim.linBud.dupMode(fn) BW_(BWn) NaN ...
                            Sim.linBud.TDDcfg(fn)];
%                         N = BW_(BWn) .* Sim.linBud.PRButilisCovera(fn) .* 5;
                        
                        % Calculate an appropriate CFI according to the BW.
                        % These are rule of thumb from [lteuniversity] or
                        % [sharenote].
                        if BW_(BWn) < 10
                            % CFI = 3 for 1.4, 3 and 5 MHz system bandwidths
                            L = 3;
                        else
                            % CFI = 2 for 10, 15 and 20 MHz system bandwidth
                            % Use CFI = 1 for Cat 4 or higher max throughput case
                            L = 2;
                        end
                        
%                         for sn = 1 : 2
%                             switch sn
%                                 case 1
%                                     cc = 'cov';
%                                 case 2
%                                     cc = 'capa';
%                             end
                        for fan = 1 : length(Sim.dem.famUseCase_)
                            for tn = 1 : length(Sim.dem.trafClas_)
                                if ~isnan(Sim.dem.trafVol.(...
                                        Sim.dem.famUseCase_{fan}...
                                        ).CEtput.laten(tn))
                                    row = cell2mat(Sim.linBud.SEmean.MU_MIMO.FDD(3:end,2)) == AeCouBS_(an);
                                    col = cell2mat(Sim.linBud.SEmean.MU_MIMO.FDD(2,3:end)) == AeCouMS_(bn);

                                    if ~any(col)
                                        proc = true;
                                    else
                                        tmp = Sim.linBud.SEmean.MU_MIMO.FDD{[false(2,1);row],[false(1,2),col]};

                                        if isnumeric( tmp )
                                            proc = true;
                                        else
                                            switch tmp
                                                case 'N/A'
                                                    proc = false;
                                                case 'Rx diversity'
                                                    proc = true;
                                                otherwise
                                                    error( 'Unknown entry for the mean spectral efficiency!' );
                                            end
                                        end
                                    end

                                    % Check if this case is ever
                                    % encountered, and if not do nt
                                    % proceed with populating the
                                    % output
                                    if proc
                                        proc = any(strcmp(Sim.linBud.BS.pwrAmpClas_{pn},rulPwrAmpClas) & ...
                                            AeCouBS_(an)==rulAeCou(:,fn) & ...
                                            BW_(BWn)==rulBand(:,fn));
                                    end
                                else
                                    proc = false;
                                end

                                if proc
                                    switch Sim.linBud.tecInterf{fn}
                                      case '3G'
%                                         xDL = repmat(...
%                                             TPut_vs_SINR(SINR) ./ 5 .* ...
%                                             BW_(BWn),Sim.utilisN,1);
%                                         xDL2 = xDL(end,:) ./ ...
%                                             (1-1./Sim.utilisN./2) ./ ...
%                                             BW_(BWn);
%                                         xDL2(1) = 0;
%                                         xDL3 = ones(size(xDL2));
                                        
                                        bareMeta.FBMC = false;
                                        AeCou = bareMeta.AeCou;
                                        bareMeta.AeCou = 2;
                                        [xDL,xDL2,xDL3] = c_SUth(SINR-2.6,Sim,L,scen,1,...
                                            bareMeta,AeCouMS_(bn),...
                                            Sim.dem.famUseCase_{fan},tn,fn,false);
%                                         xDL(9,:) = min(xDL(9,end) ./ ...
%                                             BW_(BWn).*5,xDL(9,:));
                                        xDL(9,:) = xDL(9,:) ./ ...
                                            BW_(BWn) .* 5;
                                        bareMeta.AeCou = AeCou;
%                                         test1(end)./xDL2(end)
%                                         199./218
                                      case '4G'
                                        bareMeta.FBMC = false;
                                        [xDL,xDL2,xDL3] = c_SUth(SINR,Sim,L,scen,1,...
                                            bareMeta,AeCouMS_(bn),...
                                            Sim.dem.famUseCase_{fan},tn,fn,false);
                                      case '5G'
                                        switch bareMeta.AeCou
                                          case {2,4}
                                            bareMeta.FBMC = true;
                                          case 32
                                            bareMeta.FBMC = false;
                                          otherwise
                                            error('Undefined')
                                        end
                                        [xDL,xDL2,xDL3] = c_SUth(SINR,Sim,L,scen,1,...
                                            bareMeta,AeCouMS_(bn),...
                                            Sim.dem.famUseCase_{fan},tn,fn,false);
                                      otherwise
                                        error('Undefined')
                                    end
                                    INDdl = [find(xDL2==0,1,'last') find(xDL2==xDL2(end),1,'first')];
%                                     [xUL,xUL2] = c_SUth(SINR,Sim,L,scen,2,...
%                                         bareMeta,AeCouMS_(bn),...
%                                         Sim.dem.famUseCase_{fan},tn,fn,false);
%                                     INDul = [find(xUL==0,1,'last') find(xUL==xUL(end),1,'first')];

                                    SINR2tput{fn,L,pn,an,bn,fbn,fan,tn,BWn} = ...
                                        [SINR(INDdl(1):INDdl(2))
                                        xDL(:,INDdl(1):INDdl(2))
                                        xDL2(INDdl(1):INDdl(2))
                                        xDL3(INDdl(1):INDdl(2))];
%                                     SINR2tput{fn,L,pn,an,bn,fbn,sn,fan,tn,BWn,2} = ...
%                                         [SINR(INDul(1):INDul(2))
%                                         xUL(INDul(1):INDul(2))
%                                         xUL2(INDul(1):INDul(2))];
                                end
                            end
                        end
%                         end
                    end
                end
            end
        end
    end
end
