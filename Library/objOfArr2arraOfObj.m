function poin = objOfArr2arraOfObj(poin)
%% Convert object of arrays to array of objects
% arraOfObj2objOfArr converts array of objects to object of arrays
%
% * v0.1 KK 16Aug17 Added header

fields = fieldnames(poin);

poin2 = struct(fields{1},num2cell(zeros(length(poin.(fields{1})),1)));
for n = 1 : length(fields)
    tmp = num2cell(poin.(fields{n}));
    [poin2.(fields{n})] = deal(tmp{:});
end
