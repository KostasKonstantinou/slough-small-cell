%% Write link budget in csv
function w_linBud(fn,Sim,visib,Cell,fam,trafClI,IND3,thisDem,IND2c,...
    thisBund2,fs1,SINRthres_DL,trafCl,Un,P,Sigma,linBud,clu,noisRis)
% w_linBud writes a csv file with the link budget parameters of a specific
% link. w_linBud does not have an output.
% 
% The input fn is the index to Sim.linBud.dupMode that refers to the band
% we wish the link budget for. The input fam is the family of use cases
% (essentially the service) which we wish the link budget for, and must be one of Sim.dem.famUseCase_. The input
% trafClI is an index to Sim.dem.trafClas_ that refers to the traffic class
% we wish the link budget for.
% 
% The input IND3 is a vector of indices to the fields of input visib that
% refers to the links which we wish the link budget for. If IND3 contains
% multiple indices, then the link budget is produced assuming
% multi-connectivity to the two BS indicated by IND3.
% 
% The input IND2c is an Mx1 logical vector, where M=length(thisDem.aeCou), 
% with true elements at the columns of thisDem which are related to the
% family of services and traffic class we wish the link budget for.
% 
% The input visib is [].
% 
% The input thisBund2 is [].
%
% The input fs1 is [].
% 
% The input noisRis is [].
% 
% The input SINRthres_DL is [].
% 
% The input trafCl,Un,P,Sigma,linBud,clu are [].
% 
% Input Sim is the SIM object, must contain the following fields [pending].
% 
% Input Cell is an array of CELL objects, must contain the following fields
% [pending].
% 
% Input thisDem is a POIN object (currently a strucutre but the intention
% is to convert into POIN class) relevant to the calculation point which we
% wish the link budget for, must contain the following fields:
% [pending].
% 
% * v0.1 Kostas Konstantinou Oct 2018

pwrAmpClasBS = 'macrocell';
situBS = 'outd';
trafCl = 'Streaming';
pwrAmpClasMS = 'handh';
situMS = 'indo';

% Find if the link vudget is for FDD or TDD
if Sim.linBud.dupMode(fn)==2
    if length(Cell) == 1
        if round(Cell.band.capa(fn)./5).*5 == Cell.band.capa(fn)
            XDD = 'FDD';
        else
            XDD = 'TDD';
        end
    elseif length(Cell) == 2
        if all(arrayfun(@(x) round(x.band.capa(fn)./5).*5 == ...
                x.band.capa(fn),Cell))
            XDD = 'FDD';
        else
            sgerr
        end
    else
        sdfdsffewf
    end
else
    XDD = 'TDD';
end

% Find the number of antennas at the BS side
AeCou = arrayfun(@(x) x.AeCou(fn),Cell);

% Simplication
% NLayers = visib.NLayers(IND3);
capa = arrayfun(@(x) x.band.capa(fn),Cell.');
capa(capa==8.79) = 10;
capa(capa==18.467) = 20;
% capa(capa>20) = 20;

% Calculate the EIRP per PRB
if length(Cell) == 1
    if isempty(Cell.PTx_dBm)
        EIRP_ovAllAnt_10MHz_100pcLoad_dBm = ...
            Cell.EIRP_ovAllAnt_10MHz_100pcLoad_dBm(fn);
    else
        PTx_dBm = Cell.PTx_dBm(fn);
    end
    if isempty(Cell.PTx_dBm)
        EIRPperPRB = EIRP_ovAllAnt_10MHz_100pcLoad_dBm - 10*log10(50);
    else
        EIRPperPRB = PTx_dBm + ...
            (Sim.linBud.(pwrAmpClasBS).(situBS).GA.(fs1) + ...
            Sim.linBud.(pwrAmpClasBS).(situBS).GB.(trafCl) + ...
            Sim.linBud.(pwrAmpClasBS).(situBS).GC) - ...
            10.*log10(capa.*5);
    end
    faFadMargi = visib.faFadMargi(IND3(1),1);
else
    EIRP_ovAllAnt_10MHz_100pcLoad_dBm = arrayfun(@(x) ...
        x.EIRP_ovAllAnt_10MHz_100pcLoad_dBm(fn),Cell);
    EIRPperPRB = EIRP_ovAllAnt_10MHz_100pcLoad_dBm - 10*log10(50);
    faFadMargi = visib.faFadMargi(IND3,length(Cell)).';
end
RxSensitivity = linBud.NoiFigu.(fs1) - 173.93 + 10*log10(180000) + ...
    SINRthres_DL.';

% Calculate the slow fading margin
m = length(Sigma);  % The number of servers
if m == 1
    slowFadingMargin = norminv(P) .* Sigma;
else
    % mvncdf expects the cross correlation matrix with the sigma1*sigma2
    % values, and multiplied by rho in the diagonal.
    rho = 0.5;  % The cross-correlation coefficient
    rhoMat = eye(m);
    rhoMat(~rhoMat) = rho;
    if size(Sigma,1) == 1
        SIGMA = Sigma' * Sigma .* rhoMat;
    else
        SIGMA = Sigma * Sigma' .* rhoMat;
    end
    slowFadingMargin = SINRthres_DL.' - fzero(@(x) mvncdf([x x],...
        SINRthres_DL.',SIGMA)-1+max(P),min(SINRthres_DL)+[-30 -10]);
end

% Calculate the maximum allowable path loss
MAPL = EIRPperPRB + linBud.GA.(fs1) - linBud.GB.(trafCl) - ...
    RxSensitivity(:,1) - slowFadingMargin - noisRis(1) - faFadMargi + ...
    Sim.linBud.(pwrAmpClasBS).(situBS).BPGmean.(fs1).ur + ...
    Sim.linBud.(pwrAmpClasMS).(situMS).BPGmean.(fs1).ur;

% Define the clutter type
switch clu.tran.pop{thisDem.clu+1,5}
    case 1
        teleden = 'Urban';
    case 2
        error('Underfined')
    case 3
        teleden = 'Suburban';
    case 4
        teleden = 'Rural';
    otherwise
        error('Underfined')
end

% Simplification
spotFreqMhz = Sim.linBud.spotFreq(fn);

% Calculate the range in metres
clu_tran = Sim.propag.UE{Un,3};
[row_clu,col_clu] = worldToSub(clu.R,[Cell.X],[Cell.Y]);
col_idx_clut_h_extreme      = 7;  % 'for i=1 and n in (1c)'
heiAgl = [Cell.heiAgl];
if all(heiAgl.' >= cell2mat(clu.tran.(clu_tran)...
        (clu.A(sub2ind(size(clu.A),row_clu,col_clu))+1,...
        col_idx_clut_h_extreme)))
    switch spotFreqMhz
        case {800,2100,3500}
            % Above rooftop, <6GHz, use Extended Hata model
            raM = cellfun(@(x,y) fzero(@(d) give_LdBnoAngleNoMCL(...
                Sim.linBud.spotFreq(fn),d,x,Sim.propag.UE{Un,2},...
                clu.tran.pop{thisDem.clu+1,5},'Extended_Hata')-y,1),...
                {Cell.heiAgl},num2cell(MAPL)) .* 1e3;
        case 26000
            % Above rooftop, >6GHz, no model available
            beep
            disp('Code this!')
            keyboard
    end
else
    switch spotFreqMhz
        case {800,2100,3500}
            % Below rooftop, <6GHz, use ITU-R 1411
            beep
            disp('Code this!')
            keyboard
        case 26000
            % Below rooftop, >6GHz, use mmWave model
            beep
            disp('Code this!')
            keyboard
    end
end

% Prepare matrix to be written
M = cell(34,2+length(IND3));
M(:,1:2) = ['Service' {[]}
    'Communication direction' {[]}
    'Duplex mode' {[]}
    'Data rate' {'Mbit/s'}
    'Spot frequency for link budget' {'MHz'}
    'Bandwidth' {'MHz'}
    'Number of PRB' {[]}
    {[]} {[]}
    'Number of Tx antennas' {[]}
    'Number of transmission layers' {[]}
    'Number of Rx antennas' {[]}
    {[]} {[]}
    'EIRP over all antennas' {['dBm/' num2str(capa) 'MHz']}
    'EIRP over all antennas per PRB' {'dBm/180kHz'}
    {[]} {[]}
    'Noise figure' {'dB'}
    'Thermal noise in PRB' {'dBm/180kHz'}
    'Rx noise floor in PRB' {'dBm/180kHz'}
    'Rx diversity gain' {'dB'}
    'SINR including Rx diversity gain' {'dB'}
    'Rx sensitivity' {'dBm/180kHz'}
    'Interference margin' {'dB'}
    'Rx antenna element gain' {'dBi'}
    'Body loss' {'dB'}
    {[]} {[]}
    'Base station antenna height' {'metres agl'}
    'Mobile antenna height' {'metres agl'}
    'Cell-area coverage-confidence' {'%'}
    'Slow fading standard deviation' {'dB'}
    'Slow fading margin' {'dB'}
    'Fast fading margin' {'dB'}
    'Maximum path loss' {'dB'}
    'Clutter' {[]}
    'Maximum range (Hata w/o terrain)' {'metres'}];

M{1,3} = fam;
M{2,3} = 'Downlink';
M{3,3} = XDD;
M{4,3} = Sim.dem.trafVol.(fam).CEtput.DL(thisBund2,trafClI);
M(5,3:end) = {spotFreqMhz};
M(6,3:end) = {capa};
M(7,3:end) = {capa.*5};

M(9,3:end) = {AeCou};
M(10,3:end) = {NLayers};
M(11,3:end) = {thisDem.aeCou(IND2c)};

M(13,3:end) = num2cell(EIRP_ovAllAnt_10MHz_100pcLoad_dBm+10.*log10(...
    capa./10));
M(14,3:end) = num2cell(EIRPperPRB);

M(16,3:end) = {linBud.NoiFigu.(fs1)};
M(17,3:end) = {-173.93+10*log10(180000)};
M(18,3:end) = {linBud.NoiFigu.(fs1)-173.93+10*log10(180000)};
M(19,3:end) = {10.*log10(thisDem.aeCou(IND2c))};
M(20,3:end) = num2cell(SINRthres_DL.');
M(21,3:end) = num2cell(RxSensitivity);
M(22,3:end) = {noisRis(1)};
M(23,3:end) = {linBud.GA.(fs1)};
M(24,3:end) = {linBud.GB.(trafCl)};

M(26,3:end) = num2cell(heiAgl);
M(27,3:end) = {Sim.propag.UE{Un,2}};
M(28,3:end) = {Sim.dem.trafVol.(fam).CEtput.celAreConfLeve(trafClI).*100};
M(29,3:end) = num2cell(Sigma.');
M(30,3:end) = num2cell(slowFadingMargin);
M(31,3:end) = num2cell(faFadMargi);
M(32,3:end) = num2cell(MAPL);
M{33,3} = teleden;
M(34,3:end) = num2cell(raM);

c = clock;
c = num2str(c(6));
c(c=='.') = '_';

% Write matrix in csv
dlmcell(fullfile('Outp',[num2str(spotFreqMhz) ' ' ...
    num2str(Sim.yea.thisYea) ' ' c '.csv']),M,',')
