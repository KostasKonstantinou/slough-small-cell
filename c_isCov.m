%%
function [isCov,Mu_DL,noisRis,isCovPer,Sigma] = c_isCov(SINRthres_DL,...
    sigmaW,sigmaI,P,P2,dupMode,famI,faFadMargi,ii,interfMultip,IM_DL,...
    noisFl_DL,RxPowerRB_DL,dop,I_UL0,isSameSitOtSec,Sim,satis,servCou,...
    IND2,BHcou,N2,subs,sitW,LocisCell,capa,interfSuppr,row,...
    SINRthresPer_DL,distBordm,IM_DL2correctiFacto,residuErr)
% []
% []
%
% * v0.1 KK 21Aug18 Added header

% thisBH = find(~satis(:,IND2));
thisBH = squeeze(any(~satis&shiftdim(repmat(IND2,[1,1,BHcou]),2),2));
% BHcou = length(thisBH);
N2n = length(N2);

isCov = false(size(satis,1),N2n);
isCovPer = false(size(satis,1),N2n,size(SINRthresPer_DL,2));
% Mu_DL = nan(size(satis,1),1);
% noisRis = nan(size(satis,1),1);

if ~any(thisBH(:))
    Mu_DL = [];
    noisRis = [];
    Sigma = [];
    return
end

% []
% In the definition of IsameSitLin there is a max(,0). This is because very
% occasionally there is a residual error in the order of -10^-21, and if
% the error is negative , then the logarithm of r becomes imaginary.
subs2 = bsxfun(@plus,subs,(0:BHcou-1)*N2n);
if servCou == 1
    if Sim.servDem.isInterfeExpl.cov
        % The BW of wanted sector:
    %     capa(LocisCell)
        % The BW of interfering sector:
    %     [row,~] = find(isCell_S(:,sitW));
        if length(capa) == 1
            tmp2 = ones(1,sum(N2));
        else
            capa2 = capa(row);
            tmp2 = capa2 ./ max(capa(LocisCell(subs)),capa2);
        end
        A = bsxfun(@times,ii.*tmp2,interfMultip);
        val = bsxfun(@times,A,~isSameSitOtSec).';

        % []
        IotSitLin = reshape(accumarray(subs2(:),val(:),...
            [N2n*BHcou 1]),N2n,BHcou).';  % mW/180kHz
    %     maxI = zeros(BHcou,N2n);
    %     for m = 1 : N2n
    %         [~,maxI(:,m)] = max(val(subs==m,:),[],1);
    %     end

        % []
        col = (1:length(subs)).';
        col2 = col([true;logical(subs(2:end)-subs(1:end-1))]);
        maxI = zeros(BHcou,N2n);
        for n = 1 : BHcou
            S = sparse(subs,col,val(:,n),N2n,length(subs));
            [~,I] = max(S,[],2);
            maxI(n,:) = I - col2 + 1;
            maxI(n,~any(S,2)) = 1;
        end

        % []
        IotherLin = reshape(accumarray(subs2(:),reshape(A.',[],1),...
            [N2n*BHcou 1]),N2n,BHcou).';  % mW/180kHz
    else
        IotSitLin = zeros(BHcou,N2n);
        maxI = zeros(BHcou,N2n);
        IotherLin = zeros(BHcou,N2n);
    end
else
    IotSitLin = zeros(BHcou,N2n,servCou);
    maxI = zeros(BHcou,N2n,servCou);
    IotherLin = zeros(BHcou,N2n,servCou);
    for n = 1 : servCou
        if length(capa) == 1
            tmp2 = ones(1,sum(N2));
        else
            capa2 = capa(row(:,n));
            tmp2 = capa2 ./ max(capa(Cell2Vec(cellfun(@(x,y) x.*ones(y,1),...
                num2cell(LocisCell(:,n)),num2cell(N2),'UniformOutput',false))),capa2);
        end
        A = bsxfun(@times, ii(n,:).*tmp2,...
            interfMultip((n-1)*BHcou+(1:BHcou),:));
        val = bsxfun(@times,A,~isSameSitOtSec(n,:)).';
        
        IotSitLin(:,:,n) = reshape(accumarray(subs2(:),val(:),...
            [N2n*BHcou 1]),N2n,BHcou).';  % mW/180kHz
        
        % []
%         for m = 1 : N2n
%             [~,maxI(:,m,n)] = max(val(subs==m,:),[],1);
%         end
        col = (1:length(subs)).';
        col2 = col([true;logical(subs(2:end)-subs(1:end-1))]);
        for hn = 1 : BHcou
            S = sparse(subs,col,val(:,hn),N2n,length(subs));
            [~,I] = max(S,[],2);
            maxI(hn,:,n) = I - col2 + 1;
            maxI(hn,~any(S,2),n) = 1;
        end
        
        % []
        IotherLin(:,:,n) = reshape(accumarray(subs2(:),reshape(A.',[],1),...
            [N2n*BHcou 1]),N2n,BHcou).';  % mW/180kHz
    end
%     A = shiftdim(repmat(10.^(ii./10),[1,1,BHcou]),2) .* ...
%         interfMultip(thisBH,:);
%     tmp = A.*shiftdim(repmat(~isSameSitOtSec,[1,1,BHcou]),2);
%     [~,maxI] = max(tmp,[],2);
%     maxI = squeeze(maxI);
%     assert(size(maxI,2)==2,'Fix next line if this fails')
%     maxI(:,2) = maxI(:,1) + size(isSameSitOtSec,1);
%     IotSitLin = squeeze(sum(tmp,2));  % mW/180kHz
%     IotherLin = squeeze(sum(A,2));  % mW/180kHz
end
IsameSitLin = max(IotherLin-IotSitLin,0);  % mW/180kHz
Mu_DL = zeros(BHcou,N2n,servCou);
noisRis = zeros(BHcou,N2n,servCou);
switch Sim.bordCoor
  case {'ignore','coordinated'}
    IM_DL2 = zeros(size(IM_DL));
  case 'non_coordinated'
    IM_DL2 = max(-0.0008061*distBordm+1.109-IM_DL2correctiFacto+residuErr,0);
  otherwise
    error('Undefined')
end
for n = 1 : servCou
    if interfSuppr == 0
        I = 10 .* log10(bsxfun(@plus,10.^((noisFl_DL+IM_DL+IM_DL2).'./10),...
            IotherLin(:,:,n)));  % dBm/180kHz
    else
        I = 10 .* log10(bsxfun(@plus,10.^((noisFl_DL+IM_DL+IM_DL2).'./10),...
            IotherLin(:,:,n) ./ 10.^(interfSuppr./10)));  % dBm/180kHz
    end
    Mu_DL(:,:,n) = bsxfun(@minus,(RxPowerRB_DL(:,n)-dop).',I);  % dB
    noisRis(:,:,n) = bsxfun(@minus,I,noisFl_DL.');
end
% I_UL = I_UL0 + noisRis(thisBH);  % dBm/180kHz
% Mu_UL = PRx_across_system_BW_UL - I_UL - dop;
r = IsameSitLin ./ IotSitLin;
r(isnan(r)) = 0;

IND3 = any(any(IotSitLin>0,3),1).';

% dim1Sub
dim1Sub = min(max(round(10.*log10(r)+30)+1,1),size(Sim.SINRsd,1));

% Calculate standard deviation of SINR, Sigma.
% If implicit interference, then Sigma = SigmaW.
% If explicit interference, then Sigma is calculated from the contribution
% of interference that is attributed from {other sites} and {other
% cells of same site}.
Sigma = cell(1,servCou);
for n = 1 : servCou
    if Sim.servDem.isInterfeExpl.cov
        % Implicit interference: Sigma = SigmaW.
        Sigma{n} = repmat(sigmaW,1,BHcou);
    else
        % Explicit interference.
        % Initialise below to eps. Then, if there are other sites, i.e. if
        % any(IND3,1) then Sigma is replaced by calculation, otherwise
        % Sigma remains at eps.
        Sigma{n} = ones(N2n,BHcou) .* eps();
    end
end
if any(IND3,1)
    % dim2Sub
    dim2Sub = nan(N2n,servCou);
    IND = bsxfun(@and,sigmaW<0.05,IND3);
    if any(IND(:))
        dim2Sub(IND) = 1;
    end
    IND = bsxfun(@and,6.45<=sigmaW&sigmaW<10.55,IND3);
    if any(IND(:))
        dim2Sub(IND) = round((sigmaW(IND)-6.5)./0.1) + 2;
    end
    
    for n = 1 : servCou
        dim3Sub = nan(N2n,BHcou);
        sigmaI2 = reshape(sigmaI(bsxfun(@plus,...
            cumsum([1;N2(1:end-1)])-1,maxI(:,:,n).'),n),[N2n,BHcou]);
        IND = bsxfun(@and,sigmaI2<0.05,IND3);
        if any(IND(:))
            dim3Sub(IND) = 1;
        end
        IND = bsxfun(@and,6.45<=sigmaI2&sigmaI2<10.55,IND3);
        if any(IND(:))
            dim3Sub(IND) = round((sigmaI2(IND)-6.5)./0.1) + 2;
        end
        
        linearInd = (dim3Sub-1).*size(Sim.SINRsd,1).*size(Sim.SINRsd,2) + ...
            bsxfun(@plus,(dim2Sub(:,n)-1).*size(Sim.SINRsd,1),...
            dim1Sub(:,:,n).');
        
        Sigma{n}(IND) = Sim.SINRsd(linearInd(IND));
    end
end
if servCou == 1
    Sigma = shiftdim(shiftdim(Sigma{1},-1),2);
else
    Sigma = shiftdim(reshape(cell2mat(Sigma),[N2n,BHcou,servCou]),1);
end

if servCou == 1
    % One server
    % arg_DL is the if argument of the next paragraph for DL, and is true
    % if the mean calculated DL SINR, Mu_DL, is greater than the threshold
    % SINR that is required for the service at cell edge, SINRthres_DL,
    % with probability higher than P, given standard devation of path loss
    % equal to Sigma.
    % arg_UL is as above but for UL.
    % Of course these two lines could be rewritten by using the built-in
    % function norminv, but intentionally are left without using the
    % norminv, so so the similarity with the case of two servers, in the
    % brach below.
%     arg_DL = 1-(1+erf((SINRthres_DL-Mu_DL)./Sigma./sqrt(2)))./2 > P;
%     arg_UL = 1-(1+erf((SINRthres_UL-Mu_UL)./Sigma./sqrt(2)))./2 > P;
    % []
%         arg_DL = SINRthres_DL(thisBH)-Mu_DL(thisBH)+faFadMargi < P2.*Sigma;
    arg_DL = SINRthres_DL - Mu_DL + faFadMargi < ...
        bsxfun(@times,P2,squeeze(Sigma));
    argPer_DL = bsxfun(@lt,bsxfun(@minus,permute(SINRthresPer_DL,...
        [3 1 2]),Mu_DL)+faFadMargi,...
        bsxfun(@times,P2,permute(Sigma,[1 3 2])));
    thisBH2 = repmat(thisBH,[1,1,size(SINRthresPer_DL,2)]);
    isCovPer(thisBH2) = argPer_DL(thisBH2);
%     arg_UL = SINRthres_UL-Mu_UL+faFadMargi < 1.017./(r+1.017).*P2.*Sigma;
else
    beep
    disp('Reporting multiple throughputs has not been implemented')
    keyboard
    
    arg_DL = false(BHcou,N2n);
    neeAsse = true(BHcou,N2n);  % Needs assessment
    
    if servCou > 2
        beep
        disp('Code this!')
        keyboard
    end

    neeAsse(~thisBH) = false;
    
    % If same site then cross-correlation coefficient equals 1, and there
    % is no benefit from MC
    neeAsse(repmat(sitW(:,1)==sitW(:,2),1,BHcou).') = false;
    [row2,col2] = find(neeAsse);
    
    s = zeros(nnz(neeAsse),servCou);
    rc = zeros(nnz(neeAsse),servCou);
    s0 = [-8;-5];
    s1 = [0.03;0.04];
    for n = 1 : servCou
        tmp3 = Mu_DL(:,:,n);
        tmp4 = SINRthres_DL(:,:,n);
        tmp5 = squeeze(Sigma(:,n,:));
        s(:,n) = (tmp3(neeAsse)-tmp4(neeAsse)-faFadMargi) ./ tmp5(neeAsse);
        rc(:,n) = floor((s(:,n)-s0(n))./s1(n)) + 1;
    end
    
    tmp6 = find(rc(:,2)+1>size(Sim.mvncdfY2,1));
    if ~isempty(tmp6)
        tmp8 = all(bsxfun(@gt,Sim.mvncdfY2(end-1:end,rc(tmp6,1)),...
            max(P(:,col2(tmp6)),[],1)).',2);
        assert(all(tmp8),'Extend')
         % Success domain
        linearInd2 = sub2ind(size(arg_DL),row2(tmp6),col2(tmp6));
        arg_DL(linearInd2) = true;
        neeAsse(linearInd2) = false;
        row2(tmp6) = [];
        col2(tmp6) = [];
        rc(tmp6,:) = [];
    end
    tmp6 = find(rc(:,1)+1>size(Sim.mvncdfY2,2));
    if ~isempty(tmp6)
        tmp8 = all(bsxfun(@gt,Sim.mvncdfY2(rc(tmp6,2),end-1:end).',...
            max(P(:,col2(tmp6)),[],1)).',2);
        assert(all(tmp8),'Extend')
         % Success domain
        linearInd2 = sub2ind(size(arg_DL),row2(tmp6),col2(tmp6));
        arg_DL(linearInd2) = true;
        neeAsse(linearInd2) = false;
        row2(tmp6) = [];
        col2(tmp6) = [];
        rc(tmp6,:) = [];
    end
    
    % Assume for all practical purposes that below <-8 and <-5 thre is no
    % chance of connectivity
    rc(rc<=0) = 1;
    
    rowSub = bsxfun(@plus,rc(:,2),[0,0,1,1]);
    colSub = bsxfun(@plus,rc(:,1),[0,1,0,1]);
    arg_DL2 = bsxfun(@gt,reshape(Sim.mvncdfY2(sub2ind(size(Sim.mvncdfY2),...
        rowSub(:),colSub(:))),nnz(neeAsse),4),max(P(:,col2),[],1).');
    
    tmp7 = all(arg_DL2,2);
    if any(tmp7)
        % Success domain
        linearInd2 = sub2ind(size(arg_DL),row2(tmp7),col2(tmp7));
        arg_DL(linearInd2) = true;
        neeAsse(linearInd2) = false;
    end
    tmp7 = ~any(arg_DL2,2);
    if any(tmp7)
        % Failure domain
        linearInd2 = sub2ind(size(arg_DL),row2(tmp7),col2(tmp7));
        neeAsse(linearInd2) = false;
    end
 
    for m = 1 : N2n
        if ~any(neeAsse(:,m))
            continue
        end

        % The cross-correlation coefficient
        rho = -0.5;
        
        for k = find(neeAsse(:,m).')
            % Create a diagonal matrix, SIGMA, (Number of servers x Number of
            % servers) with the cross-correlation values in the diagonal and then
            % every element is multiplied by sigma1*sigma2. rhoMat is an auxhiliary
            % function to create the diagnal first. 5G Norma assumed
            % similar to 3GPP 0.5 cross-correlation coefficient across sites. Note
            % however, that this value is intended to be used only across
            % macrosites, whereas for simplicity it is being used for small cell
            % sites as well. This is not correct, however, the alternative, which
            % is to define the cross-correlation coefficient with the angular and
            % separation distance was not implemented in time for 5G Norma
            % deliverables. For future study.
            % mvncdf expects the cross correlation matrix with the
            % sigma1*sigma2 values, and multiplied by rho in the diagonal.
            rhoMat = eye(servCou);
            rhoMat(~rhoMat) = rho;
            SIGMA = Sigma(k,:,m)' * Sigma(k,:,m) .* rhoMat;

            % Calculate arg_DL, in a similar fashion to the single-server branch.
            % Note that arg_DL is a scalar, i.e. (1x1).
%             arg_DL(k) = 1 - mvncdf(squeeze(SINRthres_DL(k,m,:)),...
%                 squeeze(Mu_DL(k,m,:))-faFadMargi,SIGMA) > max(P(:,m));
            % Convert into numpy array
            SIGMA_array = py.numpy.reshape(SIGMA(:).',int32(size(SIGMA)));
            % Call the mvncdf function in Python
%             try
            rv = py.scipy.stats.multivariate_normal(...
                squeeze(Mu_DL(k,m,:)).'-faFadMargi,SIGMA_array);
%             catch
%                 tol = 1e-7;
%                 SIGMA(abs(SIGMA)<tol) = sign(SIGMA(abs(SIGMA)<tol)) .* tol;
%                 
%                 SIGMA_array = py.numpy.reshape(SIGMA(:).',int32(size(SIGMA)));
%                 rv = py.scipy.stats.multivariate_normal(...
%                     squeeze(Mu_DL(k,m,:)).'-faFadMargi,SIGMA_array);
%             end
            arg_DL(k,m) = 1-rv.cdf(squeeze(SINRthres_DL(k,m,:)).') > ...
                max(P(:,m));
    %             mvncdf_py_res = py.mvncdf_func.mvncdf_py(...
    %                 squeeze(SINRthres_DL(k,m,:)).',...
    %                 squeeze(Mu_DL(k,m,:)).'-faFadMargi,SIGMA_array);
    %             arg_DL(k) = 1 - mvncdf_py_res > max(P(:,m));
    %             clear mvncdf_py_res
    
        %     if famI == 1
        %         arg_UL = 1-mvncdf(SINRthres_UL,Mu_UL-faFadMargi(:,2),SIGMA) > max(P);
        %     end
        end
    end
end

arg_UL = true(size(arg_DL));

% Calclulate the proceed logical variable, which is true if the relevant if
% arguments are true, i.e. the link to the best server(s) is adequately
% strong.
% if famI~=1 && (~arg_DL||~arg_UL)
%     pause(0)
% end
% if famI~=1
%     % For non-MBB slices, limitation only for the DL.
%     % Be careful here! famI==1 does not necessarily mean that it
%     % corresponds to eMBB. famI==1 simply means the first family to have
%     % been read according to the order in the config file. Ideally we
%     % should have a service parameter that will say if we require DL as
%     % well as UL cell-edge requirement. Why UL requirement does not need to
%     % be met by non-eMBB? This is because we reserved other non-EMBB
%     % techniques to ensure UL connectivity, such as nIoT, repetition, relay
%     % mode. This should be changed and externalised for clarity.
%     beep
%     disp('Code this!')
%     keyboard
%     arg_UL = true;
% end

% Find hours with DL success and UL success
isCov(thisBH) = arg_UL(thisBH) & arg_DL(thisBH);

% Find hours with DL success and UL failure. UL could be served by another
% band if TDD SD.
% if any(thisBH(:,m)) && any(~isCov(thisBH(:,m),m)&arg_DL(thisBH(:,m)))
%     if dupMode == 1
%         % TDD.
%         % For 5G Norma it was decided that TDD
%         % bands are Supplementary Downlink (TDD SD), thus the uplink could
%         % have been provided by another FDD band, and therefore we only
%         % need to ensure that DL is successfull. Beware! Ideally we should
%         % have checked that there are site(s) with FDD bands that are
%         % 'close' enough to ensure DL service. In 5G Norma this was not
%         % done because of lack of time to implement this feature, and
%         % because it was not that important: there is a great degree of
%         % overlap in FDD (sub-1GHz and low bands) in central London. For
%         % future study.
%         isCov(thisBH(:,m),m) = arg_DL(thisBH(:,m));
%     else
%         % FDD
% %         beep
% %         disp('Check UL power control!')
% %         keyboard
% 
% %         Pt==23
%     end
% end
end
