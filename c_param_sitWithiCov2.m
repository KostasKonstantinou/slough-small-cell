function params2 = c_param_sitWithiCov2(visib,servCou,fn,Sim,Cell,MS,...
    isCell,fs1,params)

if size(visib.SE_DL,2) < servCou
    params2 = cell(1,17);
    return
end

[pwrAmpClasBS,isCell_S,sit_,sec_,ii_,sigmaLe_,situBS] = ...
    deal(params{[10,18,22:26]});

% []
isDel = isnan(visib.SE_DL(:,servCou));
if any(isDel)
    names = fieldnames(visib);
    for n = 1 : length(names)
        visib.(names{n})(isDel,:) = [];
    end
    sit_(isDel) = [];
    sec_(isDel) = [];
    ii_(isDel) = [];
    sigmaLe_(isDel) = [];
end
clear isDel n names

N1 = cellfun(@(x) size(x,1),sit_);
N2 = N1 - 1;
N1cell = num2cell(N1);
N2cell = num2cell(N2);
Sn2 = sum(N2);
Ln2 = length(N2);

N = 1000;
if Sn2 <= N
    N2_S = sparse(~bsxfun(@gt,1:Sn2,cumsum(N2)) & ...
        bsxfun(@gt,1:Sn2,[0;cumsum(N2(1:end-1))]));
else
    n_ = 1 : N : Ln2;
    N2_S = logical(sparse(Ln2,Sn2));
    N3 = 0;
    for n = 1 : length(n_)
        thisN = n_(n) : min(n_(n)+N-1,Ln2);
        thisN2 = N2(thisN);
        N2_S(thisN,N3+(1:sum(thisN2))) = ...
            sparse(~bsxfun(@gt,1:sum(thisN2),cumsum(thisN2)) & ...
            bsxfun(@gt,1:sum(thisN2),[0;cumsum(thisN2(1:end-1))]));
        N3 = N3 + sum(thisN2);
    end
end
clear N n n_ thisN thisN2 N3

% []
% sitW: wanted site (1xN)
% secW: wanted sector (1xN)
% sitI: interferer site, cell vector (pathCou x 1), each cell is a matrix
% (interfererCou x servCou)
% secI: interferer sector (1xM)
% w1: wanted RSRP of Link 1
% wN: wanted RSRP of all wanted links (1xN)
% iN: interferer RSRP (1xM)
tmp = cellfun(@(x) repmat(x,1,servCou),sit_,'UniformOutput',false);
sitW = cell2mat(cellfun(@(x,y) y(1:x+1:end),N1cell,tmp,...
    'UniformOutput',false));
for n = 1 : length(sit_)
    tmp{n}(1:N1(n)+1:end) = [];
end
sitI = cellfun(@(x) reshape(x,[],servCou),tmp,'UniformOutput',false);
%
tmp = cellfun(@(x) repmat(x,1,servCou),sec_,'UniformOutput',false);
secW = cell2mat(cellfun(@(x,y) y(1:x+1:end),N1cell,tmp,...
    'UniformOutput',false));
for n = 1 : length(sec_)
    tmp{n}(1:N1(n)+1:end) = [];
end
secI = cellfun(@(x) reshape(x,[],servCou),tmp,'UniformOutput',false);
%
tmp = cellfun(@(x) repmat(x,1,servCou),ii_,'UniformOutput',false);
wN = cell2mat(cellfun(@(x,y) y(1:x+1:end),N1cell,tmp,...
    'UniformOutput',false));
for n = 1 : length(ii_)
    tmp{n}(1:N1(n)+1:end) = [];
end
iN = cellfun(@(x) reshape(x,[],servCou),tmp,'UniformOutput',false);
%
tmp = cellfun(@(x) repmat(x,1,servCou),sigmaLe_,'UniformOutput',false);
sigmaW = cell2mat(cellfun(@(x,y) y(1:x+1:end),N1cell,tmp,...
    'UniformOutput',false));
for n = 1 : length(sigmaLe_)
    tmp{n}(1:N1(n)+1:end) = [];
end
sigmaI = cellfun(@(x) reshape(x,[],servCou),tmp,'UniformOutput',false);

% []
% sub2ind(size(tmp),dim1Sub,dim2Sub,dim3Sub)
% {secCou,secW}(loadW,loadI,secI)
% if servCou == 1
%     [row2,~] = find(N2_S.');
% else
%     sitI2 = cell2mat(cellfun(@transpose,sitI,'UniformOutput',false).').';
%     [row2,~] = find(isCell_S(:,sitI2(:)));
%     row2 = reshape(row2,sum(N2),servCou);
% end
%
row = cell(length(N2),servCou);
for n = 1 : servCou
    [row(:,n),~] = cellfun(@(x) find(isCell_S(:,x(:,n))),sitI,...
        'UniformOutput',false);  % (size(sitI,1)x1)
end
row = cell2mat(row);

if Sn2 < 1e3
    subs = (sum(bsxfun(@gt,1:Sn2,cumsum(N2)),1)+1).';
else
    subs = Cell2Vec(cellfun(@(x,y) ones(x,1).*y,...
        N2cell,num2cell(1:length(N2cell)).','UniformOutput',false)).';
end

% []
[~,LocisCell] = ismember(sitW,isCell);

% []
trafClI = Sim.dem.modeAlTrafClasAs;

% Define cell-edge coverage-confidence, P.
% Jakes formula considers that interference is thermal noise. For the
% cell-edge coverage-confidence it would make sense to use the calculated
% SINR standard deviation (SD). However, this is done within the for loop
% and it will burden the simulation to move this paragraph within the for
% loop. As a patch for small cells which are in LoS in the wanted path, the
% code here is patched to assume that SINR SD = sigmaI.
Un = false(size(Sim.propag.UE,1),length(Sim.dem.famUseCase_));
celAreConfLeve = zeros(length(Sim.dem.famUseCase_),1);
[~,LocMSsitu] = ismember(visib.situMS,Sim.dem.trafVol.MBB.devic.situ);
Ln = zeros(size(visib.studyAind,1),servCou);
for famI = 1 : length(Sim.dem.famUseCase_)
    fam = Sim.dem.famUseCase_{famI};
    C = unique(MS(famI).pwrAmpClas);
    assert(length(C)==1,'Check this!')
    CEtputI = find(Sim.dem.trafVol.(fam).CEtput.yea==Sim.yea.thisYea) + ...
        (0:length(Sim.dem.trafClas_)-1);
    celAreConfLeve(famI) = Sim.dem.trafVol.(fam...
        ).CEtput.celAreConfLeve(CEtputI(trafClI));  % Cell-edge coverage-confidence
    Un(:,famI) = strcmp(C{1},Sim.propag.UE(:,1)) & ...
        strcmp(fam,Sim.propag.UE(:,5));
    tmp = arrayfun(@(x) x.n(fn,Un(:,famI)),Cell.');
    
    IND = visib.famI == famI;
    if any(IND)
        Ln(IND,:) = tmp(LocisCell(IND,:));
    end
end
clear famI fam C
[C,ia,Pic] = unique(repmat(10000.*visib.famI,servCou,1) + 1000.*Ln(:) + ...
    repmat(100.*LocMSsitu(:),servCou,1) + round(sigmaW(:)./0.1).*0.1,...
    'stable');
Pic = reshape(Pic,size(Ln));
P = zeros(length(C),1);
P2 = zeros(length(C),1);
for n = 1 : length(C)
    Sigma = sigmaW(ia(n));
    if Sigma==2^-52 && Sim.servDem.isInterfeExpl.cov
        Sigma = sigmaI{ia(n)}(1);
        % If the above fails (I think it will fail in 2023), then calculate
        % it with the lines below:
%         spotFreqMhz = Sim.linBud.spotFreq(fn);
%         Sigma = 0.65.*log10(spotFreqMhz).^2 - 1.3.*log10(spotFreqMhz) + ...
%             6.6;
    end

    tmp2 = celAreConfLeve(visib.famI(rem(ia(n)-1,length(visib.famI))+1));
    if Sigma > 0
        tmp = fzero(@(pcE) Jakes(pcE,Sigma./Ln(ia(n)),'area')-tmp2,...
            [eps(0) tmp2]);
    else
        % This can happen if all the path is above water
        tmp = fzero(@(pcE) Jakes(pcE,(2^-52)./Ln(ia(n)),'area')-tmp2,...
            [eps(0) tmp2]);
    end

    P(n) = tmp;
    P2(n) = erfinv((1-tmp).*2-1) .* sqrt(2);
end
clear celAreConfLeve Ln LocMSsitu C ia Sigma

% []
iN = 10.^(cell2mat(iN).'./10);
sigmaI = cell2mat(sigmaI);

% Sorts the visibility entries with descending SINR_DL order, and store the
% indices of that order as visibI. visibI(1) is thus the easiest link to
% serve.
% IotherLin = 10.^(visib.i1_DL./10) + 10.^(visib.i2_DL./10);  % mW/180kHz
% I = 10.*log10(10.^(visib.noisFl_DL./10)+IotherLin);
I = visib.noisFl_DL;
SINR_DL = wN(:,1) - I - Sim.linBud.dop(visib.bund2(:,1),fn);
tmp = cellfun(@(x) Sim.dem.prio.(x),Sim.dem.famUseCase_(visib.famI));
if size(tmp,1) == 1
    tmp = tmp.';
end
[~,visibI] = sortrows([tmp SINR_DL],[ 1 -2 ] );
clear IotherLin I SINR_DL

% Calculate the part of uplink interference that is common to all paths
[~,LocpwrAmpClasBS] = ismember(pwrAmpClasBS(sitW),Sim.propag.BS(:,1));
[~,LocsituBS] = ismember(situBS(sitW),{'outd'});
[~,LocpwrAmpClasMS] = ismember(visib.pwrAmpClasMS,...
    Sim.dem.trafVol.MBB.devic.pwrAmpClas);
[~,LocsituMS] = ismember(visib.situMS,Sim.dem.trafVol.MBB.devic.situ);
assert(max(LocpwrAmpClasBS(:))<10 && max(LocsituBS(:))<10 && ...
    max(LocpwrAmpClasMS)<10 && max(LocsituMS)<10)
[C,ia,I_ULic] = unique(1000.*LocpwrAmpClasBS + 100.*LocsituBS + ...
    10.*LocpwrAmpClasMS + LocsituMS,'stable');
I_UL0 = zeros(length(C),1);  % dBm/180kHz
IM_DL0 = zeros(length(C),1);
for n = 1 : length(C)
    I_UL0(n) = Sim.linBud.(pwrAmpClasBS{sitW(ia(n))}).outd.NoiFloo + ...
        Sim.linBud.(pwrAmpClasBS{sitW(ia(n))}).outd.NoiFigu.(fs1) + ...
        10.*log10(0.18.*1000000) + ...
        Sim.linBud.(pwrAmpClasBS{sitW(ia(n))}).outd.IM - ...
        Sim.linBud.(visib.pwrAmpClasMS{ia(n)}).(visib.situMS{ia(n)}).IM;
    IM_DL0(n) = Sim.linBud.(visib.pwrAmpClasMS{ia(n)}).(visib.situMS{ia(...
        n)}).IM;
end
clear C ia NoiFloo LocpwrAmpClasBS LocsituBS LocpwrAmpClasMS LocsituMS

params2 = {sitW,sitI,secW,secI,wN,iN,sigmaW,sigmaI,visibI,LocisCell,Un,...
    Pic,P,P2,I_ULic,I_UL0,IM_DL0,N2,subs,N2_S,row};
