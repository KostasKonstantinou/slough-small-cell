%% Find network improvements for this year to satisfy unserved demand
function [Cell,edgCloSit,thisDem,interfIequi,unservOut,bund1] = find_impro(...
    Cell,edgCloSit,thisDem,interfIequi,unserv,Sim,studyA,clu,ter,...
    SINR2tput,Cell_save,InP_,tenan_,bund1,dsm,SlLigDat)
% find_impro finds improvements for this year to satisfy unserved demand.
% 
% unserv is the unserved demand expressed in Mbit/s in MxN, where M =
% length(thisDem) and N = length(Sim.dem.famUseCase_). unserv could be
% derived from thisDem.satis and thisDem.v, but it is faster to work with
% unserv, thus this function updates both thisDem and unserv.
% 
% Cell is an array of CELL objects. [A list of the required fields is pending]
% edgCloSit is an array of EDGCLOSIT objects. [A list of the required fields is pending]
% thisDem is an array of DEM objects. [A list of the required fields is pending]
% 
% Note that this function updates Cell, edgCloSit and thisDem, thus these
% are also output variables.
% 
% Cell_save is an array of CELL objects. These correspond to the status of
% the network prior to application of any rule, in case it is needed for
% rule reconsideration. Rules can be applied before enterring this function
% because refreshes and D-RAN to C-RAN rules are forced rules. A
% reconsideration occurs when there is leftover unserved demand after the
% application of a rule, and a rule with more antennas or carriers could be
% re-considered.
% 
% Sim is the SIM object. [A list of the required fields is pending]
% studyA is the study area object. [A list of the required fields is pending]
% clu is the CLU object. [A list of the required fields is pending]
% ter is the TER object. [A list of the required fields is pending]
% SINR2tput is the SINR2tput object. [A list of the required fields is pending]
% 
% InP_, tenan_ could be calculated by Sim.isShari, but to save on time are
% provided as input. InP_, tenan_ are cell lists of which InP can serve
% which tenant. For example InP_ = {[1]}, tenan_ = {[1,2]}, means InP 1 can
% serve Tenant 1 and Tenant 2, and that other InP (if any) do not serve any
% traffic.
% 
% unservOut is the amount of unserved demand at the end of optimisation.
% If the network cannot be further improved then there can be leftover
% unserved demand.
% 
% Kostas Konstantinou Oct 2018

%% Simplifications
isExpi = Sim.yea.thisYea > [Cell.expiYea];
Operator = {Cell.Operator};
pwrAmpClas = {Cell.pwrAmpClas};

%% Checking the feasibility of a site commission
% Find pixels where cells can be commisioned
construct = Sim.construct;

% Find pixels that are too close to existing infrastructure
construct = repmat(construct,[1,1,1,length(Sim.dem.operators)]);
for on = 1 : length(Sim.dem.operators)
    isOperato = strcmp(Sim.dem.operators{on},Operator);
    for m = 1 : size(construct,3)
        [I,J] = find(construct(:,:,m,on));
        IND = strcmp(Sim.propag.BS{m,1},pwrAmpClas) & ~isExpi & isOperato;
        if any(IND)
            if Sim.propag.BS{m,7} > 0
                fun = @(A,B) abs(A-B)<Sim.propag.BS{m,7}./Sim.SA.pixSize;
                Cell_IND_I = [Cell(IND).I];
                Cell_IND_J = [Cell(IND).J];
                C = bsxfun(fun,Cell_IND_I,I);
                [row,col] = find(C);
                if nnz(IND) > 1
                    % Cell_IND_I(col) is (1xN)
                    isTooClo = sqrt((Cell_IND_I(col).'-I(row)).^2 + ...
                        (Cell_IND_J(col).'-J(row)).^2) < ...
                        Sim.propag.BS{m,7} ./ Sim.SA.pixSize;
                else
                    % Cell_IND_I(col) is (Nx1)
                    isTooClo = sqrt((Cell_IND_I(col)-I(row)).^2 + ...
                        (Cell_IND_J(col)-J(row)).^2) < ...
                        Sim.propag.BS{m,7} ./ Sim.SA.pixSize;
                end
                construct(sub2ind(size(construct),...
                    I(row(isTooClo)),J(row(isTooClo)),...
                    m.*ones(nnz(isTooClo),1),...
                    on.*ones(nnz(isTooClo),1))) = false;
            end
        end
    end
end
clear I J n row m isOperato

% isOperato = strcmp('Vodafone',Operator);
% IND = strcmp('macrocell',pwrAmpClas) & ~isExpi & isOperato;
% figure
% hold on
% plot([Cell(IND).J],[Cell(IND).I],'r.')
% plot([Cell(IND).J],[Cell(IND).I],'ro')
% [row,col] = find(construct(:,:,1,1));
% plot(col,row,'kx')
% axis equal
% set(gca,'ydir','reverse')
% clear pwrAmpClas IND isOperato row col

%% Create a list of network improvements
% For each InP set, find improvement objects
p_ = [1 primes(size(unserv,1))];
pn = 1;
for on = 1 : length(InP_)
    % Sort the unserved demand in decreasing order
    [sortB,sortI] = sort(sum(unserv(:,tenan_{on}) * ...
        Sim.dem.consiInMeritPerTena(InP_{on}),2),'descend');
    IND = sortB == 0;
    sortI(IND) = [];

    % isOperato is true if the cell belongs to current InP
    if length(InP_{on}) == 1
        isOperato = strcmp(Operator,Sim.dem.operators(InP_{on}));
    else
        isOperato = ismember(Operator,Sim.dem.operators(InP_{on}));
    end
    
    % Find the map coordinates of the pixel that is selected to search for
    % a network improvement. The pixel selection is done simply by
    % selecting the pixel with the most unserved demand. This could be
    % improved in the future by looking at boxes/groups of pixels, similar
    % to coverage-extending exercises; there is commented code below but
    % progress on this was not sought further as it is unclear how
    % boxes/groups are affected by multiple bands.
    I = [thisDem(sortI).I];
    J = [thisDem(sortI).J];
    
%     % [Title]
%     Group = [];
%     for m = 1 : size(Sim.propag.BS,1)
%         % Calculate the groups
%         Cell_IND = find(strcmp(Sim.propag.BS{m,1},pwrAmpClas) & ...
%             ~isExpi & isOperato);
%         if ~isempty(Cell_IND)
%             pixMax = max(cell2mat(Sim.propag.BS(m,5))) ./ Sim.SA.pixSize;
%             Group = [Group
%                 c_group(Cell(Cell_IND),Sim,I,J,pixMax,Cell_IND)];
%         end
%     end
%     clear m Cell_IND
%         
%     % Sort the groups with the decreasing demand 
%     % Remove groups that overlap
%     [~,groupID] = sort(arrayfun(@(x) sum(sortB(x.memb)),Group),'descend');
%     Cell_IND = false(length(Cell),1);
%     IND = false(length(thisDem),1);
%     k = 1;
%     while k <= length(groupID)
%         neig = Group(groupID(k)).neig;
%         memb = Group(groupID(k)).memb;
%         if ~any(Cell_IND(neig)) && ~any(IND(memb))
%             Cell_IND(neig) = true;
%             IND(memb) = true;
% 
% %             plot(Group(groupID(k)).X + ...
% %                 Group(groupID(k)).r .* [-1 -1 1 1 -1],...
% %                 Group(groupID(k)).Y + ...
% %                 Group(groupID(k)).r .* [-1 1 1 -1 -1],'k','linewidth',2)
% %             X = [Group(groupID(k)).X.*ones(length(neig),1),...
% %                 [Cell(neig).J].'];
% %             Y = [Group(groupID(k)).Y.*ones(length(neig),1),...
% %                 [Cell(neig).I].'];
% %             line(X.',Y.','color','r','tag','del')
% 
%             k = k + 1;
%         else
%             groupID(k) = [];
% 
% %             plot(Group(groupID(k)).X + Group(groupID(k)).r.*[-1 -1 1 1 -1],...
% %                 Group(groupID(k)).Y + Group(groupID(k)).r.*[-1 1 1 -1 -1],...
% %                 'r','linewidth',2)
%         end
%     end
%     clear Cell_IND IND
%         
%     Group = Group(groupID);
        
        % [Title]
%         x0 = u_group(curGroup,curPoin,geographi,targServ,clu,Cell,...
%             availConfig,Sim,Flag);
    
%     figure
%     axis equal
%     set(gca,'ydir','reverse')
%     hold on
%     plot(J,I,'.')
%     plot([Cell.J],[Cell.I],'r.')
%     plot(bsxfun(@plus,[Group.X],...
%         bsxfun(@times,[Group.r],[-1 -1 1 1 -1].')),...
%         bsxfun(@plus,[Group.Y],...
%         bsxfun(@times,[Group.r],[-1 1 1 -1 -1].')),'k')
    
%     figure
%     axis equal
%     set(gca,'ydir','reverse')
%     hold on
%     plot(J,I,'.')
%     plot([Cell.J],[Cell.I],'r.')
%     plot(bsxfun(@plus,[Group(groupID).X],...
%         bsxfun(@times,[Group(groupID).r],[-1 -1 1 1 -1].')),...
%         bsxfun(@plus,[Group(groupID).Y],...
%         bsxfun(@times,[Group(groupID).r],[-1 1 1 -1 -1].')),'k')
    
    % While there are pixels with unserved demand which have not been
    % considered, perform network improvement
    N = 0;
%     multiWaitbar('Improving network...',0);
    while ~isempty(sortI)
%         thisGroup = Group(1);
        
        % Compile list of candidate improvements for the pixel at I,J
        thisImpro = c_thisImpro(Sim,on,InP_,I,J,construct,Cell,studyA,...
            isExpi,isOperato,[Cell.rul],pwrAmpClas,thisDem,SlLigDat,...
            edgCloSit,clu,ter,SINR2tput,unserv,tenan_,interfIequi,bund1,...
            dsm);
        
        % If there are possible improvements for this pixel, then calculate
        % their merit
        if ~isempty(thisImpro)
            % There are possible improvements for this pixel
            
            % Update while counter
            N = N + 1;
            
            % Calculate merit of each improvement and apply the improvement
            % that leads to the greatest merit, impro
            [thisDem,Cell,edgCloSit,impro,interfIequi,bund1] = c_merit(...
                thisDem,Cell,edgCloSit,thisImpro,Sim,studyA,clu,ter,...
                SINR2tput,unserv,Cell_save,[],InP_,tenan_,...
                interfIequi,bund1,dsm);
            
            % Calculate how much is the unserved demand after improvement
            % impro
            unserv = c_unserv(Sim,thisDem,studyA,'unserv','per pixel');
            
            % Display optimisation iteration information
            disp(['Itera ' num2str(N)])
            disp(impro)
            disp(sum(unserv,1))
            
            % If improvement impro was not found, i.e. all candidate
            % improvements had zero merit, then remove pixel I,J from the
            % list of pixels that need improvement. This is because there
            % is no available improvement that could serve the unserved
            % demand. This can occur when the demand is too dense and no
            % further antennas or carriers can be added.
            if ~isempty(impro)
                % Improvement impro has nonzero merit
                
                % Sort the unserved demand in decreasing order
                [sortB,sortI] = sort(sum(unserv(:,tenan_{on}) * ...
                    Sim.dem.consiInMeritPerTena(InP_{on}),2),'descend');
                IND = sortB == 0;
                sortI(IND) = [];
%                 sortB(IND) = [];

                % Find the map coordinates of the pixel that is selected to
                % search for a network improvement
                I = [thisDem(sortI).I];
                J = [thisDem(sortI).J];

                % If improvement impro is a new site, then simplified
                % variables and construct are updated
                if ~Sim.rul(impro.rulID).isDecomm
                    % Commission new site or upgrade existing site
                    if ~Sim.rul(impro.rulID).isUpg
                        % Commission new site
                        
                        % Update simplified isExpi, Operator, pwrAmpClas
                        isExpi(end+1) = false;
                        oI = impro.operatoI;
                        pI = impro.pwrAmpClasI;
                        pwrAmpClas{end+1} = Sim.propag.BS{pI,1};
                        Operator{end+1} = impro.operato;

                        % Update isOperato
                        if length(InP_{on}) == 1
                            isOperato(end+1) = strcmp(impro.operato,...
                                Sim.dem.operators(InP_{on}));
                        else
                            isOperato(end+1) = ismember(Operator,...
                                Sim.dem.operators(InP_{on}));
                        end

                        % Update construct according to proximity to the
                        % new site
                        [cI,cJ] = find(construct(:,:,pI,oI));
                        C = abs(Cell(impro.CellIND).I-cI)<Sim.propag.BS{...
                            pI,7} ./ Sim.SA.pixSize;
                        row = find(C);
                        isTooClo = sqrt((Cell(impro.CellIND).I-cI(row)).^2 + ...
                            (Cell(impro.CellIND).J-cJ(row)).^2) < ...
                            Sim.propag.BS{pI,7} ./ Sim.SA.pixSize;
                        construct(sub2ind(size(construct),...
                            cI(row(isTooClo)),cJ(row(isTooClo)),...
                            pI.*ones(nnz(isTooClo),1),...
                            oI.*ones(nnz(isTooClo),1))) = false;
                    else
                        % Upgrade existing site
                    end
                else
                end
            else
                % We could not find an improvement for this pixel, thus
                % remove it and go to the next
                P = min(p_(pn),length(sortI));
                sortI(1:P) = [];
                I(1:P) = [];
                J(1:P) = [];
                pn = pn + 1;
            end
        else
            % We could not find an improvement for this pixel, thus remove it and go to the next
            sortI(1) = [];
            I(1) = [];
            J(1) = [];
        end
%         multiWaitbar('Improving network...',(L0-length(sortI))./L0);

        p_map_serv_GE(Cell,studyA,Sim,thisDem,false,1:length(Cell))
        
        if length(Cell) == 5
            % Read COMB_SLOUGH
            M = m_shaperead(fullfile(Sim.path.on,'05 RW DB - General',...
                'Slough','COMB_SLOUGH'));
            combSl.lat = M.ncst{1}(:,2);
            combSl.lon = M.ncst{1}(:,1);
            combSl.X = combSl.lon;
            combSl.Y = combSl.lat;
            IND = ~isnan(combSl.lat);
            [combSl.X(IND),combSl.Y(IND)] = latLon2easNor(combSl.lat(IND),...
                combSl.lon(IND),[],Sim.mstruct.ostn);
            clear M

            ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
            figure('paperpositionmode','auto','unit','in',...
                'pos',[1 1 7+1./16 (7+1./16).*ratio])
            set(gca,'DataAspectRatio',[10 10 1])
            set(gca,'pos',[0 0 1 1])
            hold on

            line(SlLigDat.X.'.*ones(2,1),SlLigDat.Y.'.*ones(2,1),...
                [zeros(1,length(SlLigDat.X));SlLigDat.heigm.'],'color','g',...
                'linewidth',1.5)

            line([Cell.X].*ones(2,1),[Cell.Y].*ones(2,1),...
                [zeros(1,length(Cell));[Cell.heiAgl]],'color','r','linewidth',2)

            view(10,70)
            axis off
            camproj('perspective')

            % Add background map
            plo_m(Sim,'ShowLabels',1,'MapType','roadmap',...
                'brighten',0.5,'grayscale',1)

            Xlim = xlim;
            Ylim = ylim;
            plot(combSl.X,combSl.Y,'b','linewidth',2)
            set(gca,'xlim',Xlim,'ylim',Ylim)

            warning off all
            export_fig(fullfile('OutpM','SlLigDat resu 20200408.jpg'),'-djpeg','-r600')
            warning on all
            delete(gcf)
            
            beep
            disp('Coded up to here')
            keyboard
        end
    end
%     multiWaitbar('Improving network...','Close');
end

% Calculate unservOut
% INfoc = studyA.INfoc(sub2ind(size(studyA.INfoc),[thisDem.I],[thisDem.J]));
demAll = c_unserv(Sim,thisDem,studyA,'all',...
    'per hour and mobility class over focus');
assert(all(Sim.dem.consiInMeritPerTena),'Code this!')
unservOut = c_unserv(Sim,thisDem,studyA,'unserv',...
    'per hour and mobility class over focus') ./ demAll;
end

%%
function thisImpro = c_thisImpro(Sim,on,InP_,I,J,construct,Cell,studyA,...
    isExpi,isOperato,rul,pwrAmpClas,thisDem,SlLigDat,edgCloSit,clu,ter,...
    SINR2tput,unserv,tenan_,interfIequi,bund1,dsm)

thisImpro = [];  % Improvement objects in this iteration
% thisImpro will have that many improvements up to the number of
% rules for the worst pixel. Only one will be selected further
% down.

% pref = zeros(2,1);

n_ = find([Sim.rul.ValidFrom] <= Sim.yea.thisYea & ...
    Sim.yea.thisYea <= [Sim.rul.ValidTo]);

name = ['OutpAux\demPerImpro' num2str(length(Cell)) '.mat'];
if exist(name,'file')
    S = load(name,'demPerImpro');
    demPerImpro = S.demPerImpro;
    clear S
else
    assert(size(Sim.rul,1)==2,'Use of Sim.rul(1) should be amended')
    assert(length(Sim.dem.operators)==1)
    thisImpro = [];
    IND = find(arrayfun(@(x) any(full(x.v(:))),thisDem));
    [X,Y] = studyA.R.intrinsicToWorld([thisDem(IND).J],[thisDem(IND).I]);
    demPerImpro = nan(length(SlLigDat.X),1);
    LABEL = ['Lamppost ' num2str(length(Cell)+1)];
    multiWaitbar(LABEL,0);
    Lia = ismember(SlLigDat.X+1i*SlLigDat.Y,[Cell.X]+1i*[Cell.Y]);
    for lign = 1 : length(SlLigDat.X)
        if isnan(SlLigDat.heigm(lign)) || Lia(lign)
            continue
        end

        % Compile list of candidate improvements for each lamppost
        thisImpro.rulID = 1;
        thisImpro.operatoI = 1;
        thisImpro.operato = Sim.dem.operators{1};
        thisImpro.pwrAmpClasI = 1;
        thisImpro.CellIND = length(Cell) + 1;
        thisImpro.X = SlLigDat.X(lign);
        thisImpro.Y = SlLigDat.Y(lign);
        [thisImpro.I,thisImpro.J] = studyA.R.worldToDiscrete(...
            thisImpro.X,thisImpro.Y);
        thisImpro.XYorIJ = 'XY';
        thisImpro.heigm = SlLigDat.heigm(lign);

        % Calculate merit of each improvement and apply the improvement
        % that leads to the greatest merit, impro
        [~,sortI] = min((X-thisImpro.X).^2+(Y-thisImpro.Y).^2);
        thisDem2 = c_merit(thisDem,Cell,edgCloSit,thisImpro,Sim,studyA,...
            clu,ter,SINR2tput,unserv,Cell,IND(sortI),InP_,tenan_,...
            interfIequi,bund1,dsm);
        demPerImpro(lign) = sum(c_unserv(Sim,thisDem2,studyA,'serve',...
            'per pixel'),1);
        
        multiWaitbar(LABEL,lign./length(SlLigDat.X));
    end
    multiWaitbar(LABEL,'Close');
    clear X Y IND
    
    save(name,'demPerImpro')
end

for n = 1 : length(n_)
    % Update rulID field
    thisImpro(end+1).rulID = n_(n);

    % Update operato field
    if length(InP_{on}) == 1
        thisImpro(end).operatoI = InP_{on};
        
        thisImpro(end).operato = Sim.dem.operators{thisImpro(end).operatoI};
        
        % Find channels that are used by the site
        tmp = cell2mat(Sim.linBud.chRaste([false;true;false;false
            strcmp(Sim.rul(n_(n)).pwrAmpClas,Sim.linBud.chRaste(5:end,2)) & ...
            strcmp(thisImpro(end).operato,Sim.linBud.chRaste(5:end,3))],5:end));
        if size(tmp,1)>1 && ...
                any(Sim.rul(n_(n)).upg.Band(tmp(1,:))>0&tmp(2,:)==1)
            proc = true;
        else
            proc = false;
        end
        
        if proc
            if ~Sim.rul(n_(n)).isDecomm && ~Sim.rul(n_(n)).isUpg
                if ~Sim.dem.operat(on).canBui(...
                        strcmp(Sim.rul(n_(n)).pwrAmpClas,...
                        Sim.linBud.BS.pwrAmpClas_),...
                        Sim.yea.thisYea==Sim.dem.operat(on).canBuiYea)
                    proc = false;
                end
            end
        end
    else
        beep
        disp('Find channels that are used by the site')
        keyboard
        
        k = find(Sim.dem.operat.canBui);
        rng(1059+Sim.yea.thisYea+n_(n))
        thisImpro(end).operatoI = k(randi(nnz(Sim.dem.operat.canBui)));
        
        thisImpro(end).operato = Sim.dem.operators{thisImpro(end).operatoI};
    end

    if proc
        % Find the power amplifier class within construct 3rd dimension
        pwrAmpClasI = find(strcmp(Sim.rul(n_(n)).pwrAmpClas,...
            Sim.propag.BS(:,1)));
        thisImpro(end).pwrAmpClasI = pwrAmpClasI;

        if ~Sim.rul(n_(n)).isDecomm
            % Commission new site or upgrade existing site
    %                     thisImpro(end).CellIND = [];
            if ~Sim.rul(n_(n)).isUpg
                % Commission new site
                thisImpro(end).CellIND = length(Cell) + 1;
%                 thisImpro = upd_I_J_or_dele(thisImpro,pwrAmpClasI,Sim,I,J,...
%                     studyA,construct,InP_,on);

                [~,I2] = max(demPerImpro);
                thisImpro(end).X = SlLigDat.X(I2);
                thisImpro(end).Y = SlLigDat.Y(I2);
                thisImpro(end).XYorIJ = 'XY';
                thisImpro(end).heigm = SlLigDat.heigm(I2);
                [thisImpro(end).I,thisImpro(end).J] = ...
                    studyA.R.worldToDiscrete(thisImpro(end).X,...
                    thisImpro(end).Y);
            else
                % Upgrade existing site
                % Find the location for existing sites that is close to the
                % worst served pixel
                isPwrAmpClas = strcmp(pwrAmpClas,Sim.rul(n_(n)).pwrAmpClas);
                IND = c_IND('upg',pwrAmpClasI,Sim,I(1),J(1),...
                    size(studyA.INfoc),[Cell.I],[Cell.J],{isExpi,isOperato,...
                    rul,isPwrAmpClas,n_(n),Cell});

                % Append to thisImpro
                if ~isempty(IND)
                    thisImpro(end).I = Cell(IND).I;
                    thisImpro(end).J = Cell(IND).J;
                    thisImpro(end).CellIND = IND;
                else
                    thisImpro(end) = [];
                end
            end
        else
            % Reinstatement of expiring or expired site
            if ~Sim.rul(n_(n)).isUpg
                IND = c_IND('decomm',pwrAmpClasI,Sim,I(1),J(1),...
                    size(studyA.INfoc),[Cell.I],[Cell.J],...
                    {isExpi,isOperato,rul});

                % Append to thisImpro
                if ~isempty(IND)
                    curre = struct('sitChainConfig',Cell(IND).sitChaiConfig,...
                        'sectoCou',Cell(IND).sectoCou,...
                        'AeCou',Cell(IND).AeCou,...
                        'Band',Cell(IND).band.capa);
                    if strcmp(DataHash(curre),Sim.rul(n_(n)).upgHash)
                        thisImpro(end).I = Cell(IND).I;
                        thisImpro(end).J = Cell(IND).J;
                        thisImpro(end).CellIND = IND;
                    else
                        thisImpro(end) = [];
                    end
                else
                    thisImpro(end) = [];
                end
            else
                % Upgrade existing site
                % Find the location for existing sites that is close to the
                % worst served pixel
                isPwrAmpClas = strcmp(pwrAmpClas,Sim.rul(n_(n)).pwrAmpClas);
                IND = c_IND('upg',pwrAmpClasI,Sim,I(1),J(1),...
                    size(studyA.INfoc),[Cell.I],[Cell.J],{isExpi,isOperato,...
                    rul,isPwrAmpClas,n_(n),Cell});

                % Append to thisImpro
                if ~isempty(IND)
                    thisImpro(end).I = Cell(IND).I;
                    thisImpro(end).J = Cell(IND).J;
                    thisImpro(end).CellIND = IND;
                else
                    thisImpro(end) = [];
                end
            end
        end
    else
        thisImpro(end) = [];
    end
end
end

%%
function thisImpro = upd_I_J_or_dele(thisImpro,pwrAmpClasI,Sim,I,J,...
    studyA,construct,InP_,on)
% Find the location for a site commission that is on the worst served pixel
[IND,thisOperato] = c_IND('new',pwrAmpClasI,Sim,I(1),J(1),...
    size(studyA.INfoc),I(1),J(1),{construct,InP_{on}});
[thisImpro,IND] = upd_I_J(thisImpro,IND,thisOperato,I,J,Sim);

if isempty(IND)
    % The site cannot be commissioned on the worst served pixel
    % Find pixels within the search area that
    % are unserved
    [IND,thisOperato] = c_IND('new',pwrAmpClasI,Sim,I(1),J(1),...
        size(studyA.INfoc),I,J,{construct,InP_{on}});
    [thisImpro,IND] = upd_I_J(thisImpro,IND,thisOperato,I,J,Sim);
end

if isempty(IND)
    % Find pixels within the search area regardless if unserved or not    
    if length(InP_{on}) == 1
        [constructI,constructJ] = find(construct(:,:,pwrAmpClasI,...
            InP_{on}));
    else
        [constructI,constructJ] = find(any(construct(:,:,pwrAmpClasI,...
            InP_{on}),4));
    end
    [IND,thisOperato] = c_IND('new',pwrAmpClasI,Sim,I(1),J(1),...
        size(studyA.INfoc),constructI.',constructJ.',{construct,InP_{on}});
    [thisImpro,IND] = upd_I_J(thisImpro,IND,thisOperato,...
        constructI.',constructJ.',Sim);
end

if isempty(IND)
    thisImpro(end) = [];
end
end

%%
function [thisImpro,IND] = upd_I_J(thisImpro,IND,thisOperato,I,J,Sim)
if ~isempty(IND)
    if size(thisOperato,1) > 1
        % Sharing is assumed
        if ~any(strcmp(thisImpro(end).operato,...
                Sim.dem.operators(thisOperato(:,IND))))
            % The randomly selected operator cannot
            % own this new site because it owns
            % existing infrastructure closeby
            if nnz(thisOperato(:,IND)) == 1
                % Ensure the operator is the one that
                % does not have existing infrastructure
                % closeby
                thisImpro(end).operatoI = find(thisOperato(:,IND));
                thisImpro(end).operato = ...
                    Sim.dem.operators{thisImpro(end).operatoI};
            else
                beep
                disp('If the code is here, it is because there are 3+ operators and this case has not been thought of!')
                keyboard
            end
        end
    end

    % Append to thisImpro
    thisImpro(end).I = I(IND);
    thisImpro(end).J = J(IND);
end
end

%%
% pwrAmpClasI = 1;
% Col = struct('Operator',{'Vodafone','Orange'},...
%     'colour',num2cell([238 1 31; 255 101 1],2).');
% Marker = struct('pwrAmpClas',Sim.propag.BS(:,1),...
%     'sectoCou',{3 2 1}.',...
%     'radiu',num2cell(cell2mat(Sim.propag.BS(:,5))./4));
% pwrAmpClas = {Cell.pwrAmpClas};
% pwrAmpClas_ = unique(pwrAmpClas);
% [row,col] = find(construct(:,:,pwrAmpClasI));
% 
% % figure
% % plot(col,row,'.')
% % axis equal
% % set(gca,'ydir','reverse')
% % 
% % [X,Y] = studyA.R.intrinsicToWorld(thisImpro(1).J,thisImpro(1).I);
% % plot(X,Y,'rx')
% % 
% % [X,Y] = studyA.R.intrinsicToWorld(thisImpro(2).J,thisImpro(2).I);
% % plot(X,Y,'ro')
% 
% 
% figure
% hold on
% [X,Y] = studyA.R.intrinsicToWorld(J,I);
% X = bsxfun(@plus,X,25./2.*[-1,-1,1,1,-1].');
% Y = bsxfun(@plus,Y,25./2.*[-1,1,1,-1,-1].');
% patch(X,Y,ones(size(X)),'facecolor',[0 0 1],...
%     'edgecolor','none','facealpha',0.5,'tag','del')
% [X,Y] = studyA.R.intrinsicToWorld(J(1),I(1));
% plot(X+cosd(0:360).*Sim.propag.BS{pwrAmpClasI,5}./2,...
%     Y+sind(0:360).*Sim.propag.BS{pwrAmpClasI,5}./2,'k--')
% text(X+Sim.propag.BS{pwrAmpClasI,5}./2,Y,...
%     [' ' num2str(Sim.propag.BS{pwrAmpClasI,5}./2) 'm'])
% [X,Y] = studyA.R.intrinsicToWorld(col,row);
% plot(X,Y,'kx')
% % [row,col] = find(construct(:,:,2,1));
% % [X,Y] = studyA.R.intrinsicToWorld(col,row);
% % plot(X,Y,'ko')
% for n = 1 : length(pwrAmpClas_)
%     for on = 1 : length(Sim.dem.operators)
%         IND = strcmp(pwrAmpClas_{n},pwrAmpClas) & ...
%             strcmp({Cell.Operator},Sim.dem.operators{on});
%         
%         MarkerI = strcmp(pwrAmpClas_{n},{Marker.pwrAmpClas});
%         
%         switch Marker(MarkerI).sectoCou
%             case 3
%                 X = bsxfun(@plus,[Cell(IND).X],Marker(MarkerI).radiu .* ...
%                     cosd([90,90-0+(-69./2:69./2),90,...
%                     90-120+(-69./2:69./2),90,...
%                     90-240+(-69./2:69./2),90]).');
%                 Y = bsxfun(@plus,[Cell(IND).Y],Marker(MarkerI).radiu .* ...
%                     sind([0,90-0+(-69./2:69./2),0,...
%                     90-120+(-69./2:69./2),0,...
%                     90-240+(-69./2:69./2),0]).');
%             case 2
%                 X = bsxfun(@plus,[Cell(IND).X],Marker(MarkerI).radiu .* ...
%                     cosd([90,90-0+(-69./2:69./2),90,...
%                     90-180+(-69./2:69./2),90]).');
%                 Y = bsxfun(@plus,[Cell(IND).Y],Marker(MarkerI).radiu .* ...
%                     sind([0,90-0+(-69./2:69./2),0,...
%                     90-180+(-69./2:69./2),0]).');
%             case 1
%                 X = bsxfun(@plus,[Cell(IND).X],Marker(MarkerI).radiu .* ...
%                     cosd([90,90-0+(-360./2:360./2),90]).');
%                 Y = bsxfun(@plus,[Cell(IND).Y],Marker(MarkerI).radiu .* ...
%                     sind([0,90-0+(-360./2:360./2),0]).');
%         end
%         patch(X,Y,ones(size(X)),'facecolor',Col(on).colour./255,...
%             'edgecolor','none','facealpha',0.5,'tag','del')
%         plot([Cell(IND).X],[Cell(IND).Y],'.','color',Col(on).colour./255,...
%             'tag','del')
%     end
% end
% axis equal
% title(['Demand not served  (Mbit/s): ' num2str(round(sum(unserv(:))))])
% % plot_map(suggest_scale(gca),[],gca);
% 
% clear Col Marker pwrAmpClas pwrAmpClas_ n on IND MarkerI X Y row col pwrAmpClasI

%%
