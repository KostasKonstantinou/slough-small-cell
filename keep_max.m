function ix = keep_max(indx,s)
% Find the index that corresponds to the n-th maximum value
% findmaxN returns ix, the index amongst indx which corresponds to the
% n-th maximum value of s. This function is used by accumarray.
% 
% Example 1: 
% Find the index amongst 2 indices that corresponds to the maximum of 8
% values
%   
% s = [-52.5;-52.6;-71.8;-52.2;-52.3;-71.4;-71.4;-73.9];
% indx = [5;2];
% ix = findmaxN(indx,s,2)
% 
% The above example returns:
% ix = 2
% 
% * v0.1 Kostas Konstantinou Dec 2016

% Find the index that corresponds to the second maximum value of s
L = length(indx);
if L == 1
    % Single index
    ix = {indx};
else
    % Multiple indices
    [~,ix] = sort(s(indx),'descend');
    ix = {indx(ix)};
end
