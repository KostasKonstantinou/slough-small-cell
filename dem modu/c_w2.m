function dem = c_w2(dem,Sim,studyA)

% Read places from Google
S = g_populartimes(studyA,Sim);

% Define weights per place type
C = unique({S.type}.');
M = [C num2cell(ones(length(C),1))];
clear C

% Convert the polygon of the study area to cell format
[Xcells,Ycells] = polysplit(studyA.buf.X,studyA.buf.Y);

% Find which entries are within the polygon
if isfield(Sim.mstruct,'ostn')
    [SX,SY] = latLon2easNor([S.lat].',[S.lon].',[],Sim.mstruct.ostn);
    SX = SX.';
    SY = SY.';
elseif isfield(Sim.mstruct,'mapprojection')
    [SX,SY] = mfwdtran(Sim.mstruct,[S.Y],[S.X]);
else
    error('Code this!')
end
IN = false(size(SX));
for m = 1 : length(Xcells)
    if ispolycw(Xcells{m},Ycells{m})
        IN(inpoly([SX.' SY.'],[Xcells{m} Ycells{m}])) = true;
    else
        IN(inpoly([SX.' SY.'],[Xcells{m} Ycells{m}])) = false;
    end
end

% Remove entries outside the study area
S(~IN) = [];
SX(~IN) = [];
SY(~IN) = [];
IN(~IN) = [];

% Write kml
k = kml('OutpM\Places');
for n = 1 : length(S)
    k.point(S(n).lon,S(n).lat,1,'iconURL','wht-stars',...
        'iconScale',1,'name',S(n).type);
end
k.save;

% Initialise w
for n = 1 : length(dem)
    dem(n).w = double(dem(n).A>0);
end

% % 511X seem to be motorway
% % 512X seem to be roads
% % 513X seem to be links
% % 514X seem to be tracks
% % 515X seem to be pedestrian
% % 519X seem to be unknown
% % isWzero = find(fix([dem.code]./10)==514);
% isPOIcandi = find(fix([dem.code]./10)==512);

% Find if the weight is applicable.
% Note that the order here is important because of the while loop that
% increases gradually the search radius is broken when the weight has been
% applied. The order is:
% 1. Check if the POI pixel contains A road
% 2. If not, check if the POI pixel contains B road
% 3. If not, check if the POI pixel contains Minor road
% 4. Check if the POI + 4 connected pixels contain A road
% 5. If not, check if the POI + 4 connected pixels contain B road
% 6. If not, check if the POI + 4 connected pixels contain Minor road
% 7. etc.
isPOIcandi = find(ismember({dem.layNam},{'A road','B road','Minor road'}));

% For each type, find entries (POI) and associate a demand-source and
% -pixel to increase the latter's weight
for m = 1 : length(M)
    if M{m,2} == 0
        continue
    end
    
    % Find database entries of this type.
    IND = find(strcmp({S.type},M{m,1}));
    x = SX(IND);
    y = SY(IND);
    [I_,J_] = studyA.R.worldToDiscrete(x,y);
    J_(isnan(I_)) = [];
    I_(isnan(I_)) = [];
    
    % For each database entry of this type, find the demand source and
    % pixel that lies close to (I_(n),J_(n)) and increase its weight
    for n = 1 : length(I_)
        % Initialise pixel size.
        % N = 0 corresponds to consideration of the same pixel as that of
        % the database entry (I_(n),J_(n)).
        % N = 1 corresponds to consideration of the same pixel plus 1
        % connected tier; connectivity of 4 is assumed.
        N = 0;

        % While loop to gradually increase the search radius for demand
        % pixels that lie close to database entry pixels
        whilArg = true;
        while whilArg
            % Find indices of the considered pixels.
            % Connectivity of 8 is assumed, but then trimmed to 4 by use of
            % distPix2max, see below.
            I = I_(n) + (-N:N);
            J = J_(n) + (-N:N);
            I(I<1) = [];
            J(J<1) = [];
            I(I>studyA.R.RasterSize(1)) = [];
            J(J>studyA.R.RasterSize(2)) = [];

            % Calculate the distance of considered pixels from
            % (I_(n),J_(n)).
            % The maximum square distance, distPix2max, is calculated to
            % trim the connectivity of 8 to 4.
            [J2,I2] = meshgrid(J,I);
            distPix2 = (I2-I_(n)).^2+(J2-J_(n)).^2;
            distPix2max = N .^ 2;
            
            % Find demand at considered pixels
            demConn = arrayfun(@(x) full(x.A(I,J)),dem(isPOIcandi),...
                'UniformOutput',false);
%             demConn = arrayfun(@(x) full(x.A(min(max(I,1),sz(1)),...
%                 min(max(J,1),sz(2)))),dem(isPOIcandi),...
%                 'UniformOutput',false);
            
            % For each demand source, update w
            for k = 1 : length(demConn)
                % Find considered pixels with non-zero demand of this
                % demand-source with connectivity of 4
                IND = demConn{k}>0 & distPix2<=distPix2max;
                
                % If there are any such pixels, then: a) find the closest
                % such demand pixel and increase its weight by adding to it
                % the value provided in the weight input file, and b) break
                % the while loop which increases the search radius for
                % pixel consideration.
                if any(IND(:))
                    if nnz(IND) > 1
                        IND_f = find(IND);
                        [~,minI] = min(distPix2(IND));
                        I3 = I2(IND_f(minI));
                        J3 = J2(IND_f(minI));
                    else
                        I3 = I;
                        J3 = J;
                    end
%                     dem(isPOIcandi(k)).w(I3,J3) = ...
%                         min(dem(isPOIcandi(k)).w(I3,J3).*M{m,2},100);
                    dem(isPOIcandi(k)).w(I3,J3) = ...
                        dem(isPOIcandi(k)).w(I3,J3) + M{m,2};
                    whilArg = false;
                    break
                end
            end

            % If none of the demand sources have demand pixels with
            % non-zero demand within the search radius, then increase the
            % search radius by one pixel
            if whilArg
                N = N + 1;
            end
        end
    end
end
