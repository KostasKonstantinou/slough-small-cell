function subsDB = select_subs(dem,Sim,fam,famI,studyA)
% select_subs returns subsDB, a structure containing the subscribers of a
% specific family of services (or just service).
% Note that the output subsDB contains all the potential
% subscribers, regardless of if these are subscribers in current year. This
% was done because we wished to potentially change the subscriber
% percentage with the passing years, e.g. increase the subscribers with
% time. 
% 
% The output structure subsDB contains the following fields:
% I - Integer scalar of the row of the subscriber entry within studyA.R.
% J - Integer scalar of the column of the subscriber entry within studyA.R.
% isSubs - Logical scalar that is true if the subscriber entry is a
%          subscriber this year.
% layI - Integer scalar index of dem. It is the demand source/layer index.
% operator - Mx1 logical vector, where M=length(Sim.dem.operators), with
%            element that is true when the subscriber belongs to the
%            operator.
% percTrafVol - Scalar that is the proportion of the demand-source demand
%               that is associated with the subscriber entry. Populated by
%               distri_trafVol_across_subs().
% firsYea - Array of integers 1xN, where
%           N=length(Sim.dem.trafVol.(fam).devic.pwrAmpClas), indicating
%           the first year the subscriber entry becomes a subscriber.
% aeCou - Array of integers 1xN, where
%         N=length(Sim.dem.trafVol.(fam).devic.pwrAmpClas), indicating the
%         number of UE antennas. If isSubs is false, then aeCou = []. If
%         isSubs is true, then the entries of aeCou can be zero when [...]
%
% The input dem is a demand source DEM object and must contain the
% following fields, see class for definition: A, layNam, w
% 
% The input Sim is a SIM object and must contain the following fields,
% see class for definition:
% dem.operators
% dem.trafVol
% dem.famUseCase_
% dem.trafVol
% SA.pixSize
% yea.start
% yea.thisYea
% seed.subs
% linBud
% 
% The input fam is the family of use cases (essentially the service) which
% we wish the subscribers for, and must be one of Sim.dem.famUseCase_.
% The input famI is the index of fam within Sim.dem.famUseCase_.
% 
% The input studyA is a study area object and must contain at least the
% following fields, see class for definition:
% INhpa
% R.RasterSize
% AGV
% INautomVeh1, INautomVeh2, etc.
% cruShipTer
% INcruShipTer1, INcruShipTer2, etc.
% 
% * v0.1 Kostas Konstantinou May 2016
% * v0.2 Alejandro Aragon Jun-Jul 2016
% * v0.3 Kostas Konstantinou Aug 2016
% * v0.4 Tasos Karousos Sep-Oct 2016
% * v0.5 Kostas Konstantinou May-Aug 2018
% * v0.6 Kostas Konstantinou Oct 2018

%% Calculations
% Initialise
subsDB = [];

% The input dem is an array of 10 structures, see layNam property:
% 1. population, expressed in number of people
% 2. DELR, expressed in number of residential addresses (premises)
% 3. DELN
% 4. DELL
% 5. motorway
% 6. A road
% 7. B road
% 8. minor road
% 9. railway
% 10. tunnelled railway
% It is hardcoded here that:
% 1. For eMBB the subscribers (people) are chosen from demand source
%    'population', rather than from the demand source DELR, because we
%    wish the susbscribers to be a proportion of the population. It is
%    reminded that 'population' is in the same locations as 'DELR'. Demand
%    source 'population' generates subscribers which are situated indoors,
%    and the thoroughfare demand sources generate subscribers which are
%    situated outdoors; the situation is not specified in this class. 
% 2. For connected traffic lights the subscribers (master traffic lights)
%    are chosen from any demand source. Demand source DELR is preferred
%    over 'popu' because there is no need to bias the location of connected
%    traffic lights according to the population.
switch fam
    case 'MBB'
        % We do not include the 2nd demand source (DELR) for MBB
        nn = [1 3:length(dem)];
    case {'conneTrafLig','AGV','CCTV','AR','ciITS','infot','semiAutoDr',...
            'assisDr'}
        % We do not include the population for the other use cases
        nn = 2 : length(dem);
    otherwise
        error('Insert this!')
end

% DEfine list of layNum
mm = 1 : length(dem) - 1;

% Initialising the subscribers' database
for layNum = mm %HO: for each layer (source of demand)
    subsDB = [subsDB
        create_DB(dem(nn(layNum)),layNum,fam,famI,Sim,studyA)]; %HO: this function initialise the subs DB for each source of demand (DELL, DELN, A Road, etc..). For each layer (each loop) we add the subscribers in the subs
    %HO: assign subs to operator. The information is saved mainly in subs.operator:  Here the indices of the subs for this operator are included (in form of Cells) in subsDB.operator struct in form of TRUE so information is saved 
end

% The model's operators
for oper = 1 : length(Sim.dem.operators)
    for layNum = mm
        subsDB  = populate_DB(dem(nn(layNum)),subsDB,layNum,oper,fam,Sim);
        %HO: find which sub belong to which oper and find number of
        %antennas per UE
    end
end
end

%% CREATE_DB subfunction
function subsDB = create_DB(dem,layNum,fam,famI,Sim,studyA)
%CREATE_DB is a helper function to create the subscriber database. The inputs are:
%       dem          : the current layer
%       layNum       : the current layer (demand source) index
%       famI : the current use case family index
%       Sim          : the simulation's data
%HO: we are mainly initalising the subsDB fields (all fields) and populating mainly subsDB.operator.
%HO: In this function he may distribute the subs to pixels.. and allocate these to an oper

% Maximum percentage of subscribers for that operator for that layer (last year)
subPer = zeros(length(Sim.dem.operators),1);
for n = 1 : length(subPer)
    subPer(n) = Sim.dem.trafVol.(Sim.dem.famUseCase_{famI}...
        ).subPenet.(Sim.dem.operators{n}).subPerc(layNum,end);
end

if all(subPer==0)
    subsDB = [];
    return
end

% Restrict within HPA or other polygon, if needed
switch Sim.dem.trafVol.(fam).mean.inPoly
    case 'INbuf'
        % Restrict on roads
        [row,col] = find(dem.A);
    case 'INhpa'
        % Restrict on roads within HPA polygon
        IN = studyA.INhpa;
        [row,col] = find(dem.A&IN);
    case 'INagv'
        % Restrict within AGV polygon
        IN = false(studyA.R.RasterSize(1),studyA.R.RasterSize(2));
        for n = 1 : length(studyA.AGV)
            IN = IN | studyA.(['INagv' num2str(n)]);
        end
        [row,col] = find(IN);
    case 'INcruShipTer'
        % Restrict within cruShipTer polygon
        IN = false(studyA.R.RasterSize(1),studyA.R.RasterSize(2));
        for n = 1 : length(studyA.cruShipTer)
            IN = IN | studyA.(['INcruShipTer' num2str(n)]);
        end
        [row,col] = find(IN);
    case 'INcctv'
        % Restrict within CCTV polygon
        IN = studyA.INhpa & studyA.ONland;
        [row,col] = find(IN);
    case 'INar'
        % Restrict within AR polygon
        IN = studyA.INhpa & studyA.ONland;
        [row,col] = find(IN);
    otherwise
        error('Insert this!')
end

% The total population of this layer
IND = sub2ind(size(dem.A),row,col);
switch Sim.dem.trafVol.(fam).mean.inPoly
    case {'INbuf','INhpa'}
        % Restrict on roads or addresses
        switch dem.layNam
            case {'popu','DELR','DELN','DELL'} %HO: Sources of demand.
                v = full(dem.A(IND).*dem.w(IND));
            case {'Motorway','A road','B road','Minor road','Railway','Tunnelled railway'}
                % Restrict on roads
                v = full(dem.A(IND).*dem.w(IND)) ./ Sim.SA.pixSize;
            otherwise
                error('N/A')
        end
    case {'INagv','INcruShipTer','INcctv','INar'}
        % Restrict within polygon
        v = ones(size(row));
    otherwise
        error('Insert this!')
end
pop = fix(sum(v));  %HO: number of people for this layer. 
IND = zeros(pop,1);
B = cumsum(v);  %HO: cumulative sum of pop for this category
%
% tic
% for n = 1 : pop
%     IND(n) = find(B>=n,1);
% end
% toc  % 8 s
%
% tic
% test2 = cellfun(@(x) find(B>=x,1),num2cell([1:pop]));
% toc  % 11 s
%
% test1 = zeros(pop,1);
% tic
if pop > 0
    n_ = 1 : round(sqrt(pop)) : pop;
    if n_(end) < pop
        n_ = [n_ pop];
    end
    for n = 1 : length(n_)-1
        IND(n_(n):n_(n+1)) = sum(bsxfun(@lt,B,n_(n):n_(n+1)),1) + 1;
    end
end
% toc  % 7 s
%
% tic
% test3 = sum(bsxfun(@lt,B,[1:pop]),1) + 1;
% toc  % 11 s

%HO: Here we find for each pop the related coordinates?
row2 = row(IND);
col2 = col(IND);

% Initialise output
rng(1429+layNum) %@HO: shall we externalise this seed? -> Yes, it would be a good idea (KK)
subsDB = struct('I',num2cell(row2),...
    'J',num2cell(col2),...
    'isSubs',false,...
    'layI',layNum,...
    'operator',false(length(Sim.dem.operators),1),...
    'percTrafVol',0,...
    'firsYea',num2cell(Sim.yea.start-round(rand(pop,... %HO: WE ARE using somewhere the firsYea -> This is the first year where the subs becomes a subs, KK: this is being used below for antenna count purposes..
    length(Sim.dem.trafVol.(fam).devic.pwrAmpClas)) .* ...
    repmat(Sim.dem.trafVol.(fam).devic.lifet,pop,1)+0.5),2),...
    'aeCou',[]); %HO: here the first year is a random value
% whos subsDB
% uint8 does not reduce the size adequately
% neither native nor sparse reduce the size adequately

% If that layer does not have population then exit
if pop == 0
    return
end

% Preallocate the subscribers to operators, as it is not possible to save the info per year -
% making use of the seed, we can deduce which year the user was subscribed on that operator and
% family
operIn = 0;
Opt.Format = 'double';
for oper = Sim.dem.operators
    % The operator's index
    operIn = operIn + 1;
     
    % Correcting the rounding errors that may give rise to inaccuracies to the number of subcribers
    switch dem.layNam
        case {'popu','DELR','DELN','DELL'}
            subs = find(~any([subsDB.operator],1));  % The available subscribers
            subCount = round(subPer(operIn)*pop); %HO: number of subs for this operator. For example for NORMA, popu (~517k) and 17% subPer the total number is ~91k.
            %HO: The following looks like a verification
            if subCount - length(subs) > 0 %@HO: Why do we need an if statment here? the previous line of code has calculated and rounded the number of subs?
                if subCount - length(subs) <= length(Sim.dem.operators) - 1 %HO: if there is one sub extra then there is no prob (there is rounding error) otherwise there is a prob...)
                    subCount = length(subs);
                else
                    error('There are more subscribers needed than actually exist');
                end
            end
        case {'Motorway','A road','B road','Minor road','Railway','Tunnelled railway'}
            subs = 1 : length(subsDB);  % The available subscribers.  @HO: it looks like we are using here the number reported in dem(5) and above as number of pixels and NOT in m value?? -> KK: correct, this is the number of demand points (can be one in each pixel or two) for the thorou case..
            subCount = round(subPer(operIn)*pop);  %@HO: why in the A road case the subPer is 100%??
        otherwise
            error('N/A')
    end
    
    % Person index to be assigned to new subscribers
    rng(sum(DataHash({Sim.seed.subs,oper{:}},Opt)))  %HO: this is a seed here. @HO: why do we need DataHash for this seed? -> KK: Yes.
    perIND = randperm(length(subs)); %HO: we are doing here a random permutation of the index of the subs. Note that these are all subs for this layer.
    perIND = subs(perIND(1:subCount)); %HO:  subCount is the number of subs for this operator. We have selected here the first subCount of subs from the total subs, so perIND have the indices of the subs for this operator chosen from the total subs
    
    tmp = [subsDB(perIND).operator]; % HO: subsDB has defined/initialized earlier.. tmp is still zero. selecting the indices of the subs for this operator from total subs. Here we are looking at subsDB.operator
    tmp(operIn,:) = true; %HO: tmp elements are All TRUE now..
    tmp = num2cell(tmp,1); %HO: convert to Cell
    [subsDB(perIND).operator] = deal(tmp{:}); %HO: tmp becomes part of subsDB now..   Here the indices of the subs for this operator are included (in form of Cells) in subsDB.operator struct in form of TRUE so information is saved     
end
end

%% POPULATE_DB subfunction
function subsDB = populate_DB(dem,subsDB,layNum,operator,fam,Sim)
%POPULATE_DB is a helper function to populate the subscriber database. The inputs are:
%       dem          : the current layer
%       subsDB       : the subscriber database to be populated
%       layNum : the current layer (demand source) index
%       operator     : the current operator index
%       famI : the current use case family index
%       Sim          : the simulation's data
%
% NOTE: Given that it is too costly to save and load the subsDB due to its size, we should
% reconstruct it every time. The only difference though is to understand when the subscriber
% actually started his contract so as to assign the right number of antennas, as his equipment may
% have been upgraded. This will occur with the help of the constant seed.

%HO: How many subs belong to one operator in this year (this what this
%funciton is doing)
Opt.Format = 'double';

% Subscribers populations
% All the subscribers in that layer and use case
subLayIndex = find([subsDB.layI]==layNum); %HO: we find here the indices of subs that belong to that layer (demand source)

% The population of that layer and use case
pop = length(subLayIndex); 

% If this layer does not have any population, exit
if pop == 0
    return
end

% The index of the first subscriber in that layer and use case family
% firstSub = subLayIndex(1);

% Finding the target percentages and population for this year
% The current traffic volume info for that use case family
trafVolUs = Sim.dem.trafVol.(fam);

% The current information subscriber penetration data
curOperator = trafVolUs.subPenet.(Sim.dem.operators{operator});

% The current and previous years
curYear = curOperator.year <= Sim.yea.thisYea;

% Percentage of subscribers for this operator and use case family, in this year and previous ones
percentage = curOperator.subPerc(layNum,curYear);

% The operator's population for that layer and use case - NOT all of them are subscribers though yet
tmp = [subsDB(subLayIndex).operator]; %HO: we have only ~17% (provided by input) of MBB pop belonging for that subs
operSub = tmp(operator,:); %HO: subs belonging to this operator. If we have only one oeprator then operSub and tmp are the same
operPop = sum(operSub);  %HO: total subrscibers for that operators. @HO: we can probably put an assertion here, operSub./operPop == subPer (calculated above or see few lines below)-> Probably yes.
 
% Number of Mobile Stations. If -999 then ignored. Otherwise the subscriber
% percentage may create all pixels as candidate subscribers  but only a few
% pixels will be selected.
MScou = curOperator.MScou(layNum,curYear);
if MScou(end) == -999
    % The total number of subscribers for this operator in this year and previous ones
    assert(all(MScou==-999))
    targPop = round(pop.*percentage);
else
    % Set the population from the csv file
    targPop = MScou;
end

if targPop(end) - operPop > 0
    if targPop(end) - operPop <= length(Sim.dem.operators) - 1
        targPop(end) = operPop;
    else
        error('There are more subscribers needed than actually exist');
    end
end

% This operator does not have subscribers in this layer, thus exit
if operPop == 0 && targPop(end) == 0
    return
end

% The device information
devic = trafVolUs.devic; %HO: Getting information provided in inputs files


% Finding the antenna count for that use case (e.g. MBB), layer (demand
% source) and year (e.g. 2017). @HO: devic.perDemSour is not the antenna it
% should be the speed of the UE pwramp? -> This ttiel is for the whole
% paragraph..
% KK: Removed the AeCou per year
IND = find(devic.perDemSour(layNum,:)>=0); %HO: find in any speed is bigger than zero for this layer (source of demand)
if isempty(IND)
    AeCou = {}; %HO: then set AeCou to zero..
else
    AeCou = cell(1,length(IND));
    for n = 1 : length(IND)
        AeCou(1,n) = Sim.linBud.(devic.pwrAmpClas{IND(n)}).(...
            devic.situ{IND(n)}).AeCou(1);
    end
    
%     AeCou = cell(length(targPop),length(IND));
%     for n = 1 : length(IND)
%         curYearLinBud = Sim.linBud.(devic.pwrAmpClas{IND(n)}).(...
%                 devic.situ{IND(n)}).AeCouYea <= Sim.yea.thisYea;
%         
%         AeCou(:,n) = Sim.linBud.(devic.pwrAmpClas{IND(n)}).(...
%             devic.situ{IND(n)}).AeCou(curYearLinBud);
%     end
end

% The lifetime of the current equipment in this layer
lifet = devic.lifet(devic.perDemSour(layNum,:)>=0); %HO: Getting information provided in inputs files. @HO: why lifetime in the first case is 0 (see next line comments)?? We are not considering this layer (see input file MBB devicPerDemSour.csv) -> KK: we set this in purpose to negative values for speed in inut file, this way we don't serve the demand for DELR. We want only to serve demand to thorou points for the MBB case..
%@HO: it looks like if the non of the speed is >0 for this layer, then we consider the lifetime for this layer is zero.. Why??

% The years of simulation
totalYears = sum(curYear);

% Subscribers
% We find the subscribers for that operator, layer, use case family and year

% Finding which subscribers are allocated to this operator
posSubs = find(operSub); %HO?: we have already done this in previous paragraph? why repeating?

% Person index to be assigned to new subscribers
rng(sum(DataHash({1632,Sim.dem.operators{operator},dem.layNam},Opt)))
perIND = randperm(operPop); %HO: we are doing here a random permutation of the index of the subs
perIND = subLayIndex(posSubs(perIND(1:targPop(end)))); %HO: this shoudl be the indices of the subs for this operator according to the total subs. %HO?: we have already done this in previous paragraph? why repeating?

% Create a map with the subscriber count
% [~,~,v] = find(dem.A);
% B = cumsum(v);

% tic
% IND = zeros(length(perIND),1);
% for n = 1 : 10 : length(perIND)
%     IND2 = n : min(n+9,length(perIND));
%     IND(IND2) = sum((repmat(perIND(IND2),length(B),1)-firstSub+1 > ...
%         repmat(B,1,length(IND2))),1) + 1;
% end
% toc
% 
% tic
% for n = 1 : length(perIND)
%     IND = sum(perIND(n) - firstSub + 1 > B) + 1;
% end
% toc

isProc = true;
if ~isempty(AeCou)
    C = unique(AeCou);
    if all(size(C)==[1 1])
        switch C{1,1}
            case '[2 1.0; 4 0.0]'
                AeCouUser_ = 2;
                isProc = false;
            case '[2 0.0; 4 1.0]'
                AeCouUser_ = 4;
                isProc = false;
            case '[1 1.0]'
                AeCouUser_ = 1;
                isProc = false;
            case '[2 1.0]'
                AeCouUser_ = 2;
                isProc = false;
            case '[4 1.0]'
                AeCouUser_ = 4;
                isProc = false;
            case '[8 1.0]'
                AeCouUser_ = 8;
                isProc = false;
            otherwise
                beep
                disp('Check if code optimisation can be done in this case')
                keyboard
        end
    else
        beep
        disp('Check if code optimisation can be done in this case')
        keyboard
    end
end

if isProc
    rng(sum(DataHash({1224,Sim.dem.operators{operator},dem.layNam},Opt)))
end

%@HO: What do we do in this paragraph? -> for every  sub we assign the
%number of antennas and set this to active.. -> Correct
IND2 = find(devic.perDemSour(layNum,:)>=0);
for n = 1 : length(perIND) %HO: we are looping for each sub belonging to this operator
    % Find index of the selected person, within populated pixels. Ranges between 1 and
    % length(row). We subtract the population that is not part of this layer.
%     IND = sum(perIND(n)-firstSub+1>B) + 1;

    % Assign the correct count of antennas based on the years of contract and lifetime.
    AeCouUser = zeros(1,length(Sim.dem.trafVol.(fam).devic.pwrAmpClas)); %   'handh'    'handh'    'handh'    'mac'    'veh'    'tr'
    if isProc
         %HO: AeCou is calculate few lines above. If this is not empty (hence there are antennas, i.e. speed of UE is >0) then get into this loop..
        if ~isempty(AeCou) % %HO: AeCou is calculate few lines above. If this is not empty (hence there are antennas, i.e. speed of UE is >0) then get into this loop..
             %@HO why when layer=4 (motorway) the antenna count is zero? ->        %because we don't have motorway in the study area..
            for aec = 1 : size(AeCou,2)
                
                
                if length(unique(percentage)) > 1
                    beep
                    disp('the subscriber base increases and the rest of this function needs amendments wrt firstYear')
                    keyboard
                    firstYear = find(n <= targPop,1);
                end
                
                
                firstYear = subsDB(perIND(n)).firsYea(IND2(aec));
                AeCouI = size(AeCou,1) - mod(totalYears + Sim.yea.thisYea - ...
                    1 - firstYear,lifet(aec));
                if AeCouI < 1
    %                 beep
    %                 disp('Assuming initial condition for UE but this may be wrong')
                    AeCouI = 1;
                end
                tmp = eval(AeCou{AeCouI,aec});
                AeCouUser(IND2(aec)) = tmp(sum(rand()>cumsum(tmp(:,2)))+1,1);  %HO: there is a randomness around the number of antennas.. Number of antenna is affected by the lifetime and by the input in LB. The code is existing for this feature, however we don't allow in the code later the antenna count to vary across years.. At first place, we have to define in the LB file the same values across all years otherwise there is an error in the code..
            end
        end
    else
        AeCouUser(IND2) = AeCouUser_;
    end

    % Update fields
%     subsDB(perIND(n)).I = row(IND);
%     subsDB(perIND(n)).J = col(IND);
    subsDB(perIND(n)).isSubs = true; %HO: the one that we found are set to active subs. %HO: we have already done this in previous function for subsDB.operator however here we are doing this for subsDB.isSubs!
    subsDB(perIND(n)).aeCou = AeCouUser; %HO: we want the antenna count per sub

%     figure
%     hist([subsDB([subsDB.aeCou]>0).aeCou],[2,4])
end
end
