function [thisDem,Sim] = dist_dem_year(dem,Sim,studyA,clu,isPlot)
%DIST_DEM_YEAR function distributes the traffic volume within a given simulation area. This
%       distribution occurs for the traffic in mobility classe, hours of the day (24 hr) traffic
%       classes, families of use cases and operators.
%
%       INPUT:
%           dem     : demand object
%           Sim     : simulation object
%           studyA  : the study area
%           clu     : the clutter data
%           isPlot  : true if plots are desired, false if not
%
%       OUTPUT:
%           thisDem : traffic volume distribution; the structure of the output variable is
%                     thisDem( pix ).( family ).( trafClass ).( pwrAmpClass ).v( hrs x mobClasses x opers )
%                   @HO: This structure is not valid anymore..

%         HO added:
%         The field of the output struct thisDem are:
%               I, J  : These are indices for demand pixel (row and column respectively)
%               IND   : Index of the pixel)
%               clu   : is the clutter for that demand pixel
%               v     :  demand in that pixel
%               satis : indicates if demand is satisfied in the pixel
%               assoc : mobility classes association
%               aeCou : Antenna counts at the UE side

%                 Notes:
%                       1) By looking at thisDem.v and Sim.demTranslati we can understand, each column of thisDem.v corresponds to which mobility class, situation, pwramp, operator, etc ..
%                       2) The size of thisDem.V/thisDem.satisf/thisDem.assoc is 24x4 in our run, i.e. hrs(24) x mobClasses(up to 6) x opers
%
%       Traffic classes indexing:
%           1: Streaming
%           2: Computing
%           3: Storage
%           4: Gaming
%           5: Communicating
%
%       Mobility classes indexing:
%           1: population            -- popu
%           2: small business (sme)  -- DELN
%           3: large business        -- DELL
%           4: stationary/pedestrian -- pedeStat
%           5: low mobility          -- lowMobi
%           6: high mobility         -- hiMobi
%
% * v0.1 AA 31May16 Created
% * v0.2 AA 4Jun16  Converted Mbit/s/pixel
% * v0.3 AA 5Jun16  Added traffic split per address within demand point
% * v0.4 AA 6Jun16  Incorporated output format for thisDem
% * v0.5 AA 15Jun16 Distributed home traffic followed Pareto distribution
% * v0.6 AA 22Jun16 Corrected I2V traffic volume distribution
% * v0.7 AA 23Jun16 Tidied up code and use 'Sim' for inputs
% * v0.8 AA 26Jun16 Reassigned logical functions to simpify main function
% * v0.9 AA 29Jun16 Added sme and large business dev cap struct arrays
% * v0.10 AA 9Jul16 Reduced FOR loops to speed up execution
% * v0.11 AA 16Jul16 Tidied up code and added description
% * v0.12 KK Aug16  Fixed bug: the population in the area should not change when it is distributed.
%                   Cleaned up the code: The demand is first distributed only to be accumulated and
%                   then redistributed; the code was changed so that there is a single pass of
%                   distribution. There was a conflict of indicing across subscribers, homes and
%                   population.
% * v0.13 AK Sep16  Added more families of use cases and operators and tidied up the code
% * v0.14 AK Oct16  Fixing rounding negative errors for demand calculations and adding an assertion
%                   test
% * v0.15 HO 14Nov18 Add more comments

fprintf(1,'Distributing the demand for %d...\n',Sim.yea.thisYea);

%% Initial sizes
sz1 = size(Sim.dem.hourDistri,1);     % hours in a day
sz2 = length(Sim.dem.mobiClasses);    % mobility classes (up to 6) (DELL, DELN, popu, low mobi, high mobi, etc..)
sz3 = length(Sim.dem.trafClas_);    % traffic classes (up to 5, in 5G NORMA we consider only 1) (Streaming, Computing, etc..)
sz4 = length(Sim.dem.famUseCase_ );   % families of use cases (e.g. MBB, etc..)
sz5 = length(Sim.dem.operators);      % operators (EE, etc..)
sz6 = length(Sim.dem.demSources);     % demand sources [layers] (up to 9) popu, DELN, Motorway, A road, B road, etc..

%% Find indices of demand points
IN = dem(2).A > 0 |... % -> population is distributed at residential addresses, so no need to consider the pop.
    dem(3).A > 0 |...
    dem(4).A > 0 |...
    dem(5).A > 0 |...
    dem(6).A > 0 |...
    dem(7).A > 0 |...
    dem(8).A > 0 |...
    dem(9).A > 0 |...
    dem(10).A > 0;
famUseCase_ = cellfun(@(x) Sim.dem.trafVol.(x).mean.inPoly,...
    Sim.dem.famUseCase_,'UniformOutput',false);
assert(all(ismember(famUseCase_,...
    {'INbuf','INhpa','INagv','INcruShipTer','INcctv','INar'})),...
    'Insert the missing one')
% if any(strcmp('INagv',famUseCase_))
%     % Within AGV polygon
%     for n = 1 : length(studyA.AGV)
%         IN = IN | studyA.(['INagv' num2str(n)]);
%     end
% end
% if any(strcmp('INcruShipTer',famUseCase_))
%     % Within cruShipTer polygon
%     for n = 1 : length(studyA.cruShipTer)
%         IN = IN | studyA.(['INcruShipTer' num2str(n)]);
%     end
% end
% if any(strcmp('INcctv',famUseCase_))
%     % Within CCTV polygon
%     IN = IN | studyA.INhpa;
% end
% if any(strcmp('INar',famUseCase_))
%     % Within CCTV polygon
%     IN = IN | studyA.INhpa;
% end
[I,J] = find(IN);
% % clear IN n famUseCase_

INDmaster = sub2ind(size(studyA.INfoc),I,J);  %HO: translate I and J indexing into single indexing in our study area. INDmaster should indicate which pixels (by index) have demand in the study area

%% Find clutter type at each pixel
if isempty(clu)
    tmp = NaN;
else
    [X,Y] = studyA.R.intrinsicToWorld(J,I); %HO: demand pixels X,Y coordinates
    [cluI,cluJ] = clu.R.worldToDiscrete(X,Y);
    tmp = clu.A(sub2ind(size(clu.A),cluI,cluJ)); %HO: tmp should have the clutter at each demand pixel
    clear cluI cluJ
end

%% Prepare output value in Mbit/s, 
N = 0;
Sim.demTranslati = cell(0,8);
for fn = 1 : sz4  % For each family , e.g. MBB
    fam = Sim.dem.famUseCase_{fn};
    CEtputI = find(Sim.dem.trafVol.(fam).CEtput.yea==Sim.yea.thisYea) + ...
        (0:length(Sim.dem.trafClas_)-1);
    t_ = find(~all(isnan(Sim.dem.trafVol.(fam).CEtput.DL(:,CEtputI)),1)); % t_=1,2,3,4,5. Sim.dem.trafVol.(fam).CEtput.DL is given in input file which is for example the Tput in DL of MBB. Here we are finding which traffic classes to consider (streaming, computing, etc..)
    for tn = 1 : length(t_)  % For each traffic class (5 traffic classes),  e.g. streaming, computing, etc..
        trafCl = Sim.dem.trafClas_{t_(tn)}; % HO: Here is the traffic class
        for n = 1 : length(Sim.linBud.UE.pwrAmpClas_) %HO: for each power amplifier class
            pwrAmpClass = Sim.linBud.UE.pwrAmpClas_{n}; % HO: Here is the power amp class
            situ = Sim.linBud.UE.situ_{n}; %HO: corresp sit f the power amplifier class
            for op = 1 : sz5  % For each operator
                oper = Sim.dem.operators{op};   %HO: here is the operator
                for m = 1 : length(Sim.dem.mobiClasses)  %HO: up to 6 mob classes
                    N = N + 1; % HO: this probably should be the number all the cases considered in this simulation, e.g. 180. All possible computation
                    Sim.demTranslati(N,:) = {fam,trafCl,pwrAmpClass,situ,oper,...
                        Sim.dem.mobiClasses{m},op,0}; %HO: this struct should include all possible cases that we have..
                end
            end
        end
    end
end
clear fn fam tn trafCl n m t_

% Initialise structure for output
thisDem = struct('I',num2cell(I),... %HO: I calculated above, index (row) of demand pix
    'J',num2cell(J),... %HO: J calculated above, index (col) of demand pix
    'IND',num2cell(INDmaster),... %HO: IND calculated above, index (absolute) of demand pix
    'clu',num2cell(tmp),... %HO: tmp calculated above, clu of demand pix
    'v',sparse(sz1,N),... %HO: sz1, N calculated above, sz1 is the time and N is the total number of combinations that we have..
    'satis',true(sz1,N),... %HO: sz1, N calculated above, sz1 is the time and N is the total number of combinations that we have..
    'assoc',[],... %HO: defined below
    'aeCou',zeros(1,N),...
    'SINR_DL',nan(sz1,N),...
    'band',nan(sz1,N),...
    'tputOveMbps',nan(sz1,N));
[thisDem.assoc] = deal(cell(sz1,N));

%% Find demand per hour per demand source per use case family
for uc = 1 : sz4  % For each family, e.g. MBB
    fam = Sim.dem.famUseCase_{uc}; %HO: this is the family of use case, MBB

    % Mobility classes to Thoroughfare associations
    assoc = Sim.dem.mobiToDemSour.(fam).assoc; %HO: each cell is a pwramp type (handh, mac, veh,etc..). For each cell this is the Association table between demand sources (layer) and mobility classes, this is provided in the input file: MBB mobiCla v0_1.csv

    % Calculate the number of people, home addresses, business addresses, and
    % number of devices on thoroughfare (HO: KK: the absolute number here does not really matter. What it matters is the ratio. These
    % values in pop (mainly for roads) does not have any meaning.. 
%     pop = arrayfun(@(x) full(sum(x.A(:))),dem);
    pop = arrayfun(@(x) full(x.A(:).'*x.w(:)),dem); %HO:  we are calculating the number of pop per source of demand (popu, DELN, etc..),
    % NOTE: SHOULD pop2 INCLUDE pop(1) OR pop(2)??
    
     %HO: Verification on the association read from inputs files
    tmp = sum(cell2mat(cellfun(@(x) x,assoc,'UniformOutput',false)),1); %HO: tmp is the sum of each mobility class across all demand sources. The sum should be either 1 or 0.
    %HO: this if below is a kind of verification.. @HO: can we replace itby the following assertion: assert(all(tmp-1<1e-3 | tmp<1e-3))
    if any(~(abs(tmp-1)<1e-3|abs(tmp)<1e-3))
        error(['File ''' Sim.dem.filenames.(fam).mobiCla ...
            ''' has an error. All entries should sum vertically to either 0 or 1.'])
    end
    carRa = Sim.dem.mobiToDemSour.carRatio; %HO: Getting carRa from input files..
    %HO: pop2 should provide the number of pop (popu, cars, business addresses,etc..), per mobility class. However, pop is the same for demand source -> KK: Could be, just the ratio is used here..)
    pop2 = [pop(2:4).'; sum(cell2mat(cellfun(@(x) x(4:6,4:9) * ...
        (pop(5:10).*carRa(4:9)).',assoc,'UniformOutput',false).'),2)].'; %@HO: What is pop2? Why are we calculating this? we need to talk to KK here .. What is the reason pop2 is higher than pop1? probably because we are multiplying by the car Ratio, etc.. -> KK: There is no need to normalise this.. the qty is irrelevant here. 
    %HO: the unit in dem for roads/tunnels etc.. is in m.
    
    %% Split by UE power amplifier class and situation
    % Calculate the split of the traffic volume of each mobility class into
    % UE power amplifier class and situation
    mobiliCla2uePwrAmpClaSitu = zeros(sz2,length(assoc),length(pop)-1);
    for n = 1 : sz2
%         tmp = cell2mat(cellfun(@(x) x(n,:),assoc,...
%             'UniformOutput',false)) * pop(2:end).';
        tmp = cell2mat(cellfun(@(x) x(n,:),assoc,...
            'UniformOutput',false)) .* repmat(pop(2:end),length(assoc),1);

        mobiliCla2uePwrAmpClaSitu(n,:,:) = tmp ./ sum(tmp(:));
    end
    
    %% Determine number of 5G subscribers 
    % Subscribers in home and business pixels for each operator and use case.
    fprintf(1,'Finding subscribers for %s...',fam);
    subsDB = select_subs(dem,Sim,fam,uc,studyA); %HO: Once select_subs is caled we have in return subsDB that includes information per sub. It indicates if the sub belongs to a specific operator, number of antennas at UE, firstYear? per sub, row and col indices of this sub, etc..
    fprintf(1,'DONE\n');
    
    % HO: Summary:
    % Here we have: pixels -> 319x475, I,J (demand pixels) -> 38120, pop
    % (population per layer) -> 1615148 [we have different things here,
    % people, cars, residentials,etc..], pop2 (population per mobility class) -> 2.3716e+06, subsDB -> 617015 (by considering subs penetration here, 17%)
    %@HO: Why subsDB is not the same as pop ? -> There is subs penetration. Why do we need to calculate pop2? And why it has more pops than total pop itslef? probabaly because we are multiplying by the Car Ratio
    
   % @HO: TODO: why do we have 6 mobility classes (first three are not moving!!)
    
    %% Demand distributing (@HO: shall we make this short paragraph, i.e. at least next 34 rows, into a new function??)
    fprintf(1,'Distributing demand...');
    
    TrafVol = Sim.dem.trafVol.(fam).mean; %HO: Getting all information about traffic volume (Mean) from input file, e.g. MBB mean from "MBB mean v0_1.csv"
    Ratio = Sim.dem.trafVol.(fam).ratio; %HO: Getting information which is provided in input csv file: e.g. for MBB from "MBB rati per hour v0_2.csv"
    HourDistri = Sim.dem.trafVol.(fam).hourDistri; %@HO: This is from input jpeg (ITU figure). MBB busyHour2tweFou v0_1.jpg in demand module folder and this is defined in Config file.  sum(HourDistri')= [1 0 0 0 0]. We are only considering streaming here.. -> this is saved in mat file
    distri = Sim.dem.trafVol.(fam).distri;  %HO: Getting information about distribution in terms of % of addresses and % of data (Pareto distribution) from the input CSV file, e.g. MBB from "MBB distri v0_1.csv"     
  
%     beep
%     disp('Hacking HourDistri!!!')
% %     HourDistri(1,:) = 1 ./ 24;
%     HourDistri(1,:) = (1-1.88./24) ./ 23;
%     HourDistri(1,18) = 1.88 ./ 24;
% %     23.*x+y==1
% %     y./(23.*x+y).*24==1.88
% %     x==(1-1.88./24)./23
% %     y==1.88./24
% %     sum(HourDistri,2)
% %     HourDistri(1,22)./mean(HourDistri(1,:))

    % Find the traffic in GB/month/km2 for this year (HO: in the following lines of code we mainly translate the demand for the current year from GB/month/km2 for the UK into DL Mbps in the study area across 24h and for each traffic class (in 5G NORMA only Streaming was assumed) (@HO: to check later these equations carefully..
    curYear = TrafVol.year == Sim.yea.thisYea; % HO: This is not the Traffic, this is just indicating the year..
    TraffGBmthkm2 = TrafVol.GbPerMonthPerSqKm(curYear);  %HO: Traffic in GB/month/km2 (provided by input file)

    if TraffGBmthkm2 > 0
        % Obtain traffic demand headlines for every use case
        TraffMBdaykm2 = 1000 * TraffGBmthkm2 ./ (365.25./12);  % Break headline from month to days, MB/day/km2 (1 month = 365.25./12 days)
        TraffMbphrkm = TraffMBdaykm2 .* HourDistri .* 8;  % Convert daily value into 24 values and service classes, Mbit/hr/km2 (1 byte = 8 bits)
        TraffMbpskm = TraffMbphrkm ./ 3600;  % Convert to Mbit/s/km2 (1 hr = 3600 secs)
        
        % Convert to Mbit/s in study area
        TraffMbpsstudyA = TraffMbpskm .* studyA.SHAPE_Area ./ 1e6;
%         switch Sim.SA.name
%           case {'West Bank','Gaza'}
%             % Demand is generated in focus and buffer
%             TraffMbpsstudyA = TraffMbpskm .* studyA.buf.SHAPE_Area ./ 1e6;
%           case 'Settlements'
%             % Demand is generated only in focus
%             TraffMbpsstudyA = TraffMbpskm .* studyA.SHAPE_Area ./ 1e6;
%           otherwise
%             error('Undefined')
%         end
                
        % There is no uplift factor for a nation, thus for UNOPS project we
        % set tmp to 1.
%         if pop(1) > 0
%             % If population resides in the area, then calculate the uplift
%             % factor due to the area being more densly populated than the
%             % country mean
%             tmp = pop(1) ./ studyA.buf.SHAPE_Area .* 1e6 ./ ... %@HO: Why are we calculating dem in study area by using both area and pop?? we just need one of them (pop) to scale it to the study area? It looks that we are amplifying demand again by multiploying by tot_area./stud_area %HO: (tmp) is 37.34=(pop_stud*area_tot)./(area_stud*pop_tot); %HO: pop(1) should be the population for the first layer (i.e. popu). -> KK: this is how much the study area is more dense to compared to the UK. We wanted more demand in the study area while implementing this..
%                 Sim.dem.trafVol.(fam).mean.popuThatTheMeaAbovCorre .* ... %HO: Sim.dem.trafVol.(fam).mean.popuThatTheMeaAbovCorre should be the total population in the UK that the mean demand corresponds to.. This is from input csv file: "MBB mean v0_1.csv"
%                 Sim.dem.trafVol.(fam).mean.areThatTheMeaAbovCorre; % HO: Sim.dem.trafVol.(fam).mean.areThatTheMeaAbovCorre is the area of the UK. This is from input csv file: "MBB mean v0_1.csv"
%         else
%             % If population does not reside in the area, then we cannot
%             % calculate an uplift factor
%             tmp = 1;
%         end
        tmp = 1;
        
        TraffMbpsstudyA = TraffMbpsstudyA .* tmp .* ...  %HO: traf*tmp*rat_resid=traf*37.34*3.78;
            Sim.dem.trafVol.(fam...
           ).mean.ratOfWorkDayPopuOveResidentiPopuInStudyAre; %HO: Sim.dem.trafVol.(fam).mean.ratOfWorkDayPopuOveResidentiPopuInStudyAre is Ratio of work day population to residential population in the study area. This is from input csv file: "MBB mean v0_1.csv"
        TraffMbpsstudyAdownlin = TraffMbpsstudyA .* ... %HO: traff_DL = traf*rat_DL_UL./(rat_DL_UL+1). This traffic is calculated per hour and across traffic classes (5x24)
            Sim.dem.trafVol.(fam).mean.downlinOveUplinRat ./ ... %HO: Sim.dem.trafVol.(fam).mean.downlinOveUplinRa is the DL/UL ratio. This is from input csv file: "MBB mean v0_1.csv"
            (Sim.dem.trafVol.(fam).mean.downlinOveUplinRat + 1); %HO: for EE run CRAN for 2017:  sum(sum(TraffMbpsstudyAdownlin)) =4.7032e+05. @HO: However by doing the following once dist_dem_year has finished the run: sum(sum(full([thisDem.v])))=  2.9715e+04; Why is that?? Why there is a factor of 15.8278???

         %HO: KK because this demand is distributed across indoor/outdoor and
    %all mobility classes however in thisDem we are only considering
    %outdoor? In addition, we are taking only the operator share (17%) of TraffMbpsstudyAdownlin
    
    
    %@HO: Question to KK:
    %   1) What are the references for the association table? KK: no
    %   specific reference. However this might not affect the end results..
    %   Note that low and high mob users should not be served by smallcells
    %   (this depends on the frequency, this is given in doppler input.
    %   2) What are the references for the car ratio? KK: no reference.
    %   3) What is the reference for the ratios? e.g. ratSmeToHome,
    %   ratLargeToSme, etc.. -> KK: no specific reference
    %   3) What about the ouputs from select_subs? What is v (, what about the unit here -> Mbps? Why it has in
    %       some case 4 dimensions? Are these dimensions per mobility class?
    %       Why antenna coun are zero for layers 1-5?
    %   4) Look at slide 21 from the presentation and try to make sense of it..
    %   5) Any reference for the Pareto distribution/layer? -> KK: popu
    %   Pareto distribution is from previous Ofcom project. However for
    %   other layers there is unifrom distributions.. There was no
    %   challenge against that..
    %   6) How can we verify that TraffMbpsstudyAdownlin is equal to
    %   thisDem for the study area? they should be equal? -> See response
    %   above. KK has done many verifications, @KK look at some code verification..
        
    
     %HO: Initialise demPerMobiliCla, @HO: what is the title of this
    %paragraph?. Before we had the demand per layer (dem), here we start
    %working on the demPerMobiliCla
        demPerMobiliCla = struct('A',zeros(sz1,sz3,length(assoc),length(pop)-1),... %HO: sz1 24h, sz3 5 traffic classes, length(assoc) 6 pwramp classes, [length(pop)-1] 9 demand sources (layers)
            'layNam',Sim.dem.mobiClasses); %HO:-> initialisation of the vector dem per mobility class..  ,  size([demPerMobiliCla.A]): [24    30     6     9 ] 
        for hn = 1 : sz1  % For each modelled hour (24hrs)
            % The hourly ratio of traffic volume for the specific hour (KK: AA works should start from here..)
            ratSmeToHome = Ratio.smeToHome(:,hn); %HO: Ratio is mainly from the input csv file: "MBB rati per hour v0_2.csv". This is per traffic class (streaming, computing, etc..)
            ratLargeToSme = Ratio.largeToSme(:,hn);  %HO: Ratio is mainly from the input csv file: "MBB rati per hour v0_2.csv". This is per traffic class (streaming, computing, etc..)
            ratPedeToLowMobi = Ratio.pedeToLowMobi(:,hn); %HO: Ratio is mainly from the input csv file: "MBB rati per hour v0_2.csv". This is per traffic class (streaming, computing, etc..)
            ratLowMobiToHiMobi = Ratio.lowMobiToHiMobi(:,hn); %HO: Ratio is mainly from the input csv file: "MBB rati per hour v0_2.csv". This is per traffic class (streaming, computing, etc..)
            ratInToOut = Ratio.inToOutdoor(:,hn);  %HO: Ratio is mainly from the input csv file: "MBB rati per hour v0_2.csv". This is per traffic class (streaming, computing, etc..)

            
            %         pop2(1)  v_h + pop2(2)  v_s + pop2(3)  v_l + pop2(4)  v_ped + pop2(5)  v_lo + pop2(6)  v_hi = b(1) -> From KK Skype
%         a(1,1)*x(1) + a(1,2)*x(2) = b(1)
%         a(2,1)*x(1) + a(2,2)*x(2) = b(2)
% 
%         a(1,1) : number of homes
%         a(1,2) : number of business
%         b(1) : total traffic volume Mbit/s
%         
%         Nh*x(1) + Nb*x(2) = b(1)
%         ratio = x(2)/x(1)
%         
%         x(1) : volume per home Mbit/s/home
%         x(2) : volume per business Mbit/s/business
            
            for tc = 1 : sz3    % For each traffic class (5 traffic classes)
                if all(pop2>0)  %HO: pop2 is the pop per mobility Class
                    A = [pop2
                        -ratSmeToHome(tc) 1 0 0 0 0 %HO: -desired_ratio * v_h + v_s = 0, v_s: traffic volume of SME, v_h: traffic volume of home, desired_ratio=ratSmeToHome. v_h and v_s are unknown
                        0 -ratLargeToSme(tc) 1 0 0 0
                        0 0 0 1 -ratPedeToLowMobi(tc) 0
                        0 0 0 0 1 -ratLowMobiToHiMobi(tc)
                        pop2(1:3) -pop2(4:6).*ratInToOut(tc)];  %?HO: why are adding other rows to the matrix A?
                    b = [TraffMbpsstudyAdownlin(tc,hn); zeros(5,1)]; % HO: traff in Mbps for study area DL (calculated above!)
                    x = A \ b; %@HO: What does "x" represent? (Matrix left division), Solve systems of linear equations Ax = B. x has length of 6 (6 mobility classes), is this the demand per mobility class? is it correct to use such a formula? Ax=b, what is the reference for it?
                elseif all(pop2(1:3)>0) && all(pop2(4:6)==0) %@HO: when do we come here? does this depend on the service, MBB, VTC? we come here if there is demand in stationary mobility and there is no vehicular demand (ped, low mob, high mob)
                    A = [pop2(1:3)
                        -ratSmeToHome(tc) 1 0
                        0 -ratLargeToSme(tc) 1];
                    b = [TraffMbpsstudyAdownlin(tc,hn); zeros(2,1)];
                    x = [A\b; zeros(3,1)];
                elseif all(pop2(1:3)==0) && all(pop2(4:6)>0)
                    A = [pop2(4:6)
                        1 -ratPedeToLowMobi(tc) 0
                        0 1 -ratLowMobiToHiMobi(tc)];
                    b = [TraffMbpsstudyAdownlin(tc,hn); zeros(2,1)];
                    x = [zeros(3,1); A\b];
                elseif all(pop2([1:3,5:6])==0) && pop2(4)>0
                    x = [zeros(3,1)
                        TraffMbpsstudyAdownlin(tc,hn) ./ pop2(4)
                        zeros(2,1)];
                elseif all(pop2(5:6)==0) && all(pop2(1:4)>0)
                    A = [pop2(1:4)
                        -ratSmeToHome(tc) 1 0 0
                        0 -ratLargeToSme(tc) 1 0
                        pop2(1:3) -pop2(4).*ratInToOut(tc)];
                    b = [TraffMbpsstudyAdownlin(tc,hn); zeros(3,1)];
                    x = [A\b;zeros(2,1)];
                elseif all(pop2(1:5)>0) && all(pop2(6)==0)
                    A = [pop2(1:5)
                        -ratSmeToHome(tc) 1 0 0 0
                        0 -ratLargeToSme(tc) 1 0 0
                        0 0 0 1 -ratPedeToLowMobi(tc)
                        pop2(1:3) -pop2(4:5).*ratInToOut(tc)];
                    b = [TraffMbpsstudyAdownlin(tc,hn); zeros(4,1)];
                    x = [A\b;0];
                elseif all(pop2([1,2,4,5])>0) && all(pop2([3,6])==0)
                    A = [pop2([1,2,4,5])
                        -ratSmeToHome(tc) 1 0 0
                        0 0 1 -ratPedeToLowMobi(tc)
                        pop2(1:2) -pop2(4:5).*ratInToOut(tc)];
                    b = [TraffMbpsstudyAdownlin(tc,hn); zeros(3,1)];
                    x = [A\b;0];
                    x = [x(1:2);0;x(3:5)];
                else
                    error('Check here')
                end

                % Correcting the rounding errors, and set these values to
                % zero.
                % Introduced tolerance of XX so that, for example an indoor
                % to outdoor ratio of 100,000 would correspond to zero
                % outdoor.
%                 x(abs(x) < eps) = 0;
                x(x.*pop2.'./(pop2*x)<1e-5) = 0;

                % If there are demands with negative values, then this is an error.
                assert(all(x>=0),'There are negative demand values for %s use case family.',fam);

%                 assert(nnz(x>0)==1,['Reporting throughput levels '...
%                     'thisDem.tputOveMbps and .band '])
                
                 %HO: What this paragraph is doing? ->HO: I guess this is
            %distributing the demand per mobility class by using pop2, x (found by solving linear equation) and mobiliCla2uePwrAmpClaSitu
                for n = 1 : length(demPerMobiliCla)  % Per mobility class
                    for m = 1 : length(assoc)  % Per UE type
                        for k = 1 : size(mobiliCla2uePwrAmpClaSitu,3)  % Per source of demand
                            demPerMobiliCla(n).A(hn,tc,m,k) = x(n) .* pop2(n) .* ... %HO: demPerMobiliCla is initialised few lines above
                                mobiliCla2uePwrAmpClaSitu(n,m,k);  %@HO: not sure what we are calculating here? sum(sum(sum(sum(demPerMobiliCla(1).A))))=2.9136e+05, this is the demand for the popu; sum(sum(sum(sum([demPerMobiliCla.A])))) =   4.7032e+05 which is equal to TraffMbpsstudyAdownlin
                        end
                    end
                end
            end
        end

        % Assign the desired distribution (@HO: Pareto distribution here from
    % input file? -> KK: not all of them are Pareto, there are uniform
    % distribution..
        distribution = cell(length(Sim.dem.demSources),1); %HO: Initialise the distribution vector

        % The distribution for each demand source (HO: we are getting the input information about the Pareto distribution per year and per layer (demand source)
        m = 0;
        distriYear = distri.year == Sim.yea.thisYea; %HO: this year is 1, @HO: we have already calculated this several times, why we don't just put this at the top of the file..
        for demSour = Sim.dem.demSources' %HO: per demand source (i.e. layer)
            m = m + 1;
            distribution{m} = [Sim.dem.trafVol.(fam).distri.(demSour{:}).perOfAdd ... %HO: "Sim.dem.trafVol.(fam).distri.(demSour{:}).perOfAdd" is from "MBB distri v0_1.csv", Pareto distribution for each layer
                Sim.dem.trafVol.(fam).distri.(demSour{:}).perOfDat(:,distriYear)];  %HO:  Look at distribution csv file (Pareto distribution)
        end

        % The users that are actually subscribers (@HO: what do we do to the sub here? Just filling percTrafVol field?
        subsDB = distri_trafVol_across_subs(subsDB,distribution,Sim,fam,...
            studyA); % we are mainly distributing the demand per subs for all layers. @HO: HO to come back to this function..
         %HO verification: sum([subsDB([subsDB.layI]==1).percTrafVol])=1,sum([subsDB([subsDB.layI]==2).percTrafVol])=1, etc ..
       
        fprintf(1,'DONE\n');

        %% Demand accummulation
        fprintf(1,'Accummulating the demand into pixels...');
        for op = 1 : sz5 %HO: per operator
            operator = Sim.dem.operators{op}; %HO: operator name

            % The data traffic percentage for this operator and year
            operSubPenet = Sim.dem.trafVol.(fam).subPenet.(operator);  %HO: from input file, e.g. MBB from "MBB subPenet v0_1.csv"

            % Accumulate traffic volume in pixels
            opSubs = [subsDB.isSubs] & arrayfun(@(x) x.operator(op),subsDB).';  %HO: it looks that we are selecting the subs belonging to this operator for all layers! @HO: opSubs is equal to sum([subsDB.isSubs]) which is equal to sum([subsDB.operator]). In which case they should be different here? -> KK: there might be different in some cases.. for example when there are two operators ..
            if any(opSubs) %HO: if we have any sub belonging to this op (we should for sure have) 
                trafPerYear = operSubPenet.year == Sim.yea.thisYea; %HO: current year indice. @HO: we have calculated this several times before..
                trafPer = operSubPenet.trafPerc(:,trafPerYear);%HO: traffic percentage across mobility classes for this year..
                [thisDem,Sim] = accu_into_pix(thisDem,Sim,studyA,... %HO: INDmaster: indicates which pixels in the study area have demand. thisDem initialised earlier, subsDB(opSubs) subs belong to this operator,  
                    INDmaster,subsDB(opSubs),demPerMobiliCla,trafPer,... 
                    assoc,sz6,Sim.dem.famUseCase_{uc},operator); %HO: this accumlates the demand per pixel probably?. @HO: we also need a flowchart for this function
                %HO: In this function also we are populating thisDem.v, thisDem.satis and thisDem.aeCoun
            end
        end

        fprintf(1,'DONE\n');
    end
end
%HO Verifications: sum(sum(full([thisDem.v])))./sum(sum(sum(sum(sum([demPerMobiliCla.A]))))) =  0.1700


% clear fam assoc pop pop2 subsDB TrafVol Ratio HourDistri distri trafPer operator
% clear TraffMbpsstudyA demPerMobiliCla distribution demSour op opSubs operSubPenet trafPerYear
% clear hn ratSmeToHome ratLargeToSme ratPedeToLowMobi ratLowMobiToHiMobi ratInToOut tc Ab x m n
% clear carRa tmp
% clear TraffMBdaykm2 TraffMbphrkm curYear TraffMbpskm

% Remove zero entries, thisDem objects with .v that are all zero @HO: why we don't include this in another function?
IND2 = arrayfun(@(x) full(~any(x.v(:))),thisDem); %it looks like thisDem.v is qty of demand, hence if there is no demand then remove those entries (zero entries..). IND2 has dimension of 180 (includes all cases, see N at the top of this file) and where it is logical value of zero then there is no demand for that case
thisDem(IND2) = [];

% Remove zero entries, columns in .v that are all zero
IND2 = sum(cell2mat(arrayfun(@(x) full(sum(x.v,1)),thisDem,...
    'UniformOutput',false)),1) == 0;
for pix = 1 : length(thisDem)
    thisDem(pix).v(:,IND2) = []; %HO: there is no demand then remove those entries (zero entries..). For the first round of results there are only 7 cases out from 18. See Sim.demTranslati for the list of remaining cases.. 
    thisDem(pix).satis(:,IND2) = []; %HO: there is no demand then remove those entries (zero entries..). For the first round of results there are only 7 cases out from 180. See Sim.demTranslati for the list of remaining cases.. 
    thisDem(pix).assoc(:,IND2) = []; %HO: there is no demand then remove those entries (zero entries..). For the first round of results there are only 7 cases out from 180. See Sim.demTranslati for the list of remaining cases.. 
    thisDem(pix).aeCou(IND2) = []; %HO: there is no demand then remove those entries (zero entries..). For the first round of results there are only 7 cases out from 180. See Sim.demTranslati for the list of remaining cases.. 
    thisDem(pix).SINR_DL(:,IND2) = [];
    thisDem(pix).band(:,IND2) = [];
    thisDem(pix).tputOveMbps(:,IND2) = [];
end
Sim.demTranslati(IND2,:) = [];%HO: this struct has all the remaining cases.. 
%HO: KK: later we remove demand which have no devices.. this means we lose
%some of the demand calculated from accu_into_pix (thisDem)


% fprintf(1,'Removing sources of demand that lack devices..');
for ucf = 1 : sz4     % families of uses cases
    % Current use case family
    fam = Sim.dem.famUseCase_{ucf}; % HO: e.g. MBB
    assoc = Sim.dem.mobiToDemSour.(fam).assoc; %HO: list of association, 6 cell(6x9each)
    
    % The device (power amplifier class) information for that use case family
%     pwrAmpClas_      = Sim.dem.trafVol.(fam).devic.pwrAmpClas;
    devicePerDemSour = Sim.dem.trafVol.(fam).devic.perDemSour; %HO: this looks like the speed of UE from input file: "MBB devicPerDemSour.csv"
    
    CEtputI = find(Sim.dem.trafVol.(fam).CEtput.yea==Sim.yea.thisYea) + ...
        (0:length(Sim.dem.trafClas_)-1);
    
    % traffic classes
    t_ = find(~all(isnan(Sim.dem.trafVol.(fam).CEtput.DL(:,CEtputI)),1)); %HO: this is also calculated in the first paragraph above. t_=1,2,3,4,5. Sim.dem.trafVol.(fam).CEtput.DL is given in input file which is for example the Tput in DL of MBB. Here we are finding which traffic classes to consider (streaming, computing, etc..)
    for tn = 1 : length(t_)    % For each traffic class (5 traffic classes)
        % The current mobility class id
        trafCl = Sim.dem.trafClas_{t_(tn)};  %HO: e.g. Streaming, etc..
        
        % devices
        for pwrac = 1 : length(Sim.linBud.UE.pwrAmpClas_) %HO: 'handh'    'handh'    'handh'    'mac'    'veh'    'tr'
            % The current power amplifier class/device
            pwrAmpClass = Sim.linBud.UE.pwrAmpClas_{pwrac}; %HO: e.g. handl, mac, etc..
            situ = Sim.linBud.UE.situ_{pwrac}; %HO: outd, indo, etc..
            
            % If mobiCla assoc is nonzero then the demand is distributed to
            % demand points. However, if devices are not provided in
            % devicPerDemSour, then this demand has to be removed, because
            % it is assumed that it is served by non-modelled cells. For
            % example indoor handheld traffic is assumed to be served by
            % indoor solutions which are not explicitely modelled. The
            % velocity of the demand is not critical here, e.g. we do not
            % need to use bsxfun(@ge,velPerDemSour.',Sim.dem.mobiClaVel),
            % because the velocity has already been taken into account in
            % the demand distribution.
            velPerDemSour = devicePerDemSour(:,pwrac); %HO: velocity of this UE across all demand sources (layers)
%             isDemSour = any(assoc{pwrac}(:,velPerDemSour>=0)>0,2);
%             assoc{pwrac}>0 & repmat(velPerDemSour.'<0,6,1)
            isRemov = find(any(assoc{pwrac}(:,velPerDemSour<0)>0,2)); %HO: for the first round this is emtpy. Here, we find negative velocity and then if this velocity has a positive value in the association table then save the indices in the isRemov variable
            if ~isempty(isRemov)
                for on = 1 : sz5
                    op = Sim.dem.operators{on};
                    for mn = 1 : length(isRemov)
                        disp(['Removing: ' fam ' ' trafCl ' ' ...
                            pwrAmpClass ' ' situ ' ' ...
                            Sim.dem.mobiClasses{isRemov(mn)}])
                        IND2 = strcmp(Sim.demTranslati(:,1),fam) & ...
                            strcmp(Sim.demTranslati(:,2),trafCl) & ...
                            strcmp(Sim.demTranslati(:,3),pwrAmpClass) & ...
                            strcmp(Sim.demTranslati(:,4),situ) & ...
                            strcmp(Sim.demTranslati(:,5),op) & ...
                            strcmp(Sim.demTranslati(:,6),Sim.dem.mobiClasses{isRemov(mn)});
                        for pix = 1 : length(thisDem) %HO: removing this layer(source of demand) and device combinations from thisDem struct
                            thisDem(pix).v(:,IND2) = [];
                            thisDem(pix).satis(:,IND2) = [];
                            thisDem(pix).assoc(:,IND2) = [];
                            thisDem(pix).aeCou(IND2) = [];
                            thisDem(pix).SINR_DL(:,IND2) = [];
                            thisDem(pix).band(:,IND2) = [];
                            thisDem(pix).tputOveMbps(:,IND2) = [];
                        end
                        Sim.demTranslati(IND2,:) = []; %HO: removing this layer(source of demand) and device combinations from Sim.demTranslati
                    end
                end
            else
%                 switch fam
%                     case 'MBB'
%                         if ~strcmp(pwrAmpClass,'mac') && strcmp(situ,'indo')
%                             sdsd
%                         end
%                     case 'MTC'
%                         if (~strcmp(pwrAmpClass,'handh') && strcmp(situ,'indo')) && ....
%                                 (~strcmp(pwrAmpClass,'mac') && strcmp(situ,'indo'))
%                             sdsd
%                         end
%                     otherwise
%                         dfdsf
%                 end
            end
        end
    end
end
clear t_ tn assoc
% fprintf(1,'DONE\n');

% Set the association value to 0, i.e. to no cell
% This is done so as to be able to quicky to convert from cell to number
% without interrogating if the entry is empty. There is no additional
% memory burden, because the assoc value would be filled-in anyway.
for n = 1 : length(thisDem)
    thisDem(n).assoc(:) = {0};  %HO: filling assoc field with "zero" and in form of cells
end

% HO: This should be the end of the distribution of demand file if we do not include plotting ..
% (@HO: why we don't include the plotting in another function)

%% HO: KK has done this tesing, which can be reused..
% test1 = zeros(length(Sim.linBud.UE.pwrAmpClas_),1);
% for pwrac = 1 : length(Sim.linBud.UE.pwrAmpClas_)
%     % The current power amplifier class/device
%     pwrAmpClass = Sim.linBud.UE.pwrAmpClas_{ pwrac };
%     situ = Sim.linBud.UE.situ_{pwrac};
% 
% %         thisDem(1305).MBB.(curTrafClass).(pwrAmpClass).(situ).Vodafone.v
%     test1(pwrac) = sum(arrayfun(@(x) sum(full(...
%         x.MBB.Streaming.(pwrAmpClass).(situ).Vodafone.v(...
%         :))),thisDem(studyA.INfoc([thisDem.IND])))) .* 3600;
% end
% clear pwrac pwrAmpClass situ
% 
% sum(test1) ./ 8 ./ studyA.SHAPE_Area .* 5 ./ 0.18  % MB/day/m2 outdoor+indoor
% % 5 for indoor : outdoor = 80 : 20
% % 0.18 because Vodafone has 18% of the marketshare

%% Reduce resolution

% figure
% plot([thisDem.J],[thisDem.I],'.')
% axis equal

% fprintf(1,'Reducing resolution..');
% thisDem_I = [thisDem(:).I].';
% thisDem_J = [thisDem(:).J].';
% IND4remo = false(size(thisDem_I));
% for row = 2 : 3 : size(studyA.INfoc,1)
%     for col = 2 : 3 : size(studyA.INfoc,2)
%         ce = thisDem_I==row & thisDem_J==col;
%         ot = any(repmat(thisDem_I,1,3) == ...
%             repmat(row-1:row+1,length(thisDem_I),1) & ...
%             repmat(thisDem_J,1,3) == ...
%             repmat(col-1:col+1,length(thisDem_J),1),2) & ~ce;
% 
%         if ~any(ce) && any(ot)
%             % Choose another centre
%             if nnz(ot) > 1
%                 otf = find(ot);
%                 ce(otf(1)) = true;
%                 ot(otf(1)) = false;
%                 
%                 proc = true;
%             else
%                 proc = false;
%             end
%         elseif ~any(ce) && ~any(ot)
%             % There are no demand points, do nothing
%             proc = false;
%         elseif any(ce) && ~any(ot)
%             % There is only centre, do nothing
%             proc = false;
%         elseif any(ce) && any(ot)
%             % Transfer demand into centre and delete other
%             proc = true;
%         end
%         
%         if proc
%             for ucf = 1 : sz4     % families of uses cases
%                 % Current use case family
%                 fam = Sim.dem.famUseCase_{ucf};
%                 
%                 % traffic classes
%                 t_ = find(~all(isnan(Sim.dem.trafVol.(fam).CEtput.DL),1));
%                 for tn = 1 : length(t_)    % For each traffic class (5 traffic classes)
%                     % The current mobility class id
%                     curTrafClass = Sim.dem.trafClas_{t_(tn)};
%                     
%                     % devices
%                     for pwrac = 1 : length(Sim.linBud.UE.pwrAmpClas_)
%                         % The current power amplifier class/device
%                         pwrAmpClass = Sim.linBud.UE.pwrAmpClas_{ pwrac };
%                         situ = Sim.linBud.UE.situ_{pwrac};
%                 
%                         for op = 1 : sz5
%                             operator = Sim.dem.operators{op};
%                         
%                             if nnz(ot) == 1
%                                 thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v = ...
%                                     thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v + ...
%                                     thisDem(ot).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v;
%                                 thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).satis(...
%                                     ~thisDem(ot).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).satis) = false;
%                             else
%                                 otf = find(ot);
%                                 for n1 = 1 : length(otf)
%                                     thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v = ...
%                                         thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v + ...
%                                         thisDem(otf(n1)).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v;
%                                     thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).satis(...
%                                         ~thisDem(otf(n1)).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).satis) = false;
%                                 end
% %                                 arrayfun(@(x) full(x.(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v),thisDem(ot),'UniformOutput',false)
% %                                 thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v = ...
% %                                     thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v + ...
% %                                     thisDem(ot).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).v;
% %                                 thisDem(ce).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).satis(...
% %                                     ~thisDem(ot).(fam).(curTrafClass).(pwrAmpClass).(situ).(operator).satis) = false;
%                             end
%                         end
%                     end
%                 end
%             end
%             
%             IND4remo(ot) = true;
%         end
%     end
% end
% thisDem(IND4remo) = [];
% fprintf(1,'DONE\n');

% figure
% plot([thisDem.J],[thisDem.I],'.')
% axis equal

%% Hardcode cruShipTer
IND2 = strcmp(Sim.demTranslati(:,1),'cruShipTer') & ...
    strcmp(Sim.demTranslati(:,2),'Streaming') & ...
    strcmp(Sim.demTranslati(:,3),'handh') & ...
    strcmp(Sim.demTranslati(:,4),'outd') & ...
    strcmp(Sim.demTranslati(:,5),'cruShipTer');
if any(IND2)
%     'D:\Dropbox (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication\05 T6-4\06 Study item 4 - techno-economic method\Investi\distri'
    A = xlsread('Liverpool Street v0_1.xlsx');
    A = A ./ 0.18;  % Because it was only Vodafone data
    A(isnan(A)) = 0;
    % 'D:\Dropbox (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication\05 T6-4\04 Study item 2 - demand distribution\Demand density estimates spreadsheet\MONARCH_NORMA_VNI_comparisons_v0_1 ob comments.xlsx'
    M = [2016 176.6./84.8  % From end of 2015 to end of 2016, UK
        2017 159.3./112.8  % From end of 2016 to end of 2017, UK
        2018 221.5./159.3  % From end of 2017 to end of 2018, UK
        2019 306.7./221.5  % From end of 2018 to end of 2019, UK
        2020 421.6./306.7  % From end of 2019 to end of 2020, UK
        2021 577.3./421.6  % From end of 2020 to end of 2021, UK
        2022 1.3  % Thenafter
        2023 1.3
        2024 1.3
        2025 1.3
        2026 1.3
        2027 1.3
        2028 1.3
        2029 1.3
        2030 1.3];
    % 'CTALTONA' 'STEINW1' 'GRASBROW'
    passeCou = [0 0 0
        3055 0 608
        3432 0 0
        0 0 2560
        2500 4077 0
        0 4301 0];
    for n = 1 : length(studyA.cruShipTer)
        polyin = polyshape(studyA.cruShipTer(n).X,studyA.cruShipTer(n).Y);
        [x,y] = centroid(polyin);
        [I,J] = studyA.R.worldToDiscrete(x,y);

        k = find(studyA.(['INcruShipTer' num2str(n)]));
        Lia = find(ismember([thisDem.IND],k));
        assert(length(Lia)==length(k))

        rowSub = (size(A,1)+1)./2 + [thisDem(Lia).I] - I;
        colSub = (size(A,2)+1)./2 + [thisDem(Lia).J] - J;
        for m = 1 : length(Lia)
            thisDem(Lia(m)).v(:,IND2) = 0;
            thisDem(Lia(m)).satis(:,IND2) = true;
            v = A(rowSub(m),colSub(m)) .* ...
                50.^2 .* passeCou(:,n) ./ max(passeCou(:)) .* ...
                prod(M(M(:,1)<=Sim.yea.thisYea,2)) .* 8 ./ 3600;
            thisDem(Lia(m)).v(:,IND2) = v;
            thisDem(Lia(m)).satis(v>0,IND2) = false;
        end
    end
end

%% Plot traffic volume distribution for indoor
% THIS WILL NOT WORK AND IT WILL BE REQUIRED TO GET AMMENDED...
% Prepare for plots below
demPlot = thisDem(studyA.INfoc([thisDem.IND]));
[X,Y] = studyA.R.intrinsicToWorld([demPlot.J],[demPlot.I]);
X = num2cell(X);
Y = num2cell(Y);
[demPlot(:).X] = deal(X{:}); %HO: saving in demoPlot the demand points coordinates
[demPlot(:).Y] = deal(Y{:}); %HO: saving in demoPlot the demand points coordinates

for uc = 1 : sz4  % For each family
    fam = Sim.dem.famUseCase_{uc};
    for on = 1 : length(Sim.dem.operators)
        if any(strcmp(Sim.demTranslati(:,1),fam) & ...
                strcmp(Sim.demTranslati(:,2),'Streaming') & ...
                strcmp(Sim.demTranslati(:,5),Sim.dem.operators{on}))
            for hn = 1 : sz1  % For each modelled hour
                Z = arrayfun(@(x) sum(full(x.v(hn,...
                    strcmp(Sim.demTranslati(:,1),fam) & ...
                    strcmp(Sim.demTranslati(:,2),'Streaming') & ...
                    strcmp(Sim.demTranslati(:,5),Sim.dem.operators{on}))),...
                    2),demPlot);
                if any(Z>0)
                    Z = Z .* 3600 ./ 8 ./ studyA.R.CellExtentInWorldX ./ ...
                        studyA.R.CellExtentInWorldY;  % MB/m2 in the modelled hour

%                     Map = [0 0.002 0 255 255
%                         0.002 0.005 255 255 0
%                         0.005 0.008 0 0 255
%                         0.008 0.01 255 165 0
%                         0.01 0.02 255 192 203
%                         0.02 0.04 255 0 0
%                         0.04 1.8 165 42 42];  % Good for 2017
                    Map = [0 0.005 0 255 255
                        0.005 0.010 255 255 0
                        0.010 0.050 0 0 255
                        0.050 0.100 255 165 0
                        0.100 0.500 255 192 203
                        0.500 1.000 255 0 0
                        1.000 Inf 165 42 42];

                    txt = {['Generated traffic volume, ' ...
                        Sim.dem.operators{on} ', '];
                        [num2str(Sim.yea.thisYea) ', ' ...
                        Sim.dem.hourDistri{hn,1} ' ' ...
                        Sim.dem.hourDistri{hn,2} ', MB/m2']};
                    for k = 1 : length(txt)
                        txt{k}(strfind(txt{k},'_')) = ' ';
                    end
                    
                    ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
                    figure('paperpositionmode','auto','unit','in',...
                        'pos',[1 1 7+1./16 (7+1./16).*ratio])
                    hold on
                    axis equal off
                    set(gca,'pos',[0 0 0.80 0.97])
                    
                    plot(studyA.X,studyA.Y,'k','linewidth',1)
                    
                    for mn = 1 : size(Map,1)
                        IND = Map(mn,1)<Z & Z<=Map(mn,2);
                        if any(IND)
                            patch(bsxfun(@plus,[demPlot(IND).X],...
                                studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
                                bsxfun(@plus,[demPlot(IND).Y],...
                                studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),...
                                ones(5,nnz(IND)).*mn,...
                                'edgecolor','none',...
                                'facecolor',Map(mn,3:5)./255)
                        end
                    end
                    
                    colormap(Map(:,3:5)./255)
                    set(gca,'clim',[0.5 size(Map,1)+0.5])
                    h = colorbar();
                    set(h,'ytick',1:size(Map,1))
                    A1 = cellfun(@(x) [num2str(x(1)) '<x<=' num2str(x(2))],...
                        num2cell(Map(:,1:2),2),'UniformOutput',false);
                    set(h,'yticklabel',A1)
                    for k = 1 : length(txt)
                        k1 = strfind(txt{k},'Deutsche Telekom');
                        if ~isempty(k1)
                            txt{k} = [txt{k}(1:k1-1) 'MNO' ...
                                txt{k}(k1+length('Deutsche Telekom'):end)];
                        end
                    end
                    title(txt)
                    
                    % Add background map
                    plo_m(Sim,'ShowLabels',1,'MapType','roadmap',...
                        'brighten',0.5,'grayscale',1)
                    
                    % Map decorations
                    add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);
                    
                    for k = 1 : length(txt)
                        txt{k}(strfind(txt{k},',')) = '';
                        txt{k}(strfind(txt{k},'/')) = '_';
                    end
                    
                    % savefig takes too long, thus commented out
                    %                             savefig(fullfile('OutpM',[txt{:}]))
                    warning off all
                    export_fig(fullfile('OutpM',[[txt{:}] '.jpg']),...
                        '-djpeg','-r600')
                    warning on all
                    delete(gcf)
                end
            end
        end
    end
end

% Plot maps of demand, per UE type, ml
% for uc = 1 : sz4  % For each family
%     fam = Sim.dem.famUseCase_{uc};
%     for on = 1 : length(Sim.dem.operators)
%         for pn = 1 : length(Sim.linBud.UE.pwrAmpClas_)
%             for m = 1 : length(Sim.dem.mobiClasses)
%                 pwrAmpClass = Sim.linBud.UE.pwrAmpClas_{pn};
%                 situ = Sim.linBud.UE.situ_{pn};
%                 mobiCla = Sim.dem.mobiClasses{m};
% 
%                 if any(strcmp(Sim.demTranslati(:,1),fam) & ...
%                         strcmp(Sim.demTranslati(:,2),'Streaming') & ...
%                         strcmp(Sim.demTranslati(:,3),pwrAmpClass) & ...
%                         strcmp(Sim.demTranslati(:,4),situ) & ...
%                         strcmp(Sim.demTranslati(:,5),Sim.dem.operators{on}) & ...
%                         strcmp(Sim.demTranslati(:,6),mobiCla))
%                     for hn = 1 : sz1  % For each modelled hour
%                         Z = arrayfun(@(x) full(x.v(hn,...
%                             strcmp(Sim.demTranslati(:,1),fam) & ...
%                             strcmp(Sim.demTranslati(:,2),'Streaming') & ...
%                             strcmp(Sim.demTranslati(:,3),pwrAmpClass) & ...
%                             strcmp(Sim.demTranslati(:,4),situ) & ...
%                             strcmp(Sim.demTranslati(:,5),Sim.dem.operators{on}) & ...
%                             strcmp(Sim.demTranslati(:,6),mobiCla))),demPlot);
%                         if any(Z>0)
%                             Z = Z .* 3600 ./ 8 ./ studyA.R.CellExtentInWorldX ./ ...
%                                 studyA.R.CellExtentInWorldY;  % MB/m2 in the modelled hour
% 
%             %                 sum(Z) .* studyA.R.CellExtentInWorldX .* ...
%             %                     studyA.R.CellExtentInWorldY ./ studyA.SHAPE_Area
% 
%  %HO: useful demand figures 
%                     %         figure
%                     %         scatter3([demPlot.X],[demPlot.Y],Z.',6,Z.','s','filled')
%                     %         camproj('perspective')
%                     %         axis tight
%                     %         daspect([20 20 1])
%                     %         set(gca,'visible','off')
% 
%                     %         figure
%                     %         hist(Z(Z>0))
%                     %         median(Z(Z>0))
%                             Map = [0 0.1 0 255 255
%                                 0.1 0.2 255 255 0
%                                 0.2 0.5 0 0 255
%                                 0.5 1.0 255 165 0
%                                 1.0 2.0 255 192 203
%                                 2.0 5.0 255 0 0
%                                 5.0 Inf 165 42 42];
%     %                         Map = [Map flipud(brewermap(size(Map,1),'RdYlBu'))]; %@HO: what is this function?
% 
%                             txt = {['Generated traffic volume, ' ...
%                                 Sim.dem.operators{on} ', ' pwrAmpClass ', ']
%                                 [situ ', ' mobiCla ', ' ...
%                                 num2str(Sim.yea.thisYea) ', ' ...
%                                 Sim.dem.hourDistri{hn,1} ' ' ...
%                                 Sim.dem.hourDistri{hn,2} ', MB/m2']};
%                             for k = 1 : length(txt)
%                                 txt{k}(strfind(txt{k},'_')) = ' ';
%                             end
% 
%                             ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
%                             figure('paperpositionmode','auto','unit','in',...
%                                 'pos',[1 1 7+1./16 (7+1./16).*ratio])
%                             hold on
%                             axis equal off
%                             set(gca,'pos',[0.06 0 0.88 1])
% 
%                             plot(studyA.X,studyA.Y,'k','linewidth',1)
% 
%                             for mn = 1 : size(Map,1)
%                                 IND = Map(mn,1)<Z & Z<=Map(mn,2);
%                                 if any(IND)
%                                     patch(bsxfun(@plus,[demPlot(IND).X],...
%                                         studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
%                                         bsxfun(@plus,[demPlot(IND).Y],...
%                                         studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),...
%                                         ones(5,nnz(IND)).*mn,...
%                                         'edgecolor','none',...
%                                         'facecolor',Map(mn,3:5)./255)
%                                 end
%                             end
% 
%                             colormap(Map(:,3:5)./255)
%                             set(gca,'clim',[0.5 size(Map,1)+0.5])
%                             h = colorbar();
%                             set(h,'ytick',1:size(Map,1))
%                             A1 = cellfun(@(x) [num2str(x(1)) '<x<=' num2str(x(2))],...
%                                 num2cell(Map(:,1:2),2),'UniformOutput',false);
%                             set(h,'yticklabel',A1)
%                             title(txt)
% 
%                             % Add background map
%                             plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)
% 
%                             % Map decorations
%                             add_scale('xy',Sim.disp.scale.x,Sim.disp.scale.y);
% 
%                             for k = 1 : length(txt)
%                                 txt{k}(strfind(txt{k},',')) = '';
%                                 txt{k}(strfind(txt{k},'/')) = '_';
%                             end
% 
%                             % savefig takes too long, thus commented out
% %                             savefig(fullfile('OutpM',[txt{:}]))
%                             warning off all
%                             export_fig(fullfile('OutpM',[[txt{:}] '.jpg']),...
%                                 '-djpeg','-r300')
%                             warning on all
%                             delete(gcf)
%                         end
%                     end
%                 end
%             end
%         end
%     end
% end
    
if isPlot == 1
    %%
    for n = 5 : 8
        [row,col,Z] = find(dem(n).A);
        [X,Y] = studyA.R.intrinsicToWorld(col,row);

        % min(Z)
        % max(Z)
        Map = [0 0.5 0 255 255
            0.5 1.0 255 255 0
            1.0 5.0 0 0 255
            5.0 10.0 255 165 0
            10.0 50.0 255 192 203
            50.0 100.0 255 0 0
            100.0 Inf 165 42 42];

        ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
        figure('paperpositionmode','auto','unit','in',...
            'pos',[1 1 7+1./16 (7+1./16).*ratio])
        hold on
        axis equal off
        set(gca,'pos',[0.06 0 0.88 1])

        plot(studyA.X,studyA.Y,'k','linewidth',1)

        for mn = 1 : size(Map,1)
            IND = Map(mn,1)<Z & Z<=Map(mn,2);
            if any(IND)
                patch(bsxfun(@plus,X(IND).',...
                    studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
                    bsxfun(@plus,Y(IND).',...
                    studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),...
                    ones(5,nnz(IND)).*mn,...
                    'edgecolor','none',...
                    'facecolor',Map(mn,3:5)./255)
            end
        end

        plot(561125,5930325,'ko')
        plot(561125,5930325,'k.')

        colormap(Map(:,3:5)./255)
        set(gca,'clim',[0.5 size(Map,1)+0.5])
        h = colorbar();
        set(h,'ytick',1:size(Map,1))
        A1 = cellfun(@(x) [num2str(x(1)) '<x<=' num2str(x(2))],...
            num2cell(Map(:,1:2),2),'UniformOutput',false);
        set(h,'yticklabel',A1)
        title(['Length of ' dem(n).layNam '  (m)'])

        set(gca,'xlim',[5.6016 5.6235].*1e5,'ylim',[5.9295 5.9309].*1e6)

        % Add background map
        plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)
    end

    %%
    for n = 5 : 8
        [row,col,Z] = find(dem(n).w);
        [X,Y] = studyA.R.intrinsicToWorld(col,row);

        % min(Z)
        % max(Z)
        Map = [0 1 0 255 255
            1 2 255 255 0
            2 5 0 0 255
            5 10 255 165 0
            10 20 255 192 203
            20 50 255 0 0
            50 Inf 165 42 42];

        ratio = (max(studyA.Y)-min(studyA.Y)) ./ (max(studyA.X)-min(studyA.X));  % Aspect ratio
        figure('paperpositionmode','auto','unit','in',...
            'pos',[1 1 7+1./16 (7+1./16).*ratio])
        hold on
        axis equal off
        set(gca,'pos',[0.06 0 0.88 1])

        plot(studyA.X,studyA.Y,'k','linewidth',1)

        for mn = 1 : size(Map,1)
            IND = Map(mn,1)<Z & Z<=Map(mn,2);
            if any(IND)
                patch(bsxfun(@plus,X(IND).',...
                    studyA.R.CellExtentInWorldX./2.*[-1 -1 1 1 -1].'),...
                    bsxfun(@plus,Y(IND).',...
                    studyA.R.CellExtentInWorldY./2.*[-1 1 1 -1 -1].'),...
                    ones(5,nnz(IND)).*mn,...
                    'edgecolor','none',...
                    'facecolor',Map(mn,3:5)./255)
            end
        end

        plot(561125,5930325,'ko')
        plot(561125,5930325,'k.')

        colormap(Map(:,3:5)./255)
        set(gca,'clim',[0.5 size(Map,1)+0.5])
        h = colorbar();
        set(h,'ytick',1:size(Map,1))
        A1 = cellfun(@(x) [num2str(x(1)) '<x<=' num2str(x(2))],...
            num2cell(Map(:,1:2),2),'UniformOutput',false);
        set(h,'yticklabel',A1)
        title(['Weight of ' dem(n).layNam '  (m)'])

        set(gca,'xlim',[5.6016 5.6235].*1e5,'ylim',[5.9295 5.9309].*1e6)

        % Add background map
        plo_m(Sim,'ShowLabels',1,'MapType','roadmap','brighten',0.5,'grayscale',1)
    end
    
    %% %HO: the following plotting are not working properly.. HO to look again here. Here are mainly plotting per mobility class the demand. It would be interesting to plot this at a certain point..
    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,1,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('Broadband, home')

    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,2,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('Broadband, business sme')

    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,3,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('Broadband, large business')
    
    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,4,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('Broadband, pedestrian')
    
    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,5,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('Broadband, low mobility')
    
    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,6,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('Broadband, high mobility')
    
    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,7,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('I2V, stationary')
    
    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,8,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('I2V, low mobility')
    
    figure
    plot(nonzeros(arrayfun(@(x) sum(sum(x.v(:,9,:),3),1),thisDem)) .* ...
        3600./8e3.*30.4167,'.')
    xlabel('Pixel index')
    ylabel('Traffic volume  (GB/month)')
    grid on
    title('I2V, high mobility')
end

% figure('PaperPositionMode','auto','unit','in',...
%     'pos',[1 1 7.5 2.63./3.5.*7.5])
% hold on
% plot(0.5:23.5,sum(cell2mat(arrayfun(@(x) full(...
%     x.MBB.Streaming.handh.outd.Vodafone.v(:,4)),...
%     thisDem(studyA.INfoc([thisDem.IND])),...
%     'UniformOutput',false).'),2),'linewidth',2,...
%     'disp','eMBB, Streaming, handheld, outdoor, pedestrian')
% plot(0.5:23.5,sum(cell2mat(arrayfun(@(x) full(...
%     x.MTC.Streaming.mac.indo.Vodafone.v(:,1)),...
%     thisDem(studyA.INfoc([thisDem.IND])),...
%     'UniformOutput',false).'),2),'linewidth',2,...
%     'disp','mMTC, white goods, indoor, home')
% plot(0.5:23.5,sum(cell2mat(arrayfun(@(x) full(...
%     x.uMTC.Streaming.veh.outd.Vodafone.v(:,4)),...
%     thisDem(studyA.INfoc([thisDem.IND])),...
%     'UniformOutput',false).'),2),'linewidth',2,...
%     'disp','uMTC, vehicle, outdoor, stationary')
% xlabel('Time')
% ylabel([{'Traffic volume within the simulation area'};{'(Mbit/s)'}])
% grid on
% title(['Vodafone, ' num2str(Sim.yea.thisYea)])
% legend('location','southoutside')
% print(gcf,fullfile('OutpM','Traffic volume within the simulation area'),...
%     '-djpeg','-r300')
% delete(gcf)

%% Example code to record a video of demand
% beep
% disp('Enable the video as standard output')
% set(gca,'CLim',[0 10.0])
% ti = datestr(now,'HH:MM');
% han = text(534200,183800,ti,'fontweight','bold','fontsize',18,...
%     'back','w','hor','right');
% Col = flipud([246 112 136
%     206 143 49
%     150 163 49]) ./ 255;
% colormap(Col)
% v = VideoWriter('5G mapping video','Uncompressed AVI');
% % v.Quality = 100;
% % v.FrameRate = 15;
% v.FrameRate = 1;
% open(v)
% for n = 1 : 24
%     A = accumarray(ceil([[thisDem.I].' [thisDem.J].']./10),...
%         arrayfun(@(x) sum(sum(x.v(n,1:6,:),2),3),thisDem),...
%         ceil(size(studyA.INfoc)./10));
%     [J,I] = meshgrid((10+1)./2 : 10 : ceil(size(studyA.INfoc,2)./10).*10,...
%         (10+1)./2 : 10 : ceil(size(studyA.INfoc,1)./10).*10);
%     [X,Y] = studyA.R.intrinsicToWorld(J,I);
%     IND = A > 0;
%     ph1 = patch(repmat(X(IND).',5,1) + ...
%         repmat((X(1,2)-X(1,1))./2.*[-1 1 1 -1 -1].',1,nnz(IND)),...
%         repmat(Y(IND).',5,1) + ...
%         repmat((Y(1,1)-Y(2,1))./2.*[-1 -1 1 1 -1].',1,nnz(IND)),...
%         repmat(A(IND).',5,1),...
%         'edgecolor','none','facealpha',0.5);
%     han.String = ['Time = ' datestr((n-0.5)./24,'HH:MM') ' hrs'];
%     
%     drawnow
%     writeVideo(v,getframe)
%     
%     delete(ph1)
% end
% close(v)
% clear A n X Y ti han Col ph1 m I J IND

%% Save subscriber details for device capabilities
% BBHomeSubs = struct('I', {PopHomes(1:length(PopHomes)).I}, ...
%     'J', {PopHomes(1:length(PopHomes)).J}, ...
%     'subs', {PopHomes(1:length(PopHomes)).subs});
% BBSmeSubs = Pop_sme;
% BBLaSubs = Pop_la;
% Data_BB = [pop h s la p_BB lo_BB hi_BB];
% Data_I2V = [p_I2V lo_I2V hi_I2V];

fprintf(1,'Distributing the demand for %d...DONE\n',Sim.yea.thisYea);

end

%% ACCU_INTO_PIX subfunction
function [thisDem,Sim] = accu_into_pix(thisDem,Sim,studyA,INDEX,subsDB,...
    demPerMobiliCla,trafPer,assoc,sz6,fam,op)
%ACCU_INTO_PIX accumulates the demand at each pixel for each mobility class, for the specific use
%       case family (uc) and operator (op). The current demand is given in demPerMobiliCla and the
%       association of demand service and mobility class through the respective table
%       (assoc). The trafPer is the percentage of data traffic for this operator and year
%
% * v0.1 KK 19Aug16 Created
% * v0.2 KK 22Aug16 Added thoroughfare
% * v0.3 AK Sep16   Added use case family, and operator information as well as the case when there
%                   are no subscribers available
% * v0.4 HO Nov18 Added comments from NORMA

layI = sparse(1:length(subsDB),[subsDB.layI],true,length(subsDB),sz6); %HO: length(subsDB) total number of sub. sz6 is the total number of layers (demand source). HO: this is done to increase the speed. Get the source (layer) index from the subscriber DB and store it in sparse logical format, MxN where M is the number of subscribers, N is the source index

CEtputI = find(Sim.dem.trafVol.(fam).CEtput.yea==Sim.yea.thisYea) + ...
    (0:length(Sim.dem.trafClas_)-1);
t_ = find(~all(isnan(Sim.dem.trafVol.(fam).CEtput.DL(:,CEtputI)),1)); %HO:For increased speed, find the services within the family and store their indices on t_.   %@HO: what are trying to find here? t_=1,2,3,4,5. Sim.dem.trafVol.(fam).CEtput.DL is given in input file which is for example the Tput in DL of MBB. Here we are finding which traffic classes to consider (streaming, computing, etc..)

% For each traffic class, accumulate the traffic of each mobility class and thoroughfare
for m = 1 : length(demPerMobiliCla)  % Per mobility class
    k_ = find(squeeze(any(any(any(demPerMobiliCla(m).A>0,4),2),1))); %HO: For each mobility class, find which UE types exist
    for kn = 1 : length(k_)  % Per UE type
        k = k_(kn);
        
        pwrAmpClass = Sim.linBud.UE.pwrAmpClas_{k}; %HO: UE type
        situ = Sim.linBud.UE.situ_{k}; %HO: UE ind/outd
        
        s_ = find(squeeze(any(any(demPerMobiliCla(m).A(:,:,k,:),2),1))).'; %HO: For each UE type, find which demand sources exist
        for sn = s_  % Per demand source. %HO: If there are any demand sources, then find how many are the UE antennas. If the number of antennas is zero, then the demand cannot be served, isProc2 = false oterwise isProc2=true
            IND = layI(:,sn); %HO: indices of subs for this demand layer (demand source). @HO: why in IND all values are zero?
            
            if any(any(demPerMobiliCla(m).A(:,:,k,sn)>0,2),1)  %HO: If there are any demand sources, then find how many are the UE antennas.
                aeCou = arrayfun(@(x) x.aeCou(k),subsDB(IND)); %HO: remember that only demand layers 5-8 have antenna count. HO: this should be the number of antenna per subs
                if any(aeCou>0)
    %                 Sim.linBud.(pwrAmpClass).(situ).AeCou
    %                 accumarray([[subsDB(IND).I]; [subsDB(IND).J]].',...
    %                     aeCou,size(studyA.INfoc),@(x) sum(x==2),[],true)
    %                 accumarray([[subsDB(IND).I]; [subsDB(IND).J]].',...
    %                     aeCou,size(studyA.INfoc),@(x) sum(x==4),[],true)
                    isProc2 = true;
                else
                    isProc2 = false;
                end

                
%                 if m==5 && k==3 && sn==5
%                     sdfsfd
%                     ans = full(A.*2);
%                 end
                
                %HO: Accumulate the demand percentages of the subscribers according to the pixel they fall into
                A = accumarray([[subsDB(IND).I]; [subsDB(IND).J]].',... %HO: coming back to dim 319x475 for NORMA
                    [subsDB(IND).percTrafVol].',size(studyA.INfoc),@sum,...
                    [],true);  %HO?: what are we doing here? Is it accumulating demanding from subs to pixel?
%                 end
                isProc = true;
            else %HO: if there are no demand sources..
                A = sparse(size(studyA.INfoc,1), size(studyA.INfoc,2)); %HO: then A becomes an empty sparse matrix
                isProc = false; %HO: there are no demand sources, set
                isProc2 = false;
            end

            if isProc
                 %HO: For each traffic class, multiply the percentage of traffic of each pixel with the total demand of the traffic class to convert % into Mbit/s
                for tn = 1 : length(t_)  % For each traffic class (5 traffic classes)
                    trafCl = Sim.dem.trafClas_{t_(tn)};  %HO: traffic class that we are dealing with ..
                    
                    v = bsxfun(@times,A(INDEX),...
                        demPerMobiliCla(m).A(:,t_(tn),k,sn).') .* ...
                        trafPer(m); %HO?: what are we doing here?
                    n_ = find(any(v,2)); %HO?: what are we doing here?
                    
                     %HO: Update the output thisDem
                    IND2 = strcmp(Sim.demTranslati(:,1),fam) & ...
                        strcmp(Sim.demTranslati(:,2),trafCl) & ...
                        strcmp(Sim.demTranslati(:,3),pwrAmpClass) & ...
                        strcmp(Sim.demTranslati(:,4),situ) & ...
                        strcmp(Sim.demTranslati(:,5),op) & ...
                        strcmp(Sim.demTranslati(:,6),Sim.dem.mobiClasses{m});
                    if isProc2
                        Sim.demTranslati{IND2,8} = aeCou(1);
                    end
                    for n = 1 : length(n_) %HO?: what are we doing here?
%                         thisDem(n_(n)).(fam).(trafCl).(pwrAmpClass).(...
%                             situ).(op).v(:,m) = ...
%                             thisDem(n_(n)).(fam).(trafCl).(pwrAmpClass).(...
%                             situ).(op).v(:,m).' + v(n_(n),:);
%                         thisDem(n_(n)).(fam).(trafCl).(pwrAmpClass).(...
%                             situ).(op).satis(v(n_(n),:)>0,m) = false;
%                         if isProc2
%                             thisDem(n_(n)).(fam).(trafCl).(...
%                                 pwrAmpClass).(situ).(op).aeCou = aeCou(1);
%                         end
                        thisDem(n_(n)).v(:,IND2) = ...
                            thisDem(n_(n)).v(:,IND2) + v(n_(n),:).'; %HO: populating v
                        thisDem(n_(n)).satis(v(n_(n),:)>0,IND2) = false; % HO: populating satisf
                        if isProc2
                            thisDem(n_(n)).aeCou(IND2) = aeCou(1); % HO: populating antenna count
                        end
                    end
                end
            end
        end
    end
end
end

%% DISTRI_TRAFVOL_ACROSS_SUBS subfunction
function subsDB = distri_trafVol_across_subs(subsDB,distri,Sim,fam,studyA)
%DISTRI_TRAFVOL_ACROSS_SUBS help function distributes the traffic of a specific use case family into
%       the respective subscribes. According to the data percentages in the distribution for each
%       layer, the percentage of traffic that corresponds to each subcriber is then calculated
%       HO: we are mainly here populating the field percTrafVol in subsDB for all layers in this function
%
% * v0.1 KK 19Aug16 Created
% * v0.2 KK 22Aug16 Added thoroughfare
% * v0.3 AK Oct16   Tidied it up to be used in a multi-family, multi-operator scenario
% * v0.4 HO Nov18   Put more comments in the function from NORMA review

for m = 1 : subsDB(end).layI %HO: so we are looping across each layer
    
    IND = find([subsDB.layI]==m); %HO: find indices of subs belonging to this layer
    
    % Set random number generator to desired state
    rng(Sim.seed.distri) %HO: seed from input file

    % Produce random numbers with the desired distribution
    isSubs = [subsDB(IND).isSubs];  % HO: Getting the subs belonging to this layer (IND)
    x = 1 - distri{m}(:,2);  %@HO: not usre what we have in distri (calculated in the previous function
    Y = [false; diff(x)==0];
    while any(Y) %@HO: what are we doing in this paragraph? -> HO: it looks like we loop until all Y are zero? Y is not used anyway.
        x(Y) = x(Y) + eps;
        Y = [false; diff(x)==0];
    end
    thisDistri = interp1(x,distri{m}(:,1),rand(length(IND),1)); %@HO: calling interp1 to do what? interp1 is a built in function.. -> We are creating random values which follows a specific distribution. thisDistri should follow the same distribution of distri{m}(:,1)
    thisDistri(isSubs)  = thisDistri(isSubs) ./ sum(thisDistri(isSubs)); %HO: get the percentage of traffic compared to total traffic (for this layer)
    thisDistri(~isSubs) = NaN;  %HO: setting the value of NaN for non subs
    
    % For each subscriber store its percentage of traffic over the total
    % traffic volume
    if ~isempty(isSubs)
        switch fam
            case {'MBB','CCTV','conneTrafLig','AR','ciITS','infot',...
                    'semiAutoDr','assisDr'}
                temp = num2cell(thisDistri(isSubs)); %HO: convert thisDistri to cells
                [subsDB(IND(isSubs)).percTrafVol] = deal(temp{:}); %HO: save in subsDB.percTrafVol
            case 'AGV'
                % Hardcoded info from Ken on number of AGV, see: D:\Dropbox
                % (Real Wireless)\5G_MONARCH_RW\09 WP6-Verfication\05
                % T6-4\04 Study item 2 - demand distribution\EC1 - sea port
                % demand\Modified slide on seaport demand v3.pptx
                %
                % Hardcoded info from Julie on automation dates, see Slide
                % 12 in: D:\Dropbox (Real Wireless)\5G_MONARCH_RW\11
                % run\CAPisce 5GM scenarios roadmap v0-3
                M = {'Altenwerder' 0 NaN
                    'Burchardkai' 177 2023
                    'Tollerort' 75 2025
                    'Eurogate' 124 2022};
                assert(isequal(M(:,1).',{studyA.AGV.name}),...
                    'Change M accordingly!')
                thisYeaCou = cell2mat(M(:,2)).' * ...
                    (Sim.yea.thisYea>=cell2mat(M(:,3)));
                for n = 1 : length(studyA.AGV)
                    str = ['INagv' num2str(n)];
                    IND2 = studyA.(str)(sub2ind(studyA.R.RasterSize,...
                        [subsDB(IND(isSubs)).I],[subsDB(IND(isSubs)).J]));
                    % Fraction of total demand in this polygon
                    tmp2 = M{n,2} ./ thisYeaCou .* ...
                        (Sim.yea.thisYea>=M{n,3});
                    % Fraction of total demand in this polygon, per pixel
%                     temp = M{n,2} .* (Sim.yea.thisYea>=M{n,3}) ./ ...
%                         thisYeaCou ./ nnz(isSubs&IND2);
                    tmp3 = thisDistri(isSubs&IND2);
                    temp = num2cell(tmp3./sum(tmp3).*tmp2);
                    [subsDB(IND(isSubs&IND2)).percTrafVol] = deal(temp{:});
                end
            otherwise
                error('Insert this info!')
        end
    end
end

% figure('PaperPositionMode','auto','unit','in',...
%     'pos',[1 1 7.5 2.63])
% hold on
% Col = parula(subsDB(end).layI);
% for m = 1 : subsDB(end).layI
%     plot(distri{m}(:,1),distri{m}(:,2),'.','color',Col(m,:),...
%         'disp',num2str(m))
% end
% grid on
% xlabel('distri(:,1)')
% ylabel('distri(:,2)')
% legend('location','eastoutside')
% print(gcf,'Spatial distribution v0_1','-djpeg','-r300')
end
